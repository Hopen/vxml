/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include <memory>
#include <vector>
#include <string>
#include <windows.h>
#include "sv_process.h"

namespace SVProcess
{

DWORD SetProcessPrivileges(DWORD	dwProcessId,
						   PCWSTR	pPrivs[], 
						   bool		pbEnabled[],
						   DWORD	dwPrivCount)
{
	HANDLE hToken = NULL;
	HANDLE hProcess = NULL;
	DWORD dwErr = ERROR_SUCCESS;

	try
	{
		if ((hProcess = ::OpenProcess(
			PROCESS_ALL_ACCESS,
			FALSE,
			dwProcessId)) == 0)
		{
			throw ::GetLastError();
		}

		if (::OpenProcessToken(
			hProcess,
			TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, 
			&hToken) == 0)
		{
			throw ::GetLastError();
		}

		DWORD dwSize = sizeof(TOKEN_PRIVILEGES) + sizeof(LUID_AND_ATTRIBUTES) * dwPrivCount;
		std::auto_ptr<TOKEN_PRIVILEGES> tkp((PTOKEN_PRIVILEGES)new BYTE[dwSize]);
		tkp->PrivilegeCount = 0;

		for (DWORD i = 0; i < dwPrivCount; i++)
		{
			if (::LookupPrivilegeValueW(NULL, pPrivs[i], &tkp->Privileges[tkp->PrivilegeCount].Luid) != 0)
			{
				tkp->Privileges[tkp->PrivilegeCount].Attributes = 
					pbEnabled[i] ? SE_PRIVILEGE_ENABLED : SE_PRIVILEGE_REMOVED;
				tkp->PrivilegeCount++;
			}
		}

		if (AdjustTokenPrivileges(
			hToken, 
			FALSE, 
			tkp.get(), 
			0,
			(PTOKEN_PRIVILEGES)NULL, 
			0) == 0)
		{
			throw ::GetLastError();
		}
	}
	catch (DWORD e)
	{
		dwErr = e;
	}
	catch (...)
	{
		dwErr = ERROR_EXCEPTION_IN_SERVICE;
	}

	if (hToken != NULL)
	{
		::CloseHandle(hToken);
	}
	if (hProcess != NULL)
	{
		::CloseHandle(hProcess);
	}

	return dwErr;
}

DWORD RemoveProcessPrivileges(DWORD dwProcessId, LPCWSTR pPrivs[], DWORD dwPrivCount)
{
	std::auto_ptr<bool> benable(new bool[dwPrivCount]);
	memset(benable.get(), false, dwPrivCount);
	return SetProcessPrivileges(dwProcessId, pPrivs, benable.get(), dwPrivCount);
}

PSYSTEM_PROCESSES GetProcessList()
{
	NTSTATUS Status;
	std::auto_ptr<SYSTEM_PROCESSES> pBuffer;

	try
	{
		for (ULONG cbBuffer = 0x10000; ; cbBuffer *= 2)
		{
			pBuffer.reset((PSYSTEM_PROCESSES)new char[cbBuffer]);

			Status = CNtAPISingleton::Instance().ZwQuerySystemInformation(
				SystemProcessesAndThreadsInformation,
				pBuffer.get(), 
				cbBuffer,
				NULL);

			if (Status != STATUS_INFO_LENGTH_MISMATCH)
			{
				break;
			}
		} 
	}
	catch (...)
	{
		return NULL;
	}

	return NT_SUCCESS(Status) ? pBuffer.release() : NULL;
}

template <class FN, class PARAM>
PSYSTEM_PROCESSES FindProcessHelper(PSYSTEM_PROCESSES pPrc, FN* pFn, PARAM par)
{
	for (;;)
	{
		if (pFn(pPrc, par))
		{
			return pPrc;
		}
		if (pPrc->NextEntryDelta == 0)
		{
			break;
		}
		pPrc = PSYSTEM_PROCESSES((LPBYTE)pPrc + pPrc->NextEntryDelta);
	}
	return NULL;
}

bool ProcessCmpById(PSYSTEM_PROCESSES pPrc, DWORD dwProcessId)
{
	return pPrc->ProcessId == dwProcessId;
}

bool ProcessCmpByName(PSYSTEM_PROCESSES pPrc, LPCWSTR pwszProcessName)
{
	if (!pPrc->ProcessName.Buffer)
	{
		return false;
	}
	return !wcsicmp(pPrc->ProcessName.Buffer, pwszProcessName);
}

PSYSTEM_PROCESSES FindProcess(PSYSTEM_PROCESSES pPrc, DWORD dwProcessId)
{
	return FindProcessHelper(pPrc, &ProcessCmpById, dwProcessId);
}

PSYSTEM_PROCESSES FindProcess(PSYSTEM_PROCESSES pPrc, LPCWSTR pwszProcessName)
{
	return FindProcessHelper(pPrc, &ProcessCmpByName, pwszProcessName);
}

DWORD GetProcessHandleCount(HANDLE hProcess)
{
	DWORD dwHandleCount = (DWORD)-1;

	try
	{
		CNtAPISingleton::Instance().ZwQueryInformationProcess(
			hProcess, 
			ProcessHandleCount,
			&dwHandleCount, 
			sizeof(DWORD), 
			NULL);
	}
	catch (...)
	{
	}

	return dwHandleCount;
}

DWORD GetProcessHandleCount(DWORD dwProcessId)
{
	std::auto_ptr<SYSTEM_PROCESSES> prcList(SVProcess::GetProcessList());
	PSYSTEM_PROCESSES pPrc = SVProcess::FindProcess(prcList.get(), dwProcessId);
	return pPrc ? pPrc->HandleCount : (DWORD)-1;
}

DWORD GetProcessThreadCount(DWORD dwProcessId)
{
	std::auto_ptr<SYSTEM_PROCESSES> prcList(SVProcess::GetProcessList());
	PSYSTEM_PROCESSES pPrc = SVProcess::FindProcess(prcList.get(), dwProcessId);
	return pPrc ? pPrc->ThreadCount : (DWORD)-1;
}

DWORD KillProcess(DWORD dwProcessId, DWORD dwTimeout)
{
	DWORD dwErr = ERROR_SUCCESS;
	HANDLE hProcess = NULL;

	try
	{
		if ((hProcess = ::OpenProcess(PROCESS_TERMINATE | SYNCHRONIZE, FALSE, dwProcessId)) == NULL)
		{
			throw ::GetLastError();
		}

		if (::TerminateProcess(hProcess, (UINT)-1) == 0)
		{
			throw ::GetLastError();
		}

		if (dwTimeout != 0 &&
			::WaitForSingleObject(hProcess, dwTimeout) != WAIT_OBJECT_0)
		{
			throw ::GetLastError();
		}
	}
	catch (DWORD e)
	{
		dwErr = e;
	}
	catch (...)
	{
		dwErr = ERROR_EXCEPTION_IN_SERVICE;
	}
	
	if (hProcess != NULL)
	{
		::CloseHandle(hProcess);
	}

	return dwErr;
}

/************************************************************************/
/* CProcessControl class implementation                                 */
/************************************************************************/

CProcessControl::CProcessControl()
{
	ZeroMemory(&m_PrcInfo, sizeof(m_PrcInfo));
}

CProcessControl::~CProcessControl()
{
	Detach();
}

bool CProcessControl::Create(
	PCWSTR					pApplicationName,
	PCWSTR					pCommandLine,
	DWORD					dwCreationFlags,
	PVOID					pEnvironment,
	PCWSTR					pCurrentDirectory,
	STARTUPINFOW*			pStartupInfo,
	LPSECURITY_ATTRIBUTES	pProcessAttributes,
	LPSECURITY_ATTRIBUTES	pThreadAttributes,
	bool					bInheritHandles)
{
	STARTUPINFOW si = { sizeof(si), 0 };

	Detach();

	return ::CreateProcessW(
		pApplicationName,
		(PWSTR)pCommandLine,
		pProcessAttributes,
		pThreadAttributes,
		bInheritHandles ? TRUE : FALSE,
		dwCreationFlags,
		pEnvironment,
		pCurrentDirectory,
		(!pStartupInfo) ? &si : pStartupInfo,
		&m_PrcInfo) != 0;
}

bool CProcessControl::CreateAsUser(
	HANDLE					hToken,
	PCWSTR					pApplicationName,
	PCWSTR					pCommandLine,
	DWORD					dwCreationFlags,
	STARTUPINFOW*			pStartupInfo,
	PVOID					pEnvironment,
	PCWSTR					pCurrentDirectory,
	LPSECURITY_ATTRIBUTES	pProcessAttributes,
	LPSECURITY_ATTRIBUTES	pThreadAttributes,
	bool					bInheritHandles)
{
	STARTUPINFOW si = { sizeof(si), 0 };

	Detach();

	return ::CreateProcessAsUserW(
		hToken,
		pApplicationName,
		(PWSTR)pCommandLine,
		pProcessAttributes,
		pThreadAttributes,
		bInheritHandles ? TRUE : FALSE,
		dwCreationFlags,
		pEnvironment,
		pCurrentDirectory,
		(!pStartupInfo) ? &si : pStartupInfo,
		&m_PrcInfo) != 0;
}

bool CProcessControl::CreateWithLogon(
	PCWSTR					pUsername,
	PCWSTR					pDomain,
	PCWSTR					pPassword,
	DWORD					nLogonFlags,
	PCWSTR					pApplicationName,
	PCWSTR					pCommandLine,
	DWORD					dwCreationFlags,
	PVOID					pEnvironment,
	PCWSTR					pCurrentDirectory,
	STARTUPINFOW*			pStartupInfo)
{
	STARTUPINFOW si = { sizeof(si), 0 };

	Detach();

	return !::CreateProcessWithLogonW(
		pUsername,
		pDomain,
		pPassword,
		nLogonFlags,
		pApplicationName,
		(PWSTR)pCommandLine,
		dwCreationFlags,
		pEnvironment,
		pCurrentDirectory,
		(!pStartupInfo) ? &si : pStartupInfo,
		&m_PrcInfo) != 0;
}

bool CProcessControl::Attach(HANDLE hProcess)
{
	Detach();

	if (::DuplicateHandle(
		::GetCurrentProcess(), 
		hProcess, 
		::GetCurrentProcess(), 
		&m_PrcInfo.hProcess, 
		0, 
		FALSE, 
		DUPLICATE_SAME_ACCESS) == 0)
	{
		return false;
	}

	m_PrcInfo.dwProcessId = GetProcessId(hProcess);

	return true;
}

bool CProcessControl::Attach(DWORD dwProcessId, DWORD dwDesiredAccess)
{
	Detach();

	if ((m_PrcInfo.hProcess = ::OpenProcess(
		dwDesiredAccess, 
		FALSE, 
		dwProcessId)) == 0)
	{
		return false;
	}

	m_PrcInfo.dwProcessId = dwProcessId;

	return true;
}

void CProcessControl::Detach()
{
	if (m_PrcInfo.hProcess)	::CloseHandle(m_PrcInfo.hProcess);
	if (m_PrcInfo.hThread)	::CloseHandle(m_PrcInfo.hThread);
	ZeroMemory(&m_PrcInfo, sizeof(m_PrcInfo));
}

bool CProcessControl::Terminate(UINT uExitCode)
{
	return ::TerminateProcess(m_PrcInfo.hProcess, uExitCode) != 0;
}

bool CProcessControl::WaitFor(DWORD dwTimeout)
{
	return ::WaitForSingleObject(m_PrcInfo.hProcess, dwTimeout) == WAIT_OBJECT_0;
}

bool CProcessControl::SetPrivileges(LPCWSTR pPrivs[], bool pbEnabled[], DWORD dwPrivCount)
{
	DWORD dwErr = SVProcess::SetProcessPrivileges(m_PrcInfo.dwProcessId, pPrivs, pbEnabled, dwPrivCount);
	::SetLastError(dwErr);
	return dwErr == ERROR_SUCCESS;
}

bool CProcessControl::RemovePrivileges(LPCWSTR pPrivs[], DWORD dwPrivCount)
{
	DWORD dwErr = SVProcess::RemoveProcessPrivileges(m_PrcInfo.dwProcessId, pPrivs, dwPrivCount);
	::SetLastError(dwErr);
	return dwErr == ERROR_SUCCESS;
}

FILETIME CProcessControl::GetCreationTime()
{
	FILETIME ftCreation, ft;
	::GetProcessTimes(m_PrcInfo.hProcess, &ftCreation, &ft, &ft, &ft);
	return ftCreation;
}

FILETIME CProcessControl::GetExitTime()
{
	FILETIME ftExit, ft;
	::GetProcessTimes(m_PrcInfo.hProcess, &ft, &ftExit, &ft, &ft);
	return ftExit;
}

FILETIME CProcessControl::GetKernelTime()
{
	FILETIME ftKernel, ft;
	::GetProcessTimes(m_PrcInfo.hProcess, &ft, &ft, &ftKernel, &ft);
	return ftKernel;
}

FILETIME CProcessControl::GetUserTime()
{
	FILETIME ftUser, ft;
	::GetProcessTimes(m_PrcInfo.hProcess, &ft, &ft, &ft, &ftUser);
	return ftUser;
}

DWORD CProcessControl::GetPID()
{
	return m_PrcInfo.dwProcessId;
}

HANDLE CProcessControl::GetHandle()
{
	return m_PrcInfo.hProcess;
}

HANDLE CProcessControl::GetMainThreadHandle()
{
	return m_PrcInfo.hThread;
}

PROCESS_INFORMATION CProcessControl::GetPrcInfo()
{
	return m_PrcInfo;
}

bool CProcessControl::GetMemoryInfo(PROCESS_MEMORY_COUNTERS* pmc)
{
	ZeroMemory(pmc, sizeof(*pmc));
	pmc->cb = sizeof(*pmc);
	return ::GetProcessMemoryInfo(m_PrcInfo.hProcess, pmc, pmc->cb) != 0;
}

PSYSTEM_PROCESSES CProcessControl::GetFullProcessInfo(PSYSTEM_PROCESSES* pPrcList)
{
	*pPrcList = SVProcess::GetProcessList();
	return FindProcess(*pPrcList, m_PrcInfo.dwProcessId);
}

DWORD CProcessControl::GetHandleCount()
{
	return SVProcess::GetProcessHandleCount(m_PrcInfo.hProcess);
}

DWORD CProcessControl::GetThreadCount()
{
	return SVProcess::GetProcessThreadCount(m_PrcInfo.dwProcessId);
}

}
