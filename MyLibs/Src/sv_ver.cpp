/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include <windows.h>
#include <stdio.h>
#include "sv_ver.h"

namespace SVVer
{

/************************************************************************/
/* ���������� ������ CWinVer                                            */
/************************************************************************/

// from winbase.h
#ifndef VER_PLATFORM_WIN32s
#define VER_PLATFORM_WIN32s             0
#endif
#ifndef VER_PLATFORM_WIN32_WINDOWS
#define VER_PLATFORM_WIN32_WINDOWS      1
#endif
#ifndef VER_PLATFORM_WIN32_NT
#define VER_PLATFORM_WIN32_NT           2
#endif
#ifndef VER_PLATFORM_WIN32_CE
#define VER_PLATFORM_WIN32_CE           3
#endif

/*
This table has been assembled from Usenet postings, personal
observations, and reading other people's code.  Please feel
free to add to it or correct it.

dwPlatFormID  dwMajorVersion  dwMinorVersion  dwBuildNumber
95             1              4               0             950
95 SP1         1              4               0        >950 && <=1080
95 OSR2        1              4             <10           >1080
98             1              4              10            1998
98 SP1         1              4              10       >1998 && <2183
98 SE          1              4              10          >=2183
ME             1              4              90            3000

NT 3.51        2              3              51
NT 4           2              4               0            1381
2000           2              5               0            2195
XP             2              5               1            2600
2003 Server    2              5               2            3790

CE             3
*/

CWinVer::CWinVer()
{
	bool bOsVersionInfoEx = true;
	
	ZeroMemory(&m_Version, sizeof(m_Version));
	m_Version.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEXW);

	if (!::GetVersionExW((OSVERSIONINFOW*)&m_Version))
	{
		bOsVersionInfoEx = false;
		m_Version.dwOSVersionInfoSize = sizeof(OSVERSIONINFOW);
		if (!::GetVersionExW((OSVERSIONINFOW*)&m_Version))
		{
			return;
		}
	}

	m_Version.dwBuildNumber &= 0xFFFF;	// Win 95 needs this
	m_nVersion = Unknown;
	
	swprintf(
		m_wszDotVersion, 
		L"%u.%u.%u", 
		m_Version.dwMajorVersion, 
		m_Version.dwMinorVersion, 
		m_Version.dwBuildNumber);

	if ((m_Version.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS) && 
		(m_Version.dwMajorVersion == 4))
	{
		if ((m_Version.dwMinorVersion < 10) && (m_Version.dwBuildNumber == 950))
			m_nVersion = W95;
		else if ((m_Version.dwMinorVersion < 10) && ((m_Version.dwBuildNumber > 950) && (m_Version.dwBuildNumber <= 1080)))
			m_nVersion = W95SP1;
		else if ((m_Version.dwMinorVersion < 10) && (m_Version.dwBuildNumber > 1080))
			m_nVersion = W95OSR2;
		else if ((m_Version.dwMinorVersion == 10) && (m_Version.dwBuildNumber == 1998))
			m_nVersion = W98;
		else if ((m_Version.dwMinorVersion == 10) && ((m_Version.dwBuildNumber > 1998) && (m_Version.dwBuildNumber < 2183)))
			m_nVersion = W98SP1;
		else if ((m_Version.dwMinorVersion == 10) && (m_Version.dwBuildNumber >= 2183))
			m_nVersion = W98SE;
		else if (m_Version.dwMinorVersion == 90)
			m_nVersion = WME;
	}
	else if (m_Version.dwPlatformId == VER_PLATFORM_WIN32_NT)
	{
		if ((m_Version.dwMajorVersion == 3) && (m_Version.dwMinorVersion == 51))
			m_nVersion = WNT351;
		else if ((m_Version.dwMajorVersion == 4) && (m_Version.dwMinorVersion == 0))
			m_nVersion = WNT4;
		else if ((m_Version.dwMajorVersion == 5) && (m_Version.dwMinorVersion == 0))
			m_nVersion = W2K;
		else if ((m_Version.dwMajorVersion == 5) && (m_Version.dwMinorVersion == 1))
			m_nVersion = WXP;
		else if ((m_Version.dwMajorVersion == 5) && (m_Version.dwMinorVersion == 2))
			m_nVersion = W2003SERVER;
	}
	else if (m_Version.dwPlatformId == VER_PLATFORM_WIN32_CE)
	{
		m_nVersion = WCE;
	}

	m_bServer = false;
	m_pwszSuite = L"";

	// Test for specific product on Windows NT 4.0 SP6 and later.
	if (bOsVersionInfoEx)
	{
		// Test for the workstation type.
		if (m_Version.wProductType == VER_NT_WORKSTATION)
		{
			if (m_Version.dwMajorVersion == 4)
				m_pwszSuite = L"Workstation";
			else if( m_Version.wSuiteMask & VER_SUITE_PERSONAL )
				m_pwszSuite = L"Home Edition";
			else
				m_pwszSuite = L"Professional";
		}
		// Test for the server type.
		else if (m_Version.wProductType == VER_NT_SERVER)
		{
			m_bServer = true;
			if (m_Version.dwMajorVersion == 5 && m_Version.dwMinorVersion == 2)
			{
				if (m_Version.wSuiteMask & VER_SUITE_DATACENTER)
					m_pwszSuite = L"Datacenter Edition";
				else if (m_Version.wSuiteMask & VER_SUITE_ENTERPRISE)
					m_pwszSuite = L"Enterprise Edition";
				else if (m_Version.wSuiteMask == VER_SUITE_BLADE)
					m_pwszSuite = L"Web Edition";
				else
					m_pwszSuite = L"Standard Edition";
			}
			else if (m_Version.dwMajorVersion == 5 && m_Version.dwMinorVersion == 0)
			{
				if (m_Version.wSuiteMask & VER_SUITE_DATACENTER)
					m_pwszSuite = L"Datacenter Server";
				else if (m_Version.wSuiteMask & VER_SUITE_ENTERPRISE)
					m_pwszSuite = L"Advanced Server";
				else
					m_pwszSuite = L"Server";
			}
			else // Windows NT 4.0
			{
				if  (m_Version.wSuiteMask & VER_SUITE_ENTERPRISE)
					m_pwszSuite = L"Server Enterprise Edition";
				else
					m_pwszSuite = L"Server";
			}
		}
	}
	else // Test for specific product on Windows NT 4.0 SP5 and earlier
	{
		HKEY hKey = NULL;
		WCHAR szProductType[128] = {0};
		DWORD dwBufLen = sizeof(szProductType);
		LONG lRet = 0;

		lRet = ::RegOpenKeyExW(
			HKEY_LOCAL_MACHINE,
			L"SYSTEM\\CurrentControlSet\\Control\\ProductOptions",
			0,
			KEY_QUERY_VALUE,
			&hKey);

		if (lRet == ERROR_SUCCESS)
		{
			lRet = ::RegQueryValueExW(
				hKey,
				L"ProductType",
				NULL, NULL,
				(LPBYTE)szProductType, &dwBufLen);

			if ((lRet == ERROR_SUCCESS) && (dwBufLen <= sizeof(szProductType)))
			{
				if (lstrcmpiW( L"WINNT", szProductType) == 0)
				{
					m_bServer = false;
					m_pwszSuite = L"Workstation";
				}
				else if (lstrcmpiW(L"LANMANNT", szProductType) == 0)
				{
					m_bServer = true;
					m_pwszSuite = L"Server";
				}
				else if (lstrcmpiW(L"SERVERNT", szProductType) == 0)
				{
					m_bServer = true;
					m_pwszSuite = L"Advanced Server";
				}
			}

			::RegCloseKey(hKey);
		}
	}
}

LPCWSTR CWinVer::GetVersionStr() const
{
	switch (m_nVersion)
	{
		case W95:			return L"Windows 95";
		case W95SP1:		return L"Windows 95 SP1";
		case W95OSR2:		return L"Windows 95 OSR2";
		case W98:			return L"Windows 98";
		case W98SP1:		return L"Windows 98 SP1";
		case W98SE:			return L"Windows 98 SE";
		case WME:			return L"Windows ME";
		case WNT351:		return L"Windows NT 3.51";
		case WNT4:			return L"Windows NT 4.0";
		case W2K:			return L"Windows 2000";
		case WXP:			return L"Windows XP";
		case W2003SERVER:	return L"Windows 2003 Server";
		case WCE:			return L"Windows CE";
	}
	return L"Unknown Windows version";
}

/************************************************************************/
/* ���������� ������ CModuleVersion                                     */
/************************************************************************/

CModuleVersion::CModuleVersion(LPCWSTR pwszFileName) :
	m_pVersionInfo(NULL)
{
	if (!Init(pwszFileName))
	{
		Destroy();
		return;
	}

	swprintf(
		m_wszDotVersion,
		L"%u.%u.%u.%u",
		HIWORD(m_FileInfo.dwProductVersionMS),
		LOWORD(m_FileInfo.dwProductVersionMS),
		HIWORD(m_FileInfo.dwProductVersionLS),
		LOWORD(m_FileInfo.dwProductVersionLS));
}

bool CModuleVersion::Init(LPCWSTR pwszFileName)
{
	m_Transl.charset = 1252;	// default = ANSI code page
	
	ZeroMemory(&m_FileInfo, sizeof(m_FileInfo));
	ZeroMemory(&m_Transl, sizeof(m_Transl));
	ZeroMemory(m_wszDotVersion, sizeof(m_wszDotVersion));

	int nVerLen;	
	DWORD dwDummyHandle; // will always be set to zero
   
	if ((nVerLen = ::GetFileVersionInfoSizeW((PWSTR)pwszFileName, &dwDummyHandle)) <= 0)
	{
		return false;
	}
	
	m_pVersionInfo = malloc(nVerLen);
	
	if (!::GetFileVersionInfoW(
		(PWSTR)pwszFileName, 
		dwDummyHandle, 
		nVerLen, 
		m_pVersionInfo))
	{
		return false;
	}

	PVOID pVI;
	UINT dwLen;
	
	if (!::VerQueryValueW(m_pVersionInfo, L"\\", &pVI, &dwLen))
	{
		return false;
	}
	
	// copy fixed info to myself, which am derived from VS_FIXEDFILEINFO
	m_FileInfo = *(VS_FIXEDFILEINFO*)pVI;

	PTRANSLATION pT;

	// Get translation info
	if (::VerQueryValueW(
		m_pVersionInfo, 
		L"\\VarFileInfo\\Translation", 
		(PVOID*)&pT, 
		&dwLen) && dwLen >= 4)
	{
		m_Transl = *pT;
	}

	return m_FileInfo.dwSignature == VS_FFI_SIGNATURE;
}

LPCWSTR CModuleVersion::GetValue(LPCWSTR pwszKeyName) const
{
	if (!IsValid())
	{
		return L"";
	}

	// To get a string value must pass query in the form
	//
	//    "\StringFileInfo\<langID><codepage>\keyname"
	//
	// where <langID><codepage> is the languageID concatenated with the
	// code page, in hex. Wow.
	//

	WCHAR wszQuery[1024] = {0};

	swprintf(
		wszQuery,
		L"\\StringFileInfo\\%04x%04x\\%s",
		(int)m_Transl.langID,
		(int)m_Transl.charset,
		pwszKeyName);

	LPCWSTR pVal = NULL;
	UINT dwLen = 0;
	
	return (::VerQueryValueW(m_pVersionInfo, wszQuery, (PVOID*)&pVal, &dwLen)) ? pVal : L"";
}

/************************************************************************/
/* ���������� ������ CDLLVersion                                        */
/************************************************************************/

bool CDLLVersion::Init(LPCWSTR pwszFileName)
{
	if (m_hDLL = ::GetModuleHandleW(pwszFileName))
	{
		return Init(m_hDLL);
	}

	if (m_hDLL = ::LoadLibraryW(pwszFileName))
	{
		m_bFreeAtExit = true;
	}
	
	return (m_hDLL) ? Init(m_hDLL) : false;
}

bool CDLLVersion::Init(HMODULE hDLL)
{
	DLLGETVERSIONPROC pDllGetVersion;
	
	if (!(pDllGetVersion = (DLLGETVERSIONPROC)::GetProcAddress(m_hDLL, "DllGetVersion")))
	{
		return false;
	}

	m_hDLL = hDLL;

	ZeroMemory(&m_DVI, sizeof(m_DVI));
	m_DVI.info1.cbSize = sizeof(m_DVI);
	
	return SUCCEEDED((*pDllGetVersion)((DLLVERSIONINFO*)&m_DVI));
}

void CDLLVersion::Destroy()
{
	if (m_bFreeAtExit)
	{
		FreeLibrary(m_hDLL);
		m_hDLL = NULL;
	}
}

}
