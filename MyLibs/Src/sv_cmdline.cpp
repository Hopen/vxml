/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include "Common.h"
#include "sv_cmdline.h"


namespace SVCmdline
{

using namespace std;

CCmdlineParser::CCmdlineParser()
{
	m_argc = 0;
	m_argv = NULL;
	m_bCaseSensitive = false;
	m_nMaxParams = -1;	// Unlimited params
}

CCmdlineParser::CCmdlineParser(int argc, wchar_t* argv[])
{
	m_bCaseSensitive = false;
	m_nMaxParams = -1;	// Unlimited params
	Reset(argc, argv);
}

CCmdlineParser::~CCmdlineParser()
{
	Clear(true);
}

void CCmdlineParser::Clear(bool bClearKeys)
{
	KEYSMAP::iterator it = m_KeysMap.begin();
	for (; it != m_KeysMap.end(); ++it)
	{
		delete it->second;
		it->second = NULL;
	}
	if (bClearKeys)
	{
		m_KeysMap.clear();
	}
}

void CCmdlineParser::Reset(int argc, wchar_t* argv[])
{
	Clear(false);
	m_argc = argc;
	m_argv = argv;
}

bool CCmdlineParser::AddKey(const wstring& sKeyName, 
							int nMinParams, 
							int nMaxParams, 
							const wstring& sKeyDescr, 
							const wstring& sParamDescr)
{
	KeyInfo key(sKeyName, m_bCaseSensitive);

	if (m_KeysMap.find(key) != m_KeysMap.end())
	{
		return false;
	}

	key.nMinParams = nMinParams;
	key.nMaxParams = nMaxParams;
	key.sKeyDescr = sKeyDescr;
	key.sParamDescr = sParamDescr;

	m_KeysMap[key] = NULL;

	return true;
}

bool CCmdlineParser::Parse()
{
	if (!m_argv)
	{
		return false;
	}
	
	Clear(false);

	for (int i = 1; i < m_argc; i++)
	{
		wstring sParam = m_argv[i];
		if (GetKey(sParam))
		{
			if (!ProcessKey(sParam, i))
			{
				return false;
			}
		}
		else
		{
			m_Params.push_back(sParam);
		}
	}

	if (m_Params.size() > (size_t)m_nMaxParams)
	{
		if (!OnMaxParams((int)m_Params.size()))
		{
			return false;
		}
	}

	return true;
}

bool CCmdlineParser::GetKey(wstring& sParam)
{
	PREFIXES::iterator it = m_Prefixes.begin();
	
	for (; it != m_Prefixes.end(); ++it)
	{
		wstring sPrefix = (*it);
		wstring sKey = sParam;

/*		if (!m_bCaseSensitive)
		{
			wcsupr((wchar_t*)sKey.c_str());
			wcsupr((wchar_t*)sPrefix.c_str());
		}*/

		if (wcsstr(sParam.c_str(), sPrefix.c_str()) == sParam.c_str())
		{
			sParam = sKey.substr(sPrefix.length(), sKey.length() - sPrefix.length());
			return true;
		}
	}

	return false;
}

bool CCmdlineParser::ProcessKey(const wstring& sKey, int& nPos)
{
	KEYSMAP::iterator it = m_KeysMap.find(KeyInfo(sKey, m_bCaseSensitive));

	if (it == m_KeysMap.end())
	{
		if (!OnInvalidKey(sKey))
		{
			return false;
		}
	}
	else
	{
		if (it->second)
		{
			if (!OnDuplicateKey(sKey))
			{
				return false;
			}
		}

		const KeyInfo& keyInfo = it->first;
		auto_ptr<KeyData> pKeyData(new KeyData);
		
		pKeyData->nPos = nPos;
		it->second = pKeyData.get();
		
		if (!keyInfo.nMinParams && !keyInfo.nMaxParams)
		{
			pKeyData.release();
			return true;
		}
		
		int nParamCount = 0;
		wstring sParam;
		for (nPos++; nPos < m_argc; nPos++)
		{
			sParam = m_argv[nPos];
			if (GetKey(sParam))
			{
				break;
			}
			pKeyData->sParams.push_back(sParam);
			nParamCount++;
		}

		if ((keyInfo.nMinParams > 0 && nParamCount < keyInfo.nMinParams) ||
			(keyInfo.nMaxParams > 0 && nParamCount > keyInfo.nMaxParams))
		{
			if (!OnInvalidKeyParamCount(sParam, nParamCount))
			{
				return false;
			}
		}
		
		pKeyData.release();

		if (nPos < m_argc)
		{
			return ProcessKey(sParam, nPos);
		}
	}

	return true;
}

void CCmdlineParser::PrintHelp(const wstring& sPrefix, 
							   const wstring& sSuffix) const
{
	if (!sPrefix.empty())
	{
		wprintf(L"%s\n", sPrefix.c_str());
	}
	
	KEYSMAP::iterator it = m_KeysMap.begin();
	const wchar_t* pwszPrefix = m_Prefixes.empty() ? L"" : m_Prefixes[0].c_str();

	for (; it != m_KeysMap.end(); ++it)
	{
		const wchar_t* pwszKeyName = it->first.sName.c_str();
		const wchar_t* pwszKeyDescr = it->first.sKeyDescr.c_str();
		const wchar_t* pwszParamDescr = it->first.sParamDescr.c_str();
		if (!pwszKeyDescr[0])
		{
			wprintf(
				L"%s%s %s\n",
				pwszPrefix,
				pwszKeyName,
				pwszParamDescr);
		}
		else if (!pwszParamDescr[0])
		{
			wprintf(
				L"%s%-16s %s\n",
				pwszPrefix,
				pwszKeyName,
				pwszKeyDescr);
		}
		else
		{
			wprintf(
				L"%s%s %s\n\t%s\n",
				pwszPrefix,
				pwszKeyName,
				pwszParamDescr,
				pwszKeyDescr);
		}
	}

	if (!sSuffix.empty())
	{
		wprintf(L"%s\n", sSuffix.c_str());
	}
}

CCmdlineParser::KEYS CCmdlineParser::GetKeys() const
{
	KEYS keysMap;
	KEYSMAP::iterator it = m_KeysMap.begin();
	for (; it != m_KeysMap.end(); ++it)
	{
		if (it->second)
		{
			keysMap[it->first.sName] = it->second;
		}
	}
	return keysMap;
}

}
