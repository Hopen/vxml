/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include <windows.h>
#include <process.h>
#include "sv_thread.h"

namespace SVThread
{

/************************************************************************/
/* ���������� ������ CThread                                            */
/************************************************************************/

CThread::CThread(LPTHREAD_START_ROUTINE pStartAddress,
				 LPVOID pParameter,
				 bool bSuspended,
				 DWORD dwStackSize,
				 LPSECURITY_ATTRIBUTES pSecurityAttributes)
{
	Create(
		pStartAddress,
		pParameter,
		bSuspended,
		dwStackSize,
		pSecurityAttributes);
}

bool CThread::Create(LPTHREAD_START_ROUTINE pStartAddress,
					 LPVOID pParameter,
					 bool bSuspended,
					 DWORD dwStackSize,
					 LPSECURITY_ATTRIBUTES pSecurityAttributes)
{
	return CreateRemote(
				GetCurrentProcess(),
				pStartAddress,
				pParameter,
				bSuspended,
				dwStackSize,
				pSecurityAttributes);
}

bool CThread::CreateRemote(HANDLE hRemoteProcess,
						   LPTHREAD_START_ROUTINE pStartAddress,
						   LPVOID pParameter,
						   bool bSuspended,
						   DWORD dwStackSize,
						   LPSECURITY_ATTRIBUTES pSecurityAttributes)
{
	Detach();

	m_ThreadParams.pParam = pParameter;
	m_ThreadParams.pThread = this;

	if (hRemoteProcess == ::GetCurrentProcess())
	{
		m_hThread = (HANDLE)_beginthreadex(
			pSecurityAttributes, 
			dwStackSize, 
			(unsigned (__stdcall*)(void*))(pStartAddress ? pStartAddress : (LPTHREAD_START_ROUTINE)ThreadStartRoutine), 
			pStartAddress ? pParameter : &m_ThreadParams,
			(bSuspended) ? CREATE_SUSPENDED : 0,
			(unsigned*)&m_dwThreadId);
	}
	else
	{
		m_hThread = ::CreateRemoteThread(
			hRemoteProcess,
			pSecurityAttributes,
			dwStackSize,
			pStartAddress ? pStartAddress : (LPTHREAD_START_ROUTINE)ThreadStartRoutine,
			pStartAddress ? pParameter : &m_ThreadParams,
			(bSuspended) ? CREATE_SUSPENDED : 0,
			&m_dwThreadId);
	}

	m_bInternalProc = pStartAddress == NULL;
	
	return m_hThread.IsValid();
}

bool CThread::Attach(DWORD dwThreadId, DWORD dwDesiredAccess)
{
	Detach();

	m_hThread = OpenThread(dwDesiredAccess, FALSE, dwThreadId);
	
	if (!m_hThread)
	{
		return false;
	}

	m_dwThreadId = dwThreadId;

	return true;
}

void CThread::Detach()
{
	m_hThread.Close();
	m_dwThreadId = 0;
	m_bInternalProc = false;
}

DWORD WINAPI CThread::ThreadStartRoutine(THREADPARAMS* pParams)
{
	DWORD dwRes;
	try
	{
		dwRes = pParams->pThread->ThreadProc(pParams->pParam);
	}
	catch (...)
	{
		dwRes = EVENT_E_INTERNALEXCEPTION;
	}
	return dwRes;
}

void CThread::Close()
{
	if (m_hThread)
	{
		if (m_bInternalProc)
		{
			::SetEvent(m_hExitEvent);
			WaitFor();
		}
		Detach();
	}
}

}
