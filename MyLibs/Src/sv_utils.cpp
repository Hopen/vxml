/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include <windows.h>
#include <shlwapi.h>
#include "sv_utils.h"
#include "sv_strutils.h"
#include "sv_handle.h"
//#include "C:\Program Files\Microsoft Visual Studio 8\VC\crt\src\mtdll.h"
//#include "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\crt\src\mtdll.h"
//#include <mtdll.h>

namespace SVUtils
{

using namespace std;

wstring WinErrToStr(DWORD dwErr)
{
    LPWSTR lpMsgBuf = NULL;

    FormatMessageW(
        FORMAT_MESSAGE_ALLOCATE_BUFFER |
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        dwErr,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPWSTR)&lpMsgBuf,
        0,
        NULL);

	if (lpMsgBuf)
	{
		wstring msg = trim_wstring(lpMsgBuf);
	    LocalFree(lpMsgBuf);
		return msg;
	}

	return wstring(L"");
}

bool SVUtils::IsFile(const wstring& sFileName)
{
	DWORD dwAttr = GetFileAttributesW(sFileName.c_str());
	return 
		((dwAttr != (DWORD)-1) && 
		!(dwAttr & FILE_ATTRIBUTE_DIRECTORY) &&
		!(dwAttr & FILE_ATTRIBUTE_REPARSE_POINT));
}

wstring GetModuleFileName(HMODULE hMod)
{
	WCHAR wszBuf[4096] = {0};
	::GetModuleFileNameW(hMod, wszBuf, 4096);
	return wszBuf;
}

wstring ExtractFileName(const wstring& sFileName)
{
	int pos = (int)sFileName.rfind(L'\\');
	if (pos < 0)
	{
		return sFileName;
	}
	return sFileName.substr(pos + 1, sFileName.length() - pos - 1);
}

wstring ExtractFilePath(const wstring& sPathName)
{
	LPWSTR pwszFileName = PathFindFileNameW(sPathName.c_str());
	return wstring(sPathName.c_str(), pwszFileName - sPathName.c_str());
}

wstring GetCompName(bool bGetDomain)
{
	if (bGetDomain)
	{
		WCHAR szBuf[4096] = {0};
		DWORD cbSize = sizeof(szBuf) / sizeof(WCHAR);
		if (GetComputerNameExW(ComputerNameDnsDomain, szBuf, &cbSize) == 0)
		{
			return L"";
		}
		wcscat(szBuf, L"\\");
		int nLen = (int)wcslen(szBuf);
		cbSize -= nLen;
		if (GetComputerNameExW(ComputerNameNetBIOS, szBuf + nLen, &cbSize) == 0)
		{
			return L"";
		}
		return wstring(szBuf);
	}

	WCHAR szCompName[4096] = {0};
	DWORD dwSize = sizeof(szCompName) / sizeof(WCHAR);
	GetComputerNameW(szCompName, &dwSize);
	return wstring(szCompName);
}

wstring GetFullPath(const wstring& sFileName, const wstring& sPath)
{
	LPWSTR pszFileName2 = NULL, p;
	DWORD dwSize;
	wstring sTmpFileName;

	if (!sPath.empty() && PathIsRelativeW(sFileName.c_str()) == TRUE)
	{
    	sTmpFileName = AddPathBackslash(sPath) + sFileName;
	}
	else
	{
		sTmpFileName = sFileName;
	}

	if (!(dwSize = ::GetFullPathNameW(
		sTmpFileName.c_str(), 
		0, 
		NULL, 
		&p)))
	{
		return sFileName;
	}

	pszFileName2 = new WCHAR[dwSize];

	::GetFullPathNameW(
		sTmpFileName.c_str(), 
		dwSize, 
		pszFileName2, 
		&p);

	wstring str(pszFileName2);
	delete pszFileName2;

	return str;
}

wstring AddPathBackslash(const wstring& sFileName)
{
	if (sFileName[sFileName.length() - 1] == L'\\')
	{
		return sFileName;
	}
	return sFileName + L'\\';
}

bool SVUtils::IsSameFile(const wstring& sFileName1, 
						 const wstring& sFileName2)
{
	return !cmpi_wstring(GetFullPath(sFileName1), GetFullPath(sFileName2));
}

bool SVUtils::CopyStringToClipboard(const wstring& sStr)
{
    if (!OpenClipboard(NULL)) 
	{
		return false;
	}

    EmptyClipboard();
    
    HANDLE hData = GlobalAlloc(
		GMEM_MOVEABLE | GMEM_DDESHARE,
        (sStr.length() + 1) * sizeof(WCHAR));
    LPVOID lpData = GlobalLock(hData);
    wcscpy((LPWSTR)lpData, sStr.c_str());
    GlobalUnlock(lpData);

    if (!SetClipboardData(CF_UNICODETEXT, hData) ||
		!CloseClipboard())
	{
		return false;
	}

    return true;
}

bool SVUtils::PasteStringFromClipboard(wstring& str)
{
    if (!OpenClipboard(NULL)) 
	{
		return false;
	}

	HANDLE hData = GetClipboardData(CF_UNICODETEXT);
	if (!hData)
	{
		CloseClipboard();
		return false;
	}
    LPVOID lpData = GlobalLock(hData);
	str = (LPWSTR)lpData;
    GlobalUnlock(lpData);

    if (!CloseClipboard())
	{
		return false;
	}

    return true;
}

bool SVUtils::ClearClipboard()
{
    if (!OpenClipboard(NULL) ||
		!EmptyClipboard() ||
		!CloseClipboard()) 
	{
		return false;
	}
	return true;
}

wstring SVUtils::GetProcessUser(DWORD dwPID)
{
	HANDLE hProcess = NULL;
	HANDLE hToken = NULL;
	DWORD dwSize = 0;
	BYTE buf[256];
	WCHAR szDomainName[MAX_COMPUTERNAME_LENGTH + 2] = {0};
	WCHAR szUserName[MAX_COMPUTERNAME_LENGTH + MAX_PATH + 2] = {0};
	SID_NAME_USE snu;
	PTOKEN_USER tokenUser;

	try
	{
		if (!(hProcess = OpenProcess(
			PROCESS_QUERY_INFORMATION,
			FALSE, 
			dwPID)))
		{
			throw 0;
		}

		if (!OpenProcessToken(hProcess, TOKEN_READ, &hToken))
		{
			throw 0;
		}

		tokenUser = (PTOKEN_USER)buf;
		if (!GetTokenInformation(hToken, TokenUser, buf, sizeof(buf), &dwSize))
		{
			throw 0;
		}

		dwSize = sizeof(szDomainName) / sizeof(WCHAR);

		if (!LookupAccountSidW(
			NULL, 
			tokenUser->User.Sid, 
			szDomainName, 
			&dwSize,
			szUserName, 
			&dwSize, 
			&snu))
		{
			throw 0;
		}

		if (szUserName[0])
		{
			wcscat(szUserName, L"\\");
		}

		wcscat(szUserName, szDomainName);
	}
	catch (...)
	{
	}

	if (hToken)
	{
		CloseHandle(hToken);
	}
	if (hProcess)
	{
		CloseHandle(hProcess);
	}

	return wstring(szUserName);
}

std::list<std::wstring> ParseCSV(const wstring& sCSV, WCHAR cDelimiter)
{
	std::list<std::wstring> smap;
	LPCWSTR p = sCSV.c_str(), q;
	while (true)
	{
		q = wcschr(p, cDelimiter);
		if (!q)
		{
			smap.push_back(trim_wstring(p));
			break;
		}
		else
		{
			smap.push_back(std::wstring(p, q - p));
			p = q + 1;
		}
	}
	return smap;
}

DWORD GetPESectionBase(LPCWSTR pwszFileName, PCSTR pszSectionName)
{
	try
	{
		HMODULE hMod = ::LoadLibraryExW(pwszFileName, NULL, LOAD_LIBRARY_AS_DATAFILE);

		if (!hMod)
		{
			return 0;
		}

		PIMAGE_DOS_HEADER		pDosHeader	= (PIMAGE_DOS_HEADER)((DWORD)hMod & 0xFFFF0000);
		PIMAGE_NT_HEADERS		pNTHeader	= (PIMAGE_NT_HEADERS)((PBYTE)pDosHeader + pDosHeader->e_lfanew);
		PIMAGE_SECTION_HEADER	pSection	= IMAGE_FIRST_SECTION(pNTHeader);
		DWORD					dwSections	= pNTHeader->FileHeader.NumberOfSections;

		for (DWORD i = 0; i < dwSections; i++, pSection++)
		{
			if (!strncmp((char*)pSection->Name, pszSectionName, sizeof(pSection->Name)))
			{
				DWORD dwBase = pSection->VirtualAddress + pNTHeader->OptionalHeader.ImageBase;
				::FreeLibrary(hMod);
				return dwBase;
			}
		}

		::FreeLibrary(hMod);
	}
	catch(...)
	{
	}

	return 0;
}

std::wstring DataToString(const void* pData, int nLen)
{
	const char* p = static_cast<const char*>(pData);
	static wchar_t hexChars[] = L"0123456789ABCDEF";
	int nChars = max(0, (nLen * 3) - 1);
	std::wstring sData(nChars, L' ');

	for (int i = 0; i < nLen; ++i, ++p)
	{
		sData[i * 3 + 0] = hexChars[(*p >> 4) & 0x0F];
		sData[i * 3 + 1] = hexChars[(*p >> 0) & 0x0F];
	}

	return sData;
}

/************************************************************************/
/* CFreeCrtPerThreadData class implementation                           */
/************************************************************************/

/*CFreeCrtPerThreadData::CFreeCrtPerThreadData()
{
}

CFreeCrtPerThreadData::~CFreeCrtPerThreadData()
{
	_ptiddata ptd = _getptd_noexit();

	if (ptd) {*/
		/*
		* Free up the _tiddata structure & its subordinate buffers
		*      _freeptd() will also clear the value for this thread
		*      of the FLS variable __flsindex.
		*/
		/*_freeptd(ptd);
	}
}*/


}
