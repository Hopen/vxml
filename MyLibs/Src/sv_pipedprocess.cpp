/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include <windows.h>
#include "sv_pipedprocess.h"

#define BUFSIZE					4096 
#define UNIQUE_PIPE_NAME		L"\\\\.\\pipe\\CPipedProcess"

CPipedProcess::CPipedProcess() :
	m_hProcess(NULL),
	m_hRead(NULL), m_hWrite(NULL),
	m_hIoThread(NULL), m_dwIoTID(0)
{
}

CPipedProcess::~CPipedProcess()
{
	Destroy();
}

bool CPipedProcess::Create(LPCWSTR pwszCmdLine)
{
	PROCESS_INFORMATION piProcInfo = {0}; 
	STARTUPINFOW siStartInfo = {0};

	Destroy();

	HANDLE hChildStdoutRd = NULL, hChildStdoutWr = NULL;
	HANDLE hChildStdinRd = NULL, hChildStdinWr = NULL;
	HANDLE hChildStdoutRdDup = NULL, hChildStdinWrDup = NULL;

	try
	{
		// Create a pipe for the child process's STDOUT. 

		if (!CreatePipe(UNIQUE_PIPE_NAME L"Stdout", &hChildStdoutRd, &hChildStdoutWr))
		{
			return false;
		}

		// Create noninheritable read handle and close the inheritable read 
		// handle. 

		if (::DuplicateHandle(
			::GetCurrentProcess(), hChildStdoutRd,
			::GetCurrentProcess(), &hChildStdoutRdDup, 
			0, FALSE, DUPLICATE_SAME_ACCESS) == 0)
		{
			return false;
		}

		CloseHandle(hChildStdoutRd);

		// Create a pipe for the child process's STDIN. 

		if (!CreatePipe(UNIQUE_PIPE_NAME L"Stdin", &hChildStdinRd, &hChildStdinWr)) 
		{
			return false;
		}

		// Duplicate the write handle to the pipe so it is not inherited. 

		if (::DuplicateHandle(
			::GetCurrentProcess(), hChildStdinWr, 
			::GetCurrentProcess(), &hChildStdinWrDup, 
			0, FALSE, DUPLICATE_SAME_ACCESS) == 0) 
		{
			return false;
		}

		CloseHandle(hChildStdinWr);

		// Set up members of the STARTUPINFO structure. 

		siStartInfo.cb = sizeof(STARTUPINFO); 
		siStartInfo.hStdError = hChildStdoutWr;
		siStartInfo.hStdOutput = hChildStdoutWr;
		siStartInfo.hStdInput = hChildStdinRd;
		siStartInfo.wShowWindow = SW_HIDE;
		siStartInfo.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;

		// Create the child process. 

		WCHAR wszCmdLine[MAX_PATH * 2] = {0};
		wcsncpy(wszCmdLine, pwszCmdLine, MAX_PATH * 2 - 1);

		if (CreateProcessW(
			NULL, 
			wszCmdLine,			// command line 
			NULL,				// process security attributes 
			NULL,				// primary thread security attributes 
			TRUE,				// handles are inherited 
			0,					// creation flags 
			NULL,				// use parent's environment 
			NULL,				// use parent's current directory 
			&siStartInfo,		// STARTUPINFO pointer 
			&piProcInfo) == 0)	// receives PROCESS_INFORMATION 
		{
			return false;
		}

		CloseHandle(piProcInfo.hThread);

		// Close the write end of the pipe before reading from the 
		// read end of the pipe. 

		CloseHandle(hChildStdoutWr);
		CloseHandle(hChildStdinRd);

		// Create IO thread

		m_hIoThread = ::CreateThread(NULL, 0, IoThreadProc, this, 0, &m_dwIoTID);
		if (m_hIoThread == NULL)
		{
			return false;
		}

		m_hProcess = piProcInfo.hProcess;
		m_hRead = hChildStdoutRdDup;
		m_hWrite = hChildStdinWrDup;
	}
	catch (...)
	{
		CloseHandle(hChildStdoutRd);
		CloseHandle(hChildStdoutWr);
		CloseHandle(hChildStdinRd);
		CloseHandle(hChildStdinWr);
		CloseHandle(hChildStdoutRdDup);
		CloseHandle(hChildStdinWrDup);
		CloseHandle(piProcInfo.hProcess);
		CloseHandle(piProcInfo.hThread);
	}

	return true;
}

void CPipedProcess::Destroy()
{
	if (m_hProcess)
	{
		::TerminateProcess(m_hProcess, 0);
		CloseHandle(m_hProcess);
	}
	CloseHandle(m_hRead);
	CloseHandle(m_hWrite);
	if (m_hIoThread != NULL)
	{
		if (::WaitForSingleObject(m_hIoThread, 2000) == WAIT_TIMEOUT)
		{
			::TerminateThread(m_hIoThread, (DWORD)-1);
		}
		CloseHandle(m_hIoThread);
	}
}

bool CPipedProcess::Write(LPCSTR pszCommand)
{
	DWORD dwWritten = 0;
	return ::WriteFile(m_hWrite, pszCommand, (DWORD)strlen(pszCommand), &dwWritten, NULL) != 0; 
}

DWORD WINAPI CPipedProcess::IoThreadProc(LPVOID lpParam)
{
	return ((CPipedProcess*)lpParam)->IoProc();
}

DWORD CPipedProcess::IoProc()
{
	DWORD dwRead; 
	CHAR chBuf[BUFSIZE] = {0};
	OVERLAPPED ovlp = {0};

	ovlp.hEvent = ::CreateEventW(NULL, FALSE, FALSE, NULL);

	for (;;) 
	{ 
		if (::ReadFile(m_hRead, chBuf, BUFSIZE, &dwRead, &ovlp) == 0 || 
			dwRead == 0)
		{
			if (::GetLastError() != ERROR_IO_PENDING)
			{
				break; 
			}
			::WaitForSingleObject(ovlp.hEvent, INFINITE);
			::GetOverlappedResult(m_hRead, &ovlp, &dwRead, TRUE);
		}
		if (::GetLastError() != ERROR_BROKEN_PIPE)
		{
			OnRead(chBuf, dwRead);
		}
	} 

	::CloseHandle(ovlp.hEvent);
	OnClose();

	return 0;
}

void CPipedProcess::CloseHandle(HANDLE& h)
{
	if (h != NULL)
	{
		::CloseHandle(h);
		h = NULL;
	}
}

bool CPipedProcess::CreatePipe(LPCWSTR pwszName, PHANDLE phRead, PHANDLE phWrite)
{
	SECURITY_ATTRIBUTES saAttr = {0}; 

	// Set the bInheritHandle flag so pipe handles are inherited. 

	saAttr.nLength = sizeof(SECURITY_ATTRIBUTES); 
	saAttr.bInheritHandle = TRUE; 
	saAttr.lpSecurityDescriptor = NULL; 

	HANDLE hRead = ::CreateNamedPipeW(
		pwszName,
		PIPE_ACCESS_INBOUND | FILE_FLAG_OVERLAPPED,
		PIPE_TYPE_BYTE | PIPE_WAIT,
		1,
		BUFSIZE,
		BUFSIZE,
		NMPWAIT_USE_DEFAULT_WAIT,
		&saAttr);

	if (hRead == NULL)
	{
		return false;
	}

	HANDLE hWrite = ::CreateFileW(
		pwszName,
		GENERIC_WRITE,
		0,
		&saAttr,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,
		NULL);

	if (hWrite == INVALID_HANDLE_VALUE)
	{
		::CloseHandle(hRead);
		return false;
	}

	*phRead = hRead;
	*phWrite = hWrite;

	return true;
}

bool CPipedProcess::WaitForProcess(DWORD dwTimeout)
{
	return ::WaitForSingleObject(m_hProcess, dwTimeout) == WAIT_OBJECT_0;
}
