/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include <windows.h>
#include <time.h>
#include "sv_log.h"
#include "sv_strutils.h"

namespace SVLog
{

/************************************************************************/
/* ���������� ������ CBinarySplitter                                    */
/************************************************************************/

bool CBinarySplitter::NeedSplit()
{
	// if not opened
	if (*(PULONGLONG)&m_ftLastSplit == 0)
	{
		return true;
	}

	SPLITCOND::const_iterator it = m_SplitCond.begin();
	for(; it != m_SplitCond.end(); ++it)
	{
		if (it->get()->NeedSplit(m_ftLastSplit, m_pBinaryWriter->GetSize()))
		{
			return true;
		}
	}

	return false;
}

void CBinarySplitter::Split()
{
	m_pBinaryWriter->Open(m_pNameGenerator->GenerateName().c_str());

	SYSTEMTIME st;
	::GetLocalTime(&st);
	::SystemTimeToFileTime(&st, &m_ftLastSplit);
}

bool CBinarySplitter::Write(IFormatter* pFmt)
{
	if (NeedSplit())
	{
		Split();
	}

	return m_pBinaryWriter->Write(pFmt->GetData().c_str(), pFmt->GetDataLen());
}

/************************************************************************/
/* ���������� ������� ������� ����������                                */
/************************************************************************/

bool CTimeOfDaySplitCondition::NeedSplit(FILETIME ftLastSplit, 
										 ULONGLONG /*nCurrentSize*/)
{
	SYSTEMTIME st;
	FILETIME ftLocal;
	
	::GetLocalTime(&st);
	::SystemTimeToFileTime(&st, &ftLocal);
	
	ULONGLONG k						= 864000000000;
	ULONGLONG nCurrentDateTime		= *(PULONGLONG)&ftLocal;
	ULONGLONG nCurrentSplitDateTime	= nCurrentDateTime / k * k + *(PULONGLONG)&m_ftTimeOfDay % k;
	ULONGLONG nLastSplitDateTime	= *(PULONGLONG)&ftLastSplit;

	return 
		(nCurrentDateTime > nCurrentSplitDateTime) && 
		(nCurrentSplitDateTime > nLastSplitDateTime);
}

bool CTimeSpanSplitCondition::NeedSplit(FILETIME ftLastSplit, 
										ULONGLONG /*nCurrentSize*/)
{
	SYSTEMTIME st;
	FILETIME ftLocal;
	
	::GetLocalTime(&st);
	::SystemTimeToFileTime(&st, &ftLocal);

	return
		(*(PULONGLONG)&ftLocal - *(PULONGLONG)&ftLastSplit) > 
		*(PULONGLONG)&m_ftTimeSpan;
}

bool CDaySplitCondition::NeedSplit(FILETIME ftLastSplit, 
								   ULONGLONG /*nCurrentSize*/)
{
	SYSTEMTIME st;
	FILETIME ftLocal;
	ULONGLONG k = 864000000000;
	
	::GetLocalTime(&st);
	::SystemTimeToFileTime(&st, &ftLocal);

	return
		*(PULONGLONG)&ftLocal / k * k > 
		*(PULONGLONG)&ftLastSplit / k * k;
}

/************************************************************************/
/* ���������� ������ CDateTimeNameGenerator                             */
/************************************************************************/

std::wstring CDateTimeNameGenerator::GenerateName()
{
	time_t tt;
	struct tm* t;
	WCHAR wszBuf[MAX_PATH + 1] = {0};

	::time(&tt);
	t = ::localtime(&tt);

	if (!::wcsftime(wszBuf, MAX_PATH, m_sFormat.c_str(), t))
	{
		wcsncpy(wszBuf, m_sFormat.c_str(), MAX_PATH);
	}

	return std::wstring(wszBuf);
}

/************************************************************************/
/* ���������� ������ CSimpleWriter                                      */
/************************************************************************/

CSimpleWriter::CSimpleWriter()
{
}

CSimpleWriter::~CSimpleWriter()
{
	Close();
}

void CSimpleWriter::CreateThread()
{
	if (m_hThread.IsValid())
	{
		return;
	}

	m_MQEvent.Create();

	m_hThread = ::CreateThread(
		NULL, 
		0, 
		(LPTHREAD_START_ROUTINE)ThreadProcStub, 
		this, 
		0, 
		&m_dwThreadId);

	m_MQEvent.Wait();
	m_MQEvent.Close();
}

void CSimpleWriter::DestroyThread()
{
	if (!m_hThread.IsValid())
	{
		return;
	}

	if (::PostThreadMessageW(m_dwThreadId, WM_QUIT, 0, 0) != 0)
	{
		::WaitForSingleObject(m_hThread, INFINITE);
	}

	m_hThread.Close();
}

bool CSimpleWriter::Write(LPCVOID lpData, DWORD cbData)
{
	if (lpData && cbData)
	{
		HANDLE hHeap = ::GetProcessHeap();
		LPVOID pBuf = ::HeapAlloc(hHeap, 0, cbData);
		memcpy(pBuf, lpData, cbData);

		if (::PostThreadMessageW(m_dwThreadId, 0, (WPARAM)cbData, (LPARAM)pBuf))
		{
			return true;
		}

		::HeapFree(hHeap, 0, pBuf);
	}

	return false;
}

ULONGLONG CSimpleWriter::GetSize() const
{
	LARGE_INTEGER size = {0};
	::GetFileSizeEx(m_hFile, &size);
	return size.QuadPart;
}

void CSimpleWriter::Close()
{
	DestroyThread();
	m_hFile.Close();
}

bool CSimpleWriter::Open(LPCWSTR pwsName)
{
	DestroyThread();

	SVSysObj::CLocker<SVSysObj::CMutex> lock(m_SplitMutex.GetLocker());

	if ((m_hFile = ::CreateFileW(
		pwsName,
		GENERIC_WRITE,
		FILE_SHARE_WRITE | FILE_SHARE_READ | FILE_SHARE_DELETE,
		0,
		OPEN_ALWAYS,
		0,
		NULL)) == INVALID_HANDLE_VALUE)
	{
		return false;
	}
	
	CreateThread();

	LARGE_INTEGER offs = {0};
	::SetFilePointerEx(m_hFile, offs, NULL, FILE_END);

	return false;
}

DWORD CSimpleWriter::ThreadProc()
{
	MSG msg = {0};
	int nRes = 0;
	DWORD dwWritten = 0;

	::PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE);
	m_MQEvent.Set();

	for (; (nRes = ::GetMessage(&msg, NULL, 0, 0)) > 0;)
	{
		if (LPVOID lpData = (LPVOID)msg.lParam)
		{
			try
			{
				SVSysObj::CLocker<SVSysObj::CMutex> lock(m_SplitMutex.GetLocker());
				::WriteFile(m_hFile, lpData, (DWORD)msg.wParam, &dwWritten, 0);
				::HeapFree(::GetProcessHeap(), 0, lpData);
			}
			catch (...)
			{
			}
		}
	}

	return (nRes < 0) ? (DWORD)-1 : 0;
}

/************************************************************************/
/* ���������� ������ CStdLogFormatter                                   */
/************************************************************************/

CStdLogFormatter& CStdLogFormatter::operator +=(const std::wstring& s)
{
	m_wsBuf += s;
	return *this;
}

CStdLogFormatter& CStdLogFormatter::FormatV(LPCWSTR pwszFmt, 
											va_list args)
{
	m_wsBuf += formatv_wstring(pwszFmt, args);
	return *this;
}

CStdLogFormatter& CStdLogFormatter::Format(LPCWSTR pwszFmt, ...)
{
	va_list	args;
	va_start(args, pwszFmt);
	FormatV(pwszFmt, args);
	va_end(args);
	return *this;
}

void CStdLogFormatter::WritePrefix()
{
	SYSTEMTIME st;
	::GetLocalTime(&st);

	for (int i = 0; i < sizeof(DWORD) * 8; i++)
	{
		DWORD dwPrefix = m_dwPrefixFlags & (1 << i);
		switch (dwPrefix)
		{
			case 0:
				break;

			case ptDate:
				Format(L"%02d.%02d.%02d ", st.wDay, st.wMonth, st.wYear % 100);
				break;

			case ptTime:
				Format(L"%02d:%02d:%02d.%03d ", st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
				break;

			case ptPID:
				Format(L"%04X ", ::GetCurrentProcessId());
				break;

			case ptTID:
				Format(L"%04X ", ::GetCurrentThreadId());
				break;

			default:
				WriteUserPrefix(dwPrefix);
				break;
		}
	}
}

void CStdLogFormatter::WriteEndl()
{
	m_wsBuf += L"\r\n";
}

void CStdLogFormatter::WriteConsole()
{
	std::string s(GetData());
	::CharToOemA(s.c_str(), (char*)s.c_str());
	puts(s.c_str());
}

std::string CStdLogFormatter::GetData()
{
	size_t nLen = m_wsBuf.length();
	std::string s(nLen, ' ');
	
	::WideCharToMultiByte(
		CP_ACP, 0, m_wsBuf.c_str(), 
		(int)nLen, (char*)s.c_str(), (int)nLen, 
		NULL, NULL);

	return s;
}

}
