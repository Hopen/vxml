/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include <Windows.h>
#include <time.h>
#include <atlbase.h>
#include <MsXml2.h>
#include "sv_simplelog.h"

#define BUF_SIZE			0x1000		// 4K

namespace SVLog
{

CSimpleLogFile::CSimpleLogFile() : 
	m_hFile(INVALID_HANDLE_VALUE), m_dwPID(0), m_bLogFunc(false)
{
	ZeroMemory(m_wszFileNameFormat, sizeof(m_wszFileNameFormat));
	ZeroMemory(&m_stLastSplit, sizeof(m_stLastSplit));
	::InitializeCriticalSection(&m_CS);
}

CSimpleLogFile::~CSimpleLogFile()
{
	::DeleteCriticalSection(&m_CS);
}

bool CSimpleLogFile::Create()
{
	time_t tt;
	struct tm* t;
	WCHAR wszFileName[MAX_PATH] = {0};

	::time(&tt);
	t = ::localtime(&tt);

	if (!::wcsftime(wszFileName, MAX_PATH - 1, m_wszFileNameFormat, t))
	{
		wcsncpy(wszFileName, m_wszFileNameFormat, MAX_PATH - 1);
	}

	if (m_hFile != INVALID_HANDLE_VALUE)
	{
		Destroy();
	}

	m_hFile = ::CreateFileW(
		wszFileName,
		GENERIC_WRITE,
		FILE_SHARE_WRITE | FILE_SHARE_READ | FILE_SHARE_DELETE,
		0,
		OPEN_ALWAYS,
		0,
		NULL);

	m_dwPID = ::GetCurrentProcessId();

	return (m_hFile != INVALID_HANDLE_VALUE);
}

bool CSimpleLogFile::Init(LPCWSTR pwszFileNameFormat, bool bLogFunc)
{
	m_bLogFunc = bLogFunc;

	if (pwszFileNameFormat == NULL)
	{
		if (::GetModuleFileNameW(NULL, m_wszFileNameFormat, MAX_PATH - 5) == 0)
		{
			return false;
		}
		wcscat(m_wszFileNameFormat, L".log");
	}
	else
	{
		wcsncpy(m_wszFileNameFormat, pwszFileNameFormat, MAX_PATH - 1);
	}

	return Create();
}

bool CSimpleLogFile::InitFromXML(LPCWSTR pwszXMLFile)
{
	CComPtr<IXMLDOMDocument> xmlDocument;

	try
	{
		xmlDocument.CoCreateInstance(L"Msxml2.DOMDocument");

		if (xmlDocument == NULL)
		{
			throw 0;
		}

		VARIANT_BOOL bSuccess = VARIANT_FALSE;
		if FAILED(xmlDocument->load(CComVariant(pwszXMLFile), &bSuccess) || 
			bSuccess == VARIANT_FALSE)
		{
			throw 0;
		}

		CComPtr<IXMLDOMElement> xmlRoot;
		if (FAILED(xmlDocument->get_documentElement(&xmlRoot)))
		{
			throw 0;
		}

		CComPtr<IXMLDOMNode> xmlNode;
		if (FAILED(xmlRoot->selectSingleNode(L"/configuration/Log/FileNameGenerator", &xmlNode)))
		{
			throw 0;
		}

		CComQIPtr<IXMLDOMElement> xmlEl = (IXMLDOMNode*)xmlNode;
		CComVariant val;
		if (FAILED(xmlEl->getAttribute(L"Format", &val)))
		{
			throw 0;
		}

		if (val.vt != VT_BSTR)
		{
			throw 0;
		}

		wcsncpy(m_wszFileNameFormat, val.bstrVal, MAX_PATH - 1);

		if (SUCCEEDED(xmlEl->getAttribute(L"LogFuncName", &val)))
		{
			if (val.vt == VT_BSTR)
			{
				m_bLogFunc = (wcsicmp(val.bstrVal, L"true") == 0);
			}
		}
	}
	catch (...)
	{
		return Init(NULL);
	}

	return Create();
}

void CSimpleLogFile::Destroy()
{
	if (m_hFile != INVALID_HANDLE_VALUE)
	{
		::CloseHandle(m_hFile);
		m_hFile = INVALID_HANDLE_VALUE;
	}
}

void CSimpleLogFile::LogV(LPCWSTR pwszPrefix, 
					LPCWSTR pwszFunc, 
					LPCWSTR pwszFormat, 
					va_list prefixArgs, 
					va_list formatArgs)
{
	SYSTEMTIME	st				= {0};
	WCHAR		wszBuf[BUF_SIZE]= {0};
	CHAR		szBuf[BUF_SIZE]	= {0};
	int			k = 0;

	__try
	{
		::GetLocalTime(&st);

		if (m_stLastSplit.wYear == 0)
		{
			m_stLastSplit = st;
		}

		if (st.wDay != m_stLastSplit.wDay)
		{
			Create();
			m_stLastSplit = st;
		}

		int len = _snwprintf(
			wszBuf, BUF_SIZE - 3, L"%02d:%02d:%02d.%03d %04X %04X ",
			(int)st.wHour, (int)st.wMinute, (int)st.wSecond, (int)st.wMilliseconds, 
			m_dwPID, ::GetCurrentThreadId());

		if (len >= 0)
		{
			if (pwszPrefix != NULL)
			{
				k = _vsnwprintf(wszBuf + len, BUF_SIZE - len - 3, pwszPrefix, prefixArgs);
			}
			if (k >= 0)
			{
				len += k;
				k = _vsnwprintf(wszBuf + len, BUF_SIZE - len - 3, pwszFormat, formatArgs);
				if (k >= 0)
				{
					len += k;
					k = 0;
					if (pwszFunc != NULL && m_bLogFunc)
					{
						k = _snwprintf(wszBuf + len, BUF_SIZE - len - 3, L" [%s]", pwszFunc);
					}
					if (k >= 0)
					{
						len += k;
					}
					wcscpy(wszBuf + len, L"\r\n");
					len += 2;
				}
			}
		}

		::WideCharToMultiByte(CP_ACP, 0, wszBuf, len, szBuf, len, NULL, NULL);

		Write(szBuf, len);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
	}
}

void CSimpleLogFile::Write(LPCSTR pszInfo, DWORD dwLen)
{
	DWORD dwWritten = 0;
	::EnterCriticalSection(&m_CS);
	__try
	{
		::SetFilePointer(m_hFile, 0, 0, FILE_END);
		::WriteFile(m_hFile, pszInfo, dwLen, &dwWritten, NULL);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
	}
	::LeaveCriticalSection(&m_CS);
}

void CSimpleLogFile::Log(LPCWSTR pwszFunc, LPCWSTR pwszFmt, ...)
{
	va_list	args;
	va_start(args, pwszFmt);
	LogV(pwszFunc, pwszFmt, args);
	va_end(args);
}

void CSimpleLogFile::LogV(LPCWSTR pwszFunc, LPCWSTR pwszFmt, va_list args)
{
	LogV(NULL, pwszFunc, pwszFmt, (va_list)NULL, args);
}

void CSimpleLogFile::LogOSErr(LPCWSTR pwszFunc, LPCWSTR pwszFmt, ...)
{
	WCHAR wszBuf[1024] = {0};
	DWORD dwErr = GetLastError();

	FormatMessageW(
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dwErr,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		wszBuf,
		sizeof(wszBuf) / sizeof(WCHAR) - 1,
		NULL);

	va_list	args;
	va_start(args, pwszFmt);
	LogOSErr(pwszFunc, pwszFmt, args, dwErr, wszBuf);
	va_end(args);
}

void CSimpleLogFile::LogOSErr(LPCWSTR	pwszFunc, 
						LPCWSTR	pwszFmt, 
						va_list	args, 
						DWORD	dwErr, 
						LPCWSTR	pwszDescr)
{
	va_list errArgs;
	va_start(errArgs, args);
	LogV(pwszFmt, pwszFunc, L" (0x%08X,%s)", args, errArgs);
	va_end(errArgs);
}

/*void CSimpleLogFile::LogException(LPCWSTR	pwszInFunc, 
							LPCSTR	pwszFile, 
							int		nLine, 
							LPCSTR	pwszFunc, 
							const	SVExceptions::CException& e)
{
	Log(pwszInFunc, L"EXCEPTION File: %S, Line: %d, Func: %S, Desc: %s", 
		pwszFile, nLine, pwszFunc, e.Format().c_str());
}*/

}
