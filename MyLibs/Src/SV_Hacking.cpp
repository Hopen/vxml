/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include <Windows.h>
#include "SV_Hacking.h"

namespace SVHacking
{
	// CWin32VirtualProtect implementation
	
	CWin32VirtualProtect::CWin32VirtualProtect(LPVOID pAddr, 
											   DWORD dwSize, 
											   DWORD dwProtect)
		: m_pAddr(pAddr), m_dwSize(dwSize), m_dwOldProtect(dwProtect)
	{
		::VirtualProtect(pAddr, dwSize, dwProtect, &m_dwOldProtect);
	}

	CWin32VirtualProtect::~CWin32VirtualProtect()
	{
		::VirtualProtect(m_pAddr, m_dwSize, m_dwOldProtect, &m_dwOldProtect);
	}

	// CWin32UserModePatcher implementation
	CWin32UserModePatcher::CWin32UserModePatcher() 
		: m_dwCodeSize(0), m_pOldFunc(NULL)
	{
		memset(m_StubCode, cmdNOP, sizeof(m_StubCode));
	}

	LPVOID CWin32UserModePatcher::Patch(LPVOID pOldFunc, 
										LPVOID pNewFunc, 
										DWORD dwCodeSize/* = sizeof(OP32_CMD)*/)
	{
		try
		{
			if (*(LPBYTE)pOldFunc == cmdJMPrel32)
			{
				// Already patched
				return GetOldProc();
			}

			if (dwCodeSize < sizeof(OP32_CMD))
			{
				dwCodeSize = sizeof(OP32_CMD);
			}
			else if (dwCodeSize > sizeof(m_StubCode) - sizeof(OP32_CMD))
			{
				dwCodeSize = sizeof(m_StubCode) - sizeof(OP32_CMD);
			}

			// Get write access to the function code
			CWin32VirtualProtect vp(pOldFunc, dwCodeSize, PAGE_READWRITE);

			// Initialize stub code
			memcpy(m_StubCode, pOldFunc, dwCodeSize);

			// Make stub code - insert jump to the remainder of original function
			OP32_CMD* pCmd = (OP32_CMD*)(m_StubCode + dwCodeSize);
			pCmd->opcode = cmdJMPrel32;
			pCmd->operand = (DWORD)pOldFunc - (DWORD)m_StubCode - sizeof(OP32_CMD);

			// Patch original function body - insert jump to stub code
			pCmd = (OP32_CMD*)pOldFunc;
			pCmd->opcode = cmdJMPrel32;
			pCmd->operand = (DWORD)pNewFunc - (DWORD)pOldFunc - sizeof(OP32_CMD);
			// Fill remainder with NOPs
			memset(pCmd + 1, cmdNOP, dwCodeSize - sizeof(OP32_CMD));

			m_pOldFunc = pOldFunc;
			m_dwCodeSize = dwCodeSize;
		}
		catch (...)
		{
			return NULL;
		}

		return GetOldProc();
	}

	bool CWin32UserModePatcher::Restore()
	{
		try
		{
			// Get write access to the function code
			CWin32VirtualProtect vp(m_pOldFunc, m_dwCodeSize, PAGE_READWRITE);
			// Restore function code
			memcpy(m_pOldFunc, m_StubCode, m_dwCodeSize);
		}
		catch (...)
		{
			return false;
		}

		return true;
	}

	LPVOID CWin32UserModePatcher::GetOldProc() const
	{
		return (LPVOID)m_StubCode;
	}
}
