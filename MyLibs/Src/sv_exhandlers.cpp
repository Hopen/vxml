/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include <Windows.h>
#include "sv_exceptions.h"
#include "sv_exhandlers.h"
#include "sv_helpclasses.h"
#include "sv_strutils.h"
#include "sv_debug.h"

namespace SVExHandlers
{

/************************************************************************/
/* CSeTranslator class implementation                                   */
/************************************************************************/

CSeTranslator::CSeTranslator() : m_pFn(_set_se_translator((_se_translator_function)&SeTranslator))
{
	TlsMapper<CSeTranslator>::Register(this);
}

CSeTranslator::~CSeTranslator()
{
	_set_se_translator(m_pFn);
	TlsMapper<CSeTranslator>::Unregister();
}

void CSeTranslator::SeTranslator(unsigned int, PEXCEPTION_POINTERS pExcPtr)
{
	CSeTranslator* pThis = TlsMapper<CSeTranslator>::Get();
	if (pThis != NULL)
	{
		pThis->ExceptionHandler(pExcPtr);
	}
}

void CSeTranslator::ExceptionHandler(PEXCEPTION_POINTERS pExcPtr)
{
	throw SVExceptions::CSystemException(EXCEPTION_LOCATION, pExcPtr);
}

/************************************************************************/
/* CVectoredExceptionHandler class implementation                       */
/************************************************************************/

#if _WIN32_WINNT > 0x500
CVectoredExceptionHandler::CVectoredExceptionHandler() :
	m_pHandler(::AddVectoredExceptionHandler(1, VectoredHandler))
{
	TlsMapper<CVectoredExceptionHandler>::Register(this);
}

CVectoredExceptionHandler::~CVectoredExceptionHandler()
{
	::RemoveVectoredExceptionHandler(m_pHandler);
	TlsMapper<CVectoredExceptionHandler>::Unregister();
}
#endif

LONG WINAPI CVectoredExceptionHandler::VectoredHandler(PEXCEPTION_POINTERS pExcPtr)
{
	CVectoredExceptionHandler* pThis = TlsMapper<CVectoredExceptionHandler>::Get();
	if (pThis != NULL)
	{
		return pThis->ExceptionHandler(pExcPtr);
	}
	return EXCEPTION_CONTINUE_SEARCH;
}

LONG CVectoredExceptionHandler::ExceptionHandler(PEXCEPTION_POINTERS /*pExcPtr*/)
{
	return EXCEPTION_CONTINUE_SEARCH;
}

/************************************************************************/
/* CUnhandledExceptionHandler class implementation                      */
/************************************************************************/

CUnhandledExceptionHandler* CUnhandledExceptionHandler::m_pThis = NULL;

CUnhandledExceptionHandler::CUnhandledExceptionHandler(DWORD /*dwFlags*/)
{
	m_pPrevHandler = SetUnhandledExceptionFilter(UnhandledExceptionFilter);
	SetErrorMode(
		SEM_FAILCRITICALERRORS | 
		SEM_NOALIGNMENTFAULTEXCEPT |
		SEM_NOGPFAULTERRORBOX |
		SEM_NOOPENFILEERRORBOX);
	m_pThis = this;
}

CUnhandledExceptionHandler::~CUnhandledExceptionHandler()
{
	SetUnhandledExceptionFilter(m_pPrevHandler);
	m_pThis = NULL;
}

LONG WINAPI CUnhandledExceptionHandler::UnhandledExceptionFilter(PEXCEPTION_POINTERS pExcPtr)
{
	__try
	{
		if (m_pThis)
		{
			return m_pThis->ExceptionHandler(pExcPtr);
		}
	}
	__except (EXCEPTION_CONTINUE_EXECUTION)
	{
	}
	return EXCEPTION_CONTINUE_EXECUTION;
}

LONG CUnhandledExceptionHandler::ExceptionHandler(PEXCEPTION_POINTERS /*pExcPtr*/)
{
	return EXCEPTION_CONTINUE_EXECUTION;
}

/************************************************************************/
/* CCRTInvParamHandler class implementation                             */
/************************************************************************/

CCRTInvParamHandler::CCRTInvParamHandler() :
	m_pPrevHandler(_set_invalid_parameter_handler(&CCRTInvParamHandler::CRTInvalidParameterHandler))
{
	TlsMapper<CCRTInvParamHandler>::Register(this);
}

CCRTInvParamHandler::~CCRTInvParamHandler()
{
	_set_invalid_parameter_handler(m_pPrevHandler);
	TlsMapper<CCRTInvParamHandler>::Unregister();
}

void CCRTInvParamHandler::CRTInvalidParameterHandler(const wchar_t* expression,
													 const wchar_t* function, 
													 const wchar_t* file,
													 unsigned int line, 
													 uintptr_t pReserved)
{
	CCRTInvParamHandler* pThis = TlsMapper<CCRTInvParamHandler>::Get();
	if (pThis != NULL)
	{
		return pThis->InvalidParameterHandler(expression, function, file, line, pReserved);
	}
}

/************************************************************************/
/* CStdSeTranslator class implementation                                */
/************************************************************************/

void CStdSeTranslator::ExceptionHandler(PEXCEPTION_POINTERS pExcPtr)
{
	if (::IsDebuggerPresent())
	{
		__asm int 3;
	}

	if (pExcPtr->ExceptionRecord->ExceptionCode != EXCEPTION_MICROSOFT_CPP)
	{
//		if (pExcPtr->ExceptionRecord->ExceptionCode == EXCEPTION_STACK_OVERFLOW)
//		{
//			::ExitThread(STATUS_STACK_OVERFLOW);
//			throw SVExceptions::CErrorException(EXCEPTION_LOCATION, L"Stack overflow");
//		}
//		else
		{
			std::wstring sBuf = SVDebug::WriteFullExceptionInfo(
				pExcPtr, 
				CALLSTACK_FLAG_REGS | CALLSTACK_FLAG_VARS);
			throw SVExceptions::CErrorException(EXCEPTION_LOCATION, sBuf.c_str());
		}
	}
}

/************************************************************************/
/* CStdUnhandledExceptionHandler class implementation                   */
/************************************************************************/

LONG CStdUnhandledExceptionHandler::ExceptionHandler(PEXCEPTION_POINTERS pExcPtr)
{
	if (::IsDebuggerPresent())
	{
		__asm int 3;
	}

	try
	{
		std::wstring sBuf = SVDebug::DumpProcessException(
			pExcPtr, 
			CALLSTACK_FLAG_ALL);
		LogException(sBuf.c_str());
	}
	catch (...)
	{
	}

	TerminateProcess(GetCurrentProcess(), (UINT)-1);

	return EXCEPTION_EXECUTE_HANDLER;
}

/************************************************************************/
/* CStdCRTInvParamHandler class implementation                          */
/************************************************************************/

void CStdCRTInvParamHandler::InvalidParameterHandler(const wchar_t* expression,
													 const wchar_t* function, 
													 const wchar_t* file, 
													 unsigned int line, 
													 uintptr_t /*pReserved*/)
{
	throw SVExceptions::CErrorException(EXCEPTION_LOCATION,
		format_wstring(
			L"Invalid parameter detected in function %s. "
			L"File: %s, Line: %d, Expression: %s", 
			function, file, line, expression));
}

}
