/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include <windows.h>
#include "sv_exceptions.h"

namespace SVExceptions
{

// Helper function
LPCWSTR UnknownException(DWORD dwExcCode)
{
	__declspec(thread) static WCHAR wszBuf[32] = {0};
	wsprintfW(wszBuf, L"Unknown exception 0x%08X", dwExcCode);
	return wszBuf;
}

// Returns exception name in string
LPCWSTR GetExceptionName(DWORD dwExcCode)
{
	switch (dwExcCode)
	{
		case EXCEPTION_ACCESS_VIOLATION:			return L"Access Violation";
		case EXCEPTION_DATATYPE_MISALIGNMENT:		return L"Datatype Misalignment";
		case EXCEPTION_BREAKPOINT:					return L"Breakpoint";
		case EXCEPTION_SINGLE_STEP:					return L"Single Step";
		case EXCEPTION_ARRAY_BOUNDS_EXCEEDED:		return L"Array Bounds Exceeded";
		case EXCEPTION_FLT_DENORMAL_OPERAND:		return L"Float Denormal Operand";
		case EXCEPTION_FLT_DIVIDE_BY_ZERO:			return L"Float Divide by Zero";
		case EXCEPTION_FLT_INEXACT_RESULT:			return L"Float Inexact Result";
		case EXCEPTION_FLT_INVALID_OPERATION:		return L"Float Invalid Operation";
		case EXCEPTION_FLT_OVERFLOW:				return L"Float Overflow";
		case EXCEPTION_FLT_STACK_CHECK:				return L"Float Stack Check";
		case EXCEPTION_FLT_UNDERFLOW:				return L"Float Underflow";
		case EXCEPTION_INT_DIVIDE_BY_ZERO:			return L"Integer Divide by Zero";
		case EXCEPTION_INT_OVERFLOW:				return L"Integer Overflow";
		case EXCEPTION_PRIV_INSTRUCTION:			return L"Privileged Instruction";
		case EXCEPTION_IN_PAGE_ERROR:				return L"In Page Error";
		case EXCEPTION_ILLEGAL_INSTRUCTION:			return L"Illegal Instruction";
		case EXCEPTION_NONCONTINUABLE_EXCEPTION:	return L"Noncontinuable Exception";
		case EXCEPTION_STACK_OVERFLOW:				return L"Stack Overflow";
		case EXCEPTION_INVALID_DISPOSITION:			return L"Invalid Disposition";
		case EXCEPTION_GUARD_PAGE:					return L"Guard Page";
		case EXCEPTION_INVALID_HANDLE:				return L"Invalid Handle";

		case EXCEPTION_MICROSOFT_CPP:				return L"Microsoft C++ Exception";
		case EXCEPTION_BORLAND_CPP1:
		case EXCEPTION_BORLAND_CPP2:
		case EXCEPTION_BORLAND_CPP3:				return L"C++Builder Exception";
		case EXCEPTION_BORLAND_DELPHI:				return L"Delphi Exception";

		case EXCEPTION_NO_MEMORY:					return L"No Memory";
		case EXCEPTION_CONTROL_C:					return L"Control C";
		case EXCEPTION_CONTROL_BREAK:				return L"Control-Break";
		case EXCEPTION_DLL_NOT_FOUND:				return L"Dll Not Found";
		case EXCEPTION_DLL_INIT_FAILED:				return L"Dll Initialization failed";
		case EXCEPTION_MODULE_NOT_FOUND:			return L"Module Not Found";
		case EXCEPTION_PROC_NOT_FOUND:				return L"Procedure Not Found";
	};

	return NULL;
}

// Returns exception description in string
LPCWSTR GetExceptionDescr(DWORD dwExcCode)
{
	switch (dwExcCode)
	{
		case EXCEPTION_NO_MEMORY:					return L"The allocation attempt failed because of a lack of available memory or heap corruption.";
		case EXCEPTION_ACCESS_VIOLATION:			return L"The thread attempted to read from or write to a virtual address for which it does not have the appropriate access";
		case EXCEPTION_DATATYPE_MISALIGNMENT:		return L"The thread attempted to read or write data that is misaligned on hardware that does not provide alignment. For example, 16-bit values must be aligned on 2-byte boundaries, 32-bit values on 4-byte boundaries, and so on";
		case EXCEPTION_BREAKPOINT:					return L"A breakpoint was encountered";
		case EXCEPTION_SINGLE_STEP:					return L"A trace trap or other single-instruction mechanism signaled that one instruction has been executed";
		case EXCEPTION_ARRAY_BOUNDS_EXCEEDED:		return L"The thread attempted to access an array element that is out of bounds, and the underlying hardware supports bounds checking";
		case EXCEPTION_FLT_DENORMAL_OPERAND:		return L"One of the operands in a floating-point operation is denormal. A denormal value is one that is too small to represent as a standard floating-point value";
		case EXCEPTION_FLT_DIVIDE_BY_ZERO:			return L"The thread attempted to divide a floating-point value by a floating-point divisor of zero";
		case EXCEPTION_FLT_INEXACT_RESULT:			return L"The result of a floating-point operation cannot be represented exactly as a decimal fraction";
		case EXCEPTION_FLT_INVALID_OPERATION:		return L"This exception represents any floating-point exception not included in this list";
		case EXCEPTION_FLT_OVERFLOW:				return L"The exponent of a floating-point operation is greater than the magnitude allowed by the corresponding type";
		case EXCEPTION_FLT_STACK_CHECK:				return L"The stack overflowed or underflowed as the result of a floating-point operation";
		case EXCEPTION_FLT_UNDERFLOW:				return L"The exponent of a floating-point operation is less than the magnitude allowed by the corresponding type";
		case EXCEPTION_INT_DIVIDE_BY_ZERO:			return L"The thread attempted to divide an integer value by an integer divisor of zero";
		case EXCEPTION_INT_OVERFLOW:				return L"The result of an integer operation caused a carry out of the most significant bit of the result";
		case EXCEPTION_PRIV_INSTRUCTION:			return L"The thread attempted to execute an instruction whose operation is not allowed in the current machine mode";
		case EXCEPTION_IN_PAGE_ERROR:				return L"The thread tried to access a page that was not present, and the system was unable to load the page. For example, this exception might occur if a network connection is lost while running a program over the network";
		case EXCEPTION_ILLEGAL_INSTRUCTION:			return L"The thread tried to execute an invalid instruction";
		case EXCEPTION_NONCONTINUABLE_EXCEPTION:	return L"The thread attempted to continue execution after a noncontinuable exception occurred";
		case EXCEPTION_STACK_OVERFLOW:				return L"The thread used up its stack";
		case EXCEPTION_INVALID_DISPOSITION:			return L"An exception handler returned an invalid disposition to the exception dispatcher. Programmers using a high-level language such as C should never encounter this exception";
		case EXCEPTION_GUARD_PAGE:					return L"Guard Page Exception";
		case EXCEPTION_INVALID_HANDLE:				return L"Invalid Handle Exception";
	};

	return GetExceptionName(dwExcCode);
}

/************************************************************************/
/* Exception classes format methods                                     */
/************************************************************************/

std::wstring CException::Format() const
{
	wchar_t wszBuf[1024] = {0};
	swprintf(wszBuf, L"File: %S, Line: %d, Func: %S", m_pszFile, m_nLine, m_pszFunc);
	return std::wstring(wszBuf);
}

std::wstring CErrorException::Format() const
{
	return CException::Format() + L", Descr: " + std::wstring(m_pwszDescr);
}

std::wstring COSErrorException::Format() const
{
	wchar_t wszBuf[1024] = {0};
	LPWSTR lpMsgBuf = NULL;

	FormatMessageW(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		m_dwErrCode,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPWSTR)&lpMsgBuf,
		0,
		NULL);

	if (lpMsgBuf)
	{
		int nLen = (int)wcslen(lpMsgBuf);
		if (lpMsgBuf[nLen - 1] == L'\n')
		{
			lpMsgBuf[nLen - 1] = L'\0';
		}
		swprintf(wszBuf, L", Code: 0x%08X, Descr: \"%s\"", m_dwErrCode, lpMsgBuf);
		LocalFree(lpMsgBuf);
	}
	else
	{
		swprintf(wszBuf, L", Code: 0x%08X", m_dwErrCode);
	}

	return CException::Format() + std::wstring(wszBuf);
}

std::wstring CSystemException::Format() const
{
	wchar_t wszBuf[1024] = {0};
	swprintf(wszBuf, L", Code: 0x%08X, Addr: %08p, Name: \"%s\"", 
		m_pExcPtr->ExceptionRecord->ExceptionCode,
		m_pExcPtr->ExceptionRecord->ExceptionAddress,
		SVExceptions::GetExceptionName(m_pExcPtr->ExceptionRecord->ExceptionCode));
	return CException::Format() + std::wstring(wszBuf);
}

}
