/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include <Windows.h>
#include <stdio.h>
#include "sv_heapcheck.h"
#include "sv_callstack.h"
#include "sv_debug.h"
#include "sv_strutils.h"

#define PREFIX_SIZE			(sizeof(ALLOC_INFO) + m_dwCallStackDeep * sizeof(LPVOID))
#define HEAP_SIGNATURE		0x8B436F087CB0C6E2
#define ISREENTRANCE		((int)::TlsGetValue(CHeapCheckerSingleton::Instance().m_dwTLSIndex) > 0)
#define BUF_SIZE			16384

namespace SVHeapCheck
{

/************************************************************************/
/* Heap stubs                                                           */
/************************************************************************/

bool CheckSignature(LPCVOID lpMem)
{
	DWORD dwPrefixSize = sizeof(CHeapChecker::ALLOC_INFO) + CHeapCheckerSingleton::Instance().m_dwCallStackDeep * sizeof(LPVOID);
	const ULONGLONG* lpSig = (const ULONGLONG*)((const BYTE*)lpMem - dwPrefixSize);
	if (IsBadReadPtr(lpSig, sizeof(ULONGLONG)))
	{
		return false;
	}
	return *lpSig == HEAP_SIGNATURE;
}

LPVOID __stdcall HeapAlloc_Stub(HANDLE hHeap,
								DWORD dwFlags,
								SIZE_T dwBytes)
{
	if (ISREENTRANCE)
	{
		return CHeapCheckerSingleton::Instance().Orig_HeapAlloc(hHeap, dwFlags, dwBytes);
	}
	return CHeapCheckerSingleton::Instance().HeapAlloc(hHeap, dwFlags, dwBytes);
}

LPVOID __stdcall HeapReAlloc_Stub(HANDLE hHeap, 
								  DWORD dwFlags, 
								  LPVOID lpMem, 
								  DWORD dwBytes)
{
	if (!CheckSignature(lpMem) || ISREENTRANCE)
	{
		return CHeapCheckerSingleton::Instance().Orig_HeapReAlloc(hHeap, dwFlags, lpMem, dwBytes);
	}
	return CHeapCheckerSingleton::Instance().HeapReAlloc(hHeap, dwFlags, lpMem, dwBytes);
}

BOOL __stdcall HeapFree_Stub(HANDLE hHeap,
							 DWORD dwFlags,
							 LPVOID lpMem)
{
	if (!CheckSignature(lpMem) || ISREENTRANCE)
	{
		return CHeapCheckerSingleton::Instance().Orig_HeapFree(hHeap, dwFlags, lpMem);
	}
	return CHeapCheckerSingleton::Instance().HeapFree(hHeap, dwFlags, lpMem);
}

SIZE_T __stdcall HeapSize_Stub(HANDLE hHeap,
							   DWORD dwFlags, 
							   LPCVOID lpMem)
{
	if (!CheckSignature(lpMem) || ISREENTRANCE)
	{
		return CHeapCheckerSingleton::Instance().Orig_HeapSize(hHeap, dwFlags, lpMem);
	}
	return CHeapCheckerSingleton::Instance().HeapSize(hHeap, dwFlags, lpMem);
}

BOOL __stdcall HeapValidate_Stub(HANDLE hHeap, 
								 DWORD dwFlags, 
								 LPCVOID lpMem)
{
	if (!CheckSignature(lpMem) || ISREENTRANCE)
	{
		return CHeapCheckerSingleton::Instance().Orig_HeapValidate(hHeap, dwFlags, lpMem);
	}
	return CHeapCheckerSingleton::Instance().HeapValidate(hHeap, dwFlags, lpMem);
}

/************************************************************************/
/* CHeapChecker class implementation                                    */
/************************************************************************/

struct PATCH_INFO
{
	LPCSTR	pszProcName;
	LPVOID	pStubProc;
	DWORD	dwPatchSize;
};

// THE PATCH TABLE
// The last constant valid only in this version of Windows (XP SP2)
PATCH_INFO patches[] = 
{
	{"HeapAlloc",	HeapAlloc_Stub,		10},
	{"HeapReAlloc",	HeapReAlloc_Stub,	10},
	{"HeapFree",	HeapFree_Stub,		10},
	{"HeapSize",	HeapSize_Stub,		8},
	{"HeapValidate",HeapValidate_Stub,	8}
};

CHeapChecker::CHeapChecker() :
	m_bLogHeapCalls(false),
	m_dwHeapFlags(DEF_HEAP_FLAGS), 
	m_dwCallStackDeep(DEF_CALLSTACK_DEEP), 
	m_dwCheckSize(DEF_CHECK_SIZE),
	m_hFile(INVALID_HANDLE_VALUE),
	m_pOrigCode(NULL),
	m_dwTLSIndex(::TlsAlloc())
{
	::InitializeCriticalSection(&m_cs);
}

CHeapChecker::~CHeapChecker()
{
/*	UninstallStubProcs();
	if (m_hFile != INVALID_HANDLE_VALUE)
	{
		::CloseHandle(m_hFile);
	}
	::VirtualFree(m_pOrigCode, 0, MEM_RELEASE);
	::DeleteCriticalSection(&m_cs);
	::TlsFree(m_dwTLSIndex);*/
}

void CHeapChecker::Init(LPCWSTR pwszLogFile,
						bool bLogHeapCalls, 
						DWORD dwHeapFlags, 
						DWORD dwCallStackDeep, 
						DWORD dwCheckSize)
{
	m_bLogHeapCalls = bLogHeapCalls;
	m_dwHeapFlags = dwHeapFlags;
	m_dwHeapFlags = dwCallStackDeep;
	m_dwCheckSize = dwCheckSize;

	// Create log file
	m_hFile = ::CreateFileW(
		pwszLogFile,
		GENERIC_WRITE,
		FILE_SHARE_WRITE | FILE_SHARE_READ | FILE_SHARE_DELETE,
		0,
		OPEN_ALWAYS,
		0,
		NULL);

	InstallStubProcs();
}

void CHeapChecker::InstallStubProcs()
{
	HMODULE hKernel32 = ::GetModuleHandleW(L"KERNEL32.DLL");
	DWORD dwOrigCodeSize = 256;
	m_pOrigCode = (LPBYTE)::VirtualAlloc(NULL, dwOrigCodeSize, MEM_COMMIT, PAGE_EXECUTE_READWRITE);

	// Fill code block with INT3
	memset(m_pOrigCode, 0xCC, dwOrigCodeSize);

	LPVOID pOrigProcs[] = {
		&Orig_HeapAlloc,	
		&Orig_HeapReAlloc,	
		&Orig_HeapFree,	
		&Orig_HeapSize,
		&Orig_HeapValidate
	};

	LPBYTE p = m_pOrigCode;
	PATCH_INFO* pi = patches;
	for (int i = 0; i < sizeof(patches) / sizeof(patches[0]); i++, pi++)
	{
		FARPROC pOrigPtr = ::GetProcAddress(hKernel32, pi->pszProcName);
		InstallCodeStub(pOrigPtr, patches[i].pStubProc, p, pi->dwPatchSize);
		*((LPVOID*)pOrigProcs[i]) = p;
		p += pi->dwPatchSize + sizeof(ORIG_THUNK);
		// 16-align
		p += 16 - ((p - m_pOrigCode) % 16);
	}

	::VirtualProtect(m_pOrigCode, dwOrigCodeSize, PAGE_EXECUTE_READ, NULL);
}

void CHeapChecker::UninstallStubProcs()
{
	HMODULE hKernel32 = ::GetModuleHandleW(L"KERNEL32.DLL");

	// Uninstall patches
	LPBYTE p = m_pOrigCode;
	PATCH_INFO* pi = patches;
	for (int i = 0; i < sizeof(patches) / sizeof(patches[0]); i++, pi++)
	{
		FARPROC pOrigPtr = ::GetProcAddress(hKernel32, pi->pszProcName);
		DWORD dwProtect = 0;
		::VirtualProtect(pOrigPtr, pi->dwPatchSize, PAGE_READWRITE, &dwProtect);
		memcpy(pOrigPtr, p, pi->dwPatchSize);
		::VirtualProtect(pOrigPtr, pi->dwPatchSize, dwProtect, &dwProtect);
		p += pi->dwPatchSize + sizeof(ORIG_THUNK);
		// 16-align
		p += 16 - ((p - m_pOrigCode) % 16);
	}
}

void CHeapChecker::InstallCodeStub(LPVOID pProcPtr, 
								   LPVOID pStubPtr, 
								   LPVOID pOrigCode,
								   DWORD dwOrigSize)
{
	PATCH_CODE*	pPatchCode	= (PATCH_CODE*)pProcPtr;
	ORIG_THUNK*	pOrigThunk	= (ORIG_THUNK*)((LPBYTE)pOrigCode + dwOrigSize);
	DWORD		dwProtect	= 0;

	// Copy original code to buffer
	memcpy(pOrigCode, pProcPtr, dwOrigSize);

	// Add jump to the rest of the procedure
	pOrigThunk->cmdJmp		= 0xE9; // JMP rel32
	pOrigThunk->procOffs	= (LPBYTE)pProcPtr - &pOrigThunk->cmdJmp + dwOrigSize - 5;

	::VirtualProtect(pProcPtr, dwOrigSize, PAGE_EXECUTE_READWRITE, &dwProtect);

	// Patch original code
	pPatchCode->cmdPush		= 0x68; // PUSH imm32
	pPatchCode->procAddr	= pStubPtr;
	pPatchCode->cmdRet		= 0xC3; // RET
	memset(pPatchCode + 1, 0x90, dwOrigSize - sizeof(PATCH_CODE)); // Fill with NOPs

	::VirtualProtect(pProcPtr, dwOrigSize, dwProtect, &dwProtect);
}

void CHeapChecker::Log(LPCSTR pszFunc, LPCSTR pszFmt, ...)
{
	SYSTEMTIME st = {0};
	CHAR szBuf[BUF_SIZE] = {0};
	DWORD dwWritten = 0;

	::GetLocalTime(&st);

	LPCSTR p = pszFunc + strlen(pszFunc);
	while (p > pszFunc && *p != ':') p--;
	if (p > pszFunc) p++;

	int len = _snprintf(szBuf, BUF_SIZE - 1, "%02d:%02d:%02d.%03d %04X %04X [%-16s] ",
		(int)st.wHour, (int)st.wMinute, (int)st.wSecond, (int)st.wMilliseconds, 
		::GetCurrentProcessId(), ::GetCurrentThreadId(), p);

	va_list args;
	va_start(args, pszFmt);
	len += _vsnprintf(szBuf + len, BUF_SIZE - 1 - len, pszFmt, args);
	strcat(szBuf, "\r\n");
	va_end(args);

	::EnterCriticalSection(&m_cs);
	::SetFilePointer(m_hFile, 0, 0, FILE_END);
	::WriteFile(m_hFile, szBuf, len + 2, &dwWritten, NULL);
	::LeaveCriticalSection(&m_cs);
}

void CHeapChecker::HeapAllocCommon(DWORD dwFlags, LPVOID lpMem, DWORD dwBytes)
{
	if (lpMem == NULL)
	{
		return;
	}
		
	// Zero info struct and call stack
	ZeroMemory(lpMem, PREFIX_SIZE);

	// Fill info struct
	ALLOC_INFO* pAllocInfo		= (ALLOC_INFO*)lpMem;
	pAllocInfo->ullSignature	= HEAP_SIGNATURE;
	pAllocInfo->dwTID			= ::GetCurrentThreadId();
	pAllocInfo->dwAllocSize		= dwBytes;
	pAllocInfo->dwAllocFlags	= dwFlags;
	::GetSystemTimeAsFileTime(&pAllocInfo->ftTimeStamp);

	// Get call stack
	LPDWORD pEBP = NULL;
	LPDWORD pdwCallStack = (LPDWORD)((LPBYTE)lpMem + sizeof(ALLOC_INFO));

	// Initial EBP
	__asm mov pEBP, ebp;

	for (DWORD i = 0; i < m_dwCallStackDeep; i++)
	{
		if (::IsBadReadPtr(pEBP + 1, 4) ||
			::IsBadReadPtr((LPVOID)*(pEBP + 1), 4))
		{
			break;
		}

		*pdwCallStack++ = *(pEBP + 1);

		if (::IsBadReadPtr(pEBP, 4))
		{	
			break;
		}

		pEBP = (LPDWORD)*pEBP;
	}

	// Fill tail with check bytes
	memset((LPBYTE)lpMem + PREFIX_SIZE + dwBytes, HEAP_CHECK_BYTE, m_dwCheckSize);
}

LPVOID CHeapChecker::HeapAlloc(HANDLE hHeap, 
							   DWORD dwFlags, 
							   SIZE_T dwBytes)
{
	CTlsValue tls(m_dwTLSIndex);

	LPVOID p = ::HeapAlloc(
		hHeap, dwFlags, 
		PREFIX_SIZE + dwBytes + m_dwCheckSize);

	HeapAllocCommon(dwFlags, p, (DWORD)dwBytes);

	if (m_bLogHeapCalls)
	{
		Log(__FUNCTION__, "hHeap=0x%p, dwFlags=%d, dwBytes=%d, RES=0x%p (%d)", hHeap, dwFlags, dwBytes, p, ::GetLastError());
	}

	return (LPBYTE)p + PREFIX_SIZE;
}

LPVOID CHeapChecker::HeapReAlloc(HANDLE hHeap, 
								 DWORD dwFlags, 
								 LPVOID lpMem, 
								 SIZE_T dwBytes)
{
	CTlsValue tls(m_dwTLSIndex);
	
	LPVOID p = ::HeapReAlloc(
		hHeap, dwFlags, 
		(LPBYTE)lpMem - PREFIX_SIZE, PREFIX_SIZE + dwBytes + m_dwCheckSize);
	
	HeapAllocCommon(dwFlags, p, (DWORD)dwBytes);

	if (m_bLogHeapCalls)
	{
		Log(__FUNCTION__, "hHeap=0x%p, dwFlags=%d, lpMem=0x%p, dwBytes=%d, RES=0x%p (%d)", hHeap, dwFlags, lpMem, dwBytes, p, ::GetLastError());
	}

	return (LPBYTE)p + PREFIX_SIZE;
}

BOOL CHeapChecker::HeapFree(HANDLE hHeap, 
							DWORD dwFlags, 
							LPVOID lpMem)
{
	CTlsValue tls(m_dwTLSIndex);

	// Check heap
	lpMem = (LPBYTE)lpMem - PREFIX_SIZE;
	DWORD dwSize = (DWORD)::HeapSize(hHeap, 0, lpMem);
	LPVOID pCheckArea = (LPBYTE)lpMem + dwSize - m_dwCheckSize;
	DWORD dwCheckSize = m_dwCheckSize;
	BOOL bValid = FALSE;

	__asm
	{
		pushfd
		pusha
		mov		edi, pCheckArea
		mov		al, HEAP_CHECK_BYTE
		mov		ecx, dwCheckSize
		cld
		repz	scasb
		mov		eax, 0
		mov		ebx, 1
		cmove	eax, ebx
		mov		bValid, eax
		popa
		popfd
	}

	if (!bValid)
	{
		// Get control structures
		const ALLOC_INFO* pAllocInfo = (const ALLOC_INFO*)lpMem;
		const DWORD* pdwCallStack = (const DWORD*)((LPBYTE)lpMem + sizeof(ALLOC_INFO));
		
		// Get source call stack
		std::wstring sSrcCallStack;
		HANDLE hProcess = ::GetCurrentProcess();
		for (DWORD i = 0; i < m_dwCallStackDeep; i++, pdwCallStack++)
		{
			if (*pdwCallStack == 0)
			{
				break;
			}
			
			// Skip first 2 entries (HeapAlloc, HeapAlloc_Stub)
			if (i < 2)
			{
				continue;
			}

			// Get next call stack address info
			SVCallStack::LocationInfo Location;
			Location.GetAddressInfo(hProcess, (LPVOID)*pdwCallStack);

			// Format call stack info
			std::wstring s = format_wstring(
				L"# 0x%p, %s+%d, %s, %s\r\n",
				*pdwCallStack, 
				Location.Function.sName.c_str(), Location.Function.dwOffs,
				Location.Line.sFile.empty() ? L"<no source info>" : 
				format_wstring(
					L"%s [%s+0x%p] %d+%d", 
					Location.Line.sFile.c_str(), 
					Location.Module.sSection.c_str(), Location.Module.dwOffs,
					Location.Line.nLineNum, 
					Location.Line.dwOffs).c_str(), 
				Location.Module.sName.c_str()
			);

			sSrcCallStack += s;
		}

		CONTEXT ctx = {0};
		// Get this call stack - we need to skip 2 upper stack frames (for HeapAlloc and HeapAlloc_Stub)
		SVDebug::InstantContext(&ctx, 2);
		SVCallStack::CStackWalker stk;
		stk.Init(&ctx, ::GetCurrentThreadId(), ::GetCurrentProcessId());
		stk.CallStackSnapshot(true);
		std::wstring sDstCallStack = stk.GetCallStackInfo();

		// Format dump
		std::wstring sDump = SVDebug::WriteMemoryDump<BYTE>(pCheckArea, m_dwCheckSize, L"%02X", 16, true, true);

		// Format heap flags
		char szHeapFlags[256] = {0};
		DWORD dwHeapFlags = pAllocInfo->dwAllocFlags;
		if (dwHeapFlags & HEAP_GENERATE_EXCEPTIONS)
		{
			strcat(szHeapFlags, "HEAP_GENERATE_EXCEPTIONS ");
		}
		if (dwHeapFlags & HEAP_NO_SERIALIZE)
		{
			strcat(szHeapFlags, "HEAP_NO_SERIALIZE ");
		}
		if (dwHeapFlags & HEAP_ZERO_MEMORY)
		{
			strcat(szHeapFlags, "HEAP_ZERO_MEMORY");
		}

		// Get allocation time
		FILETIME ft = {0};
		SYSTEMTIME st = {0};
		::FileTimeToLocalFileTime(&pAllocInfo->ftTimeStamp, &ft);
		::FileTimeToSystemTime(&ft, &st);

		// Log all collected info
		Log(__FUNCTION__, 
			"HEAP CORRUPTION DETECTED\r\n"
			"\tSignature:        %16I64x\r\n"
			"\tBlock address:    0x%p (0x%p with heap check)\r\n"
			"\tAllocated thread: %04X (%d)\r\n"
			"\tAllocation size:  %d (%d with heap check)\r\n"
			"\tAllocation flags: 0x%08X (%s)\r\n" 
			"\tAllocation time:  %02d:%02d:%02d.%03d %02d.%02d.%04d\r\n"
			"\r\n<<<< Source call stack >>>>\r\n"
			"%S\r\n"
			"<<<< Destination call stack >>>>\r\n"
			"%S\r\n"
			"<<<< Corrupted bytes >>>>\r\n"
			"%S",
			pAllocInfo->ullSignature,
			(LPBYTE)lpMem + PREFIX_SIZE, lpMem,
			pAllocInfo->dwTID, pAllocInfo->dwTID,
			pAllocInfo->dwAllocSize, pAllocInfo->dwAllocSize + PREFIX_SIZE + m_dwCheckSize,
			dwHeapFlags, szHeapFlags,
			st.wHour, st.wMinute, st.wSecond, st.wMilliseconds, st.wDay, st.wMonth, st.wYear,
			sSrcCallStack.c_str(),
			sDstCallStack.c_str(),
			sDump.c_str());

		// Break into debugger if any
		if (::IsDebuggerPresent() != 0)
		{
			__asm int 3;
		}
	}

	BOOL bRes = ::HeapFree(hHeap, dwFlags, lpMem);
	
	if (m_bLogHeapCalls)
	{
		Log(__FUNCTION__, "hHeap=0x%p, dwFlags=%d, lpMem=0x%p, RES=%d (%d)", hHeap, dwFlags, lpMem, bRes, ::GetLastError());
	}
	
	return bRes;
}

SIZE_T CHeapChecker::HeapSize(HANDLE hHeap, 
							  DWORD dwFlags, 
							  LPCVOID lpMem)
{
	CTlsValue tls(m_dwTLSIndex);
	
	SIZE_T size = ::HeapSize(hHeap, dwFlags, (LPBYTE)lpMem - PREFIX_SIZE);
	
	if (m_bLogHeapCalls)
	{
		Log(__FUNCTION__, "hHeap=0x%p, dwFlags=%d, lpMem=0x%p, RES=%d (%d)", hHeap, dwFlags, lpMem, size, ::GetLastError());
	}
	
	return size - PREFIX_SIZE - m_dwCheckSize;
}

BOOL CHeapChecker::HeapValidate(HANDLE hHeap, 
								DWORD dwFlags, 
								LPCVOID lpMem)
{
	CTlsValue tls(m_dwTLSIndex);
	
	BOOL bRes = ::HeapValidate(hHeap, dwFlags, (LPBYTE)lpMem - PREFIX_SIZE);
	
	if (m_bLogHeapCalls)
	{
		Log(__FUNCTION__, "hHeap=0x%p, dwFlags=%d, lpMem=0x%p, RES=%d (%d)", hHeap, dwFlags, lpMem, bRes, ::GetLastError());
	}
	
	return bRes;
}

}
