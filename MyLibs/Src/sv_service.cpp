/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include <windows.h>
#include <memory>
#include <strstream>
#include "sv_service.h"
#include "sv_exceptions.h"
#include "sv_utils.h"

namespace SVService
{

CSvcHandle SCMConnect(DWORD dwDesiredAccess, 
					  LPCWSTR pwszMachineName, 
					  LPCWSTR pwszUserName,
					  LPCWSTR pwszPassword)
{
    if (pwszMachineName != NULL && pwszUserName != NULL)
    {
		NETRESOURCEW NetRes;
		wstring sRemoteName(wstring(pwszMachineName) + L"\\IPC$");

		NetRes.dwType = RESOURCETYPE_ANY;
		NetRes.lpLocalName = NULL;
		NetRes.lpRemoteName = (LPWSTR)sRemoteName.c_str();
		NetRes.lpProvider = NULL;

		// ��������� ���������� ���������� � �������� IPC$ ��
		// ��������� ������
		if (WNetAddConnection2W(
			&NetRes,
			pwszPassword,
			pwszUserName, 0) != ERROR_SUCCESS)
		{
			THROW_LASTERR;
		}
    }

	CSvcHandle hSCM(::OpenSCManagerW(
		pwszMachineName,
		SERVICES_ACTIVE_DATABASEW,
		dwDesiredAccess));

	if (!hSCM)
	{
		THROW_LASTERR;
	}
	
	return hSCM;
}

/************************************************************************/
/* ���������� ������ CServiceManager                                    */
/************************************************************************/

wstring CServiceManager::GetSvcName(const wstring& sKey, 
									BOOL (WINAPI *GetNameFunc)(SC_HANDLE, LPCWSTR, LPWSTR, LPDWORD)) const
{
	DWORD cchBuffer = 0;
	CSvcHandle hSCM(OpenInt(READ_CONTROL));

	GetNameFunc(
		hSCM,
		sKey.c_str(),
		NULL,
		&cchBuffer);

	if (GetLastError() != ERROR_INSUFFICIENT_BUFFER)
	{
		THROW_LASTERR;
	}

	wstring sName(int(cchBuffer), L'\0');
	cchBuffer++;

	if (!GetNameFunc(
		hSCM,
		sKey.c_str(),
		(LPWSTR)sName.c_str(),
		&cchBuffer))
	{
		THROW_LASTERR;
	}

	return sName;
}

wstring CServiceManager::GetSvcDescription(const wstring& sSvcName) const
{
	CRegKeyHandle hKey;
	wstring sKeyName(L"System\\CurrentControlSet\\Services\\" + sSvcName);

	if (!::RegOpenKeyExW(
		HKEY_LOCAL_MACHINE, 
		sKeyName.c_str(), 
		0, 
		KEY_READ, 
		&hKey))
	{
		THROW_LASTERR;
	}

	DWORD dwType = REG_SZ;
	DWORD cbData = 0;

	if (::RegQueryValueExW(
		hKey, 
		L"Description", 
		NULL, 
		&dwType, 
		NULL, 
		&cbData) != ERROR_SUCCESS)
	{
		return L"";
	}

	wstring sDescr(cbData / sizeof(wchar_t), L'\0');

	if (::RegQueryValueExW(
		hKey, 
		L"Description", 
		NULL, 
		&dwType, 
		(LPBYTE)sDescr.c_str(), 
		&cbData) != ERROR_SUCCESS)
	{
		return L"";
	}

	return sDescr;
}

void CServiceManager::EnumServices(vector<wstring>& services,
								   DWORD dwServiceState, 
								   DWORD dwServiceType)
{
	CSvcHandle hSCM(OpenInt(SC_MANAGER_ENUMERATE_SERVICE));

	DWORD cbBufSize = 0;
	DWORD dwServicesReturned = 0;
	DWORD dwResumeHandle = 0;
	
	::EnumServicesStatusW(
		hSCM,
		dwServiceType,
		dwServiceState,
		NULL,
		0,
		&cbBufSize,
		&dwServicesReturned,
		&dwResumeHandle);

	if (GetLastError() != ERROR_MORE_DATA)
	{
		THROW_LASTERR;
	}
	
	auto_ptr<ENUM_SERVICE_STATUSW> lpServices((LPENUM_SERVICE_STATUSW)(new BYTE[cbBufSize]));
	
	if (!::EnumServicesStatusW(
		hSCM,
		dwServiceType,
		dwServiceState,
		lpServices.get(),
		cbBufSize,
		&cbBufSize,
		&dwServicesReturned,
		&dwResumeHandle))
	{
		THROW_LASTERR;
	}

	services.clear();
	LPENUM_SERVICE_STATUSW pSvc = lpServices.get();
	for (DWORD i = 0; i < dwServicesReturned; i++, pSvc++)
	{
		services.push_back(pSvc->lpServiceName);
	}
};

CSvcHandle CServiceManager::ServiceInstall(const wstring& sServiceName,
										   const wstring& sDisplayName,
										   const wstring& sBinaryPathName,
										   DWORD dwDesiredAccess,
										   DWORD dwServiceType,
										   DWORD dwStartType,
										   DWORD dwErrorControl,
										   const wstring& sLoadOrderGroup,
										   vector<wstring> dependencies,
										   const wstring& sServiceStartName,
										   const wstring& sPassword)
{
	CSvcHandle hSCM(OpenInt(SC_MANAGER_CREATE_SERVICE));

	ostrstream dependStr;

	vector<wstring>::iterator it = dependencies.begin();
	for (; it != dependencies.end(); ++it)
	{
		LPCWSTR pszDep = (*it).c_str();
		dependStr.write((const char*)pszDep, streamsize((wcslen(pszDep) + 1) * sizeof(wchar_t)));
	}

	char null[] = {0, 0};
	dependStr.write(null, sizeof(null));

	CSvcHandle hService = ::CreateServiceW(
		hSCM,
		sServiceName.c_str(),
		sDisplayName.c_str(),
		dwDesiredAccess,
		dwServiceType,
		dwStartType,
		dwErrorControl,
		sBinaryPathName.c_str(),
		sLoadOrderGroup.empty() ? NULL : sLoadOrderGroup.c_str(),
		NULL,
		dependencies.empty() ? NULL : (LPCWSTR)dependStr.rdbuf()->str(),
		sServiceStartName.empty() ? NULL : sServiceStartName.c_str(),
		sPassword.empty() ? NULL : sPassword.c_str());

	if (!hService)
	{
		THROW_LASTERR;
	}

	return hService;
}

void CServiceManager::ServiceUninstall(const wstring& sSvcName)
{
	CSvcHandle hService(ServiceOpen(sSvcName, DELETE));
	if (!::DeleteService(hService))
	{
		THROW_LASTERR;
	}
}

CSvcHandle CServiceManager::ServiceOpen(const wstring& sSvcName, 
										DWORD dwDesiredAccess)
{
	CSvcHandle hSCM(OpenInt(0));
	CSvcHandle hService(::OpenServiceW(hSCM, sSvcName.c_str(), dwDesiredAccess));
	if (!hService)
	{
		THROW_LASTERR;
	}
	return hService;
}

/************************************************************************/
/* ���������� ������ CServiceControl                                    */
/************************************************************************/

CServiceControl::Config CServiceControl::GetConfig() const
{
	DWORD cbBytesNeeded = 0;

	::QueryServiceConfigW(m_hService, NULL, 0, &cbBytesNeeded);
	
	if (GetLastError() != ERROR_INSUFFICIENT_BUFFER)
	{
		THROW_LASTERR;
	}
	
	auto_ptr<BYTE> pBuf(new BYTE[cbBytesNeeded]);
	
	if (!::QueryServiceConfigW(
		m_hService, 
		(LPQUERY_SERVICE_CONFIGW)pBuf.get(), 
		cbBytesNeeded, 
		&cbBytesNeeded))
	{
		THROW_LASTERR;
	}

	Config cfg;
	LPQUERY_SERVICE_CONFIGW pConfig = (LPQUERY_SERVICE_CONFIGW)pBuf.get();

	cfg.dwServiceType = pConfig->dwServiceType;
	cfg.dwStartType = pConfig->dwStartType;
	cfg.dwErrorControl = pConfig->dwErrorControl;
	cfg.sDisplayName = pConfig->lpDisplayName;
	cfg.sBinaryPathName = pConfig->lpBinaryPathName;
	cfg.sLoadOrderGroup = pConfig->lpLoadOrderGroup;
	cfg.sServiceStartName = pConfig->lpServiceStartName;

	LPCWSTR pwszDep = pConfig->lpDependencies;
	while (*pwszDep)
	{
		cfg.dependencies.push_back(pwszDep);
		pwszDep += wcslen(pwszDep) + 1;
	}

	// Get SERVICE_CONFIG_DESCRIPTION
	cfg.sDescription = ((LPSERVICE_DESCRIPTIONW)GetConfig2(SERVICE_CONFIG_DESCRIPTION).get())->lpDescription;
	// Get SERVICE_CONFIG_FAILURE_ACTIONS
	pBuf = GetConfig2(SERVICE_CONFIG_FAILURE_ACTIONS);
	LPSERVICE_FAILURE_ACTIONSW pActions = (LPSERVICE_FAILURE_ACTIONSW)pBuf.get();

	cfg.FailureActions.dwResetPeriod = pActions->dwResetPeriod;
	if (pActions->lpRebootMsg)
		cfg.FailureActions.sRebootMsg = pActions->lpRebootMsg;
	if (pActions->lpCommand)
		cfg.FailureActions.sCommand = pActions->lpCommand;

	for (DWORD i = 0; i < pActions->cActions; i++)
	{
		cfg.FailureActions.Actions.push_back(pActions->lpsaActions[i]);
	}

	return cfg;
}

auto_ptr<BYTE> CServiceControl::GetConfig2(DWORD dwInfoLevel) const
{
	DWORD cbBytesNeeded = 0;

	::QueryServiceConfig2W(
		m_hService,
		dwInfoLevel,
		NULL, 0,
		&cbBytesNeeded);

	if (GetLastError() != ERROR_INSUFFICIENT_BUFFER)
	{
		THROW_LASTERR;
	}
	
	auto_ptr<BYTE> pBuf(new BYTE[cbBytesNeeded]);
	
	if (!::QueryServiceConfig2W(
		m_hService,
		dwInfoLevel,
		pBuf.get(), 
		cbBytesNeeded,
		&cbBytesNeeded))
	{
		THROW_LASTERR;
	}

	return pBuf;
}

bool CServiceControl::SetConfig(const Config& config, 
								int nChangeType, 
								LPCWSTR pwszPassword)
{
	if (nChangeType & ctStandard)
	{
		ostrstream stream;

		if (nChangeType & ctDependencies)
		{
			for (int i = 0; i < (int)config.dependencies.size(); i++)
			{
				LPCWSTR pwszDep = config.dependencies[i].c_str();
				stream.write((const char*)pwszDep, std::streamsize((wcslen(pwszDep) + 1) * sizeof(wchar_t)));
			}
			WCHAR cNull = L'\0';
			stream.write((const char*)&cNull, sizeof(wchar_t));
		}

		if (!::ChangeServiceConfigW(
			m_hService,
			(nChangeType & ctServiceType) ? config.dwServiceType : SERVICE_NO_CHANGE,
			(nChangeType & ctStartType) ? config.dwStartType : SERVICE_NO_CHANGE,
			(nChangeType & ctErrorControl) ? config.dwErrorControl : SERVICE_NO_CHANGE,
			(nChangeType & ctBinaryPathName) ? config.sBinaryPathName.c_str() : NULL,
			(nChangeType & ctLoadOrderGroup) ? config.sLoadOrderGroup.c_str() : NULL,
			NULL,
			(nChangeType & ctDependencies) ? (LPCWSTR)stream.rdbuf()->str() : NULL,
			(nChangeType & ctServiceStartName) ? config.sServiceStartName.c_str() : NULL,
			pwszPassword,
			(nChangeType & ctDisplayName) ? config.sDisplayName.c_str() : NULL))
		{
			return false;
		}
	}

	if (nChangeType & ctDescription)
	{
		SERVICE_DESCRIPTIONW descr;
		
		descr.lpDescription = (LPWSTR)config.sDescription.c_str();
		
		if (!::ChangeServiceConfig2W(
			m_hService, 
			SERVICE_CONFIG_DESCRIPTION, 
			&descr))
		{
			return false;
		}
	}

	if (nChangeType & ctFailureActions)
	{
		SERVICE_FAILURE_ACTIONSW actions;
		
		actions.dwResetPeriod = config.FailureActions.dwResetPeriod;
		actions.lpRebootMsg = (LPWSTR)config.FailureActions.sRebootMsg.c_str();
		actions.lpCommand = (LPWSTR)config.FailureActions.sCommand.c_str();
		actions.cActions = (DWORD)config.FailureActions.Actions.size();
		actions.lpsaActions = (SC_ACTION*)&config.FailureActions.Actions[0];
		
		if (!::ChangeServiceConfig2W(
			m_hService, 
			SERVICE_CONFIG_FAILURE_ACTIONS, 
			&actions))
		{
			return false;
		}
	}
	
	return true;
}

SERVICE_STATUS_PROCESS CServiceControl::GetStatus() const
{
	SERVICE_STATUS_PROCESS status;
	DWORD cbBufSize = sizeof(status);
	
	if (!::QueryServiceStatusEx(
		m_hService, 
		SC_STATUS_PROCESS_INFO, 
		(LPBYTE)&status, 
		cbBufSize, 
		&cbBufSize))
	{
		THROW_LASTERR;
	}
	
	return status;
}

bool CServiceControl::Start(vector<wstring>& args)
{
	if (!::StartServiceW(
		m_hService,
		(DWORD)args.size(),
		(args.size() > 0) ? (LPCWSTR*)&args[0] : NULL))
	{
		if (::GetLastError() != ERROR_SERVICE_ALREADY_RUNNING)
		{
			return false;
		}
	}

	SERVICE_STATUS status;
	
	// ���������� ���������� ������������� ������
	for (;;)
	{
		if (!::QueryServiceStatus(m_hService, &status))
		{
			return false;
		}

		if (status.dwCurrentState != SERVICE_START_PENDING)
		{
			break;
		}

		// ������ ��������, ��� �����-������ ����� ����� �����
		// �������� �������� ��������, ��� ������� �� ��������
/*		DWORD dwWait = status.dwWaitHint;
		if (!dwWait)
		{
			dwWait = 100;
		}

		::Sleep(dwWait);*/
		::Sleep(100);
	}

	if (status.dwCurrentState != SERVICE_RUNNING)
	{
		return false;
	}

	return true;
}

bool CServiceControl::Control(DWORD dwControl, 
							  DWORD dwState, 
							  DWORD dwTimeout)
{
	SERVICE_STATUS status;
	
	if (!::ControlService(
		m_hService,
		dwControl,
		&status))
	{
		return false;
	}

	// �������� ����� ������ ��������
	DWORD dwStart = ::GetTickCount();
	DWORD dwCheckPoint = 0, dwWait;

	// ������� ���� ������ ������ ��������� ���������
	while (
		status.dwCurrentState != dwState &&
		status.dwCurrentState != SERVICE_STOPPED)
	{
		// ���������, �� ����� �� �������
		if (dwTimeout != INFINITE)
		{
			if (::GetTickCount() - dwStart >= dwTimeout)
			{
				return ::SetLastError(ERROR_TIMEOUT), false;
			}
		}

		// ���������� �������� �������� �� ��������� ��������
		// ���������
		if (dwCheckPoint != status.dwCheckPoint)
		{
			dwCheckPoint = status.dwCheckPoint;
			dwWait = status.dwWaitHint;
		}
		else
		{
			dwWait = 100;
		}

		// �����
		::Sleep(100/*dwWait - ��. ����*/);

		// �������� ��������� ������
		if (!::QueryServiceStatus(m_hService, &status))
		{
			return false;
		}
	}

	if (status.dwCurrentState == SERVICE_STOPPED &&
		status.dwWin32ExitCode != ERROR_SUCCESS)
	{
		::SetLastError(status.dwWin32ExitCode);
		return false;
	}
	
    return true;
}

bool CServiceControl::StopWithDependents()
{
	Config config = GetConfig();

	CServiceManager man;
	for (int i = 0; i < (int)config.dependencies.size(); i++)
	{
		CServiceControl svc = man.ServiceOpen(config.dependencies[i], 0);
		svc.Stop();
	}

	return Stop();
}

/************************************************************************/
/* ���������� ������ CNTService                                         */
/************************************************************************/

CNTService::CNTService(const wstring& sName, 
					   const wstring& sDisplayName, 
					   const wstring& sDescription) :
	m_sServiceName(sName),
	m_sDisplayName(sDisplayName),
	m_sDescription(sDescription)
{
	Init();
	m_hExitEvent = ::CreateEventW(NULL, TRUE, FALSE, NULL);
	m_hPendingEvent = ::CreateEventW(NULL, FALSE, FALSE, NULL);
}

void CNTService::Install()
{
	SVService::CServiceManager svcMan;

	SVService::CServiceControl svc(svcMan.ServiceInstall(
		m_sServiceName, 
		m_sDisplayName, 
		SVUtils::GetModuleFileName()));

	SVService::CServiceControl::Config config;
	config.sDescription = m_sDescription;

	if (!svc.SetConfig(config, SVService::CServiceControl::ctDescription))
	{
		THROW_LASTERR;
	}
}

void CNTService::Uninstall()
{
	SVService::CServiceManager svcMan;

	try
	{
		SVService::CServiceControl svc(svcMan.ServiceOpen(m_sServiceName, SERVICE_STOP | SERVICE_QUERY_STATUS));
		svc.Stop();
	}
	catch (...)
	{
	}

	svcMan.ServiceUninstall(m_sServiceName);
}

void CNTService::Start()
{
	SVService::CServiceManager svcMan;
	SVService::CServiceControl svc(svcMan.ServiceOpen(m_sServiceName, SERVICE_START | SERVICE_QUERY_STATUS));

	if (!svc.Start())
	{
		THROW_LASTERR;
	}
}

void CNTService::Stop()
{
	SVService::CServiceManager svcMan;
	SVService::CServiceControl svc(svcMan.ServiceOpen(m_sServiceName, SERVICE_STOP | SERVICE_QUERY_STATUS));

	if (!svc.Stop())
	{
		THROW_LASTERR;
	}
}

void CNTService::Init()
{
	m_dwPendingCode = 0;
	m_hStatus = NULL;
	ZeroMemory(&m_Status, sizeof(m_Status));
	m_Status.dwServiceType = SERVICE_WIN32; 
	m_Status.dwControlsAccepted = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_PAUSE_CONTINUE;
}

bool CNTService::SetStatus(DWORD dwCurrentState)
{
	if (m_hStatus != NULL)
	{
		m_Status.dwCurrentState = dwCurrentState;
		return ::SetServiceStatus(m_hStatus, &m_Status) != 0;
	}
	return false;
}

void CNTService::StartPending(DWORD dwPendingCode)
{
	m_dwPendingCode = dwPendingCode;
	m_Status.dwCheckPoint = 0;
	m_Status.dwWaitHint = 2000;

	SetStatus(dwPendingCode);
	
	if (!m_PendingThread.Handle.IsValid())
	{
		m_PendingThread.Create((LPTHREAD_START_ROUTINE)PendingThreadProc, this);
	}
}

void CNTService::StopPending()
{
	m_Status.dwCheckPoint = 0;
	m_Status.dwWaitHint = 0;

	if (m_PendingThread.Handle.IsValid())
	{
		::SetEvent(m_hPendingEvent);
		m_PendingThread.WaitFor();
	}
}

DWORD WINAPI CNTService::PendingThreadProc(CNTService* pService)
{
	return pService->PendingProc();
}

DWORD CNTService::PendingProc()
{
	while (WaitForSingleObject(m_hPendingEvent, 1000) != WAIT_OBJECT_0)
	{
		SetStatus(m_dwPendingCode);
		m_Status.dwCheckPoint++;
	}

	return 0;
}

void CNTService::ServiceMain(DWORD dwArgc, LPWSTR* lpszArgv)
{
	if (!(m_hStatus = ::RegisterServiceCtrlHandlerExW(
		lpszArgv[0], 
		(LPHANDLER_FUNCTION_EX)HandlerEx, 
		this)))
	{
		return;
	}

	StartPending(SERVICE_START_PENDING);
	
	bool bRes = false;
	try
	{
		for (DWORD i = 0; i < dwArgc; i++)
		{
			m_Args.push_back(lpszArgv[i]);
		}
		bRes = OnInit();
	}
	catch (...)
	{
	}

	StopPending();

	if (bRes)
	{
		SetStatus(SERVICE_RUNNING);

		try
		{
			Work();
			OnStop();
		}
		catch (...)
		{
		}
		
		StopPending();
	}

	SetStatus(SERVICE_STOPPED);
	Init();
}

DWORD WINAPI CNTService::HandlerEx(DWORD dwControl,
								   DWORD dwEventType,
								   LPVOID lpEventData,
								   CNTService* pService)
{
	if (dwControl == SERVICE_CONTROL_STOP)
	{ 
		::SetEvent(pService->m_hExitEvent);
		pService->StartPending(SERVICE_STOP_PENDING);
	}
	else
	{
		::SetServiceStatus(pService->m_hStatus, &pService->m_Status);
		return ERROR_CALL_NOT_IMPLEMENTED;
	}

	return NO_ERROR;
}

void CNTService::StartConsole()
{
	if (!m_ConsoleThread.Handle.IsValid())
	{
		m_ConsoleThread.Create((LPTHREAD_START_ROUTINE)ConsoleThreadProc, this);
	}
}

void CNTService::StopConsole()
{
	if (m_ConsoleThread.Handle.IsValid())
	{
		::SetEvent(m_hExitEvent);
		m_ConsoleThread.WaitFor();
	}
}

DWORD WINAPI CNTService::ConsoleThreadProc(CNTService* pService)
{
	return pService->ConsoleProc();
}

DWORD CNTService::ConsoleProc()
{
	try
	{
		if (OnInit())
		{
			Work();
			OnStop();
		}
	}
	catch (...)
	{
		return EVENT_E_INTERNALEXCEPTION;
	}

	return 0;
}

/************************************************************************/
/* ���������� ������ CServiceDispatcher                                 */
/************************************************************************/

CServiceDispatcher::CServiceDispatcher()
{
	if (g_pSvcDispatcher)
	{
		throw 0;
	}
	g_pSvcDispatcher = this;
}

CServiceDispatcher::~CServiceDispatcher()
{
	g_pSvcDispatcher = NULL;
}

void CServiceDispatcher::AddService(LPCWSTR pwszSvcName, CNTService* pSvc)
{
	m_SvcMap[pwszSvcName] = pSvc;
}

bool CServiceDispatcher::Start()
{
	if (m_SvcMap.empty())
	{
		return false;
	}

	std::auto_ptr<SERVICE_TABLE_ENTRYW> pSvc(new SERVICE_TABLE_ENTRYW[m_SvcMap.size() + 1]);

	SVCMAP::iterator it = m_SvcMap.begin();
	SERVICE_TABLE_ENTRYW* pEntry = pSvc.get();
	for (; it != m_SvcMap.end(); ++it, ++pEntry)
	{
		SERVICE_TABLE_ENTRYW entry;
		pEntry->lpServiceName = wcsdup(it->first.c_str());
		pEntry->lpServiceProc = ServiceMain;
	}

	pEntry->lpServiceName = NULL;
	pEntry->lpServiceProc = NULL;

	return ::StartServiceCtrlDispatcherW(pSvc.get()) != 0;
}

void CServiceDispatcher::StartConsole()
{
	if (m_SvcMap.empty())
	{
		return;
	}

	SetConsoleCtrlHandler(ConsoleCtrlHandler, TRUE);
	
	vector<HANDLE> handles;

	SVCMAP::iterator it = m_SvcMap.begin();
	for (; it != m_SvcMap.end(); ++it)
	{
		it->second->StartConsole();
		handles.push_back(it->second->m_ConsoleThread.GetHandle());
	}
	
	WaitForMultipleObjects((DWORD)handles.size(), &handles[0], TRUE, INFINITE);
}

BOOL WINAPI CServiceDispatcher::ConsoleCtrlHandler(DWORD dwCtrlType)
{
	switch (dwCtrlType)
	{
		case CTRL_C_EVENT:
		case CTRL_BREAK_EVENT:
		case CTRL_CLOSE_EVENT:
		case CTRL_LOGOFF_EVENT:
		case CTRL_SHUTDOWN_EVENT:
			break;

		default:
			return FALSE;
	}

	SVCMAP::iterator it = g_pSvcDispatcher->m_SvcMap.begin();
	for (; it != g_pSvcDispatcher->m_SvcMap.end(); ++it)
	{
		::SetEvent(it->second->m_hExitEvent);
	}

	return TRUE;
}

VOID WINAPI CServiceDispatcher::ServiceMain(DWORD dwArgc, LPWSTR* lpszArgv)
{
	SVCMAP::iterator it = g_pSvcDispatcher->m_SvcMap.find(lpszArgv[0]);
	if (it != g_pSvcDispatcher->m_SvcMap.end())
	{
		it->second->ServiceMain(dwArgc, lpszArgv);
	}
}

CServiceDispatcher* g_pSvcDispatcher = NULL;

}
