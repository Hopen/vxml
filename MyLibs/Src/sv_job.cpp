/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include <Windows.h>
#include "sv_job.h"

namespace SVJob
{

/************************************************************************/
/* CJob class implementation                                            */
/************************************************************************/

CJob::CJob() : m_pNotify(NULL)
{
	Init();
}

CJob::~CJob()
{
	Destroy();
}

CJob::CJob(LPCWSTR pwszName, LPSECURITY_ATTRIBUTES lpJobAttributes)
{
	Init();
	Create(pwszName, lpJobAttributes);
}

bool CJob::Init()
{
	DWORD dwTID;
	m_hIOCP = ::CreateIoCompletionPort(INVALID_HANDLE_VALUE, 0, (DWORD)this, 0);
	m_hThread = ::CreateThread(NULL, 0, CJob::CallbackThread, (LPVOID)this, 0, &dwTID);
	return m_hThread.IsValid() && m_hIOCP.IsValid();
}

bool CJob::Create(LPCWSTR pwszName, LPSECURITY_ATTRIBUTES lpJobAttributes)
{
	m_hJob = ::CreateJobObjectW(lpJobAttributes, pwszName);
	if (m_hJob.IsValid())
	{
		if (SetCompletionPort(m_hIOCP, this))
		{
			return true;
		}
	}
	return false;
}

bool CJob::Open(LPCWSTR	pwszName,
				DWORD	dwDesiredAccess,
				BOOL	bInheritHandles)
{
	m_hJob = ::OpenJobObjectW(dwDesiredAccess, bInheritHandles, pwszName);
	if (m_hJob.IsValid())
	{
		if (SetCompletionPort(m_hIOCP, this))
		{
			return true;
		}
	}
	return false;
}

void CJob::Destroy()
{
	::PostQueuedCompletionStatus(m_hIOCP, 0, 0, NULL);
	::WaitForSingleObject(m_hThread, INFINITE);
	m_hJob.Close();
	m_hThread.Close();
	m_hIOCP.Close();
}

bool CJob::AssignProcess(HANDLE hProcess)
{
	return ::AssignProcessToJobObject(m_hJob, hProcess) != 0;
}

bool CJob::Terminate(UINT uExitCode)
{
	return ::TerminateJobObject(m_hJob, uExitCode) != 0;
}

bool CJob::SetCompletionPort(HANDLE hIOCP, PVOID pvCompletionKey)
{
	JOBOBJECT_ASSOCIATE_COMPLETION_PORT	t = {pvCompletionKey, hIOCP};
	return SetInformation<JOBOBJECT_ASSOCIATE_COMPLETION_PORT>(
		JobObjectAssociateCompletionPortInformation, &t);
}

CJobLimits CJob::GetLimits()
{
	CJobLimits lim;

	GetInformation<JOBOBJECT_EXTENDED_LIMIT_INFORMATION>(
		JobObjectExtendedLimitInformation, &lim);

	return lim;
}

bool CJob::SetLimits(CJobLimits& lim)
{
	return SetInformation<JOBOBJECT_EXTENDED_LIMIT_INFORMATION>(
		JobObjectExtendedLimitInformation, &lim);
}

CJobUIRestrictions CJob::GetUIRestrictions()
{
	CJobUIRestrictions Restr;
	GetInformation<JOBOBJECT_BASIC_UI_RESTRICTIONS>(JobObjectBasicUIRestrictions, &Restr);
	return Restr;
}

bool CJob::SetUIRestrictions(CJobUIRestrictions Restr)
{
	return SetInformation<JOBOBJECT_BASIC_UI_RESTRICTIONS>(
		JobObjectBasicUIRestrictions, &Restr) != 0;
}

bool CJob::SetEndOfJobTimeInfo(DWORD dwFlags)
{
	JOBOBJECT_END_OF_JOB_TIME_INFORMATION t = { dwFlags };
	return SetInformation<JOBOBJECT_END_OF_JOB_TIME_INFORMATION>(
		JobObjectEndOfJobTimeInformation, &t) != 0;
}

DWORD CJob::GetProcessIDCount()
{
	JOBOBJECT_BASIC_PROCESS_ID_LIST	idlist;
	ZeroMemory(&idlist, sizeof(idlist));
	GetInformation<JOBOBJECT_BASIC_PROCESS_ID_LIST>(JobObjectBasicProcessIdList, &idlist);
	return idlist.NumberOfAssignedProcesses;
}

std::vector<DWORD>	CJob::GetProcessIDList()
{
	std::vector<DWORD> v;
	int nMaxPIDS = GetProcessIDCount();
	size_t iStructSize = nMaxPIDS * sizeof(ULONG_PTR) + sizeof(JOBOBJECT_BASIC_PROCESS_ID_LIST);
	std::auto_ptr<JOBOBJECT_BASIC_PROCESS_ID_LIST> pList((PJOBOBJECT_BASIC_PROCESS_ID_LIST)(new BYTE[iStructSize]));

	GetInformation<JOBOBJECT_BASIC_PROCESS_ID_LIST>(JobObjectBasicProcessIdList, pList.get());
	for (DWORD i = 0; i < pList->NumberOfProcessIdsInList; i++)
	{
		v.push_back(pList->ProcessIdList[i]);
	}

	return v;
}

CJobNotify*	CJob::GetNotifyHandler()
{
	return m_pNotify;
}

CJobNotify*	CJob::SetNotifyHandler(CJobNotify* pNotify)
{
	CJobNotify*	pOldNotify = m_pNotify;
	m_pNotify = pNotify;
	return pOldNotify;
}

DWORD WINAPI CJob::CallbackThread(LPVOID pThis)
{
	try
	{
		((CJob*)pThis)->CallbackThread();
	}
	catch (...)
	{
	}
	return 0;
}

void CJob::CallbackThread()
{
	DWORD dwMsg = 0;
	DWORD dwKey = 0;
	DWORD dwPID = 0;

	while (::GetQueuedCompletionStatus(m_hIOCP, &dwMsg, &dwKey, (LPOVERLAPPED*)&dwPID, INFINITE))
	{
		if (dwKey != (DWORD)this)
		{
			// ��� � ������ ������ ����������
			break;
		}

		if (!m_pNotify)
		{
			// ��� �������� - ����� ��� � ��������
			continue;
		}

		try
		{
			switch(dwMsg)
			{
				case JOB_OBJECT_MSG_END_OF_JOB_TIME:
					m_pNotify->OnEndOfJodTime();
					break;

				case JOB_OBJECT_MSG_END_OF_PROCESS_TIME:
					m_pNotify->OnEndOfProcessTime(dwPID);
					break;

				case JOB_OBJECT_MSG_ACTIVE_PROCESS_LIMIT:
					m_pNotify->OnActiveProcessLimit();
					break;

				case JOB_OBJECT_MSG_ACTIVE_PROCESS_ZERO:
					m_pNotify->OnActiveProcessZero();
					break;

				case JOB_OBJECT_MSG_NEW_PROCESS:
					m_pNotify->OnNewProcess(dwPID);
					break;

				case JOB_OBJECT_MSG_EXIT_PROCESS:
					m_pNotify->OnExitProcess(dwPID);
					break;

				case JOB_OBJECT_MSG_ABNORMAL_EXIT_PROCESS:
					m_pNotify->OnAbnormalExitProcess(dwPID);
					break;

				case JOB_OBJECT_MSG_PROCESS_MEMORY_LIMIT:
					m_pNotify->OnProcessMemoryLimit(dwPID);
					break;

				case JOB_OBJECT_MSG_JOB_MEMORY_LIMIT:
					m_pNotify->OnJobMemoryLimit(dwPID);
					break;
			}
		}
		catch (...)
		{
		}
	}
}

/************************************************************************/
/* CJobLimits class implementation                                      */
/************************************************************************/

LARGE_INTEGER CJobLimits::GetPerProcessUserTime()
{
	LARGE_INTEGER li;
	ZeroMemory(&li, sizeof(li));

	return (LimitFlag[JOB_OBJECT_LIMIT_PROCESS_TIME])
		? this->BasicLimitInformation.PerProcessUserTimeLimit : li;
}

LARGE_INTEGER CJobLimits::GetPerJobUserTime()
{
	LARGE_INTEGER li;
	ZeroMemory(&li, sizeof(li));

	return (LimitFlag[JOB_OBJECT_LIMIT_JOB_TIME])
		? this->BasicLimitInformation.PerJobUserTimeLimit : li;
}

DWORD CJobLimits::GetLimitFlags()
{
	return this->BasicLimitInformation.LimitFlags;
}

SIZE_T CJobLimits::GetMinWorkingSetSize()
{
	return (LimitFlag[JOB_OBJECT_LIMIT_WORKINGSET])
		? this->BasicLimitInformation.MinimumWorkingSetSize : 0;
}

SIZE_T CJobLimits::GetMaxWorkingSetSize()
{
	return (LimitFlag[JOB_OBJECT_LIMIT_WORKINGSET])
		? this->BasicLimitInformation.MaximumWorkingSetSize : 0;
}

DWORD CJobLimits::GetActiveProcessLimit()
{
	return (LimitFlag[JOB_OBJECT_LIMIT_ACTIVE_PROCESS])
		? this->BasicLimitInformation.ActiveProcessLimit : 0;
}

ULONG_PTR CJobLimits::GetAffinity()
{
	return (LimitFlag[JOB_OBJECT_LIMIT_AFFINITY])
		? this->BasicLimitInformation.Affinity : 0;
}

DWORD CJobLimits::GetPriority()
{
	return (LimitFlag[JOB_OBJECT_LIMIT_PRIORITY_CLASS])
		? this->BasicLimitInformation.PriorityClass : 0;
}

DWORD CJobLimits::GetSchedulingClass()
{
	return (LimitFlag[JOB_OBJECT_LIMIT_SCHEDULING_CLASS])
		? this->BasicLimitInformation.SchedulingClass : 0;
}

LARGE_INTEGER CJobLimits::SetPerProcessUserTime(LARGE_INTEGER liLimit)
{
	LimitFlag[JOB_OBJECT_LIMIT_PROCESS_TIME] = true;
	this->BasicLimitInformation.PerProcessUserTimeLimit = liLimit;
	return liLimit;
}

LARGE_INTEGER CJobLimits::SetPerJobUserTime(LARGE_INTEGER liLimit)
{
	LimitFlag[JOB_OBJECT_LIMIT_JOB_TIME] = true;
	this->BasicLimitInformation.PerJobUserTimeLimit = liLimit;
	return liLimit;
}

SIZE_T CJobLimits::SetMinWorkingSetSize(SIZE_T dwVal)
{
	LimitFlag[JOB_OBJECT_LIMIT_WORKINGSET] = true;
	this->BasicLimitInformation.MinimumWorkingSetSize = dwVal;
	return dwVal;
}

SIZE_T CJobLimits::SetMaxWorkingSetSize(SIZE_T dwVal)
{
	LimitFlag[JOB_OBJECT_LIMIT_WORKINGSET] = true;
	this->BasicLimitInformation.MaximumWorkingSetSize = dwVal;
	return dwVal;
}

DWORD CJobLimits::SetActiveProcessLimit(DWORD dwLim)
{
	LimitFlag[JOB_OBJECT_LIMIT_ACTIVE_PROCESS] = true;
	this->BasicLimitInformation.ActiveProcessLimit = dwLim;
	return dwLim;
}

DWORD CJobLimits::SetPriority(DWORD dwPriority)
{
	LimitFlag[JOB_OBJECT_LIMIT_PRIORITY_CLASS] = true;
	this->BasicLimitInformation.PriorityClass = dwPriority;
	return dwPriority;
}

ULONG_PTR CJobLimits::SetAffinity(ULONG_PTR pAffinity)
{
	LimitFlag[JOB_OBJECT_LIMIT_AFFINITY] = true;
	this->BasicLimitInformation.Affinity = pAffinity;
	return pAffinity;
}

DWORD CJobLimits::SetSchedulingClass(DWORD dwSC)
{
	LimitFlag[JOB_OBJECT_LIMIT_SCHEDULING_CLASS] = true;
	this->BasicLimitInformation.SchedulingClass = dwSC;
	return dwSC;
}

SIZE_T CJobLimits::SetProcessMemoryLimit(SIZE_T liLimit)
{
	LimitFlag[JOB_OBJECT_LIMIT_PROCESS_MEMORY] = true;
	((JOBOBJECT_EXTENDED_LIMIT_INFORMATION*)this)->ProcessMemoryLimit = liLimit;
	return liLimit;
}

SIZE_T CJobLimits::SetJobMemoryLimit(SIZE_T liLimit)
{
	LimitFlag[JOB_OBJECT_LIMIT_JOB_MEMORY] = true;
	((JOBOBJECT_EXTENDED_LIMIT_INFORMATION*)this)->JobMemoryLimit = liLimit;
	return liLimit;
}

SIZE_T CJobLimits::GetProcessMemoryLimit()
{
	return 	(LimitFlag[JOB_OBJECT_LIMIT_PROCESS_MEMORY])
		? ((JOBOBJECT_EXTENDED_LIMIT_INFORMATION*)this)->ProcessMemoryLimit : 0;
}

SIZE_T CJobLimits::GetJobMemoryLimit()
{
	return (LimitFlag[JOB_OBJECT_LIMIT_JOB_MEMORY])
		? ((JOBOBJECT_EXTENDED_LIMIT_INFORMATION*)this)->JobMemoryLimit : 0;
}

SIZE_T CJobLimits::GetPeakProcessMemoryUsed()
{
	return ((JOBOBJECT_EXTENDED_LIMIT_INFORMATION*)this)->PeakProcessMemoryUsed;
}

SIZE_T CJobLimits::GetPeakJobMemoryUsed()
{
	return ((JOBOBJECT_EXTENDED_LIMIT_INFORMATION*)this)->PeakJobMemoryUsed;
}

}
