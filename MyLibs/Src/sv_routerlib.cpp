/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include "sv_routerlib.h"
#include "sv_utils.h"

namespace SVRouterLib
{

/************************************************************************/
/* CRouterClient class implementation                                   */
/************************************************************************/

CRouterClient::CRouterClient()
{
	WSADATA wsd = {0};
	::WSAStartup(MAKEWORD(2,2), &wsd);
}

CRouterClient::~CRouterClient()
{
	CRouterConnector::Close();
	::WSACleanup();
}

DWORD CRouterClient::RegisterEvent(LPCWSTR pwszEvent)
{
	DWORD dwHash = CEventSubscribers::HashStr(pwszEvent);
	m_EventHashMap[dwHash] = pwszEvent;
	return dwHash;
}

std::wstring CRouterClient::EventByHash(DWORD dwHash)
{
	EVENTHASHMAP::const_iterator it = m_EventHashMap.find(dwHash);
	return (it == m_EventHashMap.end()) ? std::wstring(L"") : it->second;
}

void CRouterClient::Connect()
{
	CRouterConnector::Init(this);
}

bool CRouterClient::ConnectorThreadActive() const
{
	DWORD dwExitCode = 0;
	::GetExitCodeThread(m_hThread, &dwExitCode);
	return dwExitCode == STILL_ACTIVE;
}

void CRouterClient::EventSubscribe(LPCWSTR pwszEvent)
{
	RegisterEvent(pwszEvent);
	CRouterConnector::EventSubscribe(pwszEvent);
}

void CRouterClient::EventUnsubscribe(LPCWSTR pwszEvent)
{
	CRouterConnector::EventUnsubscribe(pwszEvent);
}

// Send methods

DWORD CRouterClient::SendTo(CLIENT_ADDRESS To, const CMessage& Msg)
{
	return SendFromTo(Self(), To, Msg);
}

DWORD CRouterClient::SendFromTo(CLIENT_ADDRESS From, CLIENT_ADDRESS To, const CMessage& Msg)
{
	DWORD dwRes = CRouterConnector::WriteFromTo(From, To, Msg, NULL);

	RegisterEvent(Msg.Name);
	LogFromTo(__FUNCTIONW__, From, To, Msg, false, dwRes);

	return dwRes;
}

DWORD CRouterClient::SendFromToAny(CLIENT_ADDRESS From, const CMessage& Msg)
{
	return SendFromTo(From, CLIENT_ADDRESS::Any(), Msg);
}

DWORD CRouterClient::SendFromToAll(CLIENT_ADDRESS From, const CMessage& Msg)
{
	return SendFromTo(From, CLIENT_ADDRESS::All(), Msg);
}

DWORD CRouterClient::SendToAny(const CMessage& Msg, const CMessage* pAck)
{
	return SendFromToAny(Self(), Msg);
}

DWORD CRouterClient::SendToAll(const CMessage& Msg)
{
	return SendFromToAll(Self(), Msg);
}

// CRouterMsgHandler methods

void CRouterClient::NotifyRead(CRouterConnector*	pConnector,
							   CMessage&			Msg,
							   CLIENT_ADDRESS		From,
							   CLIENT_ADDRESS		To)
{
	LogFromTo(__FUNCTIONW__, From, To, Msg, true, 0);
	RegisterEvent(Msg.Name);
	try
	{
		OnReceive(Msg, From, To);
	}
	catch (EDeliverError)
	{
		throw;
	}
	catch (...)
	{
	}
}

void CRouterClient::NotifyWrite(CRouterConnector	*pConnector, 
								CMessage			&Msg,
								CLIENT_ADDRESS		From, 
								CLIENT_ADDRESS		To)
{

}

void CRouterClient::NotifyDeliverError(CRouterConnector*	pConnector,
									   CMessage&			Msg,
									   CLIENT_ADDRESS*		pTo)
{
	Log(__FUNCTIONW__, 
		L"Error delivering msg \"%s\" to %08X-%08X", 
		Msg.Name,
		pTo->ClientID, pTo->ChildID);
}

void CRouterClient::NotifyConnect(CRouterConnector*	pConnector,
								  LPCWSTR			pwszHost,
								  WORD				wPort)
{
	Log(__FUNCTIONW__, L"Connected to %s:%d, Addr = 0x%08X", pwszHost, wPort, CRouterConnector::Self().ClientID);
	try
	{
		OnConnect(pwszHost, wPort);
	}
	catch (...)
	{
	}
}

void CRouterClient::NotifyDisconnect(CRouterConnector* pConnector, bool& bConnect)
{
	Log(__FUNCTIONW__, L"Disconnected");
	try
	{
		OnDisconnect(bConnect);
	}
	catch (...)
	{
	}
}

void CRouterClient::NotifyError(CRouterConnector* pConnector, HRESULT hError)
{
	std::wstring sDescr = SVUtils::WinErrToStr(hError);
	Log(__FUNCTIONW__, L"Error HR=0x%08X (%s)", hError, sDescr.c_str());
	try
	{
		OnError(hError, sDescr.c_str());
	}
	catch (...)
	{
	}
}

void CRouterClient::NotifyStatusChange(CRouterConnector*	pConnector, 
									   CLIENT_ADDRESS		client, 
									   int					nStatus)
{
	Log(__FUNCTIONW__, L"%08X-%08X -> %s", client.ClientID, client.ChildID, ClientStatusToString(nStatus));
	try
	{
		OnStatusChange(client, nStatus);
	}
	catch (...)
	{
	}
}

void CRouterClient::NotifyEventSubscribe(CRouterConnector*	pConnector,
										 DWORD				dwEventHash,
										 CLIENT_ADDRESS		client,
										 int				nStatus,
										 int				nConsumers)
{
	std::wstring sEventName = EventByHash(dwEventHash);
	
	if (sEventName.length() != 0)
	{
		Log(__FUNCTIONW__,
			L"Hash=%08X, Name=\"%s\", Client=%08X-%08X -> %s",
			dwEventHash,
			sEventName.c_str(),
			client.ClientID, client.ChildID,
			EventStatusToString(nStatus));
	}

	try
	{
		OnEventSubscribe(dwEventHash, client, nStatus, nConsumers);
	}
	catch (...)
	{
	}
}

void CRouterClient::NotifyMonitorEvent(CRouterConnector*	pConnector,
									   CMessage&			Msg,
									   CLIENT_ADDRESS*		pFrom,
									   CLIENT_ADDRESS*		pTo)
{
}

// Helper methods

LPCWSTR CRouterClient::ClientStatusToString(int nStatus)
{
	switch (nStatus)
	{
		case CS_CONNECTED:		return L"CS_CONNECTED";
		case CS_DISCONNECTED:	return L"CS_DISCONNECTED";
	}

	return L"CS_UNKNOWN";
}

LPCWSTR CRouterClient::EventStatusToString(int nStatus)
{
	switch (nStatus)
	{
		case ES_ACTIVE:			return L"ES_ACTIVE";
		case ES_SUBSCRIBED:		return L"ES_SUBSCRIBED";
		case ES_NOT_SUBSCRIBED:	return L"ES_NOT_SUBSCRIBED";
	}

	return L"ES_UNKNOWN";
}

CLIENT_ADDRESS CRouterClient::ClientToAddr(DWORD dwClient)
{
	return CLIENT_ADDRESS(dwClient, 0);
}

// Logs

void CRouterClient::LogFromTo(LPCWSTR			pwszFunc,
							  CLIENT_ADDRESS	From,
							  CLIENT_ADDRESS	To,
							  const CMessage&	Msg,
							  bool				bRead,
							  DWORD				dwRes)
{
	Log(pwszFunc, L"%08X-%08X -> %08X-%08X %s EVENT %s", 
		From.ClientID, From.ChildID,
		To.ClientID, To.ChildID,
		bRead ? L"READ" : ((dwRes == NO_ERROR) ? L"SUCCEEDED" : L"FAILED"),
		Msg.Dump().c_str());
}

/************************************************************************/
/* CRouterClientDelegate implementation                                 */
/************************************************************************/

void CRouterClientDelegate::RegisterNamedReceiveHandler(LPCWSTR		pwszName, 
														CRouterClientDelegate::MsgHandler	pHandler, 
														bool		bAutoSubscribe, 
														bool		bRegister)
{
	HANDLERSMAP::iterator it = m_HandlersMap.find(pwszName);

	if (bRegister)
	{
		if (bAutoSubscribe && it != m_HandlersMap.end())
		{
			EventSubscribe(pwszName);
		}

		MsgHandlerEx handler = {pHandler, bAutoSubscribe};
		m_HandlersMap[pwszName] = handler;
	}
	else if (it != m_HandlersMap.end())
	{
		m_HandlersMap.erase(it);
	}
}

void CRouterClientDelegate::OnReceive(CMessage &Msg, CLIENT_ADDRESS From, CLIENT_ADDRESS To)
{
	if (m_ReceiveHandler)
	{
		m_ReceiveHandler(Msg, From, To);
	}

	HANDLERSMAP::const_iterator it = m_HandlersMap.find(Msg.Name);
	if (it != m_HandlersMap.end())
	{
		MsgHandlerEx handler = it->second;
		handler.pHandler(Msg, From, To);
	}
}

void CRouterClientDelegate::OnConnect(LPCWSTR pwszHost, WORD wPort)
{
	if (m_ConnectedHandler)
	{
		m_ConnectedHandler(pwszHost, wPort);
	}

	HANDLERSMAP::const_iterator it = m_HandlersMap.begin();
	for (; it != m_HandlersMap.end(); ++it)
	{
		const MsgHandlerEx& handler = it->second;
		if (handler.bAutoSubscribe)
		{
			EventSubscribe(it->first.c_str());
		}
	}
}

void CRouterClientDelegate::OnDisconnect(bool &bConnectAgain)
{
	bConnectAgain = false;
	if (m_DisconnectedHandler)
	{
		m_DisconnectedHandler(bConnectAgain);
	}
}

void CRouterClientDelegate::OnError(HRESULT hr, LPCWSTR pwszDescr)
{
}

void CRouterClientDelegate::OnStatusChange(CLIENT_ADDRESS client, int nStatus)
{
	if (m_StatusChangeHandler)
	{
		m_StatusChangeHandler(client, nStatus);
	}
}

void CRouterClientDelegate::OnEventSubscribe(DWORD			dwEventHash,
											 CLIENT_ADDRESS	client,
											 int			nStatus,
											 int			nConsumers)
{
	if (m_SubscribeHandler)
	{
		m_SubscribeHandler(dwEventHash, client, nStatus, nConsumers);
	}
}

}
