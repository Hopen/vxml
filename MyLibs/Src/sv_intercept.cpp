/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include <Windows.h>
#include "sv_intercept.h"

namespace SVInterceptors
{

LPVOID ModifyModuleAPIThunk(HMODULE hModule, 
							LPCSTR pszDllName, 
							LPCSTR pszAPIName, 
							LPVOID pNewThunk)
{
	DWORD dwImageBase = (DWORD)hModule;
	PIMAGE_DOS_HEADER pDosHdr		= (PIMAGE_DOS_HEADER)dwImageBase;
	PIMAGE_NT_HEADERS pNtHdr		= (PIMAGE_NT_HEADERS)(dwImageBase + pDosHdr->e_lfanew);
	PIMAGE_IMPORT_DESCRIPTOR piid	= (PIMAGE_IMPORT_DESCRIPTOR)(dwImageBase 
		+ pNtHdr->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
	IMAGE_IMPORT_DESCRIPTOR iid		= {0};
	LPVOID pOldThunk = NULL;

	for (;; piid++)
	{
		if (memcmp(piid, &iid, sizeof(iid)) == 0)
		{
			break;
		}
		LPCSTR pszName = (LPCSTR)(dwImageBase + piid->Name);
		if (stricmp(pszName, pszDllName) == 0)
		{
			LPDWORD pImportByNameRVA = (LPDWORD)(dwImageBase + piid->OriginalFirstThunk);
			for (int i = 0; pImportByNameRVA[i] != 0; i++)
			{
				if ((pImportByNameRVA[i] & 0x80000000) == 0)
				{
					PIMAGE_IMPORT_BY_NAME piibn = (PIMAGE_IMPORT_BY_NAME)(dwImageBase + pImportByNameRVA[i]);
					if (stricmp((LPCSTR)piibn->Name, pszAPIName) == 0)
					{
						LPVOID* pThunk = (LPVOID*)(dwImageBase + piid->FirstThunk);
						pOldThunk = pThunk[i];

						DWORD dwOldProtect = 0;
						::VirtualProtect(pThunk + i, sizeof(LPVOID), PAGE_READWRITE, &dwOldProtect);			
						pThunk[i] = pNewThunk;
						::VirtualProtect(pThunk + i, sizeof(LPVOID), dwOldProtect, &dwOldProtect);

						return pOldThunk;
					}
				}
			}
		}
	}

	return pOldThunk;
}

void InstallHelper(LPCVOID pProcPtr, LPCVOID pStubPtr)
{
	__asm
	{
/*		mov eax, pProcPtr
		mov byte ptr [eax], 068h	// push offset pStubPtr
		inc eax
		mov ebx, pStubPtr
		mov dword ptr [eax], ebx
		add eax, 4
		mov byte ptr [eax], 0C3h	// ret*/
		mov eax, pProcPtr
		mov byte ptr [eax], 0E9h
		mov ebx, pStubPtr
		sub ebx, 5
		sub ebx, eax
		mov dword ptr [eax+1], ebx
	}
}

bool InstallStubCode(LPCSTR pszDllName, LPCSTR pszProcName, LPVOID pStubPtr, STUB_CODE* pStubCode)
{
	HMODULE hMod = ::GetModuleHandleA(pszDllName);

	if (hMod == NULL)
	{
		return false;
	}

	LPVOID pProcPtr = ::GetProcAddress(hMod, pszProcName);

	if (pProcPtr == NULL)
	{
		return false;
	}

	return InstallStubCode(pProcPtr, pStubPtr, pStubCode);
}

bool InstallStubCode(LPVOID pProcPtr, LPVOID pStubPtr, STUB_CODE* pStubCode)
{
	DWORD dwOldProtect = 0;
	DWORD dwSize = sizeof(pStubCode->data);

	pStubCode->pProcPtr = pProcPtr;
	pStubCode->pStubPtr = pStubPtr;
	memcpy(pStubCode->data, pProcPtr, dwSize);

	if (::VirtualProtect(pProcPtr, dwSize, 
		PAGE_EXECUTE_READWRITE, &dwOldProtect) == 0)
	{
		return false;
	}

	InstallHelper(pProcPtr, pStubPtr);

	::VirtualProtect(pProcPtr, dwSize, dwOldProtect, &dwOldProtect);

	return true;
}

bool UninstallStubCode(const STUB_CODE* pStubCode)
{
	DWORD dwOldProtect = 0;
	DWORD dwSize = sizeof(pStubCode->data);
	
	if (::VirtualProtect(pStubCode->pProcPtr, 
		dwSize, PAGE_EXECUTE_READWRITE, &dwOldProtect) == 0)
	{
		return false;
	}
	
	memcpy(pStubCode->pProcPtr, pStubCode->data, dwSize);
	
	::VirtualProtect(pStubCode->pProcPtr, dwSize, dwOldProtect, &dwOldProtect);

	return true;
}

/************************************************************************/
/* CAPICodePatcher class implementation                                 */
/************************************************************************/

#pragma pack(push)
#pragma pack(1)

struct PATCH
{
	BYTE cmdJMP;
	LPVOID pOffs;
	void Apply()
	{
		cmdJMP = 0xE9;
	}
};

struct OLDAPI_ENTRY
{
	BYTE cbMachineCode[16];
	OLDAPI_ENTRY()
	{
//		Clear();
	}
	void Clear()
	{
		ZeroMemory(this, sizeof(*this));
	}
	bool IsFree() const
	{
		static OLDAPI_ENTRY zeroApi;
		return memcmp(this, &zeroApi, sizeof(zeroApi)) == 0;
	}
};

#pragma pack(pop)

static OLDAPI_ENTRY g_OldApiTable[0x1000];

CAPICodePatcher::CAPICodePatcher(LPCSTR pszDll, 
								 LPCSTR pszAPI, 
								 LPVOID pStubPtr, 
								 LPVOID* pOldProc, 
								 DWORD dwPatchSize) :
	m_hDll(NULL), m_pProc(NULL),
	m_pOldProc(NULL), m_dwPatchSize(dwPatchSize)
{
	if (dwPatchSize > sizeof(OLDAPI_ENTRY) - sizeof(PATCH))
	{
		return;
	}

	if ((m_hDll = ::LoadLibraryA(pszDll)) == NULL)
	{
		return;
	}

	if ((m_pProc = ::GetProcAddress(m_hDll, pszAPI)) == NULL)
	{
		return;
	}

	// Find free old API slot...
	OLDAPI_ENTRY* p = g_OldApiTable;
	OLDAPI_ENTRY* q = p + sizeof(g_OldApiTable) / sizeof(g_OldApiTable[0]); 
	for (; p < q; p++)
	{
		if (p->IsFree())
		{
			m_pOldProc = p;
			if (pOldProc) *pOldProc = p;
			memcpy(p->cbMachineCode, m_pProc, dwPatchSize);
			PATCH* patch = (PATCH*)(p->cbMachineCode + dwPatchSize);
			patch->pOffs = (LPBYTE)m_pProc + dwPatchSize;
			patch->Apply();
			DWORD dwOldProtect = 0;
			::VirtualProtect(m_pProc, dwPatchSize, PAGE_READWRITE, &dwOldProtect);
			patch = (PATCH*)m_pProc;
			patch->pOffs = pStubPtr;
			patch->Apply();
			::VirtualProtect(m_pProc, dwPatchSize, dwOldProtect, &dwOldProtect);
			return;
		}
	}
}

CAPICodePatcher::~CAPICodePatcher()
{
	DWORD dwOldProtect = 0;
	::VirtualProtect(m_pProc, m_dwPatchSize, PAGE_READWRITE, &dwOldProtect);
	memcpy(m_pProc, m_pOldProc, m_dwPatchSize);
	::VirtualProtect(m_pProc, m_dwPatchSize, dwOldProtect, &dwOldProtect);
	((OLDAPI_ENTRY*)m_pOldProc)->Clear();
}

}
