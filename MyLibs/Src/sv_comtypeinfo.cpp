/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include <windows.h>
#include <atlbase.h>
#include "sv_comtypeinfo.h"


namespace SVCOMTypeInfo {

CCOMMethodsInfo::CCOMMethodsInfo(IDispatch* pDisp)
{
	if (pDisp)
	{
		Init(pDisp);
	}
}

CCOMMethodsInfo::CCOMMethodsInfo(BSTR bstrProgId)
{
	Init(bstrProgId);
}

CCOMMethodsInfo::~CCOMMethodsInfo()
{
	Destroy();
}

HRESULT CCOMMethodsInfo::Init(IDispatch* pDisp)
{
	if (!pDisp)
	{
		return E_FAIL;
	}

	Destroy();

	ITypeInfo* pTypeInfo = NULL;
	HRESULT hr;

	try
	{
		if ((hr = pDisp->GetTypeInfo(0, GetUserDefaultLCID(), &pTypeInfo)) != S_OK)
		{
			throw hr;
		}

		if (pTypeInfo)
		{
			int i = 0;
			FUNCDESC* pfd = NULL;

			while ((hr = pTypeInfo->GetFuncDesc(i++, &pfd)) == S_OK)
			{
//				if (pfd->memid >= 0x60000000 && pfd->memid < 0x60010000)
//				{
//					continue;
//				}

				MethodInfo mi;

				BSTR* bstrNames = new BSTR[pfd->cParams + 1];
				if (!bstrNames)
				{
					throw E_OUTOFMEMORY;
				}
				ZeroMemory(bstrNames, sizeof(bstrNames) * (pfd->cParams + 1));

				UINT cNames = 0;
				if ((hr = pTypeInfo->GetNames(
					pfd->memid,
					bstrNames,
					pfd->cParams + 1,
					&cNames)) != S_OK)
				{
					throw hr;
				}

				mi.dwFlags = pfd->invkind;
				mi.dwRetValType = pfd->elemdescFunc.tdesc.vt & VT_TYPEMASK;
				if (mi.dwRetValType == VT_PTR)
				{
					mi.dwRetValSubType = pfd->elemdescFunc.tdesc.lptdesc->vt;
				}
				else
				{
					mi.dwRetValSubType = 0;
				}
				mi.sName = bstrNames[0];
				mi.memid = pfd->memid;

				if (mi.sName != L"AddRef" &&
					mi.sName != L"Release" &&
					mi.sName != L"QueryInterface" &&
					mi.sName != L"GetIDsOfNames" &&
					mi.sName != L"Invoke" &&
					mi.sName != L"GetTypeInfo" &&
					mi.sName != L"GetTypeInfoCount")
				{
					for (int i = 0; i < pfd->cParams; i++)
					{
						ParamInfo pi;

						pi.dwFlags = pfd->lprgelemdescParam[i].paramdesc.wParamFlags;
						pi.dwType = pfd->lprgelemdescParam[i].tdesc.vt & VT_TYPEMASK;
						if (pi.dwType == VT_PTR)
						{
							pi.dwSubType = pfd->lprgelemdescParam[i].tdesc.lptdesc->vt;
						}
						else
						{
							pi.dwSubType = 0;
						}
						pi.sName = bstrNames[i + 1] ? bstrNames[i + 1] : L"";

						mi.Params.push_back(pi);
					}

					MethodInfo* pmi = new MethodInfo(mi);
					if (!pmi)
					{
						throw E_OUTOFMEMORY;
					}
					m_Methods.push_back(pmi);
				}

				for (UINT i = 0; i < cNames; i++)
				{
					SysFreeString(bstrNames[i]);
				}
				delete bstrNames;
			}

			pTypeInfo->Release();
		}
	}
	catch (HRESULT hr)
	{
		Destroy();
		return hr;
	}
	catch (...)
	{
		Destroy();
		return DISP_E_EXCEPTION;
	}
	
	return S_OK;
}

HRESULT CCOMMethodsInfo::Init(BSTR bstrProgId)
{
	CComPtr<IDispatch> obj;
	HRESULT hr = obj.CoCreateInstance(bstrProgId);
	if (hr != S_OK)
	{
		return hr;
	}
	return Init(obj.p);
}

void CCOMMethodsInfo::Destroy()
{
	std::vector<MethodInfo*>::iterator it = m_Methods.begin();
	for (; it != m_Methods.end(); ++it)
	{
		MethodInfo* pmi = *it;
		std::vector<ParamInfo>::iterator it = pmi->Params.begin();
		delete pmi;
	}
	m_Methods.clear();
}

int CCOMMethodsInfo::GetMethodCount() const
{
	return (int)m_Methods.size();
}

const MethodInfo* CCOMMethodsInfo::GetMethodInfo(int nIndex) const
{
	if (nIndex < 0 || nIndex > (int)m_Methods.size() - 1)
	{
		return NULL;
	}
	return m_Methods[nIndex];
}

std::wstring MethodInfo::GetDesc(int /*nIndex*/, bool bIDLstyle) const
{
	struct TypeToString {
		static std::wstring Do(WORD vt)
		{
			switch (vt)
			{
				case VT_I1:					return L"CHAR";
				case VT_I2:					return L"SHORT";
				case VT_I4:					return L"LONG";
				case VT_I8:					return L"LONGLONG";
				case VT_INT:				return L"INT";
				case VT_UI1:				return L"BYTE";
				case VT_UI2:				return L"USHORT";
				case VT_UI4:				return L"ULONG";
				case VT_UI8:				return L"ULONGLONG";
				case VT_UINT:				return L"UINT";
				case VT_R4:					return L"FLOAT";
				case VT_R8:					return L"DOUBLE";
				case VT_CY:					return L"CY";
				case VT_DATE:				return L"DATE";
				case VT_BSTR:				return L"BSTR";
				case VT_BOOL:				return L"BOOL";
				case VT_SAFEARRAY:			return L"SAFEARRAY";
				case VT_DISPATCH:			return L"IDispatch*";
				case VT_VARIANT:			return L"VARIANT";
				case VT_HRESULT:			return L"HRESULT";
				case VT_VOID:				return L"VOID";
				case VT_PTR:				return L"PVOID";
				case VT_ERROR:				return L"ERROR";
				case VT_UNKNOWN:			return L"UNKNOWN";
				case VT_DECIMAL:			return L"DECIMAL";
				case VT_CARRAY:				return L"CARRAY";
				case VT_USERDEFINED:		return L"USERDEFINED";
				case VT_LPSTR:				return L"LPSTR";
				case VT_LPWSTR:				return L"LPWSTR";
				case VT_RECORD:				return L"RECORD";
				case VT_INT_PTR:			return L"INT*";
				case VT_UINT_PTR:			return L"UINT*";
				case VT_FILETIME:			return L"FILETIME";
				case VT_BLOB:				return L"BLOB";
				case VT_STREAM:				return L"STREAM";
				case VT_STORAGE:			return L"STORAGE";
				case VT_STREAMED_OBJECT:	return L"STREAMED_OBJECT";
				case VT_STORED_OBJECT:		return L"STORED_OBJECT";
				case VT_BLOB_OBJECT:		return L"BLOB_OBJECT";
				case VT_CF:					return L"CF";
				case VT_CLSID:				return L"CLSID";
				case VT_VERSIONED_STREAM:	return L"VERSIONED_STREAM";
			}
			WCHAR szBuf[16] = {0};
			swprintf(szBuf, L"<%d>", (int)vt);
			return std::wstring(szBuf);
		}
	};

	std::wstring desc;
	DWORD dwType = dwRetValType;
	DWORD dwSubType = dwRetValSubType;

	if (bIDLstyle)
	{
		desc = L"[";
		if (dwFlags & INVOKE_PROPERTYGET)
		{
			desc += L"propget, ";
		}
		else if (dwFlags & INVOKE_PROPERTYPUT)
		{
			desc += L"propput, ";
		}
		else if (dwFlags & INVOKE_PROPERTYPUTREF)
		{
			desc += L"propputref, ";
		}
		WCHAR szBuf[32];
		swprintf(szBuf, L"id(%d)] ", (int)memid);
		desc += szBuf;
	}
	else
	{
		for (int i = 0; i < (int)Params.size(); i++)
		{
			if (Params[i].dwFlags & PARAMFLAG_FRETVAL)
			{
				dwType = Params[i].dwSubType;
				dwSubType = 0;
				break;
			}
		}
	}
	
	if (dwType == VT_PTR)
	{
		desc += TypeToString::Do((WORD)dwSubType) + L"*";
	}
	else
	{
		desc += TypeToString::Do((WORD)dwType);
	}
	desc += L" ";
	desc += sName;

	bool bProp = 
		!bIDLstyle && (
		(dwFlags & INVOKE_PROPERTYGET) ||
		(dwFlags & INVOKE_PROPERTYPUT) ||
		(dwFlags & INVOKE_PROPERTYPUTREF));

	desc += bProp ? L"[" : L"(";

	for (int i = 0; i < (int)Params.size(); i++)
	{
		const ParamInfo& pi = Params[i];
		if (bIDLstyle || !(pi.dwFlags & PARAMFLAG_FRETVAL))
		{
			if (i)
			{
				desc += L", ";
			}

			std::wstring t;
			if (pi.dwFlags & PARAMFLAG_FIN)
			{
				t = L"in";
			}
			if (pi.dwFlags & PARAMFLAG_FOUT)
			{
				t += t.empty() ? L"out" : L",out";
			}
			if (pi.dwFlags & PARAMFLAG_FRETVAL)
			{
				t += t.empty() ? L"retval" : L",retval";
			}
			if (pi.dwFlags & PARAMFLAG_FOPT)
			{
				t += t.empty() ? L"optional" : L",optional";
			}
			if (!t.empty())
			{
				desc += L"[" + t + L"] ";
			}

			if (pi.dwType == VT_PTR)
			{
				desc += TypeToString::Do((WORD)pi.dwSubType) + L"*";
			}
			else
			{
				desc += TypeToString::Do((WORD)pi.dwType);
			}
			if (!pi.sName.empty())
			{
				desc += L" ";
				desc += pi.sName;
			}
		}
	}

	desc += bProp ? L"]" : L")";

	return desc;
}

}
