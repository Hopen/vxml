/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include "Common.h"
#include <windows.h>
#include "sv_exception.h"
#include "sv_threadpool.h"

#define IOCP_SHUTDOWN		0xFFFF
#define IOCP_KEY_JOB		0xFAFA
#define IOCP_OVRL_JOB		0xFEFD

using namespace SVException;
using namespace SVHandle;


namespace SVThreadPool
{

CThreadPool::CThreadPool() :
	m_nWorkerThreads(0),
	m_nBusyThreads(0),
	m_dwThreadsMin(2),
	m_dwThreadsMax(8),
	m_dwCPUHiThreshold(90),
	m_dwCPULoThreshold(75)
{
}

CThreadPool::~CThreadPool()
{
	Shutdown();
}

bool CThreadPool::Initialize()
{
	if (m_hCompletitionPort)
	{
		return false;
	}

	m_hExitEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);

	SYSTEM_INFO si;
	::GetSystemInfo(&si);

	m_hCompletitionPort = ::CreateIoCompletionPort(
		INVALID_HANDLE_VALUE, 
		NULL, 
		NULL, 
		2 * si.dwNumberOfProcessors);

	if (!m_hCompletitionPort)
	{
		THROW_LASTERR;
	}

	ResizePool(2 * si.dwNumberOfProcessors);

	SYSTEMTIME st;
	FILETIME huj;
	GetLocalTime(&st);
	SystemTimeToFileTime(&st, &m_ftOldTime);
	GetProcessTimes(GetCurrentProcess(), &huj, &huj, &huj, &m_ftOldCPUTime);

	return true;
}

bool CThreadPool::Shutdown()
{
	if (!m_hCompletitionPort)
	{
		return false;
	}

	DWORD dwWorkerThreads = m_nWorkerThreads;

	while (m_nWorkerThreads != 0) 
	{
		::ResetEvent(m_hExitEvent);
		::PostQueuedCompletionStatus(
			m_hCompletitionPort, 
			0, 
			NULL, 
			(LPOVERLAPPED)IOCP_SHUTDOWN);
		::WaitForSingleObject(m_hExitEvent, INFINITE);					
	};

	m_hCompletitionPort.Close();
	m_hExitEvent.Close();

	return true;
}

void CThreadPool::ResizePool(LONG nThreadCount)
{
	LONG nWorkerThreads = m_nWorkerThreads;

	if (nWorkerThreads > nThreadCount)
	{
		for (LONG i = 0; i < nWorkerThreads - nThreadCount; i++)
		{
			::PostQueuedCompletionStatus(
				m_hCompletitionPort, 
				0, 
				NULL, 
				(LPOVERLAPPED)IOCP_SHUTDOWN);
		}
		return;
	}
	else if (nWorkerThreads < nThreadCount)
	{
		for (LONG i = 0; i < nThreadCount - nWorkerThreads; i++)
		{
			AddWorkerThread();
		}
	}
}

void CThreadPool::AddWorkerThread()
{
	DWORD dwThreadId;
	CHandle hThread(::CreateThread(
		NULL, 
		0, 
		(LPTHREAD_START_ROUTINE)ThreadFunc,
		this,
		0, 
		&dwThreadId));
	if (!hThread)
	{
		THROW_LASTERR;
	}
}

bool CThreadPool::ExecuteJob(CThreadPoolJob* pJob)
{
	if (!m_hCompletitionPort)
	{
		return false;
	}

	ULONG_PTR dwKey = IOCP_KEY_JOB;
	CHECK_OSERROR(::PostQueuedCompletionStatus(
		m_hCompletitionPort, 
		0, 
		dwKey, 
		(LPOVERLAPPED)pJob));

	return true;
}

bool CThreadPool::ExecuteJob(COverlappedJob* pJob, HANDLE hWatchHandle)
{
	if (!m_hCompletitionPort)
	{
		return false;
	}

	ULONG_PTR dwKey = IOCP_OVRL_JOB;

	CHECK_OSERROR(::CreateIoCompletionPort(
		hWatchHandle, 
		m_hCompletitionPort, 
		dwKey, 
		0));

	if (pJob)
	{
		try
		{
			pJob->pHandler->BeginIO();
		}
		catch (...)
		{
		}
	}

	return true;
}

void CThreadPool::ProcessJob(CThreadPoolJob* pJob)
{
	try
	{
		pJob->Execute();
	}
	catch (...)
	{
	}

	if (pJob->CanDeleteJob())
	{
		delete pJob;
	}
}

void CThreadPool::ProcessJob(COverlappedJob* pJob)
{
	try
	{
		pJob->pHandler->OnIOCompleted();
	}
	catch (...)
	{
	}

	if (pJob->pHandler->CanDeleteJob())
	{
		delete pJob;
	}
}

DWORD CThreadPool::ThreadPoolFunc()
{
	DWORD		dwBytesTransfered;
	ULONG		dwCompletionKey;
	OVERLAPPED*	pOverlapped;

	// ����� ���������� � ���
	InterlockedIncrement(&m_nWorkerThreads);
	InterlockedIncrement(&m_nBusyThreads);

	for (bool bStayInPool = true; bStayInPool;)
	{
		// ����� ���������� ���������� � �������, ����� ��� ����� ��� ������
		InterlockedDecrement(&m_nBusyThreads);
		
		BOOL bOk = GetQueuedCompletionStatus(
			m_hCompletitionPort, 
			&dwBytesTransfered, 
			&dwCompletionKey, 
			&pOverlapped, 
			INFINITE);

		DWORD dwErr = GetLastError();

		// ������ ���� ��� ������, ��� ��� �� �����
		int nBusyThreads = InterlockedIncrement(&m_nBusyThreads);
		int nWorkerThreads = m_nWorkerThreads;

		// ������� �� ��� �������� ��� ���� ����� � ���?
		if (nBusyThreads == m_nWorkerThreads)		// ��� ������ ������
		{
			if (nBusyThreads < m_dwThreadsMax)	// ��� �� ��������
			{
				// ��������� ������������ �����, ��� �� m_nCPULoThreshold %
				if (GetCPUUsage() < m_dwCPULoThreshold)
				{
					// ��������� ����� � ���
					ResizePool(m_nWorkerThreads + 1);
				}
			}
		}

		if (!bOk || pOverlapped == (LPOVERLAPPED)IOCP_SHUTDOWN)
		{
			break;
		}

		switch (dwCompletionKey)
		{
			case IOCP_KEY_JOB:
				{
					CThreadPoolJob* pJob = (CThreadPoolJob*)pOverlapped;
					ProcessJob(pJob);
				}
				break;

			case IOCP_OVRL_JOB:
				{
					COverlappedJob* pJob = (COverlappedJob*)pOverlapped;
					ProcessJob(pJob);
				}
				break;
		}

		if (GetCPUUsage() > m_dwCPUHiThreshold && 
			m_nWorkerThreads > m_dwThreadsMin)		// ��� ������ ������������
		{
			// ������� ����� �� ����
			bStayInPool = false;
		}
	}

	InterlockedDecrement(&m_nBusyThreads);
	InterlockedDecrement(&m_nWorkerThreads);
	::SetEvent(m_hExitEvent);

	return 0;
}

DWORD CThreadPool::GetCPUUsage()
{
	return 80;

	FILETIME ftTime, ftCPUTime;
	FILETIME huj;
	SYSTEMTIME st;

	GetLocalTime(&st);
	SystemTimeToFileTime(&st, &ftTime);
	GetProcessTimes(GetCurrentProcess(), &huj, &huj, &huj, &ftCPUTime);

	// ���, ���, �������� ������� �������, �.�. ������� �� 
	// ������������� �� ������ ��� �������� ��������.
	ULONGLONG ullCPUUsage = 
		(*(PULONGLONG)&ftCPUTime - *(PULONGLONG)&m_ftOldCPUTime) / 
		(*(PULONGLONG)&ftTime - *(PULONGLONG)&m_ftOldTime);

	m_ftOldTime = ftTime;
	m_ftOldCPUTime = ftCPUTime;

	return ullCPUUsage;
}

}
