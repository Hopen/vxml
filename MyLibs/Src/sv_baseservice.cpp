/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include <windows.h>
#include "sv_service.h"
#include "sv_utils.h"

int BaseSvcStartup(SVService::CNTService* pService, int argc, wchar_t* argv[])
{
	SVService::CServiceDispatcher svcDisp;
	svcDisp.AddService(pService->GetServiceName(), pService);

	try
	{
		if (argc == 1)
		{
			if (!svcDisp.Start())
			{
				if (::GetLastError() == ERROR_FAILED_SERVICE_CONTROLLER_CONNECT)
				{
					wprintf(
						L"The service must be started by SCM!"
						L"\nUse '/console' option to run it as a console process.\n");
				}
				else
				{
					wprintf(L"Cannot start service\n");
				}
				return -1;
			}
		}
		else if (argc == 2)
		{
			const wchar_t* pArg = argv[1];
			if (pArg[0] == '-' || pArg[0] == '/')
			{
				pArg++;
				if (!_wcsicmp(pArg, L"install"))
				{
					wprintf(L"Installing service...\n");
					pService->Install();
					wprintf(L"Service installed\n");
					return 0;
				}
				else if (!_wcsicmp(pArg, L"uninstall"))
				{
					wprintf(L"Uninstalling service...\n");
					pService->Uninstall();
					wprintf(L"Service uninstalled\n");
					return 0;
				}
				else if (!_wcsicmp(pArg, L"start"))
				{
					wprintf(L"Starting service...\n");
					pService->Start();
					wprintf(L"Service started\n");
					return 0;
				}
				else if (!_wcsicmp(pArg, L"stop"))
				{
					wprintf(L"Stopping service...\n");
					pService->Stop();
					wprintf(L"Service stopped\n");
					return 0;
				}
				else if (!_wcsicmp(pArg, L"console"))
				{
					svcDisp.StartConsole();
					return 0;
				}
				else if (!_wcsicmp(pArg, L"?"))
				{
				}
				else
				{
					wprintf(L"Invalid option \"%s\"\n", argv[1]);
				}
			}
			else
			{
				wprintf(L"Invalid option \"%s\"\n", argv[1]);
			}
		}

		wprintf(
			L"\n%s [(/|-)(install|uninstall|start|stop|console)]\n\n"
			L"/install      Install the service\n"
			L"/uninstall    Uninstall the service\n"
			L"/start        Start the service\n"
			L"/stop         Stop the service\n"
			L"/console      Run the service as a console process\n",
			SVUtils::ExtractFileName(argv[0]).c_str()
		);
	}
	catch (...)
	{
		return -1;
	}
		
	return 0;
}
