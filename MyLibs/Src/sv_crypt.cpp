/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include "sv_crypt.h"
#include <memory.h>

#define ROTATE_LEFT(x, n)		(((x) << (n)) | ((x) >> (32-(n))))
typedef unsigned long word;

namespace SVCrypt
{

#pragma warning(push)
#pragma warning(disable:4100)	// warning C4100: 'xxx' : unreferenced formal parameter

unsigned long LitEnd2BigEnd(unsigned long value)
{
	__asm
	{
		mov		eax,	[ebp + 8]	// value
		xchg	al,		ah
		ror		eax,	16
		xchg	al,		ah
	}
}

#pragma warning(pop)

void SHA1(const void* pData, unsigned nSize, void* pHash)
{
	const word K[] = {
		0x5A827999, 
		0x6ED9EBA1, 
		0x8F1BBCDC, 
		0xCA62C1D6
	};

	word t, TEMP;
	word W[80];
	word A, B, C, D, E;
	word H[5] = {
		0x67452301,
		0xEFCDAB89,
		0x98BADCFE,
		0x10325476,
		0xC3D2E1F0
	};

	int nBlockCount = (nSize + 63) / 64;

	if (nSize == 0)
	{
		nBlockCount++;
	}

	int nRem = nBlockCount * 64 - nSize;

	if (nRem > 0 && nRem < 9)
	{
		nBlockCount++;
	}

	const unsigned char* p = (const unsigned char*)pData;
	unsigned char* q = (unsigned char*)W;

	for (int i = 0, k = nSize; i < nBlockCount; i++)
	{
		int n = k > 64 ? 64 : k;

		if (k > 0)
		{
			memcpy(W, p, n);
		}

		if (k < 0)
		{
			memset(q, 0, 64);
		}
		else if (k < 64)
		{
			q[k] = 0x80;
			memset(q + k + 1, 0, 64 - k - 1);
		}

		k -= 64;
		p += 64;

		//	a. Divide M(i) into 16 words W(0), W(1), ... , W(15), where W(0)
		//		is the left-most word.

		for (t = 0; t < 16; t++)
		{
			W[t] = LitEnd2BigEnd(W[t]);
		}

		if (i == nBlockCount - 1 && k != 0)
		{
			W[14] = nSize >> 29;
			W[15] = nSize << 3;
		}

		//	b. For t = 16 to 79 let
		//		W(t) = S^1(W(t-3) XOR W(t-8) XOR W(t-14) XOR W(t-16)).

		for (t = 16; t < 80; t++)
		{
			TEMP = W[t - 3] ^ W[t - 8] ^ W[t - 14] ^ W[t - 16];
			W[t] = ROTATE_LEFT(TEMP, 1);
		}

		//	c. Let A = H0, B = H1, C = H2, D = H3, E = H4.

		A = H[0], B = H[1], C = H[2], D = H[3], E = H[4];

		//	d. For t = 0 to 79 do
		//	TEMP = S^5(A) + f(t;B,C,D) + E + W(t) + K(t);
		//	E = D;  D = C;  C = S^30(B);  B = A; A = TEMP;

		//	f(t;B,C,D) = (B AND C) OR ((NOT B) AND D)         ( 0 <= t <= 19)
		//	f(t;B,C,D) = B XOR C XOR D                        (20 <= t <= 39)
		//	f(t;B,C,D) = (B AND C) OR (B AND D) OR (C AND D)  (40 <= t <= 59)
		//	f(t;B,C,D) = B XOR C XOR D                        (60 <= t <= 79)

		for (t = 0; t < 20; t++)
		{
			TEMP = ROTATE_LEFT(A, 5) + ((B & C) | ((~B) & D)) + E + W[t] + K[0];
			E = D; D = C; C = ROTATE_LEFT(B, 30); B = A; A = TEMP;
		}
		for (; t < 40; t++)
		{
			TEMP = ROTATE_LEFT(A, 5) + (B ^ C ^ D) + E + W[t] + K[1];
			E = D; D = C; C = ROTATE_LEFT(B, 30); B = A; A = TEMP;
		}
		for (; t < 60; t++)
		{
			TEMP = ROTATE_LEFT(A, 5) + ((B & C) | (B & D) | (C & D)) + E + W[t] + K[2];
			E = D; D = C; C = ROTATE_LEFT(B, 30); B = A; A = TEMP;
		}
		for (; t < 80; t++)
		{
			TEMP = ROTATE_LEFT(A, 5) + (B ^ C ^ D) + E + W[t] + K[3];
			E = D; D = C; C = ROTATE_LEFT(B, 30); B = A; A = TEMP;
		}

		//	e. Let H0 = H0 + A, H1 = H1 + B, H2 = H2 + C, H3 = H3 + D, H4 = H4 + E

		H[0] += A, H[1] += B, H[2] += C, H[3] += D, H[4] += E;
	}

	for (t = 0; t < 5; t++)
	{
		H[t] = LitEnd2BigEnd(H[t]);
	}

	memcpy(pHash, H, sizeof(H));
}

void MD5(const void* pData, unsigned nSize, void* pHash)
{
	/* F, G, H and I are basic MD5 functions.
	*/
#define F(x, y, z) (((x) & (y)) | ((~x) & (z)))
#define G(x, y, z) (((x) & (z)) | ((y) & (~z)))
#define H(x, y, z) ((x) ^ (y) ^ (z))
#define I(x, y, z) ((y) ^ ((x) | (~z)))

	/* FF, GG, HH, and II transformations for rounds 1, 2, 3, and 4.
	Rotation is separate from addition to prevent recomputation.
	*/
#define FF(a, b, c, d, x, s, ac) { \
	(a) += F ((b), (c), (d)) + (x) + (unsigned long)(ac); \
	(a) = ROTATE_LEFT ((a), (s)); \
	(a) += (b); \
}
#define GG(a, b, c, d, x, s, ac) { \
	(a) += G ((b), (c), (d)) + (x) + (unsigned long)(ac); \
	(a) = ROTATE_LEFT ((a), (s)); \
	(a) += (b); \
}
#define HH(a, b, c, d, x, s, ac) { \
	(a) += H ((b), (c), (d)) + (x) + (unsigned long)(ac); \
	(a) = ROTATE_LEFT ((a), (s)); \
	(a) += (b); \
}
#define II(a, b, c, d, x, s, ac) { \
	(a) += I ((b), (c), (d)) + (x) + (unsigned long)(ac); \
	(a) = ROTATE_LEFT ((a), (s)); \
	(a) += (b); \
}

	/* Constants for MD5Transform routine.
	*/
#define S11 7
#define S12 12
#define S13 17
#define S14 22
#define S21 5
#define S22 9
#define S23 14
#define S24 20
#define S31 4
#define S32 11
#define S33 16
#define S34 23
#define S41 6
#define S42 10
#define S43 15
#define S44 21

	word X[16];
	word A, B, C, D;
	word H[4] = {
		0x67452301,
		0xEFCDAB89,
		0x98BADCFE,
		0x10325476
	};

	int nBlockCount = (nSize + 63) / 64;

	if (nSize == 0)
	{
		nBlockCount++;
	}

	int nRem = nBlockCount * 64 - nSize;

	if (nRem < 9)
	{
		nBlockCount++;
	}

	const unsigned char* p = (const unsigned char*)pData;
	unsigned char* q = (unsigned char*)X;

	for (int i = 0, k = nSize; i < nBlockCount; i++)
	{
		int n = k > 64 ? 64 : k;

		if (k > 0)
		{
			memcpy(X, p, n);
		}

		if (k < 0)
		{
			memset(q, 0, 64);
		}
		else if (k < 64)
		{
			q[k] = 0x80;
			memset(q + k + 1, 0, 64 - k - 1);
		}

		k -= 64;
		p += 64;

		if (i == nBlockCount - 1)
		{
			X[15] = nSize >> 29;
			X[14] = nSize << 3;
		}

		A = H[0], B = H[1], C = H[2], D = H[3];

		/* Round 1 */
		FF (A, B, C, D, X[ 0], S11, 0xD76AA478); /* 1 */
		FF (D, A, B, C, X[ 1], S12, 0xE8C7B756); /* 2 */
		FF (C, D, A, B, X[ 2], S13, 0x242070DB); /* 3 */
		FF (B, C, D, A, X[ 3], S14, 0xC1BDCEEE); /* 4 */
		FF (A, B, C, D, X[ 4], S11, 0xF57C0FAF); /* 5 */
		FF (D, A, B, C, X[ 5], S12, 0x4787C62A); /* 6 */
		FF (C, D, A, B, X[ 6], S13, 0xA8304613); /* 7 */
		FF (B, C, D, A, X[ 7], S14, 0xFD469501); /* 8 */
		FF (A, B, C, D, X[ 8], S11, 0x698098D8); /* 9 */
		FF (D, A, B, C, X[ 9], S12, 0x8B44F7AF); /* 10 */
		FF (C, D, A, B, X[10], S13, 0xFFFF5BB1); /* 11 */
		FF (B, C, D, A, X[11], S14, 0x895CD7BE); /* 12 */
		FF (A, B, C, D, X[12], S11, 0x6B901122); /* 13 */
		FF (D, A, B, C, X[13], S12, 0xFD987193); /* 14 */
		FF (C, D, A, B, X[14], S13, 0xA679438E); /* 15 */
		FF (B, C, D, A, X[15], S14, 0x49B40821); /* 16 */
		
		/* Round 2 */
		GG (A, B, C, D, X[ 1], S21, 0xF61E2562); /* 17 */
		GG (D, A, B, C, X[ 6], S22, 0xC040B340); /* 18 */
		GG (C, D, A, B, X[11], S23, 0x265E5A51); /* 19 */
		GG (B, C, D, A, X[ 0], S24, 0xE9B6C7AA); /* 20 */
		GG (A, B, C, D, X[ 5], S21, 0xD62F105D); /* 21 */
		GG (D, A, B, C, X[10], S22, 0x2441453); /* 22 */
		GG (C, D, A, B, X[15], S23, 0xD8A1E681); /* 23 */
		GG (B, C, D, A, X[ 4], S24, 0xE7D3FBC8); /* 24 */
		GG (A, B, C, D, X[ 9], S21, 0x21E1CDE6); /* 25 */
		GG (D, A, B, C, X[14], S22, 0xC33707D6); /* 26 */
		GG (C, D, A, B, X[ 3], S23, 0xF4D50D87); /* 27 */		
		GG (B, C, D, A, X[ 8], S24, 0x455A14ED); /* 28 */
		GG (A, B, C, D, X[13], S21, 0xA9E3E905); /* 29 */
		GG (D, A, B, C, X[ 2], S22, 0xFCEFA3F8); /* 30 */
		GG (C, D, A, B, X[ 7], S23, 0x676F02D9); /* 31 */
		GG (B, C, D, A, X[12], S24, 0x8D2A4C8A); /* 32 */
		
		/* Round 3 */
		HH (A, B, C, D, X[ 5], S31, 0xFFFA3942); /* 33 */
		HH (D, A, B, C, X[ 8], S32, 0x8771F681); /* 34 */
		HH (C, D, A, B, X[11], S33, 0x6D9D6122); /* 35 */
		HH (B, C, D, A, X[14], S34, 0xFDE5380C); /* 36 */
		HH (A, B, C, D, X[ 1], S31, 0xA4BEEA44); /* 37 */
		HH (D, A, B, C, X[ 4], S32, 0x4BDECFA9); /* 38 */
		HH (C, D, A, B, X[ 7], S33, 0xF6BB4B60); /* 39 */
		HH (B, C, D, A, X[10], S34, 0xBEBFBC70); /* 40 */
		HH (A, B, C, D, X[13], S31, 0x289B7EC6); /* 41 */
		HH (D, A, B, C, X[ 0], S32, 0xEAA127FA); /* 42 */
		HH (C, D, A, B, X[ 3], S33, 0xD4EF3085); /* 43 */
		HH (B, C, D, A, X[ 6], S34, 0x4881D05); /* 44 */
		HH (A, B, C, D, X[ 9], S31, 0xD9D4D039); /* 45 */
		HH (D, A, B, C, X[12], S32, 0xE6DB99E5); /* 46 */
		HH (C, D, A, B, X[15], S33, 0x1FA27CF8); /* 47 */
		HH (B, C, D, A, X[ 2], S34, 0xC4AC5665); /* 48 */
		
		/* Round 4 */
		II (A, B, C, D, X[ 0], S41, 0xF4292244); /* 49 */
		II (D, A, B, C, X[ 7], S42, 0x432AFF97); /* 50 */
		II (C, D, A, B, X[14], S43, 0xAB9423A7); /* 51 */
		II (B, C, D, A, X[ 5], S44, 0xFC93A039); /* 52 */
		II (A, B, C, D, X[12], S41, 0x655B59C3); /* 53 */
		II (D, A, B, C, X[ 3], S42, 0x8F0CCC92); /* 54 */
		II (C, D, A, B, X[10], S43, 0xFFEFF47D); /* 55 */
		II (B, C, D, A, X[ 1], S44, 0x85845DD1); /* 56 */
		II (A, B, C, D, X[ 8], S41, 0x6FA87E4F); /* 57 */
		II (D, A, B, C, X[15], S42, 0xFE2CE6E0); /* 58 */
		II (C, D, A, B, X[ 6], S43, 0xA3014314); /* 59 */
		II (B, C, D, A, X[13], S44, 0x4E0811A1); /* 60 */
		II (A, B, C, D, X[ 4], S41, 0xF7537E82); /* 61 */
		II (D, A, B, C, X[11], S42, 0xBD3AF235); /* 62 */
		II (C, D, A, B, X[ 2], S43, 0x2AD7D2BB); /* 63 */
		II (B, C, D, A, X[ 9], S44, 0xEB86D391); /* 64 */

		H[0] += A, H[1] += B, H[2] += C, H[3] += D;
	}

	memcpy(pHash, H, sizeof(H));

#undef F
#undef G
#undef H
#undef I

#undef FF
#undef GG
#undef HH
#undef II

#undef S11
#undef S12
#undef S13
#undef S14
#undef S21
#undef S22
#undef S23
#undef S24
#undef S31
#undef S32
#undef S33
#undef S34
#undef S41
#undef S42
#undef S43
#undef S44
}

}

#undef SHIFT
