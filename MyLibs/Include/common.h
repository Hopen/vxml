/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#pragma warning(disable:4786)
#pragma warning(disable:4163)

#define PRINTMSG(msg)			message(__FILE__ "(" CONST_TO_STRING(__LINE__) ") : " msg)
#define TODO(msg)				PRINTMSG("ToDo at " ##__FUNCTION__ "(): " msg)

#define LODWORD(X)				DWORD(X & 0xFFFFFFFF)
#define HIDWORD(X)				DWORD(X >> 32)
#define MAKEQWORD(L,H)			(((ULONGLONG)H << 32) | L)
