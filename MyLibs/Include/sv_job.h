/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include <vector>
#include "sv_handle.h"

namespace SVJob
{

/************************************************************************/
/* Rm's job classes                                                     */
/************************************************************************/

class CJob;
class CJobNotify;

// CJobUIRestrictions

class CJobUIRestrictions : protected JOBOBJECT_BASIC_UI_RESTRICTIONS
{
	friend CJob;

private:
	bool GetFlag(DWORD Flag) { return (UIRestrictionsClass & Flag) == Flag; }
	void SetFlag(DWORD Flag,bool bEnable)
	{
		if(bEnable)
		{
			this->UIRestrictionsClass |= Flag;
		}
		else
		{
			this->UIRestrictionsClass &= ~Flag;
		}
	}

	__declspec(property(get=GetFlag,put=SetFlag)) bool Flag[];

public:
	CJobUIRestrictions() { this->UIRestrictionsClass = 0; }
	CJobUIRestrictions(const CJobUIRestrictions& other) { *this = other; }

	bool GetLimitHandles()						{ return Flag[JOB_OBJECT_UILIMIT_HANDLES];						}
	void SetLimitHandles(bool bEnable)			{		 Flag[JOB_OBJECT_UILIMIT_HANDLES] = bEnable;			}

	bool GetLimitReadClipboard()				{ return Flag[JOB_OBJECT_UILIMIT_READCLIPBOARD];				}
	void SetLimitReadClipboard(bool bEnable)	{		 Flag[JOB_OBJECT_UILIMIT_READCLIPBOARD] = bEnable;		}

	bool GetLimitWriteClipboard()				{ return Flag[JOB_OBJECT_UILIMIT_WRITECLIPBOARD];				}
	void SetLimitWriteClipboard(bool bEnable)	{		 Flag[JOB_OBJECT_UILIMIT_WRITECLIPBOARD] = bEnable;		}

	bool GetLimitSystemParameters()				{ return Flag[JOB_OBJECT_UILIMIT_SYSTEMPARAMETERS];				}
	void SetLimitSystemParameters(bool bEnable)	{		 Flag[JOB_OBJECT_UILIMIT_SYSTEMPARAMETERS] = bEnable;	}

	bool GetLimitDisplaySettings()				{ return Flag[JOB_OBJECT_UILIMIT_DISPLAYSETTINGS];				}
	void SetLimitDisplaySettings(bool bEnable)	{		 Flag[JOB_OBJECT_UILIMIT_DISPLAYSETTINGS] = bEnable;	}

	bool GetLimitGlobalAtoms()					{ return Flag[JOB_OBJECT_UILIMIT_GLOBALATOMS];					}
	void SetLimitGlobalAtoms(bool bEnable)		{		 Flag[JOB_OBJECT_UILIMIT_GLOBALATOMS] = bEnable;		}

	bool GetLimitDesktop()						{ return Flag[JOB_OBJECT_UILIMIT_DESKTOP];						}
	void SetLimitDesktop(bool bEnable)			{		 Flag[JOB_OBJECT_UILIMIT_DESKTOP] = bEnable;			}

	bool GetLimitExitWindows()					{ return Flag[JOB_OBJECT_UILIMIT_EXITWINDOWS];					}
	void SetLimitExitWindows(bool bEnable)		{		 Flag[JOB_OBJECT_UILIMIT_EXITWINDOWS] = bEnable;		}
};

// CJobLimits

class CJobLimits : private JOBOBJECT_EXTENDED_LIMIT_INFORMATION
{
	friend CJob;

public:
	bool GetLimitFlag(DWORD Flag)
	{
		return (this->BasicLimitInformation.LimitFlags & Flag) == Flag;
	}
	void SetLimitFlag(DWORD Flag,bool bEnable) 
	{
		if (bEnable)
		{
			this->BasicLimitInformation.LimitFlags |= Flag;
		}
		else
		{
			this->BasicLimitInformation.LimitFlags &= ~Flag; 
		}
	}

private:
	__declspec(property(get=GetLimitFlag,put=SetLimitFlag)) bool LimitFlag[];

public:
	CJobLimits() { this->BasicLimitInformation.LimitFlags = 0; }
	CJobLimits(const CJobUIRestrictions& other) { *this = other; }

	LARGE_INTEGER	GetPerProcessUserTime();
	LARGE_INTEGER	GetPerJobUserTime();
	DWORD			GetLimitFlags();
	SIZE_T			GetMinWorkingSetSize();
	SIZE_T			GetMaxWorkingSetSize();
	DWORD			GetActiveProcessLimit();
	ULONG_PTR		GetAffinity();
	DWORD			GetPriority();
	DWORD			GetSchedulingClass();

	LARGE_INTEGER	SetPerProcessUserTime(LARGE_INTEGER liLimit);
	LARGE_INTEGER	SetPerJobUserTime(LARGE_INTEGER liLimit);
	SIZE_T			SetMinWorkingSetSize(SIZE_T dwVal);
	SIZE_T			SetMaxWorkingSetSize(SIZE_T dwVal);
	DWORD			SetActiveProcessLimit(DWORD dwLim);
	DWORD			SetPriority(DWORD dwPriority);
	ULONG_PTR		SetAffinity(ULONG_PTR pAffinity);
	DWORD			SetSchedulingClass(DWORD dwSC);

	SIZE_T			SetProcessMemoryLimit(SIZE_T liLimit);
	SIZE_T			SetJobMemoryLimit(SIZE_T liLimit);
	SIZE_T			GetProcessMemoryLimit();
	SIZE_T			GetJobMemoryLimit();
	SIZE_T			GetPeakProcessMemoryUsed();
	SIZE_T			GetPeakJobMemoryUsed();
};

// CJob class

class CJob
{
private:
	SVHandle::CHandle	m_hJob;
	SVHandle::CHandle	m_hThread;
	SVHandle::CHandle	m_hIOCP;
	CJobNotify*			m_pNotify;

protected:
	template <class T>
	bool SetInformation(JOBOBJECTINFOCLASS JobInfoClass, T* pJobInfo)
	{
		return ::SetInformationJobObject(m_hJob, JobInfoClass, pJobInfo, sizeof(T)) != 0;
	}

	template <class T>
	bool GetInformation(JOBOBJECTINFOCLASS JobInfoClass, T* pJobInfo)
	{
		DWORD dwReturnLength = 0;
		return
			(::QueryInformationJobObject(m_hJob, JobInfoClass, pJobInfo, sizeof(T), &dwReturnLength) != 0) &&
			(dwReturnLength == sizeof(T));
	}

	bool SetCompletionPort(HANDLE hIOCP, PVOID pvCompletionKey);

	static DWORD WINAPI CallbackThread(LPVOID pThis);
	void CallbackThread();

	bool Init();

	DWORD GetProcessIDCount();

public:
	CJob();
	CJob(LPCWSTR pwszName, LPSECURITY_ATTRIBUTES lpJobAttributes = NULL);
	~CJob();

	bool Create(LPCWSTR pwszName = NULL, LPSECURITY_ATTRIBUTES lpJobAttributes = NULL);
	bool Open(
		LPCWSTR	pwszName,
		DWORD	dwDesiredAccess = JOB_OBJECT_ALL_ACCESS,
		BOOL	bInheritHandles = FALSE);
	void Destroy();

	bool				AssignProcess(HANDLE hProcess);
	bool				Terminate(UINT uExitCode);

	CJobUIRestrictions	GetUIRestrictions();
	bool				SetUIRestrictions(CJobUIRestrictions Restr);

	CJobLimits			GetLimits();
	bool				SetLimits(CJobLimits& lim);

	bool				SetEndOfJobTimeInfo(DWORD dwFlags);

	std::vector<DWORD>	GetProcessIDList();

	CJobNotify*	GetNotifyHandler();
	CJobNotify*	SetNotifyHandler(CJobNotify* pNotify);

	HANDLE GetHandle() const
	{
		return m_hJob;
	}
};

// CJobNotify class

class CJobNotify
{
public:
	virtual void OnEndOfJodTime()						{}
	virtual void OnEndOfProcessTime(DWORD /*dwPID*/)	{}
	virtual void OnActiveProcessLimit()					{}
	virtual void OnActiveProcessZero()					{}
	virtual void OnNewProcess(DWORD /*dwPID*/)			{}
	virtual void OnExitProcess(DWORD /*dwPID*/)			{}
	virtual void OnProcessMemoryLimit(DWORD /*dwPID*/)	{}
	virtual void OnJobMemoryLimit(DWORD /*dwPID*/)		{}
	virtual void OnAbnormalExitProcess(DWORD /*dwPID*/)	{}
};

}
