/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

namespace SVInterceptors
{

struct STUB_CODE
{
	LPVOID	pProcPtr;
	LPCVOID	pStubPtr;
	BYTE	data[6];
};

LPVOID ModifyModuleAPIThunk(HMODULE hModule, 
							LPCSTR pszDllName, 
							LPCSTR pszAPIName, 
							LPVOID pNewThunk);

bool InstallStubCode(LPCSTR pszDllName, LPCSTR pszProcName, LPVOID pStubPtr, STUB_CODE* pStubCode);
bool InstallStubCode(LPVOID pProcPtr, LPVOID pStubPtr, STUB_CODE* pStubCode);
bool UninstallStubCode(const STUB_CODE* pStubCode);

class CAPICodePatcher
{
private:
	HMODULE	m_hDll;
	LPVOID	m_pProc;
	LPVOID	m_pOldProc;
	DWORD	m_dwPatchSize;

public:
	CAPICodePatcher(LPCSTR pszDll, LPCSTR pszAPI, LPVOID pStubPtr, LPVOID* pOldProc, DWORD dwPatchSize);
	~CAPICodePatcher();
};

}
