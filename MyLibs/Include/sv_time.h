/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include <time.h>
#include <oleauto.h>
#include <malloc.h>

#define SEC_INTERVAL	(ULONGLONG)10000000
#define DAY_SECONDS		(ULONGLONG)86400
#define DAY_INTERVAL	ULONGLONG(DAY_SECONDS * SEC_INTERVAL)


namespace SVTime
{

/************************************************************************/
/* ����� CTime                                                          */
/************************************************************************/

class CTime 
{
protected:
	FILETIME m_T;

public:
	inline CTime();
	inline CTime(const CTime& time);
	inline CTime(const FILETIME& time);
	inline CTime(DATE time);
	inline CTime(const SYSTEMTIME& time);
	inline CTime(const struct tm& time);
	inline CTime(const VARIANT& time);
	inline CTime(ULONGLONG time);

	inline operator DATE() const;
	inline operator const FILETIME&() const;
	inline operator const SYSTEMTIME() const;
	inline operator struct tm() const;
	inline operator VARIANT() const;
	inline operator ULONGLONG() const;

	inline const CTime& operator =(const CTime& time);
	inline const CTime& operator =(const FILETIME& time);
	inline const CTime& operator =(DATE time);
	inline const CTime& operator =(const SYSTEMTIME& time);
	inline const CTime& operator =(const struct tm& time);
	inline const CTime& operator =(const VARIANT& time);
	inline const CTime& operator =(ULONGLONG time);

	inline bool operator ==(const CTime& time) const;
	inline bool operator !=(const CTime& time) const;
	inline bool operator <(const CTime& time) const;
	inline bool operator >(const CTime& time) const;
	inline bool operator <=(const CTime& time) const;
	inline bool operator >=(const CTime& time) const;

	inline const CTime operator +(const CTime& time) const;
	inline const CTime operator -(const CTime& time) const;
	inline const CTime& operator +=(const CTime& time);
	inline const CTime& operator -=(const CTime& time);

	inline const CTime& Now();
	inline const CTime& Local();
	inline const CTime& System();
	inline const CTime& ToLocal();
	inline const CTime& ToSystem();

	inline void Invalidate();
	inline bool IsValid() const
	{
		return !m_T.dwHighDateTime && !m_T.dwLowDateTime;
	}
};

/************************************************************************/
/* ����� CDateTime                                                      */
/************************************************************************/

class CDateTime: public CTime
{
public:
	CDateTime(bool bGetLocalTime = false);
	CDateTime(const CTime& time);
	CDateTime(const FILETIME& time);
	CDateTime(DATE time);
	CDateTime(const SYSTEMTIME& time);
	CDateTime(const struct tm& time);
	CDateTime(const VARIANT& time);
	CDateTime(LPCSTR pszTime);
	CDateTime(LPCWSTR pszTime);
	CDateTime(
		long nYear, 
		long nMonth, 
		long nDay, 
		long nHour = 0, 
		long nMin = 0, 
		long nSec = 0,
		long nMSec = 0);
	CDateTime(ULONGLONG qwTime): CTime(qwTime) { }

	inline bool IsValid() const;

	inline int GetYear() const;
	inline int GetMonth() const;
	inline int GetDay() const;
	inline int GetHour() const;
	inline int GetMinute() const;
	inline int GetSecond() const;
	inline int GetMilliseconds() const;
	inline int GetDayOfWeek() const;
	inline int GetDayOfYear() const;

	inline void SetYear(int nYear);
	inline void SetMonth(int nMonth);
	inline void SetDay(int nDay);
	inline void SetHour(int nHour);
	inline void SetMinute(int nMinute);
	inline void SetSecond(int nSecond);
	inline void SetMilliseconds(int nMilliseconds);

	inline bool IsLeapYear() const;
	inline bool IsNoon() const;
	inline bool IsMidnight() const;
	inline bool IsAM() const;
	inline bool IsPM() const;

	inline bool SetDateTime(
		long nYear, 
		long nMonth, 
		long nDay, 
		long nHour, 
		long nMin, 
		long nSec,
		long nMSec = 0);
	inline bool SetDate(long nYear, long nMonth, long nDay);
	inline bool SetTime(long nHour, long nMin, long nSec, long nMSec = 0);

	inline bool ParseDateTime(
		const char* lpszDate, 
		DWORD dwFlags = 0, 
		LCID lcid = LANG_USER_DEFAULT);
	inline bool ParseDateTime(
		const wchar_t* lpszDate, 
		DWORD dwFlags = 0, 
		LCID lcid = LANG_USER_DEFAULT);

	inline std::wstring Format(
		DWORD dwFlags = 0, 
		LCID lcid = LANG_USER_DEFAULT) const;
	inline std::wstring Format(LPCWSTR lpszFormat) const;

	inline std::wstring FormatTime(
		LPCWSTR szFmt = NULL, 
		DWORD dwFlags = 0, 
		LCID lcid = LOCALE_USER_DEFAULT) const;

	inline std::wstring FormatDate(
		LPCWSTR szFmt = NULL, 
		DWORD dwFlags = 0, 
		LCID lcid = LOCALE_USER_DEFAULT) const;

	static inline CDateTime GetLocal();
	static inline CDateTime GetSystem();
};

/************************************************************************/
/* ��������������� ������� ��� ������ � ��������                        */
/************************************************************************/

inline ULONGLONG GetTickCountEx();

#ifndef __BORLANDC__
__declspec(naked)
inline ULONGLONG RDTSC()
{
	__asm rdtsc
	__asm ret
}
#endif

inline ULONGLONG GetPerfCounter();
inline ULONGLONG GetPerfFreq();
inline void VariantTimeToFileTime(DATE time, LPFILETIME pFt);
inline void FileTimeToVariantTime(const FILETIME* pFt, DATE* pTime);
inline FILETIME GetLocalFileTime();
inline FILETIME GetSystemFileTime();

/************************************************************************/
/* ���������� ������ CTime                                              */
/************************************************************************/

inline CTime::CTime()
{
	Invalidate();
}

inline CTime::CTime(const CTime& time)
{
	*this = time;
}

inline CTime::CTime(const FILETIME& time)
{
	*this = time;
}

inline CTime::CTime(DATE time)
{
	*this = time;
}

inline CTime::CTime(const SYSTEMTIME& time)
{
	*this = time;
}

inline CTime::CTime(const struct tm& time)
{
	*this = time;
}

inline CTime::CTime(const VARIANT& time)
{
	*this = time;
}

inline CTime::CTime(ULONGLONG time)
{
	*this = time;
}

inline CTime::operator DATE() const
{
	DATE time;
	FileTimeToVariantTime(&m_T, &time);
	return time;
}

inline CTime::operator const FILETIME&() const
{
	return m_T;
}

inline CTime::operator const SYSTEMTIME() const
{
	SYSTEMTIME st;
	::FileTimeToSystemTime(&m_T, &st);
	return st;
}

inline CTime::operator struct tm() const
{
	SYSTEMTIME st;
	::FileTimeToSystemTime(&m_T, &st);
	struct tm time;
	time.tm_year = st.wYear - 1900;
	time.tm_mon = st.wMonth - 1;
	time.tm_mday = st.wDay;
	time.tm_wday = st.wDayOfWeek;
	time.tm_hour = st.wHour;
	time.tm_min = st.wMinute;
	time.tm_sec = st.wSecond;
	time.tm_isdst = -1;
    ::mktime(&time);
	return time;
}

inline CTime::operator VARIANT() const
{
	VARIANT v;
	v.vt = VT_DATE;
	FileTimeToVariantTime(&m_T, &v.date);
	return v;
}

inline CTime::operator ULONGLONG() const
{
	return *PULONGLONG(&m_T);
}

inline const CTime& CTime::operator =(const CTime& time)
{
	m_T = time.m_T;
	return *this;
}

inline const CTime& CTime::operator =(const FILETIME& time)
{
	m_T = time;
	return *this;
}

inline const CTime& CTime::operator =(DATE time)
{
	VariantTimeToFileTime(time, &m_T);
	return *this;
}

inline const CTime& CTime::operator =(const SYSTEMTIME& time)
{
	::SystemTimeToFileTime(&time, &m_T);
	return *this;
}

inline const CTime& CTime::operator =(const struct tm& time)
{
	SYSTEMTIME st;
	st.wYear = time.tm_year + 1900;
	st.wMonth = time.tm_mon + 1;
	st.wDay = time.tm_mday;
	st.wDayOfWeek = time.tm_wday;
	st.wHour = time.tm_hour;
	st.wMinute = time.tm_min;
	st.wSecond = time.tm_sec;
	st.wMilliseconds = 0;
	::SystemTimeToFileTime(&st, &m_T);
	return *this;
}

inline const CTime& CTime::operator =(const VARIANT& time)
{
	if (time.vt != VT_DATE)
	{
		Invalidate();
	}
	else
	{
		VariantTimeToFileTime(time.date, &m_T);
	}
	return *this;
}

inline const CTime& CTime::operator =(ULONGLONG time)
{
	m_T = *LPFILETIME(&time);
	return *this;
}

bool inline CTime::operator ==(const CTime& time) const
{
	return *(PULONGLONG)&m_T == *(PULONGLONG)&time.m_T;
}

bool inline CTime::operator !=(const CTime& time) const
{
	return *(PULONGLONG)&m_T != *(PULONGLONG)&time.m_T;
}

bool inline CTime::operator <(const CTime& time) const
{
	return *(PULONGLONG)&m_T < *(PULONGLONG)&time.m_T;
}

bool inline CTime::operator >(const CTime& time) const
{
	return *(PULONGLONG)&m_T > *(PULONGLONG)&time.m_T;
}

bool inline CTime::operator <=(const CTime& time) const
{
	return *(PULONGLONG)&m_T <= *(PULONGLONG)&time.m_T;
}

bool inline CTime::operator >=(const CTime& time) const
{
	return *(PULONGLONG)&m_T >= *(PULONGLONG)&time.m_T;
}

inline const CTime CTime::operator +(const CTime& time) const
{
	return *(PULONGLONG)&m_T + *(PULONGLONG)&time.m_T;
}

inline const CTime CTime::operator -(const CTime& time) const
{
	return *(PULONGLONG)&m_T - *(PULONGLONG)&time.m_T;
}

inline const CTime& CTime::operator +=(const CTime& time)
{
	*(PULONGLONG)&m_T += *(PULONGLONG)&time.m_T;
	return *this;
}

inline const CTime& CTime::operator -=(const CTime& time)
{
	*(PULONGLONG)&m_T -= *(PULONGLONG)&time.m_T;
	return *this;
}

inline const CTime& CTime::Now()
{
	*PULONGLONG(&m_T) = GetTickCountEx();
	return *this;
}

inline const CTime& CTime::Local()
{
	SYSTEMTIME st;
	::GetLocalTime(&st);
	::SystemTimeToFileTime(&st, &m_T);
	return *this;
}

inline const CTime& CTime::System()
{
	SYSTEMTIME st;
	::GetSystemTime(&st);
	::SystemTimeToFileTime(&st, &m_T);
	return *this;
}

inline const CTime& CTime::ToLocal()
{
	FileTimeToLocalFileTime(&m_T, &m_T);
	return *this;
}

inline const CTime& CTime::ToSystem()
{
	LocalFileTimeToFileTime(&m_T, &m_T);
	return *this;
}

inline void CTime::Invalidate()
{
	m_T.dwHighDateTime = m_T.dwLowDateTime = 0;
}


/************************************************************************/
/* ���������� ������ CDateTime                                          */
/************************************************************************/

inline CDateTime::CDateTime(bool bGetLocalTime)
{
	if (bGetLocalTime)
	{
		Local();
	}
}

inline CDateTime::CDateTime(const CTime& time): CTime(time)
{
}

inline CDateTime::CDateTime(const FILETIME& time): CTime(time)
{
}

inline CDateTime::CDateTime(DATE time): CTime(time)
{
}

inline CDateTime::CDateTime(const SYSTEMTIME& time): CTime(time)
{
}

inline CDateTime::CDateTime(const struct tm& time): CTime(time)
{
}

inline CDateTime::CDateTime(const VARIANT& time): CTime(time)
{
}

inline CDateTime::CDateTime(LPCSTR pszTime)
{
	ParseDateTime(pszTime);
}

inline CDateTime::CDateTime(LPCWSTR pszTime)
{
	ParseDateTime(pszTime);
}

inline CDateTime::CDateTime(long nYear, 
							long nMonth, 
							long nDay, 
							long nHour, 
							long nMin, 
							long nSec,
							long nMSec)
{
	SetDateTime(nYear, nMonth, nDay, nHour, nMin, nSec, nMSec);
}

inline bool CDateTime::IsValid() const
{
	if (!m_T.dwLowDateTime && !m_T.dwHighDateTime)
	{
		return false;
	}
	return true;
}

inline int CDateTime::GetYear() const
{
	SYSTEMTIME st;
	::FileTimeToSystemTime(&m_T, &st);
	return st.wYear;
}

inline int CDateTime::GetMonth() const
{
	SYSTEMTIME st;
	::FileTimeToSystemTime(&m_T, &st);
	return st.wMonth;
}

inline int CDateTime::GetDay() const
{
	SYSTEMTIME st;
	::FileTimeToSystemTime(&m_T, &st);
	return st.wDay;
}

inline int CDateTime::GetHour() const
{
	SYSTEMTIME st;
	::FileTimeToSystemTime(&m_T, &st);
	return st.wHour;
}

inline int CDateTime::GetMinute() const
{
	SYSTEMTIME st;
	::FileTimeToSystemTime(&m_T, &st);
	return st.wMinute;
}

inline int CDateTime::GetSecond() const
{
	SYSTEMTIME st;
	::FileTimeToSystemTime(&m_T, &st);
	return st.wSecond;
}

inline int CDateTime::GetMilliseconds() const
{
	SYSTEMTIME st;
	::FileTimeToSystemTime(&m_T, &st);
	return st.wMilliseconds;
}

inline int CDateTime::GetDayOfWeek() const
{
	SYSTEMTIME st;
	::FileTimeToSystemTime(&m_T, &st);
	return st.wDayOfWeek;
}

inline int CDateTime::GetDayOfYear() const
{
	if (IsValid())
	{
		return (operator struct tm()).tm_yday;
	}
	return -1;
}

inline void CDateTime::SetYear(int nYear)
{
	SYSTEMTIME st = *this;
	st.wYear = nYear;
	::SystemTimeToFileTime(&st, &m_T);
}

inline void CDateTime::SetMonth(int nMonth)
{
	SYSTEMTIME st = *this;
	st.wMonth = nMonth;
	::SystemTimeToFileTime(&st, &m_T);
}

inline void CDateTime::SetDay(int nDay)
{
	SYSTEMTIME st = *this;
	st.wDay = nDay;
	::SystemTimeToFileTime(&st, &m_T);
}

inline void CDateTime::SetHour(int nHour)
{
	SYSTEMTIME st = *this;
	st.wHour = nHour;
	::SystemTimeToFileTime(&st, &m_T);
}

inline void CDateTime::SetMinute(int nMinute)
{
	SYSTEMTIME st = *this;
	st.wMinute = nMinute;
	::SystemTimeToFileTime(&st, &m_T);
}

inline void CDateTime::SetSecond(int nSecond)
{
	SYSTEMTIME st = *this;
	st.wSecond = nSecond;
	::SystemTimeToFileTime(&st, &m_T);
}

inline void CDateTime::SetMilliseconds(int nMilliseconds)
{
	SYSTEMTIME st = *this;
	st.wMilliseconds = nMilliseconds;
	::SystemTimeToFileTime(&st, &m_T);
}

inline bool CDateTime::IsLeapYear() const
{
	long year = GetYear();
	return (((year & 3) == 0) && ((year % 100) != 0 || (year % 400) == 0));
}

inline bool CDateTime::IsNoon() const
{
    return GetHour() == 12 && GetMinute() == 0 && GetSecond() == 0;
}

inline bool CDateTime::IsMidnight() const
{
    return GetHour() == 0 && GetMinute() == 0 && GetSecond() == 0;
}

inline bool CDateTime::IsAM() const
{
	return GetHour() < 12;
}

inline bool CDateTime::IsPM() const
{
	return GetHour() >= 12;
}

inline bool CDateTime::SetDateTime(long nYear, 
								   long nMonth, 
								   long nDay, 
								   long nHour, 
								   long nMin, 
								   long nSec,
								   long nMSec)
{
	SYSTEMTIME st;
	st.wYear = (WORD)nYear;
	st.wMonth = (WORD)nMonth;
	st.wDay = (WORD)nDay;
	st.wHour = (WORD)nHour;
	st.wMinute = (WORD)nMin;
	st.wSecond = (WORD)nSec;
	st.wMilliseconds = (WORD)nMSec;
	::SystemTimeToFileTime(&st, &m_T);
    return true;
}

inline bool CDateTime::SetDate(long nYear, long nMonth, long nDay)
{
	SetDateTime(
		nYear, 
		nMonth, 
		nDay, 
		GetHour(), 
		GetMinute(), 
		GetSecond(), 
		GetMilliseconds());
    return true;
}

inline bool CDateTime::SetTime(long nHour, 
							   long nMin, 
							   long nSec, 
							   long nMSec)
{
	SetDateTime(
		GetYear(), 
		GetMonth(), 
		GetDay(), 
		nHour, 
		nMin, 
		nSec,
		nMSec);
    return true;
}

inline bool CDateTime::ParseDateTime(const char* lpszDate, 
									 DWORD dwFlags, 
									 LCID lcid)
{
	int nLen = (int)strlen(lpszDate) + 1;
	wchar_t* pwStr = (wchar_t*)alloca(nLen * sizeof(wchar_t));
	pwStr[0] = '\0';
	::MultiByteToWideChar(CP_ACP, 0, lpszDate, -1, pwStr, 0x07FFFFFF);
	return ParseDateTime(pwStr, dwFlags, lcid);
}

inline bool CDateTime::ParseDateTime(const wchar_t* lpszDate, 
									 DWORD dwFlags, 
									 LCID lcid)
{
	double date;
	if (FAILED(::VarDateFromStr(
		const_cast<wchar_t*>(lpszDate), 
		lcid, 
		dwFlags, 
		&date)))
	{
		Invalidate();
	}
	if (date < 1.0)
	{
		*this = ULONGLONG(date * DAY_INTERVAL);
	}
	else
	{
		VariantTimeToFileTime(date, &m_T);
	}
	return IsValid();
}

inline std::wstring CDateTime::Format(DWORD dwFlags, LCID lcid) const
{
	CComBSTR bstr;

	if (IsValid())
	{
		::VarBstrFromDate(*this, lcid, dwFlags, &bstr);
	}
    
	return std::wstring((LPCWSTR)bstr);
}

inline std::wstring CDateTime::Format(LPCWSTR lpszFormat) const
{
	struct tm time = *this;
	wchar_t wszOut[256] = {0};

    if (IsValid())
	{
		::wcsftime(wszOut, sizeof(wszOut) - 1, lpszFormat, &time);
	}
    
	return std::wstring(wszOut);
}

inline std::wstring CDateTime::FormatTime(LPCWSTR szFmt, 
										  DWORD dwFlags, 
										  LCID lcid) const
{
	SYSTEMTIME st = *this;
	wchar_t wszOut[256] = {0};

	::GetTimeFormatW(lcid, dwFlags, &st, szFmt, wszOut, sizeof(wszOut) - 1);

	return std::wstring(wszOut);
}

inline std::wstring CDateTime::FormatDate(LPCWSTR szFmt, 
										  DWORD dwFlags, 
										  LCID lcid) const
{
	SYSTEMTIME st = *this;
	wchar_t wszOut[256] = {0};

	::GetDateFormatW(lcid, dwFlags, &st, szFmt, wszOut, sizeof(wszOut) - 1);

	return std::wstring(wszOut);
}

inline CDateTime CDateTime::GetLocal()
{
	return CDateTime(GetLocalFileTime());
}

inline CDateTime CDateTime::GetSystem()
{
	return CDateTime(GetSystemFileTime());
}

/************************************************************************/
/* ��������������� ������� ��� ������ � ��������                        */
/************************************************************************/

inline ULONGLONG GetTickCountEx()
{
	CTime t;
	t.Local();
	return (ULONGLONG)t / 10000;
}

inline ULONGLONG GetPerfCounter()
{
	ULONGLONG cnt;
	::QueryPerformanceCounter((LARGE_INTEGER*)&cnt);
	return cnt;
}

inline ULONGLONG GetPerfFreq()
{
	ULONGLONG freq;
	::QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
	return freq;
}

inline void VariantTimeToFileTime(DATE time, LPFILETIME pFt)
{
	SYSTEMTIME st;
	VariantTimeToSystemTime(time, &st);
	::SystemTimeToFileTime(&st, pFt);
}

inline void FileTimeToVariantTime(const FILETIME* pFt, DATE* pTime)
{
	SYSTEMTIME st;
	::FileTimeToSystemTime(pFt, &st);
	SystemTimeToVariantTime(&st, pTime);
}

inline FILETIME GetSystemFileTime()
{
	FILETIME ft;
	::GetSystemTimeAsFileTime(&ft);
	return ft;
}

inline FILETIME GetLocalFileTime()
{
	FILETIME ft;
	SYSTEMTIME st;
	::GetLocalTime(&st);
	::SystemTimeToFileTime(&st, &ft);
	return ft;
}

}
