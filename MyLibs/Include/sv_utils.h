/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include <string>
#include <list>

namespace SVUtils
{

std::wstring WinErrToStr(DWORD dwErr);

bool IsFile(const std::wstring& sFileName);
std::wstring GetModuleFileName(HMODULE hMod = NULL);
std::wstring ExtractFileName(const std::wstring& sFileName);
std::wstring ExtractFilePath(const std::wstring& sPathName);
std::wstring GetCompName(bool bGetDomain = false);
std::wstring GetFullPath(const std::wstring& sFileName, const std::wstring& sPath = L"");
std::wstring AddPathBackslash(const std::wstring& sFileName);
bool IsSameFile(const std::wstring& sFileName1, const std::wstring& sFileName2);

bool CopyStringToClipboard(const std::wstring& str);
bool PasteStringFromClipboard(std::wstring& str);
bool ClearClipboard();

std::wstring GetProcessUser(DWORD dwPID = GetCurrentProcessId());

std::list<std::wstring> ParseCSV(const std::wstring& sCSV, WCHAR cDelimiter);

DWORD GetPESectionBase(LPCWSTR pwszFileName, PCSTR pszSectionName);

inline __int64 rand64()
{
	return 
		((ULONGLONG)rand() << 48) | 
		((ULONGLONG)rand() << 32) | 
		(rand() << 16) | 
		rand();
}

/*
class CFreeCrtPerThreadData
{
public:
	CFreeCrtPerThreadData();
	~CFreeCrtPerThreadData();
};
*/

}
