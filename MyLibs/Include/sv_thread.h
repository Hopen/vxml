/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include "sv_handle.h"
#include <WinBase.h>


namespace SVThread
{

/************************************************************************/
/* ����� CThread                                                        */
/************************************************************************/

class CThread
{
private:
	static VOID CALLBACK APCProcStub(ULONG_PTR)
	{
	}

protected:
	struct THREADPARAMS
	{
		LPVOID		pParam;
		CThread*	pThread;
		THREADPARAMS()
		{
			pParam = NULL;
			pThread = NULL;
		}
	};

	DWORD				m_dwThreadId;
	SVHandle::CHandle	m_hThread;
	SVHandle::CHandle	m_hExitEvent;
	bool				m_bInternalProc;
	THREADPARAMS		m_ThreadParams;

	static DWORD WINAPI ThreadStartRoutine(THREADPARAMS* pParams);
	virtual DWORD ThreadProc(LPVOID pParams) { return 0; }

public:
	CThread() : 
		m_dwThreadId(0),
		m_bInternalProc(false)
	{
	}

	CThread(LPTHREAD_START_ROUTINE pStartAddress,
			LPVOID pParameter = NULL,
			bool bSuspended = false,
			DWORD dwStackSize = 0,
			LPSECURITY_ATTRIBUTES pSecurityAttributes = NULL);

	CThread(DWORD dwThreadId, DWORD dwDesiredAccess = THREAD_ALL_ACCESS)
	{
		Attach(dwThreadId, dwDesiredAccess);
	}

	~CThread() 
	{
		Close();
	}

	bool Create(LPTHREAD_START_ROUTINE pStartAddress = NULL,
				LPVOID pParameter = NULL,
				bool bSuspended = false,
				DWORD dwStackSize = 0,
				LPSECURITY_ATTRIBUTES pSecurityAttributes = NULL);
	
	bool CreateRemote(HANDLE hRemoteProcess,
					  LPTHREAD_START_ROUTINE pStartAddress = NULL,
					  LPVOID pParameter = NULL,
					  bool bSuspended = false,
					  DWORD dwStackSize = 0,
					  LPSECURITY_ATTRIBUTES pSecurityAttributes = NULL);

	bool Attach(DWORD dwThreadId, DWORD dwDesiredAccess = THREAD_ALL_ACCESS);
	void Detach();
	void Close();

	int GetPriority() const
	{
		return ::GetThreadPriority(m_hThread);
	}
	void SetPriority(int nPriority)
	{
		::SetThreadPriority(m_hThread, nPriority);
	}

	FILETIME GetCreationTime() const
	{
		FILETIME ft, tmp;
		::GetThreadTimes(m_hThread, &ft, &tmp, &tmp, &tmp);
		return ft;
	}
	FILETIME GetExitTime() const
	{
		FILETIME ft, tmp;
		::GetThreadTimes(m_hThread, &tmp, &ft, &tmp, &tmp);
		return ft;
	}
	FILETIME GetKernelTime() const
	{
		FILETIME ft, tmp;
		::GetThreadTimes(m_hThread, &tmp, &tmp, &ft, &tmp);
		return ft;
	}
	FILETIME GetUserTime() const
	{
		FILETIME ft, tmp;
		::GetThreadTimes(m_hThread, &tmp, &tmp, &tmp, &ft);
		return ft;
	}
	
	DWORD Suspend()
	{
		return ::SuspendThread(m_hThread);
	}
	DWORD Resume()
	{
		return ::ResumeThread(m_hThread);
	}
	
	bool GetPriorityBoost() const
	{
		BOOL bDisabled = TRUE;
		::GetThreadPriorityBoost(m_hThread,&bDisabled);
		return !bDisabled;
	}
	bool SetPriorityBoost(bool bEnable)
	{
		return ::SetThreadPriorityBoost(m_hThread, bEnable ? FALSE : TRUE) != 0;
	}
	
	DWORD SetAffinityMask(DWORD dwAffinityMask)
	{
		return (DWORD)::SetThreadAffinityMask(m_hThread, dwAffinityMask);
	}
	DWORD SetIdealProcessor(DWORD dwIdealProcessor)
	{
		return ::SetThreadIdealProcessor(m_hThread, dwIdealProcessor);
	}

	DWORD GetExitCode() const
	{
		DWORD dwExitCode = STILL_ACTIVE;
		::GetExitCodeThread(m_hThread, &dwExitCode);
		return dwExitCode;
	}

	bool Terminate(DWORD dwExitCode)
	{
		return ::TerminateThread(m_hThread, dwExitCode) != 0;
	}

	bool PostMessage(UINT msg, WPARAM wParam = 0, LPARAM lParam = NULL)
	{
		return ::PostThreadMessage(m_dwThreadId, msg, wParam, lParam) != 0;
	}
	bool QueueAPC(PAPCFUNC pfnAPC, DWORD dwData)
	{
		return ::QueueUserAPC(pfnAPC, m_hThread, dwData) != 0;
	}
	DWORD QueueAPCAndWait(PAPCFUNC pfnAPC = NULL, 
						  DWORD dwData = 0,
						  DWORD dwTimeout = INFINITE)
	{
		if (!QueueAPC(pfnAPC ? pfnAPC : APCProcStub, dwData))
		{
			return 0;
		}
		return ::WaitForSingleObject(m_hThread, dwTimeout);
	}
	DWORD WaitFor(DWORD dwTimeout = INFINITE)
	{
		return ::WaitForSingleObject(m_hThread, dwTimeout);
	}

	bool GetContext(CONTEXT& context) const
	{
		return ::GetThreadContext(m_hThread, &context) != 0;
	}
	bool SetContext(const CONTEXT& context)
	{
		return ::SetThreadContext(m_hThread, &context) != 0;
	}

	const SVHandle::CHandle& GetHandle() const
	{
		return m_hThread;
	}
	DWORD GetTID() const
	{
		return m_dwThreadId;
	}

	__declspec(property(get = GetHandle)) SVHandle::CHandle Handle;
	__declspec(property(get = GetTID)) DWORD TID;	

#if (_WIN32_WINNT >= 0x502)
	CThread(HANDLE hThread)
	{
		Attach(hThread);
	}
	bool Attach(HANDLE hThread)
	{
		return Attach(::GetThreadId(hThread));
	}
	DWORD GetProcessID()
	{
		return ::GetProcessIdOfThread(m_hThread);
	}
#endif
};

}
