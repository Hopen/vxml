/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include <shlwapi.h>

namespace SVVer
{

/************************************************************************/
/* ����� CWinVer - ��������� ��������� ���������� � ������ Windows      */
/************************************************************************/

class CWinVer
{
public:
	enum eVersion
	{
		Unknown,
		W95,
		W95SP1,
		W95OSR2,
		W98,
		W98SP1,
		W98SE,
		WME,
		WNT351,
		WNT4,
		W2K,
		WXP,
		W2003SERVER,
		WCE
	};

private:
	OSVERSIONINFOEXW	m_Version;
	eVersion			m_nVersion;
	WCHAR				m_wszDotVersion[128];
	LPCWSTR				m_pwszSuite;
	bool				m_bServer;

public:
	CWinVer();
	
	bool IsServer() const
	{
		return m_bServer;
	}
	eVersion GetVersion() const
	{
		return m_nVersion;
	}
	bool IsValid() const
	{
		return m_Version.dwMajorVersion != 0;
	}

	LPCWSTR GetDotVersion() const
	{
		return m_wszDotVersion;
	}
	LPCWSTR GetVersionStr() const;
	LPCWSTR GetSuite() const
	{
		return m_pwszSuite;
	}
	LPCWSTR GetCSDVersion() const
	{
		return m_Version.szCSDVersion;
	}
};

/************************************************************************/
/* ����� CModuleVersion - ��������� ���������� � ������ ������          */
/************************************************************************/

class CModuleVersion
{
private:
	typedef	struct tagTRANSLATION
	{
		WORD langID;         // language ID
		WORD charset;        // character set (code page)
	} TRANSLATION, *PTRANSLATION;

	PVOID				m_pVersionInfo;
	VS_FIXEDFILEINFO	m_FileInfo;
	TRANSLATION			m_Transl;
	WCHAR				m_wszDotVersion[128];

protected:
	bool Init(LPCWSTR pwszFileName);
	void Destroy()
	{
		if (m_pVersionInfo)
		{
			free(m_pVersionInfo);
			m_pVersionInfo = NULL;
		}
	}

public:
	CModuleVersion(LPCWSTR pwszFileName);
	~CModuleVersion()
	{
		Destroy();
	}

	bool IsValid() const
	{
		return m_FileInfo.dwSignature == VS_FFI_SIGNATURE;
	}

	LPCWSTR GetValue(LPCWSTR pwszKeyName) const;

	ULONGLONG GetFileVersion() const
	{
		return ((ULONGLONG)m_FileInfo.dwFileVersionMS << 32) | m_FileInfo.dwFileVersionLS;
	}
	ULONGLONG GetProductVersion() const
	{
		return ((ULONGLONG)m_FileInfo.dwProductVersionMS << 32) | m_FileInfo.dwProductVersionLS;
	}

	DWORD GetType()			const { return m_FileInfo.dwFileType;		}
	DWORD GetOS()			const { return m_FileInfo.dwFileOS;			}
	DWORD GetFlags()		const { return m_FileInfo.dwFileFlags;		}
	
	LPCWSTR GetCompany()	const { return GetValue(L"CompanyName");	}
	LPCWSTR GetProduct()	const { return GetValue(L"ProductName");	}
	LPCWSTR GetFileDescr()	const { return GetValue(L"FileDescription");}
	LPCWSTR GetDotVersion()	const { return m_wszDotVersion;				}
};

/************************************************************************/
/* ����� CDLLVersion - ��������� ���������� � ������ DLL-����������     */
/************************************************************************/

class CDLLVersion
{
private:
	HMODULE			m_hDLL;
	BOOL			m_bFreeAtExit;
	DLLVERSIONINFO2	m_DVI;

	bool Init(HMODULE hDLL); 
	bool Init(LPCWSTR pwszFileName);
	void Destroy();

public:
	CDLLVersion(HMODULE hDLL) :
		m_hDLL(NULL), m_bFreeAtExit(false)
	{
		ZeroMemory(&m_DVI, sizeof(m_DVI));
		Init(hDLL);
	}
	CDLLVersion(LPCWSTR pwszFileName) :
		m_hDLL(NULL), m_bFreeAtExit(false)
	{
		ZeroMemory(&m_DVI, sizeof(m_DVI));
		Init(pwszFileName);
	}
	~CDLLVersion()
	{
		Destroy();
	}

	bool IsValid() const
	{
		return m_DVI.info1.cbSize != 0;
	}

	DWORD GetMajor()	const { return m_DVI.info1.dwMajorVersion;			}
	DWORD GetMinor()	const { return m_DVI.info1.dwMinorVersion;			}
	DWORD GetBuild()	const { return (DWORD)m_DVI.ullVersion & 0xFFFF;	}
	DWORD GetRelease()	const { return m_DVI.info1.dwBuildNumber;			}
	
	bool IsNT() const { return m_DVI.info1.dwPlatformID == DLLVER_PLATFORM_NT;}
};

}
