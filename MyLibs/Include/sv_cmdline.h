/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include <string>
#include <vector>
#include <map>


namespace SVCmdline
{

using namespace std;

class CCmdlineParser
{
private:
	struct KeyInfo
	{
		wstring sName;
		wstring sKeyDescr;
		wstring sParamDescr;
		bool	bCaseSensitive;
		int		nMinParams;
		int		nMaxParams;
		KeyInfo(const wstring& _sName, bool _bCaseSensitive) :
			sName(_sName),
			bCaseSensitive(_bCaseSensitive),
			nMinParams(0),
			nMaxParams(0)
		{
		}
		bool operator <(const KeyInfo& key) const
		{
			return bCaseSensitive ? 
				(wcscmp(sName.c_str(), key.sName.c_str()) < 0) :
				(_wcsicmp(sName.c_str(), key.sName.c_str()) < 0);
		}
	};

	void Clear(bool bClearKeys);
	bool GetKey(wstring& sParam);
	bool ProcessKey(const wstring& sKey, int& nPos);

public:
	typedef vector<wstring> PARAMS;

	struct KeyData
	{
		int		nPos;
		PARAMS	sParams;
	};

	typedef map<wstring,const KeyData*> KEYS;

protected:
	typedef vector<wstring>			PREFIXES;
	typedef map<KeyInfo,KeyData*>	KEYSMAP;

	int			m_argc;
	wchar_t**	m_argv;
	bool		m_bCaseSensitive;
	int			m_nMaxParams;
	
	mutable PREFIXES	m_Prefixes;
	mutable KEYSMAP		m_KeysMap;
	mutable PARAMS		m_Params;

	virtual bool OnInvalidKey(const wstring& sKeyName)
	{
		wprintf(L"Error: unknown parameter '%s'\n\n", sKeyName.c_str());
		return false;
	}
	virtual bool OnInvalidKeyParamCount(const wstring& sKeyName, int nCount)
	{
		wprintf(L"Error: invalid number of arguments (%d) for parameter '%s'\n\n", nCount, sKeyName.c_str());
		return false;
	}
	virtual bool OnDuplicateKey(const wstring& sKeyName)
	{
		wprintf(L"Warning: duplicate parameter '%s'\n\n", sKeyName.c_str());
		return true;
	}
	virtual bool OnMaxParams(int /*nParamCount*/)
	{
		wprintf(L"Error: invalid number of parameters\n\n");
		return false;
	}

public:
	CCmdlineParser();
	CCmdlineParser(int argc, wchar_t* argv[]);
	~CCmdlineParser();

	void Reset(int argc, wchar_t* argv[]);

	void SetCaseSensitive(bool bCaseSensitive = true)
	{
		m_bCaseSensitive = bCaseSensitive;
	}
	void SetMaxParams(int nMaxParams)
	{
		m_nMaxParams = nMaxParams;
	}

	void AddKeyPrefix(const wstring& sPrefix)
	{
		m_Prefixes.push_back(sPrefix);
	}
	bool AddKey(const wstring& sKeyName, 
				int nMinParams = 0, 
				int nMaxParams = 0, 
				const wstring& sKeyDescr = L"", 
				const wstring& sParamDescr = L"");
	void ClearKeys()
	{
		Clear(true);
	}
	
	bool Parse();
	void PrintHelp(const wstring& sPrefix = L"", const wstring& sSuffix = L"") const;
	
	bool HasKey(const wstring& sKeyName) const
	{
		return FindKey(sKeyName) != NULL;
	}
	const KeyData* FindKey(const wstring& sKeyName) const
	{
		KEYSMAP::iterator it = m_KeysMap.find(KeyInfo(sKeyName, m_bCaseSensitive));
		return (it == m_KeysMap.end()) ? NULL : it->second;
	}
	KEYS GetKeys() const;
	const PARAMS& GetParams() const
	{
		return m_Params;
	}
};

}
