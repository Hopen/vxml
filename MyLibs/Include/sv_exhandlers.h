/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include <eh.h>

namespace SVExHandlers
{

/************************************************************************/
/* Exception handling classes                                           */
/************************************************************************/

// Win32 SEH translator
class CSeTranslator
{
private:
	_se_translator_function m_pFn;

	static void SeTranslator(unsigned int, PEXCEPTION_POINTERS pExcPtr);

protected:
	virtual void ExceptionHandler(PEXCEPTION_POINTERS pExcPtr);

public:
	CSeTranslator();
	~CSeTranslator();
};

// Win32 vectored exception handler
class CVectoredExceptionHandler
{
private:
	PVOID m_pHandler;

	static LONG WINAPI VectoredHandler(PEXCEPTION_POINTERS pExcPtr);

protected:
	virtual LONG ExceptionHandler(PEXCEPTION_POINTERS pExcPtr);

public:
	CVectoredExceptionHandler();
	~CVectoredExceptionHandler();
};

// Win32 unhandled exception handler
class CUnhandledExceptionHandler
{
private:
	LPTOP_LEVEL_EXCEPTION_FILTER		m_pPrevHandler;
	static CUnhandledExceptionHandler*	m_pThis;

	static LONG WINAPI UnhandledExceptionFilter(PEXCEPTION_POINTERS pExcPtr);

protected:
	virtual LONG ExceptionHandler(PEXCEPTION_POINTERS pExcPtr);

public:
	CUnhandledExceptionHandler(DWORD dwFlags = 
		SEM_FAILCRITICALERRORS		| 
		SEM_NOALIGNMENTFAULTEXCEPT	|
		SEM_NOGPFAULTERRORBOX		|
		SEM_NOOPENFILEERRORBOX);
	~CUnhandledExceptionHandler();
};

// CRT invalid parameter handler
class CCRTInvParamHandler
{
private:
	_invalid_parameter_handler m_pPrevHandler;

	static void CRTInvalidParameterHandler(const wchar_t* expression,
										   const wchar_t* function, 
										   const wchar_t* file,
										   unsigned int line, 
										   uintptr_t pReserved);

protected:
	virtual void InvalidParameterHandler(const wchar_t* /*expression*/,
		const wchar_t* /*function*/, 
		const wchar_t* /*file*/, 
		unsigned int /*line*/, 
		uintptr_t /*pReserved*/)
	{
	}

public:
	CCRTInvParamHandler();
	~CCRTInvParamHandler();
};

// Standard CRT invalid parameter handler
class CStdCRTInvParamHandler : public CCRTInvParamHandler
{
protected:
	virtual void InvalidParameterHandler(const wchar_t* expression,
		const wchar_t* function, 
		const wchar_t* file, 
		unsigned int line, 
		uintptr_t pReserved);
};

// Standard SEH translator
class CStdSeTranslator : 
	public SVExHandlers::CSeTranslator, 
	public CStdCRTInvParamHandler
{
protected:
	virtual void ExceptionHandler(PEXCEPTION_POINTERS pExcPtr);
};

// Standard unhandled exception handler
class CStdUnhandledExceptionHandler : public SVExHandlers::CUnhandledExceptionHandler
{
protected:
	virtual LONG ExceptionHandler(PEXCEPTION_POINTERS pExcPtr);
	virtual void LogException(LPCWSTR pwszExInfo) = NULL;
};

}
