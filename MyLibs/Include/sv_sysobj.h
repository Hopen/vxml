/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include "sv_handle.h"

namespace SVSysObj
{

// ������� ������� � rm822

template <typename LOCKOBJ>
class CLocker
{
private:
	LOCKOBJ* m_pLockObj;
	
protected:
	CLocker(LOCKOBJ* pLockObj) : m_pLockObj(pLockObj)
	{
		m_pLockObj->Lock();
	}
	void Lock()
	{
		if (m_pLockObj)
		{
			m_pLockObj->Lock();
		}
	}
	
public:
	CLocker(const CLocker &other)
	{
		m_pLockObj = other.m_pLockObj;
		Lock();
	}
	~CLocker()
	{
		Unlock();
	}
	void Unlock()
	{
		if (m_pLockObj)
		{
			LOCKOBJ* tmp = m_pLockObj;
			m_pLockObj = NULL;
			tmp->Unlock();
		}
	}
	CLocker& operator =(const CLocker &other)
	{
		if (this != &other)
		{
			Unlock();
			m_pLockObj = other->m_pLockObj;
			Lock();					
		}
		return *this;
	}

	friend LOCKOBJ;
};

class CCriticalSection : private CRITICAL_SECTION
{
public:
	CCriticalSection()
	{
		::InitializeCriticalSection(this);
	}
	~CCriticalSection()
	{
		::DeleteCriticalSection(this);
	}
	void Enter()
	{
		::EnterCriticalSection(this);
	}
	void Leave()
	{
		::LeaveCriticalSection(this);
	}
	// locker object methods
	void Lock()
	{
		Enter();
	}
	void Unlock()
	{
		Leave();
	}
	bool TryEnter()
	{
		return ::TryEnterCriticalSection(this) != 0;
	}
	bool TryLock()
	{
		return TryEnter();
	}
	// locker implementation
	typedef	CLocker<CCriticalSection> CLocker;
	CCriticalSection::CLocker GetLocker()
	{
		return CLocker(this);
	}
};

class CMutex
{
private:
	SVHandle::CHandle m_hMutex;

public:
	CMutex(bool bCreate = true)
	{
		if (bCreate) 
		{
			Create();
		}
	}
	CMutex(PCWSTR pwszName)
	{
		Create(pwszName);
	}
	~CMutex()
	{
		Close();
	}
	operator HANDLE() const
	{
		return m_hMutex;
	}
	bool Create(PCWSTR pwszName = NULL,
				LPSECURITY_ATTRIBUTES lpSecurityAttributes = NULL,
				bool bInitialOwner = false)
	{
		Close();
		return (m_hMutex = ::CreateMutexW(
			lpSecurityAttributes, 
			bInitialOwner, 
			pwszName)) != NULL;
	}
	bool Open(PCWSTR pwszName,
			  DWORD dwDesiredAccess = MUTEX_MODIFY_STATE,
			  bool bInheritHandle = false)
	{
		Close();
		return (m_hMutex = ::OpenMutexW(
			dwDesiredAccess, 
			bInheritHandle, 
			pwszName)) != NULL;
	}
	bool Own(DWORD dwTimeOut = INFINITE)
	{
		if (!m_hMutex)
		{
			return false;
		}

		switch (::WaitForSingleObject(m_hMutex, dwTimeOut))
		{
			case WAIT_OBJECT_0:
				return true;

			case WAIT_ABANDONED:
			case WAIT_TIMEOUT:
				break;
		}

		return false;
	}
	bool Release()
	{
		return (!m_hMutex) ? false : (::ReleaseMutex(m_hMutex) != 0);
	}
	void Close()
	{
		m_hMutex.Close();
	}
	// locker object methods
	void Lock()
	{
		Own();
	}
	bool TryLock()
	{
		return Own(0);
	}
	void Unlock() 
	{
		Release();
	}
	// locker implementation
	typedef	CLocker<CMutex> CLocker;
	CMutex::CLocker GetLocker()
	{
		return CLocker(this);
	}
};

class CInterLocked
{
private:
	LONG m_lValue;

public:
	CInterLocked(LONG lValue = 0) : m_lValue(lValue)
	{
	}
	LONG operator +=(LONG lValue)
	{
		return InterlockedExchangeAdd(&m_lValue, lValue);
	}
	LONG operator -=(LONG lValue)
	{
		return InterlockedExchangeAdd(&m_lValue, -lValue);
	}
	LONG operator  =(LONG lValue)
	{
		return m_lValue = lValue;
	}
	operator LONG() const
	{
		return m_lValue;
	}
	LONG operator ++()
	{
		return InterlockedIncrement(&m_lValue);
	}
	LONG operator ++(int)
	{
		return InterlockedIncrement(&m_lValue) - 1;
	}
	LONG operator --()
	{
		return InterlockedDecrement(&m_lValue);
	}
	LONG operator --(int)
	{
		return InterlockedDecrement(&m_lValue) + 1;
	}
};

class CEvent
{
private:
	SVHandle::CHandle m_hEvent;

public:
	CEvent()
	{
	}
	CEvent(const CEvent& evt) : m_hEvent(evt.m_hEvent)
	{
	}
	bool Create(PCWSTR pwszName = NULL,
				bool bManualReset = false,
				bool bInitialState = false,
				LPSECURITY_ATTRIBUTES lpSecurityAttributes = NULL)
	{
		return (m_hEvent = ::CreateEventW(
			lpSecurityAttributes,
			bManualReset ? TRUE : FALSE,
			bInitialState ? TRUE : FALSE,
			pwszName)) != NULL;
	}
	bool Open(PCWSTR pwszName,
			  DWORD dwDesiredAccess	= EVENT_ALL_ACCESS,
			  bool bInheritHandle = false)
	{
		return (m_hEvent = ::OpenEventW(
			dwDesiredAccess, 
			bInheritHandle, 
			pwszName)) != NULL;
	}
	void Close()
	{
		m_hEvent.Close();
	}
	bool Set()
	{
		return ::SetEvent(m_hEvent) != 0;
	}
	bool Reset()
	{
		return ::ResetEvent(m_hEvent) != 0;
	}
	bool Pulse()
	{
		return ::PulseEvent(m_hEvent) != 0;
	}
	bool Wait(DWORD dwTimeOut = INFINITE)
	{
		return ::WaitForSingleObject(m_hEvent, dwTimeOut) == WAIT_OBJECT_0;
	}
	bool operator !() const
	{
		return !m_hEvent;
	}
	operator HANDLE() const
	{
		return m_hEvent;
	}
	CEvent& operator =(const CEvent& evt)
	{
		if (this != &evt)
		{
			m_hEvent = evt.m_hEvent;
		}
		return *this;
	}	
};

}
