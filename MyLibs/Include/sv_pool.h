/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma	once

#include "sv_sysobj.h"
#include <map>
#include <algorithm>

/************************************************************************/
/* ����������� � ���������� ����������� ���                             */
/************************************************************************/

namespace SVPool
{

template <class KEY, class ID>
struct IPoolObj
{
	virtual bool CanRemoveFromPool()	= NULL;
	virtual	void AssignKey(KEY key)		= NULL;
	virtual	void AssignId(ID id)		= NULL;
	virtual ~IPoolObj()					{ }
};

// ��� �������� � �������������� ���������
template <typename Interface, class KEY, class ID, class KEYGEN>
class CPool
{
public:
	typedef	Interface			IObject;
	typedef std::vector<KEY>	KEYLIST;
	typedef std::vector<ID>		IDLIST;

private:
	typedef	SVSysObj::CCriticalSection	SyncObj;
	typedef SyncObj::CLocker			CLocker;

	struct ITEM
	{
		IObject*	pObj;
		SyncObj		lock;
		int			nRefs;
		KEY			Key;
		ID			Id;
		ITEM(IObject* obj) 
			: pObj(obj), nRefs(), Key(), Id()
		{
		}
		~ITEM()
		{
			if (pObj)
			{
				delete pObj;
			}
		}
	};

	typedef	std::map<ID,KEY>	IDMAP;
	typedef	std::map<KEY,ITEM*>	OBJMAP;

	IDMAP	m_IdMap;
	OBJMAP	m_ObjMap;
	SyncObj	m_Sync;

public:
	class CPtr
	{
		friend CPool;
	private:
		CPool*	m_pPool;
		ITEM*	m_pItem;

		void Remove()
		{
			if (m_pPool && m_pItem)
			{
				m_pPool->Remove(m_pItem);
			}
		}

	protected:
		CPtr(CPool* pPool, ITEM* pItem)
			: m_pPool(pPool), m_pItem(pItem)
		{
			if (m_pItem)
			{
				m_pItem->lock.Lock();
			}
		}

	public:
		CPtr() : m_pPool(NULL), m_pItem(NULL)
		{
		}
		~CPtr()
		{
			Remove();
		}
		CPtr(const CPtr& other) 
		{
			*this = other;
		}
		CPtr& operator =(const CPtr& other)
		{
			if (this != &other)
			{
				Remove();

				m_pItem	= other.m_pItem;
				m_pPool	= other.m_pPool;

				if (m_pItem)
				{
					m_pItem->lck.Lock();
					++m_pItem->Refs;
				}
			}

			return *this;
		}
		IObject* operator ->() const
		{
			return (IObject*)*this;
		}
		operator IObject*() const
		{
			return (m_pItem) ? m_pItem->pObj : NULL;
		}
		KEY Key()
		{
			return (m_pItem) ? m_pItem->Key : 0;
		}
		ID Id()
		{
			return (m_pItem) ? m_pItem->Id : 0;
		}
	};

protected:
	void Remove(ITEM* pItem)
	{
		if (pItem)
		{
			pItem->lock.Unlock();
			CLocker	lock = m_Sync.GetLocker();
			if (--pItem->nRefs <= 0)
			{
				if (pItem->pObj->CanRemoveFromPool())
				{
					m_ObjMap.erase(pItem->Key);
					m_IdMap.erase(pItem->Id);
					delete pItem;
				}
			}
		}
	}

	template <typename LIST, typename MAP>
	LIST GetList(MAP& m)
	{
		LIST list;

		CLocker	lock = m_Sync.GetLocker();

		list.reserve(m.size());

		MAP::const_iterator it = m.begin();
		MAP::const_iterator e = m.end();

		for (; it != e; ++it)
		{
			list.push_back(it->first);
		}

		return list;
	}

public:
	CPool()
	{
	}
	KEY Add(IObject* pObj, KEY key = KEY())
	{
		ITEM*	pItem	= new ITEM(pObj);
		CLocker	lock	= m_Sync.GetLocker();

		if (m_ObjMap.find(key) == m_ObjMap.end())
		{
			if (key == KEY())
			{
				key = KEYGEN();
			}
			pObj->AssignKey(pItem->Key = key);
			m_ObjMap[key] = pItem;
		}

		return key;
	}
	void AssignId(KEY key, ID id)
	{
		CLocker	lock = m_Sync.GetLocker();
		OBJMAP::const_iterator it = m_ObjMap.find(key);

		if (it != m_ObjMap.end())
		{
			it->second->pObj->AssignId(id);
			m_IdMap[id] = key;
		}
	}
	CPtr Lookup(KEY key)
	{
		ITEM* pItem = NULL;

		{
			CLocker	lock = m_Sync.GetLocker();
			OBJMAP::const_iterator it = m_ObjMap.find(key);
			if (it != m_ObjMap.end() && (pItem = it->second) != NULL)
			{
				++pItem->nRefs;
			}
		}

		return CPtr(this, pItem);
	}
	CPtr GetObjById(ID id)
	{
		ITEM* pItem = NULL;

		{
			CLocker	lock = m_Sync.GetLocker();
			KEY key();
			IDMAP::const_iterator it = m_IdMap.find(id);
			if (it != m_IdMap.end())
			{
				OBJMAP::const_iterator it2 = m_ObjMap.find(it->second);
				if (it2 != m_ObjMap.end() && (pItem = it2->second) != NULL)
				{
					++pItem->nRefs;
				}
			}
		}

		return CPtr(this, pItem);
	}
	int GetCount()
	{
		CLocker	lock = m_Sync.GetLocker();
		return (int)m_ObjMap.size();
	}

	template <class fn>
	void for_each(fn func)
	{	
		CLocker	lock = m_Sync.GetLocker();
		OBJMAP::const_iterator it = m_ObjMap.begin();
		for (; it != m_ObjMap.end();)
		{
			ITEM* pItem = it->second;
			OBJMAP::const_iterator next = ++it;
			{
				++pItem->nRefs;
				CPtr ptr(this, pItem);
				func(ptr);
			}
			it = next;
		}
	}
	KEYLIST GetKeyList()
	{
		return GetList<KEYLIST,OBJMAP>(m_ObjMap);
	}
	IDLIST GetIdList()
	{
		return GetList<IDLIST,IDMAP>(m_IdMap);
	}
};

}
