/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include <string>
#include <stdarg.h>

/************************************************************************/
/*                   ������� ��� �������������� �����                   */
/************************************************************************/

#define STRW2A(lpw) (\
	!lpw ? NULL : \
	StrW2AHelper((LPSTR)alloca(wcslen(lpw) + 1), lpw))

#define STRW2AN(lpw,N) (\
	!lpw ? NULL : \
	StrW2AHelper((LPSTR)alloca(N + 1), lpw, N))

#define STRA2W(lpa) (\
	!lpa ? NULL : \
	StrA2WHelper((LPWSTR)alloca((strlen(lpa) + 1) * 2), lpa))

#define STRA2WN(lpa,N) (\
	!lpa ? NULL : \
	StrA2WHelper((LPWSTR)alloca((N + 1) * 2), lpa, N))

#ifdef _UNICODE
#define STRT2A(lpw)			STRW2A(lpw)
#else
#define STRT2A(lpa)			(lpa)
#endif

#ifdef _UNICODE
#define STRT2W(lpw)			(lpw)
#else
#define STRT2W(lpa)			STRA2W(lpa)
#endif

#ifdef _UNICODE
#define STRA2T(lps)			STRA2W(lps)
#else
#define STRA2T(lps)			(lps)
#endif

#ifdef _UNICODE
#define STRW2T(lps)			(lps)
#else
#define STRW2T(lps)			STRW2A(lps)
#endif


/************************************************************************/
/*           ��������������� ������� ��� �������������� �����           */
/************************************************************************/

inline LPSTR StrW2AHelper(LPSTR lpa, LPCWSTR lpw, int nLen = -1)
{
	lpa[0] = '\0';
	WideCharToMultiByte(CP_ACP, 0, lpw, nLen, lpa, 0x07FFFFFF, NULL, NULL);
	return lpa;
}

inline LPWSTR StrA2WHelper(LPWSTR lpw, LPCSTR lpa, int nLen = -1)
{
	lpw[0] = '\0';
	MultiByteToWideChar(CP_ACP, 0, lpa, nLen, lpw, 0x07FFFFFF);
	return lpw;
}

inline std::wstring StrA2wstring(LPCSTR lpa)
{
	size_t nLen = strlen(lpa);
	std::wstring str(nLen, L'\0');
	StrA2WHelper((LPWSTR)str.c_str(), lpa, (int)nLen);
	return str;
}

/************************************************************************/
/*                 ������� ��� �������������� ��������                  */
/************************************************************************/

inline char CW2A(wchar_t c)
{
	char buf[2];
	WideCharToMultiByte(CP_ACP, 0, &c, 1, buf, 2, NULL, NULL);
	return buf[0];
}

inline wchar_t CA2W(char c)
{
	wchar_t buf[2];
	MultiByteToWideChar(CP_ACP, 0, &c, 1, buf, 2);
	return buf[0];
}

/*inline char CT2T(char& dest, char src)
{
return dest = src;
}

inline char CT2T(char& dest, wchar_t src)
{
return dest = CW2A(src);
}

inline wchar_t CT2T(wchar_t& dest, char src)
{
return dest = CA2W(src);
}

inline wchar_t CT2T(wchar_t& dest, wchar_t src)
{
return dest = src;
}*/


/************************************************************************/
/*               ��������� ��� ��������� ����� � std::map               */
/************************************************************************/

struct LPCSTRLess {
	bool operator()(const LPCSTR& _X, const LPCSTR& _Y) const
	{
		return strcmp(_X, _Y) < 0; 
	}
};

struct LPCSTRLessIC {
	bool operator()(const LPCSTR& _X, const LPCSTR& _Y) const
	{
		return _strcmpi(_X, _Y) < 0; 
	}
};

struct LPCWSTRLess {
	bool operator()(const LPCWSTR& _X, const LPCWSTR& _Y) const
	{
		return wcscmp(_X, _Y) < 0; 
	}
};

struct LPCWSTRLessIC {
	bool operator()(const LPCWSTR& _X, const LPCWSTR& _Y) const
	{
		return _wcsicmp(_X, _Y) < 0; 
	}
};

struct wstringLessIC {
	bool operator()(const std::wstring& _X, const std::wstring& _Y) const
	{
		return _wcsicmp(_X.c_str(), _Y.c_str()) < 0; 
	}
};

struct stringLessIC {
	bool operator()(const std::string& _X, const std::string& _Y) const
	{
		return _stricmp(_X.c_str(), _Y.c_str()) < 0; 
	}
};


/************************************************************************/
/*           ��������������� ������� ��� ������ � std::wstring          */
/************************************************************************/

inline std::wstring formatv_wstring(const wchar_t* fmt, va_list args)
{
	wchar_t* p = (wchar_t*)malloc(256 * sizeof(wchar_t));
	int	len, n = 256;
#if _MSC_VER >= 1400
	while ((len = _vsnwprintf_s(p, n, n - 1, fmt, args)) == -1)
#else
	while ((len = _vsnwprintf(p, n - 1, fmt, args)) == -1)
#endif
	{
		n += 256;
		p = (wchar_t*)realloc(p, n * sizeof(wchar_t));
	}
	p[len] = L'\0';
	std::wstring str = p;
	free(p);
	return str;
}

inline std::string formatv_string(const char* fmt, va_list args)
{
	char* p = (char*)malloc(256);
	int	len, n = 256;
#if _MSC_VER >= 1400
	while ((len = _vsnprintf_s(p, n, n - 1, fmt, args)) == -1)
#else
	while ((len = _vsnprintf(p, n - 1, fmt, args)) == -1)
#endif
	{
		n += 256;
		p = (char*)realloc(p, n);
	}
	p[len] = '\0';
	std::string str = p;
	free(p);
	return str;
}

inline std::wstring format_wstring(const wchar_t* fmt, ...)
{
//	std::wstring s;
//	size_t pos;
//	s = fmt;
//	if((pos = s.find(L"\"%\"")) != std::string::npos)	// ��� ���������� �������, ����� ���������� ������ % � ������
//		s.replace(pos, 3, L"\"%%\"");
//
	va_list args;
//	fmt = s.c_str();
	va_start(args, fmt);
	return formatv_wstring(fmt, args);
}

inline std::string format_string(const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	return formatv_string(fmt, args);
}

inline std::wstring trim_wstring(const std::wstring& str)
{
	const wchar_t* p = str.c_str();
	const wchar_t* q = str.c_str() + str.length() - 1;
	while (*p && iswspace(*p)) p++;
	while (q > p && iswspace(*q)) q--;
	return std::wstring(p, q - p + 1);
}

inline std::string trim_string(const std::string& str)
{
	const char* p = str.c_str();
	const char* q = str.c_str() + str.length() - 1;
	while (*p && isspace(*p)) p++;
	while (q > p && isspace(*q)) q--;
	return std::string(p, q - p + 1);
}

inline int cmpi_wstring(const std::wstring& str1, const std::wstring& str2)
{
	return _wcsicmp(str1.c_str(), str2.c_str());
}

inline int cmpi_string(const std::string& str1, const std::string& str2)
{
	return _stricmp(str1.c_str(), str2.c_str());
}
