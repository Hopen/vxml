/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#pragma warning(push)
#pragma warning(disable:4005)
#include <ntstatus.h>
#pragma warning(pop)

#include "sv_helpclasses.h"
typedef LONG KPRIORITY;
typedef LONG NTSTATUS;

#define NT_SUCCESS(Status)                      ((NTSTATUS)(Status) >= 0)
//#define STATUS_INFO_LENGTH_MISMATCH             ((NTSTATUS)0xC0000004L)

//////////////////////////////////////////////////////////////////////////
// System Information and Control
//////////////////////////////////////////////////////////////////////////

/*
The system information classes available in the "free" (retail) build of the system are
listed below along with a remark as to whether the information class can be queried,
set, or both. Some of the information classes labeled "SystemNotImplementedXxx" are
implemented in the "checked" build, and a few of these classes are briefly described
later. 
*/

typedef enum _SYSTEM_INFORMATION_CLASS {	//		Query	Set
	SystemBasicInformation,					// 0	Y		N
	SystemProcessorInformation,				// 1	Y		N
	SystemPerformanceInformation,			// 2	Y		N
	SystemTimeOfDayInformation,				// 3	Y		N
	SystemNotImplemented1,					// 4	Y		N
	SystemProcessesAndThreadsInformation,	// 5	Y		N
	SystemCallCounts,						// 6	Y		N
	SystemConfigurationInformation,			// 7	Y		N
	SystemProcessorTimes,					// 8	Y		N
	SystemGlobalFlag,						// 9	Y		Y
	SystemNotImplemented2,					// 10	Y		N
	SystemModuleInformation,				// 11	Y		N
	SystemLockInformation,					// 12	Y		N
	SystemNotImplemented3,					// 13	Y		N
	SystemNotImplemented4,					// 14	Y		N
	SystemNotImplemented5,					// 15	Y		N
	SystemHandleInformation,				// 16	Y		N
	SystemObjectInformation,				// 17	Y		N
	SystemPagefileInformation,				// 18	Y		N
	SystemInstructionEmulationCounts,		// 19	Y		N
	SystemInvalidInfoClass1,				// 20
	SystemCacheInformation,					// 21	Y		Y
	SystemPoolTagInformation,				// 22	Y		N
	SystemProcessorStatistics,				// 23	Y		N
	SystemDpcInformation,					// 24	Y		Y
	SystemNotImplemented6,					// 25	Y		N
	SystemLoadImage,						// 26	N		Y
	SystemUnloadImage,						// 27	N		Y
	SystemTimeAdjustment,					// 28	Y		Y
	SystemNotImplemented7,					// 29	Y		N
	SystemNotImplemented8,					// 30	Y		N
	SystemNotImplemented9,					// 31	Y		N
	SystemCrashDumpInformation,				// 32	Y		N
	SystemExceptionInformation,				// 33	Y		N
	SystemCrashDumpStateInformation,		// 34	Y		Y/N
	SystemKernelDebuggerInformation,		// 35	Y		N
	SystemContextSwitchInformation,			// 36	Y		N
	SystemRegistryQuotaInformation,			// 37	Y		Y
	SystemLoadAndCallImage,					// 38	N		Y
	SystemPrioritySeparation,				// 39	N		Y
	SystemNotImplemented10,					// 40	Y		N
	SystemNotImplemented11,					// 41	Y		N
	SystemInvalidInfoClass2,				// 42
	SystemInvalidInfoClass3,				// 43
	SystemTimeZoneInformation,				// 44	Y		N
	SystemLookasideInformation,				// 45	Y		N
	SystemSetTimeSlipEvent,					// 46	N		Y
	SystemCreateSession,					// 47	N		Y
	SystemDeleteSession,					// 48	N		Y
	SystemInvalidInfoClass4,				// 49
	SystemRangeStartInformation,			// 50	Y		N
	SystemVerifierInformation,				// 51	Y		Y
	SystemAddVerifier,						// 52	N		Y
	SystemSessionProcessesInformation		// 53	Y		N
} SYSTEM_INFORMATION_CLASS;

typedef enum _PROCESSINFOCLASS {
	ProcessBasicInformation,
	ProcessQuotaLimits,
	ProcessIoCounters,
	ProcessVmCounters,
	ProcessTimes,
	ProcessBasePriority,
	ProcessRaisePriority,
	ProcessDebugPort,
	ProcessExceptionPort,
	ProcessAccessToken,
	ProcessLdtInformation,
	ProcessLdtSize,
	ProcessDefaultHardErrorMode,
	ProcessIoPortHandlers,          // Note: this is kernel mode only
	ProcessPooledUsageAndLimits,
	ProcessWorkingSetWatch,
	ProcessUserModeIOPL,
	ProcessEnableAlignmentFaultFixup,
	ProcessPriorityClass,
	ProcessWx86Information,
	ProcessHandleCount,
	ProcessAffinityMask,
	ProcessPriorityBoost,
	ProcessDeviceMap,
	ProcessSessionInformation,
	ProcessForegroundInformation,
	ProcessWow64Information,
	MaxProcessInfoClass
} PROCESSINFOCLASS;

typedef enum _THREADINFOCLASS {
    ThreadIsIoPending = 16
} THREADINFOCLASS;

extern "C" {

/*
ZwQuerySystemInformation queries information about the system.

Parameters:

SystemInformationClass

  The type of system information to be queried.The permitted values are a subset of
  the enumeration SYSTEM_INFORMATION_CLASS, described in the following section.

SystemInformation

  Points to a caller-allocated buffer or variable that receives the requested system
  information.

SystemInformationLength

  The size in bytes of SystemInformation, which the caller should set according to the
  given SystemInformationClass.

ReturnLength

  Optionally points to a variable that receives the number of bytes actually returned to
  SystemInformation; if SystemInformationLength is too small to contain the available
  information, the variable is normally set to zero except for two information classes
  (6 and 11) when it is set to the number of bytes required for the available information.
  If this information is not needed, ReturnLength may be a null pointer.

Return Value

  Returns STATUS_SUCCESS or an error status, such as STATUS_INVALID_INFO_CLASS,
  STATUS_NOT_IMPLEMENTED or STATUS_INFO_LENGTH_MISMATCH.
*/

NTSYSAPI
NTSTATUS
NTAPI
ZwQuerySystemInformation(
	IN SYSTEM_INFORMATION_CLASS SystemInformationClass,
	IN OUT PVOID SystemInformation,
	IN ULONG SystemInformationLength,
	OUT PULONG ReturnLength OPTIONAL);

/*
NtSetSystemInformation sets information that affects the operation of the system.

Parameters:

SystemInformationClass

  The type of system information to be set.The permitted values are a subset of the
  enumeration SYSTEM_INFORMATION_CLASS, described in the following section.

SystemInformation

  Points to a caller-allocated buffer or variable that contains the system information to
  be set.

SystemInformationLength

  The size in bytes of SystemInformation, which the caller should set according to the
  given SystemInformationClass.

Return Value

  Returns STATUS_SUCCESS or an error status, such as STATUS_INVALID_INFO_CLASS,
  STATUS_NOT_IMPLEMENTED or STATUS_INFO_LENGTH_MISMATCH.
*/

NTSYSAPI
NTSTATUS
NTAPI
NtSetSystemInformation(
	IN SYSTEM_INFORMATION_CLASS SystemInformationClass,
	IN OUT PVOID SystemInformation,
	IN ULONG SystemInformationLength);

NTSYSAPI
NTSTATUS
NTAPI
ZwQueryInformationProcess (
	IN HANDLE ProcessHandle,
	IN PROCESSINFOCLASS ProcessInformationClass,
	OUT PVOID ProcessInformation,
	IN ULONG ProcessInformationLength,
	OUT PULONG ReturnLength OPTIONAL);

}

/*
SystemBasicInformation

Members:

Unknown

  Always contains zero; interpretation unknown.

MaximumIncrement

  The maximum number of 100-nanosecond units between clock ticks. Also the
  number of 100-nanosecond units per clock tick for kernel intervals measured in clock
  ticks.

PhysicalPageSize

  The size in bytes of a physical page.
  NumberOfPhysicalPages
  The number of physical pages managed by the operating system.

LowestPhysicalPage

  The number of the lowest physical page managed by the operating system (numbered
  from zero).

HighestPhysicalPage

  The number of the highest physical page managed by the operating system (numbered
  from zero).

AllocationGranularity

  The granularity to which the base address of virtual memory reservations is rounded.

LowestUserAddress

  The lowest virtual address potentially available to user mode applications.

HighestUserAddress

  The highest virtual address potentially available to user mode applications.

ActiveProcessors

  A bit mask representing the set of active processors in the system. Bit 0 is processor 0;
  bit 31 is processor 31.

NumberProcessors

  The number of processors in the system.
*/

typedef struct _SYSTEM_BASIC_INFORMATION { // Information Class 0
	ULONG Unknown;
	ULONG MaximumIncrement;
	ULONG PhysicalPageSize;
	ULONG NumberOfPhysicalPages;
	ULONG LowestPhysicalPage;
	ULONG HighestPhysicalPage;
	ULONG AllocationGranularity;
	ULONG LowestUserAddress;
	ULONG HighestUserAddress;
	ULONG ActiveProcessors;
	UCHAR NumberProcessors;
} SYSTEM_BASIC_INFORMATION, *PSYSTEM_BASIC_INFORMATION;

/*
SystemProcessorInformation

Members:

ProcessorArchitecture

  The system's processor architecture. Some of the possible values are defined in winnt.h
  with identifiers of the form PROCESSOR_ARCHITECTURE_* (where '*' is a wildcard).

ProcessorLevel

  The system's architecture-dependent processor level. Some of the possible values are
  defined in the Win32 documentation for the SYSTEM_INFO structure.

ProcessorRevision

  The system's architecture-dependent processor revision. Some of the possible values are
  defined in the Win32 documentation for the SYSTEM_INFO structure.

Unknown

  Always contains zero; interpretation unknown.

FeatureBits

  A bit mask representing any special features of the system's processor (for example,
  whether the Intel MMX instruction set is available).The flags for the Intel platform
  include:

	Intel Mnemonic	Value	Description
	-------------------------------------------------------------
	VME				0x0001	Virtual-8086 Mode Enhancements
	TCS				0x0002	Time Stamp Counter
					0x0004	CR4 Register
	CMOV			0x0008	Conditional Mov/Cmp Instruction
	PGE				0x0010	PTE Global Bit
	PSE				0x0020	Page Size Extensions
	MTRR			0x0040	Memory Type Range Registers
	CXS				0x0080	CMPXCHGB8 Instruction
	MMX				0x0100	MMX Technology
	PAT				0x0400	Page Attribute Table
	FXSR			0x0800	Fast Floating Point Save and Restore
	SIMD			0x2000	Streaming SIMD Extension
*/

typedef struct _SYSTEM_PROCESSOR_INFORMATION { // Information Class 1
	USHORT ProcessorArchitecture;
	USHORT ProcessorLevel;
	USHORT ProcessorRevision;
	USHORT Unknown;
	ULONG FeatureBits;
} SYSTEM_PROCESSOR_INFORMATION, *PSYSTEM_PROCESSOR_INFORMATION;

/*
SystemPerformanceInformation

Members:

IdleTime

  The total idle time, measured in units of 100-nanoseconds, of all the processors in the
  system.

ReadTransferCount

  The number of bytes read by all calls to NtReadFile.

WriteTransferCount

  The number of bytes written by all calls to NtWriteFile.

OtherTransferCount

  The number of bytes transferred to satisfy all other I/O operations, such as
  NtDeviceIoControlFile.

ReadOperationCount

  The number of calls to NtReadFile.

WriteOperationCount

  The number of calls to NtWriteFile.

OtherOperationCount

  The number of calls to all other I/O system services such as NtDeviceIoControlFile.

AvailablePages

  The number of pages of physical memory available to processes running on the
  system.

TotalCommittedPages

  The number of pages of committed virtual memory.

TotalCommitLimit

  The number of pages of virtual memory that could be committed without
  extending the system's pagefiles.

PeakCommitment

  The peak number of pages of committed virtual memory.

PageFaults

  The number of page faults (both soft and hard).

WriteCopyFaults

  The number of page faults arising from attempts to write to copy-on-write pages.

TransitionFaults

  The number of soft page faults (excluding demand zero faults).

DemandZeroFaults

  The number of demand zero faults.

PagesRead

  The number of pages read from disk to resolve page faults.

PageReadIos

  The number of read operations initiated to resolve page faults.

PagefilePagesWritten

  The number of pages written to the system's pagefiles.

PagefilePageWriteIos

  The number of write operations performed on the system's pagefiles.

MappedFilePagesWritten

  The number of pages written to mapped files.

MappedFilePageWriteIos

  The number of write operations performed on mapped files.

PagedPoolUsage

  The number of pages of virtual memory used by the paged pool.

NonPagedPoolUsage

  The number of pages of virtual memory used by the nonpaged pool.

PagedPoolAllocs

  The number of allocations made from the paged pool.

PagedPoolFrees

  The number of allocations returned to the paged pool.

NonPagedPoolAllocs

  The number of allocations made from the nonpaged pool.

NonPagedPoolFrees

  The number of allocations returned to the nonpaged pool.

TotalFreeSystemPtes

  The number of available System Page Table Entries.

SystemCodePage

  The number of pages of pageable operating system code and static data in physical
  memory.The meaning of "operating system code and static data" is defined by address
  range (lowest system address to start of system cache) and includes a contribution from
  win32k.sys.

TotalSystemDriverPages

  The number of pages of pageable device driver code and static data.

TotalSystemCodePages

  The number of pages of pageable operating system code and static data.The meaning
  of "operating system code and static data" is defined by load time (SERVICE_BOOT_START
  driver or earlier) and does not include a contribution from win32k.sys.

SmallNonPagedLookasideListAllocateHits

  The number of times an allocation could be satisfied by one of the small nonpaged
  lookaside lists.

SmallPagedLookasideListAllocateHits

  The number of times an allocation could be satisfied by one of the small-paged
  lookaside lists.

MmSystemCachePage

  The number of pages of the system cache in physical memory.

PagedPoolPage

  The number of pages of paged pool in physical memory.

SystemDriverPage

  The number of pages of pageable device driver code and static data in physical
  memory.

FastReadNoWait

  The number of asynchronous fast read operations.

FastReadWait

  The number of synchronous fast read operations.

FastReadResourceMiss

  The number of fast read operations not possible because of resource conflicts.

FastReadNotPossible

  The number of fast read operations not possible because file system intervention
  required.

FastMdlReadNoWait

  The number of asynchronous fast read operations requesting a Memory Descriptor
  List (MDL) for the data.

FastMdlReadWait

  The number of synchronous fast read operations requesting an MDL for the data.

FastMdlReadResourceMiss

  The number of synchronous fast read operations requesting an MDL for the data not
  possible because of resource conflicts.

FastMdlReadNotPossible

  The number of synchronous fast read operations requesting an MDL for the data not
  possible because file system intervention required.

MapDataNoWait

  The number of asynchronous data map operations.

MapDataWait

  The number of synchronous data map operations.

MapDataNoWaitMiss

  The number of asynchronous data map operations that incurred page faults.

MapDataWaitMiss

  The number of synchronous data map operations that incurred page faults.

PinMappedDataCount

  The number of requests to pin mapped data.

PinReadNoWait

  The number of asynchronous requests to pin mapped data.

PinReadWait

  The number of synchronous requests to pin mapped data.

PinReadNoWaitMiss

  The number of asynchronous requests to pin mapped data that incurred page faults
  when pinning the data.

PinReadWaitMiss

  The number of synchronous requests to pin mapped data that incurred page faults
  when pinning the data.

CopyReadNoWait

  The number of asynchronous copy read operations.

CopyReadWait

  The number of synchronous copy read operations.

CopyReadNoWaitMiss

  The number of asynchronous copy read operations that incurred page faults when
  reading from the cache.

CopyReadWaitMiss

  The number of synchronous copy read operations that incurred page faults when
  reading from the cache.

MdlReadNoWait

  The number of synchronous read operations requesting an MDL for the cached data.

MdlReadWait

  The number of synchronous read operations requesting an MDL for the cached data.

MdlReadNoWaitMiss

  The number of synchronous read operations requesting an MDL for the cached data
  that incurred page faults.

MdlReadWaitMiss

  The number of synchronous read operations requesting an MDL for the cached data
  that incurred page faults.

ReadAheadIos

  The number of read ahead operations performed in anticipation of sequential access.

LazyWriteIos

  The number of write operations initiated by the Lazy Writer.

LazyWritePages

  The number of pages written by the Lazy Writer.

DataFlushes

  The number of cache flushes in response to flush requests.

DataPages

  The number of cache pages flushed in response to flush requests.

ContextSwitches

  The number of context switches.

FirstLevelTbFills

  The number of first level translation buffer fills.

SecondLevelTbFills

  The number of second level translation buffer fills.

SystemCalls

  The number of system calls executed.
*/

typedef struct _SYSTEM_PERFORMANCE_INFORMATION { // Information Class 2
	LARGE_INTEGER IdleTime;
	LARGE_INTEGER ReadTransferCount;
	LARGE_INTEGER WriteTransferCount;
	LARGE_INTEGER OtherTransferCount;
	ULONG ReadOperationCount;
	ULONG WriteOperationCount;
	ULONG OtherOperationCount;
	ULONG AvailablePages;
	ULONG TotalCommittedPages;
	ULONG TotalCommitLimit;
	ULONG PeakCommitment;
	ULONG PageFaults;
	ULONG WriteCopyFaults;
	ULONG TransitionFaults;
	ULONG Reserved1;
	ULONG DemandZeroFaults;
	ULONG PagesRead;
	ULONG PageReadIos;
	ULONG Reserved2[2];
	ULONG PagefilePagesWritten;
	ULONG PagefilePageWriteIos;
	ULONG MappedFilePagesWritten;
	ULONG MappedFilePageWriteIos;
	ULONG PagedPoolUsage;
	ULONG NonPagedPoolUsage;
	ULONG PagedPoolAllocs;
	ULONG PagedPoolFrees;
	ULONG NonPagedPoolAllocs;
	ULONG NonPagedPoolFrees;
	ULONG TotalFreeSystemPtes;
	ULONG SystemCodePage;
	ULONG TotalSystemDriverPages;
	ULONG TotalSystemCodePages;
	ULONG SmallNonPagedLookasideListAllocateHits;
	ULONG SmallPagedLookasideListAllocateHits;
	ULONG Reserved3;
	ULONG MmSystemCachePage;
	ULONG PagedPoolPage;
	ULONG SystemDriverPage;
	ULONG FastReadNoWait;
	ULONG FastReadWait;
	ULONG FastReadResourceMiss;
	ULONG FastReadNotPossible;
	ULONG FastMdlReadNoWait;
	ULONG FastMdlReadWait;
	ULONG FastMdlReadResourceMiss;
	ULONG FastMdlReadNotPossible;
	ULONG MapDataNoWait;
	ULONG MapDataWait;
	ULONG MapDataNoWaitMiss;
	ULONG MapDataWaitMiss;
	ULONG PinMappedDataCount;
	ULONG PinReadNoWait;
	ULONG PinReadWait;
	ULONG PinReadNoWaitMiss;
	ULONG PinReadWaitMiss;
	ULONG CopyReadNoWait;
	ULONG CopyReadWait;
	ULONG CopyReadNoWaitMiss;
	ULONG CopyReadWaitMiss;
	ULONG MdlReadNoWait;
	ULONG MdlReadWait;
	ULONG MdlReadNoWaitMiss;
	ULONG MdlReadWaitMiss;
	ULONG ReadAheadIos;
	ULONG LazyWriteIos;
	ULONG LazyWritePages;
	ULONG DataFlushes;
	ULONG DataPages;
	ULONG ContextSwitches;
	ULONG FirstLevelTbFills;
	ULONG SecondLevelTbFills;
	ULONG SystemCalls;
} SYSTEM_PERFORMANCE_INFORMATION, *PSYSTEM_PERFORMANCE_INFORMATION;

typedef struct _UNICODE_STRING {
    USHORT Length;
    USHORT MaximumLength;
    PWSTR  Buffer;
} UNICODE_STRING;
typedef UNICODE_STRING *PUNICODE_STRING;
typedef const UNICODE_STRING *PCUNICODE_STRING;

typedef struct _CLIENT_ID {
    DWORD UniqueProcess;
    DWORD UniqueThread;
} CLIENT_ID;

typedef struct _VM_COUNTERS {
    SIZE_T PeakVirtualSize;
    SIZE_T VirtualSize;
    ULONG PageFaultCount;
    SIZE_T PeakWorkingSetSize;
    SIZE_T WorkingSetSize;
    SIZE_T QuotaPeakPagedPoolUsage;
    SIZE_T QuotaPagedPoolUsage;
    SIZE_T QuotaPeakNonPagedPoolUsage;
    SIZE_T QuotaNonPagedPoolUsage;
    SIZE_T PagefileUsage;
    SIZE_T PeakPagefileUsage;
} VM_COUNTERS;

typedef struct _SYSTEM_THREADS {
    LARGE_INTEGER KernelTime;
    LARGE_INTEGER UserTime;
    LARGE_INTEGER CreateTime;
    ULONG WaitTime;
    PVOID StartAddress;
    CLIENT_ID ClientId;
    KPRIORITY Priority;
    KPRIORITY BasePriority;
    ULONG ContextSwitchCount;
    LONG State;
    LONG WaitReason;
} SYSTEM_THREADS, *PSYSTEM_THREADS;

typedef struct _SYSTEM_PROCESSES {
    ULONG NextEntryDelta;
    ULONG ThreadCount;
    ULONG Reserved1[6];
    LARGE_INTEGER CreateTime;
    LARGE_INTEGER UserTime;
    LARGE_INTEGER KernelTime;
    UNICODE_STRING ProcessName;
    KPRIORITY BasePriority;
    ULONG ProcessId;
    ULONG InheritedFromProcessId;
    ULONG HandleCount;
    ULONG Reserved2[2];
    VM_COUNTERS VmCounters;
#if _WIN32_WINNT >= 0x500
    IO_COUNTERS IoCounters;
#endif
    SYSTEM_THREADS Threads[1];
} SYSTEM_PROCESSES, *PSYSTEM_PROCESSES;

typedef struct _THREAD_BASIC_INFORMATION {
	NTSTATUS ExitStatus;
	PNT_TIB TebBaseAddress;
	CLIENT_ID ClientId;
	KAFFINITY AffinityMask;
	KPRIORITY Priority;
	KPRIORITY BasePriority;
} THREAD_BASIC_INFORMATION, *PTHREAD_BASIC_INFORMATION;


class CNtAPIHelper
{
private:
	typedef NTSTATUS (NTAPI *PZWQUERYSYSTEMINFORMATION)(
		IN		SYSTEM_INFORMATION_CLASS	SystemInformationClass,
		IN OUT	PVOID						SystemInformation,
		IN		ULONG						SystemInformationLength,
		OUT		PULONG						ReturnLength OPTIONAL);

	typedef NTSTATUS (NTAPI *PZWQUERYINFORMATIONPROCESS)(
		IN		HANDLE				ProcessHandle,
		IN		PROCESSINFOCLASS	ProcessInformationClass,
		OUT		PVOID				ProcessInformation,
		IN		ULONG				ProcessInformationLength,
		OUT		PULONG				ReturnLength OPTIONAL);

	HMODULE						m_hNtDLL;
	PZWQUERYSYSTEMINFORMATION	m_pZwQuerySystemInformation;
	PZWQUERYINFORMATIONPROCESS	m_pZwQueryInformationProcess;

public:
	CNtAPIHelper() :
	  m_hNtDLL(NULL),
		  m_pZwQuerySystemInformation(NULL),
		  m_pZwQueryInformationProcess(NULL)
	  {
		  if ((m_hNtDLL = ::LoadLibraryW(L"NTDLL.DLL")) != NULL)
		  {
			  *(FARPROC*)&m_pZwQuerySystemInformation		= ::GetProcAddress(m_hNtDLL, "ZwQuerySystemInformation");
			  *(FARPROC*)&m_pZwQueryInformationProcess	= ::GetProcAddress(m_hNtDLL, "ZwQueryInformationProcess");
		  }
	  }
	  ~CNtAPIHelper()
	  {
		  if (m_hNtDLL != NULL)
		  {
			  ::FreeLibrary(m_hNtDLL);
		  }
	  }
	  NTSTATUS ZwQuerySystemInformation(IN		SYSTEM_INFORMATION_CLASS	SystemInformationClass, 
										IN OUT	PVOID						SystemInformation, 
										IN		ULONG						SystemInformationLength, 
										OUT		PULONG						ReturnLength OPTIONAL)
	  {
		  if (m_pZwQuerySystemInformation)
		  {
			  return m_pZwQuerySystemInformation(
				  SystemInformationClass, 
				  SystemInformation, 
				  SystemInformationLength, 
				  ReturnLength);
		  }
		  return STATUS_INVALID_PARAMETER;
	  }
	  NTSTATUS ZwQueryInformationProcess(IN		HANDLE				ProcessHandle,
										 IN		PROCESSINFOCLASS	ProcessInformationClass,
										 OUT	PVOID				ProcessInformation,
										 IN		ULONG				ProcessInformationLength,
										 OUT	PULONG				ReturnLength OPTIONAL)
	  {
		  if (m_pZwQueryInformationProcess)
		  {
			  return m_pZwQueryInformationProcess(
				  ProcessHandle, 
				  ProcessInformationClass, 
				  ProcessInformation, 
				  ProcessInformationLength,
				  ReturnLength);
		  }
		  return STATUS_INVALID_PARAMETER;
	  }
};

class CNtAPISingleton : public CMeyerSingleton<CNtAPIHelper>
{
};
