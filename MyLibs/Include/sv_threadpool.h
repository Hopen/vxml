/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include "sv_handle.h"


namespace SVThreadPool
{

using namespace SVHandle;

class CThreadPoolJob
{
private:
	bool m_bDeleteJob;

public:
	CThreadPoolJob(bool bDeleteJob = true) 
	{ 
		m_bDeleteJob = bDeleteJob;
	}
	virtual ~CThreadPoolJob() { }
	virtual void Execute() = 0;
	bool CanDeleteJob() const
	{
		return m_bDeleteJob;
	}
};

struct COverlappedJob;

class COverlappedResultHandler
{
protected:
	COverlappedJob* m_pJob;

public:
	COverlappedResultHandler(COverlappedJob* pJob)
		: m_pJob(pJob) { }
	virtual ~COverlappedResultHandler() { }
	virtual void BeginIO() { }
	virtual void OnIOCompleted() = 0;
	virtual bool CanDeleteJob() { return true; }
};

struct COverlappedJob : public OVERLAPPED
{
	COverlappedResultHandler* pHandler;
	COverlappedJob()
	{
		Internal = InternalHigh = 0;
		Offset = OffsetHigh = 0;
		hEvent = NULL;
	}
};

class COverlappedResultHandler;

class CThreadPool
{
protected:
	CHandle		m_hCompletitionPort;
	CHandle		m_hExitEvent;

	DWORD		m_dwThreadsMin;
	DWORD		m_dwThreadsMax;
	DWORD		m_dwCPULoThreshold;
	DWORD		m_dwCPUHiThreshold;

	LONG		m_nBusyThreads;
	LONG		m_nWorkerThreads;

	FILETIME	m_ftOldTime;
	FILETIME	m_ftOldCPUTime;

	static DWORD WINAPI ThreadFunc(CThreadPool* pThreadPool)
	{
		return pThreadPool->ThreadPoolFunc();
	}

	void ResizePool(LONG nThreadCount);
	void AddWorkerThread();

	void ProcessJob(CThreadPoolJob* pJob);
	void ProcessJob(COverlappedJob* pJob);

	DWORD ThreadPoolFunc();
	DWORD GetCPUUsage();

public:
	CThreadPool();
	virtual ~CThreadPool();

	bool Initialize();
	bool Shutdown();
	void WaitFor(DWORD dwTimeout = INFINITE);

	bool ExecuteJob(CThreadPoolJob* pJob);
	bool ExecuteJob(COverlappedJob* pJob, HANDLE hWatchHandle);

	void SetThreadPoolMin(int nMin) { m_dwThreadsMin = nMin; }
	void SetThreadPoolMax(int nMax) { m_dwThreadsMax = nMax; }
	void SetCPULoThreshold(int nThreshold) { m_dwCPULoThreshold = nThreshold; }
	void SetCPUHiThreshold(int nThreshold) { m_dwCPUHiThreshold = nThreshold; }
};

}
