/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include <string>
#include <vector>
#include <map>
#include "sv_handle.h"
#include "sv_thread.h"

namespace SVService
{

using namespace std;
using namespace SVHandle;
using namespace SVThread;

CSvcHandle SCMConnect(DWORD dwDesiredAccess, 
					  LPCWSTR pwszMachineName = NULL, 
					  LPCWSTR pwszUserName = NULL,
					  LPCWSTR pwszPassword = NULL);


/************************************************************************/
/* ����� CServiceManager                                                */
/************************************************************************/

class CServiceManager
{
private:
	CSvcHandle m_hSCM;

	CSvcHandle OpenInt(DWORD dwDesiredAccess) const
	{
		if (!m_hSCM)
		{
			return SCMConnect(dwDesiredAccess);
		}
		return m_hSCM;
	}

	wstring GetSvcName(
		const wstring& sKey,
		BOOL (WINAPI *GetNameFunc)(SC_HANDLE, LPCWSTR, LPWSTR, LPDWORD)) const;

public:
	void Open(DWORD dwDesiredAccess = SC_MANAGER_ALL_ACCESS, 
			  LPCWSTR pwszMachineName = NULL, 
			  LPCWSTR pwszUserName = NULL,
			  LPCWSTR pwszPassword = NULL)
	{
		m_hSCM = SCMConnect(
			dwDesiredAccess, 
			pwszMachineName, 
			pwszUserName, 
			pwszPassword);
	}
	void Close()
	{
		m_hSCM.Close();
	}

	wstring GetSvcKeyName(const wstring& sDisplayName) const
	{
		return GetSvcName(sDisplayName, ::GetServiceKeyNameW);
	}
	wstring GetSvcDisplayName(const wstring& sKeyName) const
	{
		return GetSvcName(sKeyName, ::GetServiceDisplayNameW);
	}
	wstring GetSvcDescription(const wstring& sSvcName) const;

	void EnumServices(vector<wstring>& services,
					  DWORD dwServiceState = SERVICE_STATE_ALL, 
					  DWORD dwServiceType = SERVICE_WIN32);

	CSvcHandle ServiceInstall(
		const wstring& sServiceName,
		const wstring& sDisplayName,
		const wstring& sBinaryPathName,
		DWORD dwDesiredAccess = SERVICE_ALL_ACCESS,
		DWORD dwServiceType = SERVICE_WIN32_OWN_PROCESS,
		DWORD dwStartType = SERVICE_AUTO_START,
		DWORD dwErrorControl = SERVICE_ERROR_NORMAL,
		const wstring& sLoadOrderGroup = L"",
		vector<wstring> dependencies = vector<wstring>(),
		const wstring& sServiceStartName = L"",
		const wstring& sPassword = L"");
	void ServiceUninstall(const wstring& sSvcName);

	CSvcHandle ServiceOpen(const wstring& sSvcName, DWORD dwDesiredAccess);
};

/************************************************************************/
/* ����� CServiceControl                                                */
/************************************************************************/

class CServiceControl
{
public:
	struct Config
	{
		struct FAILUREACTIONS
		{
			DWORD dwResetPeriod;
			wstring sRebootMsg;
			wstring sCommand;
			vector<SC_ACTION> Actions;
		};

		DWORD dwServiceType;
		DWORD dwStartType;
		DWORD dwErrorControl;
		wstring sServiceName;
		wstring sDisplayName;
		wstring sBinaryPathName;
		wstring sLoadOrderGroup;
		vector<wstring> dependencies;
		wstring sServiceStartName;

		wstring sDescription;
		FAILUREACTIONS FailureActions;
	};

	enum eChangeConfigType
	{
		ctServiceType		= 0x0001,
		ctStartType			= 0x0002,
		ctErrorControl		= 0x0004,
		ctBinaryPathName	= 0x0008,
		ctLoadOrderGroup	= 0x0010,
		ctDependencies		= 0x0020,
		ctServiceStartName	= 0x0040,
		ctDisplayName		= 0x0080,
		ctDescription		= 0x0100,
		ctFailureActions	= 0x0200,
		ctStandard			= 0x00FF,
		ctAll				= 0x03FF 
	};

private:
	CSvcHandle m_hService;

	auto_ptr<BYTE> GetConfig2(DWORD dwInfoLevel) const;

public:
	CServiceControl(const CSvcHandle& hService) :
		m_hService(hService) { }
	CSvcHandle GetHandle() const
	{
		return m_hService;
	}
	Config GetConfig() const;
	bool SetConfig(const Config& config, int nChangeType = ctAll, LPCWSTR pwszPassword = L"");
	SERVICE_STATUS_PROCESS GetStatus() const;
	bool Start(vector<wstring>& args = vector<wstring>());
	bool Stop(DWORD dwTimeout = INFINITE)
	{
		return Control(SERVICE_CONTROL_STOP, SERVICE_STOPPED, dwTimeout);
	}
	bool Pause(DWORD dwTimeout = INFINITE)
	{
		return Control(SERVICE_CONTROL_PAUSE, SERVICE_PAUSED, dwTimeout);
	}
	bool Continue(DWORD dwTimeout = INFINITE)
	{
		return Control(SERVICE_CONTROL_CONTINUE, SERVICE_RUNNING, dwTimeout);
	}
	bool Control(DWORD dwControl, DWORD dwState, DWORD dwTimeout = INFINITE);
	bool StopWithDependents();
};

/************************************************************************/
/* ����� CNTService                                                     */
/************************************************************************/

class CNTService
{
	friend class CServiceDispatcher;
private:
	SERVICE_STATUS_HANDLE m_hStatus;
	CThread m_ConsoleThread;
	CThread m_PendingThread;
	CHandle m_hPendingEvent;
	DWORD m_dwPendingCode;

	wstring m_sServiceName;
	wstring m_sDisplayName;
	wstring m_sDescription;

	void Init();

	void ServiceMain(DWORD dwArgc, LPWSTR* lpszArgv);
	void ServiceMain();

	static DWORD WINAPI HandlerEx(DWORD dwControl,
								  DWORD dwEventType,
								  LPVOID lpEventData,
								  CNTService* pService);

	static DWORD WINAPI ConsoleThreadProc(CNTService* pService);
	static DWORD WINAPI PendingThreadProc(CNTService* pService);
	
	DWORD ConsoleProc();
	DWORD PendingProc();

protected:
	typedef vector<wstring> ARGS;

	ARGS m_Args;
	CHandle m_hExitEvent;
	SERVICE_STATUS m_Status;

	void StartPending(DWORD dwPendingCode);
	void StopPending();
	bool SetStatus(DWORD dwCurrentState);

	virtual bool OnInit() { return true; }
	virtual void Work() = 0;
	virtual void OnStop() { }
	
public:
	CNTService(const wstring& sName, 
			   const wstring& sDisplayName, 
			   const wstring& sDescription);
	virtual ~CNTService() { }
	void Install();
	void Uninstall();
	void Start();
	void Stop();
	void StartConsole();
	void StopConsole();
	LPCWSTR GetServiceName() const
	{
		return m_sServiceName.c_str();
	}
};

/************************************************************************/
/* ����� CServiceDispatcher                                             */
/************************************************************************/

class CServiceDispatcher
{
private:
	typedef map<wstring,CNTService*> SVCMAP;

	static BOOL WINAPI ConsoleCtrlHandler(DWORD dwCtrlType);
	static VOID WINAPI ServiceMain(DWORD dwArgc, LPWSTR* lpszArgv);

	SVCMAP m_SvcMap;

public:
	CServiceDispatcher();
	~CServiceDispatcher();
	void AddService(LPCWSTR pwszSvcName, CNTService* pSvc);
	bool Start();
	void StartConsole();
};

extern CServiceDispatcher* g_pSvcDispatcher;

}

extern int BaseSvcStartup(SVService::CNTService* pService, int argc, wchar_t* argv[]);
