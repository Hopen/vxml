/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include <memory>
#include <string>
#include <list>
#include <set>
#include "sv_sysobj.h"

namespace SVLog
{

/************************************************************************/
/* ���������� ����                                                      */
/************************************************************************/

class IBinaryWriter
{
public:
	virtual	~IBinaryWriter() { }
	virtual	bool Write(LPCVOID lpData, DWORD cbData) = NULL;
	virtual	ULONGLONG GetSize() const = NULL;
	virtual	void Close() = NULL;
	virtual	bool Open(LPCWSTR pwszName) = NULL;
};

class ISplitCondition
{
public:
	virtual	~ISplitCondition() { }
	virtual	bool NeedSplit(FILETIME ftLastSplit, ULONGLONG nCurrentSize) = NULL;
};

class INameGenerator
{
public:
	virtual	~INameGenerator() { }
	virtual	std::wstring GenerateName() = NULL;
};

class IFormatter
{
public:
	virtual	~IFormatter() { }
	virtual	std::string GetData() = NULL;
	virtual int GetDataLen() const = NULL;
};

class ILogFilter
{
public:
	virtual	~ILogFilter() { }
	virtual	bool Reject(int nLogType) = NULL;
};

/************************************************************************/
/* ����� CBinarySplitter                                                */
/************************************************************************/

class CBinarySplitter
{
private:
	class CondPtr : public std::auto_ptr<ISplitCondition>
	{
	public:
		CondPtr(ISplitCondition* p) : std::auto_ptr<ISplitCondition>(p) { }
		CondPtr(const CondPtr& ptr) : std::auto_ptr<ISplitCondition>(((CondPtr&)ptr).release()) { }
	};

	typedef	std::list<CondPtr> SPLITCOND;

	std::auto_ptr<IBinaryWriter>	m_pBinaryWriter;
	std::auto_ptr<INameGenerator>	m_pNameGenerator;
	SPLITCOND						m_SplitCond;
	FILETIME						m_ftLastSplit;

	bool NeedSplit();
	void Split();

	CBinarySplitter(const CBinarySplitter&) { }

public:
	CBinarySplitter()
	{
		ZeroMemory(&m_ftLastSplit, sizeof(m_ftLastSplit));
	}
	~CBinarySplitter()
	{
		m_pBinaryWriter.reset(NULL);
		m_pNameGenerator.reset(NULL);
	}
	void AddSplitCondition(ISplitCondition*	pSplitCond)
	{
		m_SplitCond.push_back(CondPtr(pSplitCond));
	}
	void ClearSplitCond()
	{
		m_SplitCond.clear();
	}
	void SetNameGenerator(INameGenerator* pNameGenerator)
	{
		m_pNameGenerator.reset(pNameGenerator);
	}
	void SetBinaryWriter(IBinaryWriter*	pBinaryWriter)
	{
		m_pBinaryWriter.reset(pBinaryWriter);
	}
	void Close()
	{
		ClearSplitCond();
		m_pBinaryWriter->Close();
	}
	bool Write(IFormatter* pFmt);
};

/************************************************************************/
/* ������ ������� ����������                                            */
/************************************************************************/

class CTimeOfDaySplitCondition : public ISplitCondition
{
private:
	FILETIME m_ftTimeOfDay;

public:
	CTimeOfDaySplitCondition(FILETIME ftTimeOfDay) :
		m_ftTimeOfDay(ftTimeOfDay)
	{
	}
	virtual	bool NeedSplit(FILETIME ftLastSplit, ULONGLONG nCurrentSize);
};

class CSizeLimitSplitCondition : public ISplitCondition
{
private:
	ULONGLONG m_nSize;

public:
	CSizeLimitSplitCondition(ULONGLONG nSize) : m_nSize(nSize)
	{
	}
	virtual	bool NeedSplit(FILETIME /*ftLastSplit*/, ULONGLONG nCurrentSize)
	{
		return nCurrentSize > m_nSize;
	}
};

class CTimeSpanSplitCondition : public ISplitCondition
{
private:
	FILETIME m_ftTimeSpan;

public:
	CTimeSpanSplitCondition(FILETIME ftTimeSpan) : 
		m_ftTimeSpan(ftTimeSpan)
	{
	}
	virtual	bool NeedSplit(FILETIME ftLastSplit, ULONGLONG nCurrentSize);
};

class CDaySplitCondition : public ISplitCondition
{
public:
	virtual	bool NeedSplit(FILETIME ftLastSplit, ULONGLONG nCurrentSize);
};

/************************************************************************/
/* ����� CDateTimeNameGenerator                                         */
/************************************************************************/

class CDateTimeNameGenerator : public INameGenerator
{
private:
	std::wstring m_sFormat;

public:
	CDateTimeNameGenerator(LPCWSTR pwszFormat) : m_sFormat(pwszFormat)
	{
	}
	virtual	std::wstring GenerateName();
};

/************************************************************************/
/* ����� CSimpleWriter                                                  */
/************************************************************************/

class CSimpleWriter : public IBinaryWriter
{
private:
	SVHandle::CHandle	m_hFile;
	SVHandle::CHandle	m_hThread;
	DWORD				m_dwThreadId;
	SVSysObj::CMutex	m_SplitMutex;
	SVSysObj::CEvent	m_MQEvent;

	static DWORD WINAPI ThreadProcStub(CSimpleWriter* pWriter)
	{
		return pWriter->ThreadProc();
	}
	
	void CreateThread();
	void DestroyThread();
	bool PostWriteData(LPCVOID lpData, DWORD cbData);
	DWORD ThreadProc();

public:
	CSimpleWriter();
	~CSimpleWriter();

	virtual	bool Write(LPCVOID lpData, DWORD cbData);
	virtual	ULONGLONG GetSize() const;
	virtual	void Close();
	virtual	bool Open(LPCWSTR pwsName);
};

/************************************************************************/
/* ����� CStdLogFormatter                                               */
/************************************************************************/

class CStdLogFormatter : public IFormatter
{
public:
	enum ePrefixType
	{
		ptDate		= 1,
		ptTime		= 2,
		ptPID		= 4,
		ptTID		= 8
	};

private:
	std::wstring	m_wsBuf;
	DWORD			m_dwPrefixFlags;

protected:
	virtual void WriteUserPrefix(DWORD /*dwPrefix*/)
	{
	}

public:
	CStdLogFormatter() : m_dwPrefixFlags(0)
	{
	}
	void SetPrefix(DWORD dwPrefixFlags)
	{
		m_dwPrefixFlags = dwPrefixFlags;
	}
	void Clear()
	{
		m_wsBuf = L"";
	}
	
	CStdLogFormatter& operator +=(const std::wstring& s);
	CStdLogFormatter& FormatV(LPCWSTR pwszFmt, va_list args);
	CStdLogFormatter& Format(LPCWSTR pwszFmt, ...);

	void WritePrefix();
	void WriteEndl();
	void WriteConsole();

	virtual	std::string GetData();
	virtual int GetDataLen() const
	{
		return (int)m_wsBuf.length();
	}
};

/************************************************************************/
/* ����� CStdLogFilter                                                  */
/************************************************************************/

class CStdLogFilter : public ILogFilter
{
private:
	std::set<int> m_Filter;

public:
	void AddLogType(int nLogType)
	{
		m_Filter.insert(nLogType);
	}
	virtual	bool Reject(int nLogType)
	{
		return m_Filter.find(nLogType) != m_Filter.end();
	}
};

}
