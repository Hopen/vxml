/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

class CPipedProcess
{
private:
	HANDLE	m_hProcess;
	HANDLE	m_hRead;
	HANDLE	m_hWrite;
	HANDLE	m_hIoThread;
	DWORD	m_dwIoTID;

	static DWORD WINAPI IoThreadProc(LPVOID lpParam);
	DWORD IoProc();

	void CloseHandle(HANDLE& h);
	bool CreatePipe(LPCWSTR pwszName, PHANDLE phRead, PHANDLE phWrite);

protected:
	virtual void OnRead(LPCSTR pszData, DWORD dwSize) = NULL;
	virtual void OnClose() 
	{
	}

public:
	CPipedProcess();
	virtual ~CPipedProcess();
	bool Create(LPCWSTR pwszCmdLine);
	void Destroy();
	bool Write(LPCSTR pszCommand);
	bool WaitForProcess(DWORD dwTimeout = INFINITE);
};
