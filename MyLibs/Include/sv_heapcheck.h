/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include "sv_helpclasses.h"

#define DEF_HEAP_FLAGS			0
#define DEF_CALLSTACK_DEEP		32
#define DEF_CHECK_SIZE			32
#define HEAP_CHECK_BYTE			0xFD

namespace SVHeapCheck
{

class CHeapChecker
{
	friend bool CheckSignature(LPCVOID lpMem);
	friend LPVOID __stdcall HeapAlloc_Stub(HANDLE hHeap, DWORD dwFlags, SIZE_T dwBytes);
	friend LPVOID __stdcall HeapReAlloc_Stub(HANDLE hHeap, DWORD dwFlags, LPVOID lpMem, DWORD dwBytes);
	friend BOOL __stdcall HeapFree_Stub(HANDLE hHeap, DWORD dwFlags, LPVOID lpMem);
	friend SIZE_T __stdcall HeapSize_Stub(HANDLE hHeap, DWORD dwFlags, LPCVOID lpMem);
	friend BOOL __stdcall HeapValidate_Stub(HANDLE hHeap, DWORD dwFlags, LPCVOID lpMem);

private:

	struct ALLOC_INFO
	{
		ULONGLONG	ullSignature;
		DWORD		dwTID;
		DWORD		dwAllocSize;
		DWORD		dwAllocFlags;
		FILETIME	ftTimeStamp;
	};

#pragma pack(push)
#pragma pack(1)

	struct PATCH_CODE
	{
		BYTE	cmdPush;
		LPVOID	procAddr;
		BYTE	cmdRet;
	};

	struct ORIG_THUNK
	{
		BYTE	cmdJmp;
		DWORD	procOffs;
	};

#pragma pack(pop)

	class CTlsValue
	{
	private:
		DWORD m_dwIndex;

	public:
		CTlsValue(DWORD dwIndex) : m_dwIndex(dwIndex)
		{
			::TlsSetValue(m_dwIndex, (LPBYTE)::TlsGetValue(m_dwIndex) + 1);
		}
		~CTlsValue()
		{
			::TlsSetValue(m_dwIndex, (LPBYTE)::TlsGetValue(m_dwIndex) - 1);
		}
	};

	typedef LPVOID (__stdcall *PHEAPALLOC)(HANDLE hHeap, DWORD dwFlags, SIZE_T dwBytes);
	typedef LPVOID (__stdcall *PHEAPREALLOC)(HANDLE hHeap, DWORD dwFlags, LPVOID lpMem, SIZE_T dwBytes);
	typedef BOOL   (__stdcall *PHEAPFREE)(HANDLE hHeap, DWORD dwFlags, LPVOID lpMem);
	typedef SIZE_T (__stdcall *PHEAPSIZE)(HANDLE hHeap, DWORD dwFlags, LPCVOID lpMem);
	typedef BOOL (__stdcall *PHEAPVALIDATE)(HANDLE hHeap, DWORD dwFlags, LPCVOID lpMem);

	PHEAPALLOC		Orig_HeapAlloc;
	PHEAPREALLOC	Orig_HeapReAlloc;
	PHEAPFREE		Orig_HeapFree;
	PHEAPSIZE		Orig_HeapSize;
	PHEAPVALIDATE	Orig_HeapValidate;

	LPBYTE			m_pOrigCode;
	HANDLE			m_hFile;
	CRITICAL_SECTION m_cs;
	DWORD			m_dwTLSIndex;

	bool			m_bLogHeapCalls;
	DWORD			m_dwHeapFlags;
	DWORD			m_dwCallStackDeep;
	DWORD			m_dwCheckSize;

	void InstallStubProcs();
	void UninstallStubProcs();

	void InstallCodeStub(LPVOID pProcPtr, LPVOID pStubPtr, LPVOID pOrigCode, DWORD dwOrigSize);
	void Log(LPCSTR pszFunc, LPCSTR pszFmt, ...);

	void HeapAllocCommon(DWORD dwFlags, LPVOID lpMem, DWORD dwBytes);
	LPVOID HeapAlloc(HANDLE hHeap, DWORD dwFlags, SIZE_T dwBytes);
	LPVOID HeapReAlloc(HANDLE hHeap, DWORD dwFlags, LPVOID lpMem, SIZE_T dwBytes);
	BOOL   HeapFree(HANDLE hHeap, DWORD dwFlags, LPVOID lpMem);
	SIZE_T HeapSize(HANDLE hHeap, DWORD dwFlags, LPCVOID lpMem);
	BOOL HeapValidate(HANDLE hHeap, DWORD dwFlags, LPCVOID lpMem);

private:
	CHeapChecker();
	~CHeapChecker();

public:
	void Init(LPCWSTR pwszLogFile,
			  bool bLogHeapCalls	= false, 
			  DWORD dwHeapFlags		= DEF_HEAP_FLAGS, 
			  DWORD dwCallStackDeep	= DEF_CALLSTACK_DEEP, 
			  DWORD dwCheckSize		= DEF_CHECK_SIZE);

	friend class CMeyerSingleton<CHeapChecker>;
};

typedef CMeyerSingleton<CHeapChecker> CHeapCheckerSingleton;

}
