/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include <string>

#define EXCEPTION_LOCATION				__FILE__, __LINE__, __FUNCTION__

#define THROW_OSERROR(code)				throw SVExceptions::COSErrorException(EXCEPTION_LOCATION, code)
#define THROW_LASTERR					throw SVExceptions::COSErrorException(EXCEPTION_LOCATION, GetLastError())
#define THROW_ERROR(msg)				throw SVExceptions::CErrorException(EXCEPTION_LOCATION, msg)
#define CHECK_HR(EXPR)					{ HRESULT hr = EXPR; if (FAILED(hr)) THROW_OSERROR(hr); }

/************************************************************************/
/* Additional exception codes                                           */
/************************************************************************/

// Common
#define EXCEPTION_NO_MEMORY				STATUS_NO_MEMORY
#define EXCEPTION_CONTROL_C				0x40010005	// Control C
#define EXCEPTION_CONTROL_BREAK			0x40010008	// Control-Break
#define EXCEPTION_DLL_NOT_FOUND			0xC0000135	// Dll Not Found
#define EXCEPTION_DLL_INIT_FAILED		0xC0000142	// Dll Initialization failed
#define EXCEPTION_MODULE_NOT_FOUND		0xC06D007E	// Module Not Found
#define EXCEPTION_PROC_NOT_FOUND		0xC06D007F	// Procedure Not Found

// Language specific - Microsoft
#define EXCEPTION_MICROSOFT_CPP			0xE06D7363	// Microsoft C++ Exception

// Language specific - Borland
#define EXCEPTION_BORLAND_CPP1			0x0EEDFAE6	// C++Builder Exception #1
#define EXCEPTION_BORLAND_CPP2			0x0EEDFADE	// C++Builder Exception #2
#define EXCEPTION_BORLAND_CPP3			0x0EEFFACE	// C++Builder Exception #3
#define EXCEPTION_BORLAND_DELPHI		0x0EEDFACE	// C++Builder Exception #3

namespace SVExceptions
{

LPCWSTR GetExceptionName(DWORD dwExcCode);
LPCWSTR GetExceptionDescr(DWORD dwExcCode);

/************************************************************************/
/* Exception classes                                                    */
/************************************************************************/

// Base exception class
class CException
{
private:
	LPCSTR		m_pszFile;
	LPCSTR		m_pszFunc;
	int			m_nLine;

public:
	CException() : 
		m_pszFile(""),
		m_pszFunc(""),
		m_nLine(0)
	{
	}
	CException(LPCSTR pszFile, int nLine, LPCSTR pszFunc) :
		m_pszFile(pszFile),
		m_pszFunc(pszFunc),
		m_nLine(nLine)
	{
	}

	LPCSTR GetFile() const { return m_pszFile; }
	LPCSTR GetFunc() const { return m_pszFunc; }
	int GetLine() const { return m_nLine; }

	virtual std::wstring Format() const;
};

// Error with description
class CErrorException : public CException
{
private:
	LPWSTR m_pwszDescr;

public:
	CErrorException(LPCWSTR pwszDescr)
	{
		m_pwszDescr = _wcsdup(pwszDescr);
	}
	~CErrorException()
	{
		free(m_pwszDescr);
	}
	CErrorException(LPCSTR pwszFile, int nLine, LPCSTR pwszFunc, 
					const std::wstring& sDescr) :
		CException(pwszFile, nLine, pwszFunc)
	{
		m_pwszDescr = _wcsdup(sDescr.c_str());
	}

	LPCWSTR GetDescription() const { return m_pwszDescr; }

	virtual std::wstring Format() const;
};

// OS error (GetLastError(), HRESULT)
class COSErrorException : public CException
{
private:
	DWORD m_dwErrCode;

public:
	COSErrorException(DWORD dwErrCode) :
		m_dwErrCode(dwErrCode)
	{
	}
	COSErrorException(LPCSTR pwszFile, int nLine, LPCSTR pwszFunc,
					  DWORD dwErrCode) :
		CException(pwszFile, nLine, pwszFunc),
		m_dwErrCode(dwErrCode)
	{
	}

	DWORD GetErrCode() const { return m_dwErrCode; }

	virtual std::wstring Format() const;
};

// SEH exception information
class CSystemException : public CException
{
private:
	PEXCEPTION_POINTERS m_pExcPtr;

public:
	CSystemException(PEXCEPTION_POINTERS pExcPtr) :
		m_pExcPtr(pExcPtr)
	{
	}
	CSystemException(LPCSTR pwszFile, int nLine, LPCSTR pwszFunc,
					 PEXCEPTION_POINTERS pExcPtr) :
		CException(pwszFile, nLine, pwszFunc),
		m_pExcPtr(pExcPtr)
	{
	}

	PEXCEPTION_POINTERS GetExcPtr() const { return m_pExcPtr; }
	LPCWSTR GetName() const { return GetExceptionName(m_pExcPtr->ExceptionRecord->ExceptionCode); }
	LPCWSTR GetDescr() const { return GetExceptionDescr(m_pExcPtr->ExceptionRecord->ExceptionCode); }

	virtual std::wstring Format() const;
};

}
