/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include <string>
#include <list>
//#include "C:\Program Files\Microsoft Visual Studio 8\DIA SDK\include\CVConst.h"
#include "C:\Program Files (x86)\Microsoft Visual Studio 12.0\DIA SDK\include\CVConst.h"
#include "sv_syminfo.h"

namespace SVCallStack
{

using namespace std;

/************************************************************************/
/* LocationInfo - ���������� �� ������ � ����                           */
/************************************************************************/

struct LocationInfo
{
	struct FUNCTION
	{
		wstring	sName;
		DWORD	dwOffs;
	};
	struct LINE
	{
		wstring	sFile;
		int		nLineNum;
		DWORD	dwOffs;
	};
	struct MODULE
	{
		wstring	sName;
		wstring sSection;
		DWORD	dwOffs;
	};

	DWORD		dwAddr;
	DWORD		dwFrame;
	FUNCTION	Function;
	LINE		Line;
	MODULE		Module;

	LocationInfo()
	{
		dwAddr = 0;
		dwFrame = 0;
		Function.dwOffs = 0;
		Line.nLineNum = 0;
		Line.dwOffs = 0;
		Module.dwOffs = 0;
	}

	void GetAddressInfo(HANDLE hProcess, PVOID pAddr);
};

/************************************************************************/
/* ����� CCallstackFrame - ���� �����                                   */
/************************************************************************/

struct CCallstackFrame
{
	struct LocalVarInfo;
	typedef list<LocalVarInfo> LOCALS;

	struct LocalVarInfo
	{
	private:
		template <class T>
			void ReadValue(HANDLE hProcess, T& val) const
		{
			DWORD dwRead = 0;
			ReadProcessMemory(hProcess, pValue, &val, sizeof(T), &dwRead);
		}

	public:
		bool				bParameter;
		wstring				sName;
		enum SymTagEnum		SymTag;
		enum LocationType	LocationType;
		enum UdtKind		UdtKind;
		enum DataKind		DataKind;
		enum BasicType		BasicType;
		DWORD				dwTypeId;
		wstring				sTypeName;
		LPCVOID				pValue;
		DWORD				dwValueSize;
		LOCALS				children;
		int					nPtrCount;
		int					nArrCount;

		LocalVarInfo()
		{
			bParameter		= false;
			SymTag			= SymTagNull;
			LocationType	= LocIsNull;
			UdtKind			= (enum UdtKind)-1;
			BasicType		= btNoType;
			nPtrCount		= 0;
			nArrCount		= 0;
		}

		wstring GetTypeStr() const;
		wstring GetValueStr(HANDLE hProcess) const;
		wstring GetFullInfo(HANDLE hProcess, int nRecurse = 0) const;
	};

	LocationInfo	Location;
	LOCALS			Locals;

	wstring GetFullInfo(HANDLE hProcess) const;
};

/************************************************************************/
/* ����� CStackWalker - ����� ��� �������� �����                        */
/************************************************************************/

class CStackWalker
{
	typedef list<CCallstackFrame> RETCALLSTACK;

	RETCALLSTACK	m_CallStack;
	HANDLE			m_hProcess;
	HANDLE			m_hThread;
	CONTEXT			m_Context;

public:
	CStackWalker();
	~CStackWalker()
	{
		Destroy();
	}
	bool Init(PCONTEXT pContext, HANDLE hThread, HANDLE hProcess = ::GetCurrentProcess());
	bool Init(PCONTEXT pContext, DWORD dwThreadId, DWORD dwProcessId);
	void Destroy();
	void CallStackSnapshot(bool bWriteVariables = true,
		bool bTrimSTDTypes = true,
		DWORD dwMaxDeep = 32);
	const RETCALLSTACK& GetCallStack() const
	{
		return m_CallStack;
	}
	wstring GetCallStackInfo() const;
};

}
