/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include "sv_ntapi.h"
#include "Psapi.h"

namespace SVProcess
{

DWORD SetProcessPrivileges(DWORD dwProcessId, LPCWSTR pPrivs[], bool pbEnabled[], DWORD dwPrivCount);
DWORD RemoveProcessPrivileges(DWORD dwProcessId, LPCWSTR pPrivs[], DWORD dwPrivCount);

PSYSTEM_PROCESSES GetProcessList();
PSYSTEM_PROCESSES FindProcess(PSYSTEM_PROCESSES pPrc, DWORD dwProcessId);
PSYSTEM_PROCESSES FindProcess(PSYSTEM_PROCESSES pPrc, LPCWSTR pwszProcessName);

DWORD GetProcessHandleCount(HANDLE hProcess);
DWORD GetProcessHandleCount(DWORD dwProcessId);
DWORD GetProcessThreadCount(DWORD dwProcessId);

DWORD KillProcess(DWORD dwProcessId, DWORD dwTimeout = 0);

/************************************************************************/
/* CProcessControl class                                                */
/************************************************************************/

class CProcessControl
{
protected:
	PROCESS_INFORMATION m_PrcInfo;

public:
	CProcessControl();
	~CProcessControl();

	bool Create(
		PCWSTR					pApplicationName,
		PCWSTR					pCommandLine		= NULL,
		DWORD					dwCreationFlags		= CREATE_UNICODE_ENVIRONMENT,
		PVOID					pEnvironment		= NULL,
		PCWSTR					pCurrentDirectory	= NULL,
		STARTUPINFOW*			pStartupInfo		= NULL,
		LPSECURITY_ATTRIBUTES	pProcessAttributes	= NULL,
		LPSECURITY_ATTRIBUTES	pThreadAttributes	= NULL,
		bool					bInheritHandles		= false);
	bool CreateAsUser(
		HANDLE					hToken,
		PCWSTR					pApplicationName,
		PCWSTR					pCommandLine		= NULL,
		DWORD					dwCreationFlags		= CREATE_UNICODE_ENVIRONMENT,
		STARTUPINFOW*			pStartupInfo		= NULL,
		PVOID					pEnvironment		= NULL,
		PCWSTR					pCurrentDirectory	= NULL,
		LPSECURITY_ATTRIBUTES	pProcessAttributes	= NULL,
		LPSECURITY_ATTRIBUTES	pThreadAttributes	= NULL,
		bool					bInheritHandles		= false);
	bool CreateWithLogon(
		PCWSTR					pUsername,
		PCWSTR					pDomain,
		PCWSTR					pPassword,
		DWORD					nLogonFlags,
		PCWSTR					pApplicationName,
		PCWSTR					pCommandLine		= NULL,
		DWORD					dwCreationFlags		= CREATE_UNICODE_ENVIRONMENT,
		PVOID					pEnvironment		= NULL,
		PCWSTR					pCurrentDirectory	= NULL,
		STARTUPINFOW*			pStartupInfo		= NULL);

	bool Attach(HANDLE hProcess);
	bool Attach(DWORD dwProcessId, DWORD dwDesiredAccess = PROCESS_ALL_ACCESS);
	void Detach();
	
	bool Terminate(UINT uExitCode = (UINT)-1);
	bool WaitFor(DWORD dwTimeout = INFINITE);

	bool SetPrivileges(LPCWSTR pPrivs[], bool pbEnabled[], DWORD dwPrivCount);
	bool RemovePrivileges(LPCWSTR pPrivs[], DWORD dwPrivCount);

	FILETIME GetCreationTime();
	FILETIME GetExitTime();
	FILETIME GetKernelTime();
	FILETIME GetUserTime();

	DWORD GetPID();
	HANDLE GetHandle();
	HANDLE GetMainThreadHandle();
	PROCESS_INFORMATION GetPrcInfo();

	bool GetMemoryInfo(PROCESS_MEMORY_COUNTERS* pmc);
	PSYSTEM_PROCESSES GetFullProcessInfo(PSYSTEM_PROCESSES* pPrcList);
	DWORD GetHandleCount();
	DWORD GetThreadCount();
};

}
