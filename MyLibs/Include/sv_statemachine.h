/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include "sv_timerqueue.h"

namespace SVStateMachine
{

// CStateMachine class

template <class STATE>
class CStateMachine
{
protected:
	typedef STATE State;
	State m_State;
	State m_OldState;

	virtual void OnStateChange(State nOldState, State nNewState) { }
	virtual void OnFinalState() { }
	virtual bool IsInFinalState() const { return false; }

public:
	CStateMachine(State st = State())
		: m_State(st), m_OldState(st)
	{
	}
	State GetState() const
	{
		return m_State;
	}
	State GetOldState() const
	{
		return m_OldState;
	}
	State SetState(State newState)
	{
		if (newState != m_State)
		{
			m_OldState = m_State;
			m_State = newState;

			OnStateChange(m_OldState, newState);

			if (IsInFinalState())
			{
				OnFinalState();
			}
		}

		return m_OldState;
	}
};

template <class STATE>
class CTimerStateMachine : protected CStateMachine<STATE>
{
private:
	HANDLE m_hTimer;

public:
	CTimerStateMachine(STATE st)
		: CStateMachine(st), m_hTimer(NULL)
	{
	}
	~CTimerStateMachine()
	{
		ResetTimer();
	}
	void SetTimer(DWORD dwTimeout, SVTimerQueue::ITimerCallback* pCallback)
	{
		ResetTimer();
		if (dwTimeout > 0)
		{
			m_hTimer = SVTimerQueue::CTimerQueueSingleton::Instance().CreateTimer(dwTimeout, pCallback);
		}
	}
	void ResetTimer()
	{
		if (m_hTimer)
		{
			SVTimerQueue::CTimerQueueSingleton::Instance().DeleteTimer(m_hTimer);
			m_hTimer = NULL;
		}
	}
};

}
