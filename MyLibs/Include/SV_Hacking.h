/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

namespace SVHacking
{
	const BYTE cmdNOP		= 0x90;
	const BYTE cmdJMPrel32	= 0xE9;

#pragma pack(push)
#pragma pack(1)
	
	struct OP32_CMD
	{
		unsigned char opcode;
		unsigned long operand;
	};

#pragma pack(pop)

	class CWin32VirtualProtect
	{
	private:
		LPVOID	m_pAddr;
		DWORD	m_dwSize;
		DWORD	m_dwOldProtect;

	public:
		CWin32VirtualProtect(LPVOID pAddr, DWORD dwSize, DWORD dwProtect);
		~CWin32VirtualProtect();
	};

	class CWin32UserModePatcher
	{
	private:
		BYTE	m_StubCode[32];
		LPVOID	m_pOldFunc;
		DWORD	m_dwCodeSize;

	public:
		CWin32UserModePatcher();
		LPVOID Patch(LPVOID pOldFunc, LPVOID pNewFunc, DWORD dwCodeSize = sizeof(OP32_CMD));
		bool Restore();
		LPVOID GetOldProc() const;
	};
};
