/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include "sv_helpclasses.h"

namespace SVTimerQueue
{

class ITimerCallback
{
public:
	virtual void OnTimer() = NULL;
};

class CTimerQueue
{
private:
	HANDLE m_hTimerQueue;

	static VOID CALLBACK WaitOrTimerCallback(PVOID lpParameter,
											 BOOLEAN TimerOrWaitFired)
	{
		try
		{
			((ITimerCallback*)lpParameter)->OnTimer();
		}
		catch (...)
		{
		}
	}

public:
	CTimerQueue()
	{
		m_hTimerQueue = ::CreateTimerQueue();
	}
	~CTimerQueue()
	{
		::DeleteTimerQueue(m_hTimerQueue);
	}
	HANDLE CreateTimer(DWORD dwDuration, ITimerCallback* pCallback, DWORD dwFlags = 0)
	{
		HANDLE hTimer = NULL;
		
		::CreateTimerQueueTimer(
			&hTimer, m_hTimerQueue, 
			&CTimerQueue::WaitOrTimerCallback, 
			pCallback, 
			dwDuration, 0, dwFlags);
		
		return hTimer;
	}
	bool DeleteTimer(HANDLE hTimer)
	{
		return ::DeleteTimerQueueTimer(m_hTimerQueue, hTimer, NULL) != 0;
	}
};

typedef CMeyerSingleton<CTimerQueue> CTimerQueueSingleton;

}
