

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Sat Oct 27 15:01:27 2018
 */
/* Compiler settings for VXMLCom.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, IID_IVXMLComInterface,0x03DC58CE,0x2499,0x4960,0xB5,0x47,0x30,0xD6,0x97,0xC3,0x59,0x9B);


MIDL_DEFINE_GUID(IID, IID_INameTable,0xCEA904CB,0xB02F,0x46C0,0x9C,0xAE,0x68,0x24,0xB1,0x0A,0x63,0x23);


MIDL_DEFINE_GUID(IID, LIBID_VXMLComLib,0x0D09FA13,0x32AD,0x4284,0x94,0x17,0x41,0x27,0x6E,0x90,0x1D,0x16);


MIDL_DEFINE_GUID(IID, DIID__IVXMLComInterfaceEvents,0xC8A10315,0xD1EF,0x4AD9,0xBB,0xA8,0xB1,0x4F,0x34,0xA7,0xCC,0xEA);


MIDL_DEFINE_GUID(CLSID, CLSID_VXMLComInterface,0xCCBC516A,0x6A6C,0x4016,0xA4,0x7D,0x2A,0x43,0x0E,0x8A,0xA4,0x88);


MIDL_DEFINE_GUID(CLSID, CLSID_NameTable,0xD783F64E,0xAF9C,0x47D3,0x95,0x26,0x57,0x72,0x21,0xE1,0x04,0x57);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



