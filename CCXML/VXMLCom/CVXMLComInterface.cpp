// CVXMLComInterface.cpp : Implementation of CVXMLComInterface

#include "stdafx.h"
#include "CVXMLComInterface.h"
#include "VXMLResCache.h"
#include "sv_helpclasses.h"
#include "ce_xml.hpp"
#include "sv_utils.h"
#include "VXMLBase.h"
#include "VXMLDebug.h"
#include "EngineConfig.h"
#include "assert.h"
#include "sid.h"

//#import "..\Bin\ISUPMessageProcessor.dll"
#import "..\Bin\ISUPMessageProcessor.dll" rename_namespace("ISUP_namespace") named_guids

using namespace enginelog;


#include <boost/property_tree/xml_parser.hpp>
#include "StringFunctions.h"

class CVXMLEngineConfig2// : public static_singleton < CVXMLEngineConfig2 >
{
	//friend class static_singleton < CVXMLEngineConfig2 >;

public:
	CVXMLEngineConfig2()
	{
		//CEngineLog pLOG;

		//pLOG.Init(L"finest", L"c:\\is3\\logs\\ccxml\\vxml_config_%y-%m-%d.log");
		//pLOG.SetSID(123123123);

		try
		{

			//pLOG.Log(LEVEL_INFO, __FUNCTIONW__, L"Config VXML read started...");



			m_sFileName = SVUtils::GetModuleFileName(g_hInst) + L".config";
			//pLOG.Log(LEVEL_INFO, __FUNCTIONW__, L"Config file name\"%s\"", m_sFileName.c_str());


			std::wifstream _in(m_sFileName.c_str(), std::ios::binary);

			//pLOG.Log(LEVEL_INFO, __FUNCTIONW__, L"read config");

			boost::property_tree::wiptree propertyTree;
			boost::property_tree::read_xml(_in, propertyTree);
			//pLOG.Log(LEVEL_INFO, __FUNCTIONW__, L"1");
			const boost::property_tree::wiptree& log      = propertyTree.get_child(L"configuration").get_child(L"Log");
			//pLOG.Log(LEVEL_INFO, __FUNCTIONW__, L"2");
			const boost::property_tree::wiptree& folder   = propertyTree.get_child(L"configuration").get_child(L"Folder");
			//pLOG.Log(LEVEL_INFO, __FUNCTIONW__, L"3");
			const boost::property_tree::wiptree& system   = propertyTree.get_child(L"configuration").get_child(L"System");
			//pLOG.Log(LEVEL_INFO, __FUNCTIONW__, L"4");
			const boost::property_tree::wiptree& settings = propertyTree.get_child(L"configuration").get_child(L"DefaultVXMLSettings");
			//pLOG.Log(LEVEL_INFO, __FUNCTIONW__, L"5");

			//propertyTree.get<std::wstring>("configuration/Log/FileNameGenerator.<xmlattr>.Format")

			this->sLogFileName = log.get<std::wstring>(L"FileNameGenerator.<xmlattr>.Format");
			this->m_sExtraLogName = log.get<std::wstring>(L"FileNameGenerator.<xmlattr>.Extra");
			this->m_sVBSLogName = log.get<std::wstring>(L"FileNameGenerator.<xmlattr>.VBS");
			
			this->sLevel = log.get<std::wstring>(L"FileNameGenerator.<xmlattr>.Level");// m_config.attr(L"//configuration/Log/FileNameGenerator/Level");
			
			//pLOG.Log(LEVEL_INFO, __FUNCTIONW__, L"6");
			
			this->XMLFolders.sVXMLFolder    = folder.get<std::wstring>(L"VXMLFolder.<xmlattr>.Value");
			this->XMLFolders.sWavFolder     = folder.get<std::wstring>(L"WavFolder.<xmlattr>.Value");
			this->XMLFolders.sGrammarFolder = folder.get<std::wstring>(L"GrammarFolder.<xmlattr>.Value");
			this->XMLFolders.sVideoFolder   = folder.get<std::wstring>(L"VideoFolder.<xmlattr>.Value");

			//pLOG.Log(LEVEL_INFO, __FUNCTIONW__, L"7");

			this->VXMLDefSet.sTimeout = settings.get<std::wstring>(L"Timeout.<xmlattr>.Value");
			this->VXMLDefSet.sInterdigittimeout = settings.get<std::wstring>(L"Interdigittimeout.<xmlattr>.Value");
			this->VXMLDefSet.sWaitTime = settings.get<std::wstring>(L"WaitTime.<xmlattr>.Value");
			this->VXMLDefSet.sTermchar = settings.get<std::wstring>(L"Termchar.<xmlattr>.Value");
			//pLOG.Log(LEVEL_INFO, __FUNCTIONW__, L"8");

			this->Systems.sKeepAliveTimeout = system.get<std::wstring>(L"KeepAliveTimeout.<xmlattr>.Value");

			//pLOG.Log(LEVEL_INFO, __FUNCTIONW__, L"9");

			_in.close();
			//pLOG.Log(LEVEL_INFO, __FUNCTIONW__, L"10");
		}
		catch (boost::property_tree::xml_parser_error error)
		{
			//singleton_auto_pointer <CScriptIDHolder> holder;
			__int64 iScriptID = GetGlobalScriptId();

			CEngineLog pLOG;

			pLOG.Init(L"finest", L"c:\\is3\\logs\\ccxml\\vxml_config_%y-%m-%d.log");
			pLOG.SetSID(iScriptID);

			std::wstring msg = _bstr_t(error.message().c_str());
			pLOG.Log(LEVEL_INFO, __FUNCTIONW__, L"Config XML parser error: \"%s\"", msg.c_str());

			//std::cout << "XML parser error! " << msg << std::endl;
			throw std::wstring(L"XML parser error!");
		}
	}

public:
	VXML::TDefaultVXMLSettings VXMLDefSet;
	VXML::TFolders XMLFolders;
	VXML::TSystem Systems;

	std::wstring sLogFileName;
	std::wstring m_sExtraLogName;
	std::wstring m_sVBSLogName;
	std::wstring sLevel;

private:
	std::wstring m_sFileName;


};


// CVXMLComInterface
HRESULT CVXMLComInterface::FinalConstruct()
{
	try
	{
		::InitializeCriticalSection(&m_CS1);
		::InitializeCriticalSection(&m_CS2);

		m_Log.reset(new CEngineLog());
		m_extraLog.reset(new CEngineLog());
		m_vbsLog.reset(new CEngineLog());
		//m_pResCache.reset(new DEBUG_NEW_PLACEMENT VXML::CResourceCache());
		m_pVar.reset(new DEBUG_NEW_PLACEMENT VXML::CScopedNameTable());
		m_pSes.reset(new DEBUG_NEW_PLACEMENT VXML::CSession(/*m_CS1, m_CS2*/));

	}
	catch (...)
	{
		CEngineLog log;
		log.Init(L"finest", L"C:\\IS3\\Logs\\vxml_extra_%y-%m-%d.log");

		log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"CCXML final construct exception");

		Error(L"VXML final construct exception", GUID_NULL, E_FAIL);
		return E_FAIL;
	}

	return S_OK;
}

void CVXMLComInterface::FinalRelease()
{
	try
	{
		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"VXML FinalRelease...");
		m_pDebuger.reset();
		//m_pResCache.reset();
		m_pVar.reset();
		m_pSes->Stop();
		m_pSes.reset();
#ifdef _EMULATE_TELSERVER
		m_pTelserv.reset();
#endif
		m_Log.reset();
		m_extraLog.reset();

		::DeleteCriticalSection(&m_CS2);
		::DeleteCriticalSection(&m_CS1);
	}
	catch (...)
	{
		CEngineLog log;
		log.Init(L"finest", L"C:\\IS3\\Logs\\vxml_extra_%y-%m-%d.log");

		log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"CCXML final release exception");
		Error(L"VXML final release exception", GUID_NULL, E_FAIL);
	}

	//	_CrtDumpMemoryLeaks();
}



STDMETHODIMP CVXMLComInterface::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IVXMLComInterface
	};

	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}



//STDMETHODIMP CVXMLComInterface::Init(PMESSAGEHEADER pMsgHeader)
//{
//	// TODO: Add your implementation code here
//
//	return S_OK;
//}

STDMETHODIMP CVXMLComInterface::Destroy(void)
{
	// TODO: Add your implementation code here
	if (m_pSes.get() && m_pSes->IsInitialized())
	{
		//delete m_pSes;
		//m_pSes = NULL;

		//if (m_pQ931Info)
		//	m_pQ931Info->Release();

		m_Log->Log(LEVEL_FINEST,__FUNCTIONW__, L"VXML session destroyed");
	}
	else
	{
		Error(L"VXML session not initialized", GUID_NULL, E_FAIL);
		return E_FAIL;
	}

	//if (m_pQ931Info)
	//	m_pQ931Info->Release();


	return S_OK;
}

STDMETHODIMP CVXMLComInterface::DoStep()
{
	try
	{
		if (m_pSes.get() != NULL && m_pSes->IsInitialized() && m_pSes->GetState() != VMS_ENDSCRIPT)
		{
			m_pSes->DoStep();
		}

		return S_OK;
	}
	catch (const std::exception& error)
	{
		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"DoStep EXCEPTION: %s", stow(error.what()).c_str());
	}
	catch (...)
	{
		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"UNHANDLED EXCEPTION while DoStep");
	}
	return E_FAIL;
}

//STDMETHODIMP CVXMLComInterface::DoStep(void)
//{
//	if (m_pSes.get() != NULL && m_pSes->IsInitialized())
//	{
//		if (m_pSes->GetState() != VMS_ENDSCRIPT)
//			m_pSes->DoStep();		
//	}
//	else
//	{
//		Error(L"VXML session not initialized", GUID_NULL, E_FAIL);
//		return E_FAIL;
//	}
//
//	return S_OK;
//}


STDMETHODIMP CVXMLComInterface::GetState()
{
	try
	{
		if (m_pSes.get() && m_pSes->IsInitialized())
		{
			return (HRESULT)m_pSes->GetState();
		}

		Error(L"VXML session not initialized", GUID_NULL, E_FAIL);
	}
	catch (const std::exception& error)
	{
		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"GetState EXCEPTION: %s", stow(error.what()).c_str());
	}
	catch (...)
	{
		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"UNHANDLED EXCEPTION while GetState");
	}

	return E_FAIL;
}

//STDMETHODIMP CVXMLComInterface::GetState(void)
//{
//	// TODO: Add your implementation code here
//	if (m_pSes.get() != NULL && m_pSes->IsInitialized())
//	{
//		return (HRESULT)m_pSes->GetState();
//	}
//	return E_FAIL;
//}

STDMETHODIMP CVXMLComInterface::SetEvent(PMESSAGEHEADER pMsgHeader)
{
#ifdef _EMULATE_TELSERVER
	if (m_pTelserv)
		m_pTelserv->SetEvent(pMsgHeader);
#endif

	try
	{
		if (m_pSes.get() && m_pSes->IsInitialized() && m_pSes->GetState() != VMS_ENDSCRIPT)
		{
			m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"IN %s", DumpMessage(pMsgHeader).c_str());
			m_pSes->OnAuxMsg(CMessage(pMsgHeader));

			return S_OK;
		}

		Error(L"VXML session not initialized", GUID_NULL, E_FAIL);
	}
	catch (const std::exception& error)
	{
		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"SetEvent EXCEPTION: %s", stow(error.what()).c_str());
	}
	catch (...)
	{
		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"UNHANDLED EXCEPTION while SetEvent");
	}

	return E_FAIL;
}


//STDMETHODIMP CVXMLComInterface::SetEvent(PMESSAGEHEADER pMsgHeader)
//{
//
//	if (m_pSes.get() != NULL && m_pSes->IsInitialized())
//	{
//		if (m_pSes->GetState() != VMS_ENDSCRIPT)
//		{
//			m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"IN %s", DumpMessage(pMsgHeader).c_str());
//			m_pSes->OnAuxMsg(CMessage(pMsgHeader));
//		}
//	}
//	else
//	{
//		Error(L"CCXML session not initialized", GUID_NULL, E_FAIL);
//		return E_FAIL;
//	}
//	return S_OK;
//}

STDMETHODIMP CVXMLComInterface::GetNextDoStepInterval(LPDWORD lpdwInterval)
{
	// TODO: Add your implementation code here
	*lpdwInterval = (DWORD)-1;
	//*lpdwInterval = 100;
	return S_OK;
}

class CLocker
{
private:
	CRITICAL_SECTION& m_CS;

public:
	CLocker(CRITICAL_SECTION& cs) : m_CS(cs)
	{
		::EnterCriticalSection(&m_CS);
	}
	~CLocker()
	{
		::LeaveCriticalSection(&m_CS);
	}
};


STDMETHODIMP CVXMLComInterface::Init(PMESSAGEHEADER pMsgHeader, IEngineCallback* pCallback)
{
	// TODO: Add your implementation code here

	//Sleep(10000);
	try
	{
		// Log initialization
		//CEngineLog log;

		//log.Init(L"finest", L"c:\\is3\\logs\\ccxml\\vxml_temp_%y-%m-%d.log");
		//log.SetSID(123123123);
		//log.Log(LEVEL_INFO, __FUNCTIONW__, L"Initializing VXML session...");


		CMessage msg(pMsgHeader);
		msg.CheckParam(L"ScriptID", CMessage::Int64, CMessage::Mandatory);

		//log.Log(LEVEL_INFO, __FUNCTIONW__, L"1");
		CParam* pSID = msg.ParamByName(L"ScriptID");

		SetGlobalScriptId(pSID->AsInt64());
		//log.Log(LEVEL_INFO, __FUNCTIONW__, L"2");


		//// ExtraLog initialization
		//m_extraLog->Init(L"finest", L"c:\\is3\\logs\\ccxml\\vxml_extra_%y-%m-%d.log");
		//m_extraLog->SetSID(pSID->AsInt64());

		//m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Initializing VXML session...");

		// Load params

		CVXMLEngineConfig2 config;

		// Log initialization
		m_Log->Init(config.sLevel.c_str(), config.sLogFileName.c_str());
		m_Log->SetSID(pSID->AsInt64());

		// ExtraLog initialization
		m_extraLog->Init(config.sLevel.c_str(), config.m_sExtraLogName.c_str());
		m_extraLog->SetSID(pSID->AsInt64());

		m_vbsLog->Init(config.sLevel.c_str(), config.m_sVBSLogName.c_str(), true);
		m_vbsLog->SetSID(pSID->AsInt64());

		//log.Log(LEVEL_INFO, __FUNCTIONW__, L"4");

		VXML::SessionCreateParams params;

		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Initializing VXML session...");

		//if (!m_pResCache->BaseReady())
		//{
		//	m_Log      -> Log(LEVEL_WARNING, __FUNCTIONW__, L"Failed to CoCreateInstance - CLSID_ResourceCache");
		//	m_extraLog -> Log(LEVEL_WARNING, __FUNCTIONW__, L"Failed to CoCreateInstance - CLSID_ResourceCache");
		//	return E_FAIL;
		//}
		//log.Log(LEVEL_INFO, __FUNCTIONW__, L"5");

#ifdef _EMULATE_TELSERVER
		//m_pTelserv = std::auto_ptr< CTelServerEmulator>(new CTelServerEmulator(pCallback,this));
		m_pTelserv.reset(new DEBUG_NEW_PLACEMENT CTelServerEmulator(pCallback,this));
		pCallback = static_cast<IEngineCallback*>(m_pTelserv.get());
#endif

		//m_pDebuger = std::auto_ptr< VXML::VXMLDebugBeholder>(new VXML::VXMLDebugBeholder(pCallback,this));
		m_pDebuger.reset(new DEBUG_NEW_PLACEMENT VXML::VXMLDebugBeholder(pCallback,this));
		//pCallback = static_cast<VXML::IDebuger*>(m_pDebuger.get());

		//log.Log(LEVEL_INFO, __FUNCTIONW__, L"6");



		//m_pQ931Info = NULL;
		m_sQ931XML  = L"";
		CParam* pQ931 = msg.ParamByName(L"SigInfo");
		if (pQ931 && !pQ931->AsWideStr().empty())
		{
			if (!CreateQ931ProtocolInfo(pQ931->AsWideStr()))
				m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"CreateQ931ProtocolInfo failed");
		}

		//log.Log(LEVEL_INFO, __FUNCTIONW__, L"7");

		//m_VOX = LoadTelLib();

		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, msg.Dump().c_str());

		//log.Log(LEVEL_INFO, __FUNCTIONW__, L"8");

		params.pCallback	  = pCallback;
		params.pEvtPrc		  = m_pDebuger;//pCallback;
		//params.pResCache	  = m_pResCache;// .reset(new DEBUG_NEW_PLACEMENT VXML::CResourceCache());
		params.pInitMsg		  = pMsgHeader;
		params.pLog			  = m_Log;
		params.pExtraLog	  = m_extraLog;
		params.pVBSLog		  = m_vbsLog;
		params.dwSurrThread	  = ::GetCurrentThreadId();
		//log.Log(LEVEL_INFO, __FUNCTIONW__, L"9");
		//params.pVXMLEngine    = this;
		params.VXMLFolders    = config.XMLFolders;
		params.VXMLDefSet     = config.VXMLDefSet;
		params.VXMLSystem     = config.Systems;
		params.pQ931Info	  = m_pQ931Info;
		params.sQ931XML       = m_sQ931XML;
		params.pDebuger       = m_pDebuger;
		//params.pVOX           = m_VOX;
		params.pVar           = m_pVar;

		if (const CParam* param = msg.ParamByName(L"WavFolder"))
		{
			params.VXMLFolders.sWavFolder = param->AsWideStr();
		}

		if (const CParam* param = msg.ParamByName(L"VXMLFolder"))
		{
			params.VXMLFolders.sVXMLFolder = param->AsWideStr();
		}

		if (const CParam* param = msg.ParamByName(L"GrammarFolder"))
		{
			params.VXMLFolders.sGrammarFolder = param->AsWideStr();
		}


		//log.Log(LEVEL_INFO, __FUNCTIONW__, L"10");
		
		params.pSession = m_pSes;
		if (!m_pSes->Initialize(params))
		{
			//log.Log(LEVEL_INFO, __FUNCTIONW__, L"!!!!!!!!!!!!!!!!!!!");
			m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CVXMLComInterface: VXML session initializing failed");
			return E_FAIL;
		}

		//log.Log(LEVEL_INFO, __FUNCTIONW__, L"11");

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"VXML session initialized");
	}
	catch (std::wstring& e)
	{
		if (m_Log)
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Error: %s", e.c_str());
		return DISP_E_EXCEPTION;
	}
	catch (_com_error& e)
	{
		if (m_Log)
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Error: %s", (LPCWSTR)e.Description());
		return DISP_E_EXCEPTION;
	}
	catch (...)
	{
		if (m_Log)
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Error: unknown exception occurred");
		return DISP_E_EXCEPTION;
	}

	return S_OK;

}

//static _bstr_t SelectSingleNode(MSXML2::IXMLDOMDocument2Ptr pDoc, const std::wstring& sQuery)
//{
//	_bstr_t  retVal(L"");
//	MSXML2::IXMLDOMNodePtr	Node;
//	if (pDoc)
//	{
//		Node = pDoc->selectSingleNode(sQuery.c_str());
//		if (Node)
//			retVal = Node->text;
//	}
//	if (Node)
//		Node.Release();
//	return retVal;
//}

static _bstr_t SelectValue(MSXML2::IXMLDOMDocument2Ptr pDoc, const std::wstring& sQuery, const std::wstring& sValue)
{
	MSXML2::IXMLDOMNodePtr	       Node;
	MSXML2::IXMLDOMElementPtr      NodeElem;
	MSXML2::IXMLDOMNamedNodeMapPtr NodeMap;
	_bstr_t  retVal(L"");
	if (pDoc)
	{
		Node = pDoc->selectSingleNode(sQuery.c_str());
		if (Node)
		{
			NodeElem =  Node;
			NodeMap  =  NodeElem->Getattributes();
			Node     =  NodeMap->getNamedItem(sValue.c_str());
			if (Node)
			{
				retVal =  Node->text;
			}
		}
	}
	if (Node)    	Node     .Release();
	if (NodeElem)	NodeElem .Release();
	if (NodeMap)	NodeMap  .Release();

	return retVal;
}
bool CVXMLComInterface::CreateQ931ProtocolInfo(const std::wstring &SigInfo)
{
	m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Initializing Q931 COM protocol...");

	CComPtr<ISUP_namespace::IISUPMessage > p;
	m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CoCreateInstance ISUP...");
	HRESULT hr = p.CoCreateInstance(L"ISUPMessageProcessor.ISUPMessage");
	if (FAILED(hr))
		return false;
	m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"ISUP created");

	MSXML2::IXMLDOMDocument2Ptr pDoc = p->ToXML(_bstr_t(SigInfo.c_str()));

	if (!pDoc)
		return false;

	pDoc->get_xml(&m_sQ931XML.GetBSTR());

	m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CoCreateInstance q931Info...");
	//Iq931Info* pQ931Info = NULL;
	hr = m_pQ931Info.CoCreateInstance(CLSID_q931Info);
	m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"q931Info created");

	//hr = CoCreateInstance(
	//	CLSID_q931Info,
	//	0,
	//	CLSCTX_INPROC_SERVER,
	//	IID_Iq931Info,
	//	(LPVOID*)&(pQ931Info));

	if (FAILED(hr) || ! m_pQ931Info.p)
		return false;

	ICallingPartyCategoryInfo         *pCallingPartyCategoryInfo         = NULL;
	IRedirectionNumberRestrictionInfo *pRedirectionNumberRestrictionInfo = NULL;
	ICorrelationIDInfo                *pCorrelationIDInfo                = NULL;
	ISCFIDInfo                        *pSCFIDInfo                        = NULL;
	IRedirectionInformationInfo       *pRedirectionInformationInfo       = NULL;
	ICauseIndicatorsInfo              *pCauseIndicatorsInfo              = NULL;
	ICalledPartyInfo                  *pCalledPartyInfo                  = NULL;
	ICallingPartyInfo                 *pCallingPartyInfo                 = NULL;
	ILocationNumberInfo               *pLocationNumberInfo               = NULL;
	IRedirectionNumberInfo            *pRedirectionNumberInfo            = NULL;
	IUserToUserInfo                   *pUserToUserInfo                   = NULL;

	//SelectSingleNode(pDoc, L"//Parameters/CallingPartyCategory");

	hr = m_pQ931Info->get_CallingPartyCategory(&pCallingPartyCategoryInfo);
	if (SUCCEEDED(hr) && pCallingPartyCategoryInfo)
	{
		pCallingPartyCategoryInfo->put_CallingPartyCategory(
			Utils::toNumX<LONG>(SelectValue(pDoc,L"//Parameters/CallingPartyCategory",L"value").GetBSTR())
			);
	}

	hr = m_pQ931Info->get_RedirectionNumberRestriction(&pRedirectionNumberRestrictionInfo);
	if (SUCCEEDED(hr) && pRedirectionNumberRestrictionInfo)
	{
		pRedirectionNumberRestrictionInfo->put_PresentationRestricted(
			Utils::toNumX<LONG>(SelectValue(pDoc,L"//Parameters/RedirectionNumberRestriction",L"value").GetBSTR())
			);
	}

	hr = m_pQ931Info->get_CorrelationID(&pCorrelationIDInfo);
	if (SUCCEEDED(hr) && pCorrelationIDInfo)
	{
		pCorrelationIDInfo->put_CorrelationID(SelectValue(pDoc,L"//Parameters/CorrelationID",L"value"));
	}

	hr = m_pQ931Info->get_SCFID(&pSCFIDInfo);
	if (SUCCEEDED(hr) && pSCFIDInfo)
	{
		pSCFIDInfo->put_SCFID(SelectValue(pDoc,L"//Parameters/SCFID",L"value"));
	}

	hr = m_pQ931Info->get_RedirectionInformation(&pRedirectionInformationInfo);
	if (SUCCEEDED(hr) && pRedirectionInformationInfo)
	{
		pRedirectionInformationInfo->put_RedirectingIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/RedirectionInformation",L"RedirectingIndicator").GetBSTR())
			);

		pRedirectionInformationInfo->put_OriginalRedirectionReason(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/RedirectionInformation",L"OriginalRedirectionReason").GetBSTR())
			);

		pRedirectionInformationInfo->put_RedirectionCounter(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/RedirectionInformation",L"RedirectionCounter").GetBSTR())
			);

		pRedirectionInformationInfo->put_RedirectingReason(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/RedirectionInformation",L"RedirectingReason").GetBSTR())
			);
	}

	hr = m_pQ931Info->get_CauseIndicators(&pCauseIndicatorsInfo);
	if (SUCCEEDED(hr) && pCauseIndicatorsInfo)
	{
		pCauseIndicatorsInfo->put_Location(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CauseIndicators",L"Location").GetBSTR())
			);

		pCauseIndicatorsInfo->put_CodingStandart(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CauseIndicators",L"CodingStandart").GetBSTR())
			);

		pCauseIndicatorsInfo->put_CauseValue(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CauseIndicators",L"CauseValue").GetBSTR())
			);

	}

	hr = m_pQ931Info->get_CalledPartyNumber(&pCalledPartyInfo);
	if (SUCCEEDED(hr) && pCalledPartyInfo)
	{
		pCalledPartyInfo->put_NatureOfAddress(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"NatureOfAddress").GetBSTR())
			);

		pCalledPartyInfo->put_OddEvenIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"OddEvenIndicator").GetBSTR())
			);

		pCalledPartyInfo->put_NumberingPlanIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"NumberingPlanIndicator").GetBSTR())
			);

		pCalledPartyInfo->put_InternalNetworkNumberIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"InternalNetworkNumberIndicator").GetBSTR())
			);

		pCalledPartyInfo->put_CalledAddressSignals(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"CalledAddressSignals").GetBSTR());

	}

	hr = m_pQ931Info->get_CallingPartyNumber(&pCallingPartyInfo);
	if (SUCCEEDED(hr) && pCallingPartyInfo)
	{
		pCallingPartyInfo->put_NatureOfAddress(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"NatureOfAddress").GetBSTR())
			);

		pCallingPartyInfo->put_OddEvenIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"OddEvenIndicator").GetBSTR())
			);

		pCallingPartyInfo->put_NumberingPlanIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"NumberingPlanIndicator").GetBSTR())
			);

		pCallingPartyInfo->put_ScreeningIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"ScreeningIndicator").GetBSTR())
			);

		pCallingPartyInfo->put_PresentationRestrictionIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"PresentationRestrInd").GetBSTR())
			);

		pCallingPartyInfo->put_CallingAddressSignals(SelectValue(pDoc,L"//Parameters/CalledPartyNumber",L"CallingAddressSignals").GetBSTR());

	}

	hr = m_pQ931Info->get_LocationNumber(&pLocationNumberInfo);
	if (SUCCEEDED(hr) && pLocationNumberInfo)
	{
		pLocationNumberInfo->put_NatureOfAddress(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/LocationNumber",L"NatureOfAddress").GetBSTR())
			);

		pLocationNumberInfo->put_OddEvenIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/LocationNumber",L"OddEvenIndicator").GetBSTR())
			);

		pLocationNumberInfo->put_NumberingPlanIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/LocationNumber",L"NumberingPlanIndicator").GetBSTR())
			);

		pLocationNumberInfo->put_ScreeningIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/LocationNumber",L"ScreeningIndicator").GetBSTR())
			);

		pLocationNumberInfo->put_PresentationRestrictionIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/LocationNumber",L"PresentationRestrictionIndicator").GetBSTR())
			);

		pLocationNumberInfo->put_InternationalNNI(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/LocationNumber",L"InternationalNNI").GetBSTR())
			);

		pLocationNumberInfo->put_LocationNumber(SelectValue(pDoc,L"//Parameters/LocationNumber",L"LocationNumber").GetBSTR());

	}

	hr = m_pQ931Info->get_RedirectionNumber(&pRedirectionNumberInfo);
	if (SUCCEEDED(hr) && pRedirectionNumberInfo)
	{
		pRedirectionNumberInfo->put_NatureOfAddress(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/RedirectionNumber",L"NatureOfAddress").GetBSTR())
			);

		pRedirectionNumberInfo->put_OddEvenIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/RedirectionNumber",L"OddEvenIndicator").GetBSTR())
			);

		pRedirectionNumberInfo->put_NumberingPlanIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/RedirectionNumber",L"NumberingPlanIndicator").GetBSTR())
			);

		pRedirectionNumberInfo->put_InternalNetworkNumberIndicator(
			Utils::toNum<LONG>(SelectValue(pDoc,L"//Parameters/RedirectionNumber",L"InternalNetworkNumberIndicator").GetBSTR())
			);

		pRedirectionNumberInfo->put_RedirectionAddressSignals(SelectValue(pDoc,L"//Parameters/RedirectionNumber",L"RedirectionAddressSignals").GetBSTR());

	}

	hr = m_pQ931Info->get_UserToUser(&pUserToUserInfo);
 	if (SUCCEEDED(hr) && pUserToUserInfo)
 	{
 		pUserToUserInfo->put_UserToUser(SelectValue(pDoc,L"//Parameters/UserToUserInformation",L"value"));
 	}

	m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Initializing Q931 COM protocol completed");
	return true;
}

//IVOXPtr CVXMLComInterface::LoadTelLib()
//{
//	m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CreateInstance TELLIB...");
//	IVOXPtr pVox;
//	HRESULT hr = pVox.CreateInstance(__uuidof(VOX));
//	if (FAILED(hr))
//	{
//		throw _com_error(hr);
//	}
//	m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"TELLIB created");
//
//	return pVox;
//}

CVXMLComInterface::CVXMLComInterface()
{
	::InitializeCriticalSection(&m_CS);
}

CVXMLComInterface::~CVXMLComInterface()
{
	::DeleteCriticalSection(&m_CS);
}

//Iq931Info* CVXMLComInterface::GetQ931ProtocolInfo(const std::wstring&SigInfo)
//{
//	if (!m_pQ931Info)
//		m_pQ931Info = CreateQ931ProtocolInfo(SigInfo);
//	return m_pQ931Info;
//}
