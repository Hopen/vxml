/************************************************************************/
/* Name     : VXMLCom\VXMLSession.h                                     */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 30 Nov 2009                                               */
/************************************************************************/

#pragma once
#include "VXMLEvent.h"
#include "VXMLBase.h"
#include "sv_thread.h"
#include "sv_handle.h"
#include "sv_sysobj.h"
#include "..\CVXMLComInterface.h"
#include "VXMLTags.h"
//#include "..\q931\q931.h"
//#include "..\Common\KeepAlive.h"
#include "..\Common\locked_queue.h"
#include "Patterns/singleton.h"
#include "VXMLAppLog.h"
#include "eventdispatcher.h"

class CKeepAliveProvider;

namespace VXML
{
	enum eWaitEvents
	{
		WE_SUCCESS = 0,
		WE_FAILED,
		WE_STOPTHREAD
	};

	struct SessionCreateParams
	{
		LPMESSAGEHEADER		pInitMsg;
		IEngineCallbackPtr	pEvtPrc;
		EngineLogPtr2		pLog;
		EngineLogPtr2		pExtraLog;
		EngineLogPtr2		pVBSLog;
		DWORD				dwSurrThread;
		TFolders			VXMLFolders;
		TDefaultVXMLSettings VXMLDefSet;
		TSystem             VXMLSystem;
		Q931Ptr				pQ931Info;
		VXMLString			sQ931XML;
		DebugerPtr          pDebuger;
		IVOXPtr             pVOX;
		SessionPtr			pSession;
		ScopedNameTablePtr  pVar;
		IEngineCallback*	pCallback;
	};

	typedef std::list<VXMLString>WaitingEvents;
	typedef std::list<CMessage> EventMessages;

	class CStateDispatcher;
	using StateDispatcherPtr = std::unique_ptr<CStateDispatcher>;

	using ExecContainerPtr = std::unique_ptr<CExecContainer>;

	class LockGuardBase
	{
	protected:
		LockGuardBase(const VXMLString& aName)
			: mName(aName)
		{
			//static TempLog log;
			//log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"IN: %s - %i", mName.c_str(), ++lock_counter);
		}

		virtual ~LockGuardBase()
		{
			//static TempLog log;
			//log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"OUT: %s - %i", mName.c_str(), --lock_counter);
		}

	private:
		VXMLString mName;
	};

	using EventDispatcherPtr = std::unique_ptr<common::CEventDispatcher<EventObject, LockGuardBase>>;

	class IDebuger: public IEngineCallback
	{
	public:
		IDebuger(IEngineCallback* _callback, IEngineVM* _engine)
		{
			m_pCallBack = _callback ;
			m_pEngine   = _engine   ;
		}
	public: // interface
		virtual void Wait        (unsigned int _line/*, unsigned int _count = 1*/) = 0;
		virtual void Increase    (/*unsigned int _count = 1*/) = 0;
		virtual void SetAuxEvent (CMessage& msg, bool _in) = 0;
		virtual void Init        (const CMessage& init_msg, SessionPtr _session) = 0;
		virtual void Log         (VXMLString _log       ) = 0;

	protected:
		IEngineCallback* m_pCallBack;
		IEngineVM*       m_pEngine;

	};

	class CSession
	{
		friend class CExecContainer;

		enum eThreadMsg
		{
			tmExecuteDoc = WM_USER + 1,
			tmSetEvent,
			tmExecutePredefinition,
			tmDocumentPrepared,
			tmTerminated,
			tmQuit = WM_QUIT
		};

	private:

		class CFindWaitingEvent
		{
		public:
			CFindWaitingEvent(VXMLString _event):
			  m_event(_event)
			{}
			  bool operator()(VXMLString _event)
			  {
				  return _event == m_event;
			  }

		private:
			VXMLString m_event;
		};

		class CTellib
		{
		public:
			IVOXPtr GetInstance()
			{
				if (m_pVox == NULL)
					m_pVox = LoadTelLib();
				return m_pVox;
			}
		private:
			IVOXPtr LoadTelLib();
			IVOXPtr m_pVox;
		};

		typedef std::shared_ptr <CTellib> TelLibPtr;

	public:
		CSession();
		virtual ~CSession(void);
		bool Initialize(const SessionCreateParams& params);
		bool IsInitialized(){ return m_Initialized; }

	private:
		typedef EventObject EventPtr;

		typedef std::list<EventPtr> EventQueue;

	
	private:
		CTag_vxml*				m_pTagVXML;
		CMessage				m_InitMsg;
		SVSysObj::CEvent		m_ThreadEvt;
		VXMLString				m_sCurDoc;
		VXMLString				m_sNextDoc;
		//CExecContext			m_ExecContext;
		EngineLogPtr2			m_Log;
		EngineLogPtr2			m_extraLog;
		DWORD					m_dwState;
		DWORD					m_dwSurrThread;
		TFolders			    m_VXMLFolders;
		TDefaultVXMLSettings	m_VXMLDefSet;
		TSystem				    m_VXMLSystem;

		SessionObject		    m_pLocal;
		SessionObject		    m_pRemote;
		SessionObject		    m_pProtocol;
		SessionObject		    m_pConnection;
		SessionObject		    m_pDialog;

		SessionGrammar			m_pGrammars;
		CMessage				m_NextEvt;
		bool                    m_bSessionEnd;
		bool					m_bPredefinitionInProcess;
		EventObject				m_pSessionVariables;
		EventObject				m_pApplicationVariables;
		EventObject				m_pDialogVariables;
		bool					m_throwEvt;
		bool                    m_DialogActive;
		//unsigned int            m_nStepCounter;

		EventScope              m_globals_scope;
		EventScope              m_globals_prompts;
		TelLibPtr				m_pVox;
		bool					m_isLogHttpDispatch = false;
		const CTagFactory       m_TagFactory;
		
		std::shared_ptr<CAppLog> m_appLog;

		//std::weak_ptr<CKeepAliveProvider> m_keep_alive;
		VXMLString				m_TerminatedNamelist;
		VXMLString				m_TerminatedHandler;
		bool					m_bTerminated = false;
		StateDispatcherPtr		m_StateDispatcher;
		EventDispatcherPtr		m_EventDispatcher;
		ExecContainerPtr		m_ExecContainer;
		std::shared_ptr < boost::thread >  m_thread;
		locked_queue      < eThreadMsg    >  m_queue;

		bool m_bThreadStarted;
		bool m_Initialized = false;
		bool m_IsStarted = false;
		int	 m_iKeepAliveTimeout{ 1 };
		//std::mutex mDbgMutex;

	private:
		void CreateSessionVariables(const EngineLogPtr& vbsLog);
		VXMLString ThreadMsgToString(const eThreadMsg& _msg)const;
		void SendQueuedEvent(int nIndex);
		public:
		void ExceptionHandler();
		private:
		void PrepareDoc(DocumentPtr pDoc, const VXMLString& sURI);
		void DoDocumentPrepared();
		void DoDocumentFailed(const VXMLString& _error_description);
		void ExecuteDoc();
		void OnThreadMsg(eThreadMsg	msg, void* pParam);
		void PostThreadMsg(eThreadMsg msg, void* pParam = NULL);
		void ProcessEvents();
		void ProcessEvent(EventPtr pEvt);
		void ProcessStep();
		void ProcessFirstEvent();

		void TranslateMsg(CMessage& msg);
		CMessage MakeCommonEvent(CMessage& msg, VXMLString message);
		void MakeEvent_MenuWav(CMessage&);
		void MakeEvent_MenuDigits(CMessage&);
		void MakeEvent_ClearDigits(CMessage&);
		void MakeEvent_SubDialogEnd(CMessage&);
		void MakeEvent_VideoAcknowledge(CMessage&);
		void MakeEvent_VideoCompleted(CMessage&);
		void MakeEvent_StopVideoCompleted(CMessage&);
		void MakeEvent_StopChannelCompleted(CMessage&);
		void MakeEvent_RecWav(CMessage&);
		void ProcessUnhandledEvent(EventPtr pEvt);
		void EmitEvent(const CMessage& evt);
		//bool LoadPredefinitions(const VXMLString& sPredefine);
		std::list<DocumentPtr> LoadPredefinitions2(const VXMLString& sPredefine);
		void ExecutePredefinitions();
		void SetApplicationLastResultInputMode(const VXMLString& _inputmode);
		void DoExit(const VXMLString& sConnectedWithMessageName, const VXMLString& sNamelist, const VXMLString& _error_description);
		void DbgEvent(CMessage& msg);

		void StartThread();
		void JoinThread();

		int filter(int code, _EXCEPTION_POINTERS *ep);

	private:
		void ThreadProc();
		void startThread();
		void SendClearDigits();

	public:
		void DoStep();
		void EmitStep();
		void WaitForStep(unsigned int _line);
		bool LoadAndExecuteDocument(const VXMLString& sURI,const bool& bFile);
		DocumentPtr LoadDocument(const VXMLString& sURI, const bool& bFile);

		void SendAuxMsg(const CMessage& msg) const;
		void DoError(CMessage &evt, const VXMLString& _subSystem);
		void DoExit(VXMLString sNamelist);
		void DoExit();
		void DoDialogError(const VXMLString& sErrorDescription);
		void DoTerminated(const VXMLString& sNamelist, const VXMLString& sHandlerName);
		void DoTerminated();
		void DoEvent(const CMessage &evt);
		void DoNext(CMessage &evt);
		ObjectId GetParentCallID()const;
		std::pair<ObjectId, VXMLString> GetParentVox()const;
		VXMLString GetWavFolder()const {return m_VXMLFolders.sWavFolder;}
		VXMLString GetGrammaPath()const {return m_VXMLFolders.sGrammarFolder;}
		VXMLString GetFileFolder()const{return m_VXMLFolders.sVXMLFolder;}
		VXMLString GetVideoFolder()const{return m_VXMLFolders.sVideoFolder;}
		VXMLString GetCurDoc()const{return m_sCurDoc;}
		eWaitEvents WaitEvent        (CMessage* pPostmsg, const VXMLString& sEvents, const VXMLString& sError, CMessage& waitmsg);
		eWaitEvents WaitMultipleEvent(CMessage* pPostmsg, WaitingEvents& sEvents, const VXMLString& sError, CMessage& waitmsg);
		void CancelEvent(const VXMLString& aName, const ObjectId aCallBackId);
		void OnAuxMsg(CMessage& msg);
		//CNameTable* AddMenu(CMessage& msg, const VXMLString& sFirstMenu);
		SessionGrammar AddGrammar(VXMLString name, VXMLString path);
		void AcceptEvent(const CMessage& msg, bool post = false);
		void SetVXML(CTag_vxml* pVXML);
		VXMLString FindGrammarEvent(const VXMLString& aGrammarName, const VXMLString& aCurDtmf);
		//VXMLString GetCurMenu()const{return m_sCurMenu;};
		CMessage CSession::CreateStatisticMsg(const VXMLString& formID)const;
		void ProccedFormComment(const VXMLString& formID , const VXMLString& comment)const;
		void ProccedLogHttpComment(const VXMLString& aComment)const;
		void ProccedBufferComment(const VXMLString& formID, const VXMLString& buffer)const;

		CComVariant FindAttrEvent(const VXMLString& aConnectionId) const;

		bool SuccessedEvent()const {return m_throwEvt;}
		void SendEvent(CMessage& evt, /*int nSendId,*/ ObjectId to/*, DWORD dwDelay*/);
		void SendEventToYourself(CMessage& evt);
		ObjectId GetParentScriptID()const;
		ObjectId GetScriptID()const;
		VXMLString GetPredefineFiles()const;
		void ChangeState(DWORD dwState);
		void SetState(DWORD dwState);
		DWORD GetState() const;
		bool DialogStart(CMessage& postmsg);
		VXMLString GetVarValue(const VXMLString& _name)const;
		VXMLString GetSpeakNumberFiles(const VXMLString& expr, int sex )const;
		VXMLString GetSpeakTimeFiles(DWORD hh, DWORD mm, DWORD ss)const;
		VXMLString GetSpeakDateFiles(DWORD dd, DWORD mm, DWORD yy)const;
		VXMLString GetSpeakMoneyFiles(const VXMLString& expr, const VXMLString& format)const;
		VXMLString GetSpeakTrafficFiles(const VXMLString& expr)const;
		VXMLString GetSpeakMinutesFiles(const VXMLString& expr)const;
		VXMLString GetSpeakSMSFiles(const VXMLString& expr)const;

		void startKeepAliveTimers(/*const ObjectId& scriptID*//*const SessionCreateParams& params*/);
		void stopKeepAliveTimers();
		bool IsActive()const{return !m_bSessionEnd;}
		void Stop();

		void ChangeCallCtrl(const ObjectId& _oldScriptID, const ObjectId& _newScriptID);

		void AddNamelistToMsg(CMessage& msg);

		//void SetKeepAlive(std::shared_ptr<CKeepAliveProvider> keep_alive) { m_keep_alive = keep_alive; }


	private:
		template <class TFunction, class...TArgs>
		void _log(TFunction aFunc, const EngineLogPtr& aLogPtr, TArgs&&...aArgs) const
		{
			const auto pLog = aLogPtr.lock();
			if (pLog)
			{
				((*pLog).*aFunc)(std::forward<TArgs>(aArgs)...);

				auto lastLog = pLog->GetLastLog();
				if (!lastLog.empty())
				{
					ProccedLogHttpComment(lastLog);
				}

			}
		}

	public:
		template <class...TArgs>
		void Log(TArgs&&...aArgs) const
		{
			_log(&CEngineLog::Log, m_Log, std::forward<TArgs>(aArgs)...);
		}

		template <class...TArgs>
		void LogExtra(TArgs&&...aArgs) const
		{
			_log(&CEngineLog::Log, m_extraLog, std::forward<TArgs>(aArgs)...);
		}
	};
}

/******************************* eof *************************************/