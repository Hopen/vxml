/************************************************************************/
/* Name     : VXMLCom\VXML\VXMLEvent.h                                  */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 2 Dec 2009                                                */
/************************************************************************/

#pragma once

#include "EvtModel.h"
#include "VXMLBase.h"
#include "VXMLNameTable.h"
#include "..//EventBridge.h"

namespace VXML
{
	class CEvent : public CNameTable
	{
	private:
		VXMLString	m_sName;
		bool		m_busy;

	public:
		CEvent(const CMessage& msg);
		CMessage ToMsg() const;
		bool MatchMask(const VXMLString& sMask);
		const VXMLString& GetName() const;
		__declspec(property(get=GetName)) VXMLString Name;
		void SetBusy(const bool& val){m_busy = val;}
		bool GetBusy()const{return m_busy;}
		__declspec(property(get=GetBusy,put=SetBusy)) bool Busy;
		void Invalidate();
	};

	class CLockEvent
	{
	public:
		CLockEvent(CEvent* pEvent):m_pEvent(pEvent)
		{
			if (m_pEvent)
				m_pEvent->Busy = true;
		}
		~CLockEvent()
		{
			if (m_pEvent) 
				m_pEvent->Busy = false;
		}
	private:
		CEvent* m_pEvent;
	};
}
