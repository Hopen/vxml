/************************************************************************/
/* Name     : VXMLCom\VXML\VXMLTags.cpp                                 */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 2 Dec 2009                                                */
/************************************************************************/
#include "..\StdAfx.h"
#include  <io.h>
#include "sv_strutils.h"
#include "..\Engine\EngineLog.h"
#include "VXMLTags.h"
#include "VXMLSession.h"
#include "VXMLEngine.h"
//#include "VXMLHTTPReader.h"
#include "VXMLBSClient.h"
#include "VXMLExecContainer.h"

#import "..\..\Bin/VXMLPassStorage.dll" no_namespace named_guids

using namespace enginelog;

#define CLEARDIGITS CMessage msg_cleardigits(L"CLEAR_DIGITS"); \
CMessage wait_cleardigits; \
msg_cleardigits[L"DestinationAddress"] = pCtx->GetParentCallID(); \
if (pCtx->WaitEvent(&msg_cleardigits, L"clear_digits.completed", L"error", wait_cleardigits)!= WE_SUCCESS) { pCtx->DoExit(); return false; }
//if (pCtx->pCtx->WaitEvent(&msg_cleardigits, L"clear_digits.completed", L"error", wait_cleardigits)== WE_STOPTHREAD) ::ExitThread(0);

#define STOPCHANNEL CMessage msg_stopchannel(L"STOP_CHANNEL"); \
CMessage wait_stopchannel; \
msg_stopchannel[L"DestinationAddress"] = pCtx->GetParentCallID(); \
pCtx->SendEvent(msg_stopchannel, pCtx->GetParentScriptID());
//if (pCtx->pCtx->WaitEvent(&msg_stopchannel, L"stop_channel.completed", L"error", wait_stopchannel)== WE_STOPTHREAD) ::ExitThread(0);


#define INCNOINPUT 	int count = pCtx->ExprEvalToInt(L"noinput_count")+1; \
pCtx->ExecNormalStatement(L"noinput_count = \"" + Utils::toStr(count) + L"\"");

#define CLEARCOUNTERS pCtx->ExecNormalStatement(L"noinput_count = \"0\""); \
	pCtx->Log(LEVEL_INFO,__FUNCTIONW__, L"CLEARCOUNTERS"); \
	pCtx->ExecNormalStatement(L"nomatch_count = \"0\""); \
	pCtx->ExecNormalStatement(L"prompt_count = \"0\"");

template <class TFunction>
class Holder
{
public:
	Holder(TFunction aFunction)
		: mBackupFunction(aFunction)
	{}

	~Holder()
	{
		mBackupFunction();
	}
	private:
		TFunction mBackupFunction;
};

#include "EngineConfig.h"

namespace VXML
{

	//static SessionPtr2 GetSessionPtr(CExecContainer* pCtx)
	//{
	//	auto pSes = pCtx->pSes.lock();
	//	if (pSes)
	//	{
	//		return pSes;
	//	}

	//	throw std::runtime_error("Cannot get session pointer");
	//	return nullptr;
	//}


	const int TRANSFERAUDIOREPEATE = 99;

	static void TranslateURI(const VXMLString& _root_folder, const VXMLString& _curdoc, const VXMLString& _uri, VXMLString& _file, VXMLString& _menu)
	{
		bool bFile       = true;
		bool bRootFolder = false;
		if (_uri[0] == '/')
			bRootFolder = true;

		for (unsigned int i=(bRootFolder||_uri[0] == '\\')?1:0;i<_uri.length();++i)
		{
			if (_uri[i] == '#')
			{
				//_menu = L"#";
				bFile = false;
				continue;
			}
			if (bFile)
			{
				_file+=_uri[i];
			}
			else
			{
				_menu+=_uri[i];
			}
		}
		// for URL
		if (_file.find(L"http://") == 0)
		{
			return;
		}
		// for absolute URI
		WIN32_FIND_DATA fileData;
		int pos = -1;
		if (!Utils::GetFile(_file + L".*",&fileData))
		{
			WCHAR fileExt[MAX_PATH] ; ZeroMemory(fileExt ,sizeof(fileExt ));
			std::wstring filename = fileData.cFileName;
			if ((pos = filename.rfind('.')) != -1)
			{
				filename._Copy_s(fileExt,MAX_PATH,filename.length() - pos, pos);
			}
			if (!wcscmp(fileExt,L".vxml") ||
				!wcscmp(fileExt,L".xml" )   )
				return;
		}
		// URI is relative 
		if (!_file.empty())
		{
			WCHAR dir   [MAX_PATH]; ZeroMemory(dir   ,sizeof(dir   ));
			WCHAR ExtDir[MAX_PATH]; ZeroMemory(ExtDir,sizeof(ExtDir));
			if ((pos = _file.rfind('\\')) != -1)
			{
				_file._Copy_s(ExtDir,MAX_PATH,pos+1); // include "\"
				_file.erase(0,pos+1);
			}
			if (bRootFolder)
			{
				wcscpy_s(dir,_root_folder.c_str());
			}
			else if ((pos = _curdoc.rfind('\\')) != -1)
			{
				_curdoc._Copy_s(dir,MAX_PATH,pos+1); // include "\"
			}
			VXMLString mask = dir + VXMLString(ExtDir) + _file + L".*";
			if (!Utils::GetFile(mask,&fileData))
			{
				WCHAR fileExt[MAX_PATH] ; ZeroMemory(fileExt ,sizeof(fileExt ));
				WCHAR fileName[MAX_PATH]; ZeroMemory(fileName,sizeof(fileName));
				std::wstring filename = fileData.cFileName;
				if ((pos = filename.rfind('.')) != -1)
				{
					filename._Copy_s(fileExt,MAX_PATH,filename.length() - pos, pos);
					filename._Copy_s(fileName,MAX_PATH,pos);
				}
				if (!_file.compare(fileName)&& 
					(!wcscmp(fileExt,L".vxml") ||
					!wcscmp(fileExt,L".xml" )   ))
					_file = dir + VXMLString(ExtDir)+ _file + fileExt;
			}

		}
	}

	static bool IsCatchEvent(const VXMLString& _name)
	{
		if (_name == L"nomatch" || 
			_name == L"noinput" || 
			_name == L"error"   || 
			_name == L"help"    ||
			_name == L"catch"  	  ) 
			return true;
		return false;
	}


	bool CEvtScope::ExecCond(CExecContainer* pCtx)
	{
		if(m_sCond.empty())
			return true;
		return pCtx->ExprEvalToBool(m_sCond);
	}

	class CScopeSaver
	{
	public:
		CScopeSaver(CExecContainer* pCtx, const VXMLString& sName)
		{
			m_pVar = pCtx;
			if (m_pVar)
				m_pVar->PushScope(sName);
		}
		~CScopeSaver()
		{
			if (m_pVar)
				m_pVar->PopScope();
		}	
	private:
		CExecContainer* m_pVar = nullptr;
	};

	CComVariant CTag::EvalAttrEvent(CExecContainer* pCtx,
		const VXMLString& sAttrName,
		VARTYPE evalType)
	{
		VXMLString sAttrVal = sAttrName;
		if (GetAttrVal(sAttrName, sAttrVal))
		{
			return pCtx->ExprEvalToType(sAttrVal, evalType);
		}

		CComVariant vExpr = pCtx->FindAttrEvent(sAttrName);
		if (vExpr.vt != evalType)
		{
			if (FAILED(vExpr.ChangeType(evalType)))
			{
				VXMLVariant::ChangeType(vExpr, VT_BSTR);
				return pCtx->ExprEvalToType(vExpr.bstrVal, evalType);
			}
		}
		return vExpr;
	}

		CTag::CTag(
			const Factory_c::CollectorPtr& _parser, 
			const CTagFactory& _factory,
			EventScope& _scope, 
			EventScope& _prompts, 
			const VXMLString& _docUri)
		{
			m_sName   = _parser->GetName();
			m_sText   = _parser->GetText();
			m_Attribs = _parser->GetAttr();
			m_iLine   = _parser->GetLine();
		}

	CTag::~CTag()
	{
	}

	void CTag::EmitError(CExecContainer* pCtx, CMessage& evt, const VXMLString& description, const VXMLString& reason)
	{
		evt[L"reason"]	= reason;
		evt[L"tagname"]	= GetName();
		evt[L"description"] = description.c_str();
		pCtx->DoError(evt, L"EXECTAGPROC");
	}

	bool CTag::CheckDigits(CExecContainer* pCtx)
	{
		auto curDTMF = pCtx->GetValue(L"utterance");

		VXMLString sTermChar = _bstr_t(pCtx->GetValue(L"termchar").vValue.bstrVal);

		VXMLString sDigits; sDigits.clear();
		if (!curDTMF.IsEmpty())
		{
			sDigits = _bstr_t(curDTMF.vValue.bstrVal);
		}
		//if (curDTMF.IsEmpty())
		if (sDigits.empty())
		{
			CMessage msg(L"GET_DIGITS");
			msg[L"DigitMask"]  = sTermChar;
			//VAR * pTimeOut  = pCtx->Find(L"timeout");
			//VAR * pWaitTime = pCtx->Find(L"WaitTime");
			auto timeOut = pCtx->GetValue(L"timeout");
			auto waitTime = pCtx->GetValue(L"WaitTime");

			if (!timeOut.IsEmpty())
			{
				msg[L"MaxSilence"] = Utils::toNum<int>(_bstr_t(timeOut.vValue.bstrVal).GetBSTR());
			}
			if (!waitTime.IsEmpty())
			{
				msg[L"MaxTime"] = Utils::toNum<int>(_bstr_t(waitTime.vValue.bstrVal).GetBSTR());
			}
			CMessage waitmsg;
			VXMLString sId;
			msg[L"DestinationAddress"] = pCtx->GetParentCallID();
			eWaitEvents retEvent = pCtx->WaitEvent(&msg, L"get_digits.completed", L"error", waitmsg);
			if (retEvent == WE_SUCCESS)
			{
				// SUCCESSED
				if (CParam* pParam = waitmsg.ParamByName(L"DigitsBuffer"))
				{
					if (!pParam->AsWideStr().empty())
					{
						pCtx->AddVar(L"utterance",VXMLString(pParam->AsWideStr()).c_str(),false);
					}
				}
				CLEARDIGITS;
			}
			else
			{
				pCtx->DoExit();
				//pCtx->DoDialogError();
				return false;
			}
		}

		return true;
	}


	CMessage CTag::CreatePlayWavMessage(CExecContainer* pCtx, const VXMLString& _uri)
	{
		VXMLString sSpeaker, sLanguage;
		auto speaker = pCtx->GetValue(L"Speaker");
		if (!speaker.IsEmpty())
			sSpeaker = _bstr_t(speaker.vValue.bstrVal);
		auto language = pCtx->GetValue(L"Language");
		if (!language.IsEmpty())
			sLanguage = _bstr_t(language.vValue.bstrVal);

		CMessage msg(L"PLAY_WAV");

		auto termchar = pCtx->GetValue(L"termchar");
		if (termchar.IsEmpty())
		{
			throw std::runtime_error("cannot find \"termchar\"");
		}

		VXMLString sTermChar = _bstr_t(termchar.vValue.bstrVal);

		//pCtx->Log(LEVEL_FINEST, __FUNCTIONW__, L"termchar == %s, vartype = %i", sTermChar.c_str(), termchar.vValue.vt);

		LPCWSTR p = _uri.c_str(), q;
		VXMLString sParamName, sParamVal;
		int k = 0;

		while (*p)
		{
			q = wcschr(p, L';');
			if (!q)
			{
				sParamVal = trim_wstring(p);
			}
			else
			{
				sParamVal = trim_wstring(VXMLString(p, q - p));
			}
			if (!sParamVal.empty())
			{
				if (sParamVal[0] == '/' || sParamVal[0] == '\\')
					sParamVal.erase(0,1);
				sParamName = format_wstring(L"File%d", ++k);
				VXMLString sFolder = pCtx->GetWavFolder();
				WCHAR szBuf[1024];
				PathCombineW(szBuf, szBuf, sFolder.c_str());
				if (!sSpeaker.empty())
					PathCombineW(szBuf, szBuf, sSpeaker.c_str());
				if (!sLanguage.empty())
					PathCombineW(szBuf, szBuf, sLanguage.c_str());
				PathCombineW(szBuf, szBuf, sParamVal.c_str());
				msg[sParamName.c_str()] = szBuf;
			}
			if (!q)
			{
				break;
			}
			p = q + 1;
		}

		msg[L"FilesCount"] = k;
		msg[L"DigitMask"] = sTermChar;

		return msg;
	}

	CParentTag::CParentTag(
		const Factory_c::CollectorPtr& _parser, 
		const CTagFactory& _factory,
		EventScope& _scope, 
		EventScope& _prompts, 
		const VXMLString& _docUri) 
		: CTag(_parser, _factory, _scope, _prompts, _docUri)
	{
		const auto& children = _parser->GetChildren();
		for (const auto& child : children)
		{
			VXMLString sName = child->GetName();
			if (sName != L"text" && sName != L"#comment")	// skip meta tags and comments
			{
				if (sName == L"menu" ||
					sName == L"form" ||
					sName == L"field" ||
					sName == L"initial" ||
					sName == L"transfer" ||
					sName == L"record" ||
					sName == L"subdialog")
				{
					CEvtScope newScope;
					_scope.push_back(newScope); // insert flag
				}
				m_Children.emplace_back(_factory.CreateTag(sName, child, _factory, _scope, _prompts, _docUri));
			}
		}
	}

	CParentTag::~CParentTag()
	{
	}

	bool CParentTag::Execute(CExecContainer* pCtx)
	{
		for each (TagPtr pTag in m_Children)
		{
			if (pCtx->SuccessedEvent())
				break;
			if(!ExecuteChild(pCtx, pTag))
				return false;
		}
		return true;
	}

	bool CParentTag::ExecuteChild(CExecContainer* pCtx, TagPtr pTag)
	{
		try
		{
			pCtx->Log(LEVEL_FINEST, __FUNCTIONW__, L"Executing tag <%s>, line = \"%i\"", pTag->GetName().c_str(), pTag->GetLine());
			pCtx->WaitForStep(pTag->GetLine()); // wait until DoStep execute
			if (!pCtx->IsActive())
			{
				pCtx->Log(LEVEL_FINEST, __FUNCTIONW__, L"Cannot execute message, current session does not active");
				return false;
			}
			if (!pTag->Execute(pCtx))
			{
				pCtx->Log(LEVEL_WARNING, __FUNCTIONW__, L"Executing tag <%s> returned FALSE, line = \"%i\"", pTag->GetName().c_str(), pTag->GetLine());
				return false;
			}
			return true;

		}
		catch (std::wstring& err)
		{
			EmitError(pCtx, CMessage(L"error.semantic"), L"A run-time error was found in the VoiceXML document", err);
		}
		catch (_com_error& err)
		{
			LPCWSTR pwszErr = (LPCWSTR)err.Description();
			EmitError(pCtx, CMessage(L"error.semantic"), L"A run-time error was found in the VoiceXML document", pwszErr ? pwszErr : L"<null>");
		}
		catch (...)
		{
			pCtx->Log(LEVEL_WARNING, __FUNCTIONW__, L"CParentTag::ExecuteChild: exception !!!");
			return false;
		}
		return true;
	}

	// CTextTag implementation
	const VXMLString& CTextTag::GetText() const
	{
		return m_sText;
	}

	// CExecutableTag implementation
	CExecutableTag::CExecutableTag(
		const Factory_c::CollectorPtr& _parser, 
		const CTagFactory& _factory, 
		EventScope& _scope, 
		EventScope& _prompts, 
		const VXMLString& _docUri) 
		: CParentTag(_parser, _factory, _scope, _prompts, _docUri)
	{
		EventScope events(_scope.begin(), _scope.end());
		// set own scope for each menu
		if (_scope.size()>1)
		{
			EventScope::reverse_iterator cit = _scope.rbegin();
			EventScope::reverse_iterator dit = _scope.rend();
			while (cit!=_scope.rend())
			{
				if (cit->GetEventName().empty())
				{
					// remember position of flag
					if (dit == _scope.rend())
						dit = cit;
					++cit;
					continue;
				}

				m_EvtScope.push_back(*cit);
				++cit;
			}
			if (dit!=_scope.rend())
			{
				EventScope copy(++dit,_scope.rend());
				copy.reverse();
				_scope.clear();
				_scope = copy;
			}
		}
		else
			_scope.clear();

		//
		m_Prompts = _prompts;
		_prompts.clear();
	}

	bool CExecutableTag::ExecEvents(CExecContainer* pCtx, const VXMLString& event/*, bool incCounter / *== true* /*/)
	{

		VXMLString sEventName(event);
		int event_count = 1;
		if (!sEventName.empty())
		{
			CAtlStringW sErrorEventName = CAtlStringW(sEventName.c_str());

			if ( (!sErrorEventName.CompareNoCase(L"noinput")) ||
				 (!sErrorEventName.CompareNoCase(L"nomatch")) 
				 )
			{
				event_count = pCtx->ExprEvalToInt(sEventName+L"_count")+1;
				pCtx->Log(LEVEL_FINEST, __FUNCTIONW__, L"Increase event count: \"%s\", count: %d", sEventName.c_str(), event_count);
				pCtx->ExecNormalStatement(sEventName + L"_count = \"" + Utils::toStr(event_count) + L"\"");
			}
		}
		else
		{
			pCtx->Log(LEVEL_INFO, __FUNCTIONW__, L"unexpected execute event: \"%s\"", sEventName.c_str());
			return false;
		}

		EventScope resEvents(m_EvtScope);
		// 1. Remove from this list all catches whose event name does not match the event 
		//     being thrown or whose cond evaluates to false after conversion to boolean

		resEvents.remove_if(CMatchNameAndCondition(pCtx,sEventName));

		// 2. Find the "correct count": the highest count among the catch elements still 
		//     on the list less than or equal to the current count value. 

		//resEvents.sort(CHighestCount(event_count));
		EventScope::iterator rit = resEvents.end();

		for (EventScope::iterator cit = resEvents.begin(); cit!=resEvents.end();++cit)
		{
			int cur_count = cit->GetCount();
			if (cur_count == event_count)
			{
				rit = cit;
				break;
			}
			if (cur_count < event_count && (rit == resEvents.end() || cur_count > rit->GetCount()))
			{
				rit = cit;
			}
		}

		// 3. Select the first element in the list with the "correct count". 
		if (rit!=resEvents.end())
		{
			pCtx->Log(LEVEL_INFO, __FUNCTIONW__, L"Processing event: \"%s\", count: %d", sEventName.c_str(), event_count);
			return rit->Execute(pCtx);
		}
		else
		{
			pCtx->Log(LEVEL_INFO, __FUNCTIONW__, L"unexpected execute event: \"%s\"", sEventName.c_str());
			return false;
		}
		return true;
	}

	bool CExecutableTag::MatchPrompt(VXML::TagPtr promptTag, VXML::CExecContainer *pCtx)
	{
		if (pCtx->ExprEvalToStr(L"reprompt")==L"0")
			return false;

		VXMLString sTagPromptCount; promptTag->GetAttrVal(L"count", sTagPromptCount);
		VXMLString sCurPromprCount = pCtx->ExprEvalToStr(L"prompt_count");

		int tag_count    = sTagPromptCount.empty()?1:Utils::toNum<int>(sTagPromptCount);
		int prompt_count = 1 + (sCurPromprCount.empty()?0:Utils::toNum<int>(sCurPromprCount));

		
		EventScope::iterator curprompt_it = std::find_if(m_Prompts.begin(),m_Prompts.end(),CFindCurPrompt(tag_count));

		EventScope::iterator rit = m_Prompts.end();
		for (EventScope::iterator cit = m_Prompts.begin(); cit!=m_Prompts.end();++cit)
		{
			int cur_count = cit->GetCount();
			if (cur_count == prompt_count)
			{
				rit = cit;
				break;
			}
			if (cur_count < prompt_count && (rit == m_Prompts.end() || cur_count > rit->GetCount()))
			{
				rit = cit;
			}
		}
		
		return (rit!=m_Prompts.end() && curprompt_it == rit);
	}

	// Tags implementation

	CTag_audio::~CTag_audio()
	{
		int test = 0;
	}

	bool CTag_audio::Execute(VXML::CExecContainer *pCtx)
	{
		VXMLString sSrc;
		VXMLString sExpr;
		VXMLString sURI;
		if (GetAttrVal(L"src",sSrc))
		{
			sURI = sSrc;
		}
		else if (GetAttrVal(L"expr",sExpr))
		{
			sURI = pCtx->ExprEvalToStr(sExpr);
		}

		if (sURI.empty())
			return true;

		return PlayAudio(sURI,pCtx);
	}

	bool CTag_audio::PlayAudio(const VXMLString& sURI, CExecContainer* pCtx)
	{
		pCtx->Log(LEVEL_INFO, __FUNCTIONW__, L"Play audio, src = %s", sURI.c_str());
		CMessage msg = CreatePlayWavMessage(pCtx, sURI);
		pCtx->Log(LEVEL_INFO, __FUNCTIONW__, L"PlayWavMessage created");
		CMessage waitmsg;
		VXMLString sId;
		msg[L"DestinationAddress"] = pCtx->GetParentCallID();
		eWaitEvents retEvent = pCtx->WaitEvent(&msg, L"wav.completed", L"error", waitmsg);
		if (retEvent == WE_SUCCESS)
		{
			CParam* pParam = waitmsg.ParamByName(L"TerminationReason");
			if (!pParam)
				return true;
			if (pParam->AsString() == L"TM_DIGIT" /*|| pParam->AsString() == L"TM_EOD"*/)
			{
				// we're got digits
				if (CParam* pParam = waitmsg.ParamByName(L"DigitsBuffer"))
				{
					VXMLString digits(pParam->AsString());
					if (!digits.empty())
					{
						pCtx->AddVar(L"utterance",VXMLString(pParam->AsString()).c_str(),false);
						CLEARDIGITS;
					}
				}
			}
		}
		else //if (retEvent == WE_STOPTHREAD)
		{
			// stop tone playing
			CMessage waitmsg, stop_chanel_msg(L"STOP_CHANNEL");
			msg[L"DestinationAddress"] = pCtx->GetParentCallID();
			pCtx->SendEvent(stop_chanel_msg, pCtx->GetParentScriptID());
			pCtx->DoExit();
			return false;
		}

		return true;
	}

	bool CTag_block::Execute(VXML::CExecContainer *pCtx)
	{
		VXMLString cond;
		if (GetAttrVal(L"cond",cond) && !pCtx->ExprEvalToBool(cond))
			return true;
		return __super::Execute(pCtx); 
	}

	CTag_catch::CTag_catch(
		const Factory_c::CollectorPtr& _parser, 
		const CTagFactory& _factory, 
		EventScope& _scope, 
		EventScope& _prompts, 
		const VXMLString& _docUri)
		: CParentTag(_parser, _factory, _scope, _prompts, _docUri)
	{ 
		try
		{
			
			VXMLString sAllEvents, sEvent, sCount, sCond;
			GetAttrVal(L"event", sAllEvents);
			GetAttrVal(L"count", sCount);
			GetAttrVal(L"cond", sCond);

			LPCWSTR p = sAllEvents.c_str(), q;

			while (*p)
			{
				q = wcschr(p, L' ');
				if (!q)
				{
					sEvent = trim_wstring(p);
				}
				else
				{
					sEvent = trim_wstring(VXMLString(p, q - p));
				}
				if (!sEvent.empty())
				{
					CEvtScope pNew(sEvent, sCount.empty() ? 1 : Utils::toNum<int>(sCount), sCond, this, _docUri);
					_scope.push_back(pNew);
				}
				if (!q)
				{
					break;
				}
				p = q + 1;
			}
		}
		catch (...)
		{
			//EmitError(pCtx, CMessage(L"error.unrecoverable"), L"When execute \"catch\" tag");
		}
	}

	bool CTag_catch::Execute(VXML::CExecContainer *pCtx)
	{
		CScopeSaver saver(pCtx, L"catch");
		return __super::Execute(pCtx); // - action
	}

	bool CTag_choice::Execute(CExecContainer* pCtx)
	{
		if (Match(pCtx,NULL)== L"filled")
		{
			// Set menu value
			auto curDTMF  = pCtx->GetValue(L"utterance");
			VXMLString statement = pCtx->GetCurMenu()+L"=\""+VXMLString(_bstr_t(curDTMF.vValue.bstrVal)) + L"\"";
			pCtx->ExecNormalStatement(statement.c_str());

			//Clear digits
			pCtx->ExecNormalStatement(L"reprompt = \"1\"");

			CLEARCOUNTERS;

			//execute child tags
			if (!__super::Execute(pCtx))
				return false;

			// decide where to go next
			VXMLString sVal;
			VXMLString sNext;
			if (GetAttrVal(L"next",sVal))
			{
				sNext = sVal;
			}
			else if (GetAttrVal(L"expr",sVal))
			{
				sNext = pCtx->ExprEvalToStr(sVal);
			}

			if (!sNext.empty())
			{
				VXMLString file, 
					menu;

				pCtx->Log(LEVEL_INFO, __FUNCTIONW__, L"Translate URI: RootFolder - <%s>, CurDoc - <%s>, URI - <%s>, File - <%s>, Menu - <%s>",
					pCtx->GetFileFolder().c_str(),
					pCtx->GetCurDoc().c_str(),
					sNext.c_str(),
					file.c_str(),
					menu.c_str()
					);
				TranslateURI(pCtx->GetFileFolder(),pCtx->GetCurDoc(),sNext,file,menu);
				pCtx->Log(LEVEL_INFO, __FUNCTIONW__, L"Translate URI: File - <%s>",
					file.c_str()
					);
				pCtx->Log(LEVEL_INFO, __FUNCTIONW__, L"Translate URI: Menu - <%s>",
					menu.c_str()
					);
				CMessage menu_msg(menu.c_str());
				pCtx->SetCurMenu(menu);
				pCtx->Log(LEVEL_INFO, __FUNCTIONW__, L"Executing next menu: <%s>", sNext.c_str());
				CMessage msg(L"NextMenu");
				msg[L"id"  ] = menu;
				msg[L"field_name"] = L"";
				if (file.length()>0) // going load another document
				{
					msg[L"file"] = file; // !!! ToDo: replace "file" by "FileName" in all documents
					msg.SetName(L"LoadNewDocument");
				}
				//pCtx->pSes->CorrectEvent(msg);
				pCtx->DoNext(msg);
			}

		}

		return true;
	}

	VXMLString CTag_choice::Match(CExecContainer* pCtx, VAR* pGrammar)
	{
		VXMLString event; event.clear();

		VXMLString sExpr;
		VXMLString sDTMF;
		if (GetAttrVal(L"dtmf",sDTMF))
		{

		}
		else if (GetAttrVal(L"expr",sExpr))
		{
			sDTMF = pCtx->ExprEvalToStr(sExpr);
		}

		if (sDTMF.empty())
			return event;

		auto termChar = pCtx->GetValue(L"termchar");
		auto curDTMF  = pCtx->GetValue(L"utterance"/*L"digits"*/);

		if (curDTMF.IsEmpty())
			return event;

		if ((sDTMF+VXMLString(_bstr_t(termChar.vValue))) == VXMLString(_bstr_t(curDTMF.vValue.bstrVal)))
			event = L"filled";

		if (sDTMF == VXMLString(_bstr_t(curDTMF.vValue.bstrVal)))
			event = L"filled";

		return event;
	}

	bool CTag_else::Execute(CExecContainer* pCtx)
	{
		// Nothing to do - this tag is processed by <if>
		return true;
	}

	bool CTag_elseif::Execute(CExecContainer* pCtx)
	{
		// Nothing to do - this tag is processed by <if>
		return true;
	}

	CTag_error::CTag_error(
		const Factory_c::CollectorPtr& _parser, 
		const CTagFactory& _factory,
		EventScope& _scope, 
		EventScope& _prompts, 
		const VXMLString& _docUri)
		: CParentTag(_parser, _factory, _scope, _prompts, _docUri)
	{ 
		try
		{
			VXMLString sName(L"error");
			VXMLString sCount, sCond;
			GetAttrVal(L"count", sCount);
			GetAttrVal(L"cond", sCond);

			CEvtScope pNew(sName, sCount.empty() ? 1 : Utils::toNum<int>(sCount), sCond, this, _docUri);
			_scope.push_back(pNew);
		}
		catch (...)
		{
			//EmitError(pCtx, CMessage(L"error.unrecoverable"), L"When execute \"error\" tag");
		}
	}

	bool CTag_error::Execute(VXML::CExecContainer *pCtx)
	{
		CScopeSaver saver(pCtx,L"error");
		return __super::Execute(pCtx); // - action
	}

	bool CTag_exit::Execute(VXML::CExecContainer *pCtx)
	{
		VXMLString sReturnName;
		if (GetAttrVal(L"namelist",sReturnName))
		{
		}
		pCtx->DoExit(sReturnName);
		return true;
	}

	bool CTag_field::Execute(CExecContainer* pCtx)
	{
		VXMLString field_name; field_name = GetAttrVal(L"name", field_name)?field_name:L"field_name";
		CScopeSaver saver(pCtx,field_name);
		pCtx->ExecNormalStatement(field_name + L"= \"\"");

		m_dtmf_audioinput.clear();
		m_bVideoCall = false;
		for each (TagPtr pTag in m_Children)
		{
			if (pCtx->SuccessedEvent())
				break;
			// skip all events
			if (IsCatchEvent(pTag->GetName()))
				continue;

			if (pTag->GetName() == L"prompt" )
			{
				if (!MatchPrompt(pTag,pCtx))
					continue;
				CTag_prompt *pPromptTag = dynamic_cast<CTag_prompt*>(pTag.get());
				if (pPromptTag)
					pPromptTag->SetOwnerTag(this);
			}

			if (pTag->GetName() == L"filled")
			{
				CTag_filled *pFilledTag = dynamic_cast<CTag_filled*>(pTag.get());
				if (pFilledTag)
					pFilledTag->SetCurField(this);
			}


			if (!ExecuteChild(pCtx, pTag))
				return false;
		}
		//pCtx->PopScope();
		return true;
	}

	bool CTag_field::CheckDigits(CExecContainer* pCtx)
	{
		//if (WasVideoCall())
		if (m_bVideoCall)
			return true;
		return __super::CheckDigits(pCtx);
	}

	bool CTag_filled::Execute(VXML::CExecContainer *pCtx)
	{
		// form & menu
		CExecutableTag* pCurField = GetCurField();
		if (!pCurField)
			return true;
		VXMLString sCurFieldName = pCurField->GetAttrVal(L"name");
		if (!CheckDigits(pCtx))
			return false;
		VXMLString exist_event = Match(pCtx, pCtx->GetValue(L"curgrammar"));
		VXMLString dtmfValue;

		auto executableTagId = pCurField->GetExecutableTagId();

		bool bSkipCommitBuffer = executableTagId == L"subdialog" 
			|| executableTagId == L"transfer";

		//if (dynamic_cast)

		bool success = true;
		if (exist_event != L"filled")
		{
			if (exist_event.empty())
			{
				auto utterance = pCtx->GetValue(L"utterance");
				dtmfValue = utterance.vValue.bstrVal;
				exist_event = L"noinput";
				if (!utterance.IsEmpty() && !dtmfValue.empty())
				{
					exist_event = L"nomatch";
				}

				//exist_event = !(pCtx->GetValue(L"utterance")).IsEmpty()?L"nomatch":L"noinput";
			}

			CMessage msg(L"event");
			msg[L"event_name"] = exist_event.c_str();
			pCtx->DoEvent(msg);
		}
		else
		{
			pCtx->ExecNormalStatement(L"reprompt = \"1\"");
			CLEARCOUNTERS;

			auto curDTMF = pCtx->GetValue(L"utterance"/*L"digits"*/);
			if (!curDTMF.IsEmpty())
			{
				dtmfValue = curDTMF.vValue.bstrVal;
				VXMLString statement = sCurFieldName/*GetCurField(pCtx)*/ + L"=\"" + dtmfValue + L"\"";
				pCtx->ExecNormalStatement(statement.c_str());
			}

			success = __super::Execute(pCtx);
		}

		if (!bSkipCommitBuffer)
		{
			pCtx->ProccedBufferComment(sCurFieldName, dtmfValue);
		}

		return success;
	}

	VXMLString CTag_filled::Match(CExecContainer* pCtx, VAR pGrammar)
	{
		VXMLString event(L"filled"); //event.clear();
		if (pGrammar.IsEmpty())
			return event;

		// find active grammar
		VXMLString grammar_name = _bstr_t(pGrammar.vValue.bstrVal);

		auto inputMode = pCtx->GetValue(L"inputmode");
		if (!inputMode.IsEmpty())
		{
			if (_bstr_t(inputMode.vValue.bstrVal) == _bstr_t(L"voice"))
			{
				// ToDo: !!!

				return event;
			}

			if (_bstr_t(inputMode.vValue.bstrVal) == _bstr_t(L"dtmf"))
			{
				// check input value
				event.clear();
				auto curDTMF = pCtx->GetValue(L"utterance");
				if (curDTMF.IsEmpty())
					return event;

				VXMLString value = curDTMF.vValue.bstrVal;
				if (value.empty())
				{
					return event;
				}

				// matching...
				event = pCtx->FindGrammarEvent(grammar_name, value);
				return event;
			}
		}
		return event;
	}

	bool CTag_filled::CheckDigits(CExecContainer* pCtx)
	{
		if (m_pCurField)
			return m_pCurField->CheckDigits(pCtx);

		return __super::CheckDigits(pCtx);
	}


	bool CTag_form::Execute(CExecContainer* pCtx)
	{
		//pCtx->PushScope(L"form");
		CScopeSaver saver(pCtx, L"form");
		pCtx->AddVar(L"curgrammar",L"",false);

		CMessage msg(L"NextMenu");
		VXMLString formID; formID.clear();
		msg[L"id"]   = formID = GetAttrVal(L"id");
		//pCtx->pSes->CorrectEvent(msg);

		VXMLString sComment; sComment.clear();
		if (GetAttrVal(L"comment",sComment))
		{
			pCtx->ProccedFormComment(formID,sComment);
		}


		CTag_field *pExecField = NULL;
		VXMLString audioinput;
		for each (TagPtr pTag in m_Children)
		{
			if (pCtx->SuccessedEvent())
				break;
			// skip all events
			if (IsCatchEvent(pTag->GetName()))
				continue;

			//CGrammarConteiner save_grammar;
			if (pTag->GetName() == L"field" || pTag->GetName() == L"subdialog" || pTag->GetName() == L"transfer" || pTag->GetName() == L"record")
			{
				VXMLString tag_field = pTag->GetAttrVal(L"name");
				VXMLString cur_field = GetCurFieldName();
				if (!cur_field.empty() && cur_field != tag_field)
					continue;

				SetCurFieldName(tag_field);
				pExecField = dynamic_cast<CTag_field*>(pTag.get());

			}

			if (pTag->GetName() == L"filled")
			{
				VXMLString sNameList;
				if (pTag->GetAttrVal(L"namelist",sNameList) && !sNameList.empty())
				{
					VXMLString sCurField = GetCurFieldName();
					if (sCurField != sNameList)
						continue;
				}
				CTag_filled *pFilledTag = dynamic_cast<CTag_filled*>(pTag.get());
				if (pFilledTag)
				{
					VXMLString sAudioInput = pExecField->GetAudioInput();
					if (!sAudioInput.empty())
					{
						pCtx->AddVar(L"utterance",sAudioInput.c_str(),false);
					}
					
					pFilledTag->SetCurField(pExecField);
				}

			}
			if (!ExecuteChild(pCtx, pTag))
				return false;

		}
		return true;
		//pCtx->PopScope();
	}

	TagPtr CTag_form::GetField(const VXMLString& _name)
	{
		for each (TagPtr pTag in m_Children)	
		{
			if ( (pTag->GetName() == L"field" || pTag->GetName() == L"subdialog" || pTag->GetName() == L"transfer") && pTag->GetAttrVal(L"name") == _name)
			{
				return pTag;				
			}
		}
		return TagPtr();
	}

	bool CTag_goto::Execute(CExecContainer* pCtx)
	{
		pCtx->Log(LEVEL_FINEST, __FUNCTIONW__, L"CTag_goto::Execute: erase counters");
		pCtx->ExecNormalStatement(L"reprompt = \"1\"");
		pCtx->ExecNormalStatement(L"prompt_count = \"0\"");

		VXMLString sVal;
		VXMLString sNext;
		pCtx->Log(LEVEL_FINEST, __FUNCTIONW__, L"CTag_goto::Execute: GetAttrVal next");
		if (GetAttrVal(L"next",sVal))
		{
			sNext = sVal;
		}
		else 
		{
			pCtx->Log(LEVEL_FINEST, __FUNCTIONW__, L"CTag_goto::Execute: GetAttrVal expr");
			if (GetAttrVal(L"expr",sVal))
			{
				sNext = pCtx->ExprEvalToStr(sVal);
			}
		}

		if (!sNext.empty())
		{
			VXMLString file, 
					   menu;

			pCtx->Log(LEVEL_INFO, __FUNCTIONW__, L"Translate URI: RootFolder - <%s>, CurDoc - <%s>, URI - <%s>, File - <%s>, Menu - <%s>",
				pCtx->GetFileFolder().c_str(),
				pCtx->GetCurDoc().c_str(),
				sNext.c_str(),
				file.c_str(),
				menu.c_str()
				);
			TranslateURI(pCtx->GetFileFolder(),pCtx->GetCurDoc(),sNext,file,menu);
			pCtx->Log(LEVEL_FINEST, __FUNCTIONW__, L"Translate URI: File - <%s>",
				file.c_str()
				);
			pCtx->Log(LEVEL_FINEST, __FUNCTIONW__, L"Translate URI: Menu - <%s>",
				menu.c_str()
				);


			pCtx->Log(LEVEL_FINEST, __FUNCTIONW__, L"CTag_goto::Execute: add new menu");
			pCtx->SetCurMenu(menu);
			pCtx->Log(LEVEL_INFO, __FUNCTIONW__, L"Executing next menu: <%s>", sNext.c_str());
			CMessage msg(L"NextMenu");
			msg[L"id"        ] = menu;
			msg[L"field_name"] = L"";
			if (file.length()>0) // it's necessary to load another document
			{
				msg[L"file"] = file;
				msg.SetName(L"LoadNewDocument");
			}
			//pCtx->pSes->CorrectEvent(msg);
			pCtx->Log(LEVEL_FINEST, __FUNCTIONW__, L"CTag_goto::Execute: DoNext");
			pCtx->DoNext(msg);

		}
		else
		{
			pCtx->Log(LEVEL_INFO, __FUNCTIONW__, L"CTag_goto::Execute: GetAttrVal nextitem");
			if (GetAttrVal(L"nextitem",sNext))
			{
				CMessage msg(L"NextMenu");
				msg[L"id"        ] = pCtx->GetCurMenu();
				msg[L"field_name"] = sNext;
				//pCtx->pSes->CorrectEvent(msg);
				pCtx->DoNext(msg);

			}
			else 
			{
				pCtx->Log(LEVEL_FINEST, __FUNCTIONW__, L"CTag_goto::Execute: EmitError");
				EmitError(pCtx, CMessage(L"error.badfetch"), L"A fetch of a document has failed", L"Exactly one of \"next\" or \"expr\" must be specified");
			}
		}

		pCtx->Log(LEVEL_FINEST, __FUNCTIONW__, L"CTag_goto::Execute: return true");
		return true;
	}

	bool CTag_grammar::Execute(CExecContainer* pCtx)
	{
		VXMLString sMode = GetAttrVal(L"mode");
		pCtx->AddVar(L"inputmode",sMode.c_str(),false);
		VXMLString sSrc = GetAttrVal(L"src");
		int pos = -1;
		if ((pos = sSrc.find(L"builtin:"))>=0)
		{
			sSrc = sSrc.replace(pos,8,L"");
			if ((pos = sSrc.find(L"dtmf/"))!= -1)
			{
				sSrc = sSrc.replace(pos,5,L"dtmf-");
			}
			if (sSrc.find(L".gr") == -1)
				sSrc = sSrc + L".gr";
		}

		pCtx->AddGrammar(sSrc, pCtx->GetGrammaPath() + sSrc);
		pCtx->AddVar(L"curgrammar",sSrc.c_str(),false);

		VXMLString term;
		if (GetAttrVal(L"term",term))
		{
			pCtx->AddVar(L"termchar", term.c_str(), false);
		}

		return true;

	}

	bool CTag_if::Execute(CExecContainer* pCtx)
	{
		// Check primary <if> condition
		bool bRes = pCtx->ExprEvalToBool(GetAttrVal(L"cond"));

		for each (TagPtr pTag in m_Children)
		{
			// If we found <else> or <elseif>, break the cycle 
			// if main <if> condition is true, so go to the end of <if>.
			// If the condition is false, evaluate it (<else> is equivalent to <elseif cond="true">)
			if (pTag->GetName() == L"else")
			{
				if (bRes)
				{
					break;
				}
				bRes = true;
			}
			else if	(pTag->GetName() == L"elseif")
			{
				if (bRes)
				{
					break;
				}
				bRes = pCtx->ExprEvalToBool(pTag->GetAttrVal(L"cond"));
			}
			else if (bRes)
			{
				// Execute child tags if we are in the true condition
				if (!ExecuteChild(pCtx, pTag))
					return false;
			}
		}

		return true;
	}

	bool CTag_log::Execute(CExecContainer* pCtx)
	{
		VXMLString sExpr, sLabel, sText;
		sText = GetAttrVal(L"expr");
		GetAttrVal(L"label", sLabel);
		sExpr = pCtx->ExprEvalToStr(sText);
		pCtx->Log(LEVEL_FINEST, __FUNCTIONW__, L"Expr: \"%s\" (source: \"%s\"), Label: \"%s\"", sExpr.c_str(), sText.c_str(), sLabel.c_str());
		return true;
	}

	//this tag will appear in version vxml 3.0 
	bool CTag_media::Execute(CExecContainer* pCtx)
	{
		CMessage play_video_msg(L"PLAY_VIDEO");
		// decide what to played
		VXMLString sSrc,sExpr,sURI, sType, sCodecs;
		if (GetAttrVal(L"src",sSrc))
		{
			sURI = sSrc;
		}
		else if (GetAttrVal(L"expr",sExpr))
		{
			sURI = pCtx->ExprEvalToStr(sExpr);
		}

		if (sURI.empty())
			return true;

		// get params
		if (GetAttrVal(L"type"  , sType  ))
		{
			play_video_msg[L"Type"] = sType;
		}
		if (GetAttrVal(L"Codecs", sCodecs))
		{
			play_video_msg[L"Codecs"] = sCodecs;
		}

		// decide, how many files do we have
		std::vector<VXMLString> VideoFiles;
		LPCWSTR p = sURI.c_str(), q;
		VXMLString sParamName, sParamVal;

		while (*p)
		{
			q = wcschr(p, L';');
			if (!q)
			{
				sParamVal = trim_wstring(p);
			}
			else
			{
				sParamVal = trim_wstring(VXMLString(p, q - p));
			}
			if (!sParamVal.empty())
			{
				VXMLString sFolder = pCtx->GetVideoFolder();
				if (!sFolder.empty() && (sParamVal[0] == '/' || sParamVal[0] == '\\'))
					sParamVal.erase(0,1);

				WCHAR szBuf[1024];
				ZeroMemory(szBuf,1024);
				if (!sFolder.empty())
					PathCombineW(szBuf, szBuf, sFolder.c_str());
				PathCombineW(szBuf, szBuf, sParamVal.c_str());
				VideoFiles.push_back(szBuf);
			}
			if (!q)
			{
				break;
			}
			p = q + 1;
		}

		// we've got <VideoFiles.size()> files to play. We should play it one by one until we don't get digits.
		//   therefore, our first action would be sending GET_DIGITS message to telnet. 
		//   after that, we're waiting 2 possible event: 
		//      1. we could get digits (GET_DIGITS_COMPLETED)
		//      2. we could get end of video playing (PLAY_VIDEO_COMPLETED)
		//   in first case, remember digits -> end.
		//   in other case, play next media file, if it exist.
		//      if not -> waiting until receive GET_DIGITS_COMPLETED event, remember digits -> end.

		CMessage waitmsg;
		unsigned int iCurrentVideoFile = 0;
		VXMLString sTermChar = _bstr_t(pCtx->GetValue(L"termchar").vValue.bstrVal);
		while (true)
		{
			play_video_msg[L"VideoFile"] = VideoFiles[iCurrentVideoFile];
			play_video_msg[L"AudioFile"] = VideoFiles[iCurrentVideoFile];
			VXMLString sId;
			play_video_msg[L"DestinationAddress"] = pCtx->GetParentCallID();
			eWaitEvents retEvent = pCtx->WaitEvent(&play_video_msg, L"play_video.acknowledge", L"error", waitmsg);
			if (retEvent == WE_STOPTHREAD)
			{
				CMessage stop_video_msg(L"STOP_VIDEO");
				pCtx->SendEvent(stop_video_msg, pCtx->GetParentScriptID());
				//::ExitThread(0);
				pCtx->DoExit();
				return false;
			}
			else if (retEvent == WE_SUCCESS)
			{
				CParam* pParam = waitmsg.ParamByName(L"ErrorCode");
				if (pParam)
				{
					pCtx->Log(LEVEL_WARNING, __FUNCTIONW__, L"Failed to play video: \"%s\", Error: \"%s\"", VideoFiles[iCurrentVideoFile].c_str(), pParam->AsString());
					break;
				}
				//CLEARDIGITS;
				CMessage get_digits_msg(L"GET_DIGITS");
				get_digits_msg[L"DigitMask"]  = sTermChar;
				auto timeOut  = pCtx->GetValue(L"timeout");
				auto waitTime = pCtx->GetValue(L"WaitTime");
				get_digits_msg[L"MaxSilence"] = Utils::toNum<int>(_bstr_t(timeOut.vValue.bstrVal).GetBSTR());
				get_digits_msg[L"MaxTime"]	  = Utils::toNum<int>(_bstr_t(waitTime.vValue.bstrVal).GetBSTR());
				//get_digits_msg[L"MaxDTMF"] = 1;

				// Do not include MaxSilence and MaxTime params into message. In this case, the time must be infinitive
				WaitingEvents waiting_events;//(L"get_digits.completed", L"play_video.completed");
				waiting_events.push_back(L"get_digits.completed");
				waiting_events.push_back(L"play_video.completed");
				retEvent = pCtx->WaitMultipleEvent(!iCurrentVideoFile?&get_digits_msg:NULL, waiting_events, L"error", waitmsg);
				if (retEvent == WE_STOPTHREAD)
				{
					CMessage stop_video_msg(L"STOP_VIDEO");
					pCtx->SendEvent(stop_video_msg, pCtx->GetParentScriptID());
					//::ExitThread(0);
					pCtx->DoExit();
					return false;
				}
				if (retEvent == WE_SUCCESS)
				{
					if (!VXMLString(waitmsg.GetName()).compare(L"get_digits.completed"))
					{
						if (CParam* pParam = waitmsg.ParamByName(L"DigitsBuffer"))
						{
							if (!pParam->AsWideStr().empty())
							{
								//pCtx->AddVar(L"digits",VXMLString(pParam->AsWideStr()).c_str(),false);
								pCtx->AddVar(L"utterance",VXMLString(pParam->AsWideStr()).c_str(),false);

								//test123
								//pCtx->ExecNormalStatement(L"tmp_utterance = \"" + VXMLString(pParam->AsWideStr()) + L"\"");

							}
						}
						CMessage stop_video_msg(L"STOP_VIDEO");
						stop_video_msg[L"DestinationAddress"] = pCtx->GetParentCallID();
						if (pCtx->WaitEvent(&stop_video_msg, L"stop_video.completed", L"error", waitmsg) != WE_SUCCESS)
						{
							//::ExitThread(0);
							pCtx->DoExit();
							return false;
						}
						pCtx->CancelEvent(L"play_video.completed", pCtx->GetParentCallID());
						break;// we've got digits over here

					}
					else if (!VXMLString(waitmsg.GetName()).compare(L"play_video.completed"))
					{
						if (++iCurrentVideoFile < VideoFiles.size())
						{
							//pCtx->pCtx->CancelEvent(L"get_digits.completed");
							continue;
						}
						//CMessage stop_chanel_msg(L"STOP_CHANNEL");
						retEvent = pCtx->WaitEvent(nullptr, L"get_digits.completed", L"error", waitmsg);
						if (retEvent == WE_SUCCESS)
						{
							if (CParam* pParam = waitmsg.ParamByName(L"DigitsBuffer"))
							{
								if (!pParam->AsWideStr().empty())
								{
									//pCtx->AddVar(L"digits",VXMLString(pParam->AsWideStr()).c_str(),false);
									pCtx->AddVar(L"utterance",VXMLString(pParam->AsWideStr()).c_str(),false);

									//test123
									//pCtx->ExecNormalStatement(L"tmp_utterance = \"" + VXMLString(pParam->AsWideStr()) + L"\"");

								}
							}
						}
						else //if(retEvent == WE_STOPTHREAD)
						{
							CMessage stop_video_msg(L"STOP_VIDEO");
							pCtx->SendEvent(stop_video_msg, pCtx->GetParentScriptID());
							//::ExitThread(0);
							pCtx->DoExit();
							return false;
						}


						CMessage stop_video_msg(L"STOP_VIDEO");
						stop_video_msg[L"DestinationAddress"] = pCtx->GetParentCallID();
						if (pCtx->WaitEvent(&stop_video_msg, L"stop_video.completed", L"error", waitmsg) != WE_SUCCESS)
						{
							//::ExitThread(0);
							pCtx->DoExit();
							return false;
						}
						pCtx->CancelEvent(L"get_digits.completed", pCtx->GetParentCallID());
						break; // we've got digits over here
					}
					else
					{
						break;
					}


				}
			}
		}
		CLEARDIGITS;
		return true;
	}

	bool CTag_menu::Execute(CExecContainer* pCtx)
	{
		//pCtx->PushScope(L"menu");
		CScopeSaver saver(pCtx,L"menu");
		CMessage msg(L"NextMenu");
		VXMLString menuID;
		msg[L"id"] = menuID = GetAttrVal(L"id");
		//pCtx->pSes->CorrectEvent(msg);

		VXMLString sComment; sComment.clear();
		if (GetAttrVal(L"comment",sComment))
		{
			pCtx->ProccedFormComment(menuID,sComment);
		}

		pCtx->ExecNormalStatement(menuID + L"= \"\"");

		bool bGetDigits = true;
		m_bVideoCall = false;
		for each (TagPtr pTag in m_Children)
		{
			if (pCtx->SuccessedEvent())
				break;
			// skip all events
			if (IsCatchEvent(pTag->GetName()))
				continue;

			if (pTag->GetName() == L"prompt" )
			{
				if (!MatchPrompt(pTag,pCtx))
					continue;
				CTag_prompt *pPromptTag = dynamic_cast<CTag_prompt*>(pTag.get());
				if (pPromptTag)
					pPromptTag->SetOwnerTag(this);
			}

			//if (pTag->GetName() == L"media")
			//	m_bVideoCall = true;

			if (pTag->GetName() == L"choice" && bGetDigits)
			{
				bGetDigits = false;
				if (!CheckDigits(pCtx))
					return false;
			}
			if (!ExecuteChild(pCtx, pTag))
				return false;
		}

		VXMLString dtmfValue;
		auto utterance = pCtx->GetValue(L"utterance");
		if (!utterance.IsEmpty())
		{
			dtmfValue = utterance.vValue.bstrVal;
		}

		if (!bGetDigits && pCtx->ExprEvalToStr(menuID).empty())
		{
			VXMLString exist_event = L"noinput";
			if (!dtmfValue.empty())
			{
				exist_event = L"nomatch";
			}
			CMessage msg(L"event");
			msg[L"event_name"] = exist_event.c_str();
			pCtx->DoEvent(msg);
		}

		if (!bGetDigits)
		{
			pCtx->ProccedBufferComment(menuID, dtmfValue);
		}

		return true;

		//pCtx->PopScope();
	}

	bool CTag_menu::CheckDigits(CExecContainer* pCtx)
	{
		//if (WasVideoCall())
		if (m_bVideoCall)
			return true;
		return __super::CheckDigits(pCtx);
	}

	CTag_noinput::CTag_noinput(
		const Factory_c::CollectorPtr& _parser, 
		const CTagFactory& _factory,
		EventScope& _scope, 
		EventScope& _prompts, 
		const VXMLString& _docUri): CParentTag(_parser, _factory, _scope, _prompts, _docUri)
	{ 
		try
		{
			VXMLString sName(L"noinput");
			VXMLString sCount, sCond;
			GetAttrVal(L"count", sCount);
			GetAttrVal(L"cond", sCond);

			CEvtScope pNew(sName, sCount.empty() ? 1 : Utils::toNum<int>(sCount), sCond, this, _docUri);
			_scope.push_back(pNew);
		}
		catch (...)
		{
			//EmitError(pCtx, CMessage(L"error.unrecoverable"), L"When execute \"noinput\" tag");
		}
	}

	bool CTag_noinput::Execute(VXML::CExecContainer *pCtx)
	{
		CScopeSaver saver(pCtx,L"noinput");
		return __super::Execute(pCtx); // - action
	}


	CTag_nomatch::CTag_nomatch(
		const Factory_c::CollectorPtr& _parser, 
		const CTagFactory& _factory,
		EventScope& _scope, 
		EventScope& _prompts, 
		const VXMLString& _docUri): CParentTag(_parser, _factory, _scope, _prompts, _docUri)
	{ 
		try
		{
			VXMLString sName(L"nomatch");
			VXMLString sCount, sCond;
			GetAttrVal(L"count", sCount);
			GetAttrVal(L"cond", sCond);

			CEvtScope pNew(sName, sCount.empty() ? 1 : Utils::toNum<int>(sCount), sCond, this, _docUri);
			_scope.push_back(pNew);
		}
		catch (...)
		{
			//EmitError(pCtx, CMessage(L"error.unrecoverable"), L"When execute \"nomatch\" tag");
		}
	}

	bool CTag_nomatch::Execute(VXML::CExecContainer *pCtx)
	{
		CScopeSaver saver(pCtx,L"nomatch");
		return __super::Execute(pCtx); // - action
	}

	bool CTag_object::Execute(CExecContainer* pCtx)
	{
		VXMLString sName,sClassID;
		if(GetAttrVal(L"name",sName) && GetAttrVal(L"classid",sClassID))
		{
			pCtx->ExecNormalStatement(sName + L"=" + sClassID);
		}

		return true;
	}


	CTag_prompt::CTag_prompt(
		const Factory_c::CollectorPtr& _parser, 
		const CTagFactory& _factory,
		EventScope& _scope, 
		EventScope& _prompts, 
		const VXMLString& _docUri)
		: CParentTag(_parser, _factory, _scope, _prompts, _docUri)
		, m_pOwner(NULL)
	{
		try
		{
			VXMLString sName(L"prompt");
			VXMLString sCount, sCond;
			GetAttrVal(L"count", sCount);
			GetAttrVal(L"cond", sCond);

			CEvtScope pNew(sName, sCount.empty() ? 1 : Utils::toNum<int>(sCount), sCond, this, _docUri);
			_prompts.push_back(pNew);
		}
		catch (...)
		{
			//EmitError(pCtx, CMessage(L"error.unrecoverable"), L"When execute \"prompt\" tag");
		}
	}
	bool CTag_prompt::Execute(CExecContainer* pCtx)
	{
		bool       bFetch = true;
		VXMLString sFetch;
		if (GetAttrVal(L"bargein",sFetch))
		{
			bFetch = pCtx->ExprEvalToBool(sFetch);
		}
		pCtx->AddVar(L"bargein",bFetch,false);

		pCtx->ExecNormalStatement(L"reprompt = \"0\"");
		//INCPROMPT;
		int count = pCtx->ExprEvalToInt(L"prompt_count")+1; 
		pCtx->ExecNormalStatement(L"prompt_count = \"" + Utils::toStr(count) + L"\"");

		for each (TagPtr pTag in m_Children)
		{
			if (pCtx->SuccessedEvent())
				break;
			if(!ExecuteChild(pCtx, pTag))
				return false;
			if (m_pOwner && pTag->GetName() == L"audio") // error: string too long
			{
				CTag_field* pCurField = dynamic_cast<CTag_field*>(m_pOwner); // error: no RTTI data!
				auto curDTMF = pCtx->GetValue(L"utterance");
				if (!curDTMF.IsEmpty() && pCurField)
				{
					VXMLString audioinput = _bstr_t(curDTMF.vValue.bstrVal);
					pCurField->SetAudioInput(audioinput);
				}

			}
			if (m_pOwner && pTag->GetName() == L"media")
			{
				if (CTag_field* pCurField = dynamic_cast<CTag_field*>(m_pOwner))
				{
					pCurField->WasVideoCall();
				}
				else if (CTag_menu* pCurMenu = dynamic_cast<CTag_menu*>(m_pOwner))
				{
					pCurMenu->WasVideoCall();
				}

			}
		}
		return true;
		//__super::Execute(pCtx);
	}

	bool CTag_property::Execute(CExecContainer* pCtx)
	{
		VXMLString sPropertyName,
				   sPropertyValue;
		if(GetAttrVal(L"name",sPropertyName) && GetAttrVal(L"value",sPropertyValue))
		{
			int pos = -1;
			while ((pos = sPropertyValue.find(L"\"",++pos))>=0)
			{
				sPropertyValue.replace(pos,1,L"");
			}


			pCtx->AddVar(sPropertyName.c_str(),sPropertyValue.c_str(),false);
		}

		return true;
	}


	bool CTag_record::GetRecord(CExecContainer* pCtx)
	{
		CMessage msg(L"REC_WAV");

		VXMLString sRecordInputName,
			sMaxTime,
			//sFinalSilence,
			sType;
		bool       //bBeep     = false, ignore it
			bDtmfterm = false;

		// get input file name than will hold the recoding
		if(GetAttrVal(L"name",sRecordInputName))
		{
			sRecordInputName = pCtx->ExprEvalToStr(sRecordInputName);
		}
		else if (GetAttrVal(L"expr",sRecordInputName))
		{
			sRecordInputName = pCtx->ExprEvalToStr(sRecordInputName);
		}
		msg[L"FileName"] = sRecordInputName.c_str();

		// get time params
		if(GetAttrVal(L"maxtime",sMaxTime))
		{
			msg[L"MaxTime"] = Utils::ConvertTimeFromCSS1(sMaxTime);
		}
		else
		{
			auto waitTime = pCtx->GetValue(L"WaitTime");
			msg[L"MaxTime"]	   = Utils::toNum<int>(_bstr_t(waitTime.vValue.bstrVal).GetBSTR());
		}

		auto timeOut  = pCtx->GetValue(L"timeout");
		msg[L"MaxSilence"] = Utils::toNum<int>(_bstr_t(timeOut.vValue.bstrVal).GetBSTR());

		VXMLString sTermChar = _bstr_t(pCtx->GetValue(L"termchar").vValue.bstrVal);
		msg[L"DigitMask"]  = sTermChar;

		CMessage waitmsg;
		VXMLString sId;
		msg[L"DestinationAddress"] = pCtx->GetParentCallID();
		eWaitEvents retEvent = pCtx->WaitEvent(&msg, L"rec_wav.completed", L"error", waitmsg);
		if (retEvent == WE_SUCCESS)
		{
			//m_RecWavCompletedMsg = waitmsg;
			// SUCCESSED
			CParam* pParam = waitmsg.ParamByName(L"TerminationReason");
			if (!pParam)
				return true;
			if (pParam->AsString() == L"TM_MAXSIL") // 1. max silence
			{
				// NOINPUT

			}
			else if (pParam->AsString() == L"TM_DIGIT") // 2. dtmf input
			{
				// we're got digits
				if (CParam* pParam = waitmsg.ParamByName(L"DigitsBuffer"))
				{
					VXMLString digits(pParam->AsString());
					pCtx->AddVar(L"utterance",VXMLString(pParam->AsString()).c_str(),false);

					//test123
					//pCtx->ExecNormalStatement(L"tmp_utterance = \"" + VXMLString(pParam->AsWideStr()) + L"\"");

					CLEARDIGITS;
				}
			}
			else if (pParam->AsString() == L"TM_MAXTIME") // 3. max time
			{
				pCtx->AddVar(L"inputmode",L"voice",false);
			}
			else if (pParam->AsString() == L"TM_USRSTOP") // 4. hand up
			{
				pCtx->AddVar(L"inputmode",L"voice",false);
			}
		}
		else //if (retEvent == WE_STOPTHREAD)
		{
			// stop tone playing
			CMessage waitmsg, stop_chanel_msg(L"STOP_CHANNEL");
			stop_chanel_msg[L"DestinationAddress"] = pCtx->GetParentCallID();
			//pCtx->pCtx->WaitEvent(&stop_chanel_msg, L"stop_chanel.completed", L"error", waitmsg);
			pCtx->SendEvent(stop_chanel_msg, pCtx->GetParentScriptID()); // why wait? why not send?
			//::ExitThread(0);
			pCtx->DoExit();
			return false;
		}
		return true;
	}

	bool  CTag_record::CheckDigits(CExecContainer* pCtx)
	{
		return true; // nothing to do, we already have got digits
	}

	bool CTag_record::Execute(CExecContainer* pCtx)
	{
		// Check conditional expression value if there is 'cond' attribute
		VXMLString sCond;
		if (GetAttrVal(L"cond", sCond))
		{
			if (!pCtx->ExprEvalToBool(sCond))
			{
				return true;
			}
		}
		
		VXMLString sRecordInputName;
		// get input file name than will hold the recoding
		if(GetAttrVal(L"name",sRecordInputName))
		{

		}
		else if (GetAttrVal(L"expr",sRecordInputName))
		{
			sRecordInputName = pCtx->ExprEvalToStr(sRecordInputName);
		}

		//pCtx->PushScope(sRecordInputName);
		CScopeSaver saver(pCtx,sRecordInputName);
		bool bGetRecord = false;
		for each (TagPtr pTag in m_Children)
		{
			if (pCtx->SuccessedEvent())
				break;
			// skip all events
			if (IsCatchEvent(pTag->GetName()))
				continue;

			if (pTag->GetName() == L"prompt" )
			{
				if (!MatchPrompt(pTag,pCtx))
					continue;
			}

			if (pTag->GetName() == L"filled")
			{
				if (!bGetRecord)
				{
					if (!GetRecord(pCtx))
						return false;
				}
				bGetRecord = true;
				CTag_filled *pFilledTag = dynamic_cast<CTag_filled*>(pTag.get());
				if (pFilledTag)
					pFilledTag->SetCurField(this);
			}


			if(!ExecuteChild(pCtx, pTag))
				return false;
		}
		return true;
	}

	bool CTag_reprompt::Execute(CExecContainer* pCtx)
	{
		pCtx->ExecNormalStatement(L"reprompt = \"1\"");
		return true;
	}

	bool CTag_say_as::Execute(VXML::CExecContainer *pCtx)
	{
		VXMLString sInterpretAs;
		if (GetAttrVal(L"interpret-as", sInterpretAs))
		{
		}
		else if (GetAttrVal(L"interpret-as-expr", sInterpretAs))
		{
			sInterpretAs = pCtx->ExprEvalToStr(sInterpretAs);
		}

		if (sInterpretAs.empty())
		{
			pCtx->Log(LEVEL_WARNING, __FUNCTIONW__, L"say_as:execute: \"interpret-as\" or \"interpret-as-expr\" value required");
			return true;
		}

		pCtx->AddVar(L"interpret_as",sInterpretAs.c_str(),false);

		VXMLString sFormat,sDetail;
		if (GetAttrVal(L"format",sFormat))
			pCtx->AddVar(L"format",sFormat.c_str(),false);
		if (GetAttrVal(L"detail",sDetail))
			pCtx->AddVar(L"detail",sDetail.c_str(),false);

		return __super::Execute(pCtx); // - action
	}
	bool CTag_script::Execute(CExecContainer* pCtx)
	{
		//pCtx->Log(__FUNCTIONW__, L"%s",GetText().c_str());
		VXMLString sScriptText = GetText();
		VXMLString sSrc, sFileSrc;
		if (sScriptText.empty() && GetAttrVal(L"src",sSrc) && !sSrc.empty())
		{

				//pCtx->pCtx->GetFileFolder(),pCtx->pCtx->GetCurDoc()
				VXMLString _root_folder = pCtx->GetFileFolder();
				VXMLString _curdoc      = pCtx->GetCurDoc();
				bool bRootFolder = false;
				if (sSrc[0] == '/')
				{
					sSrc.erase(0,1); // delete symbol "\"
					bRootFolder = true;
				}

				// for absolute URI
				//WIN32_FIND_DATA fileData;
				int pos = -1;
				//if (!Utils::GetFile(sSrc,&fileData))
				if ((_waccess(sSrc.c_str(),4))!=-1)
				{
					sFileSrc = sSrc;
				}
				// URI is relative 
				if (sFileSrc.empty())
				{
					WCHAR dir   [MAX_PATH]; ZeroMemory(dir   ,sizeof(dir   ));
					WCHAR ExtDir[MAX_PATH]; ZeroMemory(ExtDir,sizeof(ExtDir));
					if ((pos = sSrc.rfind('\\')) != -1)
					{
						sSrc._Copy_s(ExtDir,MAX_PATH,pos+1); // include "\"
						sSrc.erase(0,pos+1);
					}
					if (bRootFolder)
					{
						wcscpy_s(dir,_root_folder.c_str());
					}
					else if ((pos = _curdoc.rfind('\\')) != -1)
					{
						_curdoc._Copy_s(dir,MAX_PATH,pos+1); // include "\"
					}
					VXMLString mask = dir + VXMLString(ExtDir) + sSrc;
					if ((_waccess(mask.c_str(),4))!=-1)
					{
						sFileSrc = mask;
					}
					else
					{
						pCtx->Log(LEVEL_WARNING, __FUNCTIONW__, L"Cannot open file: \"%s\"", sSrc.c_str());
					}

				}
				if (!sFileSrc.empty())
				{
					std::wifstream _in(sFileSrc.c_str(),std::ios::binary);
					// read whole file
					DWORD   dwDataSize=0;                                    
					if( _in.seekg(0, std::ios::end) )
					{
						dwDataSize = _in.tellg();
					}
					std::wstring buff(dwDataSize,0);
					if( dwDataSize && _in.seekg(0, std::ios::beg) )
					{
						std::copy( std::istreambuf_iterator< wchar_t >(_in) ,
							std::istreambuf_iterator< wchar_t >() , 
							buff.begin() );
						m_sText = buff;
					}
					else
					{
						pCtx->Log(LEVEL_WARNING, __FUNCTIONW__, L"File: \"%s\" is empty", sSrc.c_str());
					}
					_in.close();
				}

		}

		pCtx->ExecScript(GetText());

		return true;
	}

	bool CTag_subdialog::CheckDigits(CExecContainer* pCtx)
	{
		return true;
	}

	bool CTag_subdialog::StartDialog(CExecContainer* pCtx)
	{
		// Check conditional expression value if there is 'cond' attribute
		VXMLString sCond;
		if (GetAttrVal(L"cond", sCond))
		{
			if (!pCtx->ExprEvalToBool(sCond))
			{
				return true;
			}
		}

		bool bStartSubDialog = true;
		VXMLString sSubDialogURI, sFunctionName;
		if(GetAttrVal(L"src",sSubDialogURI))
		{

		}
		else if (GetAttrVal(L"expr",sSubDialogURI))
		{
			sSubDialogURI = pCtx->ExprEvalToStr(sSubDialogURI);
		}
		else if (GetAttrVal(L"function", sFunctionName))
		{
			bStartSubDialog = false;
		}

		// ToDo: supporting file text !!!
		VXMLString sCurDoc       = pCtx->GetCurDoc();
		VXMLString file, 
			menu;

		if (bStartSubDialog)
		{
			TranslateURI(pCtx->GetFileFolder(), sCurDoc, sSubDialogURI, file, menu);
			if (file.empty())
				file = sCurDoc;
		}

		CMessage msg;// (L"ANY2TA_RUN_SCRIPT");

		if (bStartSubDialog)
		{
			msg.SetName(L"ANY2TA_RUN_SCRIPT");
		}
		else
		{
			msg.SetName(L"V2SC_RUN_FUNCTION");
		}

		msg[L"parenttype"   ] = L"dialog";

		if (!bStartSubDialog && !sFunctionName.empty())
		{
			msg[L"function"] = sFunctionName.c_str();
		}
		else
		{
			msg[L"FileName"] = file;
			msg[L"NextExecuteMenuID"] = menu;
		}

		VXMLString sNamelist;
		if (GetAttrVal(L"namelist", sNamelist))
		{
			msg[L"namelist"] = sNamelist.c_str();
			pCtx->AddNamelistToMsg(msg);
		}


		return pCtx->DialogStart(msg);
	}

	bool CTag_subdialog::Execute(CExecContainer* pCtx)
	{
		VXMLString subdialog_name; subdialog_name = GetAttrVal(L"name", subdialog_name)?subdialog_name:L"subdialog_name";
		//pCtx->PushScope(subdialog_name);
		CScopeSaver saver(pCtx,subdialog_name);
		pCtx->ExecNormalStatement(subdialog_name + L"= \"\"");
		{
			pCtx->Log(LEVEL_WARNING, __FUNCTIONW__, L"ExecNormalStatement failed: subdialog_name = \"\"");
		}
		for each (TagPtr pTag in m_Children)
		{
			if (pCtx->SuccessedEvent())
				break;
			// skip all events
			if (IsCatchEvent(pTag->GetName()))
				continue;

			if (pTag->GetName() == L"prompt" )
			{
				if (!MatchPrompt(pTag,pCtx))
					continue;
			}

			if (pTag->GetName() == L"filled")
			{
				if(!StartDialog(pCtx))
					return false;
				CTag_filled *pFilledTag = dynamic_cast<CTag_filled*>(pTag.get());
				if (pFilledTag)
					pFilledTag->SetCurField(this);
			}

			if (!ExecuteChild(pCtx, pTag))
				return false;
		}
		return true;
	}

	bool CTag_submit::Execute(CExecContainer* pCtx)
	{
		VXMLString sURL, sNext, sExpr, sNamelist, sMethod, sEnctype;

		// Get URL
		if (GetAttrVal(L"next",sNext))
		{
			sURL = sNext;
		}
		else if (GetAttrVal(L"expr",sExpr))
		{
			sURL = pCtx->ExprEvalToStr(sExpr);
		}
		if (sURL.empty())
		{
			EmitError(pCtx, CMessage(L"error.badfetch"), L"A fetch of a document has failed", L"Exactly one of \"next\" or \"expr\" must be specified");
			return true;
		}

		VXMLString file, menu, sNextMenu;

		TranslateURI(pCtx->GetFileFolder(),pCtx->GetCurDoc(),sURL,file,menu);
		sURL = file;
		sNextMenu = menu;


		CComPtr<IPassKeeper> pKeeper;
		if(SUCCEEDED(pKeeper.CoCreateInstance(L"VXMLPassStorage.PassKeeper")))
		{
			pCtx->Log(LEVEL_INFO, __FUNCTIONW__, L"Retreiving user / password for '%s'", sURL.c_str());
			BSTR user, password;

			if(pKeeper->GetUserPassword(_bstr_t(sURL.c_str()), &user, &password))
			{

				VXMLString strAddress(sURL);
				WCHAR sDirectURL[MAX_PATH] ; ZeroMemory(sDirectURL ,sizeof(sDirectURL ));
				int pos = -1;
				if ((pos = strAddress.find(L"http://")) == 0)
				{
					strAddress._Copy_s(sDirectURL,MAX_PATH,strAddress.length() - 7,7);
					strAddress = VXMLString(L"http://")+user+L":"+password+L"@"+sDirectURL;
				}
				else if ((pos = strAddress.find(L"https://")) == 0)
				{
					strAddress._Copy_s(sDirectURL,MAX_PATH,strAddress.length() - 8,8);
					strAddress = VXMLString(L"https://")+user+L":"+password+L"@"+sDirectURL;
				}

				sURL = strAddress;
				pCtx->Log(LEVEL_FINEST, __FUNCTIONW__, L"New address is: '%s'", sURL.c_str());
			}
		}

		//Get namelist of vars, which we want to send
		GetAttrVal(L"namelist",sNamelist);

		VXMLString sListValue;
		LPCWSTR p = sNamelist.c_str(), q;
		VXMLString sParamName, sParamVal;

		while (*p)
		{
			q = wcschr(p, L' ');
			if (!q)
			{
				sParamVal = trim_wstring(p);
			}
			else
			{
				sParamVal = trim_wstring(VXMLString(p, q - p));
			}
			if (!sParamVal.empty())
			{
				if (!sListValue.empty())
					sListValue+=L" ";
				//VXMLString listvalue = pCtx->ExprEvalToStr(sParamVal);
				//NameListValue[sParamVal] = listvalue;
				sListValue+=pCtx->ExprEvalToStr(sParamVal);//listvalue;
				
			}
			if (!q)
			{
				break;
			}
			p = q + 1;
		}

		//if (!NameListValue.size())
		if (sListValue.empty())
			return true; // nothing to send

		// Get HTTP method which we want to use
		GetAttrVal(L"method",sMethod);
		// Get encryption type
		GetAttrVal(L"enctype", sEnctype);

		//We've got all we want. Now, try to send it
		try
		{
			VXMLString sAnswer;
			CSyncClient client(sMethod);
			int stat = client.Send(sURL,sListValue,sAnswer);
			if (!stat)
			{ // success
				CMessage msg(L"NextMenu");
				msg[L"id"        ] = sNextMenu;
				msg[L"field_name"] = L"";
				msg[L"url"       ] = sURL;
				msg[L"FileText" ] = sAnswer;
				msg.SetName(L"LoadNewDocument");
				pCtx->DoNext(msg);
			}
			else
			{ // failed
				VXMLString reason;
				if (stat == 1)
				{
					reason = L"Couldn't receive submit document, internal error: Invalid response";
				}
				else
				{
					reason = L"server return value: ";
					reason+= Utils::toStr(stat);
				}

				EmitError(pCtx, CMessage(L"error.badfetch"), L"A fetch of a document has failed", reason);
			}		
		}
		catch (std::exception& e)
		{
			VXMLString reason;
			reason = L"Couldn't receive submit document, internal error: ";
			reason+= _bstr_t(e.what());
			EmitError(pCtx, CMessage(L"error.badfetch"), L"A fetch of a document has failed", reason);
		}
		return true;
	}

	bool CTag_transfer::CheckDigits(CExecContainer* pCtx)
	{
		return true;
	}

	class COnline
	{
	public:
		COnline (CExecContainer* pCtx): mCtx(pCtx)
		{
			mCtx->StartKeepAliveTimers();
		}
		~COnline()
		{
			mCtx->StopKeepAliveTimers();
		}
	private:
		CExecContainer* mCtx;
	};

	bool CTag_transfer::GetTransfer(CExecContainer* pCtx)
	{
		CMessage msg(L"dialog.transfer");
		VXMLString sDest,
				   sExpr,
				   sURI,
				   sAai,
				   /*sIPProtocol,
				   sStartType,*/
				   sSIPContact,
				   sSIPUserToUser,
				   sTrunk,
				   sTransferAudio;

		bool bBridge         = false;
		//UINT uMaxTime        = 0,
		//	 uConnectTimeout = 0;

		if (GetAttrVal(L"dest",sExpr))
		{
			sURI = sExpr;
		}
		else if (GetAttrVal(L"destexpr",sExpr))
		{
			sURI = pCtx->ExprEvalToStr(sExpr);
		}

		if (sURI.empty())
			return true;

		VXMLString sBridge;
		if (GetAttrVal(L"bridge",sBridge))
		{
			bBridge = pCtx->ExprEvalToBool(sBridge);
		}

		VXMLString sMaxTime;
		if(GetAttrVal(L"maxtime",sMaxTime))
		{
		}
		else
		{
			auto waitTime = pCtx->GetValue(L"WaitTime");
			sMaxTime = _bstr_t(waitTime.vValue.bstrVal).GetBSTR();
		}

		VXMLString sConnectTimeout;
		if(GetAttrVal(L"connecttimeout",sConnectTimeout))
		{
		}

		if (GetAttrVal(L"aai",sAai))
		{
		}
		else if (GetAttrVal(L"aaiexpr",sAai))
		{
			sAai = pCtx->ExprEvalToStr(sAai);
		}

		msg[L"uri"]            = sURI;
		msg[L"bridge"]         = bBridge;
		msg[L"maxtime"]        = sMaxTime;
		msg[L"connecttimeout"] = sConnectTimeout;
		msg[L"aai"]            = sAai;
		msg[L"targettype"]     = L"ccxml";
		msg[L"type"]		   = bBridge?L"bridge":L"blind";

		if(GetAttrVal(L"SIPContact",sSIPContact))
		{
			msg[L"SIPContact"] = pCtx->ExprEvalToStr(sSIPContact);
		}
		if(GetAttrVal(L"SIPUserToUser",sSIPUserToUser))
		{
			msg[L"SIPUserToUser"] = pCtx->ExprEvalToStr(sSIPUserToUser);
		}

		if(GetAttrVal(L"trunk",sTrunk))
		{
			msg[L"trunk"] = sTrunk;
		}

		sTransferAudio.clear();
		if (GetAttrVal(L"transferaudio",sExpr))
		{
			sTransferAudio = sExpr;
		}
		else if (GetAttrVal(L"transferaudioexpr",sExpr))
		{
			sTransferAudio = pCtx->ExprEvalToStr(sExpr);
		}

		VXMLString sNamelist;
		if (GetAttrVal(L"namelist", sNamelist))
		{
			msg[L"namelist"] = sNamelist.c_str();
			pCtx->AddNamelistToMsg(msg);
		}

		// transferaudio is the URI of audio source to play while the transfer attempt is in progress (before far-end answer).
		//   repeat it 99 times to make audio continued
		bool bMakePlayWav = false;
		if (!sTransferAudio.empty())
		{
 			bMakePlayWav = true;
		}

		CMessage waitmsg;
		WaitingEvents waiting_events;
		waiting_events.push_back(L"dialog.transfer.complete");
		waiting_events.push_back(L"connection.disconnect.hangup");
		waiting_events.push_back(L"connection.disconnect.transfer");
		if (bMakePlayWav)
		{
			waiting_events.push_back(L"dialog.stop_channel");
			waiting_events.push_back(L"wav.completed");
			pCtx->SendEvent(CreatePlayWavMessage(pCtx,sTransferAudio), pCtx->GetParentCallID()/*pCtx->pCtx->GetParentScriptID()*/);
		}
		else
		{
			pCtx->ChangeCallCtrl(pCtx->GetScriptID(), pCtx->GetParentScriptID());
		}

		eWaitEvents retEvent = WE_SUCCESS;

		if (true)
		{
			//auto holdFunction = [&]() 
			//{
			//	pCtx->SetTransferActive(false);
			//};

			//pCtx->SetTransferActive(true);
			//Holder<decltype(holdFunction)> holder(holdFunction);

			COnline keepAlive(pCtx);
			pCtx->SendEvent(msg, pCtx->GetParentScriptID());
			while (true)
			{
				retEvent = pCtx->WaitMultipleEvent(nullptr, waiting_events, L"error", waitmsg);

				if (retEvent == WE_STOPTHREAD)
				{
					//pCtx->pSes->StopChannel();
					//STOPCHANNEL;
					//::ExitThread(0);
					break;
				}
				if (retEvent == WE_SUCCESS)
				{
					if (!VXMLString(waitmsg.GetName()).compare(L"wav.completed") && bMakePlayWav)
					{
						CLEARDIGITS;
						pCtx->SendEvent(CreatePlayWavMessage(pCtx,sTransferAudio), pCtx->GetParentCallID()/*pCtx->pCtx->GetParentScriptID()*/);
						continue;
					}

					if (bMakePlayWav)
					{
						CMessage msg_stopchannel(L"STOP_CHANNEL"); 
						//CMessage wait_stopchannel; 
						//msg_stopchannel[L"DestinationAddress"] = pCtx->pCtx->GetParentCallID(); \
						//retEvent = pCtx->pCtx->WaitEvent(&msg_stopchannel, L"stop_channel.completed", L"error", wait_stopchannel);
						WaitingEvents stop_events;
						stop_events.push_back(L"stop_channel.completed");
						stop_events.push_back(L"wav.completed");
						//EventMessages waitingMessages;
						CMessage waitmsg2;
						retEvent = pCtx->WaitMultipleEvent(&msg_stopchannel, stop_events, L"error", waitmsg2);
						if (retEvent != WE_SUCCESS)
							break;
					}

					if (!VXMLString(waitmsg.GetName()).compare(L"dialog.stop_channel"))
					{
						bMakePlayWav = false;
						waiting_events.remove(L"wav.completed");
						waiting_events.remove(L"dialog.stop_channel");

						pCtx->ChangeCallCtrl(pCtx->GetScriptID(), pCtx->GetParentScriptID());

						continue;
					}
					if (!VXMLString(waitmsg.GetName()).compare(L"dialog.transfer.complete"))
					{
						pCtx->ChangeCallCtrl(pCtx->GetParentScriptID(), pCtx->GetScriptID());

						// SUCCESSED
						if (CParam* pParam = waitmsg.ParamByName(L"DigitsBuffer"))
						{
							if (!pParam->AsWideStr().empty())
							{
								pCtx->AddVar(L"utterance",VXMLString(pParam->AsWideStr()).c_str(),false);

								//test123
								//pCtx->ExecNormalStatement(L"tmp_utterance = \"" + VXMLString(pParam->AsWideStr()) + L"\"");

							}
						}
						CLEARDIGITS;
					}
					// An attempt has been made to transfer the caller to another line and will not return.
					if (!VXMLString(waitmsg.GetName()).compare(L"connection.disconnect.transfer"))
					{
						pCtx->Log(LEVEL_INFO, __FUNCTIONW__, L"Connection: '%llu' has redirected", waitmsg.ParamByName(L"CallbackID")->AsInt64());
						pCtx->DoExit(L"");
					}
					// The caller hung up.
					if (!VXMLString(waitmsg.GetName()).compare(L"connection.disconnect.hangup")) 
					{
						// ToDo:
					}
				}
				break; //repeat only if we've got dialog.stop_channel
			}  // if (true)
		}
		if (retEvent == WE_STOPTHREAD)
		{
			pCtx->Log(LEVEL_FINEST, __FUNCTIONW__, L"CTag_transfer::GetTransfer DoExit()");
			//::ExitThread(0);
			pCtx->DoExit();
			return false;
		}
		return true;
	}

	bool CTag_transfer::Execute(CExecContainer* pCtx)
	{
		VXMLString sCond;
		if (GetAttrVal(L"cond", sCond))
		{
			if (!pCtx->ExprEvalToBool(sCond))
			{
				return true;
			}
		}

		VXMLString sTransferName = GetAttrVal(L"name");
		bool bTransfer = false;

		if (true)
		{
			//pCtx->SetTransferActive(true);

			//auto holdFunction = [&]()
			//{
			//	pCtx->SetTransferActive(false);
			//};

			//Holder<decltype(holdFunction)> holder(holdFunction);

			CScopeSaver saver(pCtx, sTransferName);
			for each (TagPtr pTag in m_Children)
			{
				if (pCtx->SuccessedEvent())
					break;
				// skip all events
				if (IsCatchEvent(pTag->GetName()))
					continue;

				if (pTag->GetName() == L"prompt")
				{
					if (!MatchPrompt(pTag, pCtx))
						continue;
				}

				if (pTag->GetName() == L"filled")
				{
					//if (pCtx->IsTransferActive())
					if (!bTransfer)
					{
						if (!GetTransfer(pCtx))
							return false;
					}
					bTransfer = true;
					//pCtx->SetTransferActive(false);

					CTag_filled *pFilledTag = dynamic_cast<CTag_filled*>(pTag.get());
					if (pFilledTag)
						pFilledTag->SetCurField(this);
				}


				if (!ExecuteChild(pCtx, pTag))
					return false;
			}
		}

		//pCtx->PopScope();
		return true;
	}

	bool CTag_throw::Execute(CExecContainer* pCtx)
	{
		VXMLString sEvent;
		if (GetAttrVal(L"event",sEvent))
		{
			CMessage msg(L"event");
			msg[L"event_name"] = sEvent;
			pCtx->DoEvent(msg);

		}
		return true;
	}

	bool CTag_vxml::Execute(VXML::CExecContainer *pCtx)
	{
		//g_throwEvent = false;
		pCtx->Log(LEVEL_INFO, __FUNCTIONW__, L"Executing first VXML tag");
		pCtx->SetVXML(this);
		VXMLString sCurrMenu = pCtx->GetCurMenu();

		// prepare
		CMessage NextMsg(L"NextMenu");
		NextMsg[L"id"] = sCurrMenu;
		//pCtx->pSes->DeclareNextEvent(msg);

		//bool bHaveToCleanDigits = true;

		if (m_Children.empty())
		{
			pCtx->Log(LEVEL_INFO, __FUNCTIONW__, L"Error: There are no childs in first VXML tag!!!");
			return false;
		}

		for each (TagPtr pTag in m_Children)
		{
			// skip all events
			if (IsCatchEvent(pTag->GetName()))
				continue;

			//if (bHaveToCleanDigits && ( (pTag->GetName() == L"menu") || (pTag->GetName() == L"form")))
			//{
			//	bHaveToCleanDigits = false;
			//	CLEARDIGITS;
			//}

			if (pTag->GetName() == L"menu")
			{	
				if (!sCurrMenu.empty() && sCurrMenu!=pTag->GetAttrVal(L"id"))
					continue;
				CMessage msg((pTag->GetAttrVal(L"id")).c_str());
				pCtx->SetCurMenu((pTag->GetAttrVal(L"id")).c_str());
				if (!ExecuteChild(pCtx, pTag))
					return false;
				break;
			}
			if (pTag->GetName() == L"form")
			{
				if (!sCurrMenu.empty() && sCurrMenu!=pTag->GetAttrVal(L"id"))
					continue;
				if(!ExecuteChild(pCtx, pTag))
					return false;
				break;
			}
			// another tags
			if(!ExecuteChild(pCtx, pTag))
				return false;
		}

		pCtx->AcceptEvent(NextMsg, true);
		return true;
	}

	bool CTag_vxml::SetMenu(VXML::CExecContainer *pCtx, EventObject pEvt)
	{
		//g_throwEvent = false;

		//CLockEvent lock(pEvt);
		pEvt->Busy = true;
		auto id = pEvt->GetSafeValue(L"id");
		if (id.IsEmpty())
			return false;

		if (id.vValue.vt != VT_BSTR)
			return false;

		VXMLString sID = _bstr_t(id.vValue.bstrVal);
		pCtx->Log(LEVEL_INFO, __FUNCTIONW__, L"vxml:set next menu: \"%s\"", sID.c_str());
		// preparations
		CMessage NextMsg(L"NextMenu");
		NextMsg[L"id"] = sID.c_str();
		//pCtx->pSes->DeclareNextEvent(msg);

		// Set menu
		bool bExecMenu = false;
		for each (TagPtr pTag in m_Children)
		{
			if (pTag->GetName() != L"menu" && 
				pTag->GetName() != L"form"   )
			{
				continue;
			}

			VXMLString menuID; 
			pTag->GetAttrVal(L"id", menuID);
			if (!sID.empty() && menuID != sID)
			{
				continue;
			}
			VAR *pNextField = NULL;

			auto nextField = pEvt->GetSafeValue(L"field_name");

			if (pTag->GetName()== L"form" && !nextField.IsEmpty())
			{
				VXMLString field  = _bstr_t(nextField.vValue.bstrVal);
				CTag_form* pForm = dynamic_cast<CTag_form*>(pTag.get());
				if (pForm)
					pForm->SetCurFieldName(field);
			}
			bExecMenu = true;
			if(!ExecuteChild(pCtx, pTag))
				return false;
			break;
		}

		// cannot find menu -> exit
		if (!bExecMenu)
		{
			pCtx->Log(LEVEL_WARNING, __FUNCTIONW__, L"[%s] Cannot find menu: \"%s\"", pCtx->GetCurDoc().c_str(), sID.c_str());
			pCtx->LogExtra(LEVEL_WARNING, __FUNCTIONW__, L"[%s] Cannot find menu: \"%s\"", pCtx->GetCurDoc().c_str(), sID.c_str());
			pEvt->Invalidate();
			return false;
		}
			
		pCtx->AcceptEvent(NextMsg);

		return true;
	}

	bool CTag_vxml::SetEvent(CExecContainer* pCtx, EventObject pEvt)
	{
		//g_throwEvent = false;

		pEvt->Busy = true;

		VXMLString sID = pCtx->GetCurMenu();
		CMessage msg(L"NextMenu");
		msg[L"id"] = sID; // return to current menu
		//pCtx->pSes->DeclareNextEvent(msg);
		pCtx->Log(LEVEL_INFO, __FUNCTIONW__, L"vxml:execute: \"%s\"; curMenu: \"%s\"", pEvt->GetName().c_str(), sID.c_str());

		VXMLString sEventName;
		auto name = pEvt->GetSafeValue(L"event_name");
		if (!name.IsEmpty())
		{
			sEventName = name.vValue.bstrVal;
		}
		else
		{
			name = pEvt->GetSafeValue(L"tagname");
			if (!name.IsEmpty())
			{
				sEventName = name.vValue.bstrVal;
			}
			else
			{
				EmitError(pCtx, CMessage(L"error.badfetch"), L"A fetch of a document has failed", L"Processing event must have \"event_name\"");
				return false;
			}
		}

		CExecutableTag * pExecTag = NULL;
		for each (TagPtr pTag in m_Children)
		{
			if (pTag->GetName() != L"menu"    && 
				pTag->GetName() != L"form"    && 
				pTag->GetName() != L"transfer"   )
			{
				continue;
			}

			pExecTag = dynamic_cast<CExecutableTag*>(pTag.get()); //the last one

			VXMLString menuID = pTag->GetAttrVal(L"id");
			if (!sID.empty() && menuID != sID) // looking for current menu or form or transfer
			{
				continue;
			}

			if (pTag->GetName()== L"form") // for form
			{
				CTag_form * pFormTag = dynamic_cast<CTag_form*>(pTag.get());
				VXMLString sCurFieldName = pFormTag->GetCurFieldName();
				TagPtr pCurField = pFormTag->GetField(sCurFieldName);
				CExecutableTag * pTag = dynamic_cast<CExecutableTag*>(pCurField.get());
				if(pTag)
					pExecTag = pTag;
			}

			break;
		}

		if (pExecTag)
		{
			if (!pExecTag->ExecEvents(pCtx, sEventName)) 
			{
				if (pEvt->MatchMask(L"error.*")) // unhandled event case
				{
					//pCtx->DoExit(L"");
					VXMLString sError = pEvt->Name;

					auto reason = pEvt->GetSafeValue(L"reason");
					if (!reason.IsEmpty())
						sError += format_wstring(L", reason: %s", VXMLString(reason.vValue.bstrVal).c_str());
					pCtx->DoDialogError(sError);
				}
			}
		}

		pCtx->AcceptEvent(msg);

		return true;

	}

	bool CTag_vxml::SetExternal(CExecContainer* pCtx, EventObject pEvt)
	{
		//g_throwEvent = false;

		pEvt->Busy = true;
		VXMLString sID = pCtx->GetCurMenu();
		//CMessage msg(L"NextMenu");
		//msg[L"id"] = sID; // return to current menu
		////pCtx->pSes->DeclareNextEvent(msg);
		//pCtx->Log(__FUNCTIONW__, L"vxml:execute: \"%s\"; curMenu: \"%s\"",pEvt->GetName().c_str(), sID.c_str());

		VXMLString sEventName;

		auto name = pEvt->GetSafeValue(L"event_name");

		if (!name.IsEmpty())
		{
			sEventName = name.vValue.bstrVal;
		}
		else
		{
			name = pEvt->GetSafeValue(L"tagname");
			if (!name.IsEmpty())
			{
				sEventName = name.vValue.bstrVal;
			}
			else
			{
				EmitError(pCtx, CMessage(L"error.badfetch"), L"A fetch of a document has failed", L"Processing event must have \"event_name\"");
				return false;
			}
		}

		for each (TagPtr pTag in m_Children)
		{
			if (pTag->GetName() != L"menu" && 
				pTag->GetName() != L"form"   )
			{
				continue;
			}

			VXMLString menuID = pTag->GetAttrVal(L"id");
			if (!sID.empty() && menuID != sID)
			{
				continue;
			}

			CExecutableTag * pExecTag = NULL;
			if (pTag->GetName()== L"form") // for form
			{
				CTag_form * pFormTag = dynamic_cast<CTag_form*>(pTag.get());
				VXMLString sCurFieldName = pFormTag->GetCurFieldName();
				TagPtr pCurField = pFormTag->GetField(sCurFieldName);
				pExecTag = dynamic_cast<CExecutableTag*>(pCurField.get());

			}
			else // for menu
			{
				pExecTag = dynamic_cast<CExecutableTag*>(pTag.get());
			}

			if (pExecTag)
			{
				pExecTag->ExecEvents(pCtx, sEventName);
			}

			break;
		}

		//pCtx->pCtx->AcceptEvent(msg);

		return true;

	}

	bool CTag_value::Execute(CExecContainer* pCtx)
	{
		auto interpretAs = pCtx->GetValue(L"interpret_as");
		auto format      = pCtx->GetValue(L"format");
		auto detail      = pCtx->GetValue(L"detail");

		if (interpretAs.IsEmpty())
		{
			pCtx->Log(LEVEL_WARNING,__FUNCTIONW__, L"value:execute: \"interpret_as\" value required");
			return true;
		}

		VXMLString sValue, sExpr;

		// Get URL
		if (GetAttrVal(L"expr",sExpr))
		{
			sValue = pCtx->ExprEvalToStr(sExpr);
		}

		if (sValue.empty())
		{
			pCtx->Log(LEVEL_WARNING, __FUNCTIONW__, L"value:execute: \"expr\" param required");
			return true;
		}

		VXMLString sFiles;
		CAtlStringW sInterpretAs = _bstr_t(interpretAs.vValue.bstrVal);
		if (0 == sInterpretAs.CollateNoCase(L"currency"))
		{
			VXMLString sFormat; sFormat.clear();
			if (!format.IsEmpty())
			{
				sFormat = _bstr_t(format.vValue.bstrVal);
			}
			sFiles = pCtx->GetSpeakMoneyFiles(sValue,sFormat);
		}
		else if (0 == sInterpretAs.CollateNoCase(L"date"))
		{
			DWORD dwDate  = 0,
				  dwMonth = 0,
				  dwYear  = 0;
			if (swscanf(sValue.c_str(), L"%u.%u.%u", &dwDate, &dwMonth, &dwYear) != 3)
			{
				EmitError(pCtx, CMessage(L"error.semantic"), L"A run-time error was found in the VoiceXML document",
					format_wstring(L"CTag_value::Execute: unexpected date format \"%s\", use \"DD:MM:YYYY\" only", sValue.c_str()));
				return true;
			}
			sFiles = pCtx->GetSpeakDateFiles(dwDate, dwMonth, dwYear);

		}
		else if (0 == sInterpretAs.CollateNoCase(L"time"))
		{
			DWORD dwHours   = 0,
				  dwMinutes = 0,
				  dwSeconds = 0;
			if (swscanf(sValue.c_str(), L"%u:%u:%u", &dwHours, &dwMinutes, &dwSeconds) != 3)
			{
				EmitError(pCtx, CMessage(L"error.semantic"), L"A run-time error was found in the VoiceXML document", 
					format_wstring(L"CTag_value::Execute: unexpected time format \"%s\", use \"HH:MM:SS\" only", sValue.c_str()));
				return true;

			}
			sFiles = pCtx->GetSpeakTimeFiles(dwHours,dwMinutes,dwSeconds);

		}
		else if (0 == sInterpretAs.CollateNoCase(L"numbers"))
		{
			int sex = ssMale;
			if (!detail.IsEmpty() && detail.vValue.vt & VT_INT)
			{
				sex = Utils::toNum<int>(VXMLString(_bstr_t(detail.vValue.bstrVal).GetBSTR()));
			}
			sFiles = pCtx->GetSpeakNumberFiles(sValue,sex);
		}
		else if (0 == sInterpretAs.CollateNoCase(L"traffic"))
		{
			sFiles = pCtx->GetSpeakTrafficFiles(sValue);
		}
		else if (0 == sInterpretAs.CollateNoCase(L"minutes"))
		{
			sFiles = pCtx->GetSpeakMinutesFiles(sValue);
		}
		else if (0 == sInterpretAs.CollateNoCase(L"sms"))
		{
			sFiles = pCtx->GetSpeakSMSFiles(sValue);
		}
		else
		{
			VXMLString sErr = L"The \"interpret-as\" attribute of \"say-as\" element allows \"currency\", \"date\", "
				                 L"\"time\" or \"numbers\" value.";
			EmitError(pCtx, CMessage(L"error.semantic"), L"A run-time error was found in the VoiceXML document", sErr);
		}

		if (sFiles.empty())
		{
			pCtx->Log(LEVEL_WARNING, __FUNCTIONW__, L"value:execute: voice filelist is empty");
			return true;
		}
		
		return PlayAudio(sFiles,pCtx);
	}

	bool CTag_var::Execute(CExecContainer* pCtx)
	{
		CComVariant vVal;
		VXMLString sVal;
		VXMLString sName;

		if (GetAttrVal(L"expr", sVal) && GetAttrVal(L"name", sName))
		{
			//vVal = pCtx->ExprEval(sVal);
			pCtx->Log(LEVEL_FINEST, __FUNCTIONW__, L"Exec statement: \"%s = %s\"", sName.c_str(), sVal.c_str());
			pCtx->ExecNormalStatement(sName + L"=" + sVal);
		}

		return true;
	}
	// CTagFactory implementation

	TagPtr CTagFactory::CreateTag(
		const VXMLString& sName, 
		const Factory_c::CollectorPtr& _parser, 
		const CTagFactory& _factory,
		EventScope& _scope, 
		EventScope& _prompts, 
		const VXMLString& _docUri) const
	{
		auto it = m_TagsMap.find(sName);
		if (it == m_TagsMap.cend())
		{
			throw std::runtime_error(std::string("Unknown tag type: ") + wtos(sName));
		}
		return (it->second)(_parser, _factory, _scope, _prompts, _docUri);
	}

	CTagFactory::CTagFactory()
	{
		struct TagFuncs
		{
#define TAG_FUNC(NAME) \
	static TagPtr tag_##NAME(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory, EventScope& _scope, EventScope& _prompts, const VXMLString& _docUri) { return std::make_shared<CTag_##NAME>(_parser, _factory, _scope, _prompts, _docUri); }

			TAG_FUNC(assign           )
				TAG_FUNC(audio            )
				TAG_FUNC(block            )
				TAG_FUNC(catch			  )
				TAG_FUNC(choice			  )
				TAG_FUNC(goto			  )
				TAG_FUNC(else             )
				TAG_FUNC(elseif           )
				TAG_FUNC(error            )
				TAG_FUNC(exit             )
				TAG_FUNC(field            )
				TAG_FUNC(filled           )
				TAG_FUNC(form             )
				TAG_FUNC(grammar          )
				TAG_FUNC(if               )
				TAG_FUNC(log              )
				TAG_FUNC(media            )
				TAG_FUNC(menu             )
				TAG_FUNC(noinput          )
				TAG_FUNC(nomatch          )
				TAG_FUNC(object           )
				TAG_FUNC(prompt           )
				TAG_FUNC(property		  )
				TAG_FUNC(record			  )
				TAG_FUNC(reprompt         )
				TAG_FUNC(return			  )
				TAG_FUNC(say_as			  )
				TAG_FUNC(script           )
				TAG_FUNC(subdialog		  )
				TAG_FUNC(submit   		  )
				TAG_FUNC(transfer         )
				TAG_FUNC(throw   		  )
				TAG_FUNC(value            )
				TAG_FUNC(var              )
				TAG_FUNC(vxml             )
#undef TAG_FUNC

		};

#define ADD_TAG(NAME) \
	m_TagsMap[L#NAME]			= &TagFuncs::tag_##NAME;

		ADD_TAG(assign           )
			ADD_TAG(audio            )
			ADD_TAG(block            )
			ADD_TAG(catch            )
			ADD_TAG(choice           )
			ADD_TAG(goto			 )
			ADD_TAG(else             )
			ADD_TAG(elseif           )
			ADD_TAG(error            )
			ADD_TAG(exit             )
			ADD_TAG(field            )
			ADD_TAG(filled           )
			ADD_TAG(form             )
			ADD_TAG(grammar          )
			ADD_TAG(if               )
			ADD_TAG(log              )
			ADD_TAG(media            )
			ADD_TAG(menu             )
			ADD_TAG(noinput          )
			ADD_TAG(nomatch          )
			ADD_TAG(object           )
			ADD_TAG(prompt           )
			ADD_TAG(property		 )
			ADD_TAG(record			 )
			ADD_TAG(reprompt         )
			ADD_TAG(return			 )
			//ADD_TAG(say_as			 )
			ADD_TAG(script           )
			ADD_TAG(subdialog		 )
			ADD_TAG(submit           )
			ADD_TAG(transfer         )
			ADD_TAG(throw            )
			ADD_TAG(value            )
			ADD_TAG(var              )
			ADD_TAG(vxml             )
#undef ADD_TAG

			m_TagsMap[L"say-as"] = &TagFuncs::tag_say_as;

	}
}