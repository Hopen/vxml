
#include "..\StdAfx.h"
#include "VXMLDocument.h"
#include "sv_strutils.h"
#include "sv_utils.h"

//extern HINSTANCE g_hInst;

namespace VXML
{
	// CDocument implementation
	CDocument::CDocument(const Factory_c::CollectorPtr& _parser, const CTagFactory _factory, EventScope& _scope, EventScope& _propmts, const VXMLString& _docUri)
		: m_pRootTag(std::make_shared<CTag_vxml>(_parser, _factory, _scope, _propmts, _docUri))
		, m_DocUri(_docUri)
	{
	}

	CDocument::~CDocument()
	{
	}
	bool CDocument::Execute(CExecContainer* pCtx)
	{
		return m_pRootTag->Execute(pCtx);
	}
}