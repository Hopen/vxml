/************************************************************************/
/* Name     : VXMLCom\VXML\VXMLEvent.cpp                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 2 Dec 2009                                                */
/************************************************************************/
#include "..\StdAfx.h"
#include "VXMLEvent.h"

// For PathMatchSpecW
#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

namespace VXML
{
	CEvent::CEvent(const CMessage& msg)
		: CNameTable(msg.GetName()), m_sName(msg.Name), m_busy(false)
	{
		//m_typeNameTable = L"CEvent";
		// Convert message to event
		this->Add(L"name", CComVariant(msg.Name), true);
		ParamsList parlist = msg.Params();
		ParamsList::const_iterator it = parlist.begin();
		for (; it != parlist.end(); ++it)
		{
			const CParam& p = *it;
			this->Add(p.Name, p.Value, true);
		}
	}

	CMessage CEvent::ToMsg() const
	{
		// Convert event to message
		CMessage msg(__super::ToMsg());
		msg.Name = m_sName.c_str();
		return msg;
	}

	bool CEvent::MatchMask(const VXMLString& sMask)
	{
		// �� ������ ��� ���� �����-�� ���� ��������� ����� ������.
		// ��� ����� ��������, �� ������� ���� �� �������� ��������
		return ::PathMatchSpecW(m_sName.c_str(), sMask.c_str()) != 0;
	}

	const VXMLString& CEvent::GetName() const
	{
		return m_sName;
	}

	void CEvent::Invalidate()
	{
		m_sName = L"Invalid";
	}
}
