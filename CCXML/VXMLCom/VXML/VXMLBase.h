/************************************************************************/
/* Name     : VXMLCom\VXMLCom.h                                         */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 30 Nov 2009                                               */
/************************************************************************/

#pragma once

#include <string>
#include <comdef.h>
#include <memory>
//#include <boost/asio.hpp>
#include <list>

#include "..\Common\Utils.h"
#include "..\ComBridgeObject.h"
//#include "..\Common\KeepAlive.h"

#include "EvtModel.h"
#include "..\Engine\Engine.h"

//#import "..\TelLib\TelLib.dll" no_namespace
#import "..\..\..\TelLib\TelLib.dll" no_namespace
#include "..\Common\sid.h"

class IEngineCallback;
class CVXMLComInterface;
class CEngineLog;
//class IVOX;
struct Iq931Info;
#ifdef _EMULATE_TELSERVER
class CTelServerEmulator;
#endif

namespace VXML
{

	namespace CLIENT_ADDRESS{
		const __int64 ANY_CLIENT = ((__int64)0x00000002 << 32);
	}

	typedef std::wstring VXMLString;
	typedef /*unsigned*/ __int64 ObjectId;

	class CSession;
	class CDocument;
	class CApplication;
	class CScopedNameTable;
	class CScriptEngine;
	class CResourceCache;
	class IDebuger;
	class CEvent;
	class CExecContainer;


	using EventObject = std::shared_ptr<CEvent>;

	typedef std::shared_ptr<CDocument> DocumentPtr;
	typedef std::shared_ptr< CScriptEngine > EnginePtr;

	using UEnginePtr = std::unique_ptr< CScriptEngine >;

	typedef std::shared_ptr< CScopedNameTable > ScopedNameTablePtr;
	typedef std::shared_ptr< CResourceCache > ResourceCachePtr;
	typedef std::weak_ptr< IDebuger > DebugerPtr;
	typedef std::shared_ptr< IDebuger > DebugerPtr2;
	typedef std::weak_ptr< CSession > SessionPtr;
	typedef std::shared_ptr< CSession > SessionPtr2;
	typedef std::shared_ptr< CApplication > ApplicationPtr;
	typedef std::weak_ptr< CEngineLog   > EngineLogPtr;
	typedef std::shared_ptr< CEngineLog   > EngineLogPtr2;
	//typedef std::shared_ptr< IEngineCallback   > IEngineCallbackPtr;
	//typedef std::shared_ptr< Iq931Info   > Q931Ptr;
	typedef CComPtr<Iq931Info> Q931Ptr;
	//typedef CComBridge<Iq931Info>  Q931Dispatch; //???

	typedef std::weak_ptr< IEngineCallback   > IEngineCallbackPtr;
	typedef std::shared_ptr< IEngineCallback   > IEngineCallbackPtr2;

#ifdef _EMULATE_TELSERVER
	typedef std::shared_ptr< CTelServerEmulator  > TelServPtr;
#endif
	


	class VXMLVariant
	{
	public:
		static CComVariant& ChangeType(CComVariant& v, VARTYPE vtNew)
		{
			HRESULT hr = v.ChangeType(vtNew);
			if (FAILED(hr))
			{
				throw _com_error(hr);
			}
			return v;
		}
	};

	struct TDefaultVXMLSettings
	{
		TDefaultVXMLSettings(): sTimeout(L"3000"),
			sInterdigittimeout(L"2000"),
			sWaitTime(L"10000"),
			sTermchar(L"#")//,
		    //sKeepAliveTimeout(L"1")
		{}
		std::wstring sTimeout;
		std::wstring sInterdigittimeout;
		std::wstring sWaitTime;
		std::wstring sTermchar;
		//std::wstring sKeepAliveTimeout;
	};

	struct TFolders
	{
		std::wstring sVXMLFolder;
		std::wstring sWavFolder;
		std::wstring sGrammarFolder;
		std::wstring sVideoFolder;
	};
	struct TSystem
	{
		TSystem(): sKeepAliveTimeout(L"1")
		{}
		std::wstring sKeepAliveTimeout;
	};

	const VXMLString SYMBOLNOTFOUND_DBGMSG      = L"Symbol not found";
	const VXMLString PROCESSNOTAVAILABLE_DBGMSG = L"Process not available";

	class CNameTable;
	using SessionObject = std::shared_ptr<CNameTable> ;
	using WeakSessionObject = std::weak_ptr<CNameTable>;
	typedef CComBridge<CNameTable>  NameTableDispatch;
	typedef CComBridge<CNameTable>* NameTableDispatchPtr;

	class VXMLGrammar;
	using SessionGrammar = std::shared_ptr<VXMLGrammar>;
	using WeakSessionGrammar = std::weak_ptr<VXMLGrammar>;
	typedef CComBridge<VXMLGrammar>  GrammarDispatch;
	typedef CComBridge<VXMLGrammar>* GrammarDispatchPtr;

}