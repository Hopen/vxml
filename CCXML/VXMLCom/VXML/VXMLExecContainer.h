/************************************************************************/
/* Name     : VXMLCom\VXML\VXMLExecContainer.h                          */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 2 Jun 2018                                                */
/************************************************************************/

#include "VXMLSession.h"
#include "KeepAliveProvider.h"
#include "VXMLDocument.h"

namespace VXML
{
	/****************************************************************************************************/
	/*                                          CExecContainer                                          */
	/****************************************************************************************************/

	class CExecContainer
	{
	public:
		CExecContainer(const SessionCreateParams& params)
			: mSes(params.pSession)
			, mVar(params.pVar)
			, mEvt(params.pEvtPrc)
			, mLog(params.pLog)
			, mXLog(params.pExtraLog)
			, m931(params.pQ931Info)
			, mXML(params.sQ931XML)
			, mDbg(params.pDebuger)
			, mCbk(params.pCallback)
		{

		}

		/***** Debuger *****/
		void DbgInit(const CMessage& aInitMsg)
		{
			//std::lock_guard<std::mutex> lock(mDbgMutex);
			auto dbg = mDbg.lock();
			if (dbg)
			{
				dbg->Init(aInitMsg, mSes);
			}
		}

		void EmitStep()
		{
			//std::lock_guard<std::mutex> lock(mDbgMutex);

			//++m_nStepCounter;
			auto dbg = mDbg.lock();
			if (dbg)
			{
				dbg->Increase();
			}
		}

		bool WaitForStep(size_t aLine)
		{
			//std::lock_guard<std::mutex> lock(mDbgMutex);

			auto dbg = mDbg.lock();
			if (!dbg)
			{
				return false;
			}

			dbg->Wait(aLine);
			return true;
		}

		template <class... TArgs>
		bool DbgEvent(TArgs&&... aArgs)
		{
			//std::lock_guard<std::mutex> lock(mDbgMutex);

			auto dbg = mDbg.lock();
			if (!dbg)
			{
				return false;
			}
			dbg->SetAuxEvent(std::forward<TArgs>(aArgs)...);

			return true;
		}

		/***** KeepAlive *****/
		void InitKeepAlive(size_t aTimer)
		{
			std::lock_guard<std::mutex> lock(mKAMutex);
			mKeepAlive = std::make_unique<CKeepAliveProvider>(mCbk, mLog);
			mKeepAlive->Init(aTimer * 60);
		}

		template <class... TArgs>
		void SubscribeTimer(TArgs&&... aArgs)
		{
			std::lock_guard<std::mutex> lock(mKAMutex);
			mKeepAlive->SubscribeTimer(std::forward<TArgs>(aArgs)...);
		}

		template <class... TArgs>
		void UnsubscribeTimer(TArgs&&... aArgs)
		{
			std::lock_guard<std::mutex> lock(mKAMutex);
			mKeepAlive->UnsubscribeTimer(std::forward<TArgs>(aArgs)...);
		}

		size_t IncreaseDeadCounter()
		{
			return mKeepAlive->IncreaseDeadCounter();
		}

		void ResetDeadTimer()
		{
			mKeepAlive->ResetDeadCounter();
		}

		/****** EngineCallBack *******/
		void PostAuxMessage(const CMessage& aMessage)
		{
			CPackedMsg pm(aMessage);
			mCbk->PostAuxMessage(pm());
		}

		IEngineCallback* GetEngineCallBack() const
		{
			return mCbk;
		}

		/**** ScriptEngine *****/

		void InitScriptEngine()
		{
			mEng = std::make_unique<CScriptEngine>(mSes.lock(), mLog);
		}

		void DestroyScriptEngine()
		{
			mEng->Clear();
			mEng.reset();
		}

		void AddObject(const std::wstring& sName, const CComVariant &object)
		{
			std::lock_guard<std::mutex> lock(mEngineMutex);
			if (FAILED(mEng->AddObject(sName, object)))
			{
				throw std::runtime_error((std::string("Cannot add object: ") + wtos(sName)).c_str());
			}
		}

		void AddObject2(const std::wstring& sName, const CComVariant &object)
		{
			std::lock_guard<std::mutex> lock(mEngineMutex);
			if (FAILED(mEng->AddObject2(sName, object)))
			{
				throw std::runtime_error((std::string("Cannot add object 2: ") + wtos(sName)).c_str());
			}
		}

		void ExecNormalStatement(const VXMLString& aStatement)
		{
			std::lock_guard<std::mutex> lock(mEngineMutex);

			mScriptExprSource = aStatement;
			if (FAILED(mEng->ExecNormalStatement(aStatement)))
			{
				throw std::runtime_error((std::string("Cannot execute: ") + wtos(aStatement)).c_str());
			}
		}

		void ExecAssignNormalStatement(const VXMLString& aStatement)
		{
			std::lock_guard<std::mutex> lock(mEngineMutex);

			mScriptExprSource = aStatement;
			if (FAILED(mEng->ExecAssignNormalStatement(aStatement)))
			{
				throw std::runtime_error((std::string("Cannot execute: ") + wtos(aStatement)).c_str());
			}
		}

		void ExecStatement(const VXMLString& aStatement)
		{
			std::lock_guard<std::mutex> lock(mEngineMutex);

			mScriptExprSource = aStatement;
			if (FAILED(mEng->ExecStatement(aStatement)))
			{
				throw std::runtime_error((std::string("Cannot execute: ") + wtos(aStatement)).c_str());
			}
		}

		void ExecScript(const VXMLString& aStatement)
		{
			std::lock_guard<std::mutex> lock(mEngineMutex);

			mScriptExprSource = aStatement;
			if (FAILED(mEng->ExecAssignNormalStatement(aStatement)))
			{
				throw std::runtime_error((std::string("Cannot execute script: ") + wtos(aStatement)).c_str());
			}
		}

		VXMLString ScriptSource() const
		{
			//std::lock_guard<std::mutex> lock(mEngineMutex);
			//return mEng->ScriptSource();
			return mScriptExprSource;
		}

		VXMLString ExprEvalToStr(const VXMLString& aExpr) const
		{
			std::lock_guard<std::mutex> lock(mEngineMutex);
			return mEng->ExprEvalToStr(aExpr);
		}

		int ExprEvalToInt(const VXMLString& aExpr) const
		{
			std::lock_guard<std::mutex> lock(mEngineMutex);
			return mEng->ExprEvalToInt(aExpr);
		}

		template <class... TArgs>
		CComVariant ExprEvalToType(TArgs&&... aArgs) const
		{
			std::lock_guard<std::mutex> lock(mEngineMutex);
			return mEng->ExprEvalToType(std::forward<TArgs>(aArgs)...);
		}

		bool ExprEvalToBool(const VXMLString& aExpr) const
		{
			std::lock_guard<std::mutex> lock(mEngineMutex);
			return mEng->ExprEvalToBool(aExpr);
		}

		CComVariant ExprEval(const VXMLString& aExpr) const
		{
			std::lock_guard<std::mutex> lock(mEngineMutex);
			return mEng->ExprEval(aExpr);
		}

		/**** Documents *****/
		bool LoadPredefinitions(const VXMLString& aPredefine)
		{
			std::lock_guard<std::mutex> lock(mDocMutex);
			mPredefineDocuments = mSes.lock()->LoadPredefinitions2(aPredefine);

			return mPredefineDocuments.empty();
		}

		template <class TFunction>
		void ExecutePredefinitions(TFunction aLog)
		{
			std::lock_guard<std::mutex> lock(mDocMutex);
			for (const auto& predefineDoc : mPredefineDocuments)
			{
				if (!predefineDoc)
				{
					continue;
				}

				aLog(predefineDoc->GetDocUri());
				predefineDoc->Execute(this);
			}
		}

		void ShortDocument(const DocumentPtr& aDocumentPtr)
		{
			std::lock_guard<std::mutex> lock(mDocMutex);
			while (mPreviousDoc.size() > 3)
				mPreviousDoc.pop_front();

			mPreviousDoc.push_back(aDocumentPtr);

			mDoc = aDocumentPtr;
		}

		bool Execute()
		{
			std::lock_guard<std::mutex> lock(mDocMutex);
			return mDoc->Execute(this);
		}

		/******* Variables ********/
		void PopScope()
		{
			std::lock_guard<std::mutex> lock(mVarMutex);
			mVar->PopScope();
		}

		template <class... TArgs>
		void PushScope(TArgs&& ...aArgs)
		{
			std::lock_guard<std::mutex> lock(mVarMutex);
			mVar->PushScope(std::forward<TArgs>(aArgs)...);
		}

		template <class... TArgs>
		void AddVar(TArgs&& ...aArgs)
		{
			std::lock_guard<std::mutex> lock(mVarMutex);
			mVar->AddVar(std::forward<TArgs>(aArgs)...);
		}

		template <class... TArgs>
		void DelVar(TArgs&& ...aArgs)
		{
			std::lock_guard<std::mutex> lock(mVarMutex);
			mVar->DelVar(std::forward<TArgs>(aArgs)...);
		}

		CComVariant GetGlobal() const
		{
			std::lock_guard<std::mutex> lock(mVarMutex);
			return mVar->GetGlobal();
		}

		const VAR GetValue(const VXMLString& sName) const
		{
			std::lock_guard<std::mutex> lock(mVarMutex);
			return mVar->GetValue(sName);
		}

		/*********** Q931 ************/
		VXMLString GetIsupMessage() const
		{
			return mXML;
		}

		Q931Ptr GetQ931Ptr() const
		{
			return m931;
		}

		/******** CSession *********/

		CComVariant FindAttrEvent(const VXMLString & aConnectionId) const
		{
			return mSes.lock()->FindAttrEvent(aConnectionId);
		}
		
		template <class... TArgs>
		void DoError(TArgs&&... aArgs)
		{
			mSes.lock()->DoError(std::forward<TArgs>(aArgs)...);
		}

		template <class... TArgs>
		void DoExit(TArgs&&... aArgs)
		{
			mSes.lock()->DoExit(std::forward<TArgs>(aArgs)...);
		}

		void DoEvent(const CMessage& aMsg)
		{
			mSes.lock()->DoEvent(aMsg);
		}

		void DoTerminated()
		{
			mSes.lock()->DoTerminated();
		}

		ObjectId GetParentCallID() const
		{
			return mSes.lock()->GetParentCallID();
		}

		ObjectId GetScriptID() const
		{
			return mSes.lock()->GetScriptID();
		}

		ObjectId GetParentScriptID() const
		{
			return mSes.lock()->GetParentScriptID();
		}

		template <class... TArgs>
		eWaitEvents WaitEvent(TArgs&&... aArgs)
		{
			return mSes.lock()->WaitEvent(std::forward<TArgs>(aArgs)...);
		}

		template <class... TArgs>
		eWaitEvents WaitMultipleEvent(TArgs&&... aArgs)
		{
			return mSes.lock()->WaitMultipleEvent(std::forward<TArgs>(aArgs)...);
		}

		VXMLString GetWavFolder() const
		{
			return mSes.lock()->GetWavFolder();
		}

		VXMLString GetFileFolder() const
		{
			return mSes.lock()->GetFileFolder();
		}

		VXMLString GetVideoFolder() const
		{
			return mSes.lock()->GetVideoFolder();
		}
		
		VXMLString GetCurDoc() const
		{
			return mSes.lock()->GetCurDoc();
		}

		bool SuccessedEvent() const
		{
			return mSes.lock()->SuccessedEvent();
		}

		template <class... TArgs>
		void Log(TArgs&&... aArgs) const
		{
			mSes.lock()->Log(std::forward<TArgs>(aArgs)...);
		}

		template <class... TArgs>
		void LogExtra(TArgs&&... aArgs) const
		{
			mSes.lock()->LogExtra(std::forward<TArgs>(aArgs)...);
		}

		bool IsActive() const
		{
			return mSes.lock()->IsActive();
		}

		template <class... TArgs>
		void SendEvent(TArgs&&... aArgs) const
		{
			mSes.lock()->SendEvent(std::forward<TArgs>(aArgs)...);
		}

		template <class... TArgs>
		void AcceptEvent(TArgs&&... aArgs) const
		{
			mSes.lock()->AcceptEvent(std::forward<TArgs>(aArgs)...);
		}

		template <class... TArgs>
		void CancelEvent(TArgs&&... aArgs) const
		{
			mSes.lock()->CancelEvent(std::forward<TArgs>(aArgs)...);
		}
		
		void SetCurMenu(const VXMLString& aCurMenu)
		{
			m_sCurMenu = aCurMenu;
		}

		VXMLString GetCurMenu() const
		{
			return m_sCurMenu;
		}

		//bool IsTransferActive() const
		//{
		//	return mIsTransferActive;
		//}

		//void SetTransferActive(bool aIsActive)
		//{
		//	mIsTransferActive = aIsActive;
		//}

		template <class... TArgs>
		void DoNext(TArgs&&... aArgs)
		{
			mSes.lock()->DoNext(std::forward<TArgs>(aArgs)...);
		}

		void DoDialogError(const VXMLString& aMsg) const
		{
			mSes.lock()->DoDialogError(aMsg);
		}
		
		template <class... TArgs>
		VXMLString FindGrammarEvent(TArgs&&... aArgs)
		{
			return mSes.lock()->FindGrammarEvent(std::forward<TArgs>(aArgs)...);
		}

		template <class... TArgs>
		void ProccedFormComment(TArgs&&... aArgs)
		{
			mSes.lock()->ProccedFormComment(std::forward<TArgs>(aArgs)...);
		}

		template <class... TArgs>
		void AddGrammar(TArgs&&... aArgs)
		{
			mSes.lock()->AddGrammar(std::forward<TArgs>(aArgs)...);
		}

		VXMLString GetGrammaPath() const
		{
			return mSes.lock()->GetGrammaPath();
		}
		
		void AddNamelistToMsg(CMessage& aMsg)
		{
			return mSes.lock()->AddNamelistToMsg(aMsg);
		}

		bool DialogStart(CMessage& aMsg)
		{
			return mSes.lock()->DialogStart(aMsg);
		}

		template <class... TArgs>
		void ChangeCallCtrl(TArgs&&... aArgs)
		{
			mSes.lock()->ChangeCallCtrl(std::forward<TArgs>(aArgs)...);
		}

		void StartKeepAliveTimers()
		{
			mSes.lock()->startKeepAliveTimers();
		}

		void StopKeepAliveTimers()
		{
			mSes.lock()->stopKeepAliveTimers();
		}

		template <class... TArgs>
		void SetVXML(TArgs&&... aArgs)
		{
			mSes.lock()->SetVXML(std::forward<TArgs>(aArgs)...);
		}


		template <class... TArgs>
		VXMLString GetSpeakMoneyFiles(TArgs&&... aArgs) const
		{
			return mSes.lock()->GetSpeakMoneyFiles(std::forward<TArgs>(aArgs)...);
		}

		template <class... TArgs>
		VXMLString GetSpeakDateFiles(TArgs&&... aArgs) const
		{
			return mSes.lock()->GetSpeakDateFiles(std::forward<TArgs>(aArgs)...);
		}

		template <class... TArgs>
		VXMLString GetSpeakTimeFiles(TArgs&&... aArgs) const
		{
			return mSes.lock()->GetSpeakTimeFiles(std::forward<TArgs>(aArgs)...);
		}

		template <class... TArgs>
		VXMLString GetSpeakNumberFiles(TArgs&&... aArgs) const
		{
			return mSes.lock()->GetSpeakNumberFiles(std::forward<TArgs>(aArgs)...);
		}

		template <class... TArgs>
		VXMLString GetSpeakTrafficFiles(TArgs&&... aArgs) const
		{
			return mSes.lock()->GetSpeakTrafficFiles(std::forward<TArgs>(aArgs)...);
		}

		template <class... TArgs>
		VXMLString GetSpeakMinutesFiles(TArgs&&... aArgs) const
		{
			return mSes.lock()->GetSpeakMinutesFiles(std::forward<TArgs>(aArgs)...);
		}

		template <class... TArgs>
		VXMLString GetSpeakSMSFiles(TArgs&&... aArgs) const
		{
			return mSes.lock()->GetSpeakSMSFiles(std::forward<TArgs>(aArgs)...);
		}

		template <class... TArgs>
		void ProccedBufferComment(TArgs&&... aArgs) const
		{
			mSes.lock()->ProccedBufferComment(std::forward<TArgs>(aArgs)...);
		}


	private:
		SessionPtr			mSes;
		DocumentPtr			mDoc;
		std::list<DocumentPtr>	mPredefineDocuments;
		std::list<DocumentPtr>  mPreviousDoc;

		ApplicationPtr		mApp;
		ScopedNameTablePtr	mVar;
		UEnginePtr		    mEng;
		IEngineCallbackPtr	mEvt;
		EngineLogPtr2		mLog;
		EngineLogPtr2		mXLog;
		Q931Ptr 			m931;
		VXMLString			mXML;
		DebugerPtr          mDbg;
		IEngineCallback*	mCbk;

		//unsigned int        m_nStepCounter = 0;
		VXMLString			m_sCurMenu;

		//mutable std::mutex mDbgMutex;
		mutable std::mutex mEngineMutex;
		mutable std::mutex mVarMutex;
		mutable std::mutex mDocMutex;
		mutable std::mutex mKAMutex;

		//bool mIsTransferActive = false;


		std::unique_ptr<CKeepAliveProvider> mKeepAlive;

		mutable VXMLString mScriptExprSource;
	};

}
/******************************* eof *************************************/