/************************************************************************/
/* Name     : VXMLCom\VXMLNameTable.h                                   */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 7 Dec 2009                                                */
/************************************************************************/

#pragma once

#include <map>
#include <vector>
#include <list>
#include <mutex>
#include "VXMLBase.h"
#include "EvtModel.h"
#include "InstanceController.h"

#define VAR_SIGNATURE			0x12345678

namespace VXML
{
	class CNameTable;

	//struct VAR
	//{
	//	CComVariant	vValue;
	//	VXMLString	sName;
	//	bool		bReadOnly;
	//	DWORD		dwSignature;
	//	VAR(const VXMLString& name, const CComVariant& value, bool readonly)
	//		: sName(name), vValue(value), bReadOnly(readonly),
	//		dwSignature(VAR_SIGNATURE)
	//	{
	//	}
	//	VAR(const VAR& rhs)
	//	{
	//		this->vValue      = rhs.vValue;
	//		this->sName       = rhs.sName;
	//		this->bReadOnly   = rhs.bReadOnly;
	//		this->dwSignature = rhs.dwSignature;
	//	}
	//	VAR()
	//	{
	//		::VariantInit(&vValue);
	//	}
	//	void SetValue(VARIANT* pNewVal);
	//	void SetValue(const CComVariant& newVal);
	//};

	struct VAR : public InstanceController<VAR>
	{
		CComVariant	vValue;
		VXMLString	sName;
		bool		bReadOnly;
	private:
		bool		bIsEmpty;
	public:

		VAR();
		VAR(const VXMLString& name, const CComVariant& value, bool readonly);
		VAR(const VAR& rhs);

		VAR& operator=(VAR const& rhs);

		virtual ~VAR();

		//void SetValue(VARIANT* pNewVal);
		void SetValue(const CComVariant& newVal);
		//void SetInitialValue(const CComVariant& newVal);

		bool IsEmpty()const { return bIsEmpty; }
	};


	class CScopedNameTable
	{
	private:
		//typedef CComPtr<CNameTable> CComTable;
		//typedef std::pair<VXMLString,CNameTable* > ScopePair;
		//typedef std::pair<VXMLString,CComVariant > ScopePair;
		//typedef std::pair<VXMLString,CComTable > ScopePair;
		typedef std::pair<VXMLString,SessionObject> ScopePair;
		typedef std::list<ScopePair> Scopes;
		//typedef std::vector<CNameTable* /*SessionObject*/> StackGlobalNames;
		//typedef std::vector<CComVariant /*SessionObject*/> StackGlobalNames;
		typedef std::vector<SessionObject> StackGlobalNames;

		//CNameTable* m_pGlobalNames;
		//CComVariant m_pGlobalNames;
		SessionObject m_pGlobalNames;
		//CComTable	m_pGlobalNames;
		Scopes		     m_Scopes;
		StackGlobalNames m_gStack;
		mutable std::mutex m_mutex;

		int m_count;

	public:
		CScopedNameTable();
		~CScopedNameTable();
		void PushScope(const VXMLString& sName);
		void PopScope();
		//void PushStack();
		//void PopStack();
		const VAR AddVar(const VXMLString& sName, const CComVariant& val, bool bReadOnly);
		void DelVar(const VXMLString& sName);
		//VAR* Find(const VXMLString& sName);
		//VAR& Lookup(const VXMLString& sName);
		const VAR GetValue(const VXMLString& sName);
		//CNameTable*	GetGlobal()const{return m_pGlobalNames/*.get()*/;}
		CComVariant	GetGlobal()const;
		//{
		//	if (CNameTable * nt = dynamic_cast<CNameTable>(m_pGlobalNames.pdispVal))
		//		return nt;
		//	return NULL;
		//}
	};
}