/************************************************************************/
/* Name     : VXMLCom\VXMLDocument.h                                    */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 15 Dec 2009                                               */
/************************************************************************/
#pragma once

#include "VXMLBase.h"
#include "VXMLTags.h"
#include <list>
#include <map>
#include "boost/shared_ptr.hpp"

namespace VXML
{
	class CDocument
	{
	private:
		TagPtr m_pRootTag;
		VXMLString m_DocUri;
	public:
		CDocument(
			const Factory_c::CollectorPtr& _parse, 
			const CTagFactory _factory, 
			EventScope& _scope, 
			EventScope& _propmts, 
			const VXMLString& docUri);

		~CDocument();
		bool Execute(CExecContainer* pCtx);
		VXMLString GetDocUri()const { return m_DocUri; }
	};
}
