/************************************************************************/
/* Name     : VXMLCom\VXMLHTTPReader.h                                  */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 11 Jun 2010                                               */
/************************************************************************/


#ifndef _HTTPREADER_H
#define _HTTPREADER_H

#include <wininet.h>
#include <string>
#include <comdef.h>

namespace VXML
{
	class CHTTPReader
	{
	public:
		UINT GetParam(wchar_t* param, wchar_t* value, wchar_t* data);
		UINT login(wchar_t* username, wchar_t* pass);
		CHTTPReader(LPCTSTR lpszServerName=NULL,bool bUseSSL=false);
		~CHTTPReader();

		bool  OpenInternet(LPCTSTR lpszAgent=TEXT("VXMLCom"));
		void  CloseInternet    ();
		bool  OpenConnection   (LPCTSTR lpszServerName=NULL);
		void  CloseConnection  ();

		bool  Get              (LPCTSTR lpszAction,LPCTSTR lpszReferer=NULL);
		bool  Post             (LPCTSTR lpszAction,LPCTSTR lpszData,LPCTSTR lpszReferer=NULL);
		void  CloseRequest     ();

		wchar_t *GetData          (wchar_t *lpszBuffer,DWORD dwSize,DWORD *lpdwBytesRead=NULL);
		wchar_t *GetData          (DWORD *lpdwBytesRead=NULL);
		DWORD GetDataSize      ();
		void  SetDataBuffer    (DWORD dwBufferSize);

		void  SetDefaultHeader (LPCTSTR lpszDefaultHeader);
		DWORD GetError         () const { return m_dwLastError; }

	protected:
		//UINT GetModuleVersion( const char *modulePath, std::wstring & version );

		bool CheckError (bool bTest);
		void StrDup     (LPTSTR& lpszDest,LPCTSTR lpszSource);
		bool SendRequest(LPCTSTR lpszVerb,LPCTSTR lpszAction,LPCTSTR lpszData,LPCTSTR lpszReferer);

		HINTERNET m_hInternet;
		HINTERNET m_hConnection;
		HINTERNET m_hRequest;

		DWORD     m_dwLastError;
		bool      m_bUseSSL;
		LPTSTR    m_lpszServerName;
		LPTSTR    m_lpszDefaultHeader;
		wchar_t  *m_lpszDataBuffer;
		DWORD     m_dwBufferSize;
	};

	class CMemBuffer
	{
	private:
		BYTE*  buffer;
		BYTE*  position;
		size_t size;
	public:
		~CMemBuffer(){ free(buffer);}
		CMemBuffer()         { position = buffer = (BYTE*)malloc(size = 4096);}
		CMemBuffer(size_t s) { position = buffer = (BYTE*)malloc(size = s ? s : 4096);}
		CMemBuffer(const void* pBuf, size_t s) {
			position = buffer = (BYTE*)malloc(size = s ? s : 4096);
			memcpy(buffer, pBuf, size);
			position += size;
		}
		CMemBuffer(const CMemBuffer& mb) {
			buffer = (BYTE*)malloc(size = mb.GetSize());
			memcpy(buffer, mb.GetData(), size);
			position = buffer + size;
		}

		CMemBuffer & operator =(const CMemBuffer& mb) {
			free(buffer);
			buffer = (BYTE*)malloc(size = mb.GetSize());
			memcpy(buffer, mb.GetData(), size);
			position = buffer + size;
			return *this;
		}

		CMemBuffer & operator +=(const CMemBuffer& mb) {
			Add(mb.GetData(), mb.GetSize());
			return *this;
		}

		const BYTE* GetData() const {return buffer;}

		void Add(const void* pBuf, size_t sizeBuf) {
			if (!pBuf || !sizeBuf) return;

			size_t busy_size = position - buffer;
			size_t free_size = size - busy_size;
			if (free_size < sizeBuf) {
				size_t must_add = sizeBuf-free_size;
				buffer = (BYTE*)realloc(buffer, (size += must_add));
				position = buffer + busy_size; // readjust current position
			}
			memcpy(position, pBuf, sizeBuf);
			position += sizeBuf;
		}

		size_t GetSize() const {return position - buffer;}

		friend bool operator ==(const CMemBuffer& mb1, const CMemBuffer& mb2) {
			bool bResult = false;
			size_t size = mb1.GetSize();
			if (size == mb2.GetSize()) {
				bResult = (0==memcmp(mb1.GetData(), mb2.GetData(), size));
			}
			return bResult;
		}
	};


	const CMemBuffer INVALID_MEM_BUFFER(0);


	class CHTTPConnector
	{
		BOOL m_bConnection;
	public:
		CHTTPConnector::CHTTPConnector();
		virtual CHTTPConnector::~CHTTPConnector();

		int PostData(std::wstring sURL,
			UINT uFileSize,
			std::wstring ObjectName, 
			LPCWSTR lpOptional, 
			std::wstring &Data);

		int GetData(std::wstring sURL,
			UINT uFileSize,
			std::wstring ObjectName, 
			std::wstring &Data);

		//int Exec(funcHTTP, ConnectionData*);

		// callback functions
		//static int Browser			(ConnectionData*);
		//static int BuyBook			(ConnectionData*);
		//static int DownloadBook		(ConnectionData*);
		//static int LoadCover        (ConnectionData*);
		//static int DownloadPage		(ConnectionData*);
	};

}


#endif //_HTTPREADER_H

//**************************** eof ******************************* //