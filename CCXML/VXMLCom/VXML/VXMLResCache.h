/************************************************************************/
/* Name     : VXMLCom\VXMLResCache.h                                    */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 1 Dec 2009                                                */
/************************************************************************/
#pragma once

#include "ce_xml.hpp"
#include "sv_utils.h"
#include "VXMLBase.h"
#include "..\..\CacheInterface\CacheInterface_i.h"
#import "MsXml6.dll"

extern HINSTANCE g_hInst;

namespace VXML
{
	class CResourceCache
	{
		CComPtr<IResourceCache> m_Res;
		HRESULT m_hr;

		CComPtr<ITestMTObj> m_Test;
		_bstr_t m_SchemaFile;

	public:
		HRESULT GetDocument(
			const VXMLString& aSrc, 
			const VXMLString& aScriptID, 
			const bool& aIsFile, 
			SAFEARRAY** aPSA,
			bool aIsSecondTry = false);
		CResourceCache();
		~CResourceCache();

		HRESULT ConnectToCache();

		bool Ready()const;

		bool TestReady()const;
		HRESULT GetHR()const{ return m_hr; }
		HRESULT WriteTestLog(const VXMLString& sSrc);
	};
}
