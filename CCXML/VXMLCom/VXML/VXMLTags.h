/************************************************************************/
/* Name     : VXMLCom\VXML\VXMLTags.h                                   */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 2 Dec 2009                                                */
/************************************************************************/

#pragma once

#include <map>
#include <list>
#include "VXMLBase.h"
#include "EvtModel.h"
#include "VXMLEvent.h"
#include "VXMLGrammar.h"
#include "..\Common\factory_c.h"

//#import "MsXml6.dll"

namespace VXML
{
	class CTagFactory			;

	class CTag_assign           ;
	class CTag_audio			;
	class CTag_block			;
	class CTag_catch            ;
	class CTag_choice           ;
	class CTag_else             ;
	class CTag_elseif           ;
	class CTag_error            ;
	class CTag_exit				;
	class CTag_goto				;
	class CTag_field			;
	class CTag_filled			;
	class CTag_form				;
	class CTag_grammar			;
	class CTag_if               ;
	class CTag_log              ;
	class CTag_media			;
	class CTag_menu				;
	class CTag_noinput          ;
	class CTag_nomatch			;
	class CTag_object			;
	class CTag_prompt			;
	class CTag_property			;
	class CTag_record			;
	class CTag_reprompt         ;
	class CTag_return			;
	class CTag_say_as			;
	class CTag_script           ;
	class CTag_subdialog		;
	class CTag_submit			;
	class CTag_transfer			;
	class CTag_throw			;
	class CTag_value    		;
	class CTag_var              ;
	class CTag_vxml				;

	class CEvtScope;
	typedef std::list <CEvtScope> EventScope;

	class CTag: public Factory_c::CFTag
	{
	protected:
		CComVariant EvalAttr(
			CExecContainer* pCtx, 
			const VXMLString& sAttrName, 
			const CComVariant& defVal, 
			VARTYPE evalType);

		CComVariant EvalAttrEvent(
			CExecContainer* pCtx, 
			const VXMLString& sAttrName, 
			VARTYPE evalType);

	public:
		CTag(
			const Factory_c::CollectorPtr& _parser, 
			const CTagFactory& _factory,
			EventScope& _scope, 
			EventScope& _prompts, 
			const VXMLString& _docUri);
		//{
		//	m_sName   = _parser->GetName();
		//	m_sText   = _parser->GetText();
		//	m_Attribs = _parser->GetAttr();
		//	m_iLine   = _parser->GetLine();
		//}

		virtual ~CTag();//{}
		virtual bool Execute(CExecContainer* pCtx) = NULL;
		void EmitError(CExecContainer* pCtx, CMessage& evt, const VXMLString& description, const VXMLString& reason);
		virtual bool CheckDigits(CExecContainer* pCtx);
		virtual CMessage CreatePlayWavMessage(CExecContainer* pCtx, const VXMLString& _uri);
		//virtual VXMLString Match(CExecContainer* pCtx, VAR* pGrammar);

	//private:
		//CEngineLog*			m_pLog;
	};

	using TagPtr = std::shared_ptr<CTag>;

	class CParentTag : public CTag
	{
	protected:
		typedef std::list<TagPtr>	TagContainer;
		TagContainer				m_Children;

	public:
		CParentTag(
			const Factory_c::CollectorPtr& _parser, 
			const CTagFactory& _factory,
			EventScope& _scope, 
			EventScope& _prompts, 
			const VXMLString& _docUri);

		~CParentTag();
		virtual bool Execute(CExecContainer* pCtx);
		bool ExecuteChild(CExecContainer* pCtx, TagPtr pTag);
	};

	class CTextTag : public CTag
	{
	public:
		template <class... TArgs>
		CTextTag(TArgs&&... aArgs) : CTag(std::forward<TArgs>(aArgs)...) { }

		const VXMLString& GetText() const;
	};

	class CEvtScope
	{
	public:
		CEvtScope():m_iCount(0),m_pExecTag(NULL){}
		CEvtScope(const VXMLString& _name,
			const int& _count,
			const VXMLString& _cond,
			CTag* const _tag, 
			const VXMLString& _url) 
			:m_sName(_name),
			 m_iCount(_count),
			 m_sCond(_cond),
			 m_pExecTag(_tag), 
			 m_sUrl(_url)
		{}
	public:
		VXMLString GetEventName()const {return m_sName;}
		int GetCount()const{return m_iCount;}
		bool ExecCond(CExecContainer* pCtx);
		bool Execute(CExecContainer* pCtx){ return m_pExecTag->Execute(pCtx);}
		VXMLString GetDocUri()const { return m_sUrl; }
	private:
		VXMLString m_sName;  // event name
		int        m_iCount; // existing counter
		VXMLString m_sCond;  // executing condition
		VXMLString m_sUrl;   // document name
		CTag*      m_pExecTag;
	};

	class CFindDocumentScopes
	{
	public:
		CFindDocumentScopes(const VXMLString& _uri) :
			m_uri(_uri)
		{}
		bool operator()(CEvtScope _event)
		{
			return !CAtlString(_event.GetDocUri().c_str()).CompareNoCase(m_uri.c_str());
		}

	private:
		VXMLString m_uri;
	};


	class CExecutableTag: public CParentTag 
	{
	public:
		CExecutableTag(
			const Factory_c::CollectorPtr& _parser, 
			const CTagFactory& _factory,
			EventScope& _scope, 
			EventScope& _prompts, 
			const VXMLString& _docUri);
	protected:
		EventScope m_EvtScope;
		EventScope m_Prompts;

		// predicates
		class CMatchNameAndCondition
		{
		public:
			CMatchNameAndCondition(CExecContainer* pCtx, const VXMLString& sEventName):
			  m_pCtx(pCtx),
				  m_sEventName(sEventName)
			  {}

			  bool operator()(CEvtScope& scope)
			  {
				  return (scope.GetEventName() != m_sEventName || !scope.ExecCond(m_pCtx));
			  }

		private:
			CExecContainer* m_pCtx;
			VXMLString    m_sEventName;
		};

		class CFindCurPrompt
		{
		public:
			CFindCurPrompt(const int& cur_count):m_iCurCount(cur_count)
			{}
			bool operator()(const CEvtScope& scope)
			{
				return (scope.GetCount()==m_iCurCount);
			}

		private:
			int m_iCurCount;
		};

		bool MatchPrompt(TagPtr promptTag, CExecContainer* pCtx);
	public:
		bool ExecEvents(CExecContainer* pCtx, const VXMLString& event/*, bool incCounter = true*/);
		virtual VXMLString GetExecutableTagId() const = 0;
		//void ExecErrors(CExecContainer* pCtx);
	};


	//extern EventScope g_EvtS;


	// TAGS
	class CTag_audio: public CTag
	{
	public:
		template <class... TArgs>
		CTag_audio(TArgs&&... aArgs) : CTag(std::forward<TArgs>(aArgs)...) { }

		~CTag_audio();
		virtual bool Execute(CExecContainer* pCtx);
	protected:
		bool PlayAudio(const VXMLString& uri, CExecContainer* pCtx);
	};

	class CTag_block: public CParentTag
	{
	public:
		template <class... TArgs>
		CTag_block(TArgs&&... aArgs) : CParentTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);
	};

	class CTag_catch: public CParentTag
	{
	public:
		CTag_catch(
			const Factory_c::CollectorPtr& _parser, 
			const CTagFactory& _factory,
			EventScope& _scope, 
			EventScope& _prompts, 
			const VXMLString& _docUri);
		virtual bool Execute(CExecContainer* pCtx);
	};

	class CTag_choice: public CParentTag
	{
	public:
		template <class... TArgs>
		CTag_choice(TArgs&&... aArgs) : CParentTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);
		//virtual void CheckDigits(CExecContainer* pCtx);
		//VXMLString MatchDTMF(CExecContainer* pCtx, const VXMLString& sDTMF);
		VXMLString Match(CExecContainer* pCtx, VAR* pGrammar);
	};

	class CTag_else : public CTag
	{
	public:
		template <class... TArgs>
		CTag_else(TArgs&&... aArgs) : CTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);
	};

	class CTag_elseif : public CTag
	{
	public:
		template <class... TArgs>
		CTag_elseif(TArgs&&... aArgs) : CTag(std::forward<TArgs>(aArgs)...) { }
		virtual bool Execute(CExecContainer* pCtx);
	};

	class CTag_error: public CParentTag
	{
	public:
		CTag_error(
			const Factory_c::CollectorPtr& _parser, 
			const CTagFactory& _factory,
			EventScope& _scope, 
			EventScope& _prompts, 
			const VXMLString& _docUri);
		virtual bool Execute(CExecContainer* pCtx);
	};


	class CTag_exit: public CTag
	{
	public:
		template <class... TArgs>
		CTag_exit(TArgs&&... aArgs) : CTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);
	};

	class CTag_field: public CExecutableTag/*CParentTag*/
	{
	public:
		template <class... TArgs>
		CTag_field(TArgs&&... aArgs) : CExecutableTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);

		void SetAudioInput(const VXMLString& audioinput){m_dtmf_audioinput = audioinput;}
		VXMLString GetAudioInput()const{return m_dtmf_audioinput;}
		void WasVideoCall(){m_bVideoCall = true;}
		//bool WasVideoCall()const {return m_bVideoCall;}
		bool CheckDigits(CExecContainer* pCtx);
		virtual VXMLString GetExecutableTagId() const { return L"field"; };

	private:
		bool m_bVideoCall = false;

	private:
		VXMLString m_dtmf_audioinput;

	};

	class CTag_filled: public CParentTag
	{
	public:
		template <class... TArgs>
		CTag_filled(TArgs&&... aArgs) : CParentTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);
		VXMLString Match(CExecContainer* pCtx, VAR pGrammar);
		bool MatchDialogEnd();
		void SetCurField(CExecutableTag* pTag){m_pCurField = pTag;}
		CExecutableTag* GetCurField()const{return m_pCurField;}

		virtual bool CheckDigits(CExecContainer* pCtx);

	private:
		CExecutableTag* m_pCurField = nullptr;

	};

	// CTag_form haven't to consist noinput and etc. events
	class CTag_form: public CExecutableTag/*CParentTag*/
	{
	public:
		template <class... TArgs>
		CTag_form(TArgs&&... aArgs) : CExecutableTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);
		void SetCurFieldName(const VXMLString& _field){m_sCurField = _field;}
		VXMLString GetCurFieldName()const {return m_sCurField;}
		TagPtr GetField(const VXMLString& _name);
		virtual VXMLString GetExecutableTagId() const { return L"form"; };
	private:
		VXMLString m_sCurField;
	};

	class CTag_goto:public CParentTag
	{
	public:
		template <class... TArgs>
		CTag_goto(TArgs&&... aArgs) : CParentTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);
	private:
	};

	class CTag_grammar:public CTag
	{
	public:
		template <class... TArgs>
		CTag_grammar(TArgs&&... aArgs) : CTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);
	};

	class CTag_if : public CParentTag
	{
	public:
		template <class... TArgs>
		CTag_if(TArgs&&... aArgs) : CParentTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);
	};

	class CTag_log : public CTag
	{
	public:
		template <class... TArgs>
		CTag_log(TArgs&&... aArgs) : CTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);
	};

	class CTag_media: public CTag
	{
	public:
		template <class... TArgs>
		CTag_media(TArgs&&... aArgs) : CTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);
	};


	class CTag_menu: public CExecutableTag
	{
	public:
		template <class... TArgs>
		CTag_menu(TArgs&&... aArgs) : CExecutableTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);

		bool CheckDigits(CExecContainer* pCtx);
		void WasVideoCall(){m_bVideoCall = true;}
		//bool WasVideoCall()const {return m_bVideoCall;}
		virtual VXMLString GetExecutableTagId() const { return L"menu"; };
	private:
		bool m_bVideoCall = false;
	};

	class CTag_noinput: public CParentTag
	{
	public:
		CTag_noinput(
			const Factory_c::CollectorPtr& _parser, 
			const CTagFactory& _factory,
			EventScope& _scope, 
			EventScope& _prompts, 
			const VXMLString& _docUri);

		virtual bool Execute(CExecContainer* pCtx);
	};

	class CTag_nomatch: public CParentTag
	{
	public:
		CTag_nomatch(
			const Factory_c::CollectorPtr& _parser, 
			const CTagFactory& _factory,
			EventScope& _scope, 
			EventScope& _prompts, 
			const VXMLString& _docUri);
		virtual bool Execute(CExecContainer* pCtx);
	};

	class CTag_object: public CTag
	{
	public:
		template <class... TArgs>
		CTag_object(TArgs&&... aArgs) : CTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);
	};


	class CTag_prompt: public CParentTag
	{
	public:
		CTag_prompt(
			const Factory_c::CollectorPtr& _parser, 
			const CTagFactory& _factory,
			EventScope& _scope, 
			EventScope& _prompts, 
			const VXMLString& _docUri);

		virtual bool Execute(CExecContainer* pCtx);
		//void SetCurField(CTag_field* pTag){m_pCurField = pTag;}
		//CTag_field* GetCurField()const{return m_pCurField;} 
		void SetOwnerTag(CTag* pOwner){m_pOwner = pOwner;}
		CTag* GetOwnerTag()const {return m_pOwner;}

	private:
		//CTag_field* m_pCurField;
		CTag* m_pOwner;

	};

	class CTag_property: public CTag
	{
	public:
		template <class... TArgs>
		CTag_property(TArgs&&... aArgs) : CTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);
	};

	class CTag_record: public CExecutableTag
	{
	public:
		template <class... TArgs>
		CTag_record(TArgs&&... aArgs) : CExecutableTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);
		bool GetRecord(CExecContainer* pCtx);

		bool CheckDigits(CExecContainer* pCtx);
		virtual VXMLString GetExecutableTagId() const { return L"record"; };
	//private:
	//	CMessage m_RecWavCompletedMsg;
	};

	class CTag_reprompt:public CTag
	{
	public:
		template <class... TArgs>
		CTag_reprompt(TArgs&&... aArgs) : CTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);
	};

	class CTag_return: public CTag_exit//CTag
	{
	public:
		template <class... TArgs>
		CTag_return(TArgs&&... aArgs) : CTag_exit(std::forward<TArgs>(aArgs)...) { }
	};

	class CTag_say_as: public CParentTag
	{
	public:
		template <class... TArgs>
		CTag_say_as(TArgs&&... aArgs) : CParentTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);

	private:
	};


	class CTag_script : public CTextTag
	{
	public:
		template <class... TArgs>
		CTag_script(TArgs&&... aArgs) : CTextTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);
	};

	class CTag_subdialog: public CExecutableTag
	{
	public:
		template <class... TArgs>
		CTag_subdialog(TArgs&&... aArgs) : CExecutableTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);

		bool CheckDigits(CExecContainer* pCtx);
		virtual VXMLString GetExecutableTagId() const { return L"subdialog"; };
	private:
		bool StartDialog(CExecContainer* pCtx);
	};

	class CTag_submit: public CTag
	{
	public:
		template <class... TArgs>
		CTag_submit(TArgs&&... aArgs) : CTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);
	};

	class CTag_transfer: public CExecutableTag
	{
	public:
		template <class... TArgs>
		CTag_transfer(TArgs&&... aArgs) : CExecutableTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);

		bool GetTransfer(CExecContainer* pCtx);
		bool CheckDigits(CExecContainer* pCtx);
		
		virtual VXMLString GetExecutableTagId() const { return L"transfer"; };
	};

	class CTag_throw:public CParentTag
	{
	public:
		template <class... TArgs>
		CTag_throw(TArgs&&... aArgs) : CParentTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);
	};

	class CTag_value: public CTag_audio
	{
	public:
		template <class... TArgs>
		CTag_value(TArgs&&... aArgs) : CTag_audio(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);
	};

	class CTag_var : public CTag
	{
	public:
		template <class... TArgs>
		CTag_var(TArgs&&... aArgs) : CTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);
	};

	class CTag_vxml: public CParentTag
	{
	public:
		template <class... TArgs>
		CTag_vxml(TArgs&&... aArgs) : CParentTag(std::forward<TArgs>(aArgs)...) { }

		virtual bool Execute(CExecContainer* pCtx);
		bool SetMenu(CExecContainer* pCtx, EventObject pEvt);
		bool SetEvent(CExecContainer* pCtx, EventObject pEvt);
		bool SetExternal(CExecContainer* pCtx, EventObject pEvt);
	};

	class CTag_assign : public CTag_var
	{
	public:
		template <class... TArgs>
		CTag_assign(TArgs&&... aArgs) : CTag_var(std::forward<TArgs>(aArgs)...) { }
	};

	// CLASS FACTORY
	class CTagFactory
	{
	private:
		using TagFunc = TagPtr(*)(
			const Factory_c::CollectorPtr& _parser, 
			const CTagFactory& _factory, 
			EventScope& _scope, 
			EventScope& _prompts, 
			const VXMLString& _docUri);

		using TagsMap = std::map<VXMLString, TagFunc>;

		TagsMap m_TagsMap;
	public:
		CTagFactory::CTagFactory();

		TagPtr CreateTag(
			const VXMLString& sName, 
			const Factory_c::CollectorPtr& _parser, 
			const CTagFactory& _factory,
			EventScope& _scope, 
			EventScope& _prompts, 
			const VXMLString& _docUri) const;
	};

}