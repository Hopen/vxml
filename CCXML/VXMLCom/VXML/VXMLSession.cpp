/************************************************************************/
/* Name     : VXMLCom\VXMLSession.cpp                                   */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 30 Nov 2009                                               */
/************************************************************************/
#include "..\StdAfx.h"
#include <algorithm>
#include <time.h>

#include <boost/format.hpp>
#include "sv_strutils.h"
#include "common.h"
#include "sid.h"

#include "BinaryFile.h"
#include "..\Engine\EngineLog.h"
#include "..\Common\Utils.h"
#include "..\Common\DumpMessage.h"
#include "..\Common\alarmmsg.h"
#include "VXMLSession.h"
#include "VXMLResCache.h"
#include "VXMLEngine.h"
#include "VXMLDebug.h"
#include "StringFunctions.h"
#include "VXMLExecContainer.h"

using namespace enginelog;

#define MSG_LOADDOCUMENT		L"LoadResource"
#define MSG_LOADDOCUMENT_ACK	L"LoadResource_Ack"

const size_t DEADTIMER_LIMIT = 2;

namespace VXML
{
	/****************************************************************************************************/
	/*                                          CStateDispatcher                                        */
	/****************************************************************************************************/

	class CStateDispatcher
	{
	public:
		CStateDispatcher(DWORD aSurrThread,
			bool aLogGetStep,
			EngineLogPtr aLog)
		{
			m_surrThread = aSurrThread;
			m_logGetStep = aLogGetStep;
			m_log = aLog;
		}

		void ChangeState(DWORD dwState)
		{
			SetState(dwState);
		}

		DWORD GetState() const
		{
			std::lock_guard<std::mutex> lock(m_mutex);
			return m_state;
		}
		void SetState(DWORD dwState)
		{
			if (GetState() == dwState) // has already set
				return;

			if (m_logGetStep)
			{
				LogState();
			}

			if (true)
			{
				if (m_surrThread == 0)
				{
					throw std::runtime_error("CStateDispatcher: m_surrThread uninitialized");
				}
				std::lock_guard<std::mutex> lock(m_mutex);

				::InterlockedExchange((LONG*)&m_state, dwState);
				::PostThreadMessageW(m_surrThread, WM_USER, 0, 0);
			}
		}
	private:
		void LogState() const
		{
			EngineLogPtr2 log = m_log.lock();
			if (!log)
			{
				return;
			}

			switch (m_state)
			{
			case VMS_BUSY:
				log->Log(LEVEL_FINEST, __FUNCTIONW__, L"SetState - VMS_BUSY");
				break;
			case VMS_READY:
				log->Log(LEVEL_FINEST, __FUNCTIONW__, L"SetState - VMS_READY");
				break;
			case VMS_WAITING:
				log->Log(LEVEL_FINEST, __FUNCTIONW__, L"SetState - VMS_WAITING");
				break;
			case VMS_ENDSCRIPT:
				log->Log(LEVEL_FINEST, __FUNCTIONW__, L"SetState - VMS_ENDSCRIPT");
				break;
			}
		}
	private:
		mutable std::mutex m_mutex;
		DWORD m_surrThread = 0;
		DWORD m_state = VMS_WAITING;
		bool m_logGetStep = false;
		EngineLogPtr m_log;
	};



	IVOXPtr CSession::CTellib::LoadTelLib()
	{
		IVOXPtr pVox;
		HRESULT hr = pVox.CreateInstance(__uuidof(VOX));
		if (FAILED(hr))
		{
			throw _com_error(hr);
		}
		return pVox;
	}

	/****************************************************************************************************/
	/*                                          CSession                                                */
	/****************************************************************************************************/

	CSession::CSession():
		m_dwState(VMS_WAITING),
		m_pTagVXML(NULL),
		m_bSessionEnd(false),
		m_bPredefinitionInProcess(false),
		m_throwEvt(false),
		m_DialogActive(true),
		m_bThreadStarted(false)
	{
		m_pVox.reset(new CTellib());
	}

	bool CSession::Initialize(const SessionCreateParams& params)
	{
		m_InitMsg = CMessage(params.pInitMsg);
		m_Log = params.pLog;
		m_extraLog = params.pExtraLog;
		m_dwSurrThread = params.dwSurrThread;
		m_VXMLFolders = params.VXMLFolders;
		m_VXMLDefSet = params.VXMLDefSet;
		m_VXMLSystem = params.VXMLSystem;

		if (CParam* param = m_InitMsg.ParamByName(L"LogHttpDispatch"))
		{
			m_isLogHttpDispatch = param->AsBool();
		}

		if (CParam* param = m_InitMsg.ParamByName(L"PrepareDialog"))
			m_DialogActive = false;

		if (CParam* param = m_InitMsg.ParamByName(L"FileName"))
		{
		}
		else if (CParam* param = m_InitMsg.ParamByName(L"FileText"))
		{
		}
		else
		{
			throw std::exception("Cannot find name or text executing vxml file in initial message");
		}

		Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession: Initialize StateDispatcher");
		m_StateDispatcher = std::make_unique<CStateDispatcher>(params.dwSurrThread, false, m_Log);

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession: Initialize EventDispatcher");
		m_EventDispatcher = std::make_unique<common::CEventDispatcher<EventObject, LockGuardBase>>();

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession: Initialize ExecContainer");
		m_ExecContainer = std::make_unique<CExecContainer>(params);

		m_InitMsg.CheckParam(L"ScriptID", CMessage::Int64, CMessage::Mandatory);
		m_InitMsg.CheckParam(L"ParentScriptID", CMessage::Int64, CMessage::Mandatory);
		m_InitMsg.CheckParam(L"parenttype", CMessage::String, CMessage::Mandatory);

		if (const CParam *pMenu = m_InitMsg.ParamByName(L"NextExecuteMenuID"))
		{
			m_ExecContainer->SetCurMenu(pMenu->AsWideStr());
		}

		m_ExecContainer->DbgInit(m_InitMsg);

		CreateSessionVariables(params.pVBSLog);

		if (!m_VXMLSystem.sKeepAliveTimeout.empty())
			m_iKeepAliveTimeout = Utils::toNum<int>(m_VXMLSystem.sKeepAliveTimeout);

		Log(LEVEL_INFO, __FUNCTIONW__, L"\n InitMsg Name: \"%s\"; Dialog ScriptID: \"%s\"", m_InitMsg.GetName(), m_InitMsg[L"ScriptID"].AsWideStr().c_str());

		m_ThreadEvt.Create();
		StartThread();
		m_ThreadEvt.Wait();

		m_Initialized = true;

		return true;
	}

	CSession::~CSession(void)
	{
		Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession::~CSession");

		PostThreadMsg(tmQuit);
		JoinThread();
		m_pVox.reset();
	}

	int CSession::filter(int code, _EXCEPTION_POINTERS *ep)
	{
		if (ep)
		{
			Log(LEVEL_WARNING, __FUNCTIONW__, L"EXCEPTION THROWN : Fatal error, exeption code: 0x%x, data address: 0x%x, instruction address: 0x%x, Program terminated", 
				ep->ExceptionRecord->ExceptionCode,
				ep->ExceptionRecord->ExceptionInformation[1],
				ep->ExceptionRecord->ExceptionAddress);
		}

		return EXCEPTION_EXECUTE_HANDLER;
	}

	class COInitializer
	{
	public:
		COInitializer()
		{
			::CoInitializeEx(NULL, COINIT_MULTITHREADED);
		}

		~COInitializer()
		{
			::CoUninitialize();
		}
	};

	class CScriptEngineLock
	{
	public:
		CScriptEngineLock(CExecContainer* aExecContainer)
			: mExecContainer(aExecContainer)
		{
			if (mExecContainer)
			{
				mExecContainer->InitScriptEngine();
			}
		}

		~CScriptEngineLock()
		{
			if (mExecContainer)
			{
				mExecContainer->DestroyScriptEngine();
			}
		}

	private:
		CExecContainer* mExecContainer = nullptr;
	};

	void CSession::startThread()
	{
		COInitializer initializer;

		m_ExecContainer->InitKeepAlive(m_iKeepAliveTimeout);
		//m_ExecContainer->InitScriptEngine();
		CScriptEngineLock lock(m_ExecContainer.get());

		{
			m_ExecContainer->AddObject(L"session", m_pSessionVariables->AsVariant());
			m_ExecContainer->AddObject(L"application", m_pApplicationVariables->AsVariant());
			m_ExecContainer->AddObject(L"dialog", m_pDialogVariables->AsVariant());

			m_ExecContainer->AddObject2(L"appLoger", m_appLog->AsVariant());

			auto setStatement = [&](const VXMLString& aStatement) 
			{
				VXMLString statement = aStatement + L"=\"" + m_InitMsg[aStatement.c_str()].AsWideStr() + L"\"";
				m_ExecContainer->ExecNormalStatement(statement);
			};

			setStatement(L"ScriptID");
			setStatement(L"Host");
			setStatement(L"parenttype");
		}
		SendClearDigits();

		ChangeState(VMS_BUSY);
		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Worker thread started");
		m_ThreadEvt.Set();

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"LoadPredefinitions");
		VXMLString sPredefine;
		if (CParam *pPredefine = m_InitMsg.ParamByName(L"predefine"))
			sPredefine = pPredefine->AsWideStr();
		else if (CParam *pPredefine = m_InitMsg.ParamByName(L"Predefine"))
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"WARNING: Deprecated param name \"Predefine\"");
			sPredefine = pPredefine->AsWideStr();
		}

		bool bLoadDocumentFailed = false;
		VXMLString sErrorDescription;
		if (!sPredefine.empty())
		{
			try
			{
				bLoadDocumentFailed = m_ExecContainer->LoadPredefinitions(sPredefine);
			}
			catch (const std::exception& aError)
			{
				m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Error: %s", stow(aError.what()).c_str());
				bLoadDocumentFailed = true;
			}
			catch (...)
			{
				m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Unknown exception while loading predefine");
				bLoadDocumentFailed = true;
			}

			if (bLoadDocumentFailed)
			{
				CMessage evt(L"error.badfetch");
				evt[L"reason"] = format_wstring(L"Predefine document \"%s\" was requested to load", sPredefine.c_str()).c_str();
				evt[L"tagname"] = L"Load_predefine";
				evt[L"description"] = L"A fetch of a document has failed";

				DoError(evt, L"SESSION");

				sErrorDescription = format_wstring(L"Failed to load predefines: %s", sPredefine.c_str());
			}
		}

		DocumentPtr mainDoc = nullptr;
		std::wstring fileName;

		if (!bLoadDocumentFailed)
		{
			m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Documents");
			if (CParam* param = m_InitMsg.ParamByName(L"FileName"))
			{
				fileName = param->AsWideStr();
				mainDoc = LoadDocument(m_InitMsg[L"FileName"].AsWideStr(), true);
				if (!mainDoc)
				{
					sErrorDescription = format_wstring(L"Error: Failed to load document: %s", m_InitMsg[L"FileName"].AsWideStr().c_str());
					bLoadDocumentFailed = true;
				}
			}
			else if (CParam* param = m_InitMsg.ParamByName(L"FileText"))
			{
				fileName = L"File text...";
				mainDoc = LoadDocument(m_InitMsg[L"FileText"].AsWideStr(), false);
				if (!mainDoc)
				{
					sErrorDescription = format_wstring(L"Error: Failed to load document: %s", std::wstring(m_InitMsg[L"FileText"].AsString().Left(30)).c_str());
					bLoadDocumentFailed = true;
				}
			}

		}


		/// Execute predefines && document

		if (!bLoadDocumentFailed)
		{
			m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Load all predefines");
			try
			{
				ExecutePredefinitions();
			}
			catch (const std::exception& aError)
			{
				m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Error: %s", stow(aError.what()).c_str());
				bLoadDocumentFailed = true;
				sErrorDescription = format_wstring(L"Failed to execute predefines: %s", stow(aError.what()).c_str());
			}
			catch (...)
			{
				m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Unknown exception while executing predefine");
				bLoadDocumentFailed = true;
				sErrorDescription = L"Unknown exception while loading predefine";
			}
		}

		if (!bLoadDocumentFailed && mainDoc)
		{
			m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"ExecuteDoc");

			Log(LEVEL_FINEST, __FUNCTIONW__, L"Prepare document: \"%s\"", fileName.c_str());
			//Log(LEVEL_FINEST, __FUNCTIONW__, L"Document \"%s\" loaded from cache", sURI.c_str());
			PrepareDoc(mainDoc, fileName);

			if (m_DialogActive)
			{
				m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Execute document immediately");
				ExecuteDoc();
			}
		}
		if (bLoadDocumentFailed)
		{
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Error: %s", sErrorDescription.c_str());
			DoDocumentFailed(sErrorDescription);
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"DoExit()");
			DoExit();
			ChangeState(VMS_ERROR);

			return;
		}

		m_IsStarted = true;
		ChangeState(VMS_READY);

		for (;;)
		{
			//eThreadMsg& msg = m_queue.front();
			eThreadMsg msg = m_queue.pop_front();
			m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"THREAD MSG RECEIVED: %i", msg);
			ChangeState(VMS_WAITING);
			//m_queue.pop();

			OnThreadMsg(msg, NULL);

			ChangeState(VMS_READY);

			if (msg == tmQuit)
			{
				m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"On tmQuit: \"%i\" processing messages denied", m_EventDispatcher->GetQueueSize());				break;
			}
		}

		{
			m_ExecContainer->DoTerminated();
		}

		Sleep(1100);

		ChangeState(VMS_ENDSCRIPT);

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"Worker thread exited");
	}

	void CSession::ThreadProc()
	{
		try
		{
			startThread();
		}
		catch (const std::exception& e)
		{
			Log(LEVEL_WARNING, __FUNCTIONW__, L"ThreadProc exception: \"%s\"", stow(e.what()).c_str());
		}
		catch (_com_error& e)
		{
			Log(LEVEL_WARNING, __FUNCTIONW__, L"ThreadProc exception: \"%s\"", (LPCWSTR)e.Description());
		}
		catch (...)
		{
			Log(LEVEL_WARNING, __FUNCTIONW__, L"ThreadProc unhandled exception caught");
		}
	}

	void CSession::SendClearDigits()
	{
		CMessage msg(L"CLEAR_DIGITS");
		auto parent = GetParentVox();
		msg[L"DestinationAddress"] = parent.first;
		msg[parent.second.c_str()] = parent.first;
		msg[L"CallbackID"] = parent.first;
		msg[L"ScriptID"] = GetScriptID();
		SendAuxMsg(msg);
	}

	DocumentPtr CSession::LoadDocument(const VXMLString& sURI, const bool& bFile)
	{
		DocumentPtr pDoc = nullptr;

		m_sNextDoc = sURI;
		// ObjectId nScriptID = (ObjectId)m_InitMsg[L"ScriptID"].AsUInt64();
		ObjectId iScriptID = m_InitMsg[L"ScriptID"].AsInt64();
		VXMLString sScriptID = format_wstring(L"%08X-%08X", HIDWORD(iScriptID), LODWORD(iScriptID));

		try
		{
			std::unique_ptr<CResourceCache> pResCache;

			Log(LEVEL_FINEST, __FUNCTIONW__, L"Create CResourceCache...");
			try
			{
				pResCache = std::make_unique<CResourceCache>();
			}
			catch (...)
			{
				Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception when created CResourceCache");
				LogExtra(LEVEL_WARNING, __FUNCTIONW__, L"Exception when created CResourceCache");
			}

			if (!pResCache)
			{
				Log(LEVEL_WARNING, __FUNCTIONW__, L"Cannot create CResourceCache");
				LogExtra(LEVEL_WARNING, __FUNCTIONW__, L"Cannot create CResourceCache");

				DoExit();
				//SetState(VMS_ENDSCRIPT);
			}

			if (!pResCache->Ready())
			{
				Log(LEVEL_WARNING, __FUNCTIONW__, L"Failed to CoCreateInstance - CLSID_ResourceCache, hr =  %08x", pResCache->GetHR());
				DoExit();
				//SetState(VMS_ENDSCRIPT);
			}

			Log(LEVEL_FINEST, __FUNCTIONW__, L"Load document: \"%s\"", sURI.c_str());

			SAFEARRAY * pSA = NULL;
			if (SUCCEEDED(/*m_ExecContext.*/pResCache->GetDocument(sURI, sScriptID, bFile, &pSA)))
			{
				Log(LEVEL_FINEST, __FUNCTIONW__, L"LoadDocument: GetDocument successful");

				std::string in_data;
				if (true)
				{
					// get back archive
					char *c1;
					// Get data pointer
					HRESULT hr = ::SafeArrayAccessData(pSA, (void **)&c1);

					int image_size = pSA->rgsabound->cElements;

					in_data.append(c1, image_size);

					::SafeArrayUnaccessData(pSA);
					::SafeArrayDestroy(pSA);
				}

				//std::istringstream is(in_data);
				//boost::archive::binary_iarchive ia(is);
				//// read class state from archive

				//if (true)
				//{
				//	Factory::CCollector parser;
				//	Log(LEVEL_FINEST, __FUNCTIONW__, L"LoadAndExecuteDocument: deserialization, in_data.size(): %i", in_data.size());
				//	ia >> parser;
				//	Log(LEVEL_FINEST, __FUNCTIONW__, L"LoadAndExecuteDocument: new CDocument");

				//	pDoc.reset(new DEBUG_NEW_PLACEMENT CDocument(&parser, m_globals_scope, m_globals_prompts, sURI));
				//	Log(LEVEL_FINEST, __FUNCTIONW__, L"LoadAndExecuteDocument: loaded");
				//}
				//is.clear();
				//in_data.clear();
				//Log(LEVEL_FINEST, __FUNCTIONW__, L"LoadAndExecuteDocument: successful");

				std::istringstream is(in_data);

				Factory_c::CollectorPtr collector;
				{
					cereal::BinaryInputArchive iarchive(is);
					Log(LEVEL_FINEST, __FUNCTIONW__, L"LoadDocument: deserialization, in_data.size()", in_data.size());
					iarchive(collector);
					Log(LEVEL_FINEST, __FUNCTIONW__, L"LoadDocument: new CDocument");
				}

				pDoc = std::make_shared<CDocument>(collector, m_TagFactory, m_globals_scope, m_globals_prompts, sURI);

			}
		}
		//catch (boost::archive::archive_exception& ex)
		//{
		//	Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception loading document [%s]: \"%s\"", sURI.c_str(), VXMLString(bstr_t(ex.what())).c_str());
		//}
		catch (std::exception& e)
		{
			Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception loading document [%s]: \"%s\"", sURI.c_str(), VXMLString(bstr_t(e.what())).c_str());
		}
		catch (std::wstring& e)
		{
			Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception loading document [%s]: \"%s\"", sURI.c_str(), e.c_str());
		}
		catch (_com_error& e)
		{
			Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception loading document [%s]: \"%s\"", sURI.c_str(), (LPCWSTR)e.Description());
		}
		catch (...)
		{
			Log(LEVEL_WARNING, __FUNCTIONW__, L"Unknown exception loading document [%s]", sURI.c_str());
		}

		return pDoc;
	}

	bool CSession::LoadAndExecuteDocument(const VXMLString& sURI,const bool& bFile)
	{
		EventScope saved_globals_scope = m_globals_scope;
		m_globals_scope.remove_if(CFindDocumentScopes(m_sCurDoc));

		DocumentPtr pDoc = LoadDocument(sURI, bFile);

		try
		{
			if (/*pSA &&*/ pDoc.get())
			{
				Log(LEVEL_FINEST, __FUNCTIONW__, L"Execute document: \"%s\"", sURI.c_str());
				//Log(LEVEL_FINEST, __FUNCTIONW__, L"Document \"%s\" loaded from cache", sURI.c_str());
				PrepareDoc(pDoc, sURI);
				return true;
			}

		}
		catch (std::exception& e)
		{
			Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception executing document [%s]: \"%s\"", sURI.c_str(), VXMLString(bstr_t(e.what())).c_str());
			return false;
		}
		catch (std::wstring& e)
		{
			Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception executing document [%s]: \"%s\"", sURI.c_str(), e.c_str());
			return false;
		}
		catch (_com_error& e)
		{
			Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception executing document [%s]: \"%s\"", sURI.c_str(), (LPCWSTR)e.Description());
			return false;
		}
		catch (...)
		{
			Log(LEVEL_WARNING, __FUNCTIONW__, L"Unknown exception executing document [%s]", sURI.c_str());
			return false;
		}

		m_globals_scope = saved_globals_scope;

		//if (!m_ExecContext.pRes->Ready())
		//	m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"Failed to CoCreateInstance - CLSID_ResourceCache");


		////CMessage msg(MSG_LOADDOCUMENT);
		////msg[L"URI"] = sURI;
		////SendAuxMsg(msg);
		//Log(LEVEL_WARNING, __FUNCTIONW__, L"Document \"%s\" was requested to load", sURI.c_str());
		//m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"[%s] Document \"%s\" was requested to load", GetCurDoc().c_str(), sURI.c_str());
		//Log(LEVEL_WARNING, __FUNCTIONW__, L"DoExit()");
		//DoExit();

		//Log(LEVEL_WARNING, __FUNCTIONW__, L"DoExit");
		//DoExit(L"");
		//Log(LEVEL_WARNING, __FUNCTIONW__, L"Document \"%s\" was requested to load", sURI.c_str());
		//m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"[%s] Document \"%s\" was requested to load", GetCurDoc().c_str(), sURI.c_str());

		CMessage evt(L"error.badfetch");
		evt[L"reason"] = format_wstring(L"[%s] Document \"%s\" was requested to load", GetCurDoc().c_str(), sURI.c_str()).c_str();
		evt[L"tagname"] = L"Load_document";
		evt[L"description"] = L"A fetch of a document has failed";

		DoError(evt, L"SESSION");
		return false;
	}

	//void CSession::

	//bool CSession::LoadPredefinitions(const VXMLString& sPredefine)
	//{
	//	//VXMLString sPredefine = m_InitMsg.ParamByName(L"predefine")->AsWideStr();
	//	
	//	ObjectId iScriptID = m_InitMsg[L"ScriptID"].AsInt64();
	//	VXMLString sScriptID = format_wstring(L"%08X-%08X", HIDWORD(iScriptID), LODWORD(iScriptID));


	//	//ResourceCachePtr	pRes;
	//	//try
	//	//{
	//	//	pRes.reset(new DEBUG_NEW_PLACEMENT CResourceCache());
	//	//}
	//	//catch (...)
	//	//{
	//	//	Log(LEVEL_WARNING, __FUNCTIONW__, L"Failed to load CResourceCache");
	//	//	//ExceptionHandler();
	//	//}
	//	//if (!pRes->Ready())
	//	//{
	//	//	Log(LEVEL_WARNING, __FUNCTIONW__, L"Failed to CoCreateInstance - CLSID_ResourceCache, hr =  %08x", pRes->GetHR());
	//	//	//m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"Failed to CoCreateInstance - CLSID_ResourceCache, hr =  %08x", pRes->GetHR());
	//	//	return false;
	//	//}
	//	std::unique_ptr<CResourceCache> pResCache;

	//	Log(LEVEL_FINEST, __FUNCTIONW__, L"Create CResourceCache...");
	//	try
	//	{
	//		pResCache = std::make_unique<CResourceCache>();
	//	}
	//	catch (...)
	//	{
	//		Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception when created CResourceCache");
	//		LogExtra(LEVEL_WARNING, __FUNCTIONW__, L"Exception when created CResourceCache");
	//	}

	//	if (!pResCache)
	//	{
	//		Log(LEVEL_WARNING, __FUNCTIONW__, L"Cannot create CResourceCache");
	//		LogExtra(LEVEL_WARNING, __FUNCTIONW__, L"Cannot create CResourceCache");

	//		DoExit();
	//		//SetState(VMS_ENDSCRIPT);
	//	}

	//	if (!pResCache->Ready())
	//	{
	//		Log(LEVEL_WARNING, __FUNCTIONW__, L"Failed to CoCreateInstance - CLSID_ResourceCache, hr =  %08x", pResCache->GetHR());
	//		DoExit();
	//		//SetState(VMS_ENDSCRIPT);
	//	}


	//	std::vector <VXMLString> _files;

	//	if (1)
	//	{
	//		LPCWSTR p = sPredefine.c_str(), q;
	//		VXMLString sURI;
	//		while (*p)
	//		{
	//			q = wcschr(p, L';');
	//			if (!q)
	//			{
	//				sURI = trim_wstring(p);
	//			}
	//			else
	//			{
	//				sURI = trim_wstring(VXMLString(p, q - p));
	//			}
	//			if (!sURI.empty())
	//			{
	//				_files.push_back(sURI);
	//			}
	//			if (!q)
	//			{
	//				break;
	//			}
	//			p = q + 1;
	//		}

	//	}



	//	//std::wistringstream in(sPredefine.c_str());
	//	//std::istream_iterator<VXMLString, wchar_t, std::char_traits<wchar_t>, ptrdiff_t> first(in), last;
	//	//for (; first != last; ++first)

	//	for each(const VXMLString& _sURI in _files)
	//	{
	//		DocumentPtr pDoc;

	//		Log(LEVEL_FINEST, __FUNCTIONW__, L"Load predefine: \"%s\"", _sURI.c_str());
	//		// LOADING
	//		try
	//		{
	//			SAFEARRAY * pSA = NULL;
	//			if (SUCCEEDED(pResCache->GetDocument(_sURI, sScriptID, true, &pSA)))
	//			{
	//				Log(LEVEL_FINEST, __FUNCTIONW__, L"Load predefine: GetDocument successful");
	//				// get back arhive
	//				char *c1;
	//				// Get data pointer
	//				HRESULT hr = ::SafeArrayAccessData(pSA, (void **)&c1);

	//				int image_size = pSA->rgsabound->cElements;

	//				std::string in_data;
	//				in_data.append(c1, image_size);

	//				::SafeArrayUnaccessData(pSA);
	//				::SafeArrayDestroy(pSA);


	//				//std::istringstream is(in_data);
	//				//boost::archive::binary_iarchive ia(is);
	//				//// read class state from archive
	//				//Factory::CCollector parser;
	//				//Log(LEVEL_FINEST, __FUNCTIONW__, L"Load predefine: deserialization...");
	//				//ia >> parser;
	//				//Log(LEVEL_FINEST, __FUNCTIONW__, L"Load predefine: deserialization successful");

	//				////pDoc = new CDocument(&parser, m_globals_scope, m_globals_prompts);
	//				//pDoc.reset(new DEBUG_NEW_PLACEMENT CDocument(&parser, m_globals_scope, m_globals_prompts, _sURI));
	//				std::istringstream is(in_data);

	//				Factory_c::CollectorPtr collector;
	//				{
	//					cereal::BinaryInputArchive iarchive(is);
	//					Log(LEVEL_FINEST, __FUNCTIONW__, L"LoadDocument: deserialization, in_data.size()", in_data.size());
	//					iarchive(collector);
	//					Log(LEVEL_FINEST, __FUNCTIONW__, L"LoadDocument: new CDocument");
	//				}

	//				pDoc = std::make_shared<CDocument>(collector, m_globals_scope, m_globals_prompts, _sURI);

	//			}
	//		}
	//		//catch (boost::archive::archive_exception& ex)
	//		//{
	//		//	Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception loading predefine document [%s]: \"%s\"", _sURI.c_str(), VXMLString(bstr_t(ex.what())).c_str());
	//		//	return false;
	//		//}
	//		catch (std::exception& e)
	//		{
	//			Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception loading predefine document [%s]: \"%s\"", _sURI.c_str(), VXMLString(bstr_t(e.what())).c_str());
	//			return false;
	//		}
	//		catch (std::wstring& e)
	//		{
	//			Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception loading predefine document [%s]: \"%s\"", _sURI.c_str(), e.c_str());
	//			return false;
	//		}
	//		catch (_com_error& e)
	//		{
	//			Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception loading predefine document [%s]: \"%s\"", _sURI.c_str(), (LPCWSTR)e.Description());
	//			return false;
	//		}
	//		catch (...)
	//		{
	//			Log(LEVEL_WARNING, __FUNCTIONW__, L"Unknown exception loading predefine document [%s]", _sURI.c_str());
	//			return false;
	//		}


	//		//EXECUTE
	//		try
	//		{
	//			if (/*pSA &&*/ pDoc.get())
	//			{
	//				Log(LEVEL_FINEST, __FUNCTIONW__, L"Execute predefine: \"%s\"", _sURI.c_str());

	//				m_bPredefinitionInProcess = true;
	//				pDoc->Execute(&m_ExecContext);
	//				m_bPredefinitionInProcess = false;

	//				m_ExecContext.predefineDocuments.push_back(pDoc);
	//			}
	//			else
	//			{
	//				CMessage evt(L"error.badfetch");
	//				evt[L"reason"] = format_wstring(L"Predefine document \"%s\" was requested to load", _sURI.c_str()).c_str();
	//				evt[L"tagname"] = L"Load_predefine";
	//				evt[L"description"] = L"A fetch of a document has failed";

	//				DoError(evt, L"SESSION");

	//				return false;

	//			}
	//		}
	//		catch (std::exception& e)
	//		{
	//			Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception executing predefine document [%s]: \"%s\"", _sURI.c_str(), VXMLString(bstr_t(e.what())).c_str());
	//			return false;
	//		}
	//		catch (std::wstring& e)
	//		{
	//			Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception executing predefine document [%s]: \"%s\"", _sURI.c_str(), e.c_str());
	//			return false;
	//		}
	//		catch (_com_error& e)
	//		{
	//			Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception executing predefine document [%s]: \"%s\"", _sURI.c_str(), (LPCWSTR)e.Description());
	//			return false;
	//		}
	//		catch (...)
	//		{
	//			Log(LEVEL_WARNING, __FUNCTIONW__, L"Unknown exception executing predefine document [%s]", _sURI.c_str());
	//			return false;
	//		}


	//	}

	//	return true;
	//}

	std::list<DocumentPtr> CSession::LoadPredefinitions2(const VXMLString & sPredefine)
	{
		std::list<DocumentPtr> result;

		ObjectId iScriptID = m_InitMsg[L"ScriptID"].AsInt64();
		VXMLString sScriptID = format_wstring(L"%08X-%08X", HIDWORD(iScriptID), LODWORD(iScriptID));
		std::unique_ptr<CResourceCache> pResCache;

		Log(LEVEL_FINEST, __FUNCTIONW__, L"Create CResourceCache...");
		try
		{
			pResCache = std::make_unique<CResourceCache>();
		}
		catch (...)
		{
			Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception when created CResourceCache");
			LogExtra(LEVEL_WARNING, __FUNCTIONW__, L"Exception when created CResourceCache");
		}

		if (!pResCache)
		{
			Log(LEVEL_WARNING, __FUNCTIONW__, L"Cannot create CResourceCache");
			LogExtra(LEVEL_WARNING, __FUNCTIONW__, L"Cannot create CResourceCache");

			DoExit();
		}

		if (!pResCache->Ready())
		{
			Log(LEVEL_WARNING, __FUNCTIONW__, L"Failed to CoCreateInstance - CLSID_ResourceCache, hr =  %08x", pResCache->GetHR());
			DoExit();
			//SetState(VMS_ENDSCRIPT);
		}

		std::vector <VXMLString> _files;

		Log(LEVEL_WARNING, __FUNCTIONW__, L"Splitting: %s", sPredefine.c_str());
		try
		{
			boost::split(_files, sPredefine, std::bind2nd(std::equal_to<wchar_t>(), ';'));
		}
		catch (std::exception& aError)
		{
			Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception while spitting predefine string [%s]: %s", sPredefine.c_str(), stow(aError.what()).c_str());
			LogExtra(LEVEL_WARNING, __FUNCTIONW__, L"Exception while spitting predefine string [%s]: %s", sPredefine.c_str(), stow(aError.what()).c_str());

		}
		catch (...)
		{
			Log(LEVEL_WARNING, __FUNCTIONW__, L"Unknown exception while spitting predefine string [%s]", sPredefine.c_str());
			Log(LEVEL_WARNING, __FUNCTIONW__, L"Unknown exception while spitting predefine string [%s]", sPredefine.c_str());

		}

		for (const auto& _sURI : _files)
		{
			if (_sURI.empty())
			{
				continue;
			}

			DocumentPtr pDoc;
			Log(LEVEL_FINEST, __FUNCTIONW__, L"Load predefine: \"%s\"", _sURI.c_str());
			// LOADING
			try
			{
				SAFEARRAY * pSA = NULL;
				if (SUCCEEDED(pResCache->GetDocument(_sURI, sScriptID, true, &pSA)))
				{
					Log(LEVEL_FINEST, __FUNCTIONW__, L"Load predefine: GetDocument successful");
					// get back arhive
					char *c1;
					// Get data pointer
					HRESULT hr = ::SafeArrayAccessData(pSA, (void **)&c1);

					int image_size = pSA->rgsabound->cElements;

					std::string in_data;
					in_data.append(c1, image_size);

					::SafeArrayUnaccessData(pSA);
					::SafeArrayDestroy(pSA);


					std::istringstream is(in_data);

					Factory_c::CollectorPtr collector;
					{
						cereal::BinaryInputArchive iarchive(is);
						Log(LEVEL_FINEST, __FUNCTIONW__, L"LoadDocument: deserialization, in_data.size()", in_data.size());
						iarchive(collector);
						Log(LEVEL_FINEST, __FUNCTIONW__, L"LoadDocument: new CDocument");
					}

					pDoc = std::make_shared<CDocument>(collector, m_TagFactory, m_globals_scope, m_globals_prompts, _sURI);

				}
			}
			catch (std::exception& e)
			{
				Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception loading predefine document [%s]: \"%s\"", _sURI.c_str(), VXMLString(bstr_t(e.what())).c_str());
			}
			catch (std::wstring& e)
			{
				Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception loading predefine document [%s]: \"%s\"", _sURI.c_str(), e.c_str());
			}
			catch (_com_error& e)
			{
				Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception loading predefine document [%s]: \"%s\"", _sURI.c_str(), (LPCWSTR)e.Description());
			}
			catch (...)
			{
				Log(LEVEL_WARNING, __FUNCTIONW__, L"Unknown exception loading predefine document [%s]", _sURI.c_str());
			}

			if (!pDoc)
			{
				throw std::runtime_error(std::string("Cannot load predefine: ") + wtos(_sURI));
			}

			result.emplace_back(pDoc);
		}
		return result;
	}

	void CSession::ExecutePredefinitions()
	{
		m_bPredefinitionInProcess = true;
		m_ExecContainer->ExecutePredefinitions([&](const VXMLString& aDocName) 
		{
			this->m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Execute predefine [%s]", aDocName.c_str());
		});
		m_bPredefinitionInProcess = false;
	}

	void CSession::PrepareDoc(DocumentPtr pDoc, const VXMLString& sURI)
	{
		if (!m_sCurDoc.empty())
		{
			// It was old document's scope
			m_ExecContainer->PopScope();
		}
		// Create new document's scope
		m_ExecContainer->PushScope(L"document");

		m_sCurDoc = sURI;
		m_sNextDoc = L"";

		m_ExecContainer->ShortDocument(pDoc);

		PostThreadMsg(tmDocumentPrepared);
	}

	void CSession::ExecuteDoc()
	{
		PostThreadMsg(tmExecuteDoc);
	}

	void CSession::CreateSessionVariables(const EngineLogPtr& vbsLog)
	{
		// Application GLOBAL variables
		m_ExecContainer->PushScope(L"Dialog");

		m_ExecContainer->AddVar(L"utterance",L"", false);
		m_ExecContainer->AddVar(L"inputmode",L"", false);

		//m_pMenus = /*SessionObject*/(new CNameTable());
		////m_ExecContext.pVar->AddVar(L"menus",CComVariant((IDispatch*)m_pMenus.get()), false);
		//m_ExecContext.pVar->AddVar(L"menus",m_pMenus.pdispVal, false);
		m_ExecContainer->AddVar(L"termchar",m_VXMLDefSet.sTermchar.c_str(),false);
		m_ExecContainer->AddVar(L"timeout",m_VXMLDefSet.sTimeout.c_str(),false);
		m_ExecContainer->AddVar(L"WaitTime",m_VXMLDefSet.sWaitTime.c_str(),false);

		m_pGrammars.reset(new DEBUG_NEW_PLACEMENT VXMLGrammar(L""));
		m_ExecContainer->AddVar(L"grammars",m_pGrammars->AsVariant(), false);

		// Session variables
		m_pLocal.reset(new DEBUG_NEW_PLACEMENT CNameTable(L"local"));
		m_pLocal->Add(L"uri",	m_InitMsg.SafeReadParam(L"B", CMessage::String, CComVariant()).Value,	true);

		m_pRemote.reset(new DEBUG_NEW_PLACEMENT CNameTable(L"remote"));
		m_pRemote->Add(L"uri",	m_InitMsg.SafeReadParam(L"A", CMessage::String, CComVariant()).Value,	true);

		m_pProtocol.reset(new DEBUG_NEW_PLACEMENT CNameTable(L"protocol"));
		m_pProtocol->Add(L"name", L"ISUP", true);
		m_pProtocol->Add(L"xml", m_ExecContainer->GetIsupMessage().c_str(), true);
		m_pProtocol->Add(L"q931", CComVariant(m_ExecContainer->GetQ931Ptr()),false);

		m_pConnection.reset(new DEBUG_NEW_PLACEMENT CNameTable(L"connection"));
		m_pConnection->Add(L"local"   ,m_pLocal   ->AsVariant(),false);
		m_pConnection->Add(L"remote"  ,m_pRemote  ->AsVariant(),false);
		m_pConnection->Add(L"protocol",m_pProtocol->AsVariant(),false);


		CMessage pSession(L"session");
		pSession[L"connection" ] = m_pConnection->AsVariant();
		m_pSessionVariables = EventObject(new DEBUG_NEW_PLACEMENT CEvent(pSession));

		CMessage pApplication(L"application");
		pApplication[L"lastresult"] = m_ExecContainer->GetGlobal();
		m_pApplicationVariables = EventObject(new DEBUG_NEW_PLACEMENT CEvent(pApplication));

		m_pDialog.reset(new DEBUG_NEW_PLACEMENT CNameTable(L"dialog"));
		CParam* paramNamelist = m_InitMsg.ParamByName(L"parameters");
		if (!paramNamelist)
			paramNamelist = m_InitMsg.ParamByName(L"namelist");
		if (paramNamelist)
		{
			std::wistringstream in(paramNamelist->AsWideStr());
			std::istream_iterator<VXMLString, wchar_t> first(in), last;
			for (; first != last; ++first)
			{
				//m_ExecContext.pVar->AddVar((*first), m_InitMsg[(*first).c_str()].AsWideStr().c_str(), false);
				m_pDialog->Add((*first), m_InitMsg[(*first).c_str()].AsWideStr().c_str(), false);
			}
		}


		CMessage pDialog(L"dialog");
		pDialog[L"parameters"] = m_pDialog->AsVariant(); // deprecated param
		pDialog[L"namelist"] = m_pDialog->AsVariant();
		m_pDialogVariables = EventObject(new DEBUG_NEW_PLACEMENT CEvent(pDialog));

		//Log(__FUNCTIONW__, L"\n InitMsg Name: \"%s\"; Dialog ScriptID: \"%s\"", m_InitMsg.GetName(), m_InitMsg[L"ScriptID"].AsWideStr().c_str());
		ObjectId nScriptID = (ObjectId)m_InitMsg[L"ScriptID"].AsUInt64();
		m_InitMsg.Remove(L"ScriptID");
		m_InitMsg[L"ScriptID"] = nScriptID;
		//Log(__FUNCTIONW__, L"\n InitMsg Name: \"%s\"; Dialog ScriptID: \"%s\"", m_InitMsg.GetName(), m_InitMsg[L"ScriptID"].AsWideStr().c_str());
		m_InitMsg[L"ScriptID"] = (ObjectId)m_InitMsg[L"ScriptID"].AsInt64();
		//Log(__FUNCTIONW__, L"\n InitMsg Name: \"%s\"; Dialog ScriptID: \"%s\"", m_InitMsg.GetName(), m_InitMsg[L"ScriptID"].AsWideStr().c_str());

		if (CParam* param = m_InitMsg.ParamByName(L"CallID"))
		{
			Log(LEVEL_FINEST, __FUNCTIONW__, L"\n InitMsg Name: \"%s\"; CallID: \"%s\"", m_InitMsg.GetName(), m_InitMsg[L"CallID"].AsWideStr().c_str());
		}
		if (CParam* param = m_InitMsg.ParamByName(L"VoxID"))
		{
			Log(LEVEL_FINEST, __FUNCTIONW__, L"\n InitMsg Name: \"%s\"; VoxID: \"%s\"", m_InitMsg.GetName(), m_InitMsg[L"VoxID"].AsWideStr().c_str());
		}

		m_appLog.reset(new DEBUG_NEW_PLACEMENT CAppLog(vbsLog));
	}


	void CSession::PostThreadMsg(eThreadMsg msg, void* pParam)
	{
		Log(LEVEL_FINEST, __FUNCTIONW__, L"THREAD MSG: %i", msg);
		m_queue.push(msg);
		//__super::PostMessage(msg, 0, (LPARAM)pParam);
	}

	void CSession::SendAuxMsg(const CMessage& msg) const
	{
		m_ExecContainer->PostAuxMessage(msg);

		if (msg == L"VXML2AC_sendComment")
		{
			return;
		}

		//Log(LEVEL_INFO, __FUNCTIONW__, L"OUT %s", msg.Dump().c_str());
		common::DumpMessage(LEVEL_INFO, L"OUT", msg, m_Log);
	}

	void CSession::OnThreadMsg(eThreadMsg msg, void* pParam)
	{
		Log(LEVEL_FINEST, __FUNCTIONW__, L"Incoming \"%s\" (%i) thread message", ThreadMsgToString(msg).c_str(), int(msg));
		
		if (!this->IsActive())
		{
			Log(LEVEL_FINEST, __FUNCTIONW__, L"Cannot execute message, current session does not active");
			return;
		}

		switch (msg)
		{
		case tmExecuteDoc:
			{

				Log(LEVEL_FINEST,__FUNCTIONW__, L"Executing document \"%s\"", m_sCurDoc.c_str());
				bool bDocRet = m_ExecContainer->Execute();
				m_DialogActive = true;
				if (bDocRet)
					Log(LEVEL_FINEST, __FUNCTIONW__, L"Execution of document \"%s\" completed", m_sCurDoc.c_str());
				else
					Log(LEVEL_FINEST, __FUNCTIONW__, L"Execution of document \"%s\" failed", m_sCurDoc.c_str());

				break;
			}
		case tmDocumentPrepared:
			{
				DoDocumentPrepared();
				break;
			}
		case tmSetEvent:
			{
				ProcessEvents();
				break;
			}
		}
	}

	DWORD CSession::GetState() const
	{
		return m_StateDispatcher->GetState();
	}

	void CSession::ChangeState(DWORD dwState)
	{
		m_StateDispatcher->ChangeState(dwState);
	}

	void CSession::SetState(DWORD dwState)
	{
		m_StateDispatcher->SetState(dwState);
	}

	void CSession::SendQueuedEvent(int nIndex)
	{
	}

	void CSession::ProcessUnhandledEvent(EventPtr pEvt)
	{
		Log(LEVEL_INFO, __FUNCTIONW__, L"Unhandled event \"%s\"", pEvt->GetName().c_str());

		if (pEvt->MatchMask(L"Invalid"))
		{
			Log(LEVEL_FINEST, __FUNCTIONW__, L"Executing DoDialogError()");
			DoDialogError(format_wstring(L"Unhandled event \"%s\"", pEvt->GetName().c_str()));
		}
	}
	void CSession::ExceptionHandler()
	{
		CMessage evt(L"error.unrecoverable");
		evt[L"tagname"]	= L"unrecoverable";
		evt[L"description"] = L"Unexpected exception caught";

		try
		{
			throw;
		}
		catch (CMessage& e)
		{
			evt += e;
		}
		catch (std::exception& e)
		{
			evt[L"reason"] = e.what();
		}
		catch (std::wstring& e)
		{
			evt[L"reason"] = e;
		}
		catch (_com_error& e)
		{
			evt[L"reason"] = (LPCWSTR)e.Description();
		}
		catch (...)
		{
			evt[L"reason"] = L"Unknown unhandled exception";
		}

		DoError(evt, L"SESSION");
	}
	void CSession::EmitEvent(const CMessage& evt)
	{
		EventObject pEvt(new DEBUG_NEW_PLACEMENT CEvent(evt));
		if (pEvt->MatchMask(L"error.*"))
		{
			m_EventDispatcher->PushFront(pEvt);
		}
		else
		{
			m_EventDispatcher->PushBack(pEvt);
		}

		common::DumpMessage(LEVEL_INFO, L"Emitting event", evt, m_Log);
		PostThreadMsg(tmSetEvent);
	}

	void CSession::DoExit(VXMLString sNamelist)
	{
		DoExit(L"END_DIALOG", sNamelist, L"no error");
	}
	void CSession::DoTerminated(const VXMLString& sNamelist, const VXMLString& sHandlerName)
	{
		CMessage stop_channel_msg(L"STOP_CHANNEL");
		SendEvent(stop_channel_msg, GetParentCallID());

		Log(LEVEL_FINEST, __FUNCTIONW__, L"Set terminated handle: %s", sHandlerName.c_str());
		m_TerminatedNamelist = sNamelist;
		m_TerminatedHandler = sHandlerName;
		m_bTerminated = true;
		DoExit();
	}

	void CSession::DoTerminated()
	{
		if (m_bTerminated)
		{
			if (!m_TerminatedHandler.empty())
			{
				Log(LEVEL_FINEST, __FUNCTIONW__, L"Execute terminated handle: %s", m_TerminatedHandler.c_str());
				m_ExecContainer->ExecScript(m_TerminatedHandler);
			}
			else
			{
				Log(LEVEL_FINEST, __FUNCTIONW__, L"no terminated handle");
			}

			DoExit(L"TERMINATED_DIALOG", m_TerminatedNamelist, L"no error");
		}
		Stop();
	}

	void CSession::DoDialogError(const VXMLString& sErrorDescription)
	{
		DoExit(L"error.dialog", L"", sErrorDescription);
	}

	void CSession::Stop()
	{
		m_bSessionEnd = true;
		::Sleep(100);
	}

	void CSession::DoExit()
	{
		ChangeCallCtrl(m_InitMsg.ParamByName(L"ScriptID")->AsInt64(), GetParentScriptID());

		Stop();
		Log(LEVEL_INFO, __FUNCTIONW__, L"Post message \"tmQuit\"");
		PostThreadMsg(tmQuit);
	}
	void CSession::DoExit(const VXMLString& sConnectedWithMessageName, const VXMLString& sNamelist, const VXMLString& sErrorDescription)
	{
		Stop();

		CMessage msg(sConnectedWithMessageName.c_str());
		msg[L"targettype" ] = m_InitMsg[L"parenttype"].Value;
		msg[L"reason"] = sErrorDescription.c_str();

		if (!sNamelist.empty())
		{
			Log(LEVEL_FINEST, __FUNCTIONW__, L"namelist == \"%s\"", sNamelist.c_str());

			msg[L"namelist"] = sNamelist;

			AddNamelistToMsg(msg);
		}
		SendEvent(msg, GetParentScriptID());
		DoExit();
	}

	void CSession::DoError(CMessage &evt, const VXMLString& _subSystem)
	{
		m_throwEvt = true;
		VXMLString sError, sTag, sDescription;
		ObjectId iScriptID = 0;
		if (CParam *pParam = evt.ParamByName(L"reason"))
			sError = pParam->AsWideStr();
		if (CParam *pParam = evt.ParamByName(L"tagname"))
			sTag = pParam->AsWideStr();
		if (CParam *pParam = evt.ParamByName(L"description"))
			sDescription = pParam->AsWideStr();
		if (CParam *pParam = m_InitMsg.ParamByName(L"ScriptID"))
			iScriptID = pParam->AsInt64();

		VXMLString sScriptID = format_wstring(L"%08X-%08X", HIDWORD(iScriptID), LODWORD(iScriptID));

		evt[L"event_name"] = evt.GetName();
		Log(LEVEL_WARNING, __FUNCTIONW__, L"Push error \"%s - %s\" to stack", sTag.c_str(), sError.c_str());
		m_extraLog->Log(LEVEL_WARNING, __FUNCTIONW__, L"[%s] Push error \"%s - %s\" to stack", GetCurDoc().c_str(), sTag.c_str(), sError.c_str());
		if (0 == CAtlString(_subSystem.c_str()).CompareNoCase(L"VBENGINE"))
			m_extraLog->LogLarge(LEVEL_WARNING, __FUNCTIONW__, L"Script source: %s", m_ExecContainer->ScriptSource().c_str());

		SendAuxMsg(AlarmMsg(L"VXML", sScriptID, _subSystem, evt.GetName(), sDescription, sError, -1111));

		this->EmitEvent(evt);
	}

	void CSession::DoEvent(const CMessage &evt)
	{
		m_throwEvt = true;
		VXMLString sEvent = evt[L"event_name"].AsWideStr();
		Log(LEVEL_INFO, __FUNCTIONW__, L"Push event \"%s\" to stack", sEvent.c_str());
		this->EmitEvent(evt);
	}

	void CSession::DoNext(CMessage &evt)
	{
		m_throwEvt = true;
		Log(LEVEL_INFO, __FUNCTIONW__, L"Push \"%s\" to stack", evt.GetName());
		this->EmitEvent(evt);
	}

	void CSession::DoDocumentPrepared()
	{
		CMessage msg(L"VXML_RUN_SCRIPT_OK");
		msg[L"ScriptID"   ]    = m_InitMsg[L"ScriptID"].AsWideStr();
		msg[L"targettype" ]    = m_InitMsg[L"parenttype"].Value;
		msg[L"CallbackID"]     = m_InitMsg[L"CallbackID"].Value;
		msg[L"ParentScriptID"] = m_InitMsg[L"ParentScriptID"].Value;
		if(m_InitMsg.ParamByName(L"PrepareDialog"))
			msg[L"PrepareDialog"] = m_InitMsg[L"PrepareDialog"].Value;
		SendEvent(msg,GetParentScriptID());
	}

	void CSession::DoDocumentFailed(const VXMLString& _error_description)
	{
		CMessage msg(L"VXML_RUN_SCRIPT_FAILED");
		msg[L"ScriptID"] = m_InitMsg[L"ScriptID"].AsWideStr();
		msg[L"targettype"] = m_InitMsg[L"parenttype"].Value;
		msg[L"CallbackID"] = m_InitMsg[L"CallbackID"].Value;
		msg[L"ParentScriptID"] = m_InitMsg[L"ParentScriptID"].Value;
		if (m_InitMsg.ParamByName(L"PrepareDialog"))
			msg[L"PrepareDialog"] = m_InitMsg[L"PrepareDialog"].Value;
		msg[L"ErrorDescription"] = _error_description.c_str();
		
		SendEvent(msg, GetParentScriptID());
	}

	void CSession::CancelEvent(const VXMLString& aName, const ObjectId aCallBackId)
	{
		m_EventDispatcher->GetEvent(aName, aCallBackId);
	}

	eWaitEvents CSession::WaitEvent(CMessage* pPostmsg, 
		const VXMLString& sEvents, 
		const VXMLString& sError, 
		CMessage& waitmsg)
	{
		MSG thread_msg = {0};

		Log(LEVEL_INFO, __FUNCTIONW__, L"Waiting for event \"%s\"...", sEvents.c_str());

		ObjectId nParentID = GetParentCallID();

		if (pPostmsg)
		{
			ObjectId nParentID = 0;
			if (CParam* param = m_InitMsg.ParamByName(L"CallID"))
			{
				nParentID = m_InitMsg[L"CallID"].Value.llVal;
				(*pPostmsg)[L"CallID"] = nParentID;
			}
			if (CParam* param = m_InitMsg.ParamByName(L"VoxID"))
			{
				nParentID = m_InitMsg[L"VoxID"].Value.llVal;
				(*pPostmsg)[L"VoxID"] = nParentID;
			}

			(*pPostmsg)[L"CallbackID"]			= nParentID;
			(*pPostmsg)[L"ScriptID"]			= m_InitMsg[L"ScriptID"].Value;
			SendAuxMsg(*pPostmsg);
		}

		for (;;)
		{
			eThreadMsg msg = m_queue.pop_front();
			Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession::WaitEvent GetMessage is \"%d\"...", msg/*thread_msg.message*/);
			
			if (msg == tmQuit)
			{
				Log(LEVEL_FINEST, __FUNCTIONW__, L"thread_msg.message == tmQuit");
				return WE_STOPTHREAD;
			}

			bool bEventExist = false;
			EventObject evt = m_EventDispatcher->GetEvent(sEvents, nParentID);
			if (evt.get())
			{
				waitmsg = evt->ToMsg();
				bEventExist = true;
			}

			if (bEventExist)
			{
				break;
			}
			ProcessFirstEvent();
		}

		return WE_SUCCESS;
	}

	eWaitEvents CSession::WaitMultipleEvent(CMessage* pPostmsg, 
		WaitingEvents& sEvents, 
		const VXMLString& sError, 
		CMessage& waitmsg)
	{
		MSG thread_msg = {0};

		WaitingEvents::const_iterator cit = sEvents.begin();
		for (;cit!=sEvents.end();++cit)
			Log(LEVEL_FINEST,__FUNCTIONW__, L"Waiting for event \"%s\"...", cit->c_str());

		ObjectId nParentID = GetParentCallID();

		if (pPostmsg)
		{
			ObjectId nParentID = 0;
			if (CParam* param = m_InitMsg.ParamByName(L"CallID"))
			{
				nParentID = m_InitMsg[L"CallID"].Value.llVal;
				(*pPostmsg)[L"CallID"] = nParentID;
			}
			if (CParam* param = m_InitMsg.ParamByName(L"VoxID"))
			{
				nParentID = m_InitMsg[L"VoxID"].Value.llVal;
				(*pPostmsg)[L"VoxID"] = nParentID;
			}

			(*pPostmsg)[L"CallbackID"]			= nParentID;
			(*pPostmsg)[L"ScriptID"]			= m_InitMsg[L"ScriptID"].Value;
			
			if (CParam* param = pPostmsg->ParamByName(L"DestinationAddress"))
			{

			}
			else
			{
				(*pPostmsg)[L"DestinationAddress"]	= nParentID;
			}
			
			SendAuxMsg(*pPostmsg);
		}


		for (;;)
		{
			eThreadMsg msg = m_queue.pop_front();
			if (msg == tmQuit)
			{
				Log(LEVEL_FINEST, __FUNCTIONW__, L"thread_msg.message == tmQuit");
				return WE_STOPTHREAD;
			}

			Log(LEVEL_FINEST, __FUNCTIONW__, L"WME THREAD MSG RECEIVED: %i", msg);

			bool bEventExist = false;
			for (const auto& msgName : sEvents)
			{
				EventObject evt = m_EventDispatcher->GetEvent(msgName, nParentID);
				if (evt.get())
				{
					waitmsg = evt->ToMsg();
					bEventExist = true;
					break;
				}
			}

			if (bEventExist)
			{
				break;
			}
			ProcessFirstEvent();
		}

		return WE_SUCCESS;
	}

	void CSession::ProcessFirstEvent()
	{
		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession::ProcessEvents(): m_EvtQ size \"%i\"", m_EventDispatcher->GetQueueSize());
		EventObject evt = m_EventDispatcher->GetFirstEvent();
		if (!evt.get())
		{
			return;
		}

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession::ProcessEvents(): pEvt \"%s\"", evt ? L"COOL" : L"NULL");
		ProcessEvent(evt);
	}

	void CSession::ProcessEvents()
	{
		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession::ProcessEvents(): m_EvtQ size \"%i\"", m_EventDispatcher->GetQueueSize());

		EventObject evt = m_EventDispatcher->GetFirstEvent();
		while (evt.get())
		{
			ProcessEvent(evt);
			evt = m_EventDispatcher->GetFirstEvent();
		}
	}

	void CSession::ProcessEvent(EventPtr pEvt)
	{
		if (!pEvt.get())
		{
			Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession::ProcessEvent: event NULL!!! ");
			throw VXMLString (L"CSession::ProcessEvent: event NULL!!!");
		}
		Log(LEVEL_INFO, __FUNCTIONW__, L"Processing event \"%s\"", pEvt->GetName().c_str());
		m_throwEvt = false;

		if (m_pTagVXML)
		{
			if (pEvt->MatchMask(L"error.*") && !m_ExecContainer->GetCurMenu().empty())
			{
				// clear stack
				//VAR * pReason = pEvt->Find(L"reason");
				//VXMLString sReason = pReason?pReason->vValue.bstrVal:L"unknown";
				//Log(__FUNCTIONW__, L"Executing error \"%s\" due to fallow reason: \"%s\"" , pEvt->GetName().c_str(), sReason.c_str());
				//CLocker lock(m_CS);
				//m_EvtQ.remove_if(CReplaceAllEventsExceptError(pEvt));
				m_pTagVXML->SetEvent(m_ExecContainer.get(), pEvt);
			}
			else if (pEvt->MatchMask(L"event")) 
			{
				m_pTagVXML->SetEvent(m_ExecContainer.get(),pEvt);
			}
			else if (pEvt->MatchMask(L"LoadNewDocument"))
			{
				//Sleep(10000);
				if (m_bSessionEnd)
				{
					Log(LEVEL_FINEST, __FUNCTIONW__, L"LoadNewDocument unavailable: session's ended");
				}
				else
				{
					auto id = pEvt->GetSafeValue(L"id");
					m_ExecContainer->SetCurMenu(!id.IsEmpty() ? id.vValue.bstrVal : L"");
					auto file = pEvt->GetSafeValue(L"file");

					if (!file.IsEmpty())
					{
						if (!LoadAndExecuteDocument(file.vValue.bstrVal, true))
						{
							Log(LEVEL_WARNING, __FUNCTIONW__, L"Error: Failed to load document");
							Log(LEVEL_WARNING, __FUNCTIONW__, L"DoDialogError()");
							//DoExit(L"");
							DoDialogError(format_wstring(L"Failed to load document=\"%s\"", VXMLString(file.vValue.bstrVal).c_str()));
						}
						ExecuteDoc();
					}
					else
					{
						auto url  = pEvt->GetSafeValue(L"url");
						auto text = pEvt->GetSafeValue(L"FileText");
						if (!url.IsEmpty() && !text.IsEmpty())
						{
							if (!LoadAndExecuteDocument(text.vValue.bstrVal,false))
							{
								Log(LEVEL_WARNING, __FUNCTIONW__, L"Error: Failed to load document");
								Log(LEVEL_WARNING, __FUNCTIONW__, L"DoDialogError()");
								//DoExit(L"");
								DoDialogError(format_wstring(L"Failed to load document=\"%s\"", VXMLString(CAtlString(text.vValue.bstrVal).Left(50)).c_str()));
							}
							ExecuteDoc();
						}
					}
				}
			}
			else if (m_pTagVXML->SetMenu(m_ExecContainer.get(), pEvt))
			{
				//ProcessUnhandledEvent(pEvt);
			}
			else if (pEvt->MatchMask(L"external.*")) 
			{
				m_pTagVXML->SetExternal(m_ExecContainer.get(), pEvt);
			}
			else
			{
				ProcessUnhandledEvent(pEvt);
			}

		}
		else
		{
			ProcessUnhandledEvent(pEvt);
		}
	}
	void CSession::OnAuxMsg(CMessage& msg)
	{
		common::DumpMessage(LEVEL_FINEST, L"Processing message", msg, m_Log);
		try
		{
			if (m_bSessionEnd)
			{
				Log(LEVEL_FINEST, __FUNCTIONW__, L"Unable to set message: session's ended");
				return;
			}

			DbgEvent(msg);
			TranslateMsg(msg);
		}
		catch (...)
		{
			ExceptionHandler();
		}
	}

	void CSession::TranslateMsg(CMessage& msg)
	{
		// Connection specific messages
		if (CParam* param = msg.ParamByName(L"AuxEvent"))
		{
			return; // massage has already send
		}

		// Connection specific messages
		else if (//msg == L"PLAY_WAV_ACK"           ||
		    //msg == L"DIGIT_GOT"              ||
			msg == L"MAIN"					 ||
			msg == L"DROPED"				 ||
			msg == L"GET_DIGITS_ACK"         ||
			msg == L"STOP_VIDEO_ACK"         ||
			msg == L"STOP_CHANNEL_ACK"       ||
			//msg == L"STOP_CHANNEL_COMPLETED" ||
			msg == L"CLEAR_DIGITS_ACK"       ||
			msg == L"EXTENSION"              ||
			//msg == L"KEEP_ALIVE"             ||
			msg == L"REC_WAV_ACK"   		   )
		{
			// just skip unuseful messages 
		}
		else if (msg == L"PLAY_WAV_ACK")
		{
			if(CParam* param = msg.ParamByName(L"ErrorCode"))
			{
				msg.SetName(L"PLAY_WAV_COMPLETED");
				MakeEvent_MenuWav(msg);
			}

		}
		else if (msg == L"KEEP_ALIVE")
		{
			if (Utils::toLower<wchar_t>(m_InitMsg[L"parenttype"].AsWideStr()) == L"ccxml")
			{
				auto counter = m_ExecContainer->IncreaseDeadCounter();
				Log(LEVEL_FINEST, __FUNCTIONW__, L"Increase dead counter: %i", counter);

				if (counter > DEADTIMER_LIMIT)
				{
					Log(LEVEL_WARNING, __FUNCTIONW__, L"Dead counter has reached the limit! Terminate dialog");
					CMessage terminate(L"TERMINATE");
					terminate[L"immediate"] = true;
					SendEvent(terminate, m_InitMsg[L"ScriptID"].AsInt64());
				}
			}
		}
		else if (msg == L"KEEP_ALIVE_ACK")
		{
			if (Utils::toLower<wchar_t>(m_InitMsg[L"parenttype"].AsWideStr()) == L"ccxml")
			{
				m_ExecContainer->ResetDeadTimer();
			}
		}
		else if (msg == L"PLAY_WAV_COMPLETED")
		{
			MakeEvent_MenuWav(msg);
		}
		else if (msg == L"GET_DIGITS_COMPLETED")
		{
			MakeEvent_MenuDigits(msg);
		}
		else if (msg == L"CLEAR_DIGITS_COMPLETED")
		{
			MakeEvent_ClearDigits(msg);
		}
		else if (msg == L"DIGIT_GOT")
		{
			auto curDTMF = m_ExecContainer->GetValue(L"bargein");
			if (!curDTMF.IsEmpty() && curDTMF.vValue.boolVal == FALSE)
			{
				CMessage stop_channel_msg(L"STOP_CHANNEL");
				SendEvent(stop_channel_msg, GetParentCallID());

				m_ExecContainer->DelVar(L"bargein");
			}
		}
		else if (msg == L"DISCONNECTED")
		{
			SendEvent(msg, m_InitMsg[L"ParentScriptID"].AsInt64());

			if (!m_IsStarted)
			{
				DoExit();
			}
			else
			{
				CMessage msg(L"error.connection.disconnect.hangup");
				msg[L"event_name"] = L"connection.disconnect.hangup";
				DoEvent(msg);
			}
		}
		else if (msg == L"PLAY_VIDEO_ACK")
		{
			MakeEvent_VideoAcknowledge(msg);
		}
		else if (msg == L"PLAY_VIDEO_COMPLETED")
		{
			MakeEvent_VideoCompleted(msg);
		}
		else if (msg == L"STOP_VIDEO_COMPLETED")
		{
			MakeEvent_StopVideoCompleted(msg);
		}
		else if (msg == L"STOP_CHANNEL_COMPLETED")
		{
			MakeEvent_StopChannelCompleted(msg);
		}
		else if (msg == L"REC_WAV_COMPLETED")
		{
			MakeEvent_RecWav(msg);
		}
		else if (msg == L"LAUNCHER")
		{
			if (!m_DialogActive)
			{
				ExecuteDoc();
			}
		}
		else if (msg == L"TERMINATE")
		{
			if (CParam* param = msg.ParamByName(L"CallbackID"))
			{
				CParam* pTerminateImmediately = msg.ParamByName(L"immediate");
				if (pTerminateImmediately && pTerminateImmediately->AsBool())
				{
					ChangeCallCtrl(m_InitMsg.ParamByName(L"ScriptID")->AsInt64(), GetParentScriptID());
					Log(LEVEL_FINEST, __FUNCTIONW__, L"TERMINATE tmQuit");
					PostThreadMsg(tmQuit);
				}
				else
				{
					VXMLString namelist, handler;
					if (CParam* param = msg.ParamByName(L"namelist"))
					{
						namelist = param->AsWideStr();
					}
					if (CParam* param = msg.ParamByName(L"handler"))
					{
						handler = param->AsWideStr();
					}


					DoTerminated(namelist, handler);
				}
			}
				
		}
		else if  ( (msg == L"VXML_RUN_SCRIPT_OK") || 
			(msg == L"VXML_RUN_SCRIPT_FAILED") || 
			(msg == L"END_DIALOG") || 
			(msg == L"SC2V_RUN_FUNCTION_ACK") ||
			//(msg == L"SC2V_RUN_FUNCTION_FAILED") ||
			(msg == L"SC2V_RUN_FUNCTION_COMPLETED") 
			)
		{
			EmitEvent(msg);
		}
		// Document specific messages
		else
		{
			if (CParam *pTargetType = msg.ParamByName(L"targettype"))
			{
				VXMLString sTargetType = pTargetType->AsWideStr();
				if (sTargetType == L"dialog")
				{
					EmitEvent(msg);
					return;
				}
			}
			CMessage evt((VXMLString(L"external.") + msg.Name).c_str());
			evt[L"event_name"] = msg.Name;
			EmitEvent(evt += msg);
		}

	}

	CMessage CSession::MakeCommonEvent(CMessage& msg, VXMLString message)
	{
		CMessage evt((message).c_str());
		return evt;
	}

	void CSession::MakeEvent_MenuWav(CMessage& msg)
	{
		CMessage evt(MakeCommonEvent(msg,L"wav.completed"));
		evt[L"CallbackID"]			= msg[L"CallbackID"].Value;
		evt[L"TerminationReason"]	= msg[L"TerminationReason"].Value;

		if (const CParam* param = msg.ParamByName(L"DigitsBuffer"))
		{
			//ProccedBufferComment(m_ExecContainer->GetCurMenu(), param->AsWideStr());

			evt[L"DigitsBuffer"] = param->AsWideStr().c_str();
		}
		else
		{
			evt[L"DigitsBuffer"] = L"";
		}

		EmitEvent(evt);
	}

	void CSession::MakeEvent_MenuDigits(CMessage& msg)
	{
		CMessage evt(MakeCommonEvent(msg,L"get_digits.completed"));
		evt[L"CallbackID"]			= msg[L"CallbackID"].Value;
		evt[L"TerminationReason"]	= msg[L"TerminationReason"].Value;
		
		if (const CParam* param = msg.ParamByName(L"DigitsBuffer"))
		{
			//ProccedBufferComment(m_ExecContainer->GetCurMenu(), param->AsWideStr());

			evt[L"DigitsBuffer"] = param->AsWideStr().c_str();
		}
		else
		{
			evt[L"DigitsBuffer"] = L"";
		}

		EmitEvent(evt);
	}

	void CSession::MakeEvent_ClearDigits(CMessage& msg)
	{
		CMessage evt(MakeCommonEvent(msg,L"clear_digits.completed"));
		evt[L"CallbackID"]			= msg[L"CallbackID"].Value;
		EmitEvent(evt);
	}

	void CSession::MakeEvent_SubDialogEnd(CMessage& msg)
	{
		CMessage evt(MakeCommonEvent(msg,L"SUB_DIALOG_END"));
		evt[L"CallbackID"]			= msg[L"CallbackID"].Value;
		evt[L"ScriptID"]			= msg[L"ScriptID"].Value;
		if (CParam* paramName  = msg.ParamByName(L"namelist"))
		{
			evt[L"namelist"] = msg[L"namelist"].Value;
			VXMLString sNameList = paramName->AsWideStr();
			LPCWSTR p = sNameList.c_str(), q;
			VXMLString sParamName;

			while (*p)
			{
				q = wcschr(p, L' ');
				if (!q)
				{
					sParamName = trim_wstring(p);
				}
				else
				{
					sParamName = trim_wstring(VXMLString(p, q - p));
				}
				if (!sParamName.empty())
				{
					evt[sParamName.c_str()] = m_ExecContainer->ExprEvalToStr(sParamName).c_str();
				}
				if (!q)
				{
					break;
				}
				p = q + 1;
			}
		}
		EmitEvent(evt);
	}

	// acknowledge 
	void CSession::MakeEvent_VideoAcknowledge(CMessage& msg)
	{
		CMessage evt(MakeCommonEvent(msg,L"play_video.acknowledge"));
		evt[L"CallbackID"]			= msg[L"CallbackID"].Value;
		evt[L"ScriptID"]			= msg[L"ScriptID"].Value;
		EmitEvent(evt);
	}

	void CSession::MakeEvent_VideoCompleted(CMessage& msg)
	{
		CMessage evt(MakeCommonEvent(msg,L"play_video.completed"));
		evt[L"CallbackID"]			= msg[L"CallbackID"].Value;
		evt[L"ScriptID"]			= msg[L"ScriptID"].Value;
		EmitEvent(evt);
	}

	void CSession::MakeEvent_StopVideoCompleted(CMessage& msg)
	{
		CMessage evt(MakeCommonEvent(msg,L"stop_video.completed"));
		evt[L"CallbackID"]			= msg[L"CallbackID"].Value;
		evt[L"ScriptID"]			= msg[L"ScriptID"].Value;
		EmitEvent(evt);
	}

	void CSession::MakeEvent_StopChannelCompleted(CMessage& msg)
	{
		CMessage evt(MakeCommonEvent(msg,L"stop_channel.completed"));
		evt[L"CallbackID"]			= msg[L"CallbackID"].Value;
		evt[L"ScriptID"]			= msg[L"ScriptID"].Value;
		EmitEvent(evt);
	}

	void CSession::MakeEvent_RecWav(CMessage& msg)
	{
		CMessage evt(MakeCommonEvent(msg,L"rec_wav.completed"));
		evt[L"CallbackID"]			= msg[L"CallbackID"].Value;
		evt[L"TerminationReason"]	= msg[L"TerminationReason"].Value;
		evt[L"DigitsBuffer"]		= msg[L"DigitsBuffer"].Value;
		EmitEvent(evt);
	}

	//CNameTable* CSession::AddMenu(CMessage& msg, const VXMLString& sFirstMenu)
	//{
	//	if (!m_sCurMenu.empty())
	//	{
	//		int test = 0;
	//	}
	//	m_sCurMenu = sFirstMenu;
	//	return NULL;
	//}


	SessionGrammar CSession::AddGrammar(VXMLString name, VXMLString path)
	{
		SessionGrammar pGrammar(new VXMLGrammar(path));

		m_pGrammars->Add(name, pGrammar->AsVariant());

		return pGrammar;
	}

	void CSession::SetVXML(CTag_vxml* pVXML)
	{
		m_pTagVXML = pVXML;
	}

	VXMLString CSession::FindGrammarEvent(const VXMLString & aGrammarName, const VXMLString & aCurDtmf)
	{
		VXMLString filledEvent;
		if (VAR* pVar = m_pGrammars->Find(aGrammarName))
		{
			GrammarDispatch* bridge = static_cast<GrammarDispatch *>(pVar->vValue.pdispVal);
			if (bridge)
			{
				filledEvent = bridge->GetSafeValue<VXMLString>(aCurDtmf);
			}
		}

		return filledEvent;
	}

	void CSession::AcceptEvent(const CMessage &msg, bool post)
	{
		if (m_throwEvt)
			return;
		if (m_bPredefinitionInProcess)
			return;
		if (m_bSessionEnd)
		{
			Log(LEVEL_FINEST, __FUNCTIONW__, L"Accept canceled: session's ended");
			return;
		}

		EventPtr pEvt(new DEBUG_NEW_PLACEMENT CEvent(msg));
		m_EventDispatcher->PushBack(pEvt);

		if (post)
		{
			PostThreadMsg(tmSetEvent);
		}
	}

	CMessage CSession::CreateStatisticMsg(const VXMLString& formID)const
	{
		CMessage postmsg;
		// format date
		wchar_t date[256];
		time_t  tt;
		time(&tt);
		wcsftime(date, 256, L"%Y-%m-%d %H:%M:%S", localtime(&tt));
		DWORD dBufSize = 1024;
		wchar_t cname[1024];
		ZeroMemory(cname, sizeof(cname));
		GetComputerName(cname, &dBufSize);

		postmsg[L"datetime"] = date;
		postmsg[L"servername"] = cname;
		postmsg[L"ScriptID"] = m_InitMsg[L"ScriptID"].AsInt64();//.Value;
		postmsg[L"anumber"] = m_InitMsg[L"A"].Value;
		postmsg[L"bnumber"] = m_InitMsg[L"B"].Value;
		postmsg[L"menuid"] = formID;
		postmsg[L"DestinationAddress"] = CLIENT_ADDRESS::ANY_CLIENT;

		return postmsg;
	}

	void CSession::ProccedFormComment(const VXMLString& formID , const VXMLString& comment)const
	{
		CMessage postmsg = CreateStatisticMsg(formID);
		postmsg.SetName(L"VXML2SA_Statistics");
		postmsg[L"comment"] = m_ExecContainer->ExprEvalToStr(comment);//comment;

		SendAuxMsg(postmsg);
	}

	void CSession::ProccedLogHttpComment(const VXMLString & aComment)const
	{
		if (!m_isLogHttpDispatch)
		{
			return;
		}

		CMessage postmsg = CreateStatisticMsg(m_ExecContainer->GetCurMenu());
		postmsg.SetName(L"VXML2AC_sendComment");
		postmsg[L"comment"] = aComment;
		SendAuxMsg(postmsg);
	}

	void CSession::ProccedBufferComment(const VXMLString& formID, const VXMLString& buffer)const
	{
		//if (m_ExecContainer->IsTransferActive())
		//{
		//	return;
		//}

		CMessage postmsg = CreateStatisticMsg(formID + L"_DigitsBuffer");
		postmsg.SetName(L"VXML2SA_Statistics");
		//postmsg[L"DigitsBuffer"] = buffer.c_str();
		postmsg[L"comment"] = (VXMLString(L"DigitsBuffer: ") + buffer).c_str();

		SendAuxMsg(postmsg);
	}

	ObjectId CSession::GetParentCallID()const
	{
		if (const CParam* param = m_InitMsg.ParamByName(L"CallID"))
			return param->AsInt64();
		if (const CParam* param = m_InitMsg.ParamByName(L"VoxID"))
			return param->AsInt64();
		return 0;
	}

	std::pair<ObjectId, VXMLString> CSession::GetParentVox() const
	{
		std::pair<ObjectId, VXMLString> parent;
		if (const CParam* param = m_InitMsg.ParamByName(L"CallID"))
		{
			parent = std::pair<ObjectId, VXMLString>(param->AsInt64(), L"CallID");
		}
		else if (const CParam* param = m_InitMsg.ParamByName(L"VoxID"))
		{
			parent = std::pair<ObjectId, VXMLString>(param->AsInt64(), L"VoxID");
		}
		else
		{
			throw std::runtime_error("Cannot find parent VOX");
		}
		return parent;
	}

	void CSession::SendEvent(CMessage& evt, /*int nSendId,*/ ObjectId to/*, DWORD dwDelay*/)
	{
		ObjectId nParentID = 0;
		if (CParam* param = m_InitMsg.ParamByName(L"CallID"))
		{
			nParentID = m_InitMsg[L"CallID"].Value.llVal;
			evt[L"CallID"] = nParentID;
		}
		if (CParam* param = m_InitMsg.ParamByName(L"VoxID"))
		{
			nParentID = m_InitMsg[L"VoxID"].Value.llVal;
			evt[L"VoxID"] = nParentID;
		}

		evt[L"CallbackID"]			= nParentID;
		evt[L"ScriptID"]			= m_InitMsg[L"ScriptID"].Value;
		evt[L"SaveScriptID"]		= m_InitMsg[L"ScriptID"].Value;

		evt[L"DestinationAddress"] = to;
		SendAuxMsg(evt);
	}

	void CSession::SendEventToYourself(CMessage& evt)
	{
		SendEvent(evt, m_InitMsg[L"ScriptID"].AsInt64());
	}

	ObjectId CSession::GetParentScriptID()const
	{
		return m_InitMsg[L"ParentScriptID"].AsInt64();
	}

	ObjectId VXML::CSession::GetScriptID() const
	{
		return m_InitMsg.ParamByName(L"ScriptID")->AsInt64();
	}

	VXMLString CSession::GetPredefineFiles()const
	{
		if (const CParam *pPredefine = m_InitMsg.ParamByName(L"predefine"))
			return pPredefine->AsWideStr();
		if (const CParam *pPredefine = m_InitMsg.ParamByName(L"Predefine"))
			return pPredefine->AsWideStr();

		return L"";
	}

	void CSession::DoStep()
	{
		try
		{
			if ((GetState() == VMS_READY))
			{
				EmitStep();
			}
		}
		catch (...)
		{
			Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception: while do next step");
			ExceptionHandler();
		}
	}

	void CSession::EmitStep()
	{
		ProcessStep();
	}

	void CSession::ProcessStep()
	{
		m_ExecContainer->EmitStep();
	}

	void CSession::WaitForStep(unsigned int _line)
	{
		if (!m_ExecContainer->WaitForStep(_line))
		{
			Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession::WaitForStep debuger NULL");
		}
	}

	void CSession::DbgEvent(CMessage& msg)
	{
		if (!m_ExecContainer->DbgEvent(msg, true))
		{
			Log(LEVEL_FINEST, __FUNCTIONW__, L"CSession::DbgEvent debuger NULL");
		}
	}

	CComVariant CSession::FindAttrEvent(const VXMLString & aConnectionId) const
	{
		return m_EventDispatcher->FindAttrEvent(aConnectionId);
	}

	bool CSession::DialogStart(CMessage& postmsg)
	{
		ObjectId nParentId = 0;
		if (const CParam* param = m_InitMsg.ParamByName(L"CallID"))
		{
			nParentId = param->AsInt64();
			postmsg[L"CallID"] = nParentId;
		}
		if (const CParam* param = m_InitMsg.ParamByName(L"VoxID"))
		{
			nParentId = param->AsInt64();
			postmsg[L"VoxID"] = nParentId;
		}

		postmsg[L"ParentScriptID"] = m_InitMsg[L"ScriptID"].Value;
		VXMLString sPredefine = GetPredefineFiles();
		if (!sPredefine.empty())
		{
			postmsg[L"predefine"] = sPredefine.c_str();
		}

		postmsg[L"DestinationAddress"] = CLIENT_ADDRESS::ANY_CLIENT;

		bool bRunFunction = postmsg.ParamByName(L"function");

		if (bRunFunction)
			postmsg[L"DestinationAddress"] = GetParentScriptID();

		CMessage waitmsg;
		VXMLString sId;
		WaitingEvents waiting_events;

		if (!bRunFunction)
		{
			waiting_events.push_back(L"VXML_RUN_SCRIPT_OK");
			waiting_events.push_back(L"VXML_RUN_SCRIPT_FAILED");
		}
		else
		{
			waiting_events.push_back(L"SC2V_RUN_FUNCTION_ACK");
			//waiting_events.push_back(L"SC2V_RUN_FUNCTION_FAILED");
		}
		eWaitEvents retEvent = WaitMultipleEvent(&postmsg, waiting_events, L"", waitmsg);

		if (retEvent == WE_SUCCESS)
		{
			bool bFailed = false;
			if (!VXMLString(waitmsg.GetName()).compare(L"VXML_RUN_SCRIPT_FAILED"))
			{
				bFailed = true;
			}

			if (!VXMLString(waitmsg.GetName()).compare(L"SC2V_RUN_FUNCTION_ACK"))
			{
				if (CParam *param = waitmsg.ParamByName(L"ErrorCode"))
				{
					bFailed = true;
				}
			}

			if (bFailed)
			{
				CMessage evt(L"error.badfetch");
				ObjectId iScriptID = m_InitMsg[L"ScriptID"].AsInt64();

				if (!bRunFunction)
				{
					evt[L"reason"] = format_wstring(L"Cannot run script [scriptID = %08X-%08X] \"%s\"", HIDWORD(iScriptID), LODWORD(iScriptID), postmsg[L"FileName"].AsWideStr().c_str());
					evt[L"tagname"] = L"Run script";
					evt[L"description"] = L"A fetch of a document has failed";
				}
				else
				{
					evt[L"reason"] = format_wstring(L"Cannot run function [scriptID = %08X-%08X] \"%s\"", HIDWORD(iScriptID), LODWORD(iScriptID), postmsg[L"function"].AsWideStr().c_str());
					evt[L"tagname"] = L"Run st3 function";
					if (CParam *param = waitmsg.ParamByName(L"ErrorDescription"))
					{
						evt[L"description"] = param->AsString();
					}
					else
					{
						evt[L"description"] = L"A fetch of a document has failed";
					}
					
				}

				DoError(evt, L"SESSION");

				return true;
			}
			else
			{
				ChangeCallCtrl(m_InitMsg.ParamByName(L"ScriptID")->AsInt64(), waitmsg.ParamByName(L"ScriptID")->AsInt64());
			}
		}
		else
		{
			DoExit();
			return false;
		}

		if (!bRunFunction)
			retEvent = WaitEvent(NULL, L"END_DIALOG", L"error", waitmsg);
		else
			retEvent = WaitEvent(NULL, L"SC2V_RUN_FUNCTION_COMPLETED", L"error", waitmsg);
		// ToDo: WaitMultiplyEvent END_DIALOG && TERMINATE_DIALOG

		if (retEvent == WE_SUCCESS)
		{

			if (CParam* paramName  = waitmsg.ParamByName(L"namelist"))
			{
				VXMLString sNameList = paramName->AsWideStr();
				LPCWSTR p = sNameList.c_str(), q;
				VXMLString sParamName;

				while (*p)
				{
					q = wcschr(p, L' ');
					if (!q)
					{
						sParamName = trim_wstring(p);
					}
					else
					{
						sParamName = trim_wstring(VXMLString(p, q - p));
					}
					if (!sParamName.empty())
					{
						VXMLString statement = sParamName + L"=\"" + waitmsg[sParamName.c_str()].AsWideStr() + L"\"";
						m_ExecContainer->ExecNormalStatement(statement);
					}
					if (!q)
					{
						break;
					}
					p = q + 1;
				}
			}
		}
		else
		{
			DoExit();
			return false;
		}

		return true;
	}


	void CSession::ChangeCallCtrl(const ObjectId& _oldScriptID, const ObjectId& _newScriptID)
	{
		if (const CParam* param = m_InitMsg.ParamByName(L"VoxID"))
			return; // do not necessary change control in conference
		CMessage change_control_msg(L"CHANGE_CALL_CONTROL");
		change_control_msg [ L"CallID"             ] = m_InitMsg[ L"CallID"   ].Value ;
		change_control_msg [ L"OldScriptID"        ] = _oldScriptID                   ;
		change_control_msg [ L"NewScriptID"        ] = _newScriptID                   ;
		change_control_msg [ L"DestinationAddress" ] = m_InitMsg[ L"CallID"   ].Value ;
		change_control_msg [ L"SourceAddress"      ] = m_InitMsg[ L"ScriptID" ].Value ;
		SendAuxMsg(change_control_msg);
	}

	VXMLString CSession::GetVarValue(const VXMLString& _name)const
	{
		try
		{
			return m_ExecContainer->ExprEvalToStr(_name);
		}
		catch (VXMLString &_error)
		{
			Log(LEVEL_WARNING, __FUNCTIONW__, L"GetVarValue error: %s", _error.c_str());
			return SYMBOLNOTFOUND_DBGMSG;
		}
		catch (...)
		{
			return SYMBOLNOTFOUND_DBGMSG;
		}
	}

	VXMLString CSession::GetSpeakNumberFiles(const VXMLString& expr, int sex)const 
	{
		_bstr_t Files = m_pVox->GetInstance()->GetSpeakNumberFiles(
			CComVariant(expr.c_str()), tagSpeakSex(sex), VARIANT_FALSE);

		return VXMLString(Files);
	}

	VXMLString CSession::GetSpeakTimeFiles(DWORD hh, DWORD mm, DWORD ss)const 
	{
		SYSTEMTIME st = {0};
		st.wHour   = hh;
		st.wMinute = mm;
		st.wSecond = ss;

		DOUBLE dblTime = 0.0;
		::SystemTimeToVariantTime(&st, &dblTime);
		CComVariant Time(dblTime);

		_bstr_t Files = m_pVox->GetInstance()->GetSpeakTimeFiles(
			Time, tfHours | tfMinutes | tfSeconds);

		return VXMLString(Files);
	}

	VXMLString CSession::GetSpeakDateFiles(DWORD dd, DWORD mm, DWORD yy)const 
	{
		SYSTEMTIME st = {0};
		st.wDay   = dd;
		st.wMonth = mm;
		st.wYear  = yy;

		DOUBLE dblTime = 0.0;
		::SystemTimeToVariantTime(&st, &dblTime);
		CComVariant Time(dblTime);

		_bstr_t Files = m_pVox->GetInstance()->GetSpeakDateFiles(
			Time, dfDays | dfMonths | dfYears);

		return VXMLString(Files);
	}

	VXMLString CSession::GetSpeakMoneyFiles(const VXMLString& expr, const VXMLString& format)const 
	{
		_bstr_t Files = m_pVox->GetInstance()->GetSpeakMoneyFiles(CComVariant(expr.c_str()),
			                           _bstr_t(format.c_str()));

		return VXMLString(Files);
	}

	VXMLString CSession::GetSpeakTrafficFiles(const VXMLString & expr) const
	{
		_bstr_t Files = m_pVox->GetInstance()->GetSpeakTrafficFiles(CComVariant(expr.c_str()));

		return VXMLString(Files);
	}

	VXMLString CSession::GetSpeakMinutesFiles(const VXMLString & expr) const
	{
		_bstr_t Files = m_pVox->GetInstance()->GetSpeakMinutesFiles(CComVariant(expr.c_str()));

		return VXMLString(Files);
	}

	VXMLString CSession::GetSpeakSMSFiles(const VXMLString & expr) const
	{
		_bstr_t Files = m_pVox->GetInstance()->GetSpeakSMSFiles(CComVariant(expr.c_str()));

		return VXMLString(Files);
	}

	void CSession::startKeepAliveTimers()
	{
		m_ExecContainer->SubscribeTimer(m_InitMsg.ParamByName(L"ScriptID")->AsInt64());
		m_ExecContainer->SubscribeTimer(m_InitMsg.ParamByName(L"ParentScriptID")->AsInt64());
	}


	void CSession::stopKeepAliveTimers()
	{
		Log(LEVEL_FINEST, __FUNCTIONW__, L"Stop Keep Alive timers");
		m_ExecContainer->UnsubscribeTimer(m_InitMsg.ParamByName(L"ScriptID")->AsInt64());
		m_ExecContainer->UnsubscribeTimer(m_InitMsg.ParamByName(L"ParentScriptID")->AsInt64());
	}

	VXMLString CSession::ThreadMsgToString(const eThreadMsg& _msg)const
	{
		switch (_msg)
		{
		case tmExecuteDoc:
			return L"Execute Document";
		case tmSetEvent:
			return L"Set Event";
		case tmExecutePredefinition:
			return L"Execute predefine Document";
		case tmDocumentPrepared:
			return L"Prepare Document";
		case tmQuit:
			return L"Quit";
		}

		return L"Unknown thread message";
	}

	void CSession::StartThread()
	{
		m_thread.reset(
		new boost::thread(boost::bind(&CSession::ThreadProc, this))
		);

		m_bThreadStarted = true;
	}

	void CSession::JoinThread()
	{
		if (m_thread->joinable())
			m_thread->join();
		m_thread.reset();
	}

	void CSession::AddNamelistToMsg(CMessage& msg)
	{
		if (CParam* paramName = msg.ParamByName(L"namelist"))
		{
			std::wistringstream in(paramName->AsWideStr());
			std::istream_iterator<VXMLString, wchar_t> first(in), last;
			for (; first != last; ++first)
			{
				VXMLString sExpr;
				sExpr = m_ExecContainer->ExprEvalToStr((*first).c_str());
				Log(LEVEL_FINEST, __FUNCTIONW__, L"\"%s\" == \"%s\"", (*first).c_str(), sExpr.c_str());

				msg[(*first).c_str()] = m_ExecContainer->ExprEval(*first);
			}
		}
	}

}

/******************************* eof *************************************/