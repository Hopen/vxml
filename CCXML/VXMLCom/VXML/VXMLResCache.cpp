/************************************************************************/
/* Name     : VXMLCom\VXMLResCache.cpp                                  */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 1 Dec 2009                                                */
/************************************************************************/

#pragma once

#include "..\StdAfx.h"
#include "EvtModel.h"
#include "VXMLResCache.h"
#include "VXMLDocument.h"
#include "VXMLSession.h"
#include "..\CacheInterface\CacheInterface_i.c"

namespace VXML
{
	CResourceCache::CResourceCache()
		: m_SchemaFile((SVUtils::ExtractFilePath(SVUtils::GetModuleFileName(g_hInst)) + L"vxml.xsd").c_str())
	{
		m_hr = m_Res.CoCreateInstance(CLSID_ResourceCache);
	}

	CResourceCache::~CResourceCache()
	{
	}

	HRESULT CResourceCache::ConnectToCache()
	{
		m_hr = E_FAIL;
		try
		{
			m_Res.Release();
			m_hr = m_Res.CoCreateInstance(CLSID_ResourceCache);
		}
		catch (...)
		{
		}

		return m_hr;
	}

	HRESULT CResourceCache::GetDocument(
		const VXMLString& aSrc,
		const VXMLString& aScriptID,
		const bool& aIsFile,
		SAFEARRAY** aPSA,
		bool aIsSecondTry)
	{
		HRESULT hr = E_FAIL;
		try
		{
			 hr = m_Res->GetDocument(_bstr_t(aSrc.c_str()), m_SchemaFile, _bstr_t(aScriptID.c_str()), aIsFile, aPSA);
		}
		catch (...)
		{
		}

		if (FAILED(hr))
		{
			if (aIsSecondTry)
			{
				return E_FAIL;
			}

			ConnectToCache();
			return GetDocument(aSrc, aScriptID, aIsFile, aPSA, true);
		}

		return hr;
	}

	bool CResourceCache::Ready()const
	{
		return !!m_Res.p;
	}

	bool CResourceCache::TestReady()const
	{
		return !!m_Test.p;
	}

	HRESULT CResourceCache::WriteTestLog(const VXMLString& sSrc)
	{
		if (!m_Test)
			return E_FAIL;
		m_Test->WriteLog(_bstr_t(sSrc.c_str()));

		return S_OK;
	}

}
