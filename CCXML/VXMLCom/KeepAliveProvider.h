/************************************************************************/
/* Name     : VXMLCom\KeepAliveProvider.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 22 Apr 2015                                               */
/************************************************************************/
#pragma once

#include "Patterns/singleton.h"
#include <list>
#include <mutex>
#include <future>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include "sv_sysobj.h"
//#include "KeepAlive.h"
#include "VXMLBase.h"


class CKeepAliveProvider //: public singleton < CKeepAliveProvider >
{
	//friend class singleton < CKeepAliveProvider >;

public:
	void SubscribeTimer(const __int64&);
	void UnsubscribeTimer(const __int64&);

	void MakeKeepAliveMsg(const __int64& scriptID);
	void Init(const unsigned int& interval);
	//void Stop();
	size_t IncreaseDeadCounter() { return ++m_DeadCounter; }
	void ResetDeadCounter() { m_DeadCounter = 0; }

private:

	void StartThread(const unsigned int& aInterval);
	void JoinThread();

	//std::wstring GetScriptIdHexFormat(ObjectId aScriptId) const;
public:
	CKeepAliveProvider(
		IEngineCallback* aCallback,
		const VXML::EngineLogPtr2& aLog);
	~CKeepAliveProvider();
private:

	void ThreadProc(const unsigned int& aInterval);

private:
	typedef std::list <__int64> SubscribersCollector;
	SubscribersCollector m_subscribers;

	//CRITICAL_SECTION	m_CS;
	std::mutex m_mutex;

	//std::shared_ptr < boost::thread >  m_thread;
	IEngineCallback*	m_engine;
	std::atomic<bool>	m_threadStarted;
	//VXML::IEngineCallbackPtr m_engine;
	//VXML::EngineLogPtr       m_log;
	//VXML::SessionPtr  		m_session;
	VXML::EngineLogPtr2 m_log;

	//SVSysObj::CEvent		m_ThreadActive;
	//unsigned int			m_iLongPoolTimer;
	//unsigned int			m_curSec;

	std::future<void> m_task;
	SVSysObj::CEvent m_ThreadEvt;

	std::atomic<size_t> m_DeadCounter;
};

/******************************* eof *************************************/