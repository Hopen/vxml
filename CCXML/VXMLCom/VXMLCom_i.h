

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Sat Oct 27 15:01:27 2018
 */
/* Compiler settings for VXMLCom.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __VXMLCom_i_h__
#define __VXMLCom_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IVXMLComInterface_FWD_DEFINED__
#define __IVXMLComInterface_FWD_DEFINED__
typedef interface IVXMLComInterface IVXMLComInterface;
#endif 	/* __IVXMLComInterface_FWD_DEFINED__ */


#ifndef __INameTable_FWD_DEFINED__
#define __INameTable_FWD_DEFINED__
typedef interface INameTable INameTable;
#endif 	/* __INameTable_FWD_DEFINED__ */


#ifndef ___IVXMLComInterfaceEvents_FWD_DEFINED__
#define ___IVXMLComInterfaceEvents_FWD_DEFINED__
typedef interface _IVXMLComInterfaceEvents _IVXMLComInterfaceEvents;
#endif 	/* ___IVXMLComInterfaceEvents_FWD_DEFINED__ */


#ifndef __VXMLComInterface_FWD_DEFINED__
#define __VXMLComInterface_FWD_DEFINED__

#ifdef __cplusplus
typedef class VXMLComInterface VXMLComInterface;
#else
typedef struct VXMLComInterface VXMLComInterface;
#endif /* __cplusplus */

#endif 	/* __VXMLComInterface_FWD_DEFINED__ */


#ifndef __NameTable_FWD_DEFINED__
#define __NameTable_FWD_DEFINED__

#ifdef __cplusplus
typedef class NameTable NameTable;
#else
typedef struct NameTable NameTable;
#endif /* __cplusplus */

#endif 	/* __NameTable_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IVXMLComInterface_INTERFACE_DEFINED__
#define __IVXMLComInterface_INTERFACE_DEFINED__

/* interface IVXMLComInterface */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IVXMLComInterface;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("03DC58CE-2499-4960-B547-30D697C3599B")
    IVXMLComInterface : public IDispatch
    {
    public:
    };
    
#else 	/* C style interface */

    typedef struct IVXMLComInterfaceVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IVXMLComInterface * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IVXMLComInterface * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IVXMLComInterface * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IVXMLComInterface * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IVXMLComInterface * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IVXMLComInterface * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IVXMLComInterface * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } IVXMLComInterfaceVtbl;

    interface IVXMLComInterface
    {
        CONST_VTBL struct IVXMLComInterfaceVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IVXMLComInterface_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IVXMLComInterface_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IVXMLComInterface_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IVXMLComInterface_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IVXMLComInterface_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IVXMLComInterface_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IVXMLComInterface_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IVXMLComInterface_INTERFACE_DEFINED__ */


#ifndef __INameTable_INTERFACE_DEFINED__
#define __INameTable_INTERFACE_DEFINED__

/* interface INameTable */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_INameTable;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("CEA904CB-B02F-46C0-9CAE-6824B10A6323")
    INameTable : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetIDsOfNames( 
            REFIID riid,
            LPOLESTR *rgszNames,
            UINT cNames,
            LCID lcid,
            DISPID *rgDispId) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Invoke( 
            DISPID dispIdMember,
            REFIID riid,
            LCID lcid,
            WORD wFlags,
            DISPPARAMS *pDispParams,
            VARIANT *pVarResult,
            EXCEPINFO *pExcepInfo,
            UINT *puArgErr) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct INameTableVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            INameTable * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            INameTable * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            INameTable * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            INameTable * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            INameTable * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            INameTable * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            INameTable * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            INameTable * This,
            REFIID riid,
            LPOLESTR *rgszNames,
            UINT cNames,
            LCID lcid,
            DISPID *rgDispId);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            INameTable * This,
            DISPID dispIdMember,
            REFIID riid,
            LCID lcid,
            WORD wFlags,
            DISPPARAMS *pDispParams,
            VARIANT *pVarResult,
            EXCEPINFO *pExcepInfo,
            UINT *puArgErr);
        
        END_INTERFACE
    } INameTableVtbl;

    interface INameTable
    {
        CONST_VTBL struct INameTableVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define INameTable_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define INameTable_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define INameTable_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define INameTable_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define INameTable_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define INameTable_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define INameTable_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define INameTable_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define INameTable_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __INameTable_INTERFACE_DEFINED__ */



#ifndef __VXMLComLib_LIBRARY_DEFINED__
#define __VXMLComLib_LIBRARY_DEFINED__

/* library VXMLComLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_VXMLComLib;

#ifndef ___IVXMLComInterfaceEvents_DISPINTERFACE_DEFINED__
#define ___IVXMLComInterfaceEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IVXMLComInterfaceEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IVXMLComInterfaceEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("C8A10315-D1EF-4AD9-BBA8-B14F34A7CCEA")
    _IVXMLComInterfaceEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IVXMLComInterfaceEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            _IVXMLComInterfaceEvents * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            _IVXMLComInterfaceEvents * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            _IVXMLComInterfaceEvents * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            _IVXMLComInterfaceEvents * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            _IVXMLComInterfaceEvents * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            _IVXMLComInterfaceEvents * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            _IVXMLComInterfaceEvents * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        END_INTERFACE
    } _IVXMLComInterfaceEventsVtbl;

    interface _IVXMLComInterfaceEvents
    {
        CONST_VTBL struct _IVXMLComInterfaceEventsVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IVXMLComInterfaceEvents_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define _IVXMLComInterfaceEvents_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define _IVXMLComInterfaceEvents_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define _IVXMLComInterfaceEvents_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define _IVXMLComInterfaceEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define _IVXMLComInterfaceEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define _IVXMLComInterfaceEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IVXMLComInterfaceEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_VXMLComInterface;

#ifdef __cplusplus

class DECLSPEC_UUID("CCBC516A-6A6C-4016-A47D-2A430E8AA488")
VXMLComInterface;
#endif

EXTERN_C const CLSID CLSID_NameTable;

#ifdef __cplusplus

class DECLSPEC_UUID("D783F64E-AF9C-47D3-9526-577221E10457")
NameTable;
#endif
#endif /* __VXMLComLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long *, unsigned long            , VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserMarshal(  unsigned long *, unsigned char *, VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserUnmarshal(unsigned long *, unsigned char *, VARIANT * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long *, VARIANT * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


