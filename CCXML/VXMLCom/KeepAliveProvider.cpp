/************************************************************************/
/* Name     : VXMLCom\KeepAliveProvider.cpp                             */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 22 Apr 2015                                               */
/************************************************************************/
#include "stdafx.h"
#include "KeepAliveProvider.h"
#include "EvtModel.h"
#include "..\Engine\EngineLog.h"
#include "sv_strutils.h"
#include "common.h"

#include <boost/asio/steady_timer.hpp>
#include <boost/asio/spawn.hpp>
#include "Patterns/string_functions.h"
#include "VXMLSession.h"
//class CLocker
//{
//private:
//	CRITICAL_SECTION& m_CS;
//
//public:
//	CLocker(CRITICAL_SECTION& cs) : m_CS(cs)
//	{
//		::EnterCriticalSection(&m_CS);
//	}
//	~CLocker()
//	{
//		::LeaveCriticalSection(&m_CS);
//	}
//};

CKeepAliveProvider::CKeepAliveProvider(
	IEngineCallback* aCallback,
	const VXML::EngineLogPtr2& aLog)
	: m_engine(aCallback)
	, m_threadStarted(false)
	, m_log(aLog)
	, m_DeadCounter(0)
{
	//::InitializeCriticalSection(&m_CS);

}

void CKeepAliveProvider::Init(const unsigned int& interval)
{
	m_log->Log(enginelog::LEVEL_INFO, __FUNCTIONW__, L"CKeepAliveProvider initializing...");
	m_log->Log(enginelog::LEVEL_INFO, __FUNCTIONW__, L"CKeepAliveProvider expired every %i seconds...", interval);

	StartThread(interval);
}


CKeepAliveProvider::~CKeepAliveProvider()
{
	m_log->Log(enginelog::LEVEL_INFO, __FUNCTIONW__, L"CKeepAliveProvider clearing...");
	JoinThread();
}

void CKeepAliveProvider::SubscribeTimer(const __int64& scriptID)
{
	std::wstring  sScriptID = format_wstring(L"%08X-%08X", HIDWORD(scriptID), LODWORD(scriptID));
	m_log->Log(enginelog::LEVEL_INFO, __FUNCTIONW__, L"SubscribeTimer for %s", sScriptID.c_str());

	{
		std::lock_guard <std::mutex> lock(m_mutex);
		m_subscribers.push_back(scriptID);
	}
}

void CKeepAliveProvider::UnsubscribeTimer(const __int64& scriptID)
{
	std::wstring  sScriptID = format_wstring(L"%08X-%08X", HIDWORD(scriptID), LODWORD(scriptID));
	m_log->Log(enginelog::LEVEL_INFO, __FUNCTIONW__, L"UnsubscribeTimer for %s", sScriptID.c_str());

	{
		std::lock_guard <std::mutex> lock(m_mutex);
		m_subscribers.remove(scriptID);
	}
}


void CKeepAliveProvider::MakeKeepAliveMsg(const __int64& scriptID)
{
	if (m_engine == nullptr)
	{
		throw std::runtime_error("Cannot make KeepAlive message couse IEngineCallback nullptr");
	}
	CMessage msg(L"KEEP_ALIVE");
	msg[L"DestinationAddress"] = scriptID;
	CPackedMsg pm(msg);
	m_engine->PostAuxMessage(pm());
}

void CKeepAliveProvider::StartThread(const unsigned int& aInterval)
{
	//m_thread.reset(
	//	new boost::thread(boost::bind(&CKeepAliveProvider::ThreadProc, this))
	//	);

	m_ThreadEvt.Create();
	m_task = std::async(std::launch::async, boost::bind(&CKeepAliveProvider::ThreadProc, this, aInterval));
	m_ThreadEvt.Wait();
}
//
void CKeepAliveProvider::JoinThread()
{
	//auto pSes = m_session.lock();
	//if (pSes)
	//	pSes->Log(enginelog::LEVEL_FINEST, __FUNCTIONW__, L"CKeepAliveProvider JoinThread");

	if (m_threadStarted == true)
	{
		m_threadStarted = false;
		//::Sleep(1500); 
		while (m_task.wait_for(std::chrono::milliseconds(1000)) != std::future_status::ready)
		{
			m_log->Log(enginelog::LEVEL_INFO, __FUNCTIONW__, L"CKeepAliveProvider wait for thread joined");
		}

	}
	m_log->Log(enginelog::LEVEL_INFO, __FUNCTIONW__, L"Thread joined");
	//m_thread.reset();
}

void CKeepAliveProvider::ThreadProc(const unsigned int& aInterval)
{
	if (aInterval == 0)
	{
		throw std::runtime_error("KeepAlive null interval");
	}

	m_log->Log(enginelog::LEVEL_INFO, __FUNCTIONW__, L"CKeepAliveProvider thread started");
	m_threadStarted = true;

	unsigned int cursec = 0;

	m_ThreadEvt.Set();

	while (m_threadStarted == true)
	{
		if (1)
		{
			std::lock_guard <std::mutex> lock(m_mutex);
			if (m_subscribers.size())
			{
				++cursec;
			}
			if (cursec >= aInterval)
			{
				cursec = 0;
				m_log->Log(enginelog::LEVEL_FINEST, __FUNCTIONW__, L"========================keep alive tick===================");

				for(auto& scriptID : m_subscribers)
				{
					MakeKeepAliveMsg(scriptID);
				}
			}
		}
		::Sleep(1000);
	}
	
	m_log->Log(enginelog::LEVEL_FINEST, __FUNCTIONW__, L"CKeepAliveProvider ThreadProc exit");
}

//std::wstring CKeepAliveProvider::GetScriptIdHexFormat(ObjectId aScriptId) const
//{
//	try
//	{
//		return (boost::wformat(L"%08X-%08X") % HIDWORD(aScriptId) % LODWORD(aScriptId)).str();
//	}
//	catch (std::exception& aError)
//	{
//		Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception GetScriptIdHexFormat: \"%s\"", stow(aError.what()));
//	}
//	catch (...)
//	{
//		Log(LEVEL_WARNING, __FUNCTIONW__, L"Exception GetScriptIdHexFormat unhandled exception");
//	}
//	return std::wstring();
//}


/******************************* eof *************************************/