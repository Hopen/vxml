#pragma once

#include <string>
#include <boost/lexical_cast.hpp>

std::wstring stow( const std::string& in, std::locale loc  = std::locale() );
std::string wtos( const std::wstring& in, std::locale loc  = std::locale() );

namespace boost
{
	template<>
	inline std::wstring lexical_cast<std::wstring, std::string>(const std::string& _value)
	{
		return stow(_value);
	}

	template<>
	inline std::string lexical_cast<std::string, std::wstring>(const std::wstring& _value)
	{
		return wtos(_value);
	}
}

