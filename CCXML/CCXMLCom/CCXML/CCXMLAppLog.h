#include <memory>
#include "CCXMLBase.h"
#include "sid.h"
#include "..\Engine\EngineLog.h"

namespace CCXML
{
	class CAppLog : public std::enable_shared_from_this<CAppLog>
	{
	public:
		CAppLog(enginelog::LEVEL aLevel, const CCXMLString& uri);

		virtual CComVariant AsVariant()
		{
			return CComVariant(new CComBridge<CAppLog>(shared_from_this()));
		}

	private:
		HRESULT log(const CCXMLString&);

		HRESULT init(const CCXMLString&);
		HRESULT init(ObjectId aId, enginelog::LEVEL aLevel, const CCXMLString&);

	public:
		// IDispatch methods
		virtual HRESULT STDMETHODCALLTYPE GetTypeInfoCount(
			/* [out] */ UINT *pctinfo);

		virtual HRESULT STDMETHODCALLTYPE GetTypeInfo(
			/* [in] */ UINT iTInfo,
			/* [in] */ LCID lcid,
			/* [out] */ ITypeInfo **ppTInfo);

		STDMETHOD(GetIDsOfNames)(REFIID riid, LPOLESTR * rgszNames, UINT cNames, LCID lcid, DISPID * rgDispId);
		STDMETHOD(Invoke)(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS * pDispParams, VARIANT * pVarResult, EXCEPINFO * pExcepInfo, UINT * puArgErr);

	private:
		EngineLogPtr2 m_log;
	};
}