#include "stdafx.h"
#include "CCXMLAppLog.h"

using namespace enginelog;

namespace CCXML
{
	enum class ID_FUNCTIONS
	{
		INIT = 1,
		LOG
	};

	CAppLog::CAppLog(LEVEL alevel, const CCXMLString& uri): m_log(nullptr)
	{
		init(GetGlobalScriptId(), alevel, uri);
	}

	HRESULT CAppLog::init(const CCXMLString& uri)
	{
		LEVEL level = LEVEL::LEVEL_FINEST;
		ULONGLONG sid = GetGlobalScriptId();
		if (m_log)
		{
			level = m_log->GetLevel();
			sid = m_log->GetSID();
		}

		return init(sid, level, uri);
	}

	HRESULT CAppLog::init(ObjectId aId, LEVEL aLevel, const CCXMLString& aUri)
	{
		m_log.reset(new CEngineLog());
		m_log->Init(aLevel, aUri.c_str(), true);
		m_log->SetSID(aId);

		return S_OK;
	}

	HRESULT CAppLog::log(const CCXMLString &log_str)
	{
		if (m_log)
		{
			m_log->Log(LEVEL_FINEST, __FUNCTIONW__, L"%s", log_str.c_str());
		}
		else
		{
			return E_NOT_SET;
		}
		return S_OK;
	}


	STDMETHODIMP CAppLog::GetTypeInfoCount(
		/* [out] */ UINT *pctinfo)
	{
		if (::IsBadWritePtr(pctinfo, sizeof(*pctinfo)))
		{
			return E_INVALIDARG;
		}

		*pctinfo = 0;
		return S_OK;
	}

	STDMETHODIMP CAppLog::GetTypeInfo(
		/* [in] */ UINT iTInfo,
		/* [in] */ LCID lcid,
		/* [out] */ ITypeInfo **ppTInfo)
	{
		return E_NOTIMPL;
	}

	STDMETHODIMP CAppLog::GetIDsOfNames(REFIID riid, LPOLESTR * rgszNames, UINT cNames, LCID lcid, DISPID * rgDispId)
	{
		// TODO: Add your implementation code here

		HRESULT hRes = S_OK;
		
		for (UINT i = 0; i < cNames; i++)
		{
			if ( 0 == CAtlStringW(*rgszNames).CompareNoCase(L"log"))
			{
				rgDispId[i] = static_cast<int>(ID_FUNCTIONS::LOG);
			}
			else if (0 == CAtlStringW(*rgszNames).CompareNoCase(L"init"))
			{
				rgDispId[i] = static_cast<int>(ID_FUNCTIONS::INIT);
			}
			else
				rgDispId[i] = DISPID_UNKNOWN;
		}
		return hRes;
	}

	STDMETHODIMP CAppLog::Invoke(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS * pDispParams, VARIANT * pVarResult, EXCEPINFO * pExcepInfo, UINT * puArgErr)
	{
		struct Error
		{
			HRESULT hr;
			CCXMLString description;
			Error(HRESULT _hr, LPCWSTR pwszDescr)
				: hr(_hr), description(pwszDescr)
			{
			}
		};

		HRESULT hr = E_FAIL;

		try
		{
			// Trying to get first arg - arguments are stored in reverse order
			VARIANT* pName = &pDispParams->rgvarg[pDispParams->cArgs - 1];
			if (::IsBadReadPtr(pName, sizeof(VARIANT)))
			{
				*puArgErr = 0;
				throw Error(E_INVALIDARG, L"Invalid parameter value for Name");
			}

			CComVariant vName(*pName);
			// Trying to cast item name to BSTR
			if (vName.ChangeType(VT_BSTR) != S_OK)
			{
				throw Error(E_INVALIDARG, L"Invalid parameter type for Name");
			}

			switch (static_cast<ID_FUNCTIONS>(dispIdMember))
			{
			case ID_FUNCTIONS::INIT:
			{
				init(vName.bstrVal);
				break;
			}
			case ID_FUNCTIONS::LOG:
			{
				log(vName.bstrVal);
				break;
			}
			}

			hr = S_OK;
		}
		catch (Error& err)
		{
			// Check pExcepInfo structure pointer
			if (::IsBadReadPtr(pExcepInfo, sizeof(EXCEPINFO)))
			{
				hr = DISP_E_EXCEPTION;
			}

			// Fill pExcepInfo structure

			ZeroMemory(pExcepInfo, sizeof(EXCEPINFO));

			pExcepInfo->bstrDescription = ::SysAllocString(err.description.c_str());
			pExcepInfo->bstrSource = ::SysAllocString(L"");
			pExcepInfo->bstrHelpFile = ::SysAllocString(L"");
			pExcepInfo->scode = err.hr;

			hr = err.hr;
		}
		catch (...)
		{
			hr = DISP_E_EXCEPTION;
		}

		//VARIANT var;
		//var.vt = VT_INT;
		//var.intVal = 0;

		//::VariantCopy(pVarResult, &var);

		return hr;

		//// TODO: Add your implementation code here
		//VAR* pVar = NULL;
		//struct Error
		//{
		//	HRESULT hr;
		//	CCXMLString description;
		//	Error(HRESULT _hr, LPCWSTR pwszDescr)
		//		: hr(_hr), description(pwszDescr)
		//	{
		//	}
		//};

		//try
		//{

		//	if (dispIdMember)
		//	{
		//		pVar = reinterpret_cast<VAR*>(dispIdMember);
		//		if (::IsBadReadPtr(pVar, sizeof(VAR)))
		//		{
		//			throw Error(DISP_E_MEMBERNOTFOUND, L"Invalid dispIdMember parameter");
		//		}
		//	}
		//	else
		//	{

		//	}
		//	if (pVar && pVar->vValue.vt == VT_DISPATCH)
		//	{
		//		VARIANT& val = pVar->vValue;
		//		if (val.vt & VT_BYREF)
		//		{
		//			::VariantCopy(pVarResult, val.pvarVal);
		//		}
		//		else
		//		{
		//			::VariantCopy(pVarResult, &val);
		//		}
		//		return S_OK;
		//	}
		//	//// The result of this 'if' construct is initialized pVar variable
		//	//if (m_bActAsAssocArray)
		//	//{
		//	//	// If this is an associative array, VB should call it like 'arr("item_name")', 
		//	//	// so it calls Item method with 1 arg for GET and 2 args for SET

		//	//	// Our 'Item' ID is always 0
		//	//	if (dispidMember != 0)
		//	//	{
		//	//		throw Error(DISP_E_MEMBERNOTFOUND, L"Invalid dispIdMember parameter");
		//	//	}

		//	//	// Need at least 1 argument for item name
		//	//	if (pdispparams->cArgs < 1)
		//	//	{
		//	//		throw Error(E_INVALIDARG, L"Invalid number of parameters");
		//	//	}

		//	//	// Trying to get first arg - arguments are stored in reverse order
		//	//	VARIANT* pName = &pdispparams->rgvarg[pdispparams->cArgs - 1];
		//	//	if (::IsBadReadPtr(pName, sizeof(VARIANT)))
		//	//	{
		//	//		*puArgErr = 0;
		//	//		throw Error(E_INVALIDARG, L"Invalid parameter value for Name");
		//	//	}

		//	//	CComVariant vName(*pName);
		//	//	// Trying to cast item name to BSTR
		//	//	if (vName.ChangeType(VT_BSTR) != S_OK)
		//	//	{
		//	//		throw Error(E_INVALIDARG, L"Invalid parameter type for Name");
		//	//	}

		//	//	// Trying to find an item by name
		//	//	pVar = Find(vName.bstrVal);

		//	//	if (pVar == NULL)
		//	//	{
		//	//		CCXMLString s = L"Member '" + CCXMLString(vName.bstrVal) + L"' not found";
		//	//		throw Error(DISP_E_MEMBERNOTFOUND, s.c_str());
		//	//	}
		//	//}

		//	// Now let's actually get or set VAR*

		//	// Get
		//	if (wFlags & DISPATCH_PROPERTYGET)
		//	{
		//		// Check pVarResult pointer
		//		if (::IsBadReadPtr(pVarResult, sizeof(VARIANT)))
		//		{
		//			throw Error(DISP_E_MEMBERNOTFOUND, L"Invalid pVarResult parameter");
		//		}

		//		// Copy VAR to pVarResult

		//		// Note!!! If this value is VT_BYREF, than it is actually stored
		//		// in another name table (scope), and here we have only reference to it.
		//		// So, modify it only in referenced table.
		//		VARIANT& val = pVar->vValue;
		//		if (val.vt & VT_BYREF)
		//		{
		//			::VariantCopy(pVarResult, val.pvarVal);
		//		}
		//		else
		//		{
		//			::VariantCopy(pVarResult, &val);
		//		}
		//	}
		//	// Set
		//	else if (wFlags & DISPATCH_PROPERTYPUT)
		//	{
		//		// Need second arg as assignment value
		//		if (pDispParams->cArgs < 1)
		//		{
		//			throw Error(E_INVALIDARG, L"Invalid number of parameters");
		//		}

		//		// Check supplied value pointer
		//		if (::IsBadReadPtr(&pDispParams->rgvarg[0], sizeof(VARIANT)))
		//		{
		//			throw Error(E_INVALIDARG, L"Invalid assignment r-value");
		//		}

		//		try
		//		{
		//			// Set VAR value
		//			pVar->SetValue(&pDispParams->rgvarg[0]);
		//		}
		//		catch (std::exception& e)
		//		{
		//			// Cannot set it
		//			throw Error(DISP_E_EXCEPTION, CComVariant(e.what()).bstrVal);
		//		}
		//	}
		//	else
		//	{
		//		//VARIANT& val = this->AsVariant();//CComVariant((IDispatch *)this);
		//		//::VariantCopy(pVarResult, &val);
		//		::VariantCopy(pVarResult, &AsVariant());
		//	}

		//	return S_OK;
		//	/*return __super::Invoke( dispidMember, riid, lcid,
		//	wFlags, pdispparams, pvarResult, pexcepinfo, puArgErr);*/
		//}
		//catch (Error& err)
		//{
		//	// Check pExcepInfo structure pointer
		//	if (::IsBadReadPtr(pExcepInfo, sizeof(EXCEPINFO)))
		//	{
		//		return DISP_E_EXCEPTION;
		//	}

		//	// Fill pExcepInfo structure

		//	ZeroMemory(pExcepInfo, sizeof(EXCEPINFO));

		//	pExcepInfo->bstrDescription = ::SysAllocString(err.description.c_str());
		//	pExcepInfo->bstrSource = ::SysAllocString(L"");
		//	pExcepInfo->bstrHelpFile = ::SysAllocString(L"");
		//	pExcepInfo->scode = err.hr;

		//	return err.hr;
		//}
		//catch (...)
		//{
		//	return DISP_E_EXCEPTION;
		//}
		return E_NOTIMPL;
	}
}