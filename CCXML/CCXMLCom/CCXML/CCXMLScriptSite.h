/************************************************************************/
/* Name     : CCXMLCom\CCXMLScriptSite.h                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 24 Nov 2009                                               */
/************************************************************************/
#pragma once

#include <windows.h>
#include <activscp.h>

class CCCXMLScriptSite : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public IActiveScriptSite 
{
public:

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	BEGIN_COM_MAP(CCCXMLScriptSite)
		COM_INTERFACE_ENTRY(IActiveScriptSite)
	END_COM_MAP()

	CCCXMLScriptSite() 
	{
	}
	~CCCXMLScriptSite() 
	{
	}

	// ������ IActiveScriptSite...
	virtual HRESULT __stdcall GetLCID(LCID *plcid) 
	{
		return S_OK;
	}

	virtual HRESULT __stdcall GetItemInfo(LPCOLESTR pstrName,
		DWORD dwReturnMask, IUnknown **ppunkItem, ITypeInfo **ppti) 
	{
		if(ppti) 
		{
			*ppti = NULL;

			// ���� ������ ITypeInfo... 
			if(dwReturnMask & SCRIPTINFO_ITYPEINFO) {
				*ppti = m_spTypeInfo;
				(*ppti)->AddRef();
			}
		}

		// ���� Windows Scripting ��������� ppunkItem...
		if(ppunkItem)
		{
			*ppunkItem = NULL;

			// ���� Windows Scripting ������� IUnknown ������ �������...
			if(dwReturnMask & SCRIPTINFO_IUNKNOWN) 
			{
				// ...� ��������� ������ � ������ CNameTable2...
				USES_CONVERSION;
				//if (!lstrcmpi(_T("event1"), pstrName)) 
				{
					// ...�� ����������� ��� ������.
					*ppunkItem = m_spUnkScriptObject;
					// ...� AddRef'�� ��� ������...
					(*ppunkItem)->AddRef();
				}
			}
		}
		return S_OK;
	}

	virtual HRESULT __stdcall GetDocVersionString(BSTR *pbstrVersion) 
	{
		return S_OK;
	}

	virtual HRESULT __stdcall OnScriptTerminate(
		const VARIANT *pvarResult, const EXCEPINFO *pexcepInfo) 
	{
		return S_OK;
	}

	virtual HRESULT __stdcall OnStateChange(SCRIPTSTATE ssScriptState) 
	{
		return S_OK;
	}

	virtual HRESULT __stdcall OnScriptError(
		IActiveScriptError *pscriptError) 
	{
		// ��� ��������� �������� � ������ ������ � �������.
		// ����� ��������� ���������� � pscriptError.
		EXCEPINFO ei;
		pscriptError->GetExceptionInfo(&ei);

		USES_CONVERSION;
		::MessageBox(::GetActiveWindow(), ei.bstrDescription, L"Script Error", MB_SETFOREGROUND);
		return S_OK;
	}

	virtual HRESULT __stdcall OnEnterScript(void) 
	{
		return S_OK;
	}

	virtual HRESULT __stdcall OnLeaveScript(void) 
	{
		return S_OK;
	}

public:   
	CComPtr<IUnknown> m_spUnkScriptObject;
	CComPtr<ITypeInfo> m_spTypeInfo;
}; 

/******************************* eof *************************************/