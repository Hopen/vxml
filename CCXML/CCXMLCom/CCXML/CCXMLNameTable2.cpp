// CCXMLNameTable2.cpp : Implementation of CNameTable2

#include "..\StdAfx.h"
#include "CCXMLNameTable2.h"
#include "sv_strutils.h"

// CCNameTable2

namespace CCXML
{
	STDMETHODIMP CNameTable2::SayHi(void)
	{
		// TODO: Add your implementation code here
		::MessageBox(NULL, L"�� ������ SayHi()", L"CCNameTable2", MB_SETFOREGROUND);
		return S_OK;
	}

	STDMETHODIMP CNameTable2::SayHi2(void)
	{
		// TODO: Add your implementation code here

		return S_OK;
	}

	STDMETHODIMP CNameTable2::GetIDsOfNames(REFIID riid, LPOLESTR* rgszNames, UINT cNames,
		LCID lcid, DISPID* rgdispid)
	{
		HRESULT hRes = S_OK;
		bool bUnkName = false;
		//if(this->m_bActAsAssocArray)
		//{
			for (UINT i = 0; i < cNames; i++)
			{
				VAR* pVar = m_event->Find(rgszNames[i]);
				if ((pVar != NULL))
				{
					rgdispid[i] = reinterpret_cast<DISPID>(pVar);
					bUnkName = true;
				}
				else
					rgdispid[i] = DISPID_UNKNOWN;
			}
		//}
		return bUnkName?hRes :__super::GetIDsOfNames(riid, rgszNames, cNames, lcid, rgdispid);
	}

	STDMETHODIMP CNameTable2::Invoke(DISPID dispidMember, REFIID riid,
		LCID lcid, WORD wFlags, DISPPARAMS* pdispparams, VARIANT* pvarResult,
		EXCEPINFO* pexcepinfo, UINT* puArgErr)
	{
		VAR* pVar = NULL;
		struct Error
		{
			HRESULT hr;
			CCXMLString description;
			Error(HRESULT _hr, LPCWSTR pwszDescr)
				: hr(_hr), description(pwszDescr)
			{
			}
		};

		try
		{

			if (dispidMember)
			{
				pVar = reinterpret_cast<VAR*>(dispidMember);
				if (::IsBadReadPtr(pVar, sizeof(VAR)))
				{
					throw Error(DISP_E_MEMBERNOTFOUND, L"Invalid dispIdMember parameter");
				}
			}
			else
			{

			}
			if(pVar && pVar->vValue.vt == VT_DISPATCH)
			{
				/*return pVar->vValue.pdispVal->Invoke(
					0, riid, lcid, wFlags, pdispparams, 
					pvarResult, pexcepinfo, puArgErr);*/

				//CNameTable::NameContainer * pNames = &((CNameTable*)pVar->vValue.pdispVal)->m_Names;
				//CNameTable::NameContainer::iterator it = pNames->begin();
				//VAR* pVar2 = reinterpret_cast<VAR*>(pVar->vValue.ppdispVal);
				VARIANT& val = pVar->vValue;
				if (val.vt & VT_BYREF)
				{
					::VariantCopy(pvarResult, val.pvarVal);
				}
				else
				{
					::VariantCopy(pvarResult, &val);
				}
				return S_OK;
			}
			//// The result of this 'if' construct is initialized pVar variable
			//if (m_bActAsAssocArray)
			//{
			//	// If this is an associative array, VB should call it like 'arr("item_name")', 
			//	// so it calls Item method with 1 arg for GET and 2 args for SET

			//	// Our 'Item' ID is always 0
			//	if (dispidMember != 0)
			//	{
			//		throw Error(DISP_E_MEMBERNOTFOUND, L"Invalid dispIdMember parameter");
			//	}

			//	// Need at least 1 argument for item name
			//	if (pdispparams->cArgs < 1)
			//	{
			//		throw Error(E_INVALIDARG, L"Invalid number of parameters");
			//	}

			//	// Trying to get first arg - arguments are stored in reverse order
			//	VARIANT* pName = &pdispparams->rgvarg[pdispparams->cArgs - 1];
			//	if (::IsBadReadPtr(pName, sizeof(VARIANT)))
			//	{
			//		*puArgErr = 0;
			//		throw Error(E_INVALIDARG, L"Invalid parameter value for Name");
			//	}

			//	CComVariant vName(*pName);
			//	// Trying to cast item name to BSTR
			//	if (vName.ChangeType(VT_BSTR) != S_OK)
			//	{
			//		throw Error(E_INVALIDARG, L"Invalid parameter type for Name");
			//	}

			//	// Trying to find an item by name
			//	pVar = Find(vName.bstrVal);

			//	if (pVar == NULL)
			//	{
			//		CCXMLString s = L"Member '" + CCXMLString(vName.bstrVal) + L"' not found";
			//		throw Error(DISP_E_MEMBERNOTFOUND, s.c_str());
			//	}
			//}

			// Now let's actually get or set VAR*

			// Get
			if (wFlags & DISPATCH_PROPERTYGET)
			{
				// Check pVarResult pointer
				if (::IsBadReadPtr(pvarResult, sizeof(VARIANT)))
				{
					throw Error(DISP_E_MEMBERNOTFOUND, L"Invalid pVarResult parameter");
				}

				// Copy VAR to pVarResult

				// Note!!! If this value is VT_BYREF, than it is actually stored
				// in another name table (scope), and here we have only reference to it.
				// So, modify it only in referenced table.
				VARIANT& val = pVar->vValue;
				if (val.vt & VT_BYREF)
				{
					::VariantCopy(pvarResult, val.pvarVal);
				}
				else
				{
					::VariantCopy(pvarResult, &val);
				}
			}
			// Set
			else if (wFlags & DISPATCH_PROPERTYPUT)
			{
				// Need second arg as assignment value
				if (pdispparams->cArgs < 1)
				{
					throw Error(E_INVALIDARG, L"Invalid number of parameters");
				}

				// Check supplied value pointer
				if (::IsBadReadPtr(&pdispparams->rgvarg[0], sizeof(VARIANT)))
				{
					throw Error(E_INVALIDARG, L"Invalid assignment r-value");
				}

				try
				{
					// Set VAR value
					pVar->SetValue(&pdispparams->rgvarg[0]);
				}
				catch (std::exception& e)
				{
					// Cannot set it
					throw Error(DISP_E_EXCEPTION, CComVariant(e.what()).bstrVal);
				}
			}
			return S_OK;
			/*return __super::Invoke( dispidMember, riid, lcid,
				wFlags, pdispparams, pvarResult, pexcepinfo, puArgErr);*/
		}
		catch (Error& err)
		{
			// Check pExcepInfo structure pointer
			if (::IsBadReadPtr(pexcepinfo, sizeof(EXCEPINFO)))
			{
				return DISP_E_EXCEPTION;
			}

			// Fill pExcepInfo structure

			ZeroMemory(pexcepinfo, sizeof(EXCEPINFO));

			pexcepinfo->bstrDescription = ::SysAllocString(err.description.c_str());
			pexcepinfo->bstrSource = ::SysAllocString(L"");
			pexcepinfo->bstrHelpFile = ::SysAllocString(L"");
			pexcepinfo->scode = err.hr;

			return err.hr;
		}
		catch (...)
		{
			return DISP_E_EXCEPTION;
		}
	}

	/*VAR& CNameTable2::Add(const CCXMLString& sName, const CComVariant& val, bool bReadOnly)
	{
		return (m_Names[sName] = VAR(sName, val, bReadOnly));
	}

	bool CNameTable2::Del(const CCXMLString& sName)
	{
		NameContainer::iterator it = m_Names.find(sName);
		if (it != m_Names.end())
		{
			m_Names.erase(it);
			return true;
		}
		return false;
	}

	VAR* CNameTable2::Find(const CCXMLString& sName)
	{
		NameContainer::iterator it = m_Names.find(sName);
		return (it == m_Names.end()) ? NULL : &it->second;
	}

	VAR& CNameTable2::Lookup(const CCXMLString& sName)
	{
		NameContainer::iterator it = m_Names.find(sName);
		if (it == m_Names.end())
		{
			throw format_wstring(L"Name '%s' not found", sName.c_str());
		}
		return it->second;
	}

	CMessage CNameTable2::ToMsg() const
	{
		CMessage msg;
		NameContainer::const_iterator it = m_Names.begin();
		for (; it != m_Names.end(); ++it)
		{
			msg[it->first.c_str()] = it->second.vValue;
		}
		return msg;
	}*/

	bool CNameTable2::AddAll(CNameTable* parent)
	{
		/*CNameTable::NameContainer::iterator iter = parent->m_Names.begin();
		for (unsigned int i=0;i<parent->m_Names.size();++i)
		{
			Add(iter->first,iter->second.vValue,false);
			++iter;
		}*/
		if (parent)
			m_event = parent;
		return m_event?true:false;
	}
}