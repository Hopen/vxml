/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include "CCXMLNameTable.h"
#include "EvtModel.h"
#include "..//EventBridge.h"

namespace CCXML
{
	class CEvent : public CNameTable
	{
	private:
		CCXMLString	m_sName;
	public:
		CEvent(const CMessage& msg);
		CEvent(const CEvent& rhs);
		//CMessage ToMsg() const;
		bool MatchMask(const CCXMLString& sMask);
		const CCXMLString& GetName() const;
		//__declspec(property(get=GetName)) CCXMLString Name;
	};
}
