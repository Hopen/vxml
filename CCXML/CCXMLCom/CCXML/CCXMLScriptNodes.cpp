/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include "StdAfx.h"
#include "CCXMLScriptNodes.h"
#include "CCXMLExceptions.h"

namespace CCXML
{
	const char IntConstStr[] = "%d";
	const char HexConstStr[] = "&H%x&";
	const char OctConstStr[] = "&O%o&";

	CComVariant CNode_UnaryExpr::Calculate()
	{
		CComVariant v = pExpr->Calculate();
		CComVariant vres;
		HRESULT hr = pUnaryFn(&v, &vres);
		if (FAILED(hr))
		{
//			ThrowVariantOpException(__FILEW__, __LINE__, __FUNCTIONW__, v);
		}
		return v;
	}

	CComVariant CNode_BinaryExprOp::Calculate()
	{
		CComVariant v1 = pLeftExpr->Calculate();
		CComVariant v2 = pRightExpr->Calculate();
		CComVariant vres;
		HRESULT hr = pBinaryFn(&v1, &v2, &vres);
		if (FAILED(hr))
		{
//			ThrowVariantOpException(__FILEW__, __LINE__, __FUNCTIONW__, v1, v2);
		}
		return vres;
	}

	CComVariant CNode_CompareExpr::Calculate()
	{
		CComVariant v1 = pLeftExpr->Calculate();
		CComVariant v2 = pRightExpr->Calculate();
		HRESULT hr = ::VarCmp(&v1, &v2, MAKELANGID(LANG_ENGLISH,SUBLANG_DEFAULT));

		if (hr != VARCMP_NULL)
		{
			switch (hr)
			{
				case VARCMP_LT:
				case VARCMP_GT:
				case VARCMP_EQ:
					return CComVariant(bNegate ? (hr != nRelation) : (hr == nRelation));

				default:
					break;
		//			ThrowVariantOpException(__FILEW__, __LINE__, __FUNCTIONW__, v1, v2);
			}
		}

		return CComVariant();
	}

	// ��, ��� ��� ����� �� Is � ��� ��� �������������.
	// �� ������, ���������� True, ���� ��� ��������� ��������� �� ���� � ��� �� ������.
	CComVariant CNode_IsExpr::Calculate()
	{
		CComVariant v1 = pLeftExpr->Calculate();
		CComVariant v2 = pRightExpr->Calculate();

		if (v1.vt == VT_DISPATCH && v2.vt == VT_DISPATCH)
		{
			return CComVariant(v1.pdispVal == v1.pdispVal);
		}

		return CComVariant(false);
	}
}
