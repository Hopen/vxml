/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include "..\StdAfx.h"
#include "CCXMLEvents.h"
#include "CCXMLNameTable.h"
// For PathMatchSpecW
#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

namespace CCXML
{
	CEvent::CEvent(const CMessage& msg)
		: CNameTable(msg.GetName()), m_sName(msg.Name)//, m_busy(false)//, m_typeNameTable(L"CEvent")
	{
		//m_typeNameTable = L"CEvent";
		// Convert message to event
		this->Add(L"name", CComVariant(msg.GetName()), true);

		//ParamsList parlist = msg.Params();
		//ParamsList::const_iterator it = parlist.begin();
		//for (; it != parlist.end(); ++it)
		//{
		//	const CParam& p = *it;
		//	this->Add(p.Name, p.Value, true);
		//}
		for (const auto& param : msg.Params())
		{
			this->Add(param.Name, CComVariant(), true);
			this->SetValue(param.Name, param.Value);
		}
	}

	CEvent::CEvent(const CEvent& rhs) : CNameTable(rhs.GetName())
	{
		this->Clone(rhs);
		m_sName = rhs.m_sName;
		//m_busy  = rhs.m_busy;
	}

	//CEvent& CEvent::operator = (const CEvent& _ev)
	//{
	//	m_sName = _ev.m_sName;
	//	m_busy	= _ev.m_busy;
	//	

	//	return *this;
	//}


	//CMessage CEvent::ToMsg() const
	//{
	//	// Convert event to message
	//	CMessage msg(__super::ToMsg());
	//	msg.Name = m_sName.c_str();
	//	return msg;
	//}

	bool CEvent::MatchMask(const CCXMLString& sMask)
	{
		// �� ������ ��� ���� �����-�� ���� ��������� ����� ������.
		// ��� ����� ��������, �� ������� ���� �� �������� ��������
		return ::PathMatchSpecW(m_sName.c_str(), sMask.c_str()) != 0;
	}

	const CCXMLString& CEvent::GetName() const
	{
		return m_sName;
	}
}
