/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include "..\StdAfx.h"
#include "CCXMLNameTable.h"
#include "sv_strutils.h"
#include "sv_utils.h"
#include "sid.h"
#include "..\EventBridge.h"
#include "..\Engine\EngineLog.h"
using namespace enginelog;

#include <boost/property_tree/xml_parser.hpp>
#include "Patterns/string_functions.h"
#include "Patterns\singleton.h"
#include "CCXMLResCache.h"

namespace CCXML
{
	VAR::VAR()
		: InstanceController(L"VAR", L"c:\\is3\\logs\\ccxml\\ccxml_temp_%y-%m-%d.log")
		, bIsEmpty(true)
		, sName(L"technical empty var")
	{
		LogDefaultCtor();
	}

	VAR::VAR(const CCXMLString& name, const CComVariant& value, bool readonly)
		: InstanceController(L"VAR", L"c:\\is3\\logs\\ccxml\\ccxml_temp_%y-%m-%d.log")
		, sName(name)
		, vValue(value)
		, bReadOnly(readonly)
		, bIsEmpty(false)
	{
		LogDefaultCtor();
		LogPlace(L"Name: %s", name.c_str());
		//SetInitialValue(value);
	}

	VAR::VAR(const VAR& rhs)
		: InstanceController(L"VAR", L"c:\\is3\\logs\\ccxml\\ccxml_temp_%y-%m-%d.log")
	{
		//SetInitialValue(rhs.vValue);
		this->vValue = rhs.vValue;
		this->sName = rhs.sName;
		this->bReadOnly = rhs.bReadOnly;
		this->bIsEmpty = rhs.bIsEmpty;

		LogCopyCtor();
		LogPlace(L"Name: %s", sName.c_str());

	}

	VAR & VAR::operator=(VAR const & rhs)
	{
		if (this == &rhs) {
			return *this;
		}
		this->vValue = rhs.vValue;
		//SetInitialValue(rhs.vValue);
		this->sName = rhs.sName;
		this->bReadOnly = rhs.bReadOnly;
		this->bIsEmpty = rhs.bIsEmpty;

		return *this;
	}


	VAR::~VAR()
	{
		vValue.Clear();

		LogDtor();
		LogPlace(L"Name: %s", sName.c_str());
	}

	//void VAR::SetInitialValue(const CComVariant& newVal)
	//{
	//	::VariantCopy(&vValue, (VARIANT*)&newVal);
	//}

	//void VAR::SetValue(VARIANT* pNewVal)
	//{
	//	if (bReadOnly)
	//	{
	//		throw format_wstring(L"Cannot set value of read-only variable '%s'", sName.c_str());
	//	}
	//	if (vValue.vt & VT_BYREF)
	//	{
	//		::VariantCopy(vValue.pvarVal, pNewVal);
	//	}
	//	else
	//	{
	//		::VariantCopy(&vValue, pNewVal);
	//	}
	//}

	void VAR::SetValue(const CComVariant& newVal)
	{
		if (vValue.vt & VT_BYREF)
		{
			::VariantCopy(vValue.pvarVal, (VARIANT*)&newVal);
		}
		else
		{
			//::VariantCopy(&vValue, (VARIANT*)&newVal);
			vValue = newVal;
		}
	}


	// CScopedNameTable implementation

	CScopedNameTable::CScopedNameTable()
	{
		//m_pGlobalNames.reset(new DEBUG_NEW_PLACEMENT CNameTable(L"GlobalNames"));
	}

	CScopedNameTable::~CScopedNameTable()
	{
		//m_gStack.clear();
	}

	void CScopedNameTable::PushScope(const CCXMLString& sName)
	{
		SessionObject pNameTbl;
		pNameTbl.reset(new DEBUG_NEW_PLACEMENT CNameTable(sName));

		std::lock_guard<std::mutex> lock(m_mutex);
		if (!m_Scopes.empty())
		{
			ScopePair& scope = *m_Scopes.rbegin();
			SessionObject& curScope = scope.second;
			pNameTbl->Clone(*curScope.get());
		}
		//m_pGlobalNames->Add(sName, pNameTbl->AsVariant(), true);
		// Add the name to the scope list
		m_Scopes.push_back(ScopePair(sName, pNameTbl));
	}

	void CScopedNameTable::PopScope()
	{
		std::lock_guard<std::mutex> lock(m_mutex);
		if (m_Scopes.empty())
		{
			return;
		}

		m_Scopes.pop_back();
	}

	//void CScopedNameTable::PushStack()
	//{
	//	std::lock_guard<std::mutex> lock(m_mutex);
	//	SessionObject copy_globals(new DEBUG_NEW_PLACEMENT CNameTable(L"copy_globals"));
	//	copy_globals->Clone(*m_pGlobalNames.get());

	//	m_gStack.push_back(m_pGlobalNames);
	//	m_pGlobalNames = copy_globals;
	//}

	//void CScopedNameTable::PopStack()
	//{
	//	std::lock_guard<std::mutex> lock(m_mutex);

	//	if (!m_gStack.size())
	//		return;
	//	m_pGlobalNames = m_gStack[m_gStack.size() - 1];
	//	m_gStack.pop_back();
	//}

	const VAR CScopedNameTable::AddVar(const CCXMLString& sName,
		const CComVariant& val,
		bool bReadOnly)
	{
		std::lock_guard<std::mutex> lock(m_mutex);

		if (m_Scopes.empty())
		{
			// Add the variable to the global name table
			//return m_pGlobalNames->Add(sName, val, bReadOnly);
			throw std::runtime_error(std::string("Empty scopes when try to add: ") + wtos(sName));

		}

		//// Add the variable to the current scope
		//CComVariant refval = m_Scopes.rbegin()->second->AddRef(sName, val, bReadOnly);
		return m_Scopes.rbegin()->second->Add(sName, val, bReadOnly);
		//// If the reference already exists, it is updated, so if there is the variable
		//// with the same name in several different scopes, it is always visible in
		//// the global name table as that in the current scope.
		//return m_pGlobalNames->Add(sName, refval, bReadOnly);
	}

	//void CScopedNameTable::DelVar(const CCXMLString& sName)
	//{
	//	std::lock_guard<std::mutex> lock(m_mutex);

	//	if (m_Scopes.empty())
	//	{
	//		// Delete the variable from the global name table
	//		m_pGlobalNames->Del(sName);
	//	}
	//	else
	//	{
	//		// Delete the variable from the current scope
	//		m_Scopes.rbegin()->second->Del(sName);
	//		m_pGlobalNames->Del(sName);
	//	}
	//}

	const VAR CScopedNameTable::GetValue(const CCXMLString & sName)
	{
		std::lock_guard<std::mutex> lock(m_mutex);

		if (m_Scopes.empty())
		{
			// Lookup the variable in the global name table
			//return m_pGlobalNames->GetSafeValue(sName);
			throw std::runtime_error(std::string("Empty scopes when try to get value: ") + wtos(sName));
		}
		// Lookup the variable in the current scope
		return m_Scopes.rbegin()->second->GetSafeValue(sName);

	}

	//CComVariant CScopedNameTable::GetGlobal()const
	//{
	//	std::lock_guard<std::mutex> lock(m_mutex);

	//	return m_pGlobalNames->AsVariant();
	//}


}