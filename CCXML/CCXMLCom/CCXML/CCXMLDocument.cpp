/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#include "..\StdAfx.h"
#include "CCXMLDocument.h"
#include "sv_strutils.h"
#include "sv_utils.h"
//#include "..\CacheInterface\factory.h"

namespace CCXML
{
	CDocument::CDocument(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory):
		m_pRootTag(std::make_shared<CTag_ccxml>(_parser, _factory))
	{
	}

	CDocument::~CDocument()
	{
	}

	void CDocument::Execute(CExecContext* pCtx)
	{
		m_pRootTag->Execute(pCtx);
	}
}
