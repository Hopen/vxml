/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include "sid.h"
#include "CCXMLNameTable.h"
#include "CCXMLEvents.h"
//#include "..\Common\factory.h"
#include "..\Common\factory_c.h"
#include <atlsync.h>

//#import "MsXml6.dll"

namespace CCXML
{
	class CTagFactory			;

	class CTag_accept           ;
	class CTag_assign           ;
	class CTag_cancel           ;
	class CTag_ccxml            ;
	class CTag_createcall       ;
	class CTag_createccxml      ;
	class CTag_createconference ;
	class CTag_destroyconference;
	class CTag_dialogprepare    ;
	class CTag_dialogstart      ;
	class CTag_dialogterminate  ;
	class CTag_disconnect       ;
	class CTag_else             ;
	class CTag_elseif           ;
	class CTag_eventprocessor   ;
	class CTag_exit             ;
	class CTag_fetch            ;
	class CTag_goto             ;
	class CTag_if               ;
	class CTag_join             ;
	class CTag_log              ;
	class CTag_merge            ;
	class CTag_move             ;
	class CTag_redirect         ;
	class CTag_reject           ;
	class CTag_script           ;
	class CTag_send             ;
	class CTag_transition       ;
	class CTag_unjoin           ;
	class CTag_var              ;

	class CTag: public Factory_c::CFTag
	{
	protected:
		//CComVariant EvalAttr(
		//	CExecContext* pCtx, 
		//	const CCXMLString& sAttrName, 
		//	const CComVariant& defVal, 
		//	VARTYPE evalType);

		CComVariant EvalAttrEvent(
			CExecContext* pCtx, 
			const CCXMLString& sAttrName, 
			VARTYPE evalType);

		//ObjectId EvalAttrEvent(
		//	CExecContext* pCtx, 
		//	const CCXMLString& sAttrName);

		ObjectId GetConnectionId(CExecContext* pCtx);
		ObjectId GetConferenceId(CExecContext* pCtx);

	public:
		CTag(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory);
		CTag::~CTag();

		virtual bool Execute(CExecContext* pCtx) = NULL;
		void EmitError(CExecContext* pCtx, CMessage& evt, const CCXMLString& description, const CCXMLString& reason);

	protected:
		int m_count;
	};

	using TagPtr = std::shared_ptr<CTag>;
	class CParentTag: public CTag
	{
	protected:
		using TagContainer = std::list<TagPtr>;
		TagContainer m_Children;

	public:
		CParentTag(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory);
		~CParentTag();
		virtual bool Execute(CExecContext* pCtx);
		bool ExecuteChild(CExecContext* pCtx, TagPtr pTag);

	};

	class CTextTag : public CTag
	{
	public:
		CTextTag(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		const CCXMLString& GetText() const;
	};

	class CTag_accept : public CTag
	{
	public:
		CTag_accept(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory);// : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_cancel : public CTag
	{
	public:
		CTag_cancel(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_ccxml : public CParentTag
	{
	public:
		CTag_ccxml(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CParentTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_createcall : public CTag
	{
	public:
		CTag_createcall(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_createccxml : public CTag
	{
	public:
		CTag_createccxml(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_createconference : public CTag
	{
	public:
		CTag_createconference(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_destroyconference : public CTag
	{
	public:
		CTag_destroyconference(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_dialogprepare : public CTag
	{
	public:
		CTag_dialogprepare(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_dialogstart : public CTag
	{
	public:
		CTag_dialogstart(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_dialogterminate : public CTag
	{
	public:
		CTag_dialogterminate(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_disconnect : public CTag
	{
	public:
		CTag_disconnect(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_else : public CTag
	{
	public:
		CTag_else(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_elseif : public CTag
	{
	public:
		CTag_elseif(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_eventprocessor : public CParentTag
	{
	public:
		CTag_eventprocessor(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CParentTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
		bool SetEvent(CExecContext* pCtx, EventObject pEvt);
	};

	class CTag_exit : public CTag_disconnect
	{
	public:
		CTag_exit(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag_disconnect(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_fetch : public CTag
	{
	public:
		CTag_fetch(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_goto : public CTag
	{
	public:
		CTag_goto(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_if : public CParentTag
	{
	public:
		CTag_if(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CParentTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_join : public CTag
	{
	public:
		CTag_join(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_log : public CTag
	{
	public:
		CTag_log(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_merge : public CTag
	{
	public:
		CTag_merge(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_move : public CTag
	{
	public:
		CTag_move(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_redirect : public CTag
	{
	public:
		CTag_redirect(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_reject : public CTag
	{
	public:
		CTag_reject(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_script : public CTextTag
	{
	public:
		CTag_script(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTextTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_send : public CTextTag
	{
	public:
		CTag_send(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTextTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_transition : public CParentTag
	{
	public:
		CTag_transition(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CParentTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
		bool MatchEvent(CExecContext* pCtx, const CCXMLString& sState, EventObject pEvt);
	};

	class CTag_unjoin : public CTag
	{
	public:
		CTag_unjoin(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};

	class CTag_var : public CTag
	{
	public:
		CTag_var(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag(_parser, _factory) { }
		virtual bool Execute(CExecContext* pCtx);
	};


	class CTag_assign : public CTag_var
	{
	public:
		CTag_assign(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory) : CTag_var(_parser, _factory) { }
	};

	class CTagFactory
	{
	private:
		using TagFunc = TagPtr(*)(const Factory_c::CollectorPtr& _parser, const CTagFactory& _factory);
		using TagsMap = std::map<CCXMLString, TagFunc>;

		TagsMap m_TagsMap;
	public:
		CTagFactory::CTagFactory();
		TagPtr CreateTag(
			const CCXMLString& sName, 
			const Factory_c::CollectorPtr& _parser,
			const CTagFactory& _factory) const;
	};

	bool ListContains(const CCXMLString& sList, const CCXMLString& sItem);
}

