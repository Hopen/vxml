/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#pragma once

#include "..\StdAfx.h"
#include "CCXMLResCache.h"
#include "CCXMLDocument.h"
#include "CCXMLSession.h"
#include "..\CacheInterface\CacheInterface_i.c"


namespace CCXML
{

	CResourceCache::CResourceCache()
		: m_SchemaFile((SVUtils::ExtractFilePath(SVUtils::GetModuleFileName(g_hInst)) + L"ccxml.xsd").c_str())
	{
		m_hr = m_Res.CoCreateInstance(CLSID_ResourceCache);
		
	}

	CResourceCache::~CResourceCache()
	{
	}

	HRESULT CResourceCache::GetDocument(const CCXMLString& sSrc, const CCXMLString& sScriptID, const bool& bFile, SAFEARRAY** pSA)
	{
		if (!m_Res)
			return E_FAIL;

		return m_Res->GetDocument(_bstr_t(sSrc.c_str()), m_SchemaFile, _bstr_t(sScriptID.c_str()), bFile, pSA);
	}

	bool CResourceCache::Ready()const
	{
		return !!m_Res.p;
	}

	bool CResourceCache::TestReady()const
	{
		return !!m_Test.p;
	}

	HRESULT CResourceCache::WriteTestLog(const CCXMLString& sSrc)
	{
		if (!m_Test)
			return E_FAIL;
		return m_Test->WriteLog(_bstr_t(sSrc.c_str()));
	}
}
