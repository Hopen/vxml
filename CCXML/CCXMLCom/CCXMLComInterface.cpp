// CCXMLComInterface.cpp : Implementation of CCCXMLComInterface

#include "stdafx.h"
#include <algorithm>
#include "CCXMLBase.h"
#include "CCXMLComInterface.h"
#include "CCXMLResCache.h"
#include "sv_helpclasses.h"
#include "ce_xml.hpp"
#include "sv_utils.h"
//#include "TelServerEmulation.h"
#include "CCXMLDebug.h"
#include "EngineConfig.h"
#include "CCXMLNameTable.h"
#include "sid.h"
#include <boost/weak_ptr.hpp>
using namespace enginelog;

#include <boost/property_tree/xml_parser.hpp>
#include "Patterns/string_functions.h"

class CCCXMLEngineConfig2 : public static_singleton < CCCXMLEngineConfig2 >
{
	friend class static_singleton < CCCXMLEngineConfig2 >;

private:
	CCCXMLEngineConfig2()
	{
		try
		{
			m_sFileName = SVUtils::GetModuleFileName(g_hInst) + L".config";
			std::wifstream _in(m_sFileName.c_str(), std::ios::binary);


			boost::property_tree::wiptree propertyTree;
			boost::property_tree::read_xml(_in, propertyTree);
			const boost::property_tree::wiptree& log    = propertyTree.get_child(L"configuration").get_child(L"Log");
			const boost::property_tree::wiptree& folder = propertyTree.get_child(L"configuration").get_child(L"Folder");
			const boost::property_tree::wiptree& system = propertyTree.get_child(L"configuration").get_child(L"System");

			//propertyTree.get<std::wstring>("configuration/Log/FileNameGenerator.<xmlattr>.Format")

			this->sLogFileName    = log.get<std::wstring>(L"FileNameGenerator.<xmlattr>.Format");
			this->m_sExtraLogName = log.get<std::wstring>(L"FileNameGenerator.<xmlattr>.Extra");
			this->m_sVBSLogName = log.get<std::wstring>(L"FileNameGenerator.<xmlattr>.VBS");
			this->sLevel          = log.get<std::wstring>(L"FileNameGenerator.<xmlattr>.Level");// m_config.attr(L"//configuration/Log/FileNameGenerator/Level");
			this->sCCXMLFolder    = folder.get<std::wstring>(L"CCXMLFolder.<xmlattr>.Value");// m_config.attr(L"//configuration/Folder/CCXMLFolder/Value");
			this->sVXMLFolder     = folder.get<std::wstring>(L"VXMLFolder.<xmlattr>.Value"); //m_config.attr(L"//configuration/Folder/VXMLFolder/Value");

			//this->Systems.sKeepAliveTimeout = system.get<std::wstring>(L"KeepAliveTimeout.<xmlattr>.Value"); //m_config.attr(L"//configuration/System/KeepAliveTimeout/Value");

			_in.close();
		}
		catch (boost::property_tree::xml_parser_error error)
		{
			//singleton_auto_pointer <CScriptIDHolder> holder;
			__int64 iScriptID = GetGlobalScriptId();

			CEngineLog pLOG;

			pLOG.Init(L"finest", L"c:\\is3\\logs\\ccxml\\ccxml_config_%y-%m-%d.log");
			pLOG.SetSID(iScriptID);

			std::wstring msg = _bstr_t(error.message().c_str());
			pLOG.Log(LEVEL_INFO, __FUNCTIONW__, L"Config XML parser error: \"%s\"", msg.c_str());
			//std::cout << "XML parser error!" << std::endl;
			throw std::wstring(L"XML parser error!");
		}
	}

public:
	std::wstring sCCXMLFolder;
	std::wstring sVXMLFolder;
	//CCXML::TSystem Systems;

	std::wstring sLogFileName;
	std::wstring m_sExtraLogName;
	std::wstring m_sVBSLogName;
	std::wstring sLevel;

private:
	std::wstring m_sFileName;


};

// CCCXMLComInterface
HRESULT CCCXMLComInterface::FinalConstruct()
{
	bool isException = false;
	std::wstring reason = L"Unknown";
	try
	{
		m_Log.reset(new CEngineLog());
		m_extraLog.reset(new CEngineLog());
		m_vbsLog.reset(new CEngineLog());
		m_pVar.reset(new DEBUG_NEW_PLACEMENT CCXML::CScopedNameTable());
		m_pSes.reset(new DEBUG_NEW_PLACEMENT CCXML::CSession());
	}
	catch (std::exception& error)
	{
		isException = true;
		reason = stow(error.what());
	}
	catch (...)
	{
		isException = true;
	}

	if (isException)
	{
		CEngineLog log;
		log.Init(L"finest", L"C:\\IS3\\Logs\\ccxml_extra_%y-%m-%d.log");

		log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"CCXML final construct exception: %s", reason.c_str());

		Error(L"CCXML final construct exception", GUID_NULL, E_FAIL);
		return E_FAIL;
	}

	return S_OK;
}

void CCCXMLComInterface::FinalRelease()
{
	bool isException = false;
	std::wstring reason = L"Unknown";

	try
	{
		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"CCXML FinalRelease...");
		m_pDebuger.reset();
		m_pSes.reset();
		m_pVar.reset();
		m_Log.reset();
		m_extraLog.reset();
	}
	catch (std::exception& error)
	{
		isException = true;
		reason = stow(error.what());
	}
	catch (...)
	{
		isException = true;
	}

	if (isException)
	{
		CEngineLog log;
		log.Init(L"finest", L"C:\\IS3\\Logs\\ccxml_extra_%y-%m-%d.log");

		log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"CCXML final release exception: %s", reason.c_str());

		Error(L"CCXML final release exception", GUID_NULL, E_FAIL);
	}

}

STDMETHODIMP CCCXMLComInterface::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_ICCXMLComInterface
	};

	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP CCCXMLComInterface::Init(PMESSAGEHEADER pMsgHeader, 
									  IEngineCallback* pCallback)
{
	try
	{
		if (true)
		{
			std::lock_guard<std::mutex> lock(m_mutex);
			CCCXMLEngineConfig2::GetInstance();
		}
		// Log initialization
		m_Log->Init(CCCXMLEngineConfig2::GetInstance()->sLevel.c_str(), CCCXMLEngineConfig2::GetInstance()->sLogFileName.c_str());
		m_extraLog->Init(CCCXMLEngineConfig2::GetInstance()->sLevel.c_str(), CCCXMLEngineConfig2::GetInstance()->m_sExtraLogName.c_str());
		m_vbsLog->Init(CCCXMLEngineConfig2::GetInstance()->sLevel.c_str(), CCCXMLEngineConfig2::GetInstance()->m_sVBSLogName.c_str(), true);

		CMessage msg(pMsgHeader);
		CParam* pSID = msg.ParamByName(L"ScriptID");

		// Log initialization
		m_Log->SetSID(pSID->AsInt64());
		m_extraLog->SetSID(pSID->AsInt64());
		m_vbsLog->SetSID(pSID->AsInt64());

		//singleton_auto_pointer <CScriptIDHolder> holder;
		//holder->SetScriptID(pSID->AsInt64());
		SetGlobalScriptId(pSID->AsUInt64());

		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Initializing CCXML session...");
		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, msg.Dump().c_str());

		m_pDebuger.reset(new DEBUG_NEW_PLACEMENT CCXML::CCXMLDebugBeholder(pCallback, this));

		CCXML::SessionCreateParams params;

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CCCXMLComInterface: Initializing CCXML session...");

		params.pCallback	= pCallback;
		params.pEvtPrc		= m_pDebuger;//pCallback;
		params.pInitMsg		= pMsgHeader;
		params.pLog			= m_Log;
		params.pExtraLog	= m_extraLog;
		params.pVBSLogUrl   = CCCXMLEngineConfig2::GetInstance()->m_sVBSLogName.c_str();
		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CCCXMLComInterface: GetCurrentThreadId");
		params.dwSurrThread	= ::GetCurrentThreadId();
		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CCCXMLComInterface: Read folders");
		params.sCCXMLFolder = CCCXMLEngineConfig2::GetInstance()->sCCXMLFolder;
		params.sVXMLFolder  = CCCXMLEngineConfig2::GetInstance()->sVXMLFolder;
		//params.CCXMLSystem  = CCCXMLEngineConfig2::GetInstance()->Systems;
		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CCCXMLComInterface: get debuger");
		params.pDebuger		= m_pDebuger;
		params.pVar         = m_pVar;

		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CCCXMLComInterface: new CCXML::CSession");

		params.pSession = m_pSes;
		if (!m_pSes.get())
		{
			m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CCCXMLComInterface: CCXML session required");
			return E_FAIL;
		}
		if (!m_pSes->Initialize(params))
		{
			m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CCCXMLComInterface: CCXML session initializing failed");
			return E_FAIL;
		}
			
		m_Log->Log(LEVEL_FINEST, __FUNCTIONW__, L"CCCXMLComInterface: CCXML session initialized");
	}
	catch (std::wstring& e)
	{
		if (m_Log)
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Error: %s", e.c_str());
		return DISP_E_EXCEPTION;
	}
	catch (_com_error& e)
	{
		if (m_Log)
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Error: %s", (LPCWSTR)e.Description());
		return DISP_E_EXCEPTION;
	}
	catch (...)
	{
		if (m_Log)
			m_Log->Log(LEVEL_WARNING, __FUNCTIONW__, L"Error: unknown exception occurred");
		return DISP_E_EXCEPTION;
	}

	return S_OK;
}

STDMETHODIMP CCCXMLComInterface::Destroy()
{
	try
	{
		if (m_pSes.get())
		{
			if (m_Log.get())
			{
				m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"CCXML session destroyed");
			}
			
			return S_OK;
		}

		Error(L"CCXML session not initialized", GUID_NULL, E_FAIL);
	}
	catch (const std::exception& error)
	{
		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"Destroy EXCEPTION: %s", stow(error.what()).c_str());
	}
	catch (...)
	{
		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"UNHANDLED EXCEPTION while Destroy");
	}

	return E_FAIL;
}

//void DoErrorLog(std::wstring error)
//{
//	CEngineLog log;
//	log.Init(L"finest", L"C:\\IS3\\Logs\\ccxml_temp_%y-%m-%d.log");
//	log.SetSID(Global::g_scriptID);
//	
//	log.Log(LEVEL_INFO, __FUNCTIONW__, L"DoStep EXCEPTION: %s", error.c_str());
//}

STDMETHODIMP CCCXMLComInterface::DoStep()
{
	try
	{
		if (m_pSes.get() != NULL && m_pSes->IsInitialized() && m_pSes->GetState() != VMS_ENDSCRIPT)
		{
			m_pSes->DoStep();
		}

		return S_OK;
	}
	catch (const std::exception& error)
	{
		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"DoStep EXCEPTION: %s", stow(error.what()).c_str());
	}
	catch (...)
	{
		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"UNHANDLED EXCEPTION while DoStep");
	}
	return E_FAIL;
}

STDMETHODIMP CCCXMLComInterface::GetState()
{
	try
	{
		if (m_pSes.get() && m_pSes->IsInitialized())
		{
			return (HRESULT)m_pSes->GetState();
		}

		Error(L"CCXML session not initialized", GUID_NULL, E_FAIL);
	}
	catch (const std::exception& error)
	{
		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"GetState EXCEPTION: %s", stow(error.what()).c_str());
	}
	catch (...)
	{
		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"UNHANDLED EXCEPTION while GetState");
	}

	return E_FAIL;
}

STDMETHODIMP CCCXMLComInterface::SetEvent(PMESSAGEHEADER pMsgHeader)
{
	try
	{
		if (m_pSes.get() && m_pSes->IsInitialized() && m_pSes->GetState() != VMS_ENDSCRIPT)
		{
			CMessage msg(pMsgHeader);
			//m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"IN %s", __DumpMessage(CMessage(pMsgHeader)).c_str());
			m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"IN %s", msg.GetName());
			CCXML::__DumpMessage(msg, m_Log);

			m_pSes->OnAuxMsg(msg);
			return S_OK;
		}
		
		Error(L"CCXML session not initialized", GUID_NULL, E_FAIL);
	}
	catch (const std::exception& error)
	{
		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"SetEvent EXCEPTION: %s", stow(error.what()).c_str());
	}
	catch (...)
	{
		m_Log->Log(LEVEL_INFO, __FUNCTIONW__, L"UNHANDLED EXCEPTION while SetEvent");
	}

 	return E_FAIL;
}

STDMETHODIMP CCCXMLComInterface::GetNextDoStepInterval(LPDWORD lpdwInterval)
{
	*lpdwInterval = (DWORD)-1;
	//*lpdwInterval = 100;
	return S_OK;
}

STDMETHODIMP CCCXMLComInterface::Invoke(DISPID dispidMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS* pdispparams, VARIANT* pvarResult, EXCEPINFO* pexcepinfo, UINT* puArgErr)
{
	// TODO: Add your implementation code here

	return S_OK;
}

CCCXMLComInterface::CCCXMLComInterface()
{
}

CCCXMLComInterface::~CCCXMLComInterface()
{
}