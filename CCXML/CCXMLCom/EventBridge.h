// EventBridge.h : Declaration of the CNameTable

#pragma once
#include "resource.h"       // main symbols

#include <memory>
#include <mutex>
#include "CCXMLCom.h"
#include "CCXMLNameTable.h"

#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif

namespace CCXML
{
	class CNameTable:
		public InstanceController<CNameTable>,
		public std::enable_shared_from_this <CNameTable>
	{
	public:
		CNameTable(const std::wstring& aName);
		virtual ~CNameTable();

		// copy ctor
		CNameTable(const CNameTable& rhs) = delete;
		CNameTable& operator= (CNameTable const& rhs) = delete;

		private:
			struct wstringLessIC {
				bool operator()(const CCXMLString& _X, const std::wstring& _Y) const
				{
					return _wcsicmp(_X.c_str(), _Y.c_str()) < 0; 
				}
			};

			using NameContainer = std::map<std::wstring, VAR, wstringLessIC>;

			NameContainer	   m_Names;
			bool			   m_bActAsAssocArray;
			mutable std::mutex m_mutex;
			std::wstring	   m_name;
			//int                m_count;

	private:
		const VAR* GetAddressOf(const CCXMLString& sName) const;

		public:
			void SetValue(const CCXMLString& sName, const CComVariant& val);
			const VAR GetValue(const CCXMLString& sName) const;
			const VAR GetSafeValue(const CCXMLString& sName) const;
			VAR& GetUnsafeValue(const CCXMLString& sName);

			//const VAR& Add(const CCXMLString& sName, const CComVariant& val, bool bReadOnly);
			const VAR Add(const CCXMLString& sName, const CComVariant& val, bool bReadOnly);
			//const CComVariant AddRef(const CCXMLString& sName, const CComVariant& val, bool bReadOnly);
			bool Del(const CCXMLString& sName);
			//const VAR* Find(const CCXMLString& sName);
			//const VAR& Lookup(const CCXMLString& sName);
			CMessage ToMsg() const;
			CMessage ToMsg(EngineLogPtr2 aLog) const;
			void Clear();
			std::size_t GetSize() const{ return m_Names.size(); }
			void Clone(const CNameTable& rhs);

			template <class Predicate>
			void ForEach(Predicate pred)
			{
				std::for_each(m_Names.cbegin(), m_Names.cend(), pred);
			}

			template <class Predicate>
			CComVariant FindByPredicate(Predicate pred) const
			{
				CComVariant result;
				const auto cit = std::find_if(m_Names.cbegin(), m_Names.cend(), pred);
				if (cit != m_Names.cend())
				{
					result = cit->second.vValue.pdispVal;
				}

				return result;
			}


/*			void Copy(const CNameTable& nt)
			{
				std::copy(nt.m_Names.begin(), nt_ m_Names
			}*/

			//SessionObject First()
			//{
			//	m_it = m_Names.begin();
			//	if (m_it == m_Names.end())
			//		return SessionObject();
			//	return ((NameTableDispatchPtr)(m_it->second.vValue.pdispVal))->GetObjectPtr();
			//}
			//SessionObject Next()
			//{
			//	if (++m_it == m_Names.end())
			//		return SessionObject();
			//	return ((NameTableDispatchPtr)(m_it->second.vValue.pdispVal))->GetObjectPtr();
			//}

			//NameContainer::iterator m_it;

			//virtual CCXMLString typeNameTable()const{return m_typeNameTable;}
		HRESULT FinalConstruct()
		{
			return S_OK;
		}

		void FinalRelease()
		{
		}

		virtual CComVariant AsVariant()
		{
			try
			{
				return CComVariant(new CComBridge<CNameTable>(shared_from_this()));
			}
			catch (...)
			{
				throw std::runtime_error("EventBridge AsVariant exception");
			}
			return CComVariant();
		}
		
		//virtual const CComVariant AsVariant() const
		//{
		//	try
		//	{
		//		return CComVariant(new CComBridge<const CNameTable>(shared_from_this()));
		//	}
		//	catch (...)
		//	{
		//		throw std::runtime_error("EventBridge AsVariant exception");
		//	}
		//	return CComVariant();
		//}



	public:
		// IDispatch methods
		virtual HRESULT STDMETHODCALLTYPE GetTypeInfoCount( 
			/* [out] */ UINT *pctinfo);

		virtual HRESULT STDMETHODCALLTYPE GetTypeInfo( 
			/* [in] */ UINT iTInfo,
			/* [in] */ LCID lcid,
			/* [out] */ ITypeInfo **ppTInfo);

		STDMETHOD(GetIDsOfNames)(REFIID riid, LPOLESTR * rgszNames, UINT cNames, LCID lcid, DISPID * rgDispId);
		STDMETHOD(Invoke)(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS * pDispParams, VARIANT * pVarResult, EXCEPINFO * pExcepInfo, UINT * puArgErr);

		//// IUnknown methods
		//virtual HRESULT STDMETHODCALLTYPE QueryInterface( 
		//	/* [in] */ REFIID riid,
		//	/* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);

		//virtual ULONG STDMETHODCALLTYPE AddRef(void);

		//virtual ULONG STDMETHODCALLTYPE Release(void);
	};

}
