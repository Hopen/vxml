// EventBridge.cpp : Implementation of CNameTable

#include "stdafx.h"
#include "sv_strutils.h"
#include "EventBridge.h"

//#include "EngineConfig.h"
#include "..\Engine\EngineLog.h"
using namespace enginelog;

#include "sv_utils.h"
#include "sid.h"
#include <boost/property_tree/xml_parser.hpp>
#include "Patterns/string_functions.h"
#include "Patterns\singleton.h"
#include "CCXMLResCache.h"


namespace CCXML
{

	class CCCXMLEngineConfig2 : public static_singleton < CCCXMLEngineConfig2 >
	{
		friend class static_singleton < CCCXMLEngineConfig2 >;

	private:
		CCCXMLEngineConfig2()
		{
			try
			{
				m_sFileName = SVUtils::GetModuleFileName(g_hInst) + L".config";
				std::wifstream _in(m_sFileName.c_str(), std::ios::binary);


				boost::property_tree::wiptree propertyTree;
				boost::property_tree::read_xml(_in, propertyTree);
				const boost::property_tree::wiptree& log = propertyTree.get_child(L"configuration").get_child(L"Log");
				const boost::property_tree::wiptree& folder = propertyTree.get_child(L"configuration").get_child(L"Folder");
				const boost::property_tree::wiptree& system = propertyTree.get_child(L"configuration").get_child(L"System");

				//propertyTree.get<std::wstring>("configuration/Log/FileNameGenerator.<xmlattr>.Format")

				this->sLogFileName = log.get<std::wstring>(L"FileNameGenerator.<xmlattr>.Format");
				this->m_sExtraLogName = log.get<std::wstring>(L"FileNameGenerator.<xmlattr>.Extra");
				this->m_sVBSLogName = log.get<std::wstring>(L"FileNameGenerator.<xmlattr>.VBS");
				this->sLevel = log.get<std::wstring>(L"FileNameGenerator.<xmlattr>.Level");// m_config.attr(L"//configuration/Log/FileNameGenerator/Level");
				this->sCCXMLFolder = folder.get<std::wstring>(L"CCXMLFolder.<xmlattr>.Value");// m_config.attr(L"//configuration/Folder/CCXMLFolder/Value");
				this->sVXMLFolder = folder.get<std::wstring>(L"VXMLFolder.<xmlattr>.Value"); //m_config.attr(L"//configuration/Folder/VXMLFolder/Value");

																							 //this->Systems.sKeepAliveTimeout = system.get<std::wstring>(L"KeepAliveTimeout.<xmlattr>.Value"); //m_config.attr(L"//configuration/System/KeepAliveTimeout/Value");

				_in.close();
			}
			catch (boost::property_tree::xml_parser_error error)
			{
				//singleton_auto_pointer <CScriptIDHolder> holder;
				__int64 iScriptID = GetGlobalScriptId();

				CEngineLog pLOG;

				pLOG.Init(L"finest", L"c:\\is3\\logs\\ccxml\\ccxml_config_%y-%m-%d.log");
				pLOG.SetSID(iScriptID);

				std::wstring msg = _bstr_t(error.message().c_str());
				pLOG.Log(LEVEL_INFO, __FUNCTIONW__, L"Config XML parser error: \"%s\"", msg.c_str());
				//std::cout << "XML parser error!" << std::endl;
				throw std::wstring(L"XML parser error!");
			}
		}

	public:
		std::wstring sCCXMLFolder;
		std::wstring sVXMLFolder;
		//CCXML::TSystem Systems;

		std::wstring sLogFileName;
		std::wstring m_sExtraLogName;
		std::wstring m_sVBSLogName;
		std::wstring sLevel;

	private:
		std::wstring m_sFileName;


	};

//
//static int CtorCount = 0;
//static int DtorCount = 0;

	CNameTable::CNameTable(const std::wstring& aName)
		: InstanceController(L"CNameTable", L"c:\\is3\\logs\\ccxml\\ccxml_temp_%y-%m-%d.log")
		, m_name(aName)
	{
		LogDefaultCtor();
		LogPlace(L"Name: %s", m_name.c_str());
	}

	void CNameTable::Clone(const CNameTable& rhs)
	{
		this->Clear();
		this->m_bActAsAssocArray = rhs.m_bActAsAssocArray;

		//std::lock_guard<std::mutex> lock(m_mutex);
		LockGuard<std::mutex> lock(L"CNameTable::Clone", m_mutex);
		NameContainer::const_iterator cit = rhs.m_Names.begin();
		while (cit != rhs.m_Names.end())
		{
			this->m_Names[cit->first] = cit->second;
			++cit;
		}
	}

	CNameTable::~CNameTable()
	{
		LogDtor();
		LogPlace(L"Name: %s", m_name.c_str());
	}


	void CNameTable::SetValue(const CCXMLString & sName, const CComVariant & val)
	{
		//std::lock_guard<std::mutex> lock(m_mutex);
		LockGuard<std::mutex> lock(L"CNameTable::SetValue", m_mutex);
		NameContainer::iterator it = m_Names.find(sName);
		if (it == m_Names.end())
		{
			throw std::runtime_error(std::string("Name \")" + wtos(sName) + "\" not found"));
		}
		it->second.SetValue(val);
	}

	const VAR CNameTable::GetValue(const CCXMLString & sName) const
	{
		//std::lock_guard<std::mutex> lock(m_mutex);
		LockGuard<std::mutex> lock(L"CNameTable::GetValue", m_mutex);
		const auto it = m_Names.find(sName);
		if (it == m_Names.end())
		{
			//throw format_wstring(L"Name '%s' not found", sName.c_str());
			throw std::runtime_error(std::string("CNameTable::GetValue: '") + wtos(sName) + "' not found");
		}

		return it->second;
	}

	const VAR CNameTable::GetSafeValue(const CCXMLString & sName) const
	{
		//std::lock_guard<std::mutex> lock(m_mutex);
		LockGuard<std::mutex> lock(L"CNameTable::GetSafeValue", m_mutex);
		const auto it = m_Names.find(sName);
		if (it == m_Names.end())
		{
			static VAR empty;
			return empty;
		}

		return it->second;
	}

	VAR& CNameTable::GetUnsafeValue(const CCXMLString & sName)
	{
		//std::lock_guard<std::mutex> lock(m_mutex);
		LockGuard<std::mutex> lock(L"CNameTable::GetUnsafeValue", m_mutex);
		auto it = m_Names.find(sName);
		if (it == m_Names.end())
		{
			//throw format_wstring(L"Name '%s' not found", sName.c_str());
			throw std::runtime_error(std::string("CNameTable::GetUnsafeValue: '") + wtos(sName) + "' not found");
		}

		return it->second;
	}

	const VAR* CNameTable::GetAddressOf(const CCXMLString & sName) const
	{
		//std::lock_guard<std::mutex> lock(m_mutex);
		LockGuard<std::mutex> lock(L"CNameTable::GetAddressOf", m_mutex);
		const auto it = m_Names.find(sName);
		if (it == m_Names.end())
		{
			return nullptr;
		}
		return &it->second;
	}

	const VAR CNameTable::Add(const CCXMLString& sName, const CComVariant& val, bool bReadOnly)
	{
		//std::lock_guard<std::mutex> lock(m_mutex);
		LockGuard<std::mutex> lock(L"CNameTable::Add", m_mutex);

		if (sName.empty())
		{
			throw std::runtime_error("Try add CNameTable variable with empty name");
		}

		//auto it = m_Names.find(sName);
		//if (it != m_Names.end())
		//{
		//	it->second.vValue = val;
		//	return it->second;
		//}

		VAR& var(m_Names[sName] = VAR(sName, val, bReadOnly));
		return var;
	}

	//const CComVariant CNameTable::AddRef(const CCXMLString& sName, const CComVariant& val, bool bReadOnly)
	//{
	//	//std::lock_guard<std::mutex> lock(m_mutex);
	//	LockGuard<std::mutex> lock(L"CNameTable::AddRef", m_mutex);

	//	auto it = m_Names.find(sName);
	//	if (it != m_Names.end())
	//	{
	//		throw std::runtime_error(std::string("CNameTable::AddRef VAR with the same name == \"") + wtos(sName) + "\" has alerady exist!");
	//	}

	//	VAR& var(m_Names[sName] = VAR(sName, val, bReadOnly));

	//	CComVariant refval;
	//	refval.vt = VT_VARIANT | VT_BYREF;
	//	refval.pvarVal = &var.vValue;

	//	return refval;
	//}


	//const VAR& CNameTable::Add(const CCXMLString& sName, const CComVariant& val, bool bReadOnly)
	//{
	//	return (m_Names[sName] = VAR(sName, val, bReadOnly));
	//}

	bool CNameTable::Del(const CCXMLString& sName)
	{
		//std::lock_guard<std::mutex> lock(m_mutex);
		LockGuard<std::mutex> lock(L"CNameTable::Del", m_mutex);
		NameContainer::iterator it = m_Names.find(sName);
		if (it != m_Names.end())
		{
			m_Names.erase(it);
			return true;
		}
		return false;
	}

	CMessage CNameTable::ToMsg() const
	{
		LockGuard<std::mutex> lock(L"CNameTable::ToMsg", m_mutex);
		CMessage msg(m_name.c_str());
		NameContainer::const_iterator it = m_Names.begin();
		for (; it != m_Names.end(); ++it)
		{
			msg[it->first.c_str()] = it->second.vValue;
		}
		return msg;
	}

	CMessage CNameTable::ToMsg(EngineLogPtr2 aLog) const
	{
		//std::lock_guard<std::mutex> lock(m_mutex);
		LockGuard<std::mutex> lock(L"CNameTable::ToMsg2", m_mutex);

		aLog->Log(LEVEL_FINEST, __FUNCTIONW__, L"CNameTable::ToMsg2: m_Names.size() \"%i\", Name: \"%s\"", m_Names.size(), m_name.c_str());

		CMessage msg(m_name.c_str());
		for (auto it = m_Names.begin(); it != m_Names.end(); ++it)
		{
			if (it->second.vValue.vt == VT_BSTR)
			{
				aLog->Log(LEVEL_FINEST, __FUNCTIONW__, L"Name: \"%s\", pointer: \"0x%p\", value: \"%s\"", it->first.c_str(), &(it->second.vValue), it->second.vValue.bstrVal);
			}

			else if (it->second.vValue.vt == VT_UI1
				|| it->second.vValue.vt == VT_UI2
				|| it->second.vValue.vt == VT_UI4
				|| it->second.vValue.vt == VT_UI8
				|| it->second.vValue.vt == VT_UINT)
			{
				aLog->Log(LEVEL_FINEST, __FUNCTIONW__, L"Name: \"%s\", pointer: \"0x%p\", value: \"%i\"", it->first.c_str(), &(it->second.vValue), it->second.vValue.ullVal);
			}
			else if (it->second.vValue.vt == VT_I2
				|| it->second.vValue.vt == VT_I4
				|| it->second.vValue.vt == VT_I1
				|| it->second.vValue.vt == VT_I8
				|| it->second.vValue.vt == VT_INT)
			{
				aLog->Log(LEVEL_FINEST, __FUNCTIONW__, L"Name: \"%s\", pointer: \"0x%p\", value: \"%i\"", it->first.c_str(), &(it->second.vValue), it->second.vValue.llVal);
			}
			else if (it->second.vValue.vt == VT_DISPATCH)
			{
				aLog->Log(LEVEL_FINEST, __FUNCTIONW__, L"ToMsg, DISPATCH ...");
				aLog->Log(LEVEL_FINEST, __FUNCTIONW__, L"ToMsg, DISPATCH Name: %s", it->first.c_str());

				NameTableDispatch* bridge = static_cast<NameTableDispatch*>(it->second.vValue.pdispVal);
				if (bridge)
				{
					CMessage bridgeMsg = bridge->ToMsg<CMessage>(aLog);
					msg[it->first.c_str()] = bridgeMsg.Dump();
				}
				continue;
			}
			else
			{
				aLog->Log(LEVEL_FINEST, __FUNCTIONW__, L"ToMsg, Name: %s", it->first.c_str());
			}

			//if (it->first == L"recipientid")
			//{
			//	aLog->Log(LEVEL_FINEST, __FUNCTIONW__, L"recipientid pointer: \"0x%p\", value: \"%s\"", &(it->second.vValue), it->second.vValue.bstrVal);
			//}

			msg[it->first.c_str()] = it->second.vValue;
		}
		return msg;
	}

	void CNameTable::Clear()
	{
		LockGuard<std::mutex> lock(L"CNameTable::Clear", m_mutex);
		//std::lock_guard<std::mutex> lock(m_mutex);
		m_Names.clear();
	}

	// CNameTable
	STDMETHODIMP CNameTable::GetTypeInfoCount( 
		/* [out] */ UINT *pctinfo)
	{
		if (::IsBadWritePtr(pctinfo, sizeof(*pctinfo)))
		{
			return E_INVALIDARG;
		}

		*pctinfo = 0;
		return S_OK;
	}

	STDMETHODIMP CNameTable::GetTypeInfo( 
		/* [in] */ UINT iTInfo,
		/* [in] */ LCID lcid,
		/* [out] */ ITypeInfo **ppTInfo)
	{
		return E_NOTIMPL;
	}

	STDMETHODIMP CNameTable::GetIDsOfNames(REFIID riid, LPOLESTR * rgszNames, UINT cNames, LCID lcid, DISPID * rgDispId)
	{
		// TODO: Add your implementation code here
		//CEngineLog log;
		//log.Init(L"finest", L"C:\\IS3\\Logs\\ccxml_temp_%y-%m-%d.log");
		//log.SetSID(Global::g_scriptID);

		HRESULT hRes = S_OK;
		//if(this->m_bActAsAssocArray)
		//{
		try
		{
			for (UINT i = 0; i < cNames; i++)
			{
				std::wstring name(static_cast<wchar_t*>(rgszNames[i]));
				//log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"GetIDsOfNames \"%s\"", name.c_str());

				const VAR* pVar = GetAddressOf(rgszNames[i]);
				if (pVar != nullptr)
				{
					rgDispId[i] = reinterpret_cast<DISPID>(pVar);
				}
				else
				{
					//log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"GetIDsOfNames DISPID_UNKNOWN");
					rgDispId[i] = DISPID_UNKNOWN;
					hRes = DISP_E_UNKNOWNNAME;
					break;
				}
			}
		}
		catch (std::exception& /*exception*/)
		{
			//log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"GetIDsOfNames Exception: \"%s\"", stow(exception.what()).c_str());
			hRes = E_OUTOFMEMORY;
		}
		catch (...)
		{
			//log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"GetIDsOfNames Unexpacted Exception");
			hRes = E_OUTOFMEMORY;
		}

		//}
		return hRes;//bUnkName?hRes :__super::GetIDsOfNames(riid, rgszNames, cNames, lcid, rgDispId);
	}

	STDMETHODIMP CNameTable::Invoke(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS * pDispParams, VARIANT * pVarResult, EXCEPINFO * pExcepInfo, UINT * puArgErr)
	{
		// TODO: Add your implementation code here
		//CEngineLog log;
		//log.Init(L"finest", L"C:\\IS3\\Logs\\ccxml_temp_%y-%m-%d.log");
		//log.SetSID(Global::g_scriptID);
		

			VAR* pVar = NULL;
			struct Error
			{
				HRESULT hr;
				CCXMLString description;
				Error(HRESULT _hr, LPCWSTR pwszDescr)
					: hr(_hr), description(pwszDescr)
				{
				}
			};

			try
			{

				if (dispIdMember)
				{
					pVar = reinterpret_cast<VAR*>(dispIdMember);
					if (::IsBadReadPtr(pVar, sizeof(VAR)))
					{
						throw Error(DISP_E_MEMBERNOTFOUND, L"Invalid dispIdMember parameter");
					}
				}
				else
				{

				}
				if(pVar && pVar->vValue.vt == VT_DISPATCH)
				{
					VARIANT& val = pVar->vValue;
					if (val.vt & VT_BYREF)
					{
						::VariantCopy(pVarResult, val.pvarVal);
					}
					else
					{
						::VariantCopy(pVarResult, &val);
					}
					return S_OK;
				}
				//// The result of this 'if' construct is initialized pVar variable
				//if (m_bActAsAssocArray)
				//{
				//	// If this is an associative array, VB should call it like 'arr("item_name")', 
				//	// so it calls Item method with 1 arg for GET and 2 args for SET

				//	// Our 'Item' ID is always 0
				//	if (dispidMember != 0)
				//	{
				//		throw Error(DISP_E_MEMBERNOTFOUND, L"Invalid dispIdMember parameter");
				//	}

				//	// Need at least 1 argument for item name
				//	if (pdispparams->cArgs < 1)
				//	{
				//		throw Error(E_INVALIDARG, L"Invalid number of parameters");
				//	}

				//	// Trying to get first arg - arguments are stored in reverse order
				//	VARIANT* pName = &pdispparams->rgvarg[pdispparams->cArgs - 1];
				//	if (::IsBadReadPtr(pName, sizeof(VARIANT)))
				//	{
				//		*puArgErr = 0;
				//		throw Error(E_INVALIDARG, L"Invalid parameter value for Name");
				//	}

				//	CComVariant vName(*pName);
				//	// Trying to cast item name to BSTR
				//	if (vName.ChangeType(VT_BSTR) != S_OK)
				//	{
				//		throw Error(E_INVALIDARG, L"Invalid parameter type for Name");
				//	}

				//	// Trying to find an item by name
				//	pVar = Find(vName.bstrVal);

				//	if (pVar == NULL)
				//	{
				//		CCXMLString s = L"Member '" + CCXMLString(vName.bstrVal) + L"' not found";
				//		throw Error(DISP_E_MEMBERNOTFOUND, s.c_str());
				//	}
				//}

				// Now let's actually get or set VAR*

				// Get
				if (wFlags & DISPATCH_PROPERTYGET)
				{
					// Check pVarResult pointer
					if (::IsBadReadPtr(pVarResult, sizeof(VARIANT)))
					{
						throw Error(DISP_E_MEMBERNOTFOUND, L"Invalid pVarResult parameter");
					}

					// Copy VAR to pVarResult

					// Note!!! If this value is VT_BYREF, than it is actually stored
					// in another name table (scope), and here we have only reference to it.
					// So, modify it only in referenced table.
					VARIANT& val = pVar->vValue;
					if (val.vt & VT_BYREF)
					{
						::VariantCopy(pVarResult, val.pvarVal);
					}
					else
					{
						::VariantCopy(pVarResult, &val);
					}
				}
				// Set
				else if (wFlags & DISPATCH_PROPERTYPUT)
				{
					// Need second arg as assignment value
					if (pDispParams->cArgs < 1)
					{
						throw Error(E_INVALIDARG, L"Invalid number of parameters");
					}

					// Check supplied value pointer
					if (::IsBadReadPtr(&pDispParams->rgvarg[0], sizeof(VARIANT)))
					{
						throw Error(E_INVALIDARG, L"Invalid assignment r-value");
					}

					try
					{
						// Set VAR value
						pVar->SetValue(&pDispParams->rgvarg[0]);
					}
					catch (std::exception& e)
					{
						// Cannot set it
						throw Error(DISP_E_EXCEPTION, CComVariant(e.what()).bstrVal);
					}
				}
				else 
				{
					//VARIANT& val = this->AsVariant();//CComVariant((IDispatch *)this);
					//::VariantCopy(pVarResult, &val);
					::VariantCopy(pVarResult, &AsVariant());
				}

				return S_OK;
				/*return __super::Invoke( dispidMember, riid, lcid,
					wFlags, pdispparams, pvarResult, pexcepinfo, puArgErr);*/
			}
			catch (Error& err)
			{
				// Check pExcepInfo structure pointer

				//log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"CNameTable::Invoke exception: \"%s\"", err.description.c_str());

				if (::IsBadReadPtr(pExcepInfo, sizeof(EXCEPINFO)))
				{
					return DISP_E_EXCEPTION;
				}

				// Fill pExcepInfo structure

				ZeroMemory(pExcepInfo, sizeof(EXCEPINFO));

				pExcepInfo->bstrDescription = ::SysAllocString(err.description.c_str());
				pExcepInfo->bstrSource = ::SysAllocString(L"");
				pExcepInfo->bstrHelpFile = ::SysAllocString(L"");
				pExcepInfo->scode = err.hr;

				return err.hr;
			}
			catch (...)
			{
				//log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"CNameTable::Invoke unxpacted exception");
				return DISP_E_EXCEPTION;
			}
	}

	//STDMETHODIMP CNameTable::QueryInterface( 
	//	/* [in] */ REFIID riid,
	//	/* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject)
	//{
	//	//return E_NOTIMPL;
	//	HRESULT retCode=S_OK;
	//	//ppvObject=0;
	//	if (IsEqualIID(riid,IID_IUnknown/*IID_IDispatch*/))
	//	{
	//		*ppvObject=this;
	//		AddRef();
	//	}
	//	else //retCode = E_NOINTERFACE;
	//	if (IsEqualIID(riid,IID_IDispatch))
	//	{
	//		*ppvObject=this;
	//		AddRef();
	//	}
	//	else retCode = E_NOINTERFACE;

	//	return retCode;
	//}

	//ULONG CNameTable::AddRef(void)
	//{
	//	return ++m_ulRefCount;
	//}

	//ULONG CNameTable::Release(void)
	//{
	//	if (!--m_ulRefCount)
	//	{
	//		delete this;
	//	}
	//	return m_ulRefCount;
	//}

}