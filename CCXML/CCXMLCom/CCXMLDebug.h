/************************************************************************/
/* Name     : CCXMLCom\CCXMLDebug.h                                     */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 21 Nov 2011                                               */
/************************************************************************/
#include <algorithm>
#include "sv_helpclasses.h"
#include "sv_sysobj.h"
#include "CCXMLSession.h"

namespace CCXML
{
	class IDebuger: public IEngineCallback
	{
	public:
		IDebuger(IEngineCallback* _callback, IEngineVM* _engine)
		{
			m_pCallBack = _callback ;
			m_pEngine   = _engine   ;
		}
	public: // interface
		virtual void Wait        (unsigned int _line/*, unsigned int _count = 1*/) = 0;
		virtual void Increase    (/*unsigned int _count = 1*/) = 0;
		virtual void SetAuxEvent (const CMessage& msg, bool _in) = 0;
		virtual void Init        (CMessage& init_msg, SessionPtr _session) = 0;
		virtual void Log         (CCXMLString _log       ) = 0;

	protected:
		IEngineCallback* m_pCallBack;
		IEngineVM*       m_pEngine;

	};

	class CCXMLDebugBeholder: public IDebuger
	{
		struct TBreakPoint
		{
			CCXMLString  filename;
			unsigned int line;
			CCXMLString  condition;
		};

		class CBreakPointByLineFinder
		{
		public:
			CBreakPointByLineFinder(unsigned int _line):m_line(_line)
			{
			}

			 bool operator()(const TBreakPoint _point)
			 {
				 return m_line == _point.line;
			 }

		private:
			unsigned int m_line;
		};

	public:
		CCXMLDebugBeholder(IEngineCallback* _callback, IEngineVM* _engine);
		virtual ~CCXMLDebugBeholder();

		// definition interface
		void Wait        ( unsigned int _line/*, unsigned int _count = 1*/ );
		void Increase    ( /*unsigned int _count = 1*/ );
		void SetAuxEvent (const CMessage& msg, bool _in );
		void Init        ( CMessage& init_msg, SessionPtr _session);
		void Log         ( CCXMLString _log        );
		void EndScript   ( const CCXMLString& _reason, const int& _code)const;
		// engine interface
		//void SetEvent(PMESSAGEHEADER pMsgHeader);
		BOOL WINAPI GetScriptName(BSTR* pbstrScriptName){return TRUE;}
		BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);

	private:
		void Add();
		void Release();
		void WaitOffline (unsigned int _line/*, unsigned int _count = 1*/);
		void WaitOnline  (unsigned int _line/*, unsigned int _count = 1*/);
		void SendToScriptoGen (CMessage& msg)const;
		std::wstring GetSymInfo(const std::wstring& _name)const;

		typedef std::list<TBreakPoint> BreakQueue;
	private:
		//unsigned int m_nCounter;
		SVSysObj::CEvent m_evtReady;
		SVSysObj::CEvent m_evtRun;
		SessionPtr       m_pSession;
		bool             m_debugflag;
		bool             m_forcebreak;
		ObjectId         m_ScriptoGenAddress;
		BreakQueue       m_BreakStack;
		bool             m_bWaitForRun;
	};
}
/******************************* eof *************************************/