

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Sat Jan 12 14:19:51 2019
 */
/* Compiler settings for CCXMLCom.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, IID_ICCXMLComInterface,0x0AB437C8,0x4091,0x4AB4,0x99,0xAD,0x5D,0x64,0xA7,0x96,0xDF,0x6D);


//MIDL_DEFINE_GUID(IID, IID_INameTable,0x57393A84,0x6249,0x4E26,0x82,0xA8,0x0E,0x33,0x9A,0x58,0x36,0x83);


MIDL_DEFINE_GUID(IID, LIBID_CCXMLComLib,0x0D09FA13,0x32AD,0x4284,0x94,0x17,0x41,0x27,0x6E,0x90,0x1D,0x16);


MIDL_DEFINE_GUID(CLSID, CLSID_CCXMLComInterface,0x43AD8055,0xB10A,0x4E70,0xB7,0x43,0xE2,0xF3,0xEC,0x58,0x8F,0x2A);


//MIDL_DEFINE_GUID(CLSID, CLSID_NameTable,0xD783F64E,0xAF9C,0x47D3,0x95,0x26,0x57,0x72,0x21,0xE1,0x04,0x57);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



