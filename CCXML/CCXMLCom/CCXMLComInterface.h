// CCXMLComInterface.h : Declaration of the CCCXMLComInterface

#pragma once
#include "resource.h"       // main symbols
#include <list>
#include <mutex>
#include "CCXMLCom.h"
#include "EvtModel.h"
#include "Engine.h"
#include "CCXMLSession.h"
#include "..\VXMLCom\VXMLCom_i.h"
#include "..\CacheInterface\CacheInterface_i.h"
//#ifdef _EMULATE_TELSERVER
//#include "TelServerEmulation.h"
//#endif

//#include "..\Common\KeepAlive.h"


#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Single-threaded COM objects are not properly supported on Windows CE platform, such as the Windows Mobile platforms that do not include full DCOM support. Define _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA to force ATL to support creating single-thread COM object's and allow use of it's single-threaded COM object implementations. The threading model in your rgs file was set to 'Free' as that is the only threading model supported in non DCOM Windows CE platforms."
#endif



// CCCXMLComInterface
class ATL_NO_VTABLE CCCXMLComInterface :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CCCXMLComInterface, &CLSID_CCXMLComInterface>,
	public ISupportErrorInfo,
	//public ICCXMLComInterface,
	public IDispatchImpl<ICCXMLComInterface, &IID_ICCXMLComInterface, &LIBID_CCXMLComLib, /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public IEngineVM
{
public:
	CCXML::SessionPtr2 m_pSes;

private:
	CCXML::EngineLogPtr2 m_Log;
	CCXML::EngineLogPtr2 m_extraLog;
	CCXML::EngineLogPtr2 m_vbsLog;

	CCXML::DebugerPtr2 m_pDebuger;

	CCXML::ScopedNameTablePtr  m_pVar;

	std::mutex m_mutex;

private:

public:
	CCCXMLComInterface();

	virtual ~CCCXMLComInterface();

DECLARE_REGISTRY_RESOURCEID(IDR_CCXMLCOM)

DECLARE_NOT_AGGREGATABLE(CCCXMLComInterface)

BEGIN_COM_MAP(CCCXMLComInterface)
	COM_INTERFACE_ENTRY(IEngineVM)
	COM_INTERFACE_ENTRY(ICCXMLComInterface)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct();
	
	void FinalRelease();

	// IEngineVM
public:
	STDMETHOD(Init)(PMESSAGEHEADER pMsgHeader, IEngineCallback* pCallback);
	STDMETHOD(Destroy)();
	STDMETHOD(DoStep)();
	STDMETHOD(GetState)();
	STDMETHOD(SetEvent)(PMESSAGEHEADER pMsgHeader);
	STDMETHOD(GetNextDoStepInterval)(LPDWORD lpdwInterval);

public:

	STDMETHOD(Invoke)(DISPID dispidMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS* pdispparams, VARIANT* pvarResult, EXCEPINFO* pexcepinfo, UINT* puArgErr);
};

OBJECT_ENTRY_AUTO(__uuidof(CCXMLComInterface), CCCXMLComInterface)


