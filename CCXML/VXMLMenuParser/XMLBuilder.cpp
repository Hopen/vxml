/************************************************************************/
/* Name     : VXMLMenuParser\XMLBuilder.cpp                             */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 16 Jul 2014                                               */
/************************************************************************/

#include "stdafx.h"
#include "XMLBuilder.h"
#include "EngineLog.h"
#include "ParserEngineConfig.h"

const CAtlString DIGITS = L"0123456789";

//boost::shared_ptr<CEngineLog>  CItem::m_pLog = NULL;
//int CItem::refCount = 0;

CItem::CItem(const std::wstring& _name, const std::wstring& _root, const std::wstring& _code, const std::wstring& _info) : m_name(_name), m_root(_root), m_code(_code), m_info(_info), m_pNode(NULL)
{
	/*if (!m_pLog)
	{
		m_pLog.reset(new CEngineLog());
		m_pLog->Init(CVXMLParserEngineConfig::GetInstance()->sLogFileName.c_str());
	}

	++refCount;

	m_pLog->Log(__FUNCTIONW__, _T("refCount: \"%i\""), refCount);*/
}

CItem::~CItem()
{
	/*int test = 0;
	--refCount;

	m_pLog->Log(__FUNCTIONW__, _T("refCount: \"%i\""), refCount);*/
}
CItem::CItem(const CItem& rhs)
{
	/*++refCount;*/
	m_name = rhs.m_name;
	m_root = rhs.m_root;
	m_code = rhs.m_code;
	m_info = rhs.m_info;
	m_parent_code = rhs.m_parent_code;
}

CComPtr < MSXML2::IXMLDOMNode > CItem::createNodeItem(CComPtr < MSXML2::IXMLDOMDocument > pDoc, const std::wstring & root_code)
{
	if (m_pNode == NULL)
	{
		MSXML2::IXMLDOMNodePtr pRecNode = NULL;
		HRESULT hr = pDoc->createNode(_variant_t((short)MSXML2::NODE_ELEMENT), _T("item"), _T(""), &pRecNode);

		if (SUCCEEDED(hr) && pRecNode)
		{
			std::wstring code, info;
			//if (!root_code.empty())
			//{
				code = /*root_code + */m_code;
				info= code[0];
				for (unsigned int i = 1; i < code.size(); ++i)
				{
					info += L".";
					info += code[i];
				}

				info += L" " + m_info;
			//}
			//else
			//{
			//	code = m_code;
			//	info = m_info;
			//}


			MSXML2::IXMLDOMElementPtr elem = static_cast<IXMLDOMElementPtr>(pRecNode);
			elem->setAttribute(L"code", _variant_t(code.c_str()));
			elem->setAttribute(L"item_name", _variant_t(info.c_str()));
			/*if (root_tag.GetLength())
			elem->setAttribute(L"item_root", _variant_t(root_tag));*/

			//CComPtr <MSXML2::IXMLDOMNode> pComRecNode(pRecNode);
			m_pNode = CComPtr <MSXML2::IXMLDOMNode>(pRecNode);
		}
	}

	return m_pNode;
}

//CComPtr < MSXML2::IXMLDOMNode > CItem::createNodeItem2(CComPtr < MSXML2::IXMLDOMDocument > pDoc, const std::wstring & root_code)
//{
//
//	CComPtr < MSXML2::IXMLDOMNode > pNode = NULL;
//
//	MSXML2::IXMLDOMNodePtr pRecNode = NULL;
//	HRESULT hr = pDoc->createNode(_variant_t((short)MSXML2::NODE_ELEMENT), _T("item"), _T(""), &pRecNode);
//
//	if (SUCCEEDED(hr) && pRecNode)
//	{
//		std::wstring code, info;
//		if (!root_code.empty())
//		{
//			code = root_code + m_code;
//			info = code[0];
//			for (unsigned int i = 1; i < code.size(); ++i)
//			{
//				info += L".";
//				info += code[i];
//			}
//
//			info += L" " + m_info;
//		}
//		else
//		{
//			code = m_code;
//			info = m_info;
//		}
//
//
//		MSXML2::IXMLDOMElementPtr elem = static_cast<IXMLDOMElementPtr>(pRecNode);
//		elem->setAttribute(L"code", _variant_t(code.c_str()));
//		elem->setAttribute(L"item_name", _variant_t(info.c_str()));
//		/*if (root_tag.GetLength())
//		elem->setAttribute(L"item_root", _variant_t(root_tag));*/
//
//		//CComPtr <MSXML2::IXMLDOMNode> pComRecNode(pRecNode);
//		pNode = CComPtr <MSXML2::IXMLDOMNode>(pRecNode);
//	}
//	
//
//	return pNode;
//}


//void CItem::appendParentInformation()
//{
//	m_code = m_parent_code + m_code;
//	std::wstring info;  info = m_code[0];
//	for (unsigned int i = 1; i < m_code.size(); ++i)
//	{
//		info += L".";
//		info += m_code[i];
//	}
//
//	m_info = info + L" " + m_info;
//}

//CItem::CItem(const std::wstring& _name, const std::wstring& _root, const std::wstring& _code, const std::wstring& _info)
//: m_name(_name), m_root(_root), m_code(_code), m_info(_info)//, m_pNode(NULL)
//{
//
//}
//
//CItem::~CItem()
//{
//
//}


void CItem::AddChild(const ItemPtr& child)
{
	//children.push_back(child);

	if (m_pNode)
	{
		m_pNode->appendChild(child->getNote(), NULL);
	}
}

//void CItem::AddChild(CComPtr < MSXML2::IXMLDOMNode > child)
//{
//	if (m_pNode && child)
//	{
//		m_pNode->appendChild(child, NULL);
//	}
//}
//
//CComPtr < MSXML2::IXMLDOMNode > CItem::getFirstChildNode()const
//{
//	CComPtr < MSXML2::IXMLDOMNode > child_first_node = NULL;
//
//	if (m_pNode)
//	{
//		m_pNode->get_firstChild(&child_first_node);
//	}
//
//	return child_first_node;
//}













//IXMLBuilder::IXMLBuilder() : m_contentHandler(new CSAXContentHandler()), m_errorHandler(new CSAXErrorHandler())
//{
//	m_pLog.reset(new CEngineLog());
//	m_pLog->Init(CVXMLParserEngineConfig::GetInstance()->sLogFileName.c_str());
//
//	m_xmlParser.reset(new CSAXXmlParser());
//
//	//m_contentHandler.CoCreateInstance(__uuidof(ISAXContentHandler), NULL, CLSCTX_INPROC_SERVER);
//}
//
//IXMLBuilder::CItem::CItem(const std::wstring& _name, const std::wstring& _root, const std::wstring& _code, const std::wstring& _info)
//: m_name(_name), m_root(_root), m_code(_code), m_info(_info), m_pNode(NULL)
//{
//	
//}
//
//IXMLBuilder::CItem::~CItem()
//{
//
//}
//
//
//void IXMLBuilder::CItem::AddChild(const ItemPtr& child)
//{
//	//children.push_back(child);
//
//	if (m_pNode)
//	{
//		m_pNode->appendChild(child->getNote(), NULL);
//	}
//}
//
//void IXMLBuilder::CItem::AddChild(CComPtr < MSXML2::IXMLDOMNode > child)
//{
//	if (m_pNode)
//	{
//		m_pNode->appendChild(child, NULL);
//	}
//}
//
//CComPtr < MSXML2::IXMLDOMNode > IXMLBuilder::CItem::getFirstChildNode()const
//{
//	CComPtr < MSXML2::IXMLDOMNode > child_first_node = NULL;
//
//	if (m_pNode)
//	{
//		m_pNode->get_firstChild(&child_first_node);
//	}
//
//	return child_first_node;
//}
//
////void IXMLBuilder::CItem::setNodeAttribute(const _bstr_t& _name, const _variant_t& _value)
////{
////	//MSXML2::IXMLDOMNodePtr pRecNode = m_pNode.operator MSXML2::IXMLDOMNode *();
////	//MSXML2::IXMLDOMElementPtr elem = static_cast<IXMLDOMElementPtr>(pRecNode);
////	MSXML2::IXMLDOMElementPtr elem = static_cast<IXMLDOMElementPtr>(static_cast<MSXML2::IXMLDOMNode*>(m_pNode));
////	elem->setAttribute(_name, _value);
////}
//
//void IXMLBuilder::Init(const std::wstring& sSchemaFile)
//{
//	try
//	{
//		/*************************************/
//		/********* INIT VXML PARSER **********/
//		/*************************************/
//
//		//std::wstring sSchemaFile = SVUtils::ExtractFilePath(SVUtils::GetModuleFileName(g_hInst)) + L"vxml.xsd";
//		if (!sSchemaFile.empty())
//		{
//			m_xmlParser->SetParserFeature(L"schema-validation", true);
//			m_xmlParser->SetParserFeature(L"use-schema-location", false);
//
//			// decide, which namespace we should use
//			int pos = -1;
//			std::wstring _file(sSchemaFile), schema_namespace;
//			if ((pos = _file.rfind('\\')) != -1)
//			{
//				_file.erase(0, pos + 1);
//			}
//			if ((pos = _file.rfind('.')) != -1)
//			{
//				WCHAR file_name[MAX_PATH]; ZeroMemory(file_name, sizeof(file_name));
//				_file._Copy_s(file_name, MAX_PATH, pos);
//				schema_namespace = std::wstring(L"x-schema:") + file_name;
//
//			}
//			m_xmlParser->AddValidationSchema(schema_namespace, sSchemaFile);
//		}
//
//
//		// Set the content handler.
//		//CComPtr<CSAXContentHandler> contentHandler(new DEBUG_NEW_PLACEMENT CSAXContentHandler());
//		if (m_xmlParser->PutContentHandler(m_contentHandler))
//		{
//			m_contentHandler->AttachElementHandler(this);
//		}
//
//
//		//CComPtr<CSAXErrorHandler> errorHandler (new DEBUG_NEW_PLACEMENT CSAXErrorHandler());
//		if (m_xmlParser->PutErrorHandler(m_errorHandler))
//		{
//			m_errorHandler->AttachElementHandler(this);
//		}
//
//
//		///*************************************/
//		///********** INIT XML WRITER **********/
//		///*************************************/
//		//
//		//HRESULT hr = m_pWriter.CoCreateInstance(__uuidof(MSXML2::MXXMLWriter60), NULL, CLSCTX_INPROC_SERVER);
//
//		//if (FAILED(hr))
//		//{
//		//	throw (std::wstring(L"CoCreateInstance IMXWriterPtr failed"));
//		//}
//
//		//m_pWriter->GetInterfacePtr()->put_byteOrderMark(VARIANT_FALSE);
//		//m_pWriter->GetInterfacePtr()->put_omitXMLDeclaration(VARIANT_FALSE);
//		//m_pWriter->GetInterfacePtr()->put_indent(VARIANT_TRUE);
//		//m_pWriter->GetInterfacePtr()->put_encoding(L"WINDOWS-1251"/*_bstr_t(CTranslatorEngineConfig::Instance().m_sEncoding.c_str()).GetBSTR()*/);
//
//		////CComPtr<IStream> pIStream;
//		////hr = CreateStreamOnHGlobal(NULL, TRUE, &pIStream);
//
//		//// the output
//		
//		//hr = m_pDoc.CoCreateInstance(CLSID_DOMDocument);
//
//		//if (FAILED(hr))
//		//{
//		//	throw (std::wstring(L"CoCreateInstance IXMLDOMDocument failed"));
//		//}
//
//		//m_pWriter->GetInterfacePtr()->put_output(_variant_t(m_pDoc));
//
//		//CComPtr <MSXML2::IVBSAXContentHandler> hdl;
//		//hr = hdl.CoCreateInstance(__uuidof(IVBSAXContentHandler), NULL, CLSCTX_INPROC_SERVER);
//
//		//if (FAILED(hr))
//		//{
//		//	throw (std::wstring(L"CoCreateInstance IVBSAXContentHandler failed"));
//		//}
//
//
//
//	}
//	catch (std::exception& e)
//	{
//		m_pLog->Log(__FUNCTIONW__, _T("Exception: \"%s\""), e.what());
//	}
//	catch (std::wstring& e)
//	{
//		m_pLog->Log(__FUNCTIONW__, _T("Exception: \"%s\""), e.c_str()); 
//	}
//	catch (_com_error& e)
//	{
//		m_pLog->Log(__FUNCTIONW__, _T("Exception: \"%s\""), (LPCWSTR)e.Description());
//	}
//	catch (...)
//	{
//		m_pLog->Log(__FUNCTIONW__, _T("Exception: Unknown unhandled exception"));
//	}
//
//}
//
//IXMLBuilder::~IXMLBuilder()
//{
//}
//
//bool IXMLBuilder::Parse(const std::wstring& _filename, const std::wstring& _root, const std::wstring& _code, std::wstring& _xml)
//{
//	m_items.clear();
//	m_root_item.reset(new CItem(L"root_item", _root, _code, L""));
//	m_items[L""] = m_root_item;
//
//	// create new document
//	CComPtr < MSXML2::IXMLDOMDocument > pDoc;
//	HRESULT hr = pDoc.CoCreateInstance(CLSID_DOMDocument, NULL, CLSCTX_INPROC_SERVER);
//	if (FAILED(hr))
//	{
//		m_pLog->Log(__FUNCTIONW__, _T("Error: CoCreateInstance IXMLDOMDocument failed"));
//		return false;
//	}
//	pDoc->put_async(VARIANT_FALSE);
//
//	CComPtr <MSXML2::IXMLDOMProcessingInstruction> pPI;
//	hr = pDoc->createProcessingInstruction(L"xml", L"version='1.0' encoding='WINDOWS-1251'", &pPI);
//	if (FAILED(hr))
//	{
//		m_pLog->Log(__FUNCTIONW__, _T("Error: createProcessingInstruction failed"));
//		return false;
//	}
//	pDoc->appendChild(pPI, NULL);
//
//	m_pDoc = pDoc;
//
//	// create root node
//	MSXML2::IXMLDOMNodePtr rootPtr = NULL;// pRootNode->GetInterfacePtr();
//	hr = pDoc->createNode(_variant_t((short)MSXML2::NODE_ELEMENT), _T("item"), _T(""), &rootPtr);
//	pDoc->appendChild(rootPtr, NULL);
//
//	CComPtr <MSXML2::IXMLDOMNode> pRootNode(rootPtr);
//	//pRootNode.Attach(rootPtr);
//	m_root_item->setNode(pRootNode);
//
//	//m_root_item->setNode(rootPtr);
//
//	//MSXML2::IXMLDOMNodePtr pRecNode = NULL;
//	//hr = pDoc->createNode(_variant_t((short)MSXML2::NODE_ELEMENT), _T("item"), _T(""), &pRecNode);
//
//	//MSXML2::IXMLDOMElementPtr elem = static_cast<IXMLDOMElementPtr>(pRecNode);
//	//elem->setAttribute(L"code", _variant_t(L"1"));
//	//elem->setAttribute(L"item_name", _variant_t(L"1"));
//
//	//hr = pRootNode->appendChild(pRecNode, NULL);
//
//
//	if (!m_xmlParser->ParseUrl(_filename))
//		return false;
//
//	m_pDoc.Detach();
//
//
//	//CComPtr < MSXML2::IXMLDOMNode > child_first_node = m_root_item->getFirstChildNode();
//	//if (child_first_node)
//	//{
//	//	_bstr_t bstrXML;
//	//	child_first_node->get_xml(&bstrXML.GetBSTR());
//
//	//	_xml = bstrXML.GetBSTR();
//	//}
//
//	_bstr_t bstrXML;
//	pDoc->get_xml(&bstrXML.GetBSTR());
//
//	_xml = bstrXML.GetBSTR();
//
//
//	return true;
//}
//
//CXMLFileBuilder::CXMLFileBuilder()
//{
//	std::wstring sSchemaFile = SVUtils::ExtractFilePath(SVUtils::GetModuleFileName(g_hInst)) + L"vxml.xsd";
//	Init(sSchemaFile);
//}
//
//CXMLFileBuilder::~CXMLFileBuilder()
//{
//
//}
//
//void CXMLFileBuilder::OnXmlStartElement(const ISATXMLElement& xmlElement)
//{
//	// menu cannot consider other menu into theyself 
//
//	CAtlString menuName, root_tag, code, rootCode;
//	std::wstring info;
//
//	// Waiting for <menu> tag
//	if (!CAtlStringW(xmlElement.GetName().c_str()).CompareNoCase(L"menu")) // vxml menu
//	{
//		// Get name menu
//		menuName = xmlElement.GetAttrVal(L"id").c_str();
//		m_pLog->Log(__FUNCTIONW__, _T("OnXmlStartElement: menu id=\"%s\""), menuName);
//
//		//// Compare format <root_tag>_<code>. Example: 0611_EKT_prepaid_1
//		//int pos = menuName.ReverseFind('_');
//
//		// get main root tag
//		std::wstring main_root = m_root_item->root();
//		// Compare format <root_tag><code>. Example: 0611_EKT_prepaid_1
//		int pos = menuName.Find(main_root.c_str());
//		if (pos < 0)
//			return;
//
//		root_tag = menuName.Left(pos + main_root.size());
//		code = menuName.Right(menuName.GetLength() - (pos + main_root.size()));
//
//		////if (root_tag.CompareNoCase(main_root.c_str())) 
//		//if (root_tag.Find(main_root.c_str())<0)
//		//	return;
//
//		// check code
//		if (0 == code.GetLength())
//			return;
//
//		//DWORD dwCode = 0;
//		//swscanf(code, L"%u", &dwCode); // cannot protect from "20help"
//
//		//if (0 == dwCode && code.CompareNoCase(L"0"))
//		//	return;
//
//		for (int i = 0; i < code.GetLength(); ++i)
//		{
//			if (DIGITS.Find(code[i]) < 0)
//				return;
//		}
//
//		xmlElement.GetAttrVal(L"info", info);
//
//		if (!info.length())
//			xmlElement.GetAttrVal(L"comment", info);
//	}
//	//else if (!CAtlStringW(xmlElement.GetName().c_str()).CompareNoCase(L"item")) // root menu
//	//{
//	//	menuName = xmlElement.GetAttrVal(L"item_name").c_str();
//	//	m_pLog->Log(__FUNCTIONW__, _T("OnXmlStartElement: item name=\"%s\""), menuName);
//
//	//	std::wstring witem_root, wcode;
//	//	xmlElement.GetAttrVal(L"item_root", witem_root);
//	//	xmlElement.GetAttrVal(L"code", wcode);
//
//	//	root_tag = witem_root.c_str();
//	//	code     = wcode     .c_str();
//	//	info     = menuName;
//	//}
//	else
//	{
//		return; // skip other
//	}
//
//	rootCode = m_root_item->code().c_str();
//	// create new item
//	CItem::ItemPtr item(new CItem(std::wstring(menuName), std::wstring(root_tag), std::wstring(code), info));
//
//	//create note
//	if (m_pDoc)
//	{
//		MSXML2::IXMLDOMNodePtr pRecNode = NULL;
//		HRESULT hr = m_pDoc->createNode(_variant_t((short)MSXML2::NODE_ELEMENT), _T("item"), _T(""), &pRecNode);
//
//		if (SUCCEEDED(hr) && pRecNode)
//		{
//			MSXML2::IXMLDOMElementPtr elem = static_cast<IXMLDOMElementPtr>(pRecNode);
//			elem->setAttribute(L"code", _variant_t(rootCode.GetLength() ? (rootCode + code):code));
//			elem->setAttribute(L"item_name", _variant_t(info.c_str()));
//			
//
//			CComPtr <MSXML2::IXMLDOMNode> pComRecNode(pRecNode);
//			item->setNode(pComRecNode);
//		}
//	}
//
//
//	// check for already exist
//	CItem::ItemMap::iterator it = m_items.find(std::wstring(code));
//	if (it != m_items.end())
//	{
//		m_pLog->Log(__FUNCTIONW__, _T("Error: menu (id=\"%s\") with code = \"%s\" is dublicated"), menuName, code);
//		return;
//	}
//
//	// insert item
//	m_items[std::wstring(code)] = item;
//
//	// insert to parent item
//	it = m_items.find(item->getParentCode());
//	if (it != m_items.end())
//	{
//		CItem::ItemPtr parentItem = it->second;
//		parentItem->AddChild(item);
//	}
//
//}
//
//void CXMLFileBuilder::OnXmlElementData(const std::wstring& elementData, int depth)
//{
//	//if (!m_pCurNode)
//	//	return;
//
//	////std::wstring sPreviousText = m_current->GetText();
//	////if (sPreviousText.length())
//	////	sPreviousText += L"\n";
//
//	////m_current->SetText(sPreviousText + elementData);
//}
//
//void CXMLFileBuilder::OnXmlEndElement(const ISATXMLElement& xmlElement)
//{
//
//
//	//// Create child note
//	//MSXML2::IXMLDOMNodePtr pRecNode = NULL;
//	//_variant_t varNodeType((short)MSXML2::NODE_ELEMENT);
//	//HRESULT hr = m_pDoc->createNode(varNodeType, L"item", _T(""), &pRecNode);
//
//	////MSXML2::IXMLDOMNodePtr pNextNode = NULL;
//	////if (SUCCEEDED(hr) && pRecNode)
//	////	m_pCurNode->appendChild(pRecNode, &pNextNode);
//
//	//DWORD dw = ::GetLastError();
//
//	////MSXML::IXMLDOMAttributePtr pRecAttr = NULL;
//	////pRecAttr = pDoc->createAttribute(_T("pole1"));
//	////pRecAttr->nodeTypedValue = _T("123");
//	////pRec1->attributes->setNamedItem(pRecAttr);
//}
//
//void CXMLFileBuilder::OnXmlError(int line, int column, const std::wstring& errorText, unsigned long errorCode)
//{
//	m_pLog->Log(__FUNCTIONW__, L"Error parsing XML document. Line %d, char %d, reason \"%s\"",
//		line, column, errorText.c_str());
//}
//
//bool CXMLFileBuilder::OnXmlAbortParse(const ISATXMLElement& xmlElement)
//{
//	return false;
//}
//
//
////void CXMLBuilder::ReadStream(const CComPtr<IStream>& _stream, void** _buff, DWORD& _size)const
////{
////	if (!_stream)
////		throw 1;
////
////	LARGE_INTEGER iLNull = { 0, 0 };
////	ULARGE_INTEGER uLLen = { 0, 0 };
////	//Take data size...
////	HRESULT hr = _stream->Seek(iLNull, STREAM_SEEK_CUR, &uLLen);
////	if (FAILED(hr))
////		throw 1;
////	//IStream -> to begin...
////	hr = _stream->Seek(iLNull, STREAM_SEEK_SET, NULL);
////	if (FAILED(hr))
////		throw 1;
////	//Size of buffer...
////	DWORD Size = (DWORD)uLLen.QuadPart;
////
////	char * szBuf = new char[Size + 1];
////	ZeroMemory(szBuf, sizeof(szBuf));
////	_stream->Read(szBuf, Size, NULL);
////	szBuf[Size] = 0;
////
////	// output
////	*_buff = szBuf;
////	_size = Size;
////}
//
////bool CXMLBuilder::GetXML(std::wstring& _dist)const
////{
////	// ReadStream(pIStream,&pParams->Output.Mem.Ptr,pParams->Output.Mem.Size);
////	_bstr_t	bstrXML;
////
////	CComPtr < MSXML2::IXMLDOMDocument > pDoc;
////	HRESULT hr = pDoc.CoCreateInstance(CLSID_DOMDocument, NULL, CLSCTX_INPROC_SERVER);
////	if (FAILED(hr))
////	{
////		m_pLog->Log(__FUNCTIONW__, _T("Error: CoCreateInstance IXMLDOMDocument failed"));
////		return false;
////	}
////	pDoc->put_async(VARIANT_FALSE);
////
////	CComPtr <MSXML2::IXMLDOMProcessingInstruction> pPI;
////	hr = pDoc->createProcessingInstruction(L"xml", L"version='1.0' encoding='WINDOWS-1251'", &pPI);
////	if (FAILED(hr))
////	{
////		m_pLog->Log(__FUNCTIONW__, _T("Error: createProcessingInstruction failed"));
////		return false;
////	}
////	pDoc->appendChild(pPI,NULL);
////
////	
////	MSXML2::IXMLDOMNodePtr pRootNode = NULL;
////	hr = pDoc->createNode(_variant_t((short)MSXML2::NODE_ELEMENT), _T("item"), _T(""), &pRootNode);
////	pDoc->appendChild(pRootNode, NULL);
////
////	MSXML2::IXMLDOMElementPtr elem = static_cast<IXMLDOMElementPtr>(pRootNode);
////	elem->setAttribute(L"code"     , _variant_t(L""));
////	elem->setAttribute(L"item_name", _variant_t(L""));
////
////	MSXML2::IXMLDOMNodePtr pRecNode = NULL;
////	hr = pDoc->createNode(_variant_t((short)MSXML2::NODE_ELEMENT), _T("item"), _T(""), &pRecNode);
////
////	elem = static_cast<IXMLDOMElementPtr>(pRecNode);
////	elem->setAttribute(L"code", _variant_t(L"1"));
////	elem->setAttribute(L"item_name", _variant_t(L"1"));
////
////	hr = pRootNode->appendChild(pRecNode,NULL);
////
////
////	pDoc->get_xml(&bstrXML.GetBSTR());
////
////	_dist = bstrXML.GetBSTR();
////	//return std::wstring(bstrXML.GetBSTR());
////	return true;
////}
//
//
////bool CXMLBuilder::InitNewDoc()
////{
////	//if (m_pDoc)
////	//	m_pDoc.Release();
////
////	//HRESULT hr = m_pDoc.CoCreateInstance(CLSID_DOMDocument, NULL, CLSCTX_INPROC_SERVER);
////	//if (FAILED(hr))
////	//{
////	//	m_pLog->Log(__FUNCTIONW__, _T("Error: CoCreateInstance IXMLDOMDocument failed"));
////	//	return false;
////	//}
////	//m_pDoc->put_async(VARIANT_FALSE);
////
////	//CComPtr <MSXML2::IXMLDOMProcessingInstruction> pPI;
////	//hr = m_pDoc->createProcessingInstruction(L"xml", L"version='1.0' encoding='WINDOWS-1251'", &pPI);
////	//if (FAILED(hr))
////	//{
////	//	m_pLog->Log(__FUNCTIONW__, _T("Error: createProcessingInstruction failed"));
////	//	return false;
////	//}
////
////	//_variant_t vNullVal;
////	//vNullVal.vt = VT_NULL;
////	//m_pDoc->insertBefore(pPI, vNullVal, NULL);
////
////	//MSXML2::IXMLDOMNodePtr pRootNode = NULL;
////	//_variant_t varNodeType((short)MSXML2::NODE_ELEMENT);
////	//hr = m_pDoc->createNode(varNodeType, _T("Root_Element"), _T(""), &pRootNode);
////	//m_pDoc->appendChild(pRootNode,&m_pCurNode);
////
////
////
////	return true;
////}
//
//bool CXMLFileBuilder::Parse(const std::wstring& _filename, const std::wstring& _root, const std::wstring& _code, std::wstring& out_xml)
//{
//	return __super::Parse(_filename, _root, _code, out_xml);
//	////m_pDoc->clear()
//
//	////MSXML::IXMLDOMDocument *pDoc = NULL;
//
//	////HRESULT hr;
//
//	////hr = CoInitialize(NULL);
//	////_ASSERTE(SUCCEEDED(hr));
//
//	////hr = CoCreateInstance(MSXML::CLSID_DOMDocument, NULL,
//	////	CLSCTX_INPROC_SERVER | CLSCTX_LOCAL_SERVER,
//	////	MSXML::IID_IXMLDOMDocument, (LPVOID*)&pDoc);
//	////if (FAILED(hr))
//	////{
//	////	return FALSE;
//	////}
//
//	////pDoc->put_async(VARIANT_FALSE);
//
//	////MSXML::IXMLDOMProcessingInstructionPtr
//	////	pPI = pDoc->createProcessingInstruction("xml", "version='1.0' encoding='WINDOWS-1251'");
//
//	////_variant_t vNullVal;
//	////vNullVal.vt = VT_NULL;
//	////pDoc->insertBefore(pPI, vNullVal);
//
//	////MSXML::IXMLDOMNodePtr pRootNode = NULL;
//	////_variant_t varNodeType((short)MSXML::NODE_ELEMENT);
//	////pRootNode = pDoc->createNode(varNodeType, _T("Root_Element"), _T(""));
//	////pDoc->appendChild(pRootNode);
//
//
//	////MSXML::IXMLDOMNodePtr pRecNode = NULL;
//	////pRecNode = pRootNode->appendChild(pDoc->createElement(_T("PEOPLE")));
//
//	////MSXML::IXMLDOMNodePtr pRec1 = NULL;
//	////pRec1 = pRecNode->appendChild(pDoc->createElement(_T("NODE_PEOPLE")));
//
//	////MSXML::IXMLDOMAttributePtr pRecAttr = NULL;
//	////pRecAttr = pDoc->createAttribute(_T("pole1"));
//	////pRecAttr->nodeTypedValue = _T("123");
//	////pRec1->attributes->setNamedItem(pRecAttr);
//
//
//	////BSTR bsXML;
//	////hr = pDoc->get_xml(&bsXML);
//
//	////hr = pDoc->save("C:\\test.xml");
//
//	////return TRUE;
//
//	//m_items.clear();
//	//m_root_item.reset(new CItem(L"root_item", L"0611_EKT_prepaid", L"", L"")); // empty_code -> means that is root item
//	//m_items[L""] = m_root_item;
//
//	//// create new document
//	//CComPtr < MSXML2::IXMLDOMDocument > pDoc;
//	//HRESULT hr = pDoc.CoCreateInstance(CLSID_DOMDocument, NULL, CLSCTX_INPROC_SERVER);
//	//if (FAILED(hr))
//	//{
//	//	m_pLog->Log(__FUNCTIONW__, _T("Error: CoCreateInstance IXMLDOMDocument failed"));
//	//	return false;
//	//}
//	//pDoc->put_async(VARIANT_FALSE);
//
//	//CComPtr <MSXML2::IXMLDOMProcessingInstruction> pPI;
//	//hr = pDoc->createProcessingInstruction(L"xml", L"version='1.0' encoding='WINDOWS-1251'", &pPI);
//	//if (FAILED(hr))
//	//{
//	//	m_pLog->Log(__FUNCTIONW__, _T("Error: createProcessingInstruction failed"));
//	//	return false;
//	//}
//	//pDoc->appendChild(pPI, NULL);
//
//	//m_pDoc = pDoc;
//
//	//// create root node
//	//MSXML2::IXMLDOMNodePtr rootPtr = NULL;// pRootNode->GetInterfacePtr();
//	//hr = pDoc->createNode(_variant_t((short)MSXML2::NODE_ELEMENT), _T("item"), _T(""), &rootPtr);
//	//pDoc->appendChild(rootPtr, NULL);
//
//	//CComPtr <MSXML2::IXMLDOMNode> pRootNode(rootPtr);
//	////pRootNode.Attach(rootPtr);
//	//m_root_item->setNode(pRootNode);
//
//	////m_root_item->setNode(rootPtr);
//
//	////MSXML2::IXMLDOMNodePtr pRecNode = NULL;
//	////hr = pDoc->createNode(_variant_t((short)MSXML2::NODE_ELEMENT), _T("item"), _T(""), &pRecNode);
//
//	////MSXML2::IXMLDOMElementPtr elem = static_cast<IXMLDOMElementPtr>(pRecNode);
//	////elem->setAttribute(L"code", _variant_t(L"1"));
//	////elem->setAttribute(L"item_name", _variant_t(L"1"));
//
//	////hr = pRootNode->appendChild(pRecNode, NULL);
//
//
//	//if (!m_xmlParser->ParseUrl(_filename))
//	//	return false;
//
//	//m_pDoc.Detach();
//
//	//_bstr_t bstrXML;
//	//pDoc->get_xml(&bstrXML.GetBSTR());
//
//	//std::wstring _dist = bstrXML.GetBSTR();
//
//
//	//return true;
//}
/******************************* eof *************************************/