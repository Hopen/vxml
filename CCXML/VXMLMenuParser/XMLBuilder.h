/************************************************************************/
/* Name     : VXMLMenuParser\XMLBuilder.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 16 Jul 2014                                               */
/************************************************************************/

#pragma once

#include <vector>
#include <map>
#include <stack>
#include "singleton.h"
#include "SAXXmlElementHandler.h"
#include "SAXXmlParser.h"
#include <boost/shared_ptr.hpp>

class CEngineLog;

class CItem
{
public:
	CItem(const std::wstring& _name, const std::wstring& _root, const std::wstring& _code, const std::wstring& _info);
	~CItem();// {}

	// copy ctor
	CItem(const CItem& rhs);


	typedef boost::shared_ptr<CItem> ItemPtr;
	typedef std::vector <ItemPtr> ItemCollector;
	typedef std::map <std::wstring, ItemPtr> ItemMap;
	typedef std::multimap <std::wstring, ItemPtr> ItemMultiMap;
	typedef std::pair <std::wstring, ItemPtr> ItemPair;
	

	void AddChild(const ItemPtr& child);
	//void AddChild(CComPtr < MSXML2::IXMLDOMNode > child);

	std::wstring getParentCode()const
	{
		//if (!m_code.length())
		//	return std::wstring(); // empty string

		//std::wstring parent_code(m_code);
		//parent_code = parent_code.erase(parent_code.length() - 1);

		//return parent_code;

		return m_parent_code;
	}

	std::wstring name()const{ return m_name; }
	std::wstring root()const{ return m_root; }
	std::wstring code()const{ return m_code; }

	//void setNode(CComPtr < MSXML2::IXMLDOMNode > _note/*MSXML2::IXMLDOMNode* _note*/) { m_pNode = _note; }

	CComPtr < MSXML2::IXMLDOMNode > createNodeItem(CComPtr < MSXML2::IXMLDOMDocument > pDoc, const std::wstring & root_code);
	//CComPtr < MSXML2::IXMLDOMNode > createNodeItem2(CComPtr < MSXML2::IXMLDOMDocument > pDoc, const std::wstring & root_code);
	CComPtr < MSXML2::IXMLDOMNode >/*MSXML2::IXMLDOMNode **/ getNote()const { return m_pNode; }
	////void setNodeAttribute(const _bstr_t& _name, const _variant_t& _value);
	//CComPtr < MSXML2::IXMLDOMNode > getFirstChildNode() const;
	//void appendParentInformation();

	void setParentCode(const std::wstring& _parent){ m_parent_code = _parent; }
	void setCode      (const std::wstring& _code  ){ m_code = _code         ; }


private:
	std::wstring m_name;
	std::wstring m_root;
	std::wstring m_code;
	std::wstring m_info;
	std::wstring m_parent_code;
	//ItemCollector children;

	CComPtr < MSXML2::IXMLDOMNode > m_pNode;
	////MSXML2::IXMLDOMNode* m_pNode;


	/*static boost::shared_ptr<CEngineLog>  m_pLog; 
	static int refCount;*/
};

// XML builder template

template <class T>
class CXmlBuilderTmpl : public ISAXXmlElementHandler
{
protected:


public:
	bool Parse(const std::wstring& _filename, const std::wstring& _root, const std::wstring& _code, std::wstring& _xml);
	CItem::ItemPtr getRootItem()const { return m_root_item; }
	CItem::ItemMap getItems()const { return m_items; }

	CComPtr < MSXML2::IXMLDOMNode > getXMLDom();

public:
	CXmlBuilderTmpl();
	~CXmlBuilderTmpl();

public:
	// Handle XML content events during parsing.
	void OnXmlStartElement(const ISATXMLElement& xmlElement);
	void OnXmlElementData(const std::wstring& elementData, int depth);
	void OnXmlEndElement(const ISATXMLElement& xmlElement);

	// Handle XML error events during parsing.
	void OnXmlError(int line, int column, const std::wstring& errorText, unsigned long errorCode);

	// Return true to stop parsing earlier.
	bool OnXmlAbortParse(const ISATXMLElement& xmlElement);

private:
	void Init(/*const std::wstring& sSchemaFile*/);

private:
	boost::shared_ptr< CSAXXmlParser       > m_xmlParser;
	boost::shared_ptr<CEngineLog>  m_pLog;

	CComPtr < CSAXContentHandler      > m_contentHandler;
	CComPtr < CSAXErrorHandler        > m_errorHandler;
	//CComPtr < MSXML2::IXMLDOMDocument > m_pDoc;

	CItem::ItemPtr m_root_item;
	CItem::ItemMap m_items;



private:
	T m_doc;
};


template <class T>
CXmlBuilderTmpl<T>::CXmlBuilderTmpl()
	: m_contentHandler(new CSAXContentHandler()), m_errorHandler(new CSAXErrorHandler())
{
	//std::wstring sSchemaFile = m_doc.getValidateSchema();
	//m_doc.Init(sSchemaFile);


	m_pLog.reset(new CEngineLog());
	m_pLog->Init(CVXMLParserEngineConfig::GetInstance()->sLevel.c_str(), CVXMLParserEngineConfig::GetInstance()->sLogFileName.c_str());

	m_xmlParser.reset(new CSAXXmlParser());

	Init();
}

template <class T>
CXmlBuilderTmpl<T>::~CXmlBuilderTmpl()
{

}
template <class T>
void CXmlBuilderTmpl<T>::OnXmlStartElement(const ISATXMLElement& xmlElement)
{
	
	//CAtlString menuName, root_tag, code, rootCode;
	//std::wstring info;
	std::wstring menuName;

	if (!m_doc.validateName(xmlElement))
		return;

	bool bRet = m_doc.validateParams(xmlElement, m_root_item->root(), m_root_item->code());
	if (!bRet )
	{
		//m_pLog->Log(__FUNCTIONW__, _T("OnXmlStartElement: item name=\"%s\""), m_doc.getName().c_str());
		return;
	}
	m_pLog->Log(enginelog::LEVEL_FINEST, __FUNCTIONW__, _T("OnXmlStartElement: item name=\"%s\""), m_doc.getName().c_str());

	//std::wstring rootCode = m_root_item->code().c_str();
	// create new item
	//CItem::ItemPtr item(new CItem(std::wstring(menuName), std::wstring(root_tag), std::wstring(code), info));
	CItem::ItemPtr item(new CItem(m_doc.getName(), m_doc.getRoot(), m_doc.getCode(), m_doc.getInfo()));
	
	std::wstring parentCode = m_doc.getParentCode();

	item->setParentCode(parentCode);

	////create note
	//if (m_pDoc)
	//{
	//	MSXML2::IXMLDOMNodePtr pRecNode = NULL;
	//	HRESULT hr = m_pDoc->createNode(_variant_t((short)MSXML2::NODE_ELEMENT), _T("item"), _T(""), &pRecNode);

	//	if (SUCCEEDED(hr) && pRecNode)
	//	{
	//		MSXML2::IXMLDOMElementPtr elem = static_cast<IXMLDOMElementPtr>(pRecNode);
	//		elem->setAttribute(L"code", _variant_t(rootCode.GetLength() ? (rootCode + code) : code));
	//		elem->setAttribute(L"item_name", _variant_t(info.c_str()));


	//		CComPtr <MSXML2::IXMLDOMNode> pComRecNode(pRecNode);
	//		item->setNode(pComRecNode);
	//	}
	//}


	// check for already exist
	CItem::ItemMap::iterator it = m_items.find(/*parentCode*/ m_doc.getCode2()/*+ m_doc.getRoot()*/);
	if (it != m_items.end())
	{
		m_pLog->Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, _T("Error: menu (id=\"%s\") with code = \"%s\" is dublicated"), m_doc.getName().c_str(), m_doc.getCode().c_str());
		return;
	}

	// insert item
	m_items[/*parentCode*/ m_doc.getCode2() /*+ m_doc.getRoot()*/] = item;
	m_doc.onXMLStart(xmlElement);

	//// insert to parent item
	//it = m_items.find(item->getParentCode());
	//if (it != m_items.end())
	//{
	//	CItem::ItemPtr parentItem = it->second;
	//	parentItem->AddChild(item);
	//}
}

template <class T>
void CXmlBuilderTmpl<T>::OnXmlElementData(const std::wstring& elementData, int depth)
{

}

template <class T>
void CXmlBuilderTmpl<T>::OnXmlEndElement(const ISATXMLElement& xmlElement)
{


	//if (m_xml_elements.size())
	//	m_xml_elements.pop();

	//if (m_doc.validateName(xmlElement) /*&& m_doc.validateParams(xmlElement, m_root_item->root())*/)
	//	m_parentCodes.pop();
	m_doc.onXMLEnd(xmlElement);
}

template <class T>
void CXmlBuilderTmpl<T>::OnXmlError(int line, int column, const std::wstring& errorText, unsigned long errorCode)
{
	m_pLog->Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, L"Error parsing XML document. Line %d, char %d, reason \"%s\"",
		line, column, errorText.c_str());
}

template <class T>
bool CXmlBuilderTmpl<T>::OnXmlAbortParse(const ISATXMLElement& xmlElement)
{
	return false;
}


template <class T>
void CXmlBuilderTmpl<T>::Init(/*const std::wstring& sSchemaFile*/)
{
	try
	{
		/*************************************/
		/********* INIT VXML PARSER **********/
		/*************************************/
		std::wstring sSchemaFile = m_doc.getValidateSchema();


		if (!sSchemaFile.empty())
		{
			m_xmlParser->SetParserFeature(L"schema-validation", true);
			m_xmlParser->SetParserFeature(L"use-schema-location", false);

			// decide, which namespace we should use
			int pos = -1;
			std::wstring _file(sSchemaFile), schema_namespace;
			if ((pos = _file.rfind('\\')) != -1)
			{
				_file.erase(0, pos + 1);
			}
			if ((pos = _file.rfind('.')) != -1)
			{
				WCHAR file_name[MAX_PATH]; ZeroMemory(file_name, sizeof(file_name));
				_file._Copy_s(file_name, MAX_PATH, pos);
				schema_namespace = std::wstring(L"x-schema:") + file_name;

			}
			m_xmlParser->AddValidationSchema(schema_namespace, sSchemaFile);
		}


		// Set the content handler.
		//CComPtr<CSAXContentHandler> contentHandler(new DEBUG_NEW_PLACEMENT CSAXContentHandler());
		if (m_xmlParser->PutContentHandler(m_contentHandler))
		{
			m_contentHandler->AttachElementHandler(this);
		}


		//CComPtr<CSAXErrorHandler> errorHandler (new DEBUG_NEW_PLACEMENT CSAXErrorHandler());
		if (m_xmlParser->PutErrorHandler(m_errorHandler))
		{
			m_errorHandler->AttachElementHandler(this);
		}
	}
	catch (std::exception& e)
	{
		m_pLog->Log(enginelog::LEVEL_WARNING,__FUNCTIONW__, _T("Exception: \"%s\""), e.what());
	}
	catch (std::wstring& e)
	{
		m_pLog->Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, _T("Exception: \"%s\""), e.c_str());
	}
	catch (_com_error& e)
	{
		m_pLog->Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, _T("Exception: \"%s\""), (LPCWSTR)e.Description());
	}
	catch (...)
	{
		m_pLog->Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, _T("Exception: Unknown unhandled exception"));
	}
	//m_doc.Init(sSchemaFile);
}

template <class T>
bool CXmlBuilderTmpl<T>::Parse(const std::wstring& _filename, const std::wstring& _root, const std::wstring& _code, std::wstring& _xml)
{
	m_items.clear();
	m_root_item.reset(new CItem(L"root_item", _root, _code, L""));
	m_items[L""] = m_root_item;
	//m_parentCodes.push(_code);

	//// create new document
	//CComPtr < MSXML2::IXMLDOMDocument > pDoc;
	//HRESULT hr = pDoc.CoCreateInstance(CLSID_DOMDocument, NULL, CLSCTX_INPROC_SERVER);
	//if (FAILED(hr))
	//{
	//	m_pLog->Log(__FUNCTIONW__, _T("Error: CoCreateInstance IXMLDOMDocument failed"));
	//	return false;
	//}
	//pDoc->put_async(VARIANT_FALSE);

	//CComPtr <MSXML2::IXMLDOMProcessingInstruction> pPI;
	//hr = pDoc->createProcessingInstruction(L"xml", L"version='1.0' encoding='WINDOWS-1251'", &pPI);
	//if (FAILED(hr))
	//{
	//	m_pLog->Log(__FUNCTIONW__, _T("Error: createProcessingInstruction failed"));
	//	return false;
	//}
	//pDoc->appendChild(pPI, NULL);

	//m_pDoc = pDoc;

	//// create root node
	//MSXML2::IXMLDOMNodePtr rootPtr = NULL;
	//hr = pDoc->createNode(_variant_t((short)MSXML2::NODE_ELEMENT), _T("item"), _T(""), &rootPtr);
	//pDoc->appendChild(rootPtr, NULL);

	//CComPtr <MSXML2::IXMLDOMNode> pRootNode(rootPtr);

	//m_root_item->setNode(pRootNode);




	if (!m_xmlParser->ParseUrl(_filename))
		return false;

	//m_pDoc.Detach();




	//_bstr_t bstrXML;
	//pDoc->get_xml(&bstrXML.GetBSTR());

	//_xml = bstrXML.GetBSTR();

	return true;
}


template <class T>
CComPtr < MSXML2::IXMLDOMNode > CXmlBuilderTmpl<T>::getXMLDom()
{
	// create new document
	CComPtr < MSXML2::IXMLDOMDocument > pDoc;
	HRESULT hr = pDoc.CoCreateInstance(CLSID_DOMDocument, NULL, CLSCTX_INPROC_SERVER);
	if (FAILED(hr))
	{
		m_pLog->Log(__FUNCTIONW__, _T("Error: CoCreateInstance IXMLDOMDocument failed"));
		return NULL;
	}
	pDoc->put_async(VARIANT_FALSE);

	CComPtr <MSXML2::IXMLDOMProcessingInstruction> pPI;
	hr = pDoc->createProcessingInstruction(L"xml", L"version='1.0' encoding='utf-8'", &pPI);
	if (FAILED(hr))
	{
		m_pLog->Log(__FUNCTIONW__, _T("Error: createProcessingInstruction failed"));
		return NULL;
	}
	pDoc->appendChild(pPI, NULL);

	//// create root node
	//MSXML2::IXMLDOMNodePtr rootPtr = NULL;// pRootNode->GetInterfacePtr();
	//hr = pDoc->createNode(_variant_t((short)MSXML2::NODE_ELEMENT), _T("item"), _T(""), &rootPtr);
	
	//pDoc->appendChild(rootPtr, NULL);

	//CComPtr <MSXML2::IXMLDOMNode> pRootNode(rootPtr);
	////pRootNode.Attach(rootPtr);
	//m_root_item->setNode(pRootNode);

	for (CItem::ItemMap::iterator it = m_items.begin(); it != m_items.end(); ++it)
	{
		CItem::ItemPtr item = it->second;

		CComPtr <MSXML2::IXMLDOMNode> pComRecNode = item->createNodeItem(pDoc, L"");
		if (!pComRecNode) // still nothing
			continue;

		//if (!item->code().length() == 1)
		//{
		//	saveItem = item;
		//}

		if (!item->code().length())
			continue;

		std::wstring parentCode = item->getParentCode();
		//if (m_doc.isParentCodeaAvilable(parentCode))
		//{
		//	CItem::ItemMap::iterator it2 = m_items.find(parentCode);
		//	if (it2 != m_items.end())
		//	{
		//		CItem::ItemPtr parentItem = it2->second;
		//		parentItem->createNodeItem(pDoc);
		//		parentItem->AddChild(item);
		//	}
		//}
		//else
		//{
		//	//m_root_item->
		//}

		CItem::ItemMap::iterator it2 = m_items.find(parentCode /*+ item->root()*/);
			if (it2 != m_items.end())
			{
				CItem::ItemPtr parentItem = it2->second;
				parentItem->createNodeItem(pDoc, L"");
				parentItem->AddChild(item);
			}
	}

	return m_root_item->getNote();
}



//class IXMLBuilder : public ISAXXmlElementHandler
//{
//protected:
//	class CItem
//	{
//	public:
//		//CItem()
//		//{
//		//	m_name = L"unknown";
//		//	m_root = L"unknown";
//		//	m_info = L"unknown";
//		//	m_code = L"-1";
//		//};
//
//		CItem(const std::wstring& _name, const std::wstring& _root, const std::wstring& _code, const std::wstring& _info);
//		//: m_name(_name), m_root(_root), m_code(_code), m_info(_info)
//		//{}
//		~CItem();
//
//		typedef boost::shared_ptr<CItem> ItemPtr;
//		//typedef std::vector <ItemPtr> ItemCollector;
//		typedef std::map <std::wstring, ItemPtr> ItemMap;
//
//		void AddChild(const ItemPtr& child);
//		void AddChild(CComPtr < MSXML2::IXMLDOMNode > child);
//
//		std::wstring getParentCode()const
//		{
//			if (!m_code.length())
//				return std::wstring(); // empty string
//
//			std::wstring parent_code(m_code);
//			parent_code = parent_code.erase(parent_code.length() - 1);
//
//			return parent_code;
//		}
//
//		std::wstring root()const{ return m_root; }
//		std::wstring code()const{ return m_code; }
//
//		void setNode(CComPtr < MSXML2::IXMLDOMNode > _note/*MSXML2::IXMLDOMNode* _note*/) { m_pNode = _note; }
//		CComPtr < MSXML2::IXMLDOMNode >/*MSXML2::IXMLDOMNode **/ getNote()const { return m_pNode; }
//		//void setNodeAttribute(const _bstr_t& _name, const _variant_t& _value);
//		CComPtr < MSXML2::IXMLDOMNode > getFirstChildNode() const;
//
//	private:
//		std::wstring m_name;
//		std::wstring m_root;
//		std::wstring m_code;
//		std::wstring m_info;
//		//ItemCollector children;
//
//		CComPtr < MSXML2::IXMLDOMNode > m_pNode;
//		//MSXML2::IXMLDOMNode* m_pNode;
//	};
//
//protected:
//	virtual bool Parse(const std::wstring& _filename, const std::wstring& _root, const std::wstring& _code, std::wstring& _xml) = 0;
//public:
//	CItem::ItemPtr getRootItem()const { return m_root_item; }
//public:
//	IXMLBuilder();
//	~IXMLBuilder();
//
//	//virtual bool Parse(const std::wstring& _filename, std::wstring& _xml) = 0;
//	//bool GetXML(std::wstring& )const;
//public:
//	// Handle XML content events during parsing.
//	virtual void OnXmlStartElement(const ISATXMLElement& xmlElement) = 0;
//	virtual void OnXmlElementData(const std::wstring& elementData, int depth) = 0;
//	virtual void OnXmlEndElement(const ISATXMLElement& xmlElement) = 0;
//
//	// Handle XML error events during parsing.
//	virtual void OnXmlError(int line, int column, const std::wstring& errorText, unsigned long errorCode) = 0;
//
//	// Return true to stop parsing earlier.
//	virtual bool OnXmlAbortParse(const ISATXMLElement& xmlElement) = 0;
//
//protected:
//	void Init(const std::wstring& sSchemaFile);
//	//bool InitNewDoc();
//
//	//void ReadStream(const CComPtr<IStream>& _stream, void** _buff, DWORD& _size)const;
//
//
//
//protected:
//	boost::shared_ptr< CSAXXmlParser       > m_xmlParser;
//	boost::shared_ptr<CEngineLog>  m_pLog;
//
//	CComPtr < CSAXContentHandler      > m_contentHandler;
//	CComPtr < CSAXErrorHandler        > m_errorHandler;
//	//CComPtr < MSXML2::IMXWriterPtr    > m_pWriter;
//	CComPtr < MSXML2::IXMLDOMDocument > m_pDoc;
//
//	//MSXML2::IXMLDOMNodePtr m_pCurNode;
//	CItem::ItemPtr m_root_item;
//	CItem::ItemMap m_items;
//};
//class CXMLFileBuilder : public IXMLBuilder, public singleton<CXMLFileBuilder>
//{
//	friend class singleton<CXMLFileBuilder>;
//
//public:
//	CXMLFileBuilder();
//	~CXMLFileBuilder();
//
//	virtual bool Parse(const std::wstring& _filename, const std::wstring& _root, const std::wstring& _code, std::wstring& out_xml);
//	//bool GetXML(std::wstring& )const;
//
//	// Handle XML content events during parsing.
//	virtual void OnXmlStartElement(const ISATXMLElement& xmlElement);
//	virtual void OnXmlElementData(const std::wstring& elementData, int depth);
//	virtual void OnXmlEndElement(const ISATXMLElement& xmlElement);
//
//	// Handle XML error events during parsing.
//	virtual void OnXmlError(int line, int column, const std::wstring& errorText, unsigned long errorCode);
//
//	// Return true to stop parsing earlier.
//	virtual bool OnXmlAbortParse(const ISATXMLElement& xmlElement);
//
//};
//
////class CXMLBuilder : public ISAXXmlElementHandler, public singleton<CXMLBuilder>
////{
////	friend class singleton<CXMLBuilder>;
////
////	class CItem
////	{
////	public:
////		//CItem()
////		//{
////		//	m_name = L"unknown";
////		//	m_root = L"unknown";
////		//	m_info = L"unknown";
////		//	m_code = L"-1";
////		//};
////
////		CItem(const std::wstring& _name, const std::wstring& _root, const std::wstring& _code, const std::wstring& _info);
////			//: m_name(_name), m_root(_root), m_code(_code), m_info(_info)
////		//{}
////		~CItem();
////
////		typedef boost::shared_ptr<CItem> ItemPtr;
////		//typedef std::vector <ItemPtr> ItemCollector;
////		typedef std::map <std::wstring, ItemPtr> ItemMap;
////
////		void AddChild(const ItemPtr& child);
////
////		std::wstring getParentCode()const
////		{
////			if (!m_code.length())
////				return std::wstring(); // empty string
////
////			std::wstring parent_code(m_code);
////			parent_code = parent_code.erase(parent_code.length() - 1);
////
////			return parent_code;
////		}
////
////		std::wstring root()const{ return m_root; }
////
////		void setNode(CComPtr < MSXML2::IXMLDOMNode > _note/*MSXML2::IXMLDOMNode* _note*/) { m_pNode = _note; }
////		CComPtr < MSXML2::IXMLDOMNode >/*MSXML2::IXMLDOMNode **/ getNote()const { return m_pNode; }
////
////	private:
////		std::wstring m_name;
////		std::wstring m_root;
////		std::wstring m_code;
////		std::wstring m_info;
////		//ItemCollector children;
////
////		CComPtr < MSXML2::IXMLDOMNode > m_pNode;
////		//MSXML2::IXMLDOMNode* m_pNode;
////	};
////
////public:
////	CXMLBuilder();
////	~CXMLBuilder();
////
////	bool Parse(const std::wstring& _filename, std::wstring& _xml);
////	//bool GetXML(std::wstring& )const;
////public:
////	// Handle XML content events during parsing.
////	virtual void OnXmlStartElement(const ISATXMLElement& xmlElement);
////	virtual void OnXmlElementData(const std::wstring& elementData, int depth);
////	virtual void OnXmlEndElement(const ISATXMLElement& xmlElement);
////
////	// Handle XML error events during parsing.
////	virtual void OnXmlError(int line, int column, const std::wstring& errorText, unsigned long errorCode);
////
////	// Return true to stop parsing earlier.
////	virtual bool OnXmlAbortParse(const ISATXMLElement& xmlElement);
////
////private:
////	void Init();
////	//bool InitNewDoc();
////
////	//void ReadStream(const CComPtr<IStream>& _stream, void** _buff, DWORD& _size)const;
////
////
////
////private:
////	boost::shared_ptr< CSAXXmlParser       > m_xmlParser;
////	boost::shared_ptr<CEngineLog>  m_pLog;
////
////	CComPtr < CSAXContentHandler      > m_contentHandler;
////	CComPtr < CSAXErrorHandler        > m_errorHandler;
////	//CComPtr < MSXML2::IMXWriterPtr    > m_pWriter;
////	CComPtr < MSXML2::IXMLDOMDocument > m_pDoc;
////
////	//MSXML2::IXMLDOMNodePtr m_pCurNode;
////	CItem::ItemPtr m_root_item;
////	CItem::ItemMap m_items;
////
////};

/******************************* eof *************************************/