/************************************************************************/
/* Name     : VXMLMenuParser\XMLRootBuilder.h                           */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 21 Jul 2014                                               */
/************************************************************************/
#pragma once

#include "XMLBuilder.h"
#include "..\Common\utils.h"
class CEngineLog;

class IXMLDocumentMy
{
public:
	class CFindRoot
	{
	public:
		CFindRoot(CItem::ItemPtr _root) :
			m_root(_root)
		{}
		bool operator()(CItem::ItemPtr _root)
		{
			int pos = _root->root().find(m_root->root());
			if (pos < 0)
				return false;
			if (pos + m_root->root().size() < _root->root().size())
				return false;

			return true;
			//return _root->root() == m_root->root();

			//return _root->root().find(m_root->root()) >= 0;
		}

	private:
		CItem::ItemPtr m_root;
	};


	class CFindCompositeRoot
	{
	public:
		CFindCompositeRoot(CItem::ItemPtr _root) :
			m_root(_root)
		{}
		bool operator()(CItem::ItemPtr _root)
		{
			int pos = _root->name().find(m_root->root());
			if (pos < 0)
				return false;

			return true;
		}

	private:
		CItem::ItemPtr m_root;
	};

	class CSortByRoot
	{
	public:
		bool operator()(CItem::ItemPtr a, CItem::ItemPtr b)
		{
			int root_a = Utils::toNum<int>(a->root());
			int root_b = Utils::toNum<int>(b->root());
			return root_a < root_b;
		}
	};

	class CFindByCode
	{
	public:
		CFindByCode(const std::wstring _parent_code)
			:m_parent_code(_parent_code)
		{}

		bool operator()(CItem::ItemPtr rhs)
		{
			return m_parent_code == rhs->code();
		}
	private:
		std::wstring m_parent_code;

	};

public:
	//virtual void Init(const std::wstring& sSchemaFile) = 0;
	virtual std::wstring getValidateSchema()const = 0;
	virtual bool validateName(const ISATXMLElement& xmlElement) = 0;
	virtual bool validateParams(const ISATXMLElement& xmlElement, const std::wstring& mainRoot, const std::wstring& mainCode) = 0;
	//virtual bool isParentCodeaAvilable(const std::wstring& parentCode) = 0;
	virtual std::wstring getParentCode()const = 0;

	virtual void onXMLStart(const ISATXMLElement& xmlElement) = 0;
	virtual void onXMLEnd  (const ISATXMLElement& xmlElement) = 0;
	virtual std::wstring getCode2()const = 0;

public:
	std::wstring getName()const{ return m_name; }
	std::wstring getRoot()const{ return m_root; }
	std::wstring getCode()const{ return m_code; }
	std::wstring getInfo()const{ return m_info; }


	CComPtr < MSXML2::IXMLDOMNode > createNodeItem(CComPtr < MSXML2::IXMLDOMDocument > pDoc);
	CComPtr < MSXML2::IXMLDOMNode > getNote()const { return m_pNode; }

protected:
	std::wstring m_name;
	std::wstring m_root;
	std::wstring m_code;
	std::wstring m_info;

	CComPtr < MSXML2::IXMLDOMNode > m_pNode;

	//std::wstring getParentCode()const
	//{
	//	if (!m_code.length())
	//		return std::wstring(); // empty string

	//	std::wstring parent_code(m_code);
	//	parent_code = parent_code.erase(parent_code.length() - 1);

	//	return parent_code;
	//}
};

class CRootDocument : public IXMLDocumentMy
{
public:
	CRootDocument();

	// interface implamentation
	//virtual void Init(const std::wstring& sSchemaFile);
	virtual std::wstring getValidateSchema()const;
	virtual bool validateName(const ISATXMLElement& xmlElement);
	virtual bool validateParams(const ISATXMLElement& xmlElement, const std::wstring& mainRoot, const std::wstring& mainCode);
	//virtual bool isParentCodeaAvilable(const std::wstring& parentCode);
	//virtual std::wstring getMenuName(const ISATXMLElement& xmlElement)const;
	//virtual bool validateRoot(const ISATXMLElement& xmlElement, std::wstring& out_root)const;
	virtual std::wstring getParentCode()const;

	virtual void onXMLStart(const ISATXMLElement& xmlElement);
	virtual void onXMLEnd  (const ISATXMLElement& xmlElement);

	virtual std::wstring getCode2()const;

private:
	typedef std::stack <std::wstring> StringStack;
	StringStack m_parentCodes;

	typedef std::stack < ISATXMLElement > XMLElementsStack;
	XMLElementsStack m_xml_elements;
};

class CVXMLDocument : public IXMLDocumentMy
{
public:
	CVXMLDocument();

	// interface implamentation
	//virtual void Init(const std::wstring& sSchemaFile);
	virtual std::wstring getValidateSchema()const;
	virtual bool validateName(const ISATXMLElement& xmlElement);
	virtual bool validateParams(const ISATXMLElement& xmlElement, const std::wstring& mainRoot, const std::wstring& mainCode);
	//virtual bool isParentCodeaAvilable(const std::wstring& parentCode);
	//virtual std::wstring getMenuName(const ISATXMLElement& xmlElement)const;
	//virtual bool validateRoot(const ISATXMLElement& xmlElement, std::wstring& out_root)const;
	virtual std::wstring getParentCode(/*const std::wstring& _top*/)const;

	virtual void onXMLStart(const ISATXMLElement& xmlElement);
	virtual void onXMLEnd  (const ISATXMLElement& xmlElement);

	virtual std::wstring getCode2()const;

};




















//class CXMLRootBuilder : public IXMLBuilder, public singleton<CXMLRootBuilder>
//{
//	friend class singleton<CXMLRootBuilder>;
//
//public:
//	CXMLRootBuilder();
//	~CXMLRootBuilder();
//
//	virtual bool Parse(const std::wstring& _filename, const std::wstring& _root, const std::wstring& _code, std::wstring& out_xml);
//	//bool GetXML(std::wstring& )const;
//
//	// Handle XML content events during parsing.
//	virtual void OnXmlStartElement(const ISATXMLElement& xmlElement);
//	virtual void OnXmlElementData(const std::wstring& elementData, int depth);
//	virtual void OnXmlEndElement(const ISATXMLElement& xmlElement);
//
//	// Handle XML error events during parsing.
//	virtual void OnXmlError(int line, int column, const std::wstring& errorText, unsigned long errorCode);
//
//	// Return true to stop parsing earlier.
//	virtual bool OnXmlAbortParse(const ISATXMLElement& xmlElement);
//};

/******************************* eof *************************************/