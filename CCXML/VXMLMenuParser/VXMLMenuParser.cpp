// VXMLMenuParser.cpp : Implementation of WinMain


#include "stdafx.h"
#include <string>
#include <sstream> 
#include <fstream>
#include "resource.h"
#include "VXMLMenuParser_i.h"
#include "singleton.h"
//#include "logger.h"
#include "mdump.h"
#include "EngineLog.h"
#include "ParserEngineConfig.h"
#include "FolderMonitor.h"
#include "XMLBuilder.h"
#include "XMLRootBuilder.h"
#include "ce_xml.hpp"

#include <stdio.h>

class CVXMLMenuParserModule : public ATL::CAtlServiceModuleT< CVXMLMenuParserModule, IDS_SERVICENAME >
{
public :
	DECLARE_LIBID(LIBID_VXMLMenuParserLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_VXMLMENUPARSER, "{8F3EEF4C-8A33-4F71-9761-1EF230F9B3EF}")
		HRESULT InitializeSecurity() throw()
	{
		// TODO : Call CoInitializeSecurity and provide the appropriate security settings for your service
		// Suggested - PKT Level Authentication, 
		// Impersonation Level of RPC_C_IMP_LEVEL_IDENTIFY 
		// and an appropriate Non NULL Security Descriptor.

		return S_OK;
	}

		HRESULT Run(int nShowCmd = SW_HIDE) throw()
		{
			::CoInitializeEx(NULL, COINIT_MULTITHREADED);
			
			CEngineLog			log;
			log.Init(CVXMLParserEngineConfig::GetInstance()->sLevel.c_str(), CVXMLParserEngineConfig::GetInstance()->sLogFileName.c_str());
			log.Log(enginelog::LEVEL_INFO, __FUNCTIONW__, L"VXMLMenuParserModule started...");

			HRESULT hr = S_OK;

			singleton_auto_pointer<MiniDumper> dumper;
			CVXMLMenuParserModule* pT = static_cast<CVXMLMenuParserModule*>(this);

			hr = pT->PreMessageLoop(nShowCmd);
			DWORD error = ::GetLastError();
			if (hr == S_OK)
			{
				singleton_auto_pointer<CFolderMonitor> manager;
				if (m_bService)
				{
					LogEvent(_T("Service started"));
					SetServiceStatus(SERVICE_RUNNING);

					log.Log(enginelog::LEVEL_INFO,__FUNCTIONW__, _T("======================================================="));
					log.Log(enginelog::LEVEL_INFO, __FUNCTIONW__, _T("Starting IS III Menu parser service 1.0.1.0 ..."));
				}

				pT->RunMessageLoop();
			}

			if (SUCCEEDED(hr))
			{
				log.Log(enginelog::LEVEL_INFO, __FUNCTIONW__, _T("Stopping IS III Menu parser 1.0.1.0 ..."));
				log.Log(enginelog::LEVEL_INFO, __FUNCTIONW__, _T("======================================================="));

				hr = pT->PostMessageLoop();
			}

			::CoUninitialize();
			return hr;
		}

		static void VXMLMenuParse();
	};

void CVXMLMenuParserModule::VXMLMenuParse()
{
	CoInitialize(NULL);
	CEngineLog			log;
	log.Init(CVXMLParserEngineConfig::GetInstance()->sLevel.c_str(), CVXMLParserEngineConfig::GetInstance()->sLogFileName.c_str());
	log.Log(enginelog::LEVEL_INFO, __FUNCTIONW__, L"Update vxml menu started...");


	//CXmlBuilderTmpl <CRootDocument> rootDoc;
	//CXmlBuilderTmpl <CVXMLDocument> vxmlDoc;

	//std::wstring rootXML;
	//singleton_auto_pointer<CXMLRootBuilder> rootBuilder;
	//if (!rootBuilder->Parse(CVXMLParserEngineConfig::GetInstance()->sRootXML, L"", L"", rootXML))
	//{
	//	log.Log(__FUNCTIONW__, _T("Cannot parse root XML file = \"%s\", internal file or file-url error"), CVXMLParserEngineConfig::GetInstance()->sRootXML.c_str());
	//}


	// Send HTTP message here
	// ...
	CoUninitialize();
}

CVXMLMenuParserModule _AtlModule;
LPCTSTR c_lpszModuleName = _T("VXMLMenuParser.exe");
HINSTANCE g_hInst;


template< typename tPair >
struct second_t {
	typename tPair::second_type operator()(const tPair& p) const { return     p.second; }
};

template< typename tMap >
second_t< typename tMap::value_type > second(const tMap& m) { return second_t<     typename tMap::value_type >(); }


const CAtlString DIGITS = L"0123456789";

std::wstring getSimpleParentCode(const std::wstring& _code)
{
	std::wstring parent_code(_code);
	parent_code = parent_code.erase(parent_code.length() - 1);
	return parent_code;
}

std::wstring ifCompositeRoot(const std::wstring& _root)
{
	CAtlString menuName(_root.c_str());
	// Compare format <root_tag>_<code>. Example: 0611_EKT_prepaid_1
	int pos = menuName.ReverseFind('_');
	CAtlString root_tag = menuName.Left(pos);
	CAtlString code = menuName.Right(menuName.GetLength() - (pos + 1));

	// check code
	if (0 == code.GetLength())
		return L"";

	for (int i = 0; i < code.GetLength(); ++i)
	{
		if (DIGITS.Find(code[i]) < 0)
			return L"";
	}

	//m_root = root_tag;
	//m_code = CAtlString(mainCode.c_str()) + code;

	
	return std::wstring(code);
}

//
extern "C" int WINAPI _tWinMain(HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/, 
								LPTSTR lpCmdLine, int nShowCmd)
{
	//return _AtlModule.WinMain(nShowCmd);
	//return _AtlModule.Run(nShowCmd);

	::CoInitialize(NULL);
	int retval = 0;

	MSXML2::IXMLDOMDocument * pDoc = NULL;

	if (1)
	{
		CEngineLog			log;
		log.Init(CVXMLParserEngineConfig::GetInstance()->sLevel.c_str(), CVXMLParserEngineConfig::GetInstance()->sLogFileName.c_str());
		log.Log(enginelog::LEVEL_INFO, __FUNCTIONW__, L"VXMLMenuParserModule started...");

		singleton_auto_pointer<MiniDumper> dumper;
		//singleton_auto_pointer<CXMLFileBuilder> fileBuilder;
		//singleton_auto_pointer<CXMLRootBuilder> rootBuilder;
		singleton_auto_pointer<CFolderMonitor> watcher;


		if (_tcsicmp(lpCmdLine, _T("/debug")) == 0)
		{
			try
			{

				while (1)
				{
					// 1. CREATE VXML DOC
					CComPtr < MSXML2::IXMLDOMDocument > pVXMLDoc;
					HRESULT hr = pVXMLDoc.CoCreateInstance(CLSID_DOMDocument, NULL, CLSCTX_INPROC_SERVER);
					if (FAILED(hr))
					{
						log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, _T("Error: CoCreateInstance IXMLDOMDocument failed"));
						break;
					}
					pVXMLDoc->put_async(VARIANT_FALSE);

					CComPtr <MSXML2::IXMLDOMProcessingInstruction> pPI;
					hr = pVXMLDoc->createProcessingInstruction(L"xml", L"version='1.0' encoding='utf-8'", &pPI);
					if (FAILED(hr))
					{
						log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, _T("Error: createProcessingInstruction failed"));
						break;
					}
					pVXMLDoc->appendChild(pPI, NULL);

					// 2. PARSE VXML FILES
					// get root elements
					CXmlBuilderTmpl <CRootDocument> rootParser;
					std::wstring outRootXML,
						rootURL = CVXMLParserEngineConfig::GetInstance()->sRootXML;

					if (!rootParser.Parse(rootURL, L"", L"", outRootXML))
					{
						log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, _T("Cannot parse root XML file = \"%s\", internal file or file-url error"), CVXMLParserEngineConfig::GetInstance()->sRootXML.c_str());
						break;
					}

					//if (1) // check xml
					//{
					//	CComPtr < MSXML2::IXMLDOMNode > pRootDoc = rootParser.getXMLDom();
					//	_bstr_t out_xml;
					//	pRootDoc->get_xml(&out_xml.GetBSTR());

					//	std::wstring s(out_xml.GetBSTR());
					//}

					// get parsing file list
					CFolderMonitor::URLContainer fileList = watcher->getVXMLFileList();
					CXmlBuilderTmpl <CVXMLDocument> vxmlParser;
					CItem::ItemCollector totalItems;
					// parse file list
					for (CFolderMonitor::URLContainer::const_iterator cit = fileList.begin(); cit != fileList.end(); ++cit)
					{
						std::wstring vxml_file_url = *cit,
							out_src;
						if (!vxmlParser.Parse(vxml_file_url, L""/*ParentItem->root()*/, L""/*ParentItem->code()*/, out_src))
						{
							log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, _T("Error: failed to parse \"%s\""), vxml_file_url.c_str());
							continue;
						}

						CItem::ItemMap vxmlItems = vxmlParser.getItems();
						//totalItems.insert(vxmlItems.begin(), vxmlItems.end());
						std::transform(vxmlItems.begin(), vxmlItems.end(), std::back_inserter(totalItems), second(vxmlItems));
					}

					/*CItem::ItemMultiMap*/CItem::ItemMap totalMap;

					// concatinate root items with vxml menu items
					CItem::ItemMap rootItems = rootParser.getItems();
					for (CItem::ItemMap::iterator it = rootItems.begin(); it != rootItems.end(); ++it)
					{

						CItem::ItemPtr RootItem = it->second;
						CItem::ItemCollector childs;

						totalMap[RootItem->code()] = RootItem;
						// looking for a root_element
						if (!RootItem->root().length())
							continue;
						
						
						// get all childs item with current root
						//     root could be simple, like <0611_NOR_services>,
						//     or with "boroda" like <0611_NOR_services_33>,
						//     where "_33" - is "boroda"

						std::wstring borodaRoot = ifCompositeRoot(RootItem->root());

						if (!borodaRoot.empty())
						{ 
							std::copy_if(totalItems.begin(), totalItems.end(), std::back_inserter(childs), IXMLDocumentMy::CFindCompositeRoot(RootItem));
						}
						else
						{
							std::copy_if(totalItems.begin(), totalItems.end(), std::back_inserter(childs), IXMLDocumentMy::CFindRoot(RootItem));
						}
						// sort by root
						//std::sort(childs.begin(), childs.end(), IXMLDocumentMy::CSortByRoot());
						// create tree
						for (int i = 0; i < childs.size(); ++i)
						{
							CItem::ItemPtr clone = CItem::ItemPtr(new CItem(*childs[i]));
							//totalMap.insert(CItem::ItemPair(RootItem->name() + RootItem->root() + RootItem->code(), clone)); // use unique key
							clone->setCode(RootItem->code() + clone->code());
							//if (clone->getParentCode().empty())
							if (!borodaRoot.empty())
							{
								CItem::ItemCollector::const_iterator cit = std::find_if(childs.begin(), childs.end(), IXMLDocumentMy::CFindByCode(clone->getParentCode()));
								if (cit == childs.end())
								{
									clone->setParentCode(RootItem->code());
								}
								else
								{
									clone->setParentCode(RootItem->code() + clone->getParentCode());
								}
							}
							else
							{
								clone->setParentCode(RootItem->code() + clone->getParentCode());
							}

							

							totalMap[clone->code()] = clone;
						}

						
					}

					for (CItem::ItemMap::iterator it = totalMap.begin(); it != totalMap.end(); ++it)
					{
						CItem::ItemPtr item = it->second;

						CComPtr <MSXML2::IXMLDOMNode> pComRecNode = item->createNodeItem(pVXMLDoc, L"");
						if (!pComRecNode) // still nothing
							continue;

						if (!item->code().length())
							continue;

						std::wstring parentCode = item->getParentCode();

						CItem::ItemMap::iterator it2 = totalMap.find(parentCode);
						if (it2 != totalMap.end())
						{
							CItem::ItemPtr parentItem = it2->second;
							parentItem->createNodeItem(pVXMLDoc, L"");
							parentItem->AddChild(item);
						}
					}
					

					//for (CItem::ItemMap::iterator it = rootItems.begin(); it != rootItems.end(); ++it)
					//{
					//	// looking for a root_element
					//	if (!it->second->root().length())
					//		continue;

					//	CItem::ItemPtr RootItem = it->second;

					//	std::pair<typename std::multimap<std::wstring, CItem::ItemPtr>::iterator, typename std::multimap<std::wstring, CItem::ItemPtr>::iterator> ret;
					//	CItem::ItemCollector childs;

					//	ret = totalMap.equal_range(RootItem->name() + RootItem->root() + RootItem->code());


					//	std::transform(ret.first, ret.second, std::back_inserter(childs), second(totalMap));
					//	//for (CItem::ItemMultiMap::iterator it = ret.first; it != ret.second; ++it)
					//	//{
					//	//	childs.push_back(it->second);
					//	//}

					//	std::wstring borodaRoot = ifCompositeRoot(RootItem->root());

					//	// sort by root
					//	std::sort(childs.begin(), childs.end(), IXMLDocumentMy::CSortByRoot());
					//	// create tree
					//	for (int i = 0; i < childs.size(); ++i)
					//	{
					//		CItem::ItemPtr clone = childs[i];

					//		// get parent node
					//		CItem::ItemPtr parentItem;
					//		std::wstring parent_code = /*childs[i]*/clone->getParentCode();


					//		if (parent_code.empty())
					//		{
					//			parentItem = RootItem;
					//			//
					//			// set parent code
					//			//parent_code = RootItem->code();
					//		}
					//		else
					//		{
					//			CItem::ItemCollector::const_iterator cit = std::find_if(childs.begin(), childs.end(), IXMLDocumentMy::CFindByCode(parent_code));
					//			if (cit != childs.end())
					//			{
					//				// set parent code
					//				parentItem = *cit;
					//				//childs[i]->setParentCode(parentItem->code());

					//				//// create node
					//				//childs[i]->appendParentInformation();
					//				//childs[i]->createNodeItem(pVXMLDoc);
					//				//parentItem->AddChild(childs[i]);
					//			}
					//			else if (!borodaRoot.empty() && getSimpleParentCode(borodaRoot) == parent_code)
					//			{
					//				parentItem = RootItem;
					//			}
					//			else
					//			{
					//				log.Log(__FUNCTIONW__, _T("Error: cannot find parent item for \"%s\", code = \"%s\", parent_code=\"%s\""),
					//					/*childs[i]*/clone->name().c_str(), /*childs[i]*/clone->code().c_str(), parent_code.c_str());
					//				continue;
					//			}

					//		}

					//		//childs[i]->setParentCode(RootItem->code() /*+ parent_code*/);
					//		// create node
					//		//childs[i]->appendParentInformation();
					//		/*childs[i]*/clone->createNodeItem(pVXMLDoc, RootItem->code());
					//		parentItem->AddChild(/*childs[i]*/clone);
					//	}
					//}

					if (0)
					{
						CItem::ItemPtr RootItem = rootParser.getRootItem();
						CComPtr < MSXML2::IXMLDOMNode > child_first_node = RootItem->getNote();
						_bstr_t out_xml;
						child_first_node->get_xml(&out_xml.GetBSTR());

						std::wstring s(out_xml.GetBSTR());
					}

					//if (0) // check xml
					//{
					//	CComPtr < MSXML2::IXMLDOMNode > pRootDoc = rootParser.getXMLDom();
					//	_bstr_t out_xml;
					//	pRootDoc->get_xml(&out_xml.GetBSTR());

					//	std::wstring s(out_xml.GetBSTR());
					//}

					if (1)
					{
						// SAVE DOCUMENT

						CComPtr < MSXML2::IXMLDOMNode > child_first_node = NULL;
						//CComPtr < MSXML2::IXMLDOMNode > pRootDoc = rootParser.getXMLDom();
						CComPtr < MSXML2::IXMLDOMNode > pRootDoc = rootParser.getRootItem()->getNote();

						if (pRootDoc)
						{
							pRootDoc->get_firstChild(&child_first_node);
						}

						if (child_first_node)
						{

							//_bstr_t out_xml;
							//child_first_node->get_xml(&out_xml.GetBSTR());

							log.Log(enginelog::LEVEL_INFO, __FUNCTIONW__, _T("Save xml to \"%s\""), CVXMLParserEngineConfig::GetInstance()->sOutputURL.c_str());

							//pVXMLDoc->cloneNode(TRUE, &child_first_node);
							//pVXMLDoc->load(child_first_node,NULL);
							pVXMLDoc->appendChild(child_first_node, NULL);
							pVXMLDoc->save(variant_t(CVXMLParserEngineConfig::GetInstance()->sOutputURL.c_str()));
						}

					}


					break; // DONT FORGET TO EXIT
				}



				//if (2)
				//{
				//	std::wstring rootXML;

				//	CXmlBuilderTmpl <CRootDocument> rootDoc;

				//	if (!rootDoc.Parse(CVXMLParserEngineConfig::GetInstance()->sRootXML, L"", L"", rootXML))
				//		//if (!rootBuilder->Parse(CVXMLParserEngineConfig::GetInstance()->sRootXML, L"", L"", rootXML))
				//	{
				//		log.Log(__FUNCTIONW__, _T("Cannot parse root XML file = \"%s\", internal file or file-url error"), CVXMLParserEngineConfig::GetInstance()->sRootXML.c_str());
				//	}
				//	else
				//	{
				//		CComPtr < MSXML2::IXMLDOMNode > pRootDoc = rootDoc.getXMLDom();
				//		_bstr_t out_xml;
				//		pRootDoc->get_xml(&out_xml.GetBSTR());

				//		std::wstring s(out_xml.GetBSTR());

				//		CFolderMonitor::URLContainer fileList = watcher->getVXMLFileList();
				//		CItem::ItemMap rootItems = rootDoc.getItems();
				//		for (CItem::ItemMap::iterator it = rootItems.begin(); it != rootItems.end(); ++it)
				//		{
				//			// looking for a root_element
				//			if (!it->second->root().length())
				//				continue;
				//			// check for file url with the same root_element
				//			CItem::ItemPtr ParentItem = it->second;
				//			CItem::ItemMap totalItems;
				//			std::wstring vxml_url, src;
				//			for (CFolderMonitor::URLContainer::const_iterator cit = fileList.begin(); cit != fileList.end(); ++cit)
				//			{
				//				vxml_url = *cit;
				//				CXmlBuilderTmpl <CVXMLDocument> vxmlDoc;


				//				if (!vxmlDoc.Parse(vxml_url, ParentItem->root(), ParentItem->code(), src))
				//				{
				//					log.Log(__FUNCTIONW__, _T("Error: failed to parse \"%s\""), vxml_url.c_str());
				//					continue;
				//				}

				//				CItem::ItemMap vxmlItems = vxmlDoc.getItems();
				//				totalItems.insert(vxmlItems.begin(), vxmlItems.end());
				//			}
				//			totalItems[it->first] = it->second; // include parentItem

				//			//// CREATE VXML DOC
				//			CComPtr < MSXML2::IXMLDOMDocument > pVXMLDoc;
				//			HRESULT hr = pVXMLDoc.CoCreateInstance(CLSID_DOMDocument, NULL, CLSCTX_INPROC_SERVER);
				//			if (FAILED(hr))
				//			{
				//				//m_pLog->Log(__FUNCTIONW__, _T("Error: CoCreateInstance IXMLDOMDocument failed"));
				//				return NULL;
				//			}
				//			pVXMLDoc->put_async(VARIANT_FALSE);

				//			CComPtr <MSXML2::IXMLDOMProcessingInstruction> pPI;
				//			hr = pVXMLDoc->createProcessingInstruction(L"xml", L"version='1.0' encoding='utf-8'", &pPI);
				//			if (FAILED(hr))
				//			{
				//				//m_pLog->Log(__FUNCTIONW__, _T("Error: createProcessingInstruction failed"));
				//				return NULL;
				//			}
				//			pVXMLDoc->appendChild(pPI, NULL);

				//			// ADD CHILDREN
				//			for (CItem::ItemMap::const_iterator cit = totalItems.begin(); cit != totalItems.end(); ++cit)
				//			{
				//				std::wstring   code = cit->first;
				//				CItem::ItemPtr item = cit->second;

				//				CComPtr <MSXML2::IXMLDOMNode> pComRecNode = item->createNodeItem(pVXMLDoc);
				//				if (!pComRecNode) // still nothing
				//					continue;

				//				if (/*!item->getParentCode()*/!code.length())
				//					continue;

				//				std::wstring parentCode = item->getParentCode();
				//				CItem::ItemMap::iterator it2 = totalItems.find(parentCode);
				//				if (it2 != totalItems.end())
				//				{
				//					CItem::ItemPtr parentItem = it2->second;
				//					parentItem->createNodeItem(pVXMLDoc);
				//					parentItem->AddChild(item);
				//				}
				//			}


				//			ParentItem->getNote()->get_xml(&out_xml.GetBSTR());

				//			std::wstring s2(out_xml.GetBSTR());
				//		}

				//		CComPtr < MSXML2::IXMLDOMNode > child_first_node = NULL;

				//		if (pRootDoc)
				//		{
				//			pRootDoc->get_firstChild(&child_first_node);
				//		}

				//		if (child_first_node)
				//		{
				//			child_first_node->get_xml(&out_xml.GetBSTR());

				//			rootXML = std::wstring(out_xml.GetBSTR());

				//			//**************** SAVE DOCUMENT **************************
				//			log.Log(__FUNCTIONW__, _T("Save xml to \"%s\""), CVXMLParserEngineConfig::GetInstance()->sOutputURL.c_str());
				//			// create new document
				//			CComPtr < IXMLDOMDocument > pSaveDoc = LiteXML::XMLFromText(rootXML.c_str());
				//			pSaveDoc->save(variant_t(CVXMLParserEngineConfig::GetInstance()->sOutputURL.c_str()));

				//		}



				//	}

				//}



			}
			catch (std::exception& e)
			{
				log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, _T("Exception: \"%s\""), e.what());
			}
			catch (std::wstring& e)
			{
				log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, _T("Exception: \"%s\""), e.c_str());
			}
			catch (_com_error& e)
			{
				log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, _T("Exception: \"%s\""), (LPCWSTR)e.Description());
			}
			catch (...)
			{
				log.Log(enginelog::LEVEL_WARNING, __FUNCTIONW__, _T("Exception: Unknown unhandled exception"));
			}
		}
		else
		{
			watcher->InitTimer(&CVXMLMenuParserModule::VXMLMenuParse);
		}

		

		retval = _AtlModule.WinMain(nShowCmd);

		//MSG msg;
		//while (Sleep(5000), true)
		//{
		//	if (PeekMessage(&msg, 0, 0, 0, PM_NOREMOVE))
		//	if (!GetMessage(&msg, 0, 0, 0))
		//		break;
		//}
	}

	::CoUninitialize();

	return retval;
}

