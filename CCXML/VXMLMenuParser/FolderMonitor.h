/************************************************************************/
/* Name     : VXMLMenuParser\FolderMonitor.h                            */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 14 Jul 2014                                               */
/************************************************************************/
#pragma once

#include <vector>
#include <boost/thread.hpp>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>

#include "singleton.h"
#include "FileMonitor.h"
#include "SAXXmlParser.h"

class CEngineLog;
class CFolderMonitor : public singleton <CFolderMonitor>
{
	friend class singleton<CFolderMonitor>;

	typedef void(*SimpleCallbackFunc)();
public:
	class CFileFindHelper
	{
	public:
		CFileFindHelper(const std::wstring& _mask) :m_mask(_mask)
		{}

		bool operator()(const std::wstring& _rhs)
		{
			CAtlString file(_rhs.c_str());
			if (file.Find(m_mask.c_str()) >= 0)
			{
				m_out_url = _rhs;
				return true;
			}
			return false;
		}
	private:
		std::wstring m_mask;
		std::wstring m_out_url;

	};

	class CTimer
	{
	public:
		CTimer(boost::asio::io_service& _io, SimpleCallbackFunc _callback);
	private:
		void Loop();

	private:
		boost::asio::deadline_timer m_timer;
		boost::asio::strand m_strand;
		SimpleCallbackFunc m_callBackFunc;
	};
public:
	typedef std::vector <std::wstring> URLContainer;

public:
	CFolderMonitor();
	~CFolderMonitor();

	void InitTimer(/*boost::asio::io_service& _io,*/ SimpleCallbackFunc _callback);
	static void OnTimerExpired();

	// handlers
	void OnRename(LPCTSTR _lpszFolderName, LPCTSTR _lpszFileName, LPCTSTR _lpszNewName);
	void OnCreate(LPCTSTR _lpszFolderName, LPCTSTR _lpszFileName);
	void OnModify(LPCTSTR _lpszFolderName, LPCTSTR _lpszFileName);
	void OnDelete(LPCTSTR _lpszFolderName, LPCTSTR _lpszFileName);

//	void ParseIntoFile();
//private:
//	void Parse(std::wstring& rootXML);


public:
	//const URLContainer& getFileList()const;
	bool getFileByMask(const std::wstring _mask, std::wstring& out_url);
	URLContainer getVXMLFileList()const { return m_fileList; };
private:
	static void updateVXMLList();

private:
	static bool m_bNeedToUpdateFileList;
	CFileMonitor m_monitor;
	CFileMonitor::Connector m_changeFileConnector;
	//boost::shared_ptr<CEngineLog>  m_pLog;
	static URLContainer m_fileList;

	static boost::mutex m_mut;

	boost::shared_ptr<CFolderMonitor::CTimer> m_timer;
	static SimpleCallbackFunc m_callback;

	boost::asio::io_service m_io;
};


/******************************* eof *************************************/
