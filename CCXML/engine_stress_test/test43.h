/************************************************************************/
/* Name     : engine_stress_test\test43.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 07 Feb 2011                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest43: public CEngineTester
{
public:
	CTest43(){};
	~CTest43(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);

private:
	static int s_playWavCounter;
};

/******************************* eof *************************************/