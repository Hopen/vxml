/************************************************************************/
/* Name     : engine_stress_test\test11.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 18 Feb 2010                                               */
/************************************************************************/

#pragma once
#include "test_engine.h"

class CTest11: public CEngineTester
{
public:
	CTest11(){};
	~CTest11(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/