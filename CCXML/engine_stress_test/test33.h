/************************************************************************/
/* Name     : engine_stress_test\test33.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 29 Jul 2010                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest33: public CEngineTester
{
public:
	CTest33(){};
	~CTest33(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/