/************************************************************************/
/* Name     : engine_stress_test\test10.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 10 Feb 2010                                               */
/************************************************************************/

#pragma once
#include "test_engine.h"

class CTest10: public CEngineTester
{
public:
	CTest10(){};
	~CTest10(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/