/************************************************************************/
/* Name     : engine_stress_test\test38.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 12 Jan 2011                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest38: public CEngineTester
{
public:
	CTest38(){};
	~CTest38(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/