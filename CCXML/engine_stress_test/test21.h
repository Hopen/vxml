/************************************************************************/
/* Name     : engine_stress_test\test21.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 17 Mar 2010                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest21: public CEngineTester
{
public:
	CTest21(){};
	~CTest21(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/