/************************************************************************/
/* Name     : engine_stress_test\test42.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 02 Feb 2011                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest42: public CEngineTester
{
public:
	CTest42(){};
	~CTest42(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/