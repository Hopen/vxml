/************************************************************************/
/* Name     : engine_stress_test\test6.cpp                              */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 13 Jan 2010                                               */
/************************************************************************/
#include "test6.h"

//#define TEST_SCRIPT		"c:\\is3\\scripts\\vxml_test\\test3.vxml"
//#define TEST_SCRIPT		"c:\\is3\\scripts\\vxml_test\\test2.vxml"
#define TEST_SCRIPT		"c:\\is3\\scripts\\vxml_test\\test6.vxml"
//#define TEST_SCRIPT		"c:\\is3\\scripts\\vxml_test\\sip.vxml"
//#define TEST_SCRIPT		"c:\\is3\\scripts\\vxml_test\\moscow_VIP\\MAIN.vxml"

//#define TEST_SCRIPT		"c:\\is3\\scripts\\vxml_test\\test0.vxml"

void CTest6::InitEngine()
{
	HRESULT hr=S_OK;

	CMessage msg(L"TA2D_RUNSCRIPT");
	msg[L"FileName"] = TEST_SCRIPT;
	msg[L"A"]						= L"53";
	msg[L"ANumMask"]				= L"";
	msg[L"B"]						= L"6277";
	msg[L"BNumMask"]				= L"";
	msg[L"Board"]					= L"DTI2";
	msg[L"CallID"]					= ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"Host"]					= L"ERIDAN";
	msg[L"InitialMessage"]			= L"OFFERED";
	msg[L"MonitorDisplayedName"]	= L"vxmlinterpretator3_predotvet";
	msg[L"OfflineFlag"]				= L"1";
	m_nScriptID = ((__int64)0x0093D80C << 32) | m_nEngineCount;
	msg[L"ScriptID"]				= m_nScriptID;
	msg[L"Timeslot"]				= m_nEngineCount % 30;
	msg[L"ParentScriptID"] = m_nScriptID - 1;
	//msg[L"predefine"] = L"c:\\is3\\scripts\\vxml_test\\test43_1.vxml;C:\\is3\\scripts\\vxml_test\\";
	msg[L"parenttype"] = L"ccxml";
	msg[L"SigInfo"] = L"0x0601100702600109010A0201000408839069329429160F0A07031369929412750801801D038090A33102005A390231D0";
	msg[L"namelist"] = L"state0 somevar";
	msg[L"state0"] = L"init";
	msg[L"somevar"] = L"1";
	msg[L"WavFolder"] = L"c:\\is3\\scripts\\vxml_test\\Wavs\\0605\\";
	msg[L"VXMLFolder"] = L"c:\\is3\\scripts\\vxml_test\\Wavs\\0605\\";
	msg[L"GrammarFolder"] = L"c:\\is3\\scripts\\vxml_test\\Wavs\\0605\\";
	//msg[L"predefine"] = L"c:\\is3\\scripts\\vxml_test\\includes2.vxml";

	//msg[L"predefine"] = L"c:\\is3\\scripts\\vxml_test\\moscow_VIP_instance_2\\Includes\\Utils_msk.vxml";


	CPackedMsg pMsg(msg);

	if (FAILED(hr = m_pEngine->Init(pMsg(), this)))
	{
		printf("IEngine::Init() failed: 0x%p\n", hr);
		return;
	}
}

void CTest6::Work()
{
	CreateEngine();
	Sleep(100);
	//CMessage msg(L"OFFERED");
	//msg[L"CallID"]		= ((__int64)0x00063C40 << 32) | m_nEngineCount;
	//msg[L"A"]			= L"53";
	//msg[L"B"]			= L"6277";

	//CPackedMsg pMsg(msg);
	//m_pEngine->SetEvent(pMsg());

	//CMessage msg2(L"CONNECTED");
	//msg2[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	//CPackedMsg pMsg2(msg2);
	//m_pEngine->SetEvent(pMsg2());
	//Sleep(5000);



	//CMessage disconnect(L"DISCONNECTED");
	//disconnect[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	//m_pEngine->SetEvent((CPackedMsg(disconnect))());

	//Sleep(2000);

	for (int i = 0; i < 10000; i++)
	{
		m_pEngine->DoStep();
		Sleep(1000);
	}
	DestroyEngine();
	delete this;
}

BOOL CTest6::PostAuxMessage(PMESSAGEHEADER pMsgHeader)
{
	CMessage init_msg(pMsgHeader);
	int user_drop = 0;
	if (user_drop)
	{
		CMessage disconnect(L"DISCONNECTED");
		disconnect[L"CallID"]		= init_msg[L"CallID"].Value;
		m_pEngine->SetEvent((CPackedMsg(disconnect))());
	}

	if (init_msg == L"PLAY_WAV")
	{
		CMessage msg(L"PLAY_WAV_ACK");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());

		//msg.SetName(L"DIGIT_GOT");
		//msg[L"Digit"]       = L"1";
		//m_pEngine->SetEvent((CPackedMsg(msg))());


		msg.SetName(L"PLAY_WAV_COMPLETED");
		msg[L"TerminationReason"] = L"TM_EOD";
		msg[L"DigitsBuffer"]	  = L"";
		//msg[L"TerminationReason"] = L"TM_DIGIT";
		//msg[L"DigitsBuffer"]	  = L"1123#";
		m_pEngine->SetEvent((CPackedMsg(msg))());
		
		//CMessage msg(L"TERMINATE");
		//msg[L"immediate"] = false;
		//msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"] = init_msg[L"ScriptID"].Value;
		//msg[L"namelist"] = L"SomeVar1 SomeVar2";
		//msg[L"handler"] = L"TerminateHandler";
		//m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"GET_DIGITS")
	{
		CMessage msg(L"GET_DIGITS_COMPLETED");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"DigitsBuffer"]		= L"1#";
		//msg[L"TerminationReason"] = L"TM_MAXTIME";// L"TM_EOD";
		msg[L"TerminationReason"] = L"TM_EOD";
		//msg[L"DigitsBuffer"] = L"111";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"CLEAR_DIGITS")
	{
		CMessage msg(L"CLEAR_DIGITS_COMPLETED");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"ANY2TA_RUN_SCRIPT")
	{
		CMessage msg(L"RUNSCRIPT_OK");
		//CMessage msg(L"VXML_RUN_SCRIPT_FAILED");
		__int64 _nScriptID = init_msg[L"ScriptID"];

		_nScriptID = _nScriptID + 1;
		msg[L"SaveScriptID"] = _nScriptID;

		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		msg[L"CallID"] = init_msg[L"CallID"].Value;
		msg[L"ScriptID"] = _nScriptID;
		msg[L"targettype"] = L"dialog";
		//msg[L"ErrorDescription"] = L"Cannot load predefine";

		m_pEngine->SetEvent((CPackedMsg(msg))());

		msg.SetName(L"VXML_RUN_SCRIPT_OK");
		m_pEngine->SetEvent((CPackedMsg(msg))());

		::Sleep(300);
		msg.SetName(L"END_DIALOG");
		msg[L"CallID"] = init_msg[L"CallID"].Value;
		msg[L"namelist"] = L"somevar";
		msg[L"somevar"] = L"somevalue";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"V2SC_RUN_FUNCTION")
	{
		//CMessage msg(L"SC2V_RUN_FUNCTION_OK");
		CMessage msg(L"SC2V_RUN_FUNCTION_ACK");
		__int64 _nScriptID = init_msg[L"ScriptID"];
		//msg[L"ErrorCode"] = 12;
		//msg[L"ErrorDescription"] = L"blablabla";


		_nScriptID = _nScriptID + 1;
		msg[L"SaveScriptID"] = _nScriptID;

		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		msg[L"ScriptID"] = _nScriptID;
		msg[L"targettype"] = L"dialog";
		//msg[L"ErrorDescription"] = L"Cannot load predefine";

		m_pEngine->SetEvent((CPackedMsg(msg))());

		::Sleep(300);
		msg.SetName(L"SC2V_RUN_FUNCTION_COMPLETED");
		msg[L"namelist"] = L"somevar";
		msg[L"somevar"] = L"somevalue";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"dialog.transfer")
	{
		//m_pEngine->SetEvent((CPackedMsg(init_msg))());
		//CMessage msg(L"connection.disconnect.transfer");
		CMessage msg(L"dialog.transfer.complete");
		//CMessage msg(L"DISCONNECTED");
		msg[L"target"] = init_msg[L"DialogID"].Value;
		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		msg[L"CallID"] = init_msg[L"CallbackID"].Value;
		msg[L"targettype"] = L"dialog";
		//msg[L"DigitsBuffer"] = L"";

		m_pEngine->SetEvent((CPackedMsg(msg))());

	}


	//if (init_msg == L"END_DIALOG")
	//{
	//	m_pEngine->SetEvent((CPackedMsg(init_msg))());
	//}
	if (CParam* param = init_msg.ParamByName(L"DestinationAddress"))
	{
		if (param->AsInt64() == m_nScriptID)
			m_pEngine->SetEvent((CPackedMsg(init_msg))());
	}

	

	return TRUE;
}

/******************************* eof *************************************/