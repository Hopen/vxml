/************************************************************************/
/* Name     : engine_stress_test\test30.cpp                             */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 21 Jun 2010                                               */
/************************************************************************/
#include "test30.h"

#define TEST_SCRIPT		"C:\\is3\\scripts\\ccxml_test\\test30_b.ccxml"

void CTest30::InitEngine()
{
	HRESULT hr=S_OK;

	CMessage msg(L"TA2D_RUNSCRIPT");
	msg[L"FileName"] = TEST_SCRIPT;
	msg[L"A"]						= L"53";
	msg[L"ANumMask"]				= L"";
	msg[L"B"]						= L"6277";
	msg[L"BNumMask"]				= L"";
	msg[L"Board"]					= L"DTI2";
	msg[L"CallID"]					= ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"Host"]					= L"ERIDAN";
	msg[L"InitialMessage"]			= L"OFFERED";
	msg[L"MonitorDisplayedName"]	= L"vxmlinterpretator3_predotvet";
	msg[L"OfflineFlag"]				= L"1";
	m_nScriptID = ((__int64)0x0093D80C << 32) | m_nEngineCount;
	msg[L"ScriptID"]				= m_nScriptID;
	msg[L"Timeslot"]				= m_nEngineCount % 30;

	CPackedMsg pMsg(msg);

	if (FAILED(hr = m_pEngine->Init(pMsg(), this)))
	{
		printf("IEngine::Init() failed: 0x%p\n", hr);
		return;
	}
}

void CTest30::Work()
{
	CreateEngine();
	Sleep(100);
	CMessage msg(L"OFFERED");
	msg[L"CallID"]		= ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"A"]			= L"53";
	msg[L"B"]			= L"6277";
	msg[L"Host"]					= L"ERIDAN";
	msg[L"MonitorDisplayedName"] = L"vxmlinterpretator3_predotvet";

	CPackedMsg pMsg(msg);
	m_pEngine->SetEvent(pMsg());

	CMessage msg2(L"CONNECTED");
	msg2[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	CPackedMsg pMsg2(msg2);
	m_pEngine->SetEvent(pMsg2());
	//Sleep(INFINITE);

	for (int i = 0; i < 1000; i++)
	{
		m_pEngine->DoStep();
		Sleep(10);
	}
	DestroyEngine();
	delete this;
}

static __int64 iOutcommingCallID = 0;
static int sMakeCallCount = 0;

BOOL CTest30::PostAuxMessage(PMESSAGEHEADER pMsgHeader)
{
	CMessage init_msg(pMsgHeader);
	if (init_msg == L"MAKE_CALL")
	{
		++sMakeCallCount;
		// connection successful
		// proceed call
		CMessage msg(L"TS2D_MakeCallOk");

		/*
		18:07:16.934 P2EE4 T2388 S07D042FF-00039AEB FT IN Name: 
		TS2D_MakeCallOk; Host = "DR-UIVRTST811"; Board = "DTI1"; TimeSlot = 28; ReasonCode = 0; ReasonText = "EGC_NOERR"; 
		ReasonDescription = ""; CallID = 0x07D04303-101B330E; CallbackID = 0x07D04303-101B114D; 
		Group = "default"; SourceAddress = 0x07D04303-101B330E
		*/
		iOutcommingCallID = ((__int64)0x00063C40 << 32) | ++m_nEngineCount;

		msg[L"CallID"]		= iOutcommingCallID;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;

		msg[L"Host"] = L"Eridan";

		m_pEngine->SetEvent((CPackedMsg(msg))());

		msg[L"A"] = L"89067837625";
		msg[L"B"] = L"900";

		// hand up call
		msg.SetName(L"CONNECTED");
		m_pEngine->SetEvent((CPackedMsg(msg))());


		//// connection failed
		//CMessage msg(L"TS2D_MakeCallFailed");
		//msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ReasonDescription"] = L"Script already terminated, MAKE_CALL ignored";
		//m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"ConferenceJoinParty")
	{
		CMessage msg(L"ConferenceJoinPartyAck");
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());

	}		
	else if (init_msg == L"ROUTE_DEV")
	{
		CMessage msg(L"ROUTE_DEV_ACK");
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());

		msg.SetName(L"DISCONNECTED");
		if (sMakeCallCount == 1)
		{
			msg[L"CallID"] = iOutcommingCallID;
		}
		if (sMakeCallCount == 2)
		{
			msg[L"CallID"] = init_msg[L"CallID"].Value;
		}

		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"UNROUTE_DEV")
	{
		CMessage msg(L"UNROUTE_DEV_ACK");
		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());

		if (init_msg[L"ListenerID"].AsInt64() == iOutcommingCallID)
		{
			// hand up call
			msg.SetName(L"DISCONNECTED");
			msg[L"CallID"] = init_msg[L"CallbackID"].Value;
			m_pEngine->SetEvent((CPackedMsg(msg))());
		}

	}
	else if (init_msg == L"ANY2TA_RUN_SCRIPT")
	{
		CMessage msg(L"RUNSCRIPT_OK");
		//CMessage msg(L"VXML_RUN_SCRIPT_FAILED");
		__int64 _nScriptID = init_msg[L"ScriptID"];

		_nScriptID = _nScriptID + 1;
		msg[L"SaveScriptID"] = _nScriptID;

		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		msg[L"CallID"] = init_msg[L"CallID"].Value;
		msg[L"ScriptID"] = _nScriptID;
		msg[L"targettype"] = L"dialog";
		//msg[L"ErrorDescription"] = L"Cannot load predefine";

		m_pEngine->SetEvent((CPackedMsg(msg))());

		msg.SetName(L"VXML_RUN_SCRIPT_OK");
		m_pEngine->SetEvent((CPackedMsg(msg))());

		{
			CMessage msg(L"END_DIALOG");
			msg[L"SaveScriptID"] = _nScriptID;
			msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
			msg[L"CallID"] = init_msg[L"CallID"].Value;
			msg[L"ScriptID"] = _nScriptID;
			msg[L"targettype"] = L"dialog";

			msg[L"namelist"] = L"telnum";
			msg[L"telnum"] = L"900";

			m_pEngine->SetEvent((CPackedMsg(msg))());
		}

	}
	//else if (init_msg == L"GET_DIGITS")
	//{
	//	CMessage msg(L"GET_DIGITS_COMPLETED");
	//	msg[L"CallID"]		= init_msg[L"CallID"].Value;
	//	msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
	//	msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
	//	msg[L"DigitsBuffer"]		= L"1#";
	//	//m_pEngine->SetEvent((CPackedMsg(msg))()); // successful input

	//}
	//else if (init_msg == L"CLEAR_DIGITS")
	//{
	//	CMessage msg(L"CLEAR_DIGITS_COMPLETED");
	//	msg[L"CallID"]		= init_msg[L"CallID"].Value;
	//	msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
	//	msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
	//	m_pEngine->SetEvent((CPackedMsg(msg))());
	//}
	//else if (init_msg == L"STOP_VIDEO")
	//{
	//	CMessage msg(L"STOP_VIDEO_ACK");
	//	msg[L"CallID"]		= init_msg[L"CallID"].Value;
	//	msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
	//	msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
	//	m_pEngine->SetEvent((CPackedMsg(msg))());
	//	msg.SetName(L"STOP_VIDEO_COMPLETED");
	//	m_pEngine->SetEvent((CPackedMsg(msg))());
	//}
	//else if (init_msg == L"STOP_CHANNEL")
	//{
	//	CMessage msg(L"STOP_CHANNEL_ACK");
	//	msg[L"CallID"]		= init_msg[L"CallID"].Value;
	//	msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
	//	msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
	//	m_pEngine->SetEvent((CPackedMsg(msg))());
	//	msg.SetName(L"STOP_CHANNEL_COMPLETED");
	//	m_pEngine->SetEvent((CPackedMsg(msg))());
	//	msg.SetName(L"GET_DIGITS_COMPLETED");
	//	msg[L"DigitsBuffer"]		= L"1#";
	//	m_pEngine->SetEvent((CPackedMsg(msg))());
	//}

	return TRUE;
}

/******************************* eof *************************************/