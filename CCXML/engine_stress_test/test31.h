/************************************************************************/
/* Name     : engine_stress_test\test31.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 9 Jul 2010                                                */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest31: public CEngineTester
{
public:
	CTest31(){};
	~CTest31(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/