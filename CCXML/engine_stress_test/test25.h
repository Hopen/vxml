/************************************************************************/
/* Name     : engine_stress_test\test25.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 12 Apr 2010                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest25: public CEngineTester
{
public:
	CTest25(){};
	~CTest25(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/