/************************************************************************/
/* Name     : engine_stress_test\test1.h                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 13 Jan 2010                                               */
/************************************************************************/

#pragma once
#include "test_engine.h"

class CTest6: public CEngineTester
{
public:
	CTest6(){};
	~CTest6(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/