/************************************************************************/
/* Name     : engine_stress_test\test27.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 28 May 2010                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest27: public CEngineTester
{
public:
	CTest27(){};
	~CTest27(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/