/************************************************************************/
/* Name     : engine_stress_test\test23.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 23 Mar 2010                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest23: public CEngineTester
{
public:
	CTest23(){};
	~CTest23(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/