/************************************************************************/
/* Name     : engine_stress_test\test47.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 11 Jun 2013                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest47: public CEngineTester
{
public:
	CTest47(){};
	~CTest47(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/