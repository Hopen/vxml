// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__9B8B6995_0AD8_4F9D_85D0_CBE0DFE13C9E__INCLUDED_)
#define AFX_STDAFX_H__9B8B6995_0AD8_4F9D_85D0_CBE0DFE13C9E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifdef VLD_DEBUG
#include "..\Common\vld.h"
#endif


// TODO: reference additional headers your program requires here

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__9B8B6995_0AD8_4F9D_85D0_CBE0DFE13C9E__INCLUDED_)
