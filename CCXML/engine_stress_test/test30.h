/************************************************************************/
/* Name     : engine_stress_test\test30.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 21 Jun 2010                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest30: public CEngineTester
{
public:
	CTest30(){};
	~CTest30(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/