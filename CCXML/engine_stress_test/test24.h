/************************************************************************/
/* Name     : engine_stress_test\test24.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 30 Mar 2010                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest24: public CEngineTester
{
public:
	CTest24(){};
	~CTest24(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/