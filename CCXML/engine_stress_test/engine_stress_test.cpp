// engine_stress_test.cpp : Defines the entry point for the console application.
//
//
//#include "stdafx.h"
//#include "..\EvtModel\EvtModel.h"
//#include "..\Engine\Engine.h"
//#include "..\CCXMLCom\CCXMLCom.h"
////#include "..\VXMLCom\VXMLCom_i.h"
//
//#define TEST_SCRIPT		"D:\\Projects\\ccxml_test\\test5.ccxml"
////#define TEST_SCRIPT		"D:\\Projects\\ccxml_test\\test9_2.ccxml"
//
//
//__int64 g_nEngineCount = 0;
//
//class CEngineTester : public IEngineCallback
//{
//private:
//	IEngineVM*	m_pEngine;
//	HANDLE		m_hThread;
//	DWORD		m_dwThreadId;
//	__int64		m_nEngineId;
//
//	static DWORD WINAPI ThreadProc(CEngineTester* pTester);
//	void Work();
//
//	void CreateEngine();
//	void DestroyEngine();
//
//	// IEngineCallback methods
//
//	BOOL WINAPI PostMessage(PMESSAGEHEADER pMsgHeader);
//	BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
//	BOOL WINAPI SendMessage(PMESSAGEHEADER pInMsgHeader, PMESSAGEHEADER* pOutMsgHeader, DWORD dwTimeOut);
//	BOOL WINAPI SendAuxMessage(PMESSAGEHEADER pInMsgHeader, PMESSAGEHEADER* pOutMsgHeader, DWORD dwTimeOut);
//	BOOL WINAPI TimerMessage(PMESSAGEHEADER pMsgHeader, DWORD dwMilliSeconds);
//	void WINAPI	MarkDamaged();
//	BOOL WINAPI GetScriptName(BSTR* pbstrScriptName);
//	
////binding extention
//	virtual void WINAPI Bind(ULONGLONG qwDestination, PMESSAGEHEADER pMsg)
//	{
//	}
//	virtual void WINAPI Unbind(ULONGLONG qwDestination, PMESSAGEHEADER pMsg)
//	{
//	}
//	virtual void WINAPI RejectBound(ULONGLONG qwDestination, PMESSAGEHEADER pMsg)
//	{
//	}
//	virtual void WINAPI RejectAllBounds(PMESSAGEHEADER pMsg)
//	{
//	}
//
//public:
//	CEngineTester();
//	~CEngineTester();
//};

#include "all_tests_headers.h"
#include "..\VXMLCom\vxml\VXMLBase.h"
#include <crtdbg.h>

//#import "..\VXMLCache\Debug\VXMLCache.exe" named_guids

int main(int argc, char* argv[])
{
//	for (int i = 0; i < 2; i++)
//	{
//		new CTest1();
//		Sleep(50);
//	}
//
//	int i = 0;
//#define VAR(j) CTest##j()
//	new VAR (1);
//#undef VAR
	//::CoInitialize(NULL);
	//CComPtr< VXMLCacheLib::IResourceCache> pCache;
	//HRESULT hr = pCache.CoCreateInstance(VXMLCacheLib::CLSID_ResourceCache);


	//if (hr == CO_E_SERVER_EXEC_FAILURE)
	//{
	//	int test = 0;
	//}
	//::CoUninitialize();


#define TEST(I)\
	new CTest##I(); \
	Sleep(50);

	//TEST(1) //ccxml
	//TEST(3) //ccxml
	//TEST(3)
	//TEST(5)//ccxml
	//TEST(6) //vxml
	//TEST(7)
	//TEST(8)
	//TEST(9)
	//TEST(10) //vxml
	//TEST(11)
	//TEST(12)
	//TEST(13)
	//TEST(14)
	//TEST(15)
	//TEST(16)
	//TEST(17)	//vxml
	//TEST(18)
	//TEST(19)
	//TEST(20) // VXML
	//TEST(21)
	//TEST(22)
	//TEST(23) //VXML
	//TEST(24)
	//TEST(25) // 0605 test
	//TEST(26)
	//TEST(27)
	//TEST(28)
	//TEST(29) // video menu
	//TEST(30) //CCXML
	//TEST(31)
	//TEST(32)
	//TEST(33)
	//TEST(34)
	//TEST(36) // video form
	//TEST(38) //CCXML
	//TEST(39)
	//TEST(40)
	//TEST(41)
	//TEST(42)
	//TEST(43) //VXML engine
	//TEST(44)
	//TEST(46) //VXML engine
	//TEST(47) // Dialogic localhost test
	TEST(49) //CCXML engine
	//TEST(50) //vxml

#undef TEST

	//Sleep(5000);
	//Sleep(30000);
	Sleep(INFINITE);
	return 0;
}
//
//CEngineTester::CEngineTester() : 
//	m_pEngine(NULL), 
//	m_hThread(NULL), 
//	m_dwThreadId(0)
//{
//	m_hThread = ::CreateThread(
//		NULL, 
//		0, 
//		(LPTHREAD_START_ROUTINE)ThreadProc, 
//		this, 
//		0, 
//		&m_dwThreadId);
//	m_nEngineId = ++g_nEngineCount;
//}
//
//CEngineTester::~CEngineTester()
//{
//	::CloseHandle(m_hThread);
//}
//
//DWORD WINAPI CEngineTester::ThreadProc(CEngineTester* pTester)
//{
//	pTester->Work();
//	return 0;
//}
//
//void CEngineTester::Work()
//{
//	CreateEngine();
//	Sleep(100);
//	CMessage msg(L"OFFERED");
//	msg[L"CallID"]		= ((__int64)0x00063C40 << 32) | g_nEngineCount;
//	msg[L"A"]			= L"53";
//	msg[L"B"]			= L"6277";
//
//	CPackedMsg pMsg(msg);
//	m_pEngine->SetEvent(pMsg());
//
//	CMessage msg2(L"CONNECTED");
//	msg2[L"CallID"] = ((__int64)0x00063C40 << 32) | g_nEngineCount;
//	CPackedMsg pMsg2(msg2);
//	m_pEngine->SetEvent(pMsg2());
//	Sleep(INFINITE);
//
//	for (int i = 0; i < 10000; i++)
//	{
//		m_pEngine->DoStep();
//	}
//	DestroyEngine();
////	new CEngineTester();
//	delete this;
//}
//
//void CEngineTester::CreateEngine()
//{
//	HRESULT hr;
//
//	//hr = ::CoInitializeEx(NULL, COINIT_MULTITHREADED/*COINIT_APARTMENTTHREADED*/);
//	::CoInitialize(NULL);
//
//	//IUnknown *dis(NULL);
//	//hr = CoCreateInstance(
//	//	/*CLSID_VXMLComInterface*/CLSID_CCXMLComInterface,
//	//	0,
//	//	CLSCTX_INPROC_SERVER,
//	//	IID_IUnknown,
//	//	(LPVOID*)&dis);
//
//	//IEngineVM *eng(NULL);
//	//hr = dis->QueryInterface(IID_IEngineVM,(LPVOID*)&eng);
//
//
//	if (FAILED(hr = CoCreateInstance(
//		/*CLSID_VXMLComInterface*/CLSID_CCXMLComInterface,
//		0,
//		CLSCTX_INPROC_SERVER,
//		IID_IEngineVM,
//		(LPVOID*)&m_pEngine)))
//	{
//		printf("Cannot create engine: 0x%p\n", hr);
//		return;
//	}
//	else
//	{
//		printf("Engine %I64i created\n", m_nEngineId);
//	}
//
//	CMessage msg(L"TA2D_RUNSCRIPT");
//	msg[L"FileName"] = TEST_SCRIPT;
//	msg[L"A"]						= L"53";
//	msg[L"ANumMask"]				= L"";
//	msg[L"B"]						= L"6277";
//	msg[L"BNumMask"]				= L"";
//	msg[L"Board"]					= L"DTI2";
//	msg[L"CallID"]					= ((__int64)0x00063C40 << 32) | g_nEngineCount;
//	msg[L"Host"]					= L"ERIDAN";
//	msg[L"InitialMessage"]			= L"OFFERED";
//	msg[L"MonitorDisplayedName"]	= L"vxmlinterpretator3_predotvet";
//	msg[L"OfflineFlag"]				= L"1";
//	__int64 nScriptID = ((__int64)0x0093D80C << 32) | g_nEngineCount;
//	msg[L"ScriptID"]				= m_nScriptID;
//	msg[L"Timeslot"]				= g_nEngineCount % 30;
//
//	CPackedMsg pMsg(msg);
//
//	if (FAILED(hr = m_pEngine->Init(pMsg(), this)))
//	{
//		printf("IEngine::Init() failed: 0x%p\n", hr);
//		return;
//	}
//
//}
//
//void CEngineTester::DestroyEngine()
//{
//	if (m_pEngine)
//	{
//		m_pEngine->Release();
//		m_pEngine = NULL;
//		printf("Engine %I64i destroyed\n", m_nEngineId);
//		::CoUninitialize();
//	}
//}
//
//BOOL WINAPI CEngineTester::PostMessage(PMESSAGEHEADER pMsgHeader)
//{
//	return TRUE;
//}
//
//BOOL WINAPI CEngineTester::PostAuxMessage(PMESSAGEHEADER pMsgHeader)
//{
//	CMessage init_msg(pMsgHeader);
//	if (init_msg == L"PLAY_WAV")
//	{
//		CMessage msg(L"PLAY_WAV_ACK");
//		msg[L"CallID"]		= init_msg[L"CallID"].Value;
//		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
//		m_pEngine->SetEvent((CPackedMsg(msg))());
//		msg.SetName(L"PLAY_WAV_COMPLETED");
//		msg[L"TerminationReason"] = L"TM_EOD";
//		msg[L"DigitsBuffer"]	  = L"1#";
//		m_pEngine->SetEvent((CPackedMsg(msg))());
//	}
//	else if (init_msg == L"GET_DIGITS")
//	{
//		CMessage msg(L"GET_DIGITS_COMPLETED");
//		msg[L"CallID"]		= init_msg[L"CallID"].Value;
//		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
//		msg[L"Digits"]		= L"1#";
//		m_pEngine->SetEvent((CPackedMsg(msg))());
//	}
//	else if (init_msg == L"CLEAR_DIGITS")
//	{
//		CMessage msg(L"CLEAR_DIGITS_COMPLETED");
//		msg[L"CallID"]		= init_msg[L"CallID"].Value;
//		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
//		m_pEngine->SetEvent((CPackedMsg(msg))());
//	}
//	return TRUE;
//}
//
//BOOL WINAPI CEngineTester::SendMessage(PMESSAGEHEADER pInMsgHeader, 
//								   PMESSAGEHEADER* pOutMsgHeader, 
//								   DWORD dwTimeOut)
//{
//	return TRUE;
//}
//
//BOOL WINAPI CEngineTester::SendAuxMessage(PMESSAGEHEADER pInMsgHeader, 
//									  PMESSAGEHEADER* pOutMsgHeader, 
//									  DWORD dwTimeOut)
//{
//	return TRUE;
//}
//
//BOOL WINAPI CEngineTester::TimerMessage(PMESSAGEHEADER pMsgHeader, DWORD dwMilliSeconds)
//{
//	return TRUE;
//}
//
//void WINAPI	CEngineTester::MarkDamaged()
//{
//}
//
//BOOL WINAPI CEngineTester::GetScriptName(BSTR *pbstrScriptName)
//{
//	return TRUE;
//}