/************************************************************************/
/* Name     : engine_stress_test\test40.cpp                             */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 25 Jan 2011                                               */
/************************************************************************/
#include "test40.h"

#define TEST_SCRIPT		"c:\\is3\\scripts\\ccxml_test\\test40.ccxml"

void CTest40::InitEngine()
{
	HRESULT hr=S_OK;

	CMessage msg(L"TA2D_RUNSCRIPT");
	msg[L"FileName"] = TEST_SCRIPT;
	msg[L"A"] = L"53";
	msg[L"ANumMask"] = L"";
	msg[L"B"] = L"6277";
	msg[L"BNumMask"] = L"";
	msg[L"Board"] = L"DTI2";
	msg[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"Host"] = L"ERIDAN";
	msg[L"InitialMessage"] = L"OFFERED";
	msg[L"MonitorDisplayedName"] = L"vxmlinterpretator3_predotvet";
	msg[L"OfflineFlag"] = L"1";
	msg[L"namelist"] = L"state0 somevar";
	msg[L"state0"] = L"init";
	msg[L"somevar"] = L"1";
	//m_nScriptID = ((__int64)0x0093D80C << 32) | m_nEngineCount;
	m_nScriptID = ((__int64)0xA80D3958 << 32) | m_nEngineCount;

	msg[L"ScriptID"] = m_nScriptID;
	//__int64 nScriptID_TEST = msg.ParamByName(L"ScriptID")->AsInt64();
	msg[L"Timeslot"] = m_nEngineCount % 30;
	msg[L"ParentScriptID"] = 0;
	msg[L"parenttype"] = L"ccxml";

	CPackedMsg pMsg(msg);

	if (FAILED(hr = m_pEngine->Init(pMsg(), this)))
	{
		printf("IEngine::Init() failed: 0x%p\n", hr);
		return;
	}
}

void CTest40::Work()
{
	CreateEngine();
	Sleep(100);
	
	CMessage msg(L"OFFERED");
	msg[L"RunRequestID"] = 0x0000D23B0000DCCE;
	msg[L"InitialMessage"] = L"OFFERED";
	msg[L"MonitorDisplayedName"] = L"8007008000_FTTB";
	msg[L"Host"] = L"KD-IVR811";
	msg[L"B"] = L"0736405";
	msg[L"Board"] = L"DTI1";
	msg[L"FileName"] = TEST_SCRIPT;
	msg[L"A"] = L"9186170606";
	msg[L"SAM0"] = L"0736405";
	msg[L"TimeSlot"] = 2;
	msg[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"SigInfo"] = L"0x0601000702480009010A02010304060190706304F50A07031319687160600B07031008700008001302030228070310087000080031020000390231C0";
	msg[L"SourceAddress"] = 0x0000D23B00000000;
	msg[L"ScriptID"] = 0x0000D2500C6F82D8;
	msg[L"RMQ_FileMap"] = 0x0000000000000428;
	msg[L"RMQ_Event"] = 0x0000000000000420;
	msg[L"RMQ_Mutex"] = 0x0000000000000414;
	msg[L"WMQ_FileMap"] = 0x0000000000000418;
	msg[L"WMQ_Event"] = 0x000000000000041C;
	msg[L"SurrogateNumber"] = 12;

	CPackedMsg pMsg(msg);
	m_pEngine->SetEvent(pMsg());

	CMessage msg2(L"CONNECTED");
	msg2[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	CPackedMsg pMsg2(msg2);
	m_pEngine->SetEvent(pMsg2());

	for (int i = 0; i < 1000; i++)
	{
		m_pEngine->DoStep();
		Sleep(10);
	}
	DestroyEngine();
	delete this;
}

BOOL CTest40::PostAuxMessage(PMESSAGEHEADER pMsgHeader)
{
	CMessage init_msg(pMsgHeader);
	if (init_msg == L"PLAY_WAV")
	{
		CMessage msg(L"PLAY_WAV_ACK");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
		msg.SetName(L"PLAY_WAV_COMPLETED");
		msg[L"TerminationReason"] = L"TM_EOD";
		//int test = 0;
		//if (!test)
		msg[L"DigitsBuffer"] = L"1#";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"GET_DIGITS")
	{
		CMessage msg(L"GET_DIGITS_COMPLETED");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		//int test = 0;
		//if (!test)
		msg[L"DigitsBuffer"] = L"1#";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"CLEAR_DIGITS")
	{
		CMessage msg(L"CLEAR_DIGITS_COMPLETED");
		//msg[L"CallID"]		= init_msg[L"CallID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"ANY2TA_RUN_SCRIPT")
	{

		//{
		//	//CALL_FAIL; Error_GC = "GCRV_CCLIBSPECIFIC"; 
		//	// Error_ccLib = "GC_SS7_LIB"; Error_ccValue = 19; Error_ccMsg = "No description available"; 
		//	// Error_gcMsg = "Event caused by call control library specific failure"; SigInfo = "0x12028493"; 
		//	// CallID = 0x07D04303-38C0B205; SourceAddress = 0x07D04303-05020098

		//	CMessage msg(L"CALL_FAIL");
		//	msg[L"CallID"] = init_msg[L"CallID"].Value;
		//	msg[L"SigInfo"] = L"0x12028493";
		//	msg[L"Error_ccValue"] = 19;
		//	msg[L"Error_gcMsg"] = L"Event caused by call control library specific failure";

		//	m_pEngine->SetEvent((CPackedMsg(msg))());
		//}

		//count++;

		CMessage msg(L"RUNSCRIPT_OK");
		//CMessage msg(L"VXML_RUN_SCRIPT_FAILED");
		//__int64 _nScriptID = init_msg[L"ScriptID"].AsInt64();

		//_nScriptID = _nScriptID + 1;
		m_nScriptID += 1;
		msg[L"SaveScriptID"] = m_nScriptID;

		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		msg[L"CallID"] = init_msg[L"CallID"].Value;
		msg[L"ParentScriptID"] = init_msg[L"ParentScriptID"].Value;
		msg[L"ScriptID"] = m_nScriptID;
		msg[L"targettype"] = L"dialog";
		//msg[L"ErrorDescription"] = L"Cannot load predefine";

		m_pEngine->SetEvent((CPackedMsg(msg))());

		msg.SetName(L"VXML_RUN_SCRIPT_OK");
		m_pEngine->SetEvent((CPackedMsg(msg))());

		//{
		//	CMessage disconnect(L"DISCONNECTED");
		//	disconnect[L"CallID"] = init_msg[L"CallID"].Value;
		//	disconnect[L"ScriptID"] = _nScriptID;
		//	//disconnect[L"ScriptID"] = init_msg[L"ScriptID"].Value;
		//	//disconnect[L"CallbackID"] = init_msg[L"CallID"].Value;

		//	m_pEngine->SetEvent((CPackedMsg(disconnect))());
		//}

		//if (count == 2)
		//{
		//msg.SetName(L"END_DIALOG");
		////__int64 _nScriptID = init_msg[L"ScriptID"];
		//msg[L"SaveScriptID"] = m_nScriptID;
		//msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		//msg[L"CallID"] = init_msg[L"CallID"].Value;
		//msg[L"ScriptID"] = m_nScriptID;
		//msg[L"targettype"] = L"dialog";

		//msg[L"namelist"] = L"somevar";
		//msg[L"somevar"] = L"somevalue";
		//m_pEngine->SetEvent((CPackedMsg(msg))());
		//}

		//{
		//	/*13:44:29.988 PA290 T4EB4 S00000168-1CCE7733 FT IN Name: AgentLineConnected; AgentID = "0525587"; 
		//	StartDT = "2017-05-19 13:44:30"; UIVR = "rd-ivr813"; CallClID = "79054859303"; 
		//	CallRef = "00013109591ecc22"; EndDT = "2017-05-19 13:44:30"; Split = "56"; 
		//	DestinationAddress = 0x00000168-1CCE7733; Source = "NN"; Vector = "703"; 
		//	SourceAddress = 0x000000AE-000056B7*/

		//	CMessage msg(L"AgentLineConnected");
		//	msg[L"AgentID"] = L"0525587";
		//	msg[L"StartDT"] = L"2017-05-19 13:44:300525587";
		//	msg[L"UIVR"] = L"rd-ivr813";
		//	msg[L"CallClID"] = L"79054859303";
		//	msg[L"CallRef"] = L"00013109591ecc22";
		//	msg[L"EndDT"] = L"2017-05-19 13:44:30";
		//	msg[L"Split"] = L"56";
		//	msg[L"DestinationAddress"] = L"0x00000168-1CCE7733";
		//	msg[L"Source"] = L"NN";
		//	msg[L"Vector"] = L"703";
		//	msg[L"SourceAddress"] = L"0x000000AE-000056B7";
		//	m_pEngine->SetEvent((CPackedMsg(msg))());
		//}

	}
	else if (init_msg == L"TERMINATE")
	{
		CMessage msg(L"TERMINATED_DIALOG");
		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		msg[L"ScriptID"] = init_msg[L"ScriptID"];
		msg[L"namelist"] = L"somevar1 somevar2 ";
		msg[L"somevar1"] = 123456678;
		msg[L"somevar2"] = L"somevalue2";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	return TRUE;
}

/******************************* eof *************************************/