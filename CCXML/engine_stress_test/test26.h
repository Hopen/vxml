/************************************************************************/
/* Name     : engine_stress_test\test26.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 12 May 2010                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest26: public CEngineTester
{
public:
	CTest26(){};
	~CTest26(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/