/************************************************************************/
/* Name     : engine_stress_test\test50.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 28 Mar 2014                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest50: public CEngineTester
{
public:
	CTest50(){};
	~CTest50(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/