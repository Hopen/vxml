/************************************************************************/
/* Name     : engine_stress_test\test3.h                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 20 Jan 2010                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest3: public CEngineTester
{
public:
	CTest3(){};
	~CTest3(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/