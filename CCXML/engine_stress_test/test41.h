/************************************************************************/
/* Name     : engine_stress_test\test41.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 31 Jan 2011                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest41: public CEngineTester
{
public:
	CTest41(){};
	~CTest41(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/