/************************************************************************/
/* Name     : engine_stress_test\test16.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 01 Mar 2010                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest16: public CEngineTester
{
public:
	CTest16(){};
	~CTest16(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/