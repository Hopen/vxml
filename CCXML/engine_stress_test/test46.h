/************************************************************************/
/* Name     : engine_stress_test\test46.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 13 Mar 2013                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest46: public CEngineTester
{
public:
	CTest46(){};
	~CTest46(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/