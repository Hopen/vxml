/************************************************************************/
/* Name     : engine_stress_test\test3.cpp                              */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 20 Jan 2010                                               */
/************************************************************************/
#include "test3.h"

//#define TEST_SCRIPT		"D:\\Projects\\ccxml_test\\test3.ccxml"
//#define TEST_SCRIPT		"C:\\is3\\scripts\\ccxml_test\\test_s.ccxml"
//#define TEST_SCRIPT		"C:\\is3\\scripts\\ccxml_test\\0611_fareast_DTI.ccxml"
#define TEST_SCRIPT		"C:\\is3\\scripts\\ccxml_test\\0611_volga_SRT.ccxml"

void CTest3::InitEngine()
{
	HRESULT hr=S_OK;

	CMessage msg(L"TA2D_RUNSCRIPT");
	msg[L"FileName"] = TEST_SCRIPT;
	msg[L"A"]						= L"53";
	msg[L"ANumMask"]				= L"";
	msg[L"B"]						= L"6277";
	msg[L"BNumMask"]				= L"";
	msg[L"Board"]					= L"DTI2";
	msg[L"CallID"]					= ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"Host"]					= L"ERIDAN";
	msg[L"InitialMessage"]			= L"OFFERED";
	msg[L"MonitorDisplayedName"]	= L"vxmlinterpretator3_predotvet";
	msg[L"OfflineFlag"]				= L"1";
	m_nScriptID = ((__int64)0x0093D80C << 32) | m_nEngineCount;
	msg[L"ScriptID"]				= m_nScriptID;
	msg[L"Timeslot"]				= m_nEngineCount % 30;

	CPackedMsg pMsg(msg);

	if (FAILED(hr = m_pEngine->Init(pMsg(), this)))
	{
		printf("IEngine::Init() failed: 0x%p\n", hr);
		return;
	}
}

void CTest3::Work()
{
	//CMessage icr(L"ICR2S_BDDATA");
	//icr[L"ErrorCode"] = (__int64)0x00000000;
	//icr[L"ErrorDescription"] = L"All Ok";
	//icr[L"Data"] = L"<root><return><cell_id>44748</cell_id><eventtype>outSms</eventtype><lac>27423</lac><latitude>56.23052</latitude><longtitude>35.6348699999999</longtitude><result>Normal</result><standard>G3</standard><start_time_mills>1467019749456</start_time_mills></return><return><cell_id>44748</cell_id><eventtype>outSms</eventtype><lac>27423</lac><latitude>56.23052</latitude><longtitude>35.6348699999999</longtitude><result>Normal</result><standard>G3</standard><start_time_mills>1467019742612</start_time_mills></return><return><cell_id>44748</cell_id><eventtype>outSms</eventtype><lac>27423</lac><latitude>56.23052</latitude><longtitude>35.6348699999999</longtitude><result>Normal</result><standard>G3</standard><start_time_mills>1467019734554</start_time_mills></return><return><cell_id>44748</cell_id><eventtype>outSms</eventtype><lac>27423</lac><latitude>56.23052</latitude><longtitude>35.6348699999999</longtitude><result>Normal</result><standard>G3</standard><start_time_mills>1467019726940</start_time_mills></return><return><cell_id>44748</cell_id><eventtype>outSms</eventtype><lac>27423</lac><latitude>56.23052</latitude><longtitude>35.6348699999999</longtitude><result>Normal</result><standard>G3</standard><start_time_mills>1467019719102</start_time_mills></return><return><cell_id>44748</cell_id><eventtype>outSms</eventtype><lac>27423</lac><latitude>56.23052</latitude><longtitude>35.6348699999999</longtitude><result>Normal</result><standard>G3</standard><start_time_mills>1467019704268</start_time_mills></return><return><cell_id>44748</cell_id><eventtype>outCall</eventtype><lac>27423</lac><latitude>56.23052</latitude><longtitude>35.6348699999999</longtitude><result>Normal</result><standard>G3</standard><start_time_mills>1467016488747</start_time_mills></return><return><cell_id>44748</cell_id><eventtype>outCall</eventtype><lac>27423</lac><latitude>56.23052</latitude><longtitude>35.6348699999999</longtitude><result>Normal</result><standard>G3</standard><start_time_mills>1467016467795</start_time_mills></return><return><cell_id>44748</cell_id><eventtype>outCall</eventtype><lac>27423</lac><latitude>56.23052</latitude><longtitude>35.6348699999999</longtitude><result>Normal</result><standard>G3</standard><start_time_mills>1467016457563</start_time_mills></return><return><cell_id>37756</cell_id><eventtype>inCall</eventtype><lac>27423</lac><latitude>56.2499</latitude><longtitude>35.5104</longtitude><result>Normal</result><standard>G2</standard><start_time_mills>1466943325082</start_time_mills></return><return><cell_id>18297</cell_id><eventtype>inCall</eventtype><lac>27423</lac><latitude>56.23052</latitude><longtitude>35.6348699999999</longtitude><result>Normal</result><standard>G2</standard><start_time_mills>1466942901467</start_time_mills></return><return><cell_id>37756</cell_id><eventtype>inCall</eventtype><lac>27423</lac><latitude>56.2499</latitude><longtitude>35.5104</longtitude><result>Normal</result><standard>G2</standard><start_time_mills>1466927290234</start_time_mills></return><return><cell_id>18293</cell_id><eventtype>outCall</eventtype><lac>27423</lac><latitude>56.23052</latitude><longtitude>35.6348699999999</longtitude><result>Normal</result><standard>G2</standard><start_time_mills>1466882245908</start_time_mills></return><return><cell_id>37756</cell_id><eventtype>outCall</eventtype><lac>27423</lac><latitude>56.2499</latitude><longtitude>35.5104</longtitude><result>Normal</result><standard>G2</standard><start_time_mills>1466882240246</start_time_mills></return><return><cell_id>37756</cell_id><eventtype>outCall</eventtype><lac>27423</lac><latitude>56.2499</latitude><longtitude>35.5104</longtitude><result>Normal</result><standard>G2</standard><start_time_mills>1466882233146</start_time_mills></return><return><cell_id>37756</cell_id><eventtype>inCall</eventtype><lac>27423</lac><latitude>56.2499</latitude><longtitude>35.5104</longtitude><result>Normal</result><standard>G2</standard><start_time_mills>1466880229413</start_time_mills></return></root>";
	//icr[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;

	//CPackedMsg pIcr(icr);
	//PMESSAGEHEADER pPackedMsg = pIcr();

	//WCHAR wszBuf[4096] = { 0 };
	//CROMessage msg333(pPackedMsg);
	//msg333.Dump(wszBuf, 4096);


	CreateEngine();
	Sleep(100);
	CMessage msg(L"OFFERED");
	msg[L"CallID"]		= ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"A"]			= L"53";
	msg[L"B"]			= L"6277";

	CPackedMsg pMsg(msg);
	m_pEngine->SetEvent(pMsg());

	CMessage msg2(L"CONNECTED");
	msg2[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	CPackedMsg pMsg2(msg2);
	m_pEngine->SetEvent(pMsg2());

	for (int i = 0; i < 1000; i++)
	{
		m_pEngine->DoStep();
		Sleep(10);
	}
	DestroyEngine();
	delete this;
}

BOOL CTest3::PostAuxMessage(PMESSAGEHEADER pMsgHeader)
{
	CMessage init_msg(pMsgHeader);
	if (init_msg == L"ANY2TA_RUN_SCRIPT")
	{
		CMessage msg(L"RUNSCRIPT_OK");
		//CMessage msg(L"VXML_RUN_SCRIPT_FAILED");
		__int64 _nScriptID = init_msg[L"ScriptID"];

		_nScriptID = _nScriptID + 1;
		msg[L"SaveScriptID"] = _nScriptID;

		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		msg[L"CallID"] = init_msg[L"CallID"].Value;
		msg[L"ScriptID"] = _nScriptID;
		msg[L"targettype"] = L"dialog";
		//msg[L"ErrorDescription"] = L"Cannot load predefine";
		
		m_pEngine->SetEvent((CPackedMsg(msg))());

		msg.SetName(L"VXML_RUN_SCRIPT_OK");
		m_pEngine->SetEvent((CPackedMsg(msg))());

		::Sleep(300);
		msg.SetName(L"END_DIALOG");
		msg[L"CallID"] = init_msg[L"CallID"].Value;
		msg[L"namelist"] = L"somevar";
		msg[L"somevar"] = L"somevalue";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}


	return TRUE;
}

/******************************* eof *************************************/