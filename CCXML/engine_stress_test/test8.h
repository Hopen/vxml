/************************************************************************/
/* Name     : engine_stress_test\test8.h                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 02 Feb 2010                                               */
/************************************************************************/

#pragma once
#include "test_engine.h"

class CTest8: public CEngineTester
{
public:
	CTest8(){};
	~CTest8(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/