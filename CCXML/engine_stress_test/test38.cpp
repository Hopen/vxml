/************************************************************************/
/* Name     : engine_stress_test\test38.cpp                             */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 12 Jan 2011                                               */
/************************************************************************/
#include "test38.h"

#define TEST_SCRIPT		"c:\\is3\\scripts\\ccxml_test\\test38.ccxml"

void CTest38::InitEngine()
{
	HRESULT hr=S_OK;

	CMessage msg(L"TA2D_RUNSCRIPT");
	msg[L"FileName"] = TEST_SCRIPT;
	msg[L"A"] = L"53";
	msg[L"ANumMask"] = L"";
	msg[L"B"] = L"6277";
	msg[L"BNumMask"] = L"";
	msg[L"Board"] = L"DTI2";
	msg[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"Host"] = L"ERIDAN";
	msg[L"InitialMessage"] = L"OFFERED";
	msg[L"MonitorDisplayedName"] = L"vxmlinterpretator3_predotvet";
	msg[L"OfflineFlag"] = L"1";
	msg[L"namelist"] = L"state0 somevar";
	msg[L"state0"] = L"init";
	msg[L"somevar"] = L"1";
	//m_nScriptID = ((__int64)0x0093D80C << 32) | m_nEngineCount;
	m_nScriptID = ((__int64)0xA80D3958 << 32) | m_nEngineCount;

	msg[L"ScriptID"] = m_nScriptID;
	//__int64 nScriptID_TEST = msg.ParamByName(L"ScriptID")->AsInt64();
	msg[L"Timeslot"] = m_nEngineCount % 30;
	msg[L"ParentScriptID"] = 0;
	msg[L"parenttype"] = L"ccxml";


	CPackedMsg pMsg(msg);

	if (FAILED(hr = m_pEngine->Init(pMsg(), this)))
	{
		printf("IEngine::Init() failed: 0x%p\n", hr);
		return;
	}
}

void CTest38::Work()
{
	CreateEngine();
	Sleep(100);
	CMessage msg(L"OFFERED");
	msg[L"CallID"]		= ((__int64)0x00063C40 << 32) | m_nEngineCount;
	msg[L"A"]			= L"53";
	msg[L"B"]			= L"6277";

	CPackedMsg pMsg(msg);
	m_pEngine->SetEvent(pMsg());

	CMessage msg2(L"CONNECTED");
	msg2[L"CallID"] = ((__int64)0x00063C40 << 32) | m_nEngineCount;
	CPackedMsg pMsg2(msg2);
	m_pEngine->SetEvent(pMsg2());

	for (int i = 0; i < 1000; i++)
	{
		m_pEngine->DoStep();
		Sleep(10);
	}
	DestroyEngine();
	delete this;
}

BOOL CTest38::PostAuxMessage(PMESSAGEHEADER pMsgHeader)
{
	CMessage init_msg(pMsgHeader);
	if (init_msg == L"PLAY_WAV")
	{
		CMessage msg(L"PLAY_WAV_ACK");
		msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"DialogID"]	= init_msg[L"DialogID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
		msg.SetName(L"PLAY_WAV_COMPLETED");
		msg[L"TerminationReason"] = L"TM_EOD";
		msg[L"DigitsBuffer"]	  = L"1#";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"GET_DIGITS")
	{
		CMessage msg(L"GET_DIGITS_COMPLETED");
		msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"DialogID"]	= init_msg[L"DialogID"].Value;
		msg[L"DigitsBuffer"]		= L"2#";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"CLEAR_DIGITS")
	{
		CMessage msg(L"CLEAR_DIGITS_COMPLETED");
		msg[L"CallID"]		= init_msg[L"CallID"].Value;
		msg[L"CallbackID"]  = init_msg[L"CallbackID"].Value;
		//msg[L"ScriptID"]	= init_msg[L"ScriptID"].Value;
		msg[L"DialogID"]	= init_msg[L"DialogID"].Value;
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	else if (init_msg == L"ANY2TA_RUN_SCRIPT")
	{
		CMessage msg(L"RUNSCRIPT_OK");
		//CMessage msg(L"VXML_RUN_SCRIPT_FAILED");
		__int64 _nScriptID = init_msg[L"ScriptID"];

		bool isDialogPrepared = false;
		if (const CParam* isPredefine = init_msg.ParamByName(L"PrepareDialog"))
		{
			isDialogPrepared = true;
			msg[L"PrepareDialog"] = true;
		}

		_nScriptID = _nScriptID + 1;
		msg[L"SaveScriptID"] = _nScriptID;

		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		msg[L"CallID"] = init_msg[L"CallID"].Value;
		msg[L"ScriptID"] = _nScriptID;
		msg[L"targettype"] = L"dialog";
		//msg[L"ErrorDescription"] = L"Cannot load predefine";

		m_pEngine->SetEvent((CPackedMsg(msg))());

		msg.SetName(L"VXML_RUN_SCRIPT_OK");
		m_pEngine->SetEvent((CPackedMsg(msg))());

		m_pEngine->SetEvent((CPackedMsg(msg))());

		if (!isDialogPrepared)
		{
			::Sleep(300);
			msg.SetName(L"END_DIALOG");
			msg[L"CallID"] = init_msg[L"CallID"].Value;
			msg[L"namelist"] = L"somevar";
			msg[L"somevar"] = L"somevalue";
			m_pEngine->SetEvent((CPackedMsg(msg))());
		}
	}
	else if (init_msg == L"LAUNCHER")
	{
		CMessage msg(L"END_DIALOG");
		__int64 _nScriptID = init_msg[L"ScriptID"];
		msg[L"CallID"] = init_msg[L"CallID"].Value;
		msg[L"SaveScriptID"] = _nScriptID;
		msg[L"CallbackID"] = init_msg[L"CallbackID"].Value;
		msg[L"ScriptID"] = _nScriptID;
		msg[L"targettype"] = L"dialog";

		msg[L"namelist"] = L"somevar";
		msg[L"somevar"] = L"somevalue";
		m_pEngine->SetEvent((CPackedMsg(msg))());
	}
	return TRUE;
}

/******************************* eof *************************************/