/************************************************************************/
/* Name     : engine_stress_test\test19.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 03 Mar 2010                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest19: public CEngineTester
{
public:
	CTest19(){};
	~CTest19(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);
};

/******************************* eof *************************************/