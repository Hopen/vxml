/************************************************************************/
/* Name     : engine_stress_test\test49.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 07 Oct 2013                                               */
/************************************************************************/
#pragma once
#include "test_engine.h"

class CTest49: public CEngineTester
{
public:
	CTest49(){};
	~CTest49(){};
protected:
	virtual void InitEngine();
	virtual void Work();
	virtual BOOL WINAPI PostAuxMessage(PMESSAGEHEADER pMsgHeader);

private:
	static int s_playWavCounter;
};

/******************************* eof *************************************/