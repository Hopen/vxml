// CorrelationIDInfo.cpp : Implementation of CCorrelationIDInfo

#include ".\stdafx.h"
#include "CorrelationIDInfo.h"
#include ".\correlationidinfo.h"


// CCorrelationIDInfo


STDMETHODIMP CCorrelationIDInfo::get_CorrelationID(BSTR* pVal)
{
	try {
		SysFreeString(*pVal);
		return m_CorrelationID.CopyTo(pVal);
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCorrelationIDInfo::put_CorrelationID(BSTR newVal)
{
	try {
		m_CorrelationID = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}
