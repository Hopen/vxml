// CallingPartyInfo.h : Declaration of the CCallingPartyInfo

#pragma once
#include "resource.h"       // main symbols

#include "q931.h"


// CCallingPartyInfo

class ATL_NO_VTABLE CCallingPartyInfo : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CCallingPartyInfo, &CLSID_CallingPartyInfo>,
	public IDispatchImpl<ICallingPartyInfo, &IID_ICallingPartyInfo, &LIBID_q931Lib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CCallingPartyInfo()
	{
		m_NatureOfAddress = 0;
		m_OddEvenIndicator = 0;
		m_NumberingPlanIndicator = 0;
		m_ScreeningIndicator = 0;
		m_PresentationRestrictionIndicator = 0;
		m_CallingNumberIncompleteIndicator = 0;
		m_CallingAddressSignals = L"";
	}

DECLARE_REGISTRY_RESOURCEID(IDR_CALLINGPARTYINFO)


BEGIN_COM_MAP(CCallingPartyInfo)
	COM_INTERFACE_ENTRY(ICallingPartyInfo)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

private:
	long m_NatureOfAddress;
	long m_OddEvenIndicator;
	long m_NumberingPlanIndicator;
	long m_ScreeningIndicator;
	long m_PresentationRestrictionIndicator;
	long m_CallingNumberIncompleteIndicator;
	CComBSTR m_CallingAddressSignals;

public:

	STDMETHOD(get_NatureOfAddress)(LONG* pVal);
	STDMETHOD(put_NatureOfAddress)(LONG newVal);
	STDMETHOD(get_OddEvenIndicator)(LONG* pVal);
	STDMETHOD(put_OddEvenIndicator)(LONG newVal);
	STDMETHOD(get_NumberingPlanIndicator)(LONG* pVal);
	STDMETHOD(put_NumberingPlanIndicator)(LONG newVal);
	STDMETHOD(get_ScreeningIndicator)(LONG* pVal);
	STDMETHOD(put_ScreeningIndicator)(LONG newVal);
	STDMETHOD(get_PresentationRestrictionIndicator)(LONG* pVal);
	STDMETHOD(put_PresentationRestrictionIndicator)(LONG newVal);
	STDMETHOD(get_CallingNumberIncompleteIndicator)(LONG* pVal);
	STDMETHOD(put_CallingNumberIncompleteIndicator)(LONG newVal);
	STDMETHOD(get_CallingAddressSignals)(BSTR* pVal);
	STDMETHOD(put_CallingAddressSignals)(BSTR newVal);
};

OBJECT_ENTRY_AUTO(__uuidof(CallingPartyInfo), CCallingPartyInfo)
