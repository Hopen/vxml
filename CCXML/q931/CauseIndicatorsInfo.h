// CauseIndicatorsInfo.h : Declaration of the CCauseIndicatorsInfo

#pragma once
#include "resource.h"       // main symbols

#include "q931.h"


// CCauseIndicatorsInfo

class ATL_NO_VTABLE CCauseIndicatorsInfo : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CCauseIndicatorsInfo, &CLSID_CauseIndicatorsInfo>,
	public IDispatchImpl<ICauseIndicatorsInfo, &IID_ICauseIndicatorsInfo, &LIBID_q931Lib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CCauseIndicatorsInfo()
	{
		m_Location = 0;
		m_CodingStandart = 0;
		m_CauseValue = 0;
	}

DECLARE_REGISTRY_RESOURCEID(IDR_CAUSEINDICATORSINFO)


BEGIN_COM_MAP(CCauseIndicatorsInfo)
	COM_INTERFACE_ENTRY(ICauseIndicatorsInfo)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

private:
	long m_Location;
	long m_CodingStandart;
	long m_CauseValue;

public:

	STDMETHOD(get_Location)(LONG* pVal);
	STDMETHOD(put_Location)(LONG newVal);
	STDMETHOD(get_CodingStandart)(LONG* pVal);
	STDMETHOD(put_CodingStandart)(LONG newVal);
	STDMETHOD(get_CauseValue)(LONG* pVal);
	STDMETHOD(put_CauseValue)(LONG newVal);
};

OBJECT_ENTRY_AUTO(__uuidof(CauseIndicatorsInfo), CCauseIndicatorsInfo)
