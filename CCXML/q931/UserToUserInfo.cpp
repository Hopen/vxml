// UserToUserInfo.cpp : Implementation of CUserToUserInfo

#include "stdafx.h"
#include "UserToUserInfo.h"


// CUserToUserInfo


STDMETHODIMP CUserToUserInfo::get_UserToUser(BSTR* pVal)
{
	try {
		SysFreeString(*pVal);
		return m_UserToUser.CopyTo(pVal);
	}
	CATCHFILTERSNOTHROW
		return S_FALSE;


	return S_OK;
}


STDMETHODIMP CUserToUserInfo::put_UserToUser(BSTR newVal)
{
	try {
		m_UserToUser = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
		return S_FALSE;

	return S_OK;
}
