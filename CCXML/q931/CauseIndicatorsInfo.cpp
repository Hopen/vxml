// CauseIndicatorsInfo.cpp : Implementation of CCauseIndicatorsInfo

#include ".\stdafx.h"
#include "CauseIndicatorsInfo.h"
#include ".\causeindicatorsinfo.h"


// CCauseIndicatorsInfo


STDMETHODIMP CCauseIndicatorsInfo::get_Location(LONG* pVal)
{
	try {
		*pVal = m_Location;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCauseIndicatorsInfo::put_Location(LONG newVal)
{
	try {
		m_Location = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCauseIndicatorsInfo::get_CodingStandart(LONG* pVal)
{
	try {
		*pVal = m_CodingStandart;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCauseIndicatorsInfo::put_CodingStandart(LONG newVal)
{
	try {
		m_CodingStandart = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCauseIndicatorsInfo::get_CauseValue(LONG* pVal)
{
	try {
		*pVal = m_CauseValue;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCauseIndicatorsInfo::put_CauseValue(LONG newVal)
{
	try {
		m_CauseValue = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}
