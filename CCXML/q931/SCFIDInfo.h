// SCFIDInfo.h : Declaration of the CSCFIDInfo

#pragma once
#include "resource.h"       // main symbols

#include "q931.h"

// CSCFIDInfo

class ATL_NO_VTABLE CSCFIDInfo : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CSCFIDInfo, &CLSID_SCFIDInfo>,
	public IDispatchImpl<ISCFIDInfo, &IID_ISCFIDInfo, &LIBID_q931Lib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CSCFIDInfo()
	{
		m_SCFID = L"";
	}

DECLARE_REGISTRY_RESOURCEID(IDR_SCFIDINFO)


BEGIN_COM_MAP(CSCFIDInfo)
	COM_INTERFACE_ENTRY(ISCFIDInfo)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

private:
	CComBSTR m_SCFID;

public:

	STDMETHOD(get_SCFID)(BSTR* pVal);
	STDMETHOD(put_SCFID)(BSTR newVal);
};

OBJECT_ENTRY_AUTO(__uuidof(SCFIDInfo), CSCFIDInfo)
