// q931Info.h : Declaration of the Cq931Info

#pragma once
#include "resource.h"       // main symbols

#include "q931.h"


// Cq931Info

class ATL_NO_VTABLE Cq931Info : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<Cq931Info, &CLSID_q931Info>,
	public IDispatchImpl<Iq931Info, &IID_Iq931Info, &LIBID_q931Lib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	Cq931Info()
	{
		m_CalledParty.CoCreateInstance(CLSID_CalledPartyInfo);
		m_CallingPartyCategory.CoCreateInstance(CLSID_CallingPartyCategoryInfo);
		m_CallingParty.CoCreateInstance(CLSID_CallingPartyInfo);
		m_CauseIndicators.CoCreateInstance(CLSID_CauseIndicatorsInfo);
		m_CorrelationID.CoCreateInstance(CLSID_CorrelationIDInfo);
		m_LocationNumber.CoCreateInstance(CLSID_LocationNumberInfo);
		m_RedirectionNumber.CoCreateInstance(CLSID_RedirectionNumberInfo);
		m_RedirectionNumberRestriction.CoCreateInstance(CLSID_RedirectionNumberRestrictionInfo);
		m_SCFID.CoCreateInstance(CLSID_SCFIDInfo);
		m_RedirectionInformation.CoCreateInstance(CLSID_RedirectionInformationInfo);
		m_UserToUserInformation.CoCreateInstance(CLSID_UserToUserInfo);
	}

DECLARE_REGISTRY_RESOURCEID(IDR_Q931INFO)


BEGIN_COM_MAP(Cq931Info)
	COM_INTERFACE_ENTRY(Iq931Info)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

private:
	CComQIPtr<ICalledPartyInfo>						m_CalledParty;
	CComQIPtr<ICallingPartyCategoryInfo>			m_CallingPartyCategory;
	CComQIPtr<ICallingPartyInfo>					m_CallingParty;
	CComQIPtr<ICauseIndicatorsInfo>					m_CauseIndicators;
	CComQIPtr<ICorrelationIDInfo>					m_CorrelationID;
	CComQIPtr<ILocationNumberInfo>					m_LocationNumber;
	CComQIPtr<IRedirectionNumberInfo>				m_RedirectionNumber;
	CComQIPtr<IRedirectionNumberRestrictionInfo>	m_RedirectionNumberRestriction;
	CComQIPtr<ISCFIDInfo>							m_SCFID;
	CComQIPtr<IRedirectionInformationInfo>			m_RedirectionInformation;
	CComQIPtr<IUserToUserInfo>			            m_UserToUserInformation;
	

public:

	STDMETHOD(get_CalledPartyNumber)(ICalledPartyInfo** pVal);
	STDMETHOD(get_CallingPartyCategory)(ICallingPartyCategoryInfo** pVal);
	STDMETHOD(get_CallingPartyNumber)(ICallingPartyInfo** pVal);
	STDMETHOD(get_CauseIndicators)(ICauseIndicatorsInfo** pVal);
	STDMETHOD(get_CorrelationID)(ICorrelationIDInfo** pVal);
	STDMETHOD(get_LocationNumber)(ILocationNumberInfo** pVal);
	STDMETHOD(get_RedirectionNumber)(IRedirectionNumberInfo** pVal);
	STDMETHOD(get_RedirectionNumberRestriction)(IRedirectionNumberRestrictionInfo** pVal);
	STDMETHOD(get_SCFID)(ISCFIDInfo** pVal);
	STDMETHOD(get_RedirectionInformation)(IRedirectionInformationInfo** pVal);
	STDMETHOD(get_UserToUser)(IUserToUserInfo** pVal);
};

OBJECT_ENTRY_AUTO(__uuidof(q931Info), Cq931Info)
