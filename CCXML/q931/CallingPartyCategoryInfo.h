// CallingPartyCategoryInfo.h : Declaration of the CCallingPartyCategoryInfo

#pragma once
#include "resource.h"       // main symbols

#include "q931.h"


// CCallingPartyCategoryInfo

class ATL_NO_VTABLE CCallingPartyCategoryInfo : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CCallingPartyCategoryInfo, &CLSID_CallingPartyCategoryInfo>,
	public IDispatchImpl<ICallingPartyCategoryInfo, &IID_ICallingPartyCategoryInfo, &LIBID_q931Lib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CCallingPartyCategoryInfo()
	{
		m_CallingPartyCategory = 0;
	}

DECLARE_REGISTRY_RESOURCEID(IDR_CALLINGPARTYCATEGORYINFO)


BEGIN_COM_MAP(CCallingPartyCategoryInfo)
	COM_INTERFACE_ENTRY(ICallingPartyCategoryInfo)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

private:
	long m_CallingPartyCategory;

public:

	STDMETHOD(get_CallingPartyCategory)(LONG* pVal);
	STDMETHOD(put_CallingPartyCategory)(LONG newVal);
};

OBJECT_ENTRY_AUTO(__uuidof(CallingPartyCategoryInfo), CCallingPartyCategoryInfo)
