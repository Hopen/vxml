// CorrelationIDInfo.h : Declaration of the CCorrelationIDInfo

#pragma once
#include "resource.h"       // main symbols

#include "q931.h"


// CCorrelationIDInfo

class ATL_NO_VTABLE CCorrelationIDInfo : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CCorrelationIDInfo, &CLSID_CorrelationIDInfo>,
	public IDispatchImpl<ICorrelationIDInfo, &IID_ICorrelationIDInfo, &LIBID_q931Lib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CCorrelationIDInfo()
	{
		m_CorrelationID = L"";
	}

DECLARE_REGISTRY_RESOURCEID(IDR_CORRELATIONIDINFO)


BEGIN_COM_MAP(CCorrelationIDInfo)
	COM_INTERFACE_ENTRY(ICorrelationIDInfo)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

private:
	CComBSTR m_CorrelationID;

public:

	STDMETHOD(get_CorrelationID)(BSTR* pVal);
	STDMETHOD(put_CorrelationID)(BSTR newVal);
};

OBJECT_ENTRY_AUTO(__uuidof(CorrelationIDInfo), CCorrelationIDInfo)
