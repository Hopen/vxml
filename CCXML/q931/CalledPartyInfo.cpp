// CalledPartyInfo.cpp : Implementation of CCalledPartyInfo

#include ".\stdafx.h"
#include "CalledPartyInfo.h"
#include ".\calledpartyinfo.h"


// CCalledPartyInfo


STDMETHODIMP CCalledPartyInfo::get_NatureOfAddress(LONG* pVal)
{
	try {
		*pVal = m_NatureOfAddress;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCalledPartyInfo::put_NatureOfAddress(LONG newVal)
{
	try {
		m_NatureOfAddress = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCalledPartyInfo::get_OddEvenIndicator(LONG* pVal)
{
	try {
		*pVal = m_OddEvenIndicator;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCalledPartyInfo::put_OddEvenIndicator(LONG newVal)
{
	try {
		m_OddEvenIndicator = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCalledPartyInfo::get_NumberingPlanIndicator(LONG* pVal)
{
	try {
		*pVal = m_NumberingPlanIndicator;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCalledPartyInfo::put_NumberingPlanIndicator(LONG newVal)
{
	try {
		m_NumberingPlanIndicator = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCalledPartyInfo::get_InternalNetworkNumberIndicator(LONG* pVal)
{
	try {
		*pVal = m_InternalNetworkNumberIndicator;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCalledPartyInfo::put_InternalNetworkNumberIndicator(LONG newVal)
{
	try {
		m_InternalNetworkNumberIndicator = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCalledPartyInfo::get_CalledAddressSignals(BSTR* pVal)
{
	try {
		SysFreeString(*pVal);
		return m_CalledAddressSignals.CopyTo(pVal);
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CCalledPartyInfo::put_CalledAddressSignals(BSTR newVal)
{
	try {
		m_CalledAddressSignals = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}
