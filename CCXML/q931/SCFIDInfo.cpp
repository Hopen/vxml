// SCFIDInfo.cpp : Implementation of CSCFIDInfo

#include ".\stdafx.h"
#include "SCFIDInfo.h"
#include ".\scfidinfo.h"


// CSCFIDInfo


STDMETHODIMP CSCFIDInfo::get_SCFID(BSTR* pVal)
{
	try {
		SysFreeString(*pVal);
		return m_SCFID.CopyTo(pVal);
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CSCFIDInfo::put_SCFID(BSTR newVal)
{
	try {
		m_SCFID = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}
