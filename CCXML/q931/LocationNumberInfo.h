// LocationNumberInfo.h : Declaration of the CLocationNumberInfo

#pragma once
#include "resource.h"       // main symbols

#include "q931.h"


// CLocationNumberInfo

class ATL_NO_VTABLE CLocationNumberInfo : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CLocationNumberInfo, &CLSID_LocationNumberInfo>,
	public IDispatchImpl<ILocationNumberInfo, &IID_ILocationNumberInfo, &LIBID_q931Lib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CLocationNumberInfo()
	{
		m_NatureOfAddress = 0;
		m_OddEvenIndicator = 0;
		m_NumberingPlanIndicator = 0;
		m_ScreeningIndicator = 0;
		m_PresentationRestrictionIndicator = 0;
		m_InternationalNetworkNumberIndicator = 0;
		m_LocationNumber = L"";
	}

DECLARE_REGISTRY_RESOURCEID(IDR_LOCATIONNUMBERINFO)


BEGIN_COM_MAP(CLocationNumberInfo)
	COM_INTERFACE_ENTRY(ILocationNumberInfo)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

private:
	long m_NatureOfAddress;
	long m_OddEvenIndicator;
	long m_NumberingPlanIndicator;
	long m_ScreeningIndicator;
	long m_PresentationRestrictionIndicator;
	long m_InternationalNetworkNumberIndicator;
	CComBSTR m_LocationNumber;

public:

	STDMETHOD(get_NatureOfAddress)(LONG* pVal);
	STDMETHOD(put_NatureOfAddress)(LONG newVal);
	STDMETHOD(get_OddEvenIndicator)(LONG* pVal);
	STDMETHOD(put_OddEvenIndicator)(LONG newVal);
	STDMETHOD(get_NumberingPlanIndicator)(LONG* pVal);
	STDMETHOD(put_NumberingPlanIndicator)(LONG newVal);
	STDMETHOD(get_ScreeningIndicator)(LONG* pVal);
	STDMETHOD(put_ScreeningIndicator)(LONG newVal);
	STDMETHOD(get_PresentationRestrictionIndicator)(LONG* pVal);
	STDMETHOD(put_PresentationRestrictionIndicator)(LONG newVal);
	STDMETHOD(get_InternationalNNI)(LONG* pVal);
	STDMETHOD(put_InternationalNNI)(LONG newVal);
	STDMETHOD(get_LocationNumber)(BSTR* pVal);
	STDMETHOD(put_LocationNumber)(BSTR newVal);
};

OBJECT_ENTRY_AUTO(__uuidof(LocationNumberInfo), CLocationNumberInfo)
