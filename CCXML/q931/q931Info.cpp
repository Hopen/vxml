// q931Info.cpp : Implementation of Cq931Info

#include ".\stdafx.h"
#include "q931Info.h"
#include ".\q931info.h"


// Cq931Info


STDMETHODIMP Cq931Info::get_CalledPartyNumber(ICalledPartyInfo** pVal)
{
	try {
		if (pVal && *pVal) {
			(*pVal)->Release();
		}
		return m_CalledParty.CopyTo(pVal);
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP Cq931Info::get_CallingPartyCategory(ICallingPartyCategoryInfo** pVal)
{
	try {
		if (pVal && *pVal) {
			(*pVal)->Release();
		}
		return m_CallingPartyCategory.CopyTo(pVal);
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP Cq931Info::get_CallingPartyNumber(ICallingPartyInfo** pVal)
{
	try {
		if (pVal && *pVal) {
			(*pVal)->Release();
		}
		return m_CallingParty.CopyTo(pVal);
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP Cq931Info::get_CauseIndicators(ICauseIndicatorsInfo** pVal)
{
	try {
		if (pVal && *pVal) {
			(*pVal)->Release();
		}
		return m_CauseIndicators.CopyTo(pVal);
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP Cq931Info::get_CorrelationID(ICorrelationIDInfo** pVal)
{
	try {
		if (pVal && *pVal) {
			(*pVal)->Release();
		}
		return m_CorrelationID.CopyTo(pVal);
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP Cq931Info::get_LocationNumber(ILocationNumberInfo** pVal)
{
	try {
		if (pVal && *pVal) {
			(*pVal)->Release();
		}
		return m_LocationNumber.CopyTo(pVal);
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP Cq931Info::get_RedirectionNumber(IRedirectionNumberInfo** pVal)
{
	try {
		if (pVal && *pVal) {
			(*pVal)->Release();
		}
		return m_RedirectionNumber.CopyTo(pVal);
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP Cq931Info::get_RedirectionNumberRestriction(IRedirectionNumberRestrictionInfo** pVal)
{
	try {
		if (pVal && *pVal) {
			(*pVal)->Release();
		}
		return m_RedirectionNumberRestriction.CopyTo(pVal);
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP Cq931Info::get_SCFID(ISCFIDInfo** pVal)
{
	try {
		if (pVal && *pVal) {
			(*pVal)->Release();
		}
		return m_SCFID.CopyTo(pVal);
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP Cq931Info::get_RedirectionInformation(IRedirectionInformationInfo** pVal)
{
	try {
		if (pVal && *pVal) {
			(*pVal)->Release();
		}
		return m_RedirectionInformation.CopyTo(pVal);
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}


STDMETHODIMP Cq931Info::get_UserToUser(IUserToUserInfo** pVal)
{
	try {
		if (pVal && *pVal) {
			(*pVal)->Release();
		}
		return m_UserToUserInformation.CopyTo(pVal);
	}
	CATCHFILTERSNOTHROW
		return S_FALSE;
}
