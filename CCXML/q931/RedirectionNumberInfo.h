// RedirectionNumberInfo.h : Declaration of the CRedirectionNumberInfo

#pragma once
#include "resource.h"       // main symbols

#include "q931.h"


// CRedirectionNumberInfo

class ATL_NO_VTABLE CRedirectionNumberInfo : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CRedirectionNumberInfo, &CLSID_RedirectionNumberInfo>,
	public IDispatchImpl<IRedirectionNumberInfo, &IID_IRedirectionNumberInfo, &LIBID_q931Lib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
public:
	CRedirectionNumberInfo()
	{
		m_NatureOfAddress = 0;
		m_OddEvenIndicator = 0;
		m_NumberingPlanIndicator = 0;
		m_InternalNetworkNumberIndicator = 0;
		m_RedirectionAddressSignals = L"";
	}

DECLARE_REGISTRY_RESOURCEID(IDR_REDIRECTIONNUMBERINFO)


BEGIN_COM_MAP(CRedirectionNumberInfo)
	COM_INTERFACE_ENTRY(IRedirectionNumberInfo)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

private:
	long m_NatureOfAddress;
	long m_OddEvenIndicator;
	long m_NumberingPlanIndicator;
	long m_InternalNetworkNumberIndicator;
	CComBSTR m_RedirectionAddressSignals;

public:

	STDMETHOD(get_NatureOfAddress)(LONG* pVal);
	STDMETHOD(put_NatureOfAddress)(LONG newVal);
	STDMETHOD(get_OddEvenIndicator)(LONG* pVal);
	STDMETHOD(put_OddEvenIndicator)(LONG newVal);
	STDMETHOD(get_NumberingPlanIndicator)(LONG* pVal);
	STDMETHOD(put_NumberingPlanIndicator)(LONG newVal);
	STDMETHOD(get_InternalNetworkNumberIndicator)(LONG* pVal);
	STDMETHOD(put_InternalNetworkNumberIndicator)(LONG newVal);
	STDMETHOD(get_RedirectionAddressSignals)(BSTR* pVal);
	STDMETHOD(put_RedirectionAddressSignals)(BSTR newVal);
};

OBJECT_ENTRY_AUTO(__uuidof(RedirectionNumberInfo), CRedirectionNumberInfo)
