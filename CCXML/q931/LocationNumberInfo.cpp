// LocationNumberInfo.cpp : Implementation of CLocationNumberInfo

#include ".\stdafx.h"
#include "LocationNumberInfo.h"
#include ".\locationnumberinfo.h"


// CLocationNumberInfo


STDMETHODIMP CLocationNumberInfo::get_NatureOfAddress(LONG* pVal)
{
	try {
		*pVal = m_NatureOfAddress;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CLocationNumberInfo::put_NatureOfAddress(LONG newVal)
{
	try {
		m_NatureOfAddress = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CLocationNumberInfo::get_OddEvenIndicator(LONG* pVal)
{
	try {
		*pVal = m_OddEvenIndicator;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CLocationNumberInfo::put_OddEvenIndicator(LONG newVal)
{
	try {
		m_OddEvenIndicator = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CLocationNumberInfo::get_NumberingPlanIndicator(LONG* pVal)
{
	try {
		*pVal = m_NumberingPlanIndicator;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CLocationNumberInfo::put_NumberingPlanIndicator(LONG newVal)
{
	try {
		m_NumberingPlanIndicator = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CLocationNumberInfo::get_ScreeningIndicator(LONG* pVal)
{
	try {
		*pVal = m_ScreeningIndicator;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CLocationNumberInfo::put_ScreeningIndicator(LONG newVal)
{
	try {
		m_ScreeningIndicator = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CLocationNumberInfo::get_PresentationRestrictionIndicator(LONG* pVal)
{
	try {
		*pVal = m_PresentationRestrictionIndicator;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CLocationNumberInfo::put_PresentationRestrictionIndicator(LONG newVal)
{
	try {
		m_PresentationRestrictionIndicator = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CLocationNumberInfo::get_InternationalNNI(LONG* pVal)
{
	try {
		*pVal = m_InternationalNetworkNumberIndicator;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CLocationNumberInfo::put_InternationalNNI(LONG newVal)
{
	try {
		m_InternationalNetworkNumberIndicator = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CLocationNumberInfo::get_LocationNumber(BSTR* pVal)
{
	try {
		SysFreeString(*pVal);
		return m_LocationNumber.CopyTo(pVal);
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}

STDMETHODIMP CLocationNumberInfo::put_LocationNumber(BSTR newVal)
{
	try {
		m_LocationNumber = newVal;
		return S_OK;
	}
	CATCHFILTERSNOTHROW
	return S_FALSE;
}
