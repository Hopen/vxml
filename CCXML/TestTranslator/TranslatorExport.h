/************************************************************************/
/*                                                                      */
/*          ���� ���� ��� ��������, �� ��� ������� �������� �.�.        */
/*                  ���� ���, � �� ����, ��� ��� �������.               */
/*                                                                      */
/************************************************************************/

#ifndef _TRANSLATOREXPORT_H_
#define _TRANSLATOREXPORT_H_

#define TRANS_API					WINAPI

#define ERROR_WARNING				0
#define ERROR_WARNING_ERROR			1
#define ERROR_WARNING_FATALERR		2

#define TRANSLATOR_IOTYPE_FILE		0
#define TRANSLATOR_IOTYPE_MEMORY	1


// Translator parameters

typedef struct tagTRANSLATORPARAMS {
	DWORD dwInputType;
	union tagINPUT {
		LPCWSTR File;
		LPCSTR Code;
	} Input;
	DWORD dwOutputType;
	union tagOUTPUT {
		LPCWSTR File;
		struct tagMEM {
			LPVOID Ptr;
			DWORD Size;
		} Mem;
	} Output;
	LPCWSTR IncludeDir;
	struct COMPILERPARAMS {
		int nMaxErr;
		bool bWarnAsErr;
		LPCWSTR pszDisabledWarnings;
	} CompilerParams;
} TRANSLATORPARAMS, *LPTRANSLATORPARAMS;

typedef const LPTRANSLATORPARAMS LPCTRANSLATORPARAMS;


// Code completion parameters

typedef struct tagCODECOMPLETE {
	WCHAR	szIdName[256];		// in
	int		nIdLine;			// in
	LPSTR	pszCode;			// in
	WCHAR	szIdType[256];		// out
	WCHAR	szProgId[256];		// out
	LPWSTR	pszTypeInfo;		// out
} CODECOMPLETE, *LPCODECOMPLETE;


// Translator error

typedef struct tagTRANSLATORERROR {
	int Level;
	WCHAR File[MAX_PATH];
	int Line;
	WCHAR Code[32];
	WCHAR Msg[256];
} TRANSLATORERROR, *LPTRANSLATORERROR;

typedef const LPTRANSLATORERROR LPCTRANSLATORERROR;


extern "C" {

LPVOID TRANS_API TranslatorCreate();
BOOL TRANS_API TranslatorTranslate(LPCVOID pTranslator,
								   LPCTRANSLATORPARAMS pParams);
DWORD TRANS_API TranslatorGetErrorCount(LPCVOID pTranslator);
BOOL TRANS_API TranslatorGetError(LPCVOID pTranslator, 
								  ULONG nIndex, 
								  LPTRANSLATORERROR pError);
BOOL TRANS_API TranslatorGetErrorText(LPCVOID pTranslator, 
									  ULONG nIndex, 
									  LPWSTR pszBuf, 
									  DWORD cbBuf);
BOOL TRANS_API TranslatorDestory(LPCVOID pTranslator);
BOOL TRANS_API TranslatorGetCodeCompleteInfo(LPCODECOMPLETE pData);

}


#endif // _TRANSLATOREXPORT_H_
