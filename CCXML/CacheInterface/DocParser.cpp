/************************************************************************/
/* Name     : CacheInterface\DocParser.cpp                              */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 07 Nov 2011                                               */
/************************************************************************/
#include "stdafx.h"
#include "DocParser.h"
//#include "CacheInterface_i.h"
//#include "CacheInterface.h"
#include "DocLoader.h"
#include "Log/SystemLog.h"
#include <fstream>

//using namespace enginelog;

CDocParser::CDocParser(CSystemLog* aLog)
	: m_contentHandler(new DEBUG_NEW_PLACEMENT CSAXContentHandler())
	, m_errorHandler(new DEBUG_NEW_PLACEMENT CSAXErrorHandler  ())
	, m_pLog(aLog)
{
	m_xmlParser .reset(new DEBUG_NEW_PLACEMENT CSAXXmlParser());
	//m_doc .reset(new DEBUG_NEW_PLACEMENT FactoryCollector());
	m_doc = Factory_c::CreateCollector();
	m_doc->SetName(L"VXML_base");
	m_current = m_doc;
}

CDocParser::~CDocParser()
{
}

int CDocParser::Parse2(const std::wstring& _xml, const std::wstring& _schema, bool bFile)
{
	int b = 0; // OK
	__try
	{
		b = Parse(_xml, _schema, bFile) ? 0 : 1;
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		b = -1;
	}

	return b;
	
}

bool CDocParser::Parse(const std::wstring& _xml, const std::wstring& _schema, bool bFile)
{
	m_pLog->LogString(LEVEL_FINEST, L"schema = \"%s\"", _schema.c_str());

	if (!_schema.empty())
	{
		m_xmlParser->SetParserFeature(L"schema-validation", true);
		m_xmlParser->SetParserFeature(L"use-schema-location", false);

		// decide, which namespace we should use
		int pos = -1;
		std::wstring _file(_schema),schema_namespace;
		if ((pos = _file.rfind('\\')) != -1)
		{
			_file.erase(0,pos+1);
		}
		if ((pos = _file.rfind('.')) != -1)
		{
			WCHAR file_name[MAX_PATH];ZeroMemory(file_name,sizeof(file_name));
			_file._Copy_s(file_name,MAX_PATH,pos);
			schema_namespace=std::wstring(L"x-schema:")+file_name;

		}
		m_pLog->LogString(LEVEL_WARNING,  L"schema_namespace = \"%s\"", schema_namespace.c_str());

		m_xmlParser->AddValidationSchema(schema_namespace, _schema);
	}

	// Set the content handler.
	if ( m_xmlParser->PutContentHandler(m_contentHandler) )
	{
		m_contentHandler->AttachElementHandler(this);
	}

	if ( m_xmlParser->PutErrorHandler(m_errorHandler) )
	{
		m_errorHandler->AttachElementHandler(this);
	}

	if (bFile)
		return m_xmlParser->ParseUrl(_xml);

	return m_xmlParser->Parse(_xml);

}

void CDocParser::OnXmlStartElement(const ISATXMLElement& xmlElement)
{
	auto newTag = Factory_c::CreateCollector(xmlElement);
	m_current = m_current->Add(newTag);
}

void CDocParser::OnXmlElementData(const wchar_t* aData, size_t aSize)
{
	m_current->Append(aData, aSize);
	//auto size = m_current->m_sText.size();

	//if (size > 0)
	//{
	//	++size;
	//}

	//m_current->m_sText.reserve(size + aSize);

	//if (size)
	//{
	//	m_current->m_sText += '\n';
	//}

	//std::copy(aData, aData + aSize, std::back_inserter(m_current->m_sText));
	//m_Texts.push_back(std::wstring(aData, aSize));
	//m_TextSize += aSize + 1;
}

//void CDocParser::OnXmlElementData(const std::wstring& elementData, int depth)
//{
//	m_current->Append(L"\n");
//	m_current->Append(elementData);
//	//std::wstring sPreviousText = m_current->GetText();
//	//if (sPreviousText.length())
//	//	sPreviousText+=L"\n";
//
//	//int pos = sPreviousText.find(L"ORDEF");
//	//if (pos > 0)
//	//{
//	//	int test = 0;
//	//}
//	//
//	//m_current->SetText(sPreviousText + elementData);
//}

void CDocParser::OnXmlEndElement(const ISATXMLElement& xmlElement)
{
	//std::wstring text;
	//text.reserve(m_TextSize);

	//int size = m_Texts.size();
	//int i = 0;
	//for (const auto& str : m_Texts)
	//{
	//	text.append(str);
	//	if (i++ < size)
	//	{
	//		text += '\n';
	//	}
	//}

	//auto size2 = text.size();

	//std::wofstream ostrm("d:\\1123.txt", std::ios::binary);
	//ostrm << m_current->GetText();
	//ostrm.close();


	//auto size = m_current->GetText().size();

	m_current = m_current->GetParent().lock();
}

void CDocParser::OnXmlError(int line, int column, const std::wstring& errorText, unsigned long errorCode)
{
	m_pLog->LogString(LEVEL_WARNING, L"Error parsing XML document. Line %d, char %d, reason \"%s\"",
						line, column, errorText.c_str()); 
}

bool CDocParser::OnXmlAbortParse(const ISATXMLElement& xmlElement)
{
	return false;
}

//const FactoryCollector& CDocParser::Get()const
//{
//	// skip VXML_base element
//	//FactoryCollector::TagContainer::const_iterator cit = m_current->GetChildren()->begin();
//	//return *cit;
//	return *m_current->GetChildren().begin();
//}

const Factory_c::CollectorPtr& CDocParser::Get() const
{
	return m_current->GetChildren().front();
}

/******************************* eof *************************************/