/************************************************************************/
/* Name     : CacheInterface\mrcpparser.h                               */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 23 Dec 2014                                               */
/************************************************************************/
#pragma once
#include "SAXXmlElementHandler.h"
#include "SAXXmlParser.h"
#include "factory.h"
#include <boost/shared_ptr.hpp>
#include "singleton.h"

//class CEngineLog;
class CSystemLog;
class CMrcpParser : public ISAXXmlElementHandler
{
public:
	CMrcpParser();
	~CMrcpParser();

	bool Parse(const std::wstring& _xml);

	// Handle XML content events during parsing.
	virtual void OnXmlStartElement(const ISATXMLElement& xmlElement);
	virtual void OnXmlElementData(const std::wstring& elementData, int depth);
	virtual void OnXmlEndElement(const ISATXMLElement& xmlElement);

	// Handle XML error events during parsing.
	virtual void OnXmlError(int line, int column, const std::wstring& errorText, unsigned long errorCode);

	// Return true to stop parsing earlier.
	virtual bool OnXmlAbortParse(const ISATXMLElement& xmlElement);

	//const Factory::CCollector& Get()const;
	const Factory::CMrcpFactory& Get()const;

private:
	//boost::shared_ptr< Factory::CCollector > m_doc;
	boost::shared_ptr < Factory::CMrcpFactory> m_mrcpDoc;
	boost::shared_ptr< CSAXXmlParser       > m_xmlParser;
	//Factory::CCollector * m_current;
	//boost::shared_ptr<CEngineLog>  m_pLog;
	singleton_auto_pointer<CSystemLog> m_pLog;
	//Factory::CCollector * m_parent;
	std::wstring m_CurNodeName;

	CComPtr<CSAXContentHandler> m_contentHandler;
	CComPtr<CSAXErrorHandler  > m_errorHandler;

};
/******************************* eof *************************************/