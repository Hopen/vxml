

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Sun Nov 18 19:41:01 2018
 */
/* Compiler settings for CacheInterface.idl:
    Oicf, W1, Zp8, env=Win32 (32b run), target_arch=X86 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __CacheInterface_i_h__
#define __CacheInterface_i_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IResourceCache_FWD_DEFINED__
#define __IResourceCache_FWD_DEFINED__
typedef interface IResourceCache IResourceCache;
#endif 	/* __IResourceCache_FWD_DEFINED__ */


#ifndef __ITestMTObj_FWD_DEFINED__
#define __ITestMTObj_FWD_DEFINED__
typedef interface ITestMTObj ITestMTObj;
#endif 	/* __ITestMTObj_FWD_DEFINED__ */


#ifndef __ResourceCache_FWD_DEFINED__
#define __ResourceCache_FWD_DEFINED__

#ifdef __cplusplus
typedef class ResourceCache ResourceCache;
#else
typedef struct ResourceCache ResourceCache;
#endif /* __cplusplus */

#endif 	/* __ResourceCache_FWD_DEFINED__ */


#ifndef __TestMTObj_FWD_DEFINED__
#define __TestMTObj_FWD_DEFINED__

#ifdef __cplusplus
typedef class TestMTObj TestMTObj;
#else
typedef struct TestMTObj TestMTObj;
#endif /* __cplusplus */

#endif 	/* __TestMTObj_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IResourceCache_INTERFACE_DEFINED__
#define __IResourceCache_INTERFACE_DEFINED__

/* interface IResourceCache */
/* [unique][helpstring][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_IResourceCache;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("DCF2F5EA-5A75-45F4-8E84-34176E8FDB09")
    IResourceCache : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetDocument( 
            /* [in] */ BSTR bstrURI,
            /* [in] */ BSTR sSchemaFile,
            /* [in] */ BSTR bstrScriptID,
            /* [in] */ BOOL bFile,
            /* [retval][out] */ SAFEARRAY * *pSerilizeDoc) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IResourceCacheVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IResourceCache * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IResourceCache * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IResourceCache * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IResourceCache * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IResourceCache * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IResourceCache * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IResourceCache * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *GetDocument )( 
            IResourceCache * This,
            /* [in] */ BSTR bstrURI,
            /* [in] */ BSTR sSchemaFile,
            /* [in] */ BSTR bstrScriptID,
            /* [in] */ BOOL bFile,
            /* [retval][out] */ SAFEARRAY * *pSerilizeDoc);
        
        END_INTERFACE
    } IResourceCacheVtbl;

    interface IResourceCache
    {
        CONST_VTBL struct IResourceCacheVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IResourceCache_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IResourceCache_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IResourceCache_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IResourceCache_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define IResourceCache_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define IResourceCache_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define IResourceCache_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define IResourceCache_GetDocument(This,bstrURI,sSchemaFile,bstrScriptID,bFile,pSerilizeDoc)	\
    ( (This)->lpVtbl -> GetDocument(This,bstrURI,sSchemaFile,bstrScriptID,bFile,pSerilizeDoc) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IResourceCache_INTERFACE_DEFINED__ */


#ifndef __ITestMTObj_INTERFACE_DEFINED__
#define __ITestMTObj_INTERFACE_DEFINED__

/* interface ITestMTObj */
/* [unique][nonextensible][dual][uuid][object] */ 


EXTERN_C const IID IID_ITestMTObj;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("0E4CCF3B-011F-4C80-B1CB-243764503620")
    ITestMTObj : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE WriteLog( 
            /* [in] */ BSTR _source) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct ITestMTObjVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            ITestMTObj * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            ITestMTObj * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            ITestMTObj * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            ITestMTObj * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            ITestMTObj * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            ITestMTObj * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [range][in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            ITestMTObj * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE *WriteLog )( 
            ITestMTObj * This,
            /* [in] */ BSTR _source);
        
        END_INTERFACE
    } ITestMTObjVtbl;

    interface ITestMTObj
    {
        CONST_VTBL struct ITestMTObjVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define ITestMTObj_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define ITestMTObj_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define ITestMTObj_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define ITestMTObj_GetTypeInfoCount(This,pctinfo)	\
    ( (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo) ) 

#define ITestMTObj_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    ( (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo) ) 

#define ITestMTObj_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    ( (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId) ) 

#define ITestMTObj_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    ( (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr) ) 


#define ITestMTObj_WriteLog(This,_source)	\
    ( (This)->lpVtbl -> WriteLog(This,_source) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __ITestMTObj_INTERFACE_DEFINED__ */



#ifndef __CacheInterfaceLib_LIBRARY_DEFINED__
#define __CacheInterfaceLib_LIBRARY_DEFINED__

/* library CacheInterfaceLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_CacheInterfaceLib;

EXTERN_C const CLSID CLSID_ResourceCache;

#ifdef __cplusplus

class DECLSPEC_UUID("7BF0B1A9-4496-4FA8-9291-E6E322B20147")
ResourceCache;
#endif

EXTERN_C const CLSID CLSID_TestMTObj;

#ifdef __cplusplus

class DECLSPEC_UUID("EDEC6AED-C299-4793-AE50-731E0D3A9FCB")
TestMTObj;
#endif
#endif /* __CacheInterfaceLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  LPSAFEARRAY_UserSize(     unsigned long *, unsigned long            , LPSAFEARRAY * ); 
unsigned char * __RPC_USER  LPSAFEARRAY_UserMarshal(  unsigned long *, unsigned char *, LPSAFEARRAY * ); 
unsigned char * __RPC_USER  LPSAFEARRAY_UserUnmarshal(unsigned long *, unsigned char *, LPSAFEARRAY * ); 
void                      __RPC_USER  LPSAFEARRAY_UserFree(     unsigned long *, LPSAFEARRAY * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


