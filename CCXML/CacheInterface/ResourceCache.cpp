// ResourceCache.cpp : Implementation of CResourceCache

#include "stdafx.h"
#include "ResourceCache.h"
//#include "CacheInterface.h"
#include "DocLoader.h"


// CResourceCache
HRESULT CResourceCache::FinalConstruct()
{
	//m_Interface.reset(new )
	return S_OK;
}


STDMETHODIMP CResourceCache::GetDocument(BSTR sURI, BSTR sSchemaFile, BSTR bstrURI, BOOL bFile, SAFEARRAY **pSA)
{
	singleton_auto_pointer<CDocLoader> loader;
	return loader->GetDocument(sURI, sSchemaFile, bstrURI, bFile, pSA);
}