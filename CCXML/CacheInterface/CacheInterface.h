/************************************************************************/
/* Name     : CacheInterface\CacheInterface.h                           */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 27 Jan 2010                                               */
/************************************************************************/
#pragma once
#include <map>
#include "boost\scoped_ptr.hpp"
#include "boost\shared_ptr.hpp"
#include "boost\thread.hpp"
#include <vector>

class CSystemLog;

class CCacheInterfaceModule : public CAtlServiceModuleT< CCacheInterfaceModule, IDS_SERVICENAME >
{
public :
	DECLARE_LIBID(LIBID_CacheInterfaceLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_CACHEINTERFACE, "{8696DB64-7D54-4CED-87E9-80143509CEA1}")
	HRESULT InitializeSecurity() throw()
	{
		// TODO : Call CoInitializeSecurity and provide the appropriate security settings for 
		// your service
		// Suggested - PKT Level Authentication, 
		// Impersonation Level of RPC_C_IMP_LEVEL_IDENTIFY 
		// and an appropiate Non NULL Security Descriptor.
		CoInitializeSecurity(NULL, -1, NULL, NULL, RPC_C_AUTHN_LEVEL_NONE,
			RPC_C_IMP_LEVEL_IMPERSONATE, NULL, EOAC_NONE, NULL);

		return S_OK;
	}

	HRESULT Run(int nShowCmd /* = SW_HIDE */)throw();

	// ctor
	CCacheInterfaceModule() = default;

public:
	void SendTestEvent();
};

/******************************* eof *************************************/