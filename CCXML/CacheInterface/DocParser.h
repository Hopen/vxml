/************************************************************************/
/* Name     : CacheInterface\DocParser.h                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 07 Nov 2011                                               */
/************************************************************************/
#pragma once
#include "SAXXmlElementHandler.h"
#include "SAXXmlParser.h"
#include "factory_c.h"
#include <boost/shared_ptr.hpp>
#include "Patterns/singleton.h"

using FactoryCollector = Factory_c::CCollector;
using FactoryCFTag = Factory_c::CFTag;

//class CEngineLog;
class CSystemLog;
class CDocParser : public ISAXXmlElementHandler
{
public:
	CDocParser(CSystemLog* aLog);
	~CDocParser();

public:
	int Parse2(const std::wstring& _xml, const std::wstring& _schema, bool bFile);
protected:
	bool Parse(const std::wstring& _xml, const std::wstring& _schema, bool bFile);
public:

	// Handle XML content events during parsing.
	virtual void OnXmlStartElement(const ISATXMLElement& xmlElement);
	//virtual void OnXmlElementData(const std::wstring& elementData, int depth);
	virtual void OnXmlElementData(const wchar_t* aData, size_t aSize);
	virtual void OnXmlEndElement(const ISATXMLElement& xmlElement);

	// Handle XML error events during parsing.
	virtual void OnXmlError(int line, int column, const std::wstring& errorText, unsigned long errorCode);

	// Return true to stop parsing earlier.
	virtual bool OnXmlAbortParse(const ISATXMLElement& xmlElement);

	//const FactoryCollector& Get()const{return *(m_doc.get());}
	const Factory_c::CollectorPtr& Get()const;

private:
	Factory_c::CollectorPtr m_doc;
	std::shared_ptr< CSAXXmlParser > m_xmlParser ;
	Factory_c::CollectorPtr m_current;
	CSystemLog* m_pLog;

	CComPtr<CSAXContentHandler> m_contentHandler;
	CComPtr<CSAXErrorHandler  > m_errorHandler;
	//std::vector <std::wstring> m_Texts;
	//size_t m_TextSize = 0;

};
/******************************* eof *************************************/