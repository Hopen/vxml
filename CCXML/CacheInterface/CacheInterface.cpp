// CacheInterface.cpp : Implementation of WinMain

#include <stdio.h>
#include "stdafx.h"
#include "resource.h"
#include "CacheInterface_i.h"
#include "CacheInterface.h"
#include "ResourceCache.h"
#include "mdump.h"

#include "DocParser.h"
#include <DbgHelp.h>
#include <ATLComTime.h>

#include "DocLoader.h"
#include "Log/SystemLog.h"
#include "factory_c.h"

HINSTANCE g_hInst;

HRESULT CCacheInterfaceModule::Run(int nShowCmd /* = SW_HIDE */)throw()
{
	//::CoInitializeEx(NULL, COINIT_MULTITHREADED);
	HRESULT hr = S_OK;

	if (1)
	{
		CCacheInterfaceModule* pT = static_cast<CCacheInterfaceModule*>(this);

		hr = pT->PreMessageLoop(nShowCmd);

		if (hr == S_OK)
		{
			singleton_auto_pointer<MiniDumper> dumper;
			singleton_auto_pointer<CDocLoader> loader;
			singleton_auto_pointer<CSystemLog> log;

			if (1)
			{
				//::Sleep(10000);
				log->LogString(LEVEL_INFO, L"VXMLCacheService started...");
				loader->Init(log->GetInstance());

				SetServiceStatus(SERVICE_RUNNING);
				pT->RunMessageLoop();
				log->LogString(LEVEL_INFO, L"VXMLCacheService cleaning...");
				loader->Clear();
			}
		}
		//_CrtDumpMemoryLeaks();

		if (SUCCEEDED(hr))
		{
			hr = pT->PostMessageLoop();
		}
	}

	//::CoUninitialize();
	return hr;
}

struct foo
{
	int a = 1;
	int b = 2;

public:
	template<class Archive>
	void serialize(Archive & archive)
	{
		archive(
			a,
			b
			);
	}
};

struct bar : public foo, public std::enable_shared_from_this<bar>
{
	int c = 3;

public:

	template<class Archive>
	void serialize(Archive & archive)
	{
		archive(
			cereal::base_class<foo>(this),
			c
			);
	}
};

void SerilizationTest()
{
	bar b;
	b.a = 22;
	std::ostringstream os;
	{
		cereal::BinaryOutputArchive oa(os);
		oa(b);
	}
	std::string  outbound_data_ = os.str();
	std::istringstream is(outbound_data_);

	bar b1;
	{
		cereal::BinaryInputArchive iarchive(is);
		iarchive(b1);
	}
	int test = 0;


	//auto parent = Factory_c::CreateCollector();
	//parent->Add(Factory_c::CreateCollector());
	//parent->Add(Factory_c::CreateCollector());

	//std::ostringstream os;
	//{
	//	cereal::BinaryOutputArchive oa(os);
	//	oa(parent);
	//}
	//std::string  outbound_data_ = os.str();

	//std::istringstream is(outbound_data_);

	//Factory_c::CollectorPtr collector;
	//{
	//	cereal::BinaryInputArchive iarchive(is);

	//	iarchive(collector);
	//}

	//if (collector)
	//{
	//	const auto& children = collector->GetChildren();
	//	for(const auto& child : children)
	//	{
	//		child->GetLine();
	//	}
	//}

}


void CCacheInterfaceModule::SendTestEvent()
{
	singleton_auto_pointer<CDocLoader> loader;
	singleton_auto_pointer<CSystemLog> log;

	loader->Init(log->GetInstance());

	std::wstring sSchemaFile = SVUtils::ExtractFilePath(SVUtils::GetModuleFileName(g_hInst)) + L"vxml.xsd";

	SAFEARRAY* pSA = NULL;
	//GetDocument(L"D:\\Projects\\vxml_test\\vmlconf.vxml",_bstr_t(sSchemaFile.c_str()),true,&pSA);

	//int time_msk = do_profiling([&]()
	//{
		loader->GetDocument(L"c:\\is3\\scripts\\vxml_test\\moscow_VIP\\Includes\\Utils_msk.vxml", _bstr_t(sSchemaFile.c_str()), L"1231", true, &pSA);
	//});
	//log->LogString(LEVEL_INFO, L"duration: %i ms", time_msk);


	//for (int i = 0; i < 5000; ++i)
	//{
	//	loader->GetDocument(L"c:\\is3\\scripts\\ccxml_test\\test12.ccxml", _bstr_t(sSchemaFile.c_str()), L"1231", true, &pSA);
	//	Factory_c::CollectorPtr doc;
	//	loader->DeserilizeDocument(pSA, doc);

	//	loader->GetDocument(L"c:\\is3\\scripts\\ccxml_test\\test13.ccxml", _bstr_t(sSchemaFile.c_str()), L"1231", true, &pSA);
	//	::SafeArrayDestroy(pSA);

	//	loader->GetDocument(L"c:\\is3\\scripts\\ccxml_test\\test14.ccxml", _bstr_t(sSchemaFile.c_str()), L"1231", true, &pSA);
	//	::SafeArrayDestroy(pSA);

	//	loader->GetDocument(L"c:\\is3\\scripts\\ccxml_test\\test15.ccxml", _bstr_t(sSchemaFile.c_str()), L"1231", true, &pSA);
	//	::SafeArrayDestroy(pSA);

	//	loader->GetDocument(L"c:\\is3\\scripts\\ccxml_test\\test16.ccxml", _bstr_t(sSchemaFile.c_str()), L"1231", true, &pSA);
	//	::SafeArrayDestroy(pSA);

	//	::Sleep(10);
	//}

	//Sleep(5000);

	loader->Clear();
}

CCacheInterfaceModule _AtlModule;

LPCTSTR c_lpszModuleName = _T("VXMLCacheService.exe");

BEGIN_OBJECT_MAP(ObjectMap)
	OBJECT_ENTRY(CLSID_ResourceCache, CResourceCache)
END_OBJECT_MAP()


//
extern "C" int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE /*hPrevInstance*/, 
                                LPTSTR lpCmdLine, int nShowCmd)
{
	//SerilizationTest();
	
	//::CoInitialize(NULL);
	::CoInitializeEx(NULL, COINIT_MULTITHREADED);
	g_hInst = hInstance;
	int retval = 0;

    retval = _AtlModule.WinMain(nShowCmd);
	_AtlModule.SendTestEvent();
	//_CrtDumpMemoryLeaks();
	::CoUninitialize();
	return retval;
}

