// TestDll.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>
#include <comutil.h>
#include <fstream>
#include "TranslatorExport.h"

typedef LPVOID ( TRANS_API *CREATEFUNCTION        ) (                                                               );
typedef BOOL   ( TRANS_API *DESTROYFUNCTION       ) ( LPCVOID pTranslator                                           );
typedef BOOL   ( TRANS_API *TRANSLATEFUNCTION     ) ( LPCVOID pTranslator, LPCTRANSLATORPARAMS pParams              );
typedef DWORD  ( TRANS_API *GETERRORCOUNTFUNCTION ) ( LPCVOID pTranslator                                           );
typedef BOOL   ( TRANS_API *GETERRORFUNCTION      ) ( LPCVOID pTranslator, ULONG nIndex, LPTRANSLATORERROR pError   );
typedef BOOL   ( TRANS_API *GETERRORTEXTFUNCTION  ) ( LPCVOID pTranslator, ULONG nIndex, LPWSTR pszBuf,	DWORD cbBuf );


int _tmain(int argc, _TCHAR* argv[])
{
	CREATEFUNCTION        pCreateFunction        ;
	TRANSLATEFUNCTION     pTranslateFunction     ;
	DESTROYFUNCTION       pDestoryFunction       ;
	GETERRORCOUNTFUNCTION pGetErrorCountFunction ;
	GETERRORFUNCTION      pGetErrorFunction      ;
	GETERRORTEXTFUNCTION  pGetErrorTextFunction  ;


	HMODULE hLib = 0;
	CoInitialize(NULL);

	try
	{
		hLib = LoadLibrary(L"TranslatorEx.dll");
		if (!hLib)
			return -1;

		// create
		pCreateFunction = (CREATEFUNCTION) GetProcAddress(hLib, "TranslatorCreate");
		void* pTrans = pCreateFunction();

		// translate
		_bstr_t SrcCode(L"C:\\is3\\scripts\\EK-VXML\\ccxml\\0611_ural_EKT.ccxml"), BinCode(L"d:\\out.ccxml");

		std::ifstream iFile(SrcCode.GetBSTR());
		std::string sInput((std::istreambuf_iterator<char>(iFile)), std::istreambuf_iterator<char>());
		iFile.close();

		//// file -> memory
		//TRANSLATORPARAMS params;
		//params.dwInputType = TRANSLATOR_IOTYPE_FILE;
		//params.Input.File = SrcCode.GetBSTR();
		//params.dwOutputType = TRANSLATOR_IOTYPE_MEMORY;
		////params.Output.File = BinCode.GetBSTR();
		//params.IncludeDir = L"";
		//params.CompilerParams.bWarnAsErr = false;
		//params.CompilerParams.nMaxErr = 0;
		//params.CompilerParams.pszDisabledWarnings = NULL;

		//// memory -> file
		//TRANSLATORPARAMS params;
		//params.dwInputType = TRANSLATOR_IOTYPE_MEMORY;
		////params.Input.File = SrcCode.GetBSTR();
		//params.Input.Code = sInput.c_str();
		//params.dwOutputType = TRANSLATOR_IOTYPE_FILE;
		//params.Output.File = BinCode.GetBSTR();
		//params.IncludeDir = L"";
		//params.CompilerParams.bWarnAsErr = false;
		//params.CompilerParams.nMaxErr = 0;
		//params.CompilerParams.pszDisabledWarnings = NULL;

		//// memory -> memory
		//TRANSLATORPARAMS params;
		//params.dwInputType = TRANSLATOR_IOTYPE_MEMORY;
		////params.Input.File = SrcCode.GetBSTR();
		//params.Input.Code = sInput.c_str();
		//params.dwOutputType = TRANSLATOR_IOTYPE_MEMORY;
		////params.Output.File = BinCode.GetBSTR();
		//params.IncludeDir = L"";
		//params.CompilerParams.bWarnAsErr = false;
		//params.CompilerParams.nMaxErr = 0;
		//params.CompilerParams.pszDisabledWarnings = NULL;

		// file -> file
		TRANSLATORPARAMS params;
		params.dwInputType = TRANSLATOR_IOTYPE_FILE;
		params.Input.File = SrcCode.GetBSTR();
		params.dwOutputType = TRANSLATOR_IOTYPE_FILE;
		params.Output.File = BinCode.GetBSTR();
		params.IncludeDir = L"";
		params.CompilerParams.bWarnAsErr = false;
		params.CompilerParams.nMaxErr = 0;
		params.CompilerParams.pszDisabledWarnings = NULL;

		pTranslateFunction = (TRANSLATEFUNCTION) GetProcAddress(hLib, "TranslatorTranslate");
		BOOL bSuccess = pTranslateFunction(pTrans,&params);

		if (bSuccess)
		{
			pGetErrorCountFunction = (GETERRORCOUNTFUNCTION) GetProcAddress(hLib, "TranslatorGetErrorCount");
			pGetErrorFunction      = (GETERRORFUNCTION     ) GetProcAddress(hLib, "TranslatorGetError");
			pGetErrorTextFunction  = (GETERRORTEXTFUNCTION ) GetProcAddress(hLib, "TranslatorGetErrorText");
			DWORD dwErrCount = pGetErrorCountFunction(pTrans);
			for (int i=0;i<dwErrCount;++i)
			{
				TRANSLATORERROR Err;
				ZeroMemory (&Err, sizeof(Err));
				if (pGetErrorFunction(pTrans,i,&Err))
				{
					int test = 0;
				}
				WCHAR errText[256];
				ZeroMemory (&errText, sizeof(errText));
				if (pGetErrorTextFunction(pTrans,i,errText,256))
				{
					int test = 0;
				}

			}
		}


		pDestoryFunction = (DESTROYFUNCTION) GetProcAddress(hLib, "TranslatorDestory");
		pDestoryFunction(pTrans);
	}
	catch (...)
	{
		FreeLibrary(hLib);
		return -1;
	}

	FreeLibrary(hLib);
	
	CoUninitialize();

	FreeLibrary(hLib);
	return 0;
}

