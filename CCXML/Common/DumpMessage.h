/************************************************************************/
/* Name     : Common\eventdispatcher.h                                  */
/* Author   : Andrey Alekseev                                           */
/* Company  : ExpertSolutions                                           */
/* Date     : 11 Apr 2018                                               */
/************************************************************************/

#pragma once
#include "..\Engine\EngineLog.h"
#include <exception>

namespace common
{
	template <class TLogger>
	void DumpMessage(
		enginelog::LEVEL aLevel,
		const wchar_t* aText,
		const CMessage& aMessage,
		const TLogger& aLog)
	{
		try
		{
			aLog->Log(
				aLevel,
				__FUNCTIONW__, 
				(std::wstring(aText) + L" - Dump Message (%i) - Name: %s").c_str(), 
				aMessage.ParamsCount(), 
				aMessage.GetName());

			std::wstring params;
			bool first = true;
			for (const auto& param : aMessage.Params())
			{
				if (!first)
				{
					params += L"; ";
				}
				first = false;
				params += param.Dump();
			}
			aLog->Log(enginelog::LEVEL_FINEST, __FUNCTIONW__, L">> %s", params.c_str());
		}
		catch (std::exception& aException)
		{
			aLog->Log(enginelog::LEVEL_FINEST, __FUNCTIONW__, L"__DumpMessage - Exception: %s", stow(aException.what()));
		}
		catch (...)
		{
			aLog->Log(enginelog::LEVEL_FINEST, __FUNCTIONW__, L"__DumpMessage - Exception: Unknown");
		}
	}

}