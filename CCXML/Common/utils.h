/************************************************************************/
/* Name     : Common\Utils.h                                            */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 14 Jan 2010                                               */
/************************************************************************/

#pragma once

#include <string>
#include <sstream> 


namespace Utils
{
	template<class T>
	std::wstring toStr(T aa)
	{
		std::wostringstream out;
		out << aa;
		return out.str();
	}

	template<class T>
	T toNum(std::wstring _in)
	{
		T num(0);
		std::wistringstream inn(_in);
		inn >> num;
		return num;
	}

	template<class T>
	T toNumX(std::wstring _in)
	{
		T num;
		if (_in.empty())
			num = 0;
		else
			std::wstringstream(_in) >> std::hex >> num;
		return num;
	}

	template <class T>
	T wtoi(std::wstring in)
	{
		T ret = 0;
		bool bTwos = false;
		if (in[0]=='-')
			bTwos = true;
		for (unsigned int i=bTwos?1:0;i<in.size();++i)
		{
			ret *= 10;
			ret += (in[i] - L'0');
		}
		if (bTwos)
			ret = 0 - ret;
		return ret;
	}

	template <class T>
	static std::basic_string<T> toLower(const std::basic_string<T>& aText)
	{
		std::basic_string<T> result;
		std::transform(aText.cbegin(), aText.cend(), std::inserter(result, result.begin()), ::tolower);

		return result;
	}

	template <class T>
	static std::basic_string<T> toUpper(const std::basic_string<T>& aText)
	{
		std::basic_string<T> result;
		std::transform(aText.cbegin(), aText.cend(), std::inserter(result, result.begin()), ::toupper);

		return result;
	}

	//extern inline ULONGLONG wtoi(std::wstring in)
	//{
	//	ULONGLONG ret = 0;

	//	for (unsigned int i=0;i<in.size();++i)
	//	{
	//		ret *= 10;
	//		ret += (in[i] - L'0');
	//	}

	//	return ret;
	//}

	extern inline int CheckFile(const std::wstring &dirname, const std::wstring &_filemask)
	{
		std::wstring dir_plus_mask = dirname; dir_plus_mask+= _filemask;

		WIN32_FIND_DATAW FindFileData;
		HANDLE hf;

		int stat = 1;
		hf = FindFirstFileW(dir_plus_mask.c_str(), &FindFileData);
		if (hf != INVALID_HANDLE_VALUE)
		{
			do
			{
				if(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
					continue;

				stat = 0; // Successful!
				break;
			}
			while (FindNextFileW(hf,&FindFileData)!=0);
			FindClose(hf);
		}
		return stat;
	}

	extern inline int GetFile(const std::wstring& uri, WIN32_FIND_DATAW* pDataOut)
	{
		HANDLE hf;

		int stat = 1;
		hf = FindFirstFileW((uri).c_str(), pDataOut);
		if (hf != INVALID_HANDLE_VALUE)
		{
			do
			{
				if(pDataOut->dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
					continue;

				stat = 0; // Successful!
				break;
			}
			while (FindNextFileW(hf,pDataOut)!=0);
			FindClose(hf);
		}
		return stat;
	}

	extern inline DWORD ConvertTimeFromCSS1(const std::wstring& sTime)
	{
		if (!sTime.length())
			return 0;
		DWORD dwTime = 0;
		std::wstring s(sTime);
		swscanf(sTime.c_str(), L"%u%s", &dwTime, s.c_str());
		s = (s.c_str()); // cleaning after swscanf()
		if (s == L"s")
		{
			dwTime *= 1000;
		}
		//int pos = -1;
		//std::wstring s(sTime);
		//if ((pos = s.find(L"ms",0))>=0)
		//{
		//	s.replace(pos,2,L"");
		//	dwTime = toNum<int>(s);
		//}
		//if ((pos = s.find(L"s",0))>=0)
		//{
		//	s.replace(pos,1,L"");
		//	dwTime = toNum<int>(s);
		//	dwTime*=1000;
		//}
		return dwTime;

	}

	extern inline DWORD ConvertTimeFromCSS2(const std::wstring& sTime)
	{
		if (!sTime.length())
			return 0;
		DWORD dwTime = 0;
		std::wstring s(sTime);
		if (swscanf(sTime.c_str(), L"%u%s", &dwTime, s.c_str()) != 2)
		{
			throw std::wstring(L"Invalid time value '" + sTime + L"'");
		}
		s = (s.c_str()); // cleaning after swscanf()
		if (s == L"s")
		{
			dwTime *= 1000;
		}
		else if (s != L"ms")
		{
			throw std::wstring(L"Invalid time unit value '" + s + L"'");
		}
		return dwTime;
	}

	extern inline std::wstring GetFileNameFromURI(const std::wstring& _uri)
	{
		int pos_left  = _uri.rfind(L'/');
		int pos_rigth = _uri.rfind(L'\\');

		int pos = (pos_left>pos_rigth)?pos_left:pos_rigth;

		std::wstring sName(_uri);
		if (pos >= 0) 
		{
			sName = _uri.substr(pos+1,_uri.length() - 1); // without "\"
		}

		return sName;
	}
	extern inline std::wstring GetFolderNameFromURI(const std::wstring& _uri)
	{
		int pos_left  = _uri.rfind(L'/');
		int pos_rigth = _uri.rfind(L'\\');

		int pos = (pos_left>pos_rigth)?pos_left:pos_rigth;

		std::wstring sName(_uri);
		if (pos >= 0) 
		{
			sName = _uri.substr(0,pos+1); // with "\"
		}

		return sName;
	}

}
/******************************* eof *************************************/