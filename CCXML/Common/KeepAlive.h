/************************************************************************/
/* Name     : VXMLCom\KeepAlive.h                                       */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 17 Jul 2013                                               */
/************************************************************************/
#pragma once
//#include <boost/asio.hpp>
//#include <boost/date_time/posix_time/posix_time.hpp>
//#include <boost/bind.hpp>
//#include <boost/thread.hpp>
//#include <boost/enable_shared_from_this.hpp>
//#include <boost/shared_ptr.hpp>


//#include ".\EvtModel.h"
//#include "..\Engine\Engine.h"


//typedef std::weak_ptr< IEngineCallback   > IEngineCallbackPtr;
//typedef std::shared_ptr< IEngineCallback   > IEngineCallbackPtr2;


//class CKeepAlive// : public boost::enable_shared_from_this<CKeepAlive> 
//{
//public:
//	CKeepAlive(const int& _max_wait, const __int64& scriptID, const __int64& parentScriptID, boost::asio::io_service& _io, IEngineCallbackPtr pExec);
//	virtual ~CKeepAlive();
//
//	void StopKeepAlive();
//
//	//__int64 GetScriptID()const { return m_scriptID; }
//
//private:
//	void OnWakeUp();
//	//void OnTimeOut();
//	void MakeKeepAliveMsg(const __int64& scriptID);
//
//private: 
//	int                      m_iMaxWait;
//	IEngineCallbackPtr       m_engine;
//	__int64                  m_scriptID;
//	__int64                  m_parentScriptID;
//	bool                     bStillKeepAlive;
//
//	//boost::asio::io_service m_io;
//	boost::asio::deadline_timer m_timer;
//	boost::asio::strand m_pStrand;
//	//boost::thread m_thread;
//};

/******************************* eof *************************************/