/************************************************************************/
/* Name     : CacheInterface\SAXXmlDataTypes.h                          */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 03 Nov 2011                                               */
/************************************************************************/

#pragma once
#include <string>
#include <map>
#include "sv_strutils.h"

#include <cereal/archives/binary.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/base_class.hpp>


class ISATXMLElement
{
public:
	using AttrContainer = std::map<std::wstring, std::wstring>;

public:
	ISATXMLElement() = default;
	~ISATXMLElement() = default;

	ISATXMLElement(const std::wstring& aName,
		const std::wstring& aText,
		const AttrContainer& aAttribs,
		int aLine)
		: m_sName(aName)
		, m_sText(aText)
		, m_iLine(aLine)
		, m_Attribs(aAttribs)
	{}

	template<class Archive>
	void serialize(Archive & archive)
	{
		archive(
			m_sName,
			m_sText,
			m_iLine,
			m_Attribs
			);
	}

public:

	std::wstring	m_sName;
	std::wstring	m_sText;
	int             m_iLine;
	AttrContainer	m_Attribs;

public:
	// Set 
	void SetName(const std::wstring& _name){m_sName = _name;}
	void SetText(const std::wstring& _text){m_sText = _text;}

	void Append(const wchar_t* aData, size_t aSize)
	{
		if (!m_sText.empty())
		{
			m_sText += '\n';
		}
		m_sText.append(aData, aSize);
		//auto size = m_sText.size();
		//
		//if (size > 0)
		//{
		//	++size;
		//}
		//m_sText.reserve(size + aSize);
		//if (size)
		//{
		//	m_sText[size] = '\n';
		//}

		//std::copy(aData, aData + aSize, std::back_inserter(m_sText));
	}

	void Append(const std::wstring& _text)
	{
		//m_sText.reserve(m_sText.size() + _text.size());
		m_sText.append(_text);
	}
	void SetAttr(const std::wstring&_attrname, const std::wstring& _attrvalue){m_Attribs[_attrname] = _attrvalue;}
	void SetLine(const int& _line){m_iLine = _line;}

	// Get
	std::wstring  GetName() const {return m_sName;}
	std::wstring  GetText() const {return m_sText;}
	AttrContainer GetAttr() const {return m_Attribs;} 
	int           GetLine() const {return m_iLine;}

	std::wstring GetAttrVal(const std::wstring& sName) const
	{
		AttrContainer::const_iterator it = m_Attribs.find(sName);
		if (it == m_Attribs.end())
		{
			throw format_wstring(L"Tag '%s' does not have attribute '%s'", m_sName.c_str(), sName.c_str());
		}
		return it->second;
	}
	bool GetAttrVal(const std::wstring& sName, std::wstring& sValue) const
	{
		AttrContainer::const_iterator it = m_Attribs.find(sName);
		if (it == m_Attribs.end())
		{
			return false;
		}
		sValue = it->second;
		return true;
	}
};

/******************************* eof *************************************/