/************************************************************************/
/* Name     : CacheInterface\SAXXmlParser.h                             */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 03 Nov 2011                                               */
/************************************************************************/

#pragma once
#include <atlbase.h>
#include "SAXContentHandler.h"
#include "SAXErrorHandler.h"
//class CSAXXmlParserImpl;

class CSAXXmlParser
{
public:
	CSAXXmlParser();
	~CSAXXmlParser();

	// Is the parser available (e.g. was the COM class created properly?).
	bool IsReady() const;

	// Put XML events handler
	bool PutContentHandler (MSXML2::ISAXContentHandler *pHandler);
	bool PutErrorHandler   (MSXML2::ISAXErrorHandler  *pErrHandler);
	// Attach XML events handler.
	//void AttachElementHandler(ISAXXmlElementHandler* pElementHandler);
	//void DetachElementHandler();

	// Set parser feature options.
	bool SetParserFeature(const std::wstring& featureName, bool value);
	bool GetParserFeature(const std::wstring& featureName, bool& value) const;

	// Add/remove XSD schemas for validation. The namespaceURI
	// can be an empty string.
	bool AddValidationSchema(const std::wstring& namespaceURI, const std::wstring& xsdPath);
	bool RemoveValidationSchema(const std::wstring& namespaceURI);

	// Parse a local file path, or a HTTP URL path.
	bool ParseUrl(const std::wstring& xmlPath);

	bool Parse(const std::wstring& xmlSource);
	bool ReadBuffer (std::wstring& _buffer);
private:
//	// Use the impl technique so we can hide the implementation
//	// and not require wrapper clients to import MSXML types.
//	// CXmlParserImpl uses wide-char strings natively.
//	CSAXXmlParserImpl* m_impl;
private:
	void CreateSaxReader();
	void CreateSchemaCache();

	// SAX reader.
	CComPtr<MSXML2::ISAXXMLReader> m_reader;
	//MSXML2::IMXWriter*     m_writer;
	//CComPtr<IStream>       m_spIStream;

	// Content and error handlers to be used with the SAX reader.
	//MSXML2::ISAXContentHandler* m_contentHandler2;
	//MSXML2::ISAXErrorHandler  * m_errorHandler2;
	//CSAXContentHandler* m_contentHandler;
	//CSAXErrorHandler*   m_errorHandler;
	// Schema cache for XSD validation.
	CComPtr<MSXML2::IXMLDOMSchemaCollection2> m_schemaCache;

	//// Our own XML events handler.
	//ISAXXmlElementHandler*  m_attachElementHandler;
};

/******************************* eof *************************************/