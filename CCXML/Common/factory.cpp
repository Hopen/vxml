/************************************************************************/
/* Name     : CacheInterface\factory.cpp                                */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 05 May 2010                                               */
/************************************************************************/

#pragma once

#include "StdAfx.h"
#include "factory.h"
//#include "sv_strutils.h"

namespace Factory
{
	std::wostream& operator<<(std::wostream& os, const CFTag& b ) 
	{ 
		UNREFERENCED_PARAMETER(b); 
		return os; 
	}
}

/******************************* eof *************************************/
