/************************************************************************/
/* Name     : Common\eventdispatcher.h                                  */
/* Author   : Andrey Alekseev                                           */
/* Company  : ExpertSolutions                                           */
/* Date     : 11 Apr 2018                                               */
/************************************************************************/

#pragma once
#include "lockguard.h"

namespace common
{
	template <class TEventObject, class TBaseLockGuard>
	class CEventDispatcher
	{
	public:
		void PushFront(TEventObject _event)
		{
			LockGuard<std::mutex, TBaseLockGuard> lock(L"CEventDispatcher::PushFront", m_mutex);
			//std::lock_guard<std::mutex> lock(m_mutex);
			m_EvtQ.push_front(_event);
		}

		void PushBack(TEventObject _event)
		{
			//std::lock_guard<std::mutex> lock(m_mutex);
			LockGuard<std::mutex, TBaseLockGuard> lock(L"CEventDispatcher::PushBack", m_mutex);
			m_EvtQ.push_back(_event);
		}

		TEventObject GetEvent(const std::wstring& aName, const ObjectId aCallBackId)
		{
			//std::lock_guard<std::mutex> lock(m_mutex);
			LockGuard<std::mutex, TBaseLockGuard> lock(L"CEventDispatcher::GetEvent", m_mutex);

			TEventObject evt;
			if (m_EvtQ.empty())
			{
				return evt;
			}

			const auto citEvent = std::find_if(m_EvtQ.cbegin(), m_EvtQ.cend(),
				[aName, aCallBackId](const TEventObject& _event)
			{
				if (_event->GetName() != aName)
				{
					return false;
				}

				auto callback = _event->GetSafeValue(L"CallbackID");
				if (!callback.IsEmpty() && callback.vValue.llVal == aCallBackId)
				{
					return true;
				}
				return false;
			});

			if (citEvent != m_EvtQ.cend())
			{
				evt = *citEvent;
				m_EvtQ.erase(citEvent);
			}

			return evt;
		}

		TEventObject GetFirstEvent()
		{
			//std::lock_guard<std::mutex> lock(m_mutex);
			LockGuard<std::mutex, TBaseLockGuard> lock(L"CEventDispatcher::GetFirstEvent", m_mutex);

			TEventObject evt;
			if (m_EvtQ.empty())
			{
				return evt;
			}

			evt = *(m_EvtQ.begin());
			m_EvtQ.pop_front();

			return evt;
		}

		void DeleteEvent(TEventObject aEvent)
		{
			//std::lock_guard<std::mutex> lock(m_mutex);
			LockGuard<std::mutex, TBaseLockGuard> lock(L"CEventDispatcher::DeleteEvent", m_mutex);
		}

		size_t GetQueueSize() const
		{
			//std::lock_guard<std::mutex> lock(m_mutex);
			LockGuard<std::mutex, TBaseLockGuard> lock(L"CEventDispatcher::GetQueueSize", m_mutex);
			return m_EvtQ.size();
		}

		const CComVariant FindAttrEvent(const std::wstring& aAttrName) const
		{
			CComVariant result;
			//std::lock_guard<std::mutex> lock(m_mutex);
			LockGuard<std::mutex, TBaseLockGuard> lock(L"CEventDispatcher::FindAttrEvent", m_mutex);
			for (const auto& evt : m_EvtQ)
			{
				const auto value = evt->GetValue(aAttrName);
				if (!value.IsEmpty())
				{
					result = value.vValue;
					break;
				}
			}
			return result;
		}

	private:
		using EventQueue = std::list<TEventObject>;
		EventQueue m_EvtQ;
		mutable std::mutex m_mutex;
	};
}

/******************************* eof *************************************/