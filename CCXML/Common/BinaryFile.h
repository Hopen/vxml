#pragma once
#include <Windows.h>

class CBinaryFile
{
	HANDLE m_hFile;

public:
	CBinaryFile(void);
	~CBinaryFile(void);

	void  Open(LPCTSTR _lpszFileName, DWORD dwFlags);
	DWORD Read(void * pBuffer, size_t size);
	void Write(void * pBuffer, size_t size);
	void Close();

	DWORD GetFileSize()
	{
		return ::GetFileSize(m_hFile, NULL);
	}
};
