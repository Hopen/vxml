/************************************************************************/
/* Name     : CacheInterface\factory.h                                  */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-IT                                                  */
/* Date     : 05 May 2010                                               */
/************************************************************************/

#pragma once

//#include "stdafx.h"
#include "windows.h"
#include <string>
#include <sstream> 
#include <fstream>
#include <vector>

//#include <boost/archive/text_iarchive.hpp>
//#include <boost/archive/text_oarchive.hpp>

#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>

#include "SAXXmlDataTypes.h"

namespace Factory
{
	class CFTag:public ISATXMLElement
	{
	private:
		friend class boost::serialization::access;
		friend std::wostream& operator<<(std::wostream& os, const CFTag& b ); 

		template<class Archive> 
		void save(Archive& ar, const unsigned int version) const 
		{
			UNREFERENCED_PARAMETER(version); 
			ar << BOOST_SERIALIZATION_NVP(m_sName);
			ar << BOOST_SERIALIZATION_NVP(m_sText);
			ar << BOOST_SERIALIZATION_NVP(m_Attribs);
			ar << BOOST_SERIALIZATION_NVP(m_iLine);
		} 

		template<class Archive> 
		void load(Archive& ar, const unsigned int version) 
		{
			UNREFERENCED_PARAMETER(version); 
			ar >> BOOST_SERIALIZATION_NVP(m_sName);
			ar >> BOOST_SERIALIZATION_NVP(m_sText);
			ar >> BOOST_SERIALIZATION_NVP(m_Attribs);
			ar >> BOOST_SERIALIZATION_NVP(m_iLine);
		} 

		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			boost::serialization::split_member(ar, *this, version); 
		}
	public:
		CFTag(){}
		virtual ~CFTag(){}

		explicit CFTag(const ISATXMLElement& rhs)
		{
			m_sName   = rhs.GetName();
			m_sText   = rhs.GetText();
			m_iLine   = rhs.GetLine();
			m_Attribs = rhs.GetAttr();
		}

	};

	BOOST_SERIALIZATION_ASSUME_ABSTRACT(CFTag)

	class CCollector:public CFTag//, IDispatch
	{
	private:
		friend class boost::serialization::access;

		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			boost::serialization::split_member(ar, *this, version);
			ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(CFTag);
		}

		template<class Archive> 
		void save(Archive& ar, const unsigned int version) const 
		{ 
			UNREFERENCED_PARAMETER(version); 
			ar << BOOST_SERIALIZATION_NVP(m_Children);

		} 

		template<class Archive> 
		void load(Archive& ar, const unsigned int version) 
		{ 
			UNREFERENCED_PARAMETER(version); 
			ar >> BOOST_SERIALIZATION_NVP(m_Children);
		} 

	public:
		typedef std::vector<CCollector> TagContainer;
	private:
		TagContainer m_Children;
		CCollector* m_pParent;
	public:
		CCollector():m_pParent(NULL){}
		//CCollector(MSXML2::IXMLDOMNodePtr node);
		virtual ~CCollector(){}
		explicit CCollector(const CFTag& rhs)
		{
			m_sName   = rhs.GetName();
			m_sText   = rhs.GetText();
			m_iLine   = rhs.GetLine();
			m_Attribs = rhs.GetAttr();
			m_pParent = NULL;
		}
		explicit CCollector(const CCollector& rhs)
		{
			m_sName    = rhs.GetName();
			m_sText    = rhs.GetText();
			m_iLine    = rhs.GetLine();
			m_Attribs  = rhs.GetAttr();
			m_Children = rhs.m_Children;
			m_pParent  = rhs.m_pParent;
		}

		CCollector* Add (CCollector& _child){
			_child.SetParent(this);
			m_Children.push_back(_child);
			return &m_Children[m_Children.size() - 1];
		}
		const TagContainer* GetChildren()const {return &m_Children;}
		void SetParent(CCollector* _parent){m_pParent = _parent;}
		CCollector* GetParent()const{return m_pParent;}
	};
}

/******************************* eof *************************************/