/************************************************************************/
/* Name     : Common\lockguard.h                                        */
/* Author   : Andrey Alekseev                                           */
/* Company  : ExpertSolutions                                           */
/* Date     : 11 Apr 2018                                               */
/************************************************************************/
#pragma once
#include <string>
#include <mutex>

namespace common
{
	template <class TMutex, class TBaseLockGuard>
	class LockGuard : public TBaseLockGuard
	{
	public:
		LockGuard(const std::wstring& aName, TMutex& aMutex)
			: TBaseLockGuard(aName)
			, mLock(aMutex)
		{
		}
	private:
		std::lock_guard<TMutex> mLock;
	};
}

/******************************* eof *************************************/