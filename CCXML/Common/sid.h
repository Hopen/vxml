/************************************************************************/
/* Name     : Common\sid.h                                              */
/* Author   : Andrey Alekseev                                           */
/* Company  : Forte-CT                                                  */
/* Date     : 25 Mar 2015                                               */
/************************************************************************/
#pragma once
#include <vector>


using ObjectId = /*unsigned*/ __int64 ;
using ObjectIdCollector = std::vector<ObjectId>;
//using ObjectIdCollector = std::vector<std::wstring>;

///* Global variable */
//namespace Global
//{
//	extern ObjectId g_scriptID;
//}


//struct Global
//{
//	static ObjectId g_scriptID;
//};
//
//template <class TModule>
//struct Global2
//{
//public:
//	Global2(TModule* aModuleRef)
//	{
//		mModulePtr = aModuleRef;
//	}
//
//	static ObjectId Get()
//	{
//		std::lock_guard<std::mutex> lock(mMutex);
//		if (mModulePtr)
//			return mModulePtr->Get();
//
//		return 0;
//	}
//
//	static void Set(ObjectId aObjectId)
//	{
//		std::lock_guard<std::mutex> lock(mMutex);
//		if (mModulePtr)
//		{
//			mModulePtr->Set(aObjectId);
//		}
//	}
//private:
//	static TModule* mModulePtr;
//	std::mutex mMutex;
//};

ObjectId GetGlobalScriptId();
void SetGlobalScriptId(ObjectId aObjectId);

//class CScriptIDHolder : public singleton < CScriptIDHolder >
//{
//	friend class singleton < CScriptIDHolder > ;
//
//private:
//	CScriptIDHolder() :m_scriptID(0)
//	{
//
//	}
//
//public:
//	void SetScriptID(__int64 _scriptID)
//	{
//		m_scriptID = _scriptID;
//	}
//
//	__int64 GetScriptID()const
//	{
//		return m_scriptID;
//	}
//
//private:
//	__int64 m_scriptID;
//};



/******************************* eof *************************************/