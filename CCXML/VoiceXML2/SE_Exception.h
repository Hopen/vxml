#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include <eh.h>
#include <string>
#include <new>
using namespace std;

#define SAFEDELETEBUFFER(buf) \
	if (buf) {\
		delete [] buf;\
		buf = NULL;\
	}

class CSE_Exception
{
private:
	unsigned int nSE;
	DWORD ExceptionCode;
	DWORD ExceptionFlags;
	PVOID ExceptionAddress;
	string ExceptionInfo;
	string RegistersInfo;
	string CallstackInfo;

	void ExtractExceptionInfo(PEXCEPTION_RECORD pExceptionRecord)
	{
		ExceptionCode = pExceptionRecord->ExceptionCode;
		ExceptionFlags = pExceptionRecord->ExceptionFlags;
		ExceptionAddress = pExceptionRecord->ExceptionAddress;

		char *buffer = NULL, *pbuf = NULL;
		try {
			pbuf = buffer = new char [4096];
			memset(buffer, 0, 4096);
			_snprintf(pbuf,4095 - strlen(buffer),"Exception: code=%p, flags=%p, address=0x%p\r\nException name: ", ExceptionCode, ExceptionFlags, ExceptionAddress );
			pbuf += strlen(pbuf);

			switch (ExceptionCode)
			{
			case EXCEPTION_ACCESS_VIOLATION :
				_snprintf(pbuf,4095 - strlen(buffer),"EXCEPTION_ACCESS_VIOLATION" );
				break;

			case EXCEPTION_ARRAY_BOUNDS_EXCEEDED :
				_snprintf(pbuf,4095 - strlen(buffer),"EXCEPTION_ARRAY_BOUNDS_EXCEEDED" );
				break;

			case EXCEPTION_BREAKPOINT :
				_snprintf(pbuf,4095 - strlen(buffer),"EXCEPTION_BREAKPOINT" );
				break;

			case EXCEPTION_DATATYPE_MISALIGNMENT :
				_snprintf(pbuf,4095 - strlen(buffer),"EXCEPTION_DATATYPE_MISALIGNMENT" );
				break;

			case EXCEPTION_FLT_DENORMAL_OPERAND :
				_snprintf(pbuf,4095 - strlen(buffer),"EXCEPTION_FLT_DENORMAL_OPERAND" );
				break;

			case EXCEPTION_FLT_DIVIDE_BY_ZERO :
				_snprintf(pbuf,4095 - strlen(buffer),"EXCEPTION_FLT_DIVIDE_BY_ZERO" );
				break;

			case EXCEPTION_FLT_INEXACT_RESULT :
				_snprintf(pbuf,4095 - strlen(buffer),"EXCEPTION_FLT_INEXACT_RESULT" );
				break;

			case EXCEPTION_FLT_INVALID_OPERATION :
				_snprintf(pbuf,4095 - strlen(buffer),"EXCEPTION_FLT_INVALID_OPERATION" );
				break;

			case EXCEPTION_FLT_OVERFLOW :
				_snprintf(pbuf,4095 - strlen(buffer),"EXCEPTION_FLT_OVERFLOW" );
				break;

			case EXCEPTION_FLT_STACK_CHECK :
				_snprintf(pbuf,4095 - strlen(buffer),"EXCEPTION_FLT_STACK_CHECK" );
				break;

			case EXCEPTION_FLT_UNDERFLOW :
				_snprintf(pbuf,4095 - strlen(buffer),"EXCEPTION_FLT_UNDERFLOW" );
				break;

			case EXCEPTION_ILLEGAL_INSTRUCTION :
				_snprintf(pbuf,4095 - strlen(buffer),"EXCEPTION_ILLEGAL_INSTRUCTION" );
				break;

			case EXCEPTION_IN_PAGE_ERROR :
				_snprintf(pbuf,4095 - strlen(buffer),"EXCEPTION_IN_PAGE_ERROR" );
				break;

			case EXCEPTION_INT_DIVIDE_BY_ZERO :
				_snprintf(pbuf,4095 - strlen(buffer),"EXCEPTION_INT_DIVIDE_BY_ZERO" );
				break;

			case EXCEPTION_INT_OVERFLOW :
				_snprintf(pbuf,4095 - strlen(buffer),"EXCEPTION_INT_OVERFLOW" );
				break;

			case EXCEPTION_INVALID_DISPOSITION :
				_snprintf(pbuf,4095 - strlen(buffer),"EXCEPTION_INVALID_DISPOSITION" );
				break;

			case EXCEPTION_NONCONTINUABLE_EXCEPTION :
				_snprintf(pbuf,4095 - strlen(buffer),"EXCEPTION_NONCONTINUABLE_EXCEPTION" );
				break;

			case EXCEPTION_PRIV_INSTRUCTION :
				_snprintf(pbuf,4095 - strlen(buffer),"EXCEPTION_PRIV_INSTRUCTION" );
				break;

			case EXCEPTION_STACK_OVERFLOW :
				_snprintf(pbuf,4095 - strlen(buffer),"EXCEPTION_STACK_OVERFLOW" );
				break;

			case EXCEPTION_SINGLE_STEP :
				_snprintf(pbuf,4095 - strlen(buffer),"EXCEPTION_SINGLE_STEP" );
				break;

			default:
				_snprintf(pbuf,4095 - strlen(buffer),"unknown" );
			}

			ExceptionInfo = buffer;
			SAFEDELETEBUFFER(buffer);
		}
		catch(bad_alloc &) {
			buffer = NULL;
			throw;
		}
		catch(...) {
			SAFEDELETEBUFFER(buffer);
			throw;
		}
	}
	void ExtractRegisterInfo(PCONTEXT pContextRecord)
	{
		char *buffer = NULL;
		try {
			buffer = new char [4096];
			memset(buffer, 0, 4096);
			_snprintf(buffer, 4095, "Registers:\r\nEAX=%p EBX=%p ECX=%p EDX=%p\r\nDS:ESI=%04X:%p ES:EDI=%04X:%p\r\nCS:EIP=%04X:%p SS:ESP=%04X:%p",
									pContextRecord->Eax,pContextRecord->Ebx,pContextRecord->Ecx,pContextRecord->Edx,
									pContextRecord->SegDs,pContextRecord->Esi, pContextRecord->SegEs,pContextRecord->Edi,
									pContextRecord->SegCs,pContextRecord->Eip, pContextRecord->SegSs,pContextRecord->Esp
					  );
			RegistersInfo = buffer;
			SAFEDELETEBUFFER(buffer);
		}
		catch(bad_alloc &) {
			buffer = NULL;
			throw;
		}
		catch(...) {
			SAFEDELETEBUFFER(buffer);
			throw;
		}
	}
	void ExtractCallstackInfo(PCONTEXT pContextRecord)
	{
		char *buffer = NULL, *pbuf = NULL;
		try {
			pbuf = buffer = new char [4096];
			memset(buffer, 0, 4096);
			LPDWORD EBP	  = (LPDWORD)pContextRecord->Ebp;
			_snprintf(pbuf,4095 - strlen(buffer),"CallStack:");
			pbuf += strlen(pbuf);

			while(true)
			{
				if(IsBadReadPtr(EBP + 1, 4))	break;
				_snprintf(pbuf,4095 - strlen(buffer),"\r\n\t%p",*(EBP+1));
				pbuf += strlen(pbuf);
				if(IsBadReadPtr(EBP,4))			break;
				EBP = (LPDWORD)*EBP;
			}

			CallstackInfo = buffer;
			SAFEDELETEBUFFER(buffer);
		}
		catch(bad_alloc &) {
			buffer = NULL;
			throw;
		}
		catch(...) {
			SAFEDELETEBUFFER(buffer);
			throw;
		}
	}
public:

	CSE_Exception(void){
		nSE = 0;
		ExceptionCode = 0;
		ExceptionFlags = 0;
		ExceptionAddress = 0;
		ExceptionInfo = "";
		RegistersInfo = "";
		CallstackInfo = "";
	}
	CSE_Exception( unsigned int n, PEXCEPTION_POINTERS pExceptionPointers ) : nSE( n )
	{
		ExtractExceptionInfo(pExceptionPointers->ExceptionRecord);
		ExtractRegisterInfo(pExceptionPointers->ContextRecord);
		ExtractCallstackInfo(pExceptionPointers->ContextRecord);
	}

	~CSE_Exception(void){}

	unsigned int getSeNumber() { return nSE; }
	DWORD getExceptionCode() { return ExceptionCode; }
	DWORD getExceptionFlags() { return ExceptionFlags; }
	PVOID getExceptionAddress() { return ExceptionAddress; }

	string getExceptionStringInfo()
	{
		string StrResult = ExceptionInfo + string("\r\n") + RegistersInfo + string("\r\n") + CallstackInfo;
		return StrResult;
	}

	static void trans_func( unsigned int u, EXCEPTION_POINTERS* pExp)
	{
		throw CSE_Exception(u, pExp);
	}
};
