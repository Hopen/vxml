// TelLibTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#import "..\TelLib\TelLib.tlb" no_namespace


int wmain(int argc, wchar_t* argv[])
{
	::CoInitialize(NULL);

	try
	{
		IVOXPtr pVox;
		HRESULT hr = pVox.CreateInstance(__uuidof(VOX));
		if (FAILED(hr))
		{
			throw _com_error(hr);
		}

		_bstr_t Files = pVox->GetSpeakNumberFiles(
			CComVariant("123.456"), ssMale, VARIANT_FALSE);
		if (FAILED(hr))
		{
			throw _com_error(hr);
		}

		std::wstring str((LPCWSTR)Files, Files.length());
		wprintf(L"GetSpeakNumberFiles() returned \"%s\"\n", str.c_str());

		SYSTEMTIME st = {0};
		DOUBLE dblTime = 0.0;
		::GetLocalTime(&st);
		::SystemTimeToVariantTime(&st, &dblTime);
		CComVariant Time(dblTime);

		Files = pVox->GetSpeakTimeFiles(
			Time, tfHours | tfMinutes | tfSeconds);
		if (FAILED(hr))
		{
			throw _com_error(hr);
		}

		str = std::wstring((LPCWSTR)Files, Files.length());
		wprintf(L"GetSpeakTimeFiles() returned \"%s\"\n", str.c_str());

		Files = pVox->GetSpeakDateFiles(
			Time, dfDays | dfMonths | dfYears);
		if (FAILED(hr))
		{
			throw _com_error(hr);
		}

		str = std::wstring((LPCWSTR)Files, Files.length());
		wprintf(L"GetSpeakDateFiles() returned \"%s\"\n", str.c_str());

		Files = pVox->GetSpeakMoneyFiles(CComVariant(12345), _bstr_t(L"roubles"));
		if (FAILED(hr))
		{
			throw _com_error(hr);
		}

		str = std::wstring((LPCWSTR)Files, Files.length());
		wprintf(L"GetSpeakMoneyFiles() returned \"%s\"\n", str.c_str());
	}
	catch (_com_error& e)
	{
		wprintf(L"COM error: 0x%08x\n", e.Error());
	}
	catch (...)
	{
		wprintf(L"Exception occurred.\n");
	}

	::CoUninitialize();

	return 0;
}

